unit ViewFileErrorUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Avk11ViewUnit;

type
  TViewFileErrorForm = class(TForm)
    StatusBar1: TStatusBar;
    ListView2: TListView;
    procedure ListView2DblClick(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
  private
    FVF: TAvk11ViewForm;
  public
    procedure CreateList(VF: TAvk11ViewForm);
  end;

var
  ViewFileErrorForm: TViewFileErrorForm;

implementation

{$R *.dfm}

procedure TViewFileErrorForm.CreateList(VF: TAvk11ViewForm);
var
  I: Integer;
  New: TListItem;
  Event: TStringList;
  Chip, Page: Integer;

procedure LineArrToChipPage(LineAddr, ChipCount: Integer; var Chip, Page: Integer);
var
  I: Integer;
begin
  Chip:= 0;
  Page:= 0;
  for I:= 1 to LineAddr do
  begin
    Inc(Chip);
    if Chip >= ChipCount then
    begin
      Chip:= 0;
      Inc(Page);
    end;
  end;
end;

begin
(*  FVF:= VF;
  ListView2.Items.Clear;
  ListView2.Items.BeginUpdate;
  for I:= 0 to FVF.DatSrc.ErrorCount - 1 do
  begin
    New:= ListView2.Items.Add;
    New.Caption:= ErrorList[FVF.DatSrc.Error[I].Code];
    New.SubItems.Add(Format('%d', [FVF.DatSrc.Error[I].BegDisCoord]));
    New.SubItems.Add(Format('%x, %d', [FVF.DatSrc.Error[I].Offset, FVF.DatSrc.Error[I].Offset]));
    New.SubItems.Add(Format('%x', [FVF.DatSrc.Error[I].Size]));
//    New.SubItems.Add(Format('%d', [FVF.DatSrc.FErrorsEvt[I]]));
//    New.SubItems.Add(Format('%d', [FVF.DatSrc.FFixOffset[I]]));
{
    LineArrToChipPage(FVF.DatSrc.Error[I].Offset div 1046, 8, Chip, Page);
    New.SubItems.Add(Format('%d', [Chip]));
    New.SubItems.Add(Format('%d', [Page]));
}
    New.Data:= Pointer(I);
  end;
  ListView2.Items.EndUpdate;
  ListView2.Repaint; *)
end;

procedure TViewFileErrorForm.ListView2DblClick(Sender: TObject);
begin
  if Assigned(ListView2.Selected) then
  begin
//    FVF.Display.CenterDisCoord:= FVF.DatSrc.Error[Integer(ListView2.Selected.Data)].BegDisCoord;
//    FVF.Display.Refresh;
  end;
end;

procedure TViewFileErrorForm.FormShortCut(var Msg: TWMKey;
  var Handled: Boolean);
begin
  if Msg.CharCode = 27 then Self.ModalResult:= mrOK;
end;

end.
