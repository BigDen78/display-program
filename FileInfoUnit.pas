{$I DEF.INC}
unit FileInfoUnit; {Language 9}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ValEdit, LanguageUnit, jpeg, ExtCtrls,
  ComCtrls, StdCtrls, AviconDataSource, AviconTypes;

type
  TFileInfoForm = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    Panel2: TPanel;
    CloseButton: TButton;
    procedure OnChangeLanguage(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
    procedure Memo1Click(Sender: TObject);
    procedure Memo1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FileInfoForm: TFileInfoForm;

procedure ShowFileInfo(DS: TAvk11DatSrc);
procedure FillFileInfoText(DS: TAvk11DatSrc; Memo1: TMemo);

implementation

{$R *.dfm}

uses
  ConfigUnit, MainUnit;


procedure FillFileInfoText(DS: TAvk11DatSrc; Memo1: TMemo);
var
  I: Integer;
  S: string;
  S1: string;
  S2: string;
  WorkTime: Extended;
  WorkLen: Extended;
  ValueId, Value: Variant;
  FormatPos1: Integer;
  T1, T2: string;
  StartT, EndT: TimeListItem;
  EndMetre: Integer;
  SystemTime1: TSystemTime;
  SystemTime2: TSystemTime;

function MyFormat1(Src1, Src2: string; Shift: Integer = 0): string;
var
  I: Integer;

begin
  while Length(Src1) < FormatPos1 do Src1:= Src1 + ' ';
  Result:= Src1 + Src2;
  for I := 1 to Shift do Result:= ' ' + Result;
end;

begin
  //    S:= LangTable.Caption['InfoBar:NameMaxLen'];
//    FormatPos1:= Length(S);
    FormatPos1:= Length(LangTable.Caption['InfoBar:NameMaxLen']);
    Memo1.Clear;

//    T1:= GetDeviceNameTextId(DS.Header.DeviceID);
//    T2:= LangTable.Caption['InfoBar:' + GetDeviceNameTextId(DS.Header.DeviceID)];

    Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:Device'], LangTable.Caption['InfoBar:' + GetDeviceNameTextId(DS.Header.DeviceID) + Config.GetProgramModeId]));

    if DS.FullHeader then
    begin
      if DS.Header.UsedItems[uiRailRoadName] = 1    then // �������� �������� ������
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:RailRoadName'], HeaderStrToString(DS.Header.RailRoadName)));
      if DS.Header.UsedItems[uiOrganization] = 1    then // �������� ����������� �������������� ��������
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:Organization'], HeaderStrToString(DS.Header.Organization)));
      if DS.Header.UsedItems[uiPathSectionName] = 1 then // �������� ������� ������
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:PathSectionName'], HeaderStrToString(DS.Header.PathSectionName)));
      if DS.Header.UsedItems[uiDirectionCode] = 1   then // ��� �����������
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:DirectionCode'], IntToStr(DS.Header.DirectionCode)));

      if DS.Header.UsedItems[uiDateTime]  = 1       then // ���� / ����� ��������
      begin

  //      UpgradeStr(Src: string; Ch: Char; Len: Integer): string;

        StartT.H:= DS.Header.Hour;
        StartT.M:= DS.Header.Minute;
        if DS.GetTimeListCount <> 0 then EndT:= DS.GetTimeList(DS.GetTimeListCount - 1)
                                    else EndT:= StartT;

  {     S1:= IntToStr(DS.Header.Day);
        while Length(S1) < 2 do S1:= '0' + S1;
        S2:= IntToStr(DS.Header.Month);
        while Length(S2) < 2 do S2:= '0' + S2; }



        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:Date'], Format('%s:%s:%d', [UpgradeInt(DS.Header.Day, '0', 2), UpgradeInt(DS.Header.Month, '0', 2), DS.Header.Year])));
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:Time'], Format('%s:%s - %s:%s', [UpgradeInt(StartT.H, '0', 2), UpgradeInt(StartT.M, '0', 2), UpgradeInt(EndT.H, '0', 2), UpgradeInt(EndT.M, '0', 2)])));


        SystemTime1.wYear:=   DS.Header.Year;
        SystemTime1.wMonth:=  DS.Header.Month;
        SystemTime1.wDay:=    DS.Header.Day;
        SystemTime1.wHour:=   StartT.H;
        SystemTime1.wMinute:= StartT.M;

        SystemTime2.wYear:=   2000;
        SystemTime2.wMonth:=  1;
        SystemTime2.wDay:=    1;
        SystemTime2.wHour:=   EndT.H;
        SystemTime2.wMinute:= EndT.M;

//        Memo1.Lines.Add('1 -' + MyFormat1(LangTable.Caption['InfoBar:Date'], DateToStr(SystemTimeToDateTime(SystemTime1))));
//        Memo1.Lines.Add('2 -' + MyFormat1(LangTable.Caption['InfoBar:Time'], Format('%s - %s', [TimeToStr(SystemTimeToDateTime(SystemTime1)), TimeToStr(SystemTimeToDateTime(SystemTime2))])));
      end;
      if DS.Header.UsedItems[uiRailPathNumber] = 1  then // ����� �/� ����
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:RailPathNumber'], Format('%d', [DS.Header.RailPathNumber])));

      if DS.Header.UsedItems[uiRailPathTextNumber] = 1  then // ����� �/� ����
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:RailPathNumber'], HeaderStrToString(DS.Header.RailPathTextNumber)));


      if DS.Header.UsedItems[uiWorkRailTypeA] = 1 then// �������������� ���� - ��� ������������ ��������
      begin
        if DS.Header.WorkRailTypeA = 0
          then Memo1.Lines.Add(MyFormat1(LangTable.Caption['Common:Rail'], LangTable.Caption['Common:Left']))
          else Memo1.Lines.Add(MyFormat1(LangTable.Caption['Common:Rail'], LangTable.Caption['Common:Right']));
      end;

      if DS.Header.UsedItems[uiPathCoordSystem] = 1     then // ������� ��������� ���������
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:PathCoordSystem'], LangTable.Caption[Format('InfoBar:PathCoordSystem_id%d', [Ord(DS.CoordSys)])]));

      if DS.Header.UsedItems[uiStartMetric] = 1     then // ��������� ����������
      begin
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:StartMetric'], MRFCrdToStr(DS.StartMRFCrd)));
        if DS.Header.HeaderVer > 1 then
          Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:EndCoord'], MRFCrdToStr(DS.EndMRFCrd)));
      end;

      if DS.Header.UsedItems[uiStartChainage] = 1     then // ��������� ����������
      begin
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:StartMetric'], CaCrdToStr(DS.CoordSys, DS.StartCaCrd)));
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:EndCoord'], CaCrdToStr(DS.CoordSys, DS.EndCaCrd)));
      end;

      if DS.Header.UsedItems[uiTrackDirection] = 1 then // ��� �����������
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:TrackDirection'], DS.Header.TrackDirection[0] + DS.Header.TrackDirection[1]));

      if (ConfigUnit.Config.ProgramMode = pmRADIOAVIONICA) or (ConfigUnit.Config.ProgramMode = pmRADIOAVIONICA_KZ) then
      begin

        if DS.isA31 then
        begin

          if DS.Header.UsedItems[uiSetMaintenanceServicesDate] = 1 then // ���� ������������ ������������ (������ ������)
            Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:MaintenanceServicesDate'], Format('%s:%s:%d', [UpgradeInt(DS.Header.MaintenanceServicesDay, '0', 2), UpgradeInt(DS.Header.MaintenanceServicesMonth, '0', 2), DS.Header.MaintenanceServicesYear])));

          if DS.Header.UsedItems[uiSetCalibrationDate] = 1 then // ���� ����������
            Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:CalibrationDate'], Format('%s:%s:%s', [UpgradeInt(DS.Header.CalibrationDay, '0', 2), UpgradeInt(DS.Header.CalibrationMonth, '0', 2), UpgradeInt(DS.Header.CalibrationYear, '0', 4)])));
        end;
      end;

      if DS.Header.UsedItems[uiSetCalibrationName] = 1 then // �������� ���������
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:CalibrationName'], HeaderStrToString(DS.Header.CalibrationName)));

      if (not DS.isA15New) and (not DS.isA15Old) and (not DS.isFilus_X17DW) then
        if DS.Header.UsedItems[uiCorrSides] = 1 then // ������������ ������� ������� ����� ����
          if DS.Header.CorrespondenceSides = csDirect
            then Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:CorrSides'], LangTable.Caption['InfoBar:Direct']))
            else Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:CorrSides'], LangTable.Caption['InfoBar:Inverse']));

      if DS.Header.UsedItems[uiTrackID] = 1 then        // TrackID
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:TrackID'], DS.Header.TrackID[0] + DS.Header.TrackID[1]));

      if DS.Header.UsedItems[uiRailSection]  = 1    then // ����� ������
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:RailSection'], Format('%d', [DS.Header.RailSection])));
      if DS.Header.UsedItems[uiOperatorName]  = 1   then // ��� ���������
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:OperatorName'], HeaderStrToString(DS.Header.OperatorName)));

      if DS.Header.UsedItems[uiMoveDir] = 1 then // ����������� ��������
      begin
        if DS.Header.MoveDir > 0 then Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:MoveDir'], LangTable.Caption['InfoBar:MoveDirInc']))
                                 else Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:MoveDir'], LangTable.Caption['InfoBar:MoveDirDec']));
      end;

      if DS.isEGOUSW or DS.isVMT_US_BigWP then
        if DS.Header.UsedItems[uiControlDir] = 1 then // ����������� ��������
        begin
          if DS.Header.ControlDirection = cd_Tail_B_Head_A
            then Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:ControlDir'], LangTable.Caption['InfoBar:cdHeadForward']))
            else Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:ControlDir'], LangTable.Caption['InfoBar:cdAssForward']));
        end;

      if DS.Header.UsedItems[uiFlawDetectorNumber] = 1   then // ����� ������������ (����)  // NOTranslate
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['����� ������������'], HeaderStrToString(DS.Header.UnitsInfo[9].WorksNumber)));

      if DS.Header.UsedItems[uiUnitsCount] = 1   then // ���������� ������ �������
      begin
        Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:DeviceUnits'], ''));
        for I := 0 to DS.Header.UnitsCount - 1 do
        begin
          Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:' + DeviceUnitTypeToTextId[DS.Header.UnitsInfo[I].UnitType]], '', 5));
          Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:WorksNumber'], HeaderStrToString(DS.Header.UnitsInfo[I].WorksNumber), 10));
          Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:FirmwareVer'], HeaderStrToString(DS.Header.UnitsInfo[I].FirmwareVer), 10));
        end;
      end;

      if MainForm.miDebug.Visible then
//        if (ConfigUnit.Config.ProgramMode = pmRADIOAVIONICA) and DS.isA31 then
        begin

          Memo1.Lines.Add(MyFormat1('', ''));
          Memo1.Lines.Add(MyFormat1('------ DEBUG ------', '------'));
          Memo1.Lines.Add(MyFormat1('', ''));

          if DS.Header.UsedItems[uiGPSTrackinDegrees] = 1 then
            Memo1.Lines.Add(MyFormat1(LangTable.Caption['������ GPS �����'], LangTable.Caption['���']))
            else
            Memo1.Lines.Add(MyFormat1(LangTable.Caption['������ GPS �����'], LangTable.Caption['����']));


          if DS.Header.UsedItems[uiAcousticContact] = 1 then // ������ ������������� ��������
            Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:AcousticContact'], LangTable.Caption['InfoBar:On']))
            else
            Memo1.Lines.Add(MyFormat1(LangTable.Caption['InfoBar:AcousticContact'], LangTable.Caption['InfoBar:Off']));

          if DS.ExHeader.Reserv2[1] = 1 then Memo1.Lines.Add(MyFormat1('C�����(�) ����������', '����'))
                                        else Memo1.Lines.Add(MyFormat1('C�����(�) ����������', '���'));

          if DS.ExHeader.Reserv2[0] = 1 then Memo1.Lines.Add(MyFormat1('�������� ������� c����� �����', '�1'));
          if DS.ExHeader.Reserv2[0] = 2 then Memo1.Lines.Add(MyFormat1('�������� ������� �����', '�2'));
        end;

    end;
end;


procedure ShowFileInfo(DS: TAvk11DatSrc);


begin
  if not Assigned(DS) then Exit;
  Application.CreateForm(TFileInfoForm, FileInfoForm);
  with FileInfoForm do
  begin
    FillFileInfoText(DS, Memo1);
  end;
  FileInfoForm.ShowModal;
  FileInfoForm.Free;
  FileInfoForm:= nil;
end;

procedure TFileInfoForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if Msg.CharCode in [13, 27] then Self.ModalResult:= mrOK;
end;

procedure TFileInfoForm.Memo1Click(Sender: TObject);
begin
//  CloseButton.SetFocus;
end;

procedure TFileInfoForm.Memo1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
//  CloseButton.SetFocus;
end;

procedure TFileInfoForm.OnChangeLanguage(Sender: TObject);
begin
  Self.Font.Name     := LangTable.Caption['General:FontName'];
  Self.Caption       := LangTable.Caption['InfoBar:FormCaption'];
  CloseButton.Caption:= LangTable.Caption['Common:CloseButton'];
end;

procedure TFileInfoForm.CloseButtonClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFileInfoForm.FormCreate(Sender: TObject);
begin
  OnChangeLanguage(nil);
end;

end.

