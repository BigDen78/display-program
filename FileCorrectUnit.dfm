object FileCorrectForm: TFileCorrectForm
  Left = 406
  Top = 238
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'FileCorrectForm'
  ClientHeight = 220
  ClientWidth = 274
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 83
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 8
    Top = 127
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Label3: TLabel
    Left = 8
    Top = 172
    Width = 32
    Height = 13
    Caption = 'Label3'
  end
  object Image1: TImage
    Left = 8
    Top = 8
    Width = 34
    Height = 35
    Picture.Data = {
      07544269746D617076020000424D760200000000000076000000280000002000
      0000200000000100040000000000000200000000000000000000100000000000
      0000000000000000800000800000008080008000000080008000808000008080
      8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
      FF00DDDDD7777777777777777777777777DDDDDD777777777777777777777777
      777DDD300000000000000000000000077777D3BBBBBBBBBBBBBBBBBBBBBBBB80
      77773BBBBBBBBBBBBBBBBBBBBBBBBBB807773BBBBBBBBBBBBBBBBBBBBBBBBBBB
      07773BBBBBBBBBBBB8008BBBBBBBBBBB077D3BBBBBBBBBBBB0000BBBBBBBBBB8
      077DD3BBBBBBBBBBB0000BBBBBBBBBB077DDD3BBBBBBBBBBB8008BBBBBBBBB80
      77DDDD3BBBBBBBBBBBBBBBBBBBBBBB077DDDDD3BBBBBBBBBBB0BBBBBBBBBB807
      7DDDDDD3BBBBBBBBB808BBBBBBBBB077DDDDDDD3BBBBBBBBB303BBBBBBBB8077
      DDDDDDDD3BBBBBBBB000BBBBBBBB077DDDDDDDDD3BBBBBBB80008BBBBBB8077D
      DDDDDDDDD3BBBBBB30003BBBBBB077DDDDDDDDDDD3BBBBBB00000BBBBB8077DD
      DDDDDDDDDD3BBBBB00000BBBBB077DDDDDDDDDDDDD3BBBBB00000BBBB8077DDD
      DDDDDDDDDDD3BBBB00000BBBB077DDDDDDDDDDDDDDD3BBBB00000BBB8077DDDD
      DDDDDDDDDDDD3BBB80008BBB077DDDDDDDDDDDDDDDDD3BBBBBBBBBB8077DDDDD
      DDDDDDDDDDDDD3BBBBBBBBB077DDDDDDDDDDDDDDDDDDD3BBBBBBBB8077DDDDDD
      DDDDDDDDDDDDDD3BBBBBBB077DDDDDDDDDDDDDDDDDDDDD3BBBBBB8077DDDDDDD
      DDDDDDDDDDDDDDD3BBBBB077DDDDDDDDDDDDDDDDDDDDDDD3BBBB807DDDDDDDDD
      DDDDDDDDDDDDDDDD3BB80DDDDDDDDDDDDDDDDDDDDDDDDDDDD333DDDDDDDDDDDD
      DDDD}
    Transparent = True
  end
  object Label4: TLabel
    Left = 48
    Top = 8
    Width = 217
    Height = 65
    AutoSize = False
    Caption = 'Label4'
    WordWrap = True
  end
  object Edit1: TComboBox
    Left = 24
    Top = 99
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 0
  end
  object Edit2: TComboBox
    Left = 24
    Top = 143
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 1
  end
  object Edit3: TComboBox
    Left = 24
    Top = 188
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 2
  end
  object Button1: TButton
    Left = 190
    Top = 185
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 3
  end
  object Panel1: TPanel
    Left = 176
    Top = 83
    Width = 2
    Height = 129
    TabOrder = 4
  end
end
