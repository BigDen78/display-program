{$I compiler.inc}

{ $ DEFINE OUTLOG}

unit EVideoUnit;

interface

uses
  {$IFDEF UNIX}Unix,{$ENDIF}
  {$IFDEF MSWINDOWS}Windows,{$ENDIF}
  Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Menus, ComCtrls, SyncObjs,
  PasLibVlcUnit, PasLibVlcClassUnit, PasLibVlcPlayerUnit;

type
  TVideoLink = record
    DisCrd: Integer;
    ms: Int64;
  end;

  TEVideoForm = class(TForm)
    PR: TPanel;
    PopupMenu: TPopupMenu;
    a1: TMenuItem;
    b1: TMenuItem;
    c1: TMenuItem;
    d1: TMenuItem;
    SaveDialog: TSaveDialog;
    LbPopupMenu: TPopupMenu;
    LbPmClear: TMenuItem;
    LnPmSaveAs: TMenuItem;
    Panel1: TPanel;
    LeftPlayer: TPasLibVlcPlayer;
    RightPlayer: TPasLibVlcPlayer;
    LeftLabel: TLabel;
    RightLabel: TLabel;
    Timer1: TTimer;
    Panel2: TPanel;
    Panel3: TPanel;
    LogMemo: TMemo;
    Button2: TButton;
    Button3: TButton;
    Button1: TButton;
    NextFrameBtn: TButton;
    SnapShotBtn: TButton;
    PlayBtn: TButton;
    PauseBtn: TButton;
    ResumeBtn: TButton;
    GetStateBtn: TButton;
    GetPosLenBtn: TButton;
    VideoAdjustBtn: TButton;
    Timer2: TTimer;
    Memo1: TMemo;
//    procedure PlayClick(Sender: TObject);
    procedure PauseClick(Sender: TObject);
    procedure GetStateClick(Sender: TObject);
    procedure ResumeClick(Sender: TObject);
    procedure GetPosLenClick(Sender: TObject);
    procedure SnapShotClick(Sender: TObject);
    procedure LeftPlayerMediaPlayerTimeChanged(Sender: TObject; time: Int64);
    procedure NextFrameClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure VideoAdjustBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure RightPlayerMediaPlayerTimeChanged(Sender: TObject; time: Int64);
    procedure RightPlayerMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormActivate(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure LeftPlayerMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    lastAudioVolume : string;
    UseLeft: boolean;
    start : DWORD;
    LeftLink: array of TVideoLink;
    RightLink: array of TVideoLink;

    Loaded_: Boolean;
    Loaded_Left: Boolean;
    Loaded_Right: Boolean;
    save_GetImage: Boolean;
    save_Left: Boolean;
    save_DisCrd: Integer;

    LeftLastPos: Integer;
    RightLastPos: Integer;

  public
    ViewForm: Pointer;

    function Open(Dir, FileName: string; Form: Pointer): Boolean;
    procedure OpenFiles(FN1, FN2: string);
    procedure GetImage(Left: Boolean; DisCrd: Integer);

    procedure SelectSide(UseLeft_: Boolean);
    procedure SetPosInMs(Pos: LongInt);
  end;

var
  EVideoForm: TEVideoForm;

implementation

uses
  Avk11ViewUnit, AviconTypes, MainUnit, ConfigUnit;

{$R *.dfm}

////////////////////////////////////////////////////////////////////////////////

function TEVideoForm.Open(Dir, FileName: string; Form: Pointer): Boolean;
var
  FNVideoLeft: string;
  FNVideoRight: string;
  FNVCoordLeft: string;
  FNVCoordRight: string;
  OKFlag: Boolean;
  TXT: TStringList;
  I, J: Integer;
  str: string;

function ExtractFileName_(in_: string): string;
var
  tmp: string;

begin
  tmp:= ExtractFileName(in_);
  Delete(tmp, Length(tmp) - Length(ExtractFileExt(in_)) + 1, Length(ExtractFileExt(in_)));
  Result:= tmp;
end;


begin
  if not Assigned(Form) then Exit;
  ViewForm:= Form;

//  ShowMessage('Open_1');

  FNVideoLeft:= Dir + ExtractFileName_(FileName) + '0.ts';
  FNVideoRight:= Dir + ExtractFileName_(FileName) + '1.ts';
  FNVCoordLeft:= Dir + ExtractFileName_(FileName) + '0.txt';
  FNVCoordRight:= Dir + ExtractFileName_(FileName) + '1.txt';

  OKFlag:= FileExists(FNVideoLeft) and FileExists(FNVideoRight) and FileExists(FNVCoordLeft) and FileExists(FNVCoordRight);
  if not OKFlag then
  begin
    Dir:= Dir + '\' + ExtractFileName_(FileName) + '\';
    FNVideoLeft:= Dir + ExtractFileName_(FileName) + '0.ts';
    FNVideoRight:= Dir + ExtractFileName_(FileName) + '1.ts';
    FNVCoordLeft:= Dir + ExtractFileName_(FileName) + '0.txt';
    FNVCoordRight:= Dir + ExtractFileName_(FileName) + '1.txt';
    OKFlag:= FileExists(FNVideoLeft) and FileExists(FNVideoRight) and FileExists(FNVCoordLeft) and FileExists(FNVCoordRight);
  end;

  if not OKFlag then
  begin
    Dir:= Config.VideoFilePath + '\' + ExtractFileName_(FileName) + '\';
    FNVideoLeft:= Dir + ExtractFileName_(FileName) + '0.ts';
    FNVideoRight:= Dir + ExtractFileName_(FileName) + '1.ts';
    FNVCoordLeft:= Dir + ExtractFileName_(FileName) + '0.txt';
    FNVCoordRight:= Dir + ExtractFileName_(FileName) + '1.txt';
    OKFlag:= FileExists(FNVideoLeft) and FileExists(FNVideoRight) and FileExists(FNVCoordLeft) and FileExists(FNVCoordRight);
  end;

//  ShowMessage('Open_2');

  if OKFlag then
  begin

    TXT:= TStringList.Create;
    TXT.LoadFromFile(FNVCoordLeft);

//  ShowMessage('Open_3');

    SetLength(LeftLink, TXT.Count);
    J:= 0;
    for I := 0 to TXT.Count - 1 do
    begin
      str:= TXT[I];
      if (TryStrToInt64(copy(str, 1, Pos(' ', str) - 1), LeftLink[J].ms) and
          TryStrToInt(copy(str, Pos(' ', str) + 1, Length(str)), LeftLink[J].DisCrd)) then inc(J);
    end;
    SetLength(LeftLink, J);

//  ShowMessage('Open_4');

    TXT.Clear;
    TXT.LoadFromFile(FNVCoordRight);
    SetLength(RightLink, TXT.Count);
    J:= 0;
    for I := 0 to TXT.Count - 1 do
    begin
      str:= TXT[I];
      if (TryStrToInt64(copy(str, 1, Pos(' ', str) - 1), RightLink[J].ms) and
          TryStrToInt(copy(str, Pos(' ', str) + 1, Length(str)), RightLink[J].DisCrd)) then inc(J);
    end;
    SetLength(RightLink, J);
    TXT.Free;

//  ShowMessage('Open_5');

    OpenFiles(FNVideoLeft, FNVideoRight);
    Result:= True;

//  ShowMessage('Open_6');

  end else Result:= False;

//  ShowMessage('Open_7');

  {$IFDEF OUTLOG}
  Memo1.Lines.Add(Format('OKFlag: %d', [Ord(OKFlag)]));
  Memo1.Lines.Add(Format('%d - FNVideoLeft: %s', [Ord(FileExists(FNVideoLeft)), FNVideoLeft]));
  Memo1.Lines.Add(Format('%d - FNVideoRight: %s', [Ord(FileExists(FNVideoRight)), FNVideoRight]));
  Memo1.Lines.Add(Format('%d - FNVCoordLeft: %s', [Ord(FileExists(FNVCoordLeft)), FNVCoordLeft]));
  Memo1.Lines.Add(Format('%d - FNVCoordRight: %s', [Ord(FileExists(FNVCoordRight)), FNVCoordRight]));
  {$ENDIF}

//  ShowMessage('Open_8');
end;

procedure TEVideoForm.OpenFiles(FN1, FN2: string);
begin

//  ShowMessage('OpenFiles_1');

  LeftPlayer.Visible:= False;
  RightPlayer.Visible:= False;

//  ShowMessage('Play');

  LeftPlayer.Play(FN1);

//  ShowMessage('Play-OK');

  RightPlayer.Play(FN2);

//  ShowMessage('OpenFiles_4');

  UseLeft:= True;
  Panel1.Caption:= '��������...';  // NOTRanslate

//  ShowMessage('OpenFiles_5');
  {$IFDEF OUTLOG}
  LogMemo.Lines.Add(Format('�������� ������: %s, %s', [FN1, FN2]));
  {$ENDIF}
end;

procedure TEVideoForm.SetPosInMs(Pos: LongInt);
begin
//  LeftPlayer.Visible:= False;
//  RightPlayer.Visible:= False;

  {$IFDEF OUTLOG}
  LogMemo.Lines.Add(Format('Set Pos_Ms: %d', [POS]));
  {$ENDIF}

  if (UseLeft) then
    if LeftPlayer.CanSeek() then
    begin
      {$IFDEF OUTLOG}
      LogMemo.Lines.Add('Set Pos - OK');
      {$ENDIF}
      LeftPlayer.SetVideoPosInMs(Pos);
      LeftLastPos:= Pos;
//      LeftPlayer.Repaint;
//      LeftPlayer.Resume;
    end else
    begin
      {$IFDEF OUTLOG}
      LogMemo.Lines.Add('Set Pos - Error');
      {$ENDIF}
    end;


  if (not UseLeft) then
    if RightPlayer.CanSeek() then
    begin
      {$IFDEF OUTLOG}
      LogMemo.Lines.Add('Set Pos - OK');
      {$ENDIF}
      RightPlayer.SetVideoPosInMs(Pos);
      RightLastPos:= Pos;
//      RightPlayer.Resume;
    end else
    begin
      {$IFDEF OUTLOG}
      LogMemo.Lines.Add('Set Pos - Error');
      {$ENDIF}
    end;
end;

procedure TEVideoForm.LeftPlayerMediaPlayerTimeChanged(Sender: TObject; time: Int64);
begin
  if not Loaded_ then
    if not Loaded_Left then
    begin
      Loaded_Left:= True;
      Loaded_:= Loaded_Right and Loaded_Left;
    end;

  LeftPlayer.Pause;
  LeftPlayer.Visible:= True;
  {$IFDEF OUTLOG}
  LogMemo.Lines.Add('Left Time Changed');
  LogMemo.Lines.Add(Format('Left: Len = %d ms, Pos = %d', [LeftPlayer.GetVideoLenInMs(), LeftPlayer.GetVideoPosInMs()]));
  {$ENDIF}

  if Loaded_ and save_GetImage then Timer2.Enabled:= True;
end;

procedure TEVideoForm.LeftPlayerMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  str: string;

begin
  if X < RightPlayer.Width div 2 then if LeftLastPos > 40 then LeftLastPos:= LeftLastPos - 40 else LeftLastPos:= 0;
  if X > LeftPlayer.Width div 2 then LeftLastPos:= LeftLastPos + 40;
  SetPosInMs(LeftLastPos);
//  Self.Caption:= IntToStr(LeftPlayer.GetVideoPosInMs()) + ' ' + IntToStr(RightPlayer.GetVideoPosInMs());
end;

procedure TEVideoForm.RightPlayerMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  str: string;

begin
  if X < RightPlayer.Width div 2 then if RightLastPos > 40 then RightLastPos:= RightLastPos - 40 else RightLastPos:= 0;
  if X > RightPlayer.Width div 2 then RightLastPos:= RightLastPos + 40;
  SetPosInMs(RightLastPos);

//  Self.Caption:= IntToStr(LeftPlayer.GetVideoPosInMs()) + ' ' + IntToStr(RightPlayer.GetVideoPosInMs());
end;

procedure TEVideoForm.RightPlayerMediaPlayerTimeChanged(Sender: TObject; time: Int64);
begin
  if not Loaded_ then
    if not Loaded_Right then
    begin
      Loaded_Right:= True;
      Loaded_:= Loaded_Right and Loaded_Left;
    end;

  RightPlayer.Pause;
  RightPlayer.Visible:= True;
  {$IFDEF OUTLOG}
  LogMemo.Lines.Add('Right Time Changed');
  LogMemo.Lines.Add(Format('Right: Len = %d ms, Pos = %d', [RightPlayer.GetVideoLenInMs(), RightPlayer.GetVideoPosInMs()]));
  {$ENDIF}

  if Loaded_ and save_GetImage then Timer2.Enabled:= True;
end;

procedure TEVideoForm.SelectSide(UseLeft_: Boolean);
begin
  if (UseLeft_) then
  begin
    LeftPlayer.Visible:= True;
    RightPlayer.Visible:= False;
    {$IFDEF OUTLOG}
    LogMemo.Lines.Add('Select Side Left');
    {$ENDIF}
  end
  else
  begin
    LeftPlayer.Visible:= False;
    RightPlayer.Visible:= True;
    {$IFDEF OUTLOG}
    LogMemo.Lines.Add('Select Side Right');
    {$ENDIF}
  end;
  UseLeft:= UseLeft_;
end;

procedure TEVideoForm.GetImage(Left: Boolean; DisCrd: Integer);
var
  I: Integer;
  RC: TRealCoord;
  CrdParams: TCrdParams;
  str: string;

begin
  if not Loaded_ then
  begin
    save_Left:= Left;
    save_DisCrd:= DisCrd;
    save_GetImage:= True;
    Exit;
  end;


  if Left then
  begin
//    ShowMessage('Enter Left');

    for I := 0 to Length(LeftLink) - 2 do
      if (DisCrd >= LeftLink[I].DisCrd) and (DisCrd < LeftLink[I + 1].DisCrd) then
      begin
        SelectSide(Left);
        SetPosInMs(LeftLink[I].ms div 1000000);
        {$IFDEF OUTLOG}
        LogMemo.Lines.Add(Format('Get Image - Side: Left; DisCrd: %d; Pos_Ms: %d', [DisCrd, LeftLink[I].ms div 1000000]));
        {$ENDIF}
        Break;
      end;

//    ShowMessage('Exit Left');
  end
  else
  begin
//    ShowMessage('Enter Rigth');
    for I := 0 to Length(RightLink) - 2 do
      if (DisCrd >= RightLink[I].DisCrd) and (DisCrd < RightLink[I + 1].DisCrd) then
      begin
        SelectSide(Left);
        SetPosInMs(RightLink[I].ms div 1000000);
        {$IFDEF OUTLOG}
        LogMemo.Lines.Add(Format('Get Image - Side: Right; DisCrd: %d; Pos_Ms: %d', [DisCrd, LeftLink[I].ms div 1000000]));
        {$ENDIF}
        Break;
      end;
//    ShowMessage('Exit Rigth');
  end;


//  ShowMessage('DisToCoordParams');
  TAvk11ViewForm(ViewForm).DatSrc.DisToCoordParams(DisCrd, CrdParams);
//  ShowMessage('DisToCoordParams - OK');
//  ShowMessage('CrdParamsToRealCrd');
  RC:= CrdParamsToRealCrd(CrdParams, TAvk11ViewForm(ViewForm).DatSrc.Header.MoveDir);
//  ShowMessage('CrdParamsToRealCrd - OK');
  if Left then str:= ' ����: �����; ����������: ' + IntToStr(LeftPlayer.GetVideoPosInMs()) + ' '
          else str:= '; ����: ������; ����������: ' + IntToStr(RightPlayer.GetVideoPosInMs()) + ' '; // NOTRanslate
  Self.Caption:= '����: ' + ExtractFileName(TAvk11ViewForm(ViewForm).DatSrc.FileName) + str + RealCrdToStr(RC); // NOTRanslate

end;

procedure TEVideoForm.PauseClick(Sender: TObject);
begin
//  LeftPlayer.Pause();
//  RightPlayer.Pause();
//  ProgBar.OnChange := ProgBarChange;
end;

procedure TEVideoForm.NextFrameClick(Sender: TObject);
begin
  LeftPlayer.NextFrame();
  RightPlayer.NextFrame();
end;

procedure TEVideoForm.FormActivate(Sender: TObject);
begin
  MainForm.DisplayModeToControl({nil} Self);
end;

procedure TEVideoForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  LeftPlayer.Stop();
  RightPlayer.Stop();
  Self.Release;
  EVideoForm:= nil;
end;

procedure TEVideoForm.FormCreate(Sender: TObject);
begin
//  lock := TCriticalSection.Create;
  // set custom path to libvlc
//  PasLibVlcPlayer1.VLC.Path := 'C:\Program Files\VideoLAN\VLC';
//  MrlEdit.Text := '..'+PathDelim+'..'+PathDelim+'_testFiles'+PathDelim+'Maximize.mp4';
//  DoResize();
//  Caption := Caption + ' - ' + {$IFDEF CPUX64}'64'{$ELSE}'32'{$ENDIF} + ' bit';
  Caption := '��������...';
  Loaded_:= False;
  Loaded_Left:= False;
  Loaded_Right:= False;
  save_GetImage:= False;

  {$IFDEF OUTLOG}
  PR.Visible:= True;
  Memo1.Visible:= True;
  {$ENDIF}

end;

procedure TEVideoForm.FormDestroy(Sender: TObject);
begin
//  lock.Free;
end;

procedure TEVideoForm.FormResize(Sender: TObject);
begin
//  DoResize();
end;

procedure TEVideoForm.GetPosLenClick(Sender: TObject);
begin
  MessageDlg(
    'Len = ' + IntToStr(LeftPlayer.GetVideoLenInMs()) + ' ms, '+
    'Pos = ' + IntToStr(LeftPlayer.GetVideoPosInMs()),
    mtInformation, [mbOK], 0);
  MessageDlg(
    'Len = ' + IntToStr(RightPlayer.GetVideoLenInMs()) + ' ms, '+
    'Pos = ' + IntToStr(RightPlayer.GetVideoPosInMs()),
    mtInformation, [mbOK], 0);
end;

procedure TEVideoForm.GetStateClick(Sender: TObject);
var
  stateName: string;
begin
{
  case PasLibVlcPlayer1.GetState() of
    plvPlayer_NothingSpecial: stateName := 'Idle';
    plvPlayer_Opening:        stateName := 'Opening';
    plvPlayer_Buffering:      stateName := 'Buffering';
    plvPlayer_Playing:        stateName := 'Playing';
    plvPlayer_Paused:         stateName := 'Paused';
    plvPlayer_Stopped:        stateName := 'Stopped';
    plvPlayer_Ended:          stateName := 'Ended';
    plvPlayer_Error:          stateName := 'Error';
    else                      stateName := 'Unknown';
  end;
  MessageDlg('State = ' + stateName, mtInformation, [mbOK], 0);
  }
end;

procedure TEVideoForm.ResumeClick(Sender: TObject);
begin
  LeftPlayer.Resume();
  RightPlayer.Resume();

//  ProgBar.OnChange := NIL;
end;


procedure TEVideoForm.SnapShotClick(Sender: TObject);
begin
//  PasLibVlcPlayer1.SnapShot(ChangeFileExt(Application.ExeName, '.png'));
end;

procedure TEVideoForm.Timer1Timer(Sender: TObject);
var
  StateStr1: string;
  StateStr2: string;
  ResStateStr: string;

begin
(*
  case LeftPlayer.GetState() of
    plvPlayer_NothingSpecial: StateStr1:= 'Idle';
    plvPlayer_Opening:        StateStr1:= 'Opening';
    plvPlayer_Buffering:      StateStr1:= 'Buffering';
    plvPlayer_Playing:        StateStr1:= 'Playing';
    plvPlayer_Paused:         StateStr1:= 'Paused';
    plvPlayer_Stopped:        StateStr1:= 'Stopped';
    plvPlayer_Ended:          StateStr1:= 'Ended';
    plvPlayer_Error:          StateStr1:= 'Error';
    else                      StateStr1:= 'Unknown';
  end;

  ResStateStr:= 'Left: ' + StateStr1 + '; Pos = ' + IntToStr(LeftPlayer.GetVideoPosInMs());

  case RightPlayer.GetState() of
    plvPlayer_NothingSpecial: StateStr2:= 'Idle';
    plvPlayer_Opening:        StateStr2:= 'Opening';
    plvPlayer_Buffering:      StateStr2:= 'Buffering';
    plvPlayer_Playing:        StateStr2:= 'Playing';
    plvPlayer_Paused:         StateStr2:= 'Paused';
    plvPlayer_Stopped:        StateStr2:= 'Stopped';
    plvPlayer_Ended:          StateStr2:= 'Ended';
    plvPlayer_Error:          StateStr2:= 'Error';
    else                      StateStr2:= 'Unknown';
  end;

  ResStateStr:= ResStateStr + ' Right: ' + StateStr2 + '; Pos = ' + IntToStr(RightPlayer.GetVideoPosInMs());
  self.Caption:= ResStateStr;
      *)
end;

procedure TEVideoForm.Timer2Timer(Sender: TObject);
begin
  Timer2.Enabled:= False;
  save_GetImage:= False;
  Sleep(300);
  GetImage(save_Left, save_DisCrd);
//  Sleep(300);
//  GetImage(save_Left, save_DisCrd);
end;

procedure TEVideoForm.VideoAdjustBtnClick(Sender: TObject);
//var
//  vaForm : TVideoAdjustForm;
begin
//  vaForm := TVideoAdjustForm.Create(SELF);
//  try
//    vaForm.ShowModal;
//  finally
//    vaForm.Free;
//  end;
end;

procedure TEVideoForm.Button1Click(Sender: TObject);
begin
  if LeftPlayer.CanSeek() then
  begin
    LeftPlayer.SetVideoPosInMs(220334);
//    ProgLabel1.Caption := PasLibVlcPlayer1.GetVideoPosStr('hh:mm:ss.ms');
  end;
  if RightPlayer.CanSeek() then
  begin
    RightPlayer.SetVideoPosInMs(220334);
//    ProgLabel1.Caption := PasLibVlcPlayer1.GetVideoPosStr('hh:mm:ss.ms');
  end;
end;

procedure TEVideoForm.Button2Click(Sender: TObject);
begin
  if LeftPlayer.CanSeek() then
  begin
    LeftPlayer.SetVideoPosInMs(221334);
//    ProgLabel1.Caption := PasLibVlcPlayer1.GetVideoPosStr('hh:mm:ss.ms');
  end;
  if RightPlayer.CanSeek() then
  begin
    RightPlayer.SetVideoPosInMs(221334);
//    ProgLabel1.Caption := PasLibVlcPlayer1.GetVideoPosStr('hh:mm:ss.ms');
  end;

//  if PasLibVlcPlayer1.CanSeek() then
//  begin
//    PasLibVlcPlayer1.SetVideoPosInMs(221334);
//    ProgLabel1.Caption := PasLibVlcPlayer1.GetVideoPosStr('hh:mm:ss.ms');
//  end;
end;

procedure TEVideoForm.Button3Click(Sender: TObject);
begin
  if LeftPlayer.CanSeek() then
  begin
    LeftPlayer.SetVideoPosInMs(222334);
//    ProgLabel1.Caption := PasLibVlcPlayer1.GetVideoPosStr('hh:mm:ss.ms');
  end;
  if RightPlayer.CanSeek() then
  begin
    RightPlayer.SetVideoPosInMs(222334);
//    ProgLabel1.Caption := PasLibVlcPlayer1.GetVideoPosStr('hh:mm:ss.ms');
  end;

//  if PasLibVlcPlayer1.CanSeek() then
//  begin
//    PasLibVlcPlayer1.SetVideoPosInMs(222334);
//    ProgLabel1.Caption := PasLibVlcPlayer1.GetVideoPosStr('hh:mm:ss.ms');
//  end;
end;

//procedure TMainForm.DoResize();
//begin
//  ProgPanel.Top := LB.Top - 8 - ProgPanel.Height;
//  ProgPanel.Width := LB.Width;

//  PasLibVlcPlayer1.Width := LB.Width;
//  PasLibVlcPlayer1.Height := Lb.Top - MrlEdit.Top - MrlEdit.Height - 16 - ProgPanel.Height - 8;
//end;

initialization;

  EVideoForm:= nil;

end.
