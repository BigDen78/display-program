object BaseCfg2Form: TBaseCfg2Form
  Left = 301
  Top = 191
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1089#1090#1086#1083#1073#1094#1086#1074
  ClientHeight = 383
  ClientWidth = 550
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = CreateList
  PixelsPerInch = 96
  TextHeight = 13
  object MoveUpButton: TButton
    Left = 214
    Top = 29
    Width = 122
    Height = 25
    Caption = #1042#1074#1077#1088#1093
    TabOrder = 0
    OnClick = MoveUpButtonClick
  end
  object MoveDownButton: TButton
    Left = 214
    Top = 61
    Width = 122
    Height = 25
    Caption = #1042#1085#1080#1079
    TabOrder = 1
    OnClick = MoveDownButtonClick
  end
  object Button3: TButton
    Left = 226
    Top = 328
    Width = 98
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 214
    Top = 93
    Width = 122
    Height = 25
    Caption = '<-----'
    TabOrder = 3
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 214
    Top = 125
    Width = 122
    Height = 25
    Caption = '----->'
    TabOrder = 4
    OnClick = Button5Click
  end
  object Label2_: TPageControl
    Left = 6
    Top = 6
    Width = 203
    Height = 347
    ActivePage = Label2
    TabOrder = 5
    object Label2: TTabSheet
      Caption = 'Label2'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ListBox1: TListBox
        Left = 0
        Top = 0
        Width = 193
        Height = 317
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        Ctl3D = True
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        OnClick = ListBox1Click
        OnDblClick = ListBox1DblClick
      end
      object Panel1: TPanel
        Left = 0
        Top = 317
        Width = 195
        Height = 2
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
      end
      object Panel2: TPanel
        Left = 193
        Top = 0
        Width = 2
        Height = 317
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
      end
    end
  end
  object PageControl1: TPageControl
    Left = 341
    Top = 6
    Width = 203
    Height = 347
    ActivePage = Label1
    TabOrder = 6
    object Label1: TTabSheet
      Caption = 'Label2'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ListBox2: TCheckListBox
        Left = 0
        Top = 0
        Width = 193
        Height = 317
        OnClickCheck = ListBox2ClickCheck
        Align = alClient
        Ctl3D = True
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        OnClick = ListBox2Click
        OnDblClick = ListBox2DblClick
      end
      object Panel3: TPanel
        Left = 0
        Top = 317
        Width = 195
        Height = 2
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
      end
      object Panel4: TPanel
        Left = 193
        Top = 0
        Width = 2
        Height = 317
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
      end
    end
  end
  object CheckBox1: TCheckBox
    Left = 8
    Top = 360
    Width = 153
    Height = 17
    Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072' '#1073#1077#1079' '#1082#1072#1090#1072#1083#1086#1075#1072
    TabOrder = 7
    OnClick = CheckBox1Click
  end
end
