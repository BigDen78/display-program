object Avk11ViewForm: TAvk11ViewForm
  Left = 327
  Top = 221
  Caption = 'Avk11ViewForm'
  ClientHeight = 538
  ClientWidth = 761
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnDeactivate = FormDeactivate
  OnKeyDown = FormKeyDown
  OnKeyUp = FormKeyUp
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  OnResize = FormResize
  OnShortCut = FormShortCut
  PixelsPerInch = 96
  TextHeight = 13
  object ScrollBar1: TScrollBar
    Left = 0
    Top = 522
    Width = 761
    Height = 16
    Align = alBottom
    PageSize = 0
    TabOrder = 0
  end
  object Button3: TButton
    Left = 12
    Top = 17
    Width = 9
    Height = 9
    Caption = 'Button3'
    TabOrder = 1
  end
  object Button4: TButton
    Left = 12
    Top = 9
    Width = 9
    Height = 9
    Caption = 'Button4'
    TabOrder = 2
  end
  object FilusX111W_HandScan: TPanel
    Left = 176
    Top = 96
    Width = 497
    Height = 273
    BevelOuter = bvNone
    TabOrder = 3
    Visible = False
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 185
      Height = 273
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object ListBox1: TListBox
        Left = 0
        Top = 0
        Width = 183
        Height = 273
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 18
        ParentFont = False
        TabOrder = 0
        OnClick = ListBox1Click
      end
      object Panel2: TPanel
        Left = 183
        Top = 0
        Width = 2
        Height = 273
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
      end
    end
    object Panel3: TPanel
      Left = 185
      Top = 0
      Width = 312
      Height = 273
      Align = alClient
      BevelOuter = bvNone
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 1
      object FileInfoMemo: TMemo
        Left = 0
        Top = 0
        Width = 312
        Height = 273
        Align = alClient
        Ctl3D = True
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
        WordWrap = False
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 312
        Height = 273
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Image1: TImage
          Left = 129
          Top = 0
          Width = 183
          Height = 273
          Align = alClient
          AutoSize = True
          Center = True
          Proportional = True
          Stretch = True
          ExplicitLeft = 104
          ExplicitTop = 88
          ExplicitWidth = 105
          ExplicitHeight = 105
        end
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 129
          Height = 273
          Align = alLeft
          BorderStyle = bsNone
          Enabled = False
          TabOrder = 0
        end
      end
    end
  end
  object AnalyzeProgressBarPanel: TPanel
    Left = 0
    Top = 0
    Width = 761
    Height = 18
    Align = alTop
    Alignment = taLeftJustify
    TabOrder = 4
    Visible = False
    object ProgressBar: TProgressBar
      Left = 201
      Top = 4
      Width = 289
      Height = 11
      TabOrder = 0
    end
  end
  object Panel5: TPanel
    Left = 288
    Top = 18
    Width = 473
    Height = 504
    Align = alRight
    TabOrder = 5
    Visible = False
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 471
      Height = 41
      Align = alTop
      TabOrder = 0
      object Button1: TButton
        Left = 8
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 0
        OnClick = Button1Click
      end
    end
    object Memo2: TMemo
      Left = 1
      Top = 42
      Width = 471
      Height = 461
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 1
    end
  end
  object Timer1: TTimer
    Tag = -1
    Enabled = False
    Interval = 10
    OnTimer = Timer1Timer
    Left = 88
    Top = 112
  end
end
