unit OperatorLog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Avk11ViewUnit, ComCtrls;

type
  TOperatorLogForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    ListView1: TListView;
    procedure Button1Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListView1Click(Sender: TObject);
  private
    { Private declarations }
  public
    AVF: TAvk11ViewForm;
  end;

var
  OperatorLogForm: TOperatorLogForm;

implementation

{$R *.dfm}

procedure TOperatorLogForm.Button1Click(Sender: TObject);
begin
  Self.Visible:= False;
  AVF:= nil;
end;

procedure TOperatorLogForm.FormCreate(Sender: TObject);
begin
  AVF:= nil;
end;

procedure TOperatorLogForm.ListBox1Click(Sender: TObject);
begin
//  if ListBox1.ItemIndex <> - 1 then
//  begin
  //  AVF.ScrollBar1.Position:= (AVF.DatSrc.AKStateData__[Integer(ListBox1.Items.Objects[ListBox1.ItemIndex])].X + AVF.DatSrc.AKStateData__[Integer(ListBox1.Items.Objects[ListBox1.ItemIndex])].Y) div 2;
//  end;
end;

procedure TOperatorLogForm.ListView1Click(Sender: TObject);
begin
  if ListView1.ItemIndex <> - 1 then
  begin
    AVF.ScrollBar1.Position:= Integer(ListView1.Selected.Data);
  end;
end;

end.
