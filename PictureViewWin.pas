unit PictureViewWin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, ComCtrls, StdCtrls, Buttons, PNGImage;

type
  TPictureView = class(TForm)
    Image1: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);

  private
    FOnCloseMediaForm: TNotifyEvent;

  public
    procedure Open_and_Play(FN: string; OnCloseMediaForm: TNotifyEvent);
  end;

var
  PictureView: TPictureView;

implementation

{$R *.dfm}

procedure TPictureView.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (self.Tag = 0) and Assigned(FOnCloseMediaForm) then FOnCloseMediaForm(self);
  Self.Release;
end;

procedure TPictureView.FormCreate(Sender: TObject);
begin
  FOnCloseMediaForm:= nil;
end;

procedure TPictureView.Open_and_Play(FN: string; OnCloseMediaForm: TNotifyEvent);
begin
  FOnCloseMediaForm:= OnCloseMediaForm;
  Image1.Picture.LoadFromFile(FN);
end;

end.
