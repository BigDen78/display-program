object FlashCardForm: TFlashCardForm
  Left = 267
  Top = 257
  BorderStyle = bsToolWindow
  Caption = #1056#1072#1073#1086#1090#1072' '#1089' '#1072#1076#1072#1087#1090#1077#1088#1086#1084' '#1082#1072#1088#1090#1099' '#1087#1072#1084#1103#1090#1080
  ClientHeight = 471
  ClientWidth = 740
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShortCut = FormShortCut
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl2: TPageControl
    Left = 2
    Top = 0
    Width = 738
    Height = 312
    ActivePage = ReadFiles
    Align = alClient
    Style = tsButtons
    TabOrder = 0
    object WaitSheet1: TTabSheet
      Caption = 'WaitSheet1'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel10: TPanel
        Left = 152
        Top = 88
        Width = 433
        Height = 73
        TabOrder = 0
        object Label8: TLabel
          Left = 7
          Top = 5
          Width = 78
          Height = 22
          Caption = #1046#1076#1080#1090#1077'....'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = 22
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object ProgressBar1: TProgressBar
          Left = 8
          Top = 33
          Width = 417
          Height = 30
          Constraints.MaxHeight = 30
          TabOrder = 0
        end
      end
    end
    object WaitSheet2: TTabSheet
      Caption = 'WaitSheet2'
      ImageIndex = 2
      object Panel13: TPanel
        Left = 80
        Top = 48
        Width = 353
        Height = 138
        TabOrder = 0
        object Label6: TLabel
          Left = 8
          Top = 11
          Width = 78
          Height = 22
          Caption = #1046#1076#1080#1090#1077'....'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = 22
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 6
          Top = 107
          Width = 316
          Height = 22
          AutoSize = False
          Caption = #1055#1088#1080#1085#1103#1090#1086': 0 '#1073#1072#1081#1090
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = 22
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Animate2: TAnimate
          Left = 40
          Top = 39
          Width = 272
          Height = 60
          Active = True
          CommonAVI = aviCopyFile
          StopFrame = 20
        end
      end
    end
    object WaitSheet3: TTabSheet
      Caption = 'WaitSheet3'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 730
        Height = 281
        Align = alClient
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 22
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label11: TLabel
          Left = 105
          Top = 0
          Width = 69
          Height = 22
          Align = alClient
          Caption = 'Label11'
          Layout = tlCenter
        end
        object Panel5: TPanel
          Left = 0
          Top = 247
          Width = 730
          Height = 34
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object Panel11: TPanel
            Left = 641
            Top = 0
            Width = 89
            Height = 34
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object Button1: TButton
              Left = 6
              Top = 5
              Width = 75
              Height = 25
              Caption = #1047#1072#1082#1088#1099#1090#1100
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ModalResult = 1
              ParentFont = False
              TabOrder = 0
              OnClick = Button3Click
            end
          end
        end
        object Panel30: TPanel
          Left = 0
          Top = 0
          Width = 105
          Height = 247
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
        end
      end
    end
    object WaitSheet4: TTabSheet
      Caption = 'WaitSheet4'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel24: TPanel
        Left = 0
        Top = 0
        Width = 730
        Height = 281
        Align = alClient
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 22
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label12: TLabel
          Left = 17
          Top = 0
          Width = 69
          Height = 22
          Align = alClient
          Caption = 'Label11'
          Layout = tlCenter
        end
        object Panel28: TPanel
          Left = 0
          Top = 247
          Width = 730
          Height = 34
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object Panel29: TPanel
            Left = 641
            Top = 0
            Width = 89
            Height = 34
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object Button2: TButton
              Left = 6
              Top = 5
              Width = 75
              Height = 25
              Caption = #1047#1072#1082#1088#1099#1090#1100
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ModalResult = 1
              ParentFont = False
              TabOrder = 0
              OnClick = Button3Click
            end
          end
        end
        object Panel31: TPanel
          Left = 0
          Top = 0
          Width = 17
          Height = 247
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
        end
      end
    end
    object ReadFiles: TTabSheet
      Caption = 'ReadFiles'
      ImageIndex = 5
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 730
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object TBDock1: TTBDock
          Left = 0
          Top = 0
          Width = 730
          Height = 45
          AllowDrag = False
          Background = MainForm.TBBackground1
          object MainToolbar: TTBToolbar
            Left = 0
            Top = 0
            Caption = 'MainToolbar'
            DockPos = 61
            DockRow = 1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Images = ImageList2
            Options = [tboImageAboveCaption]
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            Stretch = True
            TabOrder = 0
            object Button7: TTBItem
              Caption = #1054#1073#1085#1086#1074#1080#1090#1100
              ImageIndex = 4
              Images = ImageList2
              OnClick = Button7Click
            end
            object TBSeparatorItem1: TTBSeparatorItem
            end
            object Button6: TTBItem
              Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
              ImageIndex = 3
              Images = ImageList2
              OnClick = Button2Click
            end
            object TBItem1: TTBItem
              Enabled = False
            end
            object TBControlItem1: TTBControlItem
              Control = CheckBox1
            end
            object TBSeparatorItem3: TTBSeparatorItem
            end
            object Button10: TTBItem
              AutoCheck = True
              Caption = #1059#1076#1072#1083#1080#1090#1100
              GroupIndex = 1
              ImageIndex = 7
              Images = ImageList2
              OnClick = Button10Click
            end
            object TBSeparatorItem2: TTBSeparatorItem
            end
            object TBItem2: TTBItem
              Caption = 'Select All'
              ImageIndex = 6
              Images = ImageList2
              OnClick = TBItem2Click
            end
            object TBControlItem2: TTBControlItem
              Control = Panel26
            end
            object CheckBox1: TCheckBox
              Left = 144
              Top = 12
              Width = 150
              Height = 17
              Caption = #1047#1072#1087#1088#1072#1096#1080#1074#1072#1090#1100' '#1080#1084#1103' '#1092#1072#1081#1083#1072
              Checked = True
              State = cbChecked
              TabOrder = 0
              OnClick = CheckBox1Click
            end
            object Panel26: TPanel
              Left = 417
              Top = 0
              Width = 280
              Height = 41
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label13: TLabel
                Left = 204
                Top = 0
                Width = 76
                Height = 41
                Align = alRight
                Alignment = taCenter
                AutoSize = False
                Caption = 'Log '#1092#1072#1081#1083' '#1074#1082#1083#1102#1095#1086#1085
                Layout = tlCenter
                Visible = False
                WordWrap = True
              end
            end
          end
        end
      end
      object ListView1: TListView
        Left = 0
        Top = 48
        Width = 730
        Height = 199
        Align = alClient
        Checkboxes = True
        Columns = <
          item
            Width = 40
          end
          item
            Caption = #8470
            Width = 30
          end
          item
            Caption = #1044#1072#1090#1072
            Width = 65
          end
          item
            Caption = #1050#1084
          end
          item
            Caption = #1055#1050
            Width = 35
          end
          item
            Caption = #1052#1077#1090#1088
            Width = 45
          end
          item
            Caption = #1055#1091#1090#1100
            Width = 40
          end
          item
            Caption = #1054#1087#1077#1088#1072#1090#1086#1088
            Width = 100
          end
          item
            Caption = #1055#1077#1088#1077#1075#1086#1085
            Width = 100
          end
          item
            Caption = #1046'.'#1044'.'
            Width = 100
          end
          item
            Width = 95
          end>
        ColumnClick = False
        ReadOnly = True
        RowSelect = True
        TabOrder = 1
        ViewStyle = vsReport
      end
      object Panel32: TPanel
        Left = 0
        Top = 247
        Width = 730
        Height = 34
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object Panel34: TPanel
          Left = 641
          Top = 0
          Width = 89
          Height = 34
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object Button4: TButton
            Left = 11
            Top = 8
            Width = 75
            Height = 25
            Caption = #1047#1072#1082#1088#1099#1090#1100
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ModalResult = 1
            ParentFont = False
            TabOrder = 0
            OnClick = Button3Click
          end
        end
      end
    end
    object SetParam: TTabSheet
      Caption = 'SetParam'
      ImageIndex = 6
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel4: TPanel
        Left = 0
        Top = 247
        Width = 730
        Height = 34
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object Panel9: TPanel
          Left = 641
          Top = 0
          Width = 89
          Height = 34
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object Button3: TButton
            Left = 11
            Top = 8
            Width = 75
            Height = 25
            Caption = #1047#1072#1082#1088#1099#1090#1100
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ModalResult = 1
            ParentFont = False
            TabOrder = 0
            OnClick = Button3Click
          end
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 41
        Width = 730
        Height = 206
        ActivePage = stNotes
        Align = alClient
        TabOrder = 1
        ExplicitTop = 44
        ExplicitHeight = 203
        object tsRegData: TTabSheet
          Caption = 'tsRegData'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PageControl4: TPageControl
            Left = 0
            Top = 39
            Width = 722
            Height = 139
            ActivePage = TabSheet10
            Align = alClient
            TabOrder = 0
            ExplicitTop = 42
            ExplicitHeight = 133
            object TabSheet1: TTabSheet
              Caption = 'TabSheet1'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel18: TPanel
                Left = 0
                Top = 0
                Width = 714
                Height = 20
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label3: TLabel
                  Left = 2
                  Top = 4
                  Width = 63
                  Height = 13
                  Caption = #1057#1087#1080#1089#1086#1082' '#1041#1059#1048
                end
                object Label4: TLabel
                  Left = 303
                  Top = 4
                  Width = 30
                  Height = 13
                  Caption = #1040#1088#1093#1080#1074
                end
              end
              object BtnMove2: TBitBtn
                Left = 204
                Top = 176
                Width = 93
                Height = 31
                Caption = #1055#1077#1088#1077#1085#1077#1089#1090#1080
                DoubleBuffered = True
                ParentDoubleBuffered = False
                TabOrder = 1
                OnClick = BtnMove2Click
              end
              object Panel20: TPanel
                Left = 0
                Top = 20
                Width = 200
                Height = 85
                Align = alLeft
                BevelOuter = bvNone
                BorderWidth = 2
                TabOrder = 2
                object ListBox21: TListBox
                  Tag = 1
                  Left = 2
                  Top = 2
                  Width = 196
                  Height = 81
                  Align = alClient
                  BevelInner = bvNone
                  BevelOuter = bvNone
                  Ctl3D = False
                  ItemHeight = 13
                  MultiSelect = True
                  ParentCtl3D = False
                  TabOrder = 0
                  OnDblClick = ListBox11DblClick
                  OnMouseUp = ListBox21MouseUp
                end
              end
              object BtnAdd2: TBitBtn
                Left = 204
                Top = 71
                Width = 93
                Height = 31
                Caption = #1044#1086#1073#1072#1074#1080#1090#1100
                DoubleBuffered = True
                ParentDoubleBuffered = False
                TabOrder = 3
                OnClick = BtnAdd2Click
              end
              object BtnDel2: TBitBtn
                Left = 204
                Top = 106
                Width = 93
                Height = 31
                Caption = #1059#1076#1072#1083#1080#1090#1100
                DoubleBuffered = True
                ParentDoubleBuffered = False
                TabOrder = 4
                OnClick = BtnDel2Click
              end
              object BtnCopy2: TBitBtn
                Left = 204
                Top = 141
                Width = 93
                Height = 31
                Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
                DoubleBuffered = True
                ParentDoubleBuffered = False
                TabOrder = 5
                OnClick = BtnCopy2Click
              end
              object Panel21: TPanel
                Left = 514
                Top = 20
                Width = 200
                Height = 91
                Align = alRight
                BevelOuter = bvNone
                BorderWidth = 2
                Color = clBlack
                TabOrder = 6
                ExplicitHeight = 85
                object ListBox22: TListBox
                  Tag = 1
                  Left = 2
                  Top = 2
                  Width = 196
                  Height = 81
                  Align = alClient
                  Ctl3D = False
                  ItemHeight = 13
                  MultiSelect = True
                  ParentCtl3D = False
                  TabOrder = 0
                  OnDblClick = ListBox11DblClick
                  OnMouseUp = ListBox21MouseUp
                end
              end
            end
            object TabSheet9: TTabSheet
              Caption = 'TabSheet9'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel3: TPanel
                Left = 0
                Top = 0
                Width = 714
                Height = 105
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel17: TPanel
                  Left = 0
                  Top = 0
                  Width = 714
                  Height = 20
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label1: TLabel
                    Left = 2
                    Top = 4
                    Width = 63
                    Height = 13
                    Caption = #1057#1087#1080#1089#1086#1082' '#1041#1059#1048
                  end
                  object Label2: TLabel
                    Left = 303
                    Top = 4
                    Width = 30
                    Height = 13
                    Caption = #1040#1088#1093#1080#1074
                  end
                end
                object Panel6: TPanel
                  Left = 0
                  Top = 20
                  Width = 200
                  Height = 85
                  Align = alLeft
                  BevelOuter = bvNone
                  BorderWidth = 2
                  TabOrder = 1
                  object ListBox11: TListBox
                    Tag = 1
                    Left = 2
                    Top = 2
                    Width = 196
                    Height = 81
                    Align = alClient
                    BevelInner = bvNone
                    BevelOuter = bvNone
                    Ctl3D = False
                    ItemHeight = 13
                    MultiSelect = True
                    ParentCtl3D = False
                    TabOrder = 0
                    OnDblClick = ListBox11DblClick
                    OnMouseUp = ListBox11MouseUp
                  end
                end
                object Panel16: TPanel
                  Left = 514
                  Top = 20
                  Width = 200
                  Height = 91
                  Align = alRight
                  BevelOuter = bvNone
                  BorderWidth = 2
                  Color = clBlack
                  TabOrder = 2
                  ExplicitHeight = 85
                  object ListBox12: TListBox
                    Tag = 1
                    Left = 2
                    Top = 2
                    Width = 196
                    Height = 81
                    Align = alClient
                    Ctl3D = False
                    ItemHeight = 13
                    MultiSelect = True
                    ParentCtl3D = False
                    TabOrder = 0
                    OnDblClick = ListBox11DblClick
                    OnMouseUp = ListBox11MouseUp
                  end
                end
                object BtnAdd1: TBitBtn
                  Left = 204
                  Top = 71
                  Width = 93
                  Height = 31
                  Caption = #1044#1086#1073#1072#1074#1080#1090#1100
                  DoubleBuffered = True
                  ParentDoubleBuffered = False
                  TabOrder = 3
                  OnClick = BtnAdd1Click
                end
                object BtnDel1: TBitBtn
                  Left = 204
                  Top = 106
                  Width = 93
                  Height = 31
                  Caption = #1059#1076#1072#1083#1080#1090#1100
                  DoubleBuffered = True
                  ParentDoubleBuffered = False
                  TabOrder = 4
                  OnClick = BtnDel1Click
                end
                object BtnMove1: TBitBtn
                  Left = 204
                  Top = 176
                  Width = 93
                  Height = 31
                  Caption = #1055#1077#1088#1077#1085#1077#1089#1090#1080
                  DoubleBuffered = True
                  ParentDoubleBuffered = False
                  TabOrder = 5
                  OnClick = BtnMove1Click
                end
                object BtnCopy1: TBitBtn
                  Left = 204
                  Top = 141
                  Width = 93
                  Height = 31
                  Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
                  DoubleBuffered = True
                  ParentDoubleBuffered = False
                  TabOrder = 6
                  OnClick = BtnCopy1Click
                end
              end
            end
            object TabSheet10: TTabSheet
              Caption = 'TabSheet10'
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object BtnAdd3: TBitBtn
                Left = 204
                Top = 71
                Width = 93
                Height = 31
                Caption = #1044#1086#1073#1072#1074#1080#1090#1100
                DoubleBuffered = True
                ParentDoubleBuffered = False
                TabOrder = 0
                OnClick = BtnAdd3Click
              end
              object BtnDel3: TBitBtn
                Left = 204
                Top = 106
                Width = 93
                Height = 31
                Caption = #1059#1076#1072#1083#1080#1090#1100
                DoubleBuffered = True
                ParentDoubleBuffered = False
                TabOrder = 1
                OnClick = BtnDel3Click
              end
              object BtnMove3: TBitBtn
                Left = 204
                Top = 176
                Width = 93
                Height = 31
                Caption = #1055#1077#1088#1077#1085#1077#1089#1090#1080
                DoubleBuffered = True
                ParentDoubleBuffered = False
                TabOrder = 2
                OnClick = BtnMove3Click
              end
              object BtnCopy3: TBitBtn
                Left = 204
                Top = 141
                Width = 93
                Height = 31
                Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
                DoubleBuffered = True
                ParentDoubleBuffered = False
                TabOrder = 3
                OnClick = BtnCopy3Click
              end
              object Panel19: TPanel
                Left = 0
                Top = 0
                Width = 714
                Height = 20
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 4
                object Label5: TLabel
                  Left = 2
                  Top = 4
                  Width = 63
                  Height = 13
                  Caption = #1057#1087#1080#1089#1086#1082' '#1041#1059#1048
                end
                object Label7: TLabel
                  Left = 303
                  Top = 4
                  Width = 30
                  Height = 13
                  Caption = #1040#1088#1093#1080#1074
                end
              end
              object Panel22: TPanel
                Left = 0
                Top = 20
                Width = 200
                Height = 85
                Align = alLeft
                BevelOuter = bvNone
                BorderWidth = 2
                TabOrder = 5
                object ListBox31: TListBox
                  Tag = 1
                  Left = 2
                  Top = 2
                  Width = 196
                  Height = 81
                  Align = alClient
                  BevelInner = bvNone
                  BevelOuter = bvNone
                  Ctl3D = False
                  ItemHeight = 13
                  MultiSelect = True
                  ParentCtl3D = False
                  TabOrder = 0
                  OnDblClick = ListBox11DblClick
                  OnMouseUp = ListBox31MouseUp
                end
              end
              object Panel23: TPanel
                Left = 514
                Top = 20
                Width = 200
                Height = 91
                Align = alRight
                BevelOuter = bvNone
                BorderWidth = 2
                Color = clBlack
                TabOrder = 6
                ExplicitHeight = 85
                object ListBox32: TListBox
                  Tag = 1
                  Left = 2
                  Top = 2
                  Width = 196
                  Height = 81
                  Align = alClient
                  Ctl3D = False
                  ItemHeight = 13
                  MultiSelect = True
                  ParentCtl3D = False
                  TabOrder = 0
                  OnDblClick = ListBox11DblClick
                  OnMouseUp = ListBox31MouseUp
                end
              end
            end
          end
          object TBDock5: TTBDock
            Left = 0
            Top = 0
            Width = 722
            Height = 39
            AllowDrag = False
            Background = MainForm.TBBackground1
            object TBToolbar4: TTBToolbar
              Left = 0
              Top = 0
              Caption = 'MainToolbar'
              DockPos = 61
              DockRow = 1
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Images = ImageList2
              Options = [tboImageAboveCaption]
              ParentFont = False
              ParentShowHint = False
              ShowHint = False
              Stretch = True
              TabOrder = 0
              object TBControlItem4: TTBControlItem
                Control = Panel35
              end
              object TBSeparatorItem6: TTBSeparatorItem
              end
              object TBItem7: TTBItem
                Caption = #1048#1084#1087#1086#1088#1090' '#1072#1088#1093#1080#1074#1072
                ImageIndex = 6
                Images = ImageList1
                OnClick = TBItem4Click
              end
              object TBItem8: TTBItem
                Caption = #1069#1082#1089#1087#1086#1088#1090' '#1072#1088#1093#1080#1074#1072
                ImageIndex = 10
                Images = ImageList2
                OnClick = TBItem8Click
              end
              object Panel35: TPanel
                Left = 0
                Top = 0
                Width = 276
                Height = 35
                BevelOuter = bvNone
                TabOrder = 0
                object Label16: TLabel
                  Left = 3
                  Top = 3
                  Width = 86
                  Height = 29
                  AutoSize = False
                  Caption = 'Label16'
                  Layout = tlCenter
                  WordWrap = True
                end
                object Edit1: TEdit
                  Left = 92
                  Top = 6
                  Width = 181
                  Height = 21
                  TabOrder = 0
                end
              end
            end
          end
        end
        object stNotes: TTabSheet
          Caption = 'stNotes'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object RichEdit2: TRichEdit
            Left = 0
            Top = 67
            Width = 720
            Height = 106
            Align = alClient
            BevelInner = bvNone
            BevelOuter = bvNone
            Ctl3D = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 0
          end
          object Panel44: TPanel
            Left = 0
            Top = 0
            Width = 722
            Height = 67
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Label9: TLabel
              Left = 24
              Top = 48
              Width = 51
              Height = 13
              Caption = #1057#1090#1088#1072#1085#1080#1094#1072':'
              Color = clBtnFace
              ParentColor = False
            end
            object Label19: TLabel
              Left = 129
              Top = 48
              Width = 46
              Height = 13
              Caption = #1050#1086#1083#1086#1085#1082#1072':'
              Color = clBtnFace
              ParentColor = False
            end
            object Label20: TLabel
              Left = 205
              Top = 48
              Width = 39
              Height = 13
              Caption = #1057#1090#1088#1086#1082#1072':'
              Color = clBtnFace
              ParentColor = False
            end
            object Error1: TImage
              Left = 4
              Top = 45
              Width = 16
              Height = 16
              Picture.Data = {
                07544269746D617076020000424D760200000000000076000000280000002000
                0000200000000100040000000000000200000000000000000000100000000000
                0000000000000000800000800000008080008000000080008000808000008080
                8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
                FF00DDDDD7777777777777777777777777DDDDDD777777777777777777777777
                777DDD300000000000000000000000077777D3BBBBBBBBBBBBBBBBBBBBBBBB80
                77773BBBBBBBBBBBBBBBBBBBBBBBBBB807773BBBBBBBBBBBBBBBBBBBBBBBBBBB
                07773BBBBBBBBBBBB8008BBBBBBBBBBB077D3BBBBBBBBBBBB0000BBBBBBBBBB8
                077DD3BBBBBBBBBBB0000BBBBBBBBBB077DDD3BBBBBBBBBBB8008BBBBBBBBB80
                77DDDD3BBBBBBBBBBBBBBBBBBBBBBB077DDDDD3BBBBBBBBBBB0BBBBBBBBBB807
                7DDDDDD3BBBBBBBBB808BBBBBBBBB077DDDDDDD3BBBBBBBBB303BBBBBBBB8077
                DDDDDDDD3BBBBBBBB000BBBBBBBB077DDDDDDDDD3BBBBBBB80008BBBBBB8077D
                DDDDDDDDD3BBBBBB30003BBBBBB077DDDDDDDDDDD3BBBBBB00000BBBBB8077DD
                DDDDDDDDDD3BBBBB00000BBBBB077DDDDDDDDDDDDD3BBBBB00000BBBB8077DDD
                DDDDDDDDDDD3BBBB00000BBBB077DDDDDDDDDDDDDDD3BBBB00000BBB8077DDDD
                DDDDDDDDDDDD3BBB80008BBB077DDDDDDDDDDDDDDDDD3BBBBBBBBBB8077DDDDD
                DDDDDDDDDDDDD3BBBBBBBBB077DDDDDDDDDDDDDDDDDDD3BBBBBBBB8077DDDDDD
                DDDDDDDDDDDDDD3BBBBBBB077DDDDDDDDDDDDDDDDDDDDD3BBBBBB8077DDDDDDD
                DDDDDDDDDDDDDDD3BBBBB077DDDDDDDDDDDDDDDDDDDDDDD3BBBB807DDDDDDDDD
                DDDDDDDDDDDDDDDD3BB80DDDDDDDDDDDDDDDDDDDDDDDDDDDD333DDDDDDDDDDDD
                DDDD}
              Stretch = True
              Transparent = True
              Visible = False
            end
            object Error2: TImage
              Left = 109
              Top = 46
              Width = 16
              Height = 16
              Picture.Data = {
                07544269746D617076020000424D760200000000000076000000280000002000
                0000200000000100040000000000000200000000000000000000100000000000
                0000000000000000800000800000008080008000000080008000808000008080
                8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
                FF00DDDDD7777777777777777777777777DDDDDD777777777777777777777777
                777DDD300000000000000000000000077777D3BBBBBBBBBBBBBBBBBBBBBBBB80
                77773BBBBBBBBBBBBBBBBBBBBBBBBBB807773BBBBBBBBBBBBBBBBBBBBBBBBBBB
                07773BBBBBBBBBBBB8008BBBBBBBBBBB077D3BBBBBBBBBBBB0000BBBBBBBBBB8
                077DD3BBBBBBBBBBB0000BBBBBBBBBB077DDD3BBBBBBBBBBB8008BBBBBBBBB80
                77DDDD3BBBBBBBBBBBBBBBBBBBBBBB077DDDDD3BBBBBBBBBBB0BBBBBBBBBB807
                7DDDDDD3BBBBBBBBB808BBBBBBBBB077DDDDDDD3BBBBBBBBB303BBBBBBBB8077
                DDDDDDDD3BBBBBBBB000BBBBBBBB077DDDDDDDDD3BBBBBBB80008BBBBBB8077D
                DDDDDDDDD3BBBBBB30003BBBBBB077DDDDDDDDDDD3BBBBBB00000BBBBB8077DD
                DDDDDDDDDD3BBBBB00000BBBBB077DDDDDDDDDDDDD3BBBBB00000BBBB8077DDD
                DDDDDDDDDDD3BBBB00000BBBB077DDDDDDDDDDDDDDD3BBBB00000BBB8077DDDD
                DDDDDDDDDDDD3BBB80008BBB077DDDDDDDDDDDDDDDDD3BBBBBBBBBB8077DDDDD
                DDDDDDDDDDDDD3BBBBBBBBB077DDDDDDDDDDDDDDDDDDD3BBBBBBBB8077DDDDDD
                DDDDDDDDDDDDDD3BBBBBBB077DDDDDDDDDDDDDDDDDDDDD3BBBBBB8077DDDDDDD
                DDDDDDDDDDDDDDD3BBBBB077DDDDDDDDDDDDDDDDDDDDDDD3BBBB807DDDDDDDDD
                DDDDDDDDDDDDDDDD3BB80DDDDDDDDDDDDDDDDDDDDDDDDDDDD333DDDDDDDDDDDD
                DDDD}
              Stretch = True
              Transparent = True
              Visible = False
            end
            object Shape7: TShape
              Left = 0
              Top = 42
              Width = 722
              Height = 1
              Align = alTop
            end
            object TBDock2: TTBDock
              Left = 0
              Top = 0
              Width = 722
              Height = 39
              AllowDrag = False
              Background = MainForm.TBBackground1
              object TBToolbar1: TTBToolbar
                Left = 0
                Top = 0
                Caption = 'MainToolbar'
                DockPos = 61
                DockRow = 1
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Images = ImageList2
                Options = [tboImageAboveCaption]
                ParentFont = False
                ParentShowHint = False
                ShowHint = False
                Stretch = True
                TabOrder = 0
                object ClearNoteBook: TTBItem
                  Caption = #1054#1095#1080#1089#1090#1080#1090#1100
                  ImageIndex = 7
                  Images = ImageList2
                  OnClick = ClearNoteBookClick
                end
                object ImportNoteBook: TTBItem
                  Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
                  ImageIndex = 11
                  Images = ImageList2
                  OnClick = ImportNoteBookClick
                end
                object ExportNoteBook: TTBItem
                  Caption = 'fgdfgdfgdf'
                  ImageIndex = 10
                  Images = ImageList2
                  OnClick = ImportNoteBookClick
                end
              end
            end
          end
          object Panel7: TPanel
            Left = 720
            Top = 67
            Width = 2
            Height = 109
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 2
            ExplicitHeight = 106
          end
          object Panel25: TPanel
            Left = 0
            Top = 176
            Width = 722
            Height = 2
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 3
            ExplicitTop = 173
          end
        end
        object tsSnapShots: TTabSheet
          Caption = 'tsSnapShots'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel33: TPanel
            Left = 0
            Top = 0
            Width = 722
            Height = 46
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Shape6: TShape
              Left = 0
              Top = 42
              Width = 722
              Height = 1
              Align = alTop
            end
            object TBDock4: TTBDock
              Left = 0
              Top = 0
              Width = 722
              Height = 39
              AllowDrag = False
              Background = MainForm.TBBackground1
              object TBToolbar3: TTBToolbar
                Left = 0
                Top = 0
                Caption = 'MainToolbar'
                DockPos = 61
                DockRow = 1
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Images = ImageList2
                Options = [tboImageAboveCaption]
                ParentFont = False
                ParentShowHint = False
                ShowHint = False
                Stretch = True
                TabOrder = 0
                object TBControlItem3: TTBControlItem
                  Control = ListBox1
                end
                object TBSeparatorItem4: TTBSeparatorItem
                end
                object SavePic: TTBItem
                  Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
                  ImageIndex = 10
                  Images = ImageList2
                  OnClick = SavePicClick
                end
                object ListBox1: TComboBox
                  Left = 0
                  Top = 7
                  Width = 145
                  Height = 21
                  Style = csDropDownList
                  TabOrder = 0
                  OnChange = ListBox1Click
                end
              end
            end
          end
          object SHPanel: TPanel
            Left = 89
            Top = 72
            Width = 330
            Height = 250
            BevelInner = bvLowered
            TabOrder = 1
            object ScreenShot: TImage
              Left = 5
              Top = 6
              Width = 320
              Height = 240
            end
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 39
        Width = 730
        Height = 2
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        ExplicitTop = 42
      end
      object TBDock3: TTBDock
        Left = 0
        Top = 0
        Width = 730
        Height = 39
        AllowDrag = False
        Background = MainForm.TBBackground1
        object TBToolbar2: TTBToolbar
          Left = 0
          Top = 0
          Caption = 'MainToolbar'
          DockPos = 61
          DockRow = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Images = ImageList2
          Options = [tboImageAboveCaption]
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          Stretch = True
          TabOrder = 0
          object tbLoad: TTBItem
            Caption = #1054#1073#1085#1086#1074#1080#1090#1100
            ImageIndex = 9
            Images = ImageList2
            OnClick = tbLoadClick
          end
          object tbSave: TTBItem
            Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
            ImageIndex = 8
            Images = ImageList2
            OnClick = tbSaveClick
          end
        end
      end
    end
  end
  object Panel12: TPanel
    Left = 0
    Top = 0
    Width = 2
    Height = 312
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
  end
  object Panel14: TPanel
    Left = 0
    Top = 312
    Width = 740
    Height = 159
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object Panel15: TPanel
      Left = 0
      Top = 0
      Width = 740
      Height = 20
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object CheckBox3: TCheckBox
        Left = 7
        Top = 2
        Width = 194
        Height = 17
        Caption = #1055#1088#1080#1086#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1077
        TabOrder = 0
      end
      object ButtonB: TButton
        Left = 184
        Top = 2
        Width = 81
        Height = 18
        Caption = 'Select All'
        TabOrder = 1
        OnClick = ButtonBClick
      end
    end
    object Memo1: TMemo
      Left = 480
      Top = 20
      Width = 260
      Height = 139
      Align = alRight
      Lines.Strings = (
        'Memo1')
      TabOrder = 1
    end
    object RichEdit1__: TMemo
      Left = 0
      Top = 20
      Width = 480
      Height = 139
      Align = alClient
      TabOrder = 2
    end
  end
  object Panel45: TPanel
    Left = 88
    Top = 353
    Width = 65
    Height = 101
    TabOrder = 3
    Visible = False
    object LeftImage: TImage
      Left = 8
      Top = 8
      Width = 16
      Height = 16
      AutoSize = True
      Picture.Data = {
        07544269746D6170F6000000424DF60000000000000076000000280000001000
        0000100000000100040000000000800000000000000000000000100000000000
        0000000000000000800000800000008080008000000080008000808000008080
        8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00DDDDDDDDDDDDD000DDD
        DDDDDDDDD00F0DDDDDDDDDDD00FF000000DDDDD00FFFFFFFF0DDDD00FFFFFFFF
        F0DDDD00FFFFFFFFF0DDDDD00FFFFFFFF0DDDDDD00FF000000DDDDDDD00F0DDD
        DDDDDDDDDD000DDDDDDDDDDDDDD00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDD}
      Transparent = True
    end
    object RightImage: TImage
      Left = 32
      Top = 8
      Width = 16
      Height = 16
      AutoSize = True
      Picture.Data = {
        07544269746D6170F6000000424DF60000000000000076000000280000001000
        0000100000000100040000000000800000000000000000000000100000000000
        0000000000000000800000800000008080008000000080008000808000008080
        8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00DDDDDDDDDDDDDD000DD
        DDDDDDDDDDD0F00DDDDDDD000000FF00DDDDDD0FFFFFFFF00DDDDD0FFFFFFFFF
        00DDDD0FFFFFFFFF00DDDD0FFFFFFFF00DDDDD000000FF00DDDDDDDDDDD0F00D
        DDDDDDDDDDD000DDDDDDDDDDDDD00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDD}
      Transparent = True
    end
    object LeftRImage: TImage
      Left = 8
      Top = 32
      Width = 16
      Height = 16
      AutoSize = True
      Picture.Data = {
        07544269746D6170F6000000424DF60000000000000076000000280000001000
        0000100000000100040000000000800000000000000000000000100000000000
        0000000000000000800000800000008080008000000080008000808000008080
        8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00DDDDDDDDDDDDD000DDD
        DDDDDDDDD0090DDDDDDDDDDD0099000000DDDDD00999999990DDDD0099999999
        90DDDD009999999990DDDDD00999999990DDDDDD0099000000DDDDDDD0090DDD
        DDDDDDDDDD000DDDDDDDDDDDDDD00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDD}
      Transparent = True
    end
    object RightRImage: TImage
      Left = 32
      Top = 32
      Width = 16
      Height = 16
      AutoSize = True
      Picture.Data = {
        07544269746D6170F6000000424DF60000000000000076000000280000001000
        0000100000000100040000000000800000000000000000000000100000000000
        0000000000000000800000800000008080008000000080008000808000008080
        8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00DDDDDDDDDDDDDD000DD
        DDDDDDDDDDD0900DDDDDDD0000009900DDDDDD09999999900DDDDD0999999999
        00DDDD099999999900DDDD09999999900DDDDD0000009900DDDDDDDDDDD0900D
        DDDDDDDDDDD000DDDDDDDDDDDDD00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDD}
      Transparent = True
    end
    object LeftGImage: TImage
      Left = 8
      Top = 56
      Width = 16
      Height = 16
      AutoSize = True
      Picture.Data = {
        07544269746D6170F6000000424DF60000000000000076000000280000001000
        0000100000000100040000000000800000000000000000000000100000000000
        0000000000000000800000800000008080008000000080008000808000008080
        8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00DDDDDDDDDDDDD000DDD
        DDDDDDDDD00A0DDDDDDDDDDD00AA000000DDDDD00AAAAAAAA0DDDD00AAAAAAAA
        A0DDDD00AAAAAAAAA0DDDDD00AAAAAAAA0DDDDDD00AA000000DDDDDDD00A0DDD
        DDDDDDDDDD000DDDDDDDDDDDDDD00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDD}
      Transparent = True
    end
    object RightGImage: TImage
      Left = 32
      Top = 56
      Width = 16
      Height = 16
      AutoSize = True
      Picture.Data = {
        07544269746D6170F6000000424DF60000000000000076000000280000001000
        0000100000000100040000000000800000000000000000000000100000000000
        0000000000000000800000800000008080008000000080008000808000008080
        8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00DDDDDDDDDDDDDD000DD
        DDDDDDDDDDD0A00DDDDDDD000000AA00DDDDDD0AAAAAAAA00DDDDD0AAAAAAAAA
        00DDDD0AAAAAAAAA00DDDD0AAAAAAAA00DDDDD000000AA00DDDDDDDDDDD0A00D
        DDDDDDDDDDD000DDDDDDDDDDDDD00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDD}
      Transparent = True
    end
    object LeftMImage: TImage
      Left = 8
      Top = 78
      Width = 16
      Height = 16
      AutoSize = True
      Picture.Data = {
        07544269746D6170F6000000424DF60000000000000076000000280000001000
        0000100000000100040000000000800000000000000000000000100000000000
        0000000000000000800000800000008080008000000080008000808000008080
        8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00DDDDDDDDDDDDD000DDD
        DDDDDDDDD00E0DDDDDDDDDDD00EE000000DDDDD00EEEEEEEE0DDDD00EEEEEEEE
        E0DDDD00EEEEEEEEE0DDDDD00EEEEEEEE0DDDDDD00EE000000DDDDDDD00E0DDD
        DDDDDDDDDD000DDDDDDDDDDDDDD00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDD}
      Transparent = True
    end
    object RightMImage: TImage
      Left = 32
      Top = 78
      Width = 16
      Height = 16
      AutoSize = True
      Picture.Data = {
        07544269746D6170F6000000424DF60000000000000076000000280000001000
        0000100000000100040000000000800000000000000000000000100000000000
        0000000000000000800000800000008080008000000080008000808000008080
        8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00DDDDDDDDDDDDDD000DD
        DDDDDDDDDDD0E00DDDDDDD000000EE00DDDDDD0EEEEEEEE00DDDDD0EEEEEEEEE
        00DDDD0EEEEEEEEE00DDDD0EEEEEEEE00DDDDD000000EE00DDDDDDDDDDD0E00D
        DDDDDDDDDDD000DDDDDDDDDDDDD00DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDD}
      Transparent = True
    end
  end
  object ImageList1: TImageList
    Left = 29
    Top = 231
    Bitmap = {
      494C010107000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      00000000000000848400000000000000000000000000FFFFFF0000FFFF000000
      0000008484000084840000848400008484000084840000848400008484000084
      8400008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      84000084840000848400000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000008484000084840000000000000000000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      840084848400848484000084000000FF000000FF000000840000848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008400000084
      000000840000008400000084000000FF000000FF000000840000008400000084
      0000008400000084000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840084848400000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000000000000084000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000084000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000840084848400000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000000000000084000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000084000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000840084848400000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008400000084
      000000840000008400000084000000FF000000FF000000840000008400000084
      0000008400000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FFFFC001001F0000
      F81F8031000F0000F81F803100070000F81F803100030000F81F800100010000
      F81F800100000000C0038001001F0000C0038FF1001F0000E0078FF1001F0000
      F00F8FF18FF10000F81F8FF1FFF90000FC3F8FF1FF750000FE7F8FF5FF8F0000
      FFFF8001FFFF0000FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFE1FFFFFFFFFFFFF
      FC1FDFFBFFFFFE7FFC1F8FFFFFFFFC3FFC1F87F7FFFFF81FE001C7EFE001F00F
      C001E3CFC001E007C001F19FC001C003C001F83FC001C003C003FC7FC003F81F
      FC1FF83FFFFFF81FFC1FF19FFFFFF81FFC1FC3CFFFFFF81FFC3F87E7FFFFF81F
      FFFF8FFBFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object Timer1: TTimer
    Interval = 200
    OnTimer = Timer1Timer
    Left = 60
    Top = 231
  end
  object SaveDialog1: TSaveDialog
    FileName = '*.a11'
    Filter = '*.a11'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 91
    Top = 231
  end
  object SaveDialog2: TSaveDialog
    DefaultExt = '*.bmp'
    FileName = '*.bmp'
    Filter = '*.bmp'
    Left = 122
    Top = 231
  end
  object Timer2: TTimer
    Interval = 10
    OnTimer = Timer2Timer
    Left = 59
    Top = 264
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.txt'
    FileName = '*.txt'
    Filter = '*.txt'
    Left = 90
    Top = 264
  end
  object ImageList2: TImageList
    Left = 122
    Top = 264
    Bitmap = {
      494C01010C000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00848484000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      00000000000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00848484000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      00000000000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00848484000000000000000000FFFF00008484
      840084848400848484008484840084848400FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF00008484
      840084848400848484008484840084848400FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      00000000000084848400FFFFFF00FFFFFF000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00848484000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000FF000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      00000000FF000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      00000000000084848400FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00848484000000000000000000FFFF00008484
      840084848400848484008484840084848400000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000FFFF00008484
      8400848484008484840084848400848484000000000000000000000000000000
      00000000FF000000FF0000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000008484840084848400848484008484
      84008484840084848400FFFFFF00FFFFFF000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00848484000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000000000000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      00000084840000848400000000000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084848400FFFFFF00000000000000000000000000FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00848484000000000000000000FFFF00008484
      8400848484008484840084848400000000000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000FFFF00008484
      840084848400848484008484840084848400000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400000000000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084848400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00848484000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      00000000FF000000FF0000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400000000000000000084848400FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000008484840084848400848484008484
      8400848484008484840084848400848484000000000000000000FFFF00008484
      840084848400848484008484840084848400FFFF0000000000000000FF000000
      0000000000000000000000000000000000000000000000000000FFFF00008484
      840084848400848484008484840084848400FFFF000000000000000000000000
      00000000FF000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400000000000000000084848400FFFFFF00FFFFFF000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00848484000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400000000000000000084848400FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484000000
      0000000000000000000000000000000000000000000000000000FFFF00008484
      840084848400848484008484840084848400FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF00008484
      840084848400848484008484840084848400FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000084848400FFFFFF00FFFFFF000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00848484000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400848484008484
      8400848484008484840084848400848484008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF00000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF000000000000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      840084848400848484000084000000FF000000FF000000840000848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF000000000000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008400000084
      000000840000008400000084000000FF000000FF000000840000008400000084
      0000008400000084000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000084000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000084000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF000000000000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008400000084
      000000840000008400000084000000FF000000FF000000840000008400000084
      0000008400000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF000000000000FFFF0000FFFF00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF00000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF008484
      8400C6C6C600C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF008484
      8400C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484000084840000848400008484000084840000848400000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF00E7DECE00CEAD7300BD9C5200C69C4200C6A55200D6BD7300EFDEBD00FFFF
      F700FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400000000000000000000000000000000000000000000000000008484
      00000000000000000000000000000000000000000000FFFFFF00F7EFE700CEAD
      7300B58C3900BD944200BD944200C69C4200D6B54A00E7BD5200DEBD4A00EFCE
      7300F7EFE700000000000000000000000000000000000000000000000000C6C6
      C600000000000000000000000000000000000000000000000000C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C60000848400008484008484
      00000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000000000000000
      00008484000000848400008484000000000000000000F7EFE700BD9C5200B58C
      4200B58C4200B58C4200B58C3900C69C4200D6AD4A00EFC65200F7DE5A00F7DE
      6B00EFC66300F7EFD60000000000000000000000000000000000C6C6C600C6C6
      C6000000000000000000000000000000000000000000C6C6C600C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C6000000000000FFFF000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      000000000000848400000084840000000000FFFFF700CEB57B00B58C3900B58C
      4200B58C4200BD944200C6A56300CEB57B00DEBD7300EFC66300F7DE6B00FFEF
      A500F7DE6B00EFCE7300FFFFF7000000000000000000C6C6C600C6C6C6000000
      00000000FF00000000000000000000000000C6C6C600C6C6C600000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C6000000000084840000FFFF
      0000FFFF0000FFFF000084840000848400008484000084840000FFFF0000FFFF
      0000FFFF0000000000008484000000000000E7DECE00B58C3900B58C4200B58C
      4200BD9C5200DED6AD00F7F7F700FFFFFF00EFF7FF00F7EFBD00FFEFA500F7DE
      6B00F7CE5200DEBD4A00EFDEBD00FFF7FF000000000000000000000000000000
      FF000000FF000000FF0000000000C6C6C600C6C6C600000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C6000000000084840000FFFF
      0000FFFF000084840000848400000000000000FFFF000000000084840000FFFF
      0000FFFF0000000000008484000000000000CEB58400BD944200B58C4200BD94
      4200E7D6B500F7FFFF00E7E7E700CED6D600E7E7E700EFF7FF00F7EFBD00EFC6
      6300EFC65200E7BD5200DEBD6B00EFE7EF000000000000000000000000000000
      00000000FF000000FF000000FF0000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C60084840000FFFF0000FFFF
      000000000000848400000000000000FFFF000000000000FFFF00000000008484
      0000FFFF0000FFFF00000000000084840000C6A56300C69C4200C69C4200CEAD
      6300F7F7F700E7E7E700C6C6BD00BDBDBD00BDBDBD00E7E7E700F7F7F700DEBD
      7300D6AD4A00D6B54A00CEA54200EFE7E7000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C60084840000FFFF0000FFFF
      0000000000008484000000FFFF000000000000FFFF000000000000FFFF008484
      0000FFFF0000FFFF00000000000084840000C6A56300D6B54A00D6AD4A00DEBD
      7300FFFFFF00D6D6CE00BDBDBD00000000000000000000000000000000000000
      00000000000000000000C69C4200E7DECE000000000000000000000000000000
      0000C6C6C600000000000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000008484840000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      000000000000000000000000000000000000C6C6C60084840000FFFF0000FFFF
      000000000000848400000000000000FFFF000000000000FFFF00000000008484
      0000FFFF0000FFFF00000000000084840000CEAD6300E7BD5200EFC65200EFCE
      7300F7F7F700E7E7E700BDBDBD00000000000000DE000000DE000000DE000000
      DE0000000000BD944200BD944200E7DECE00000000000000000000000000C6C6
      C600C6C6C600000000000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000008484840000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000000000000000
      000000000000000000000000000000000000C6C6C60084840000FFFF0000FFFF
      0000000000008484000000FFFF0000000000000000000000000000FFFF008484
      0000FFFF0000FFFF00000000000084840000DECE8400E7BD5200F7DE5A00F7DE
      6B00F7EFBD00EFF7FF00DEDEDE00000000000000DE000000FF000000FF000000
      0000B58C4200B58C4200CEAD7300EFE7EF000000000000000000C6C6C600C6C6
      C600000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      00000000000000000000000000000000000084848400FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000000000000000
      000000000000000000000000000000000000C6C6C6000000000084840000FFFF
      0000FFFF0000000000008484000000FFFF0084840000FFFF000000000000FFFF
      0000FFFF0000000000008484000000000000F7E7CE00DEBD4A00F7DE5A00FFE7
      7300FFEFA500F7EFBD00F7F7F700000000000000DE000000FF002121FF002121
      FF0000000000B58C3900E7D6B500EFE7F70000000000C6C6C600C6C6C6000000
      00000000FF000000FF000000FF0000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000008484840000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000000000000000
      000000000000000000008484840000000000C6C6C6000000000084840000FFFF
      0000FFFF000000000000848400000000000084840000FFFF0000FFFF0000FFFF
      0000FFFF0000000000008484000000000000FFFFFF00E7CE8400F7DE6B00FFF7
      A500FFE77300F7DE6B00EFCE7B00000000000000DE00000000002121FF004A4A
      FF004A4AFF0000000000FFF7EF00FFFFFF000000000000000000000000000000
      FF000000FF000000FF00000000000000000000000000000000000000FF000000
      FF000000FF0000000000000000000000000084848400FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000000000000000
      000000000000000000000000000000000000C6C6C6000000000000FFFF008484
      0000848400000000000000FFFF000000000084840000FFFF0000FFFF0000FFFF
      00000000000084840000C6C6C6000000000000000000FFF7EF00EFCE7B00F7DE
      6B00F7DE5A00F7DE5A00EFC652000000000000000000BD944200000000004A4A
      FF006B6BFF006B6BFF0000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000008484840000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000C6C6C6000000000000FF
      FF000000000000FFFF0000000000C6C6C60084840000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000F7EFE700E7CE
      8400DEBD4A00E7BD5200E7BD520000000000C69C4200BD944200B58C39000000
      00006B6BFF008C8CFF008C8CFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400848484008484
      840000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600000000008484000084840000848400008484
      00008484000000000000000000000000000000000000FFFFFF0000000000FFFF
      F700F7E7CE00DECE8400CEAD6300C6A55200C6A55200CEAD7300E7D6B500FFF7
      EF00000000008C8CFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000FFFF00FFFFFF0000FF
      FF00848484008484840084848400848484008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFF700FFFFF700FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFFF800803F803FC001F800
      803F803F8031F800802F802F8031F800804F80278031F800800F80638001F800
      80018001800100008001800080010000800180008FF10000800180018FF10000
      800F80638FF1001F804F80278FF1001F802F802F8FF1001F803F803F8FF5001F
      803F803F8001001FFFFFFFFFFFFF001F8001FFFFFFFFFFFF8001FFFFFE1FFFFF
      8001FFFFFC1FDFFB8001FFFFFC1F8FFF8001FFFFFC1F87F78001F3CFE001C7EF
      8001F99FC001E3CF8001FC3FC001F19F8001FE7FC001F83F8001FC3FC003FC7F
      8001F99FFC1FF83F8001F3CFFC1FF19F8001FFFFFC1FC3CF8001FFFFFC3F87E7
      8003FFFFFFFF8FFB8007FFFFFFFFFFFFF81FA003FFFFEDB680008007EFDFEAAA
      00008003C78FEAAA400000018307EDB640000000C003FFFF41400000E007FFF9
      02A00000F00FFFF001400000F01F003902A00000E01F003901000000C00F0039
      400000008007002141000000C183003F45018001E3C7003FAA03C000F7EF003F
      C103A001FFFF007FFFFFF81BFFFF8FFF00000000000000000000000000000000
      000000000000}
  end
  object OpenDialog2: TOpenDialog
    DefaultExt = 'lst'
    FileName = '*.lst'
    Filter = #1040#1088#1093#1080#1074#1099' '#1079#1072#1087#1080#1089#1077#1081'|*.lst|'#1042#1089#1077' '#1092#1072#1081#1083#1099'|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 102
    Top = 189
  end
  object SaveDialog3: TSaveDialog
    DefaultExt = 'lst'
    FileName = #1057#1087#1080#1089#1082#1080'.lst'
    Filter = '*.lst'
    Left = 70
    Top = 189
  end
end
