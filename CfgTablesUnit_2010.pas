/////////////////////////////////////////////////////
////    ������ ��������� �������� � ��������     ////
////            ���������������� ������          ////
////                                             ////
//// ������ �.�. ���� 2009                       ////
//// ������ 1.0.0                                ////
////   � �������� �.�.                           ////
/////////////////////////////////////////////////////

// ������� ���������
// 12.05.09 - ������������ ������ � CfgTablesUnit (��������)
// 14.05.09 - ������ CurrentGroupIndex.
// 25.05.09 - ���������� ������� GetCurrentGroupName (��������� ������ ��� ���������� ������� ������)
// 26.05.09 - ���������� ��������� ������������ (��������). ������� ����������� �� ���-�� ����� CAPTION � ������ - 5000.
// 18.06.09 - ��������� TFakeLanguageTable.
// 27.11.09 - ��������� ��������� �������.
// 20.01.09 - ��������� ��������� �������� ���� TLanguageTable.TranslateInterfaces.
// 26.01.09 - �������� ������. ����� TLng ��� ���������� ������� � �������� �������� �� ��������� �������.
// 27.12.10 - ������ ������������ � CfgTablesUnit_2010. � ��������� �� Unicod, ��� ��������� �������� ���������� ������

unit CfgTablesUnit_2010;

interface

uses Classes, Forms, OmniXML, Graphics, {PublicFunc, }Dialogs, SysUtils;

type

    // ���� ������� ������� �������������, �������� ���� � �������� �����
    TElement = record
      Id: string;
      Value: Variant;
    end;

    // � ����� ������ ��������� ��������� (�������� ������ �������� �����)
    //TGroup = array of TElement;
    TGroup = record
      Name: string;
      Default: Boolean;
      Elements: array of TElement;
    end;

    // ��������� ������ ������� ��������� ��������� ����� (�������� �������� ����)
    TGroupMas = array of TGroup;

    // ����� ����� ��� �������� �������� ������ � ������
    TCustomTable = class(TComponent)
    private
      FGroupMas: TGroupMas;
      FCurrentGroup: Integer;
      FDefaultGroup: Integer;
      function GetNodeName: string; virtual; abstract;
      property FNodeName: string read GetNodeName;
    protected
      procedure LoadNodeFromFile(FileName, NodeName: string); // ��������� ������� ��� ��������� � ������ ���������� ������ ���� �� ����� � ������� ���������� �����
      procedure SetGroupCount(Value: Integer); // ��������� ������� ��� ���� ����� ��� �������� ������� ������ ���-�� �����
      procedure SetElementCount(GroupNum, Value: Integer); // ��������� ������� ��� ���� ����� ��� �������� ������� ������ ���-�� ��������� � ������ � ������� GroupNum
      procedure SetGroupName(GroupNum: Integer; Value: string); // ��������� ������� ��� ���� ����� ��� �������� ������� ������ ��� ������ � ������� GroupNum
      procedure SetGroupDefaultness(GroupNum: Integer; Value: Boolean); // ��������� ������� ��� ���� ����� ��� �������� ������� ������ ��������� �� ������ � ������� GroupNum
      procedure SetElementValue(GroupNum, ElementNum: Integer; Value: TElement); // ��������� ������� ��� ���� ����� ��� �������� ������� ��������� ������ � ������� ElementNum, ����������� � ������ � ������� GroupNum
      function GetGroupCount: Integer;
      function GetGroupName(Index: Integer): string;
      function GetCurrentGroupName: string;
      procedure SetGroup(Name: string);
      function GetElementByID(Id: string): Variant;
    public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      procedure Clear;
      procedure LoadFromFile(FileName: string); virtual;
      procedure LoadFromXML(XML: String);
      procedure ResetToDefault;
      property GroupsCount: Integer read GetGroupCount;
      property GroupName[Index: Integer]: string read GetGroupName;
      property CurrentGroup: string read GetCurrentGroupName write SetGroup;
      property CurrentGroupIndex: Integer read FCurrentGroup;
    end;

    // ������ ��� �������� � �������� ������ � �������� ������
    TColorTable = class(TCustomTable)
    private
      function GetNodeName: string; override;
    protected
      function GetColorByID(id: string): TColor;
    public
      procedure LoadFromFile(FileName: string); override;
      property Color[index: string]: TColor read GetColorByID; default;
    published
    end;

    // ������ ��� �������� � �������� ������ �����
    TLanguageTable = class(TCustomTable)
    private
      ExcList: TStringList;

      function GetNodeName: string; override;
    protected
      function GetCaptionByID(id: string): string; virtual;
    public
      procedure LoadFromFile(FileName: string); override;
      property Caption[index: string]: string read GetCaptionByID; default;

      procedure TranslateInterfaces;  // �������� Published ��������� TranslateInterface �� ���� ������ ����������. �������� ����� �������� ���� ����.

      constructor Create(AOwner: TComponent);
      destructor Destroy; override;

    published
    end;

    TFakeLanguageTable = class(TLanguageTable)
    protected
      function GetCaptionByID(id: string): string; override;
    end;

    TLng = class
    private
      LanguageTable: TLanguageTable;
      constructor Create();
    public
      class function Caption( Index: string ): string;
      class procedure SetLanguageTable( LanguageTable: TLanguageTable );
    end;

implementation

uses
  CommonUnit;

var
  LngInstanse: TLng;

{ TCustomTable }

procedure TCustomTable.Clear;
var
  i: Integer;
begin
  for i:=0 to Length(FGroupMas)-1 do
     SetLength(FGroupMas[i].Elements, 0);
  SetLength(FGroupMas, 0);
  FCurrentGroup:=-1;
end;

constructor TCustomTable.Create(AOwner: TComponent);
begin
  inherited;
  Clear;
end;

destructor TCustomTable.Destroy;
begin
  Clear;
  inherited;
end;

function TCustomTable.GetCurrentGroupName: string;
begin
  if FCurrentGroup < 0 then Result:= '' else Result:=FGroupMas[FCurrentGroup].Name;
end;

function TCustomTable.GetElementByID(Id: string): Variant;
var
  i: Integer;
  st: string;
begin
  if FCurrentGroup>=0 then
    for i:=0 to Length(FGroupMas[FCurrentGroup].Elements)-1 do
      begin
        //Utf8ToUnicode(PWideChar(st), PAnsiChar(FGroupMas[FCurrentGroup].Elements[i].Id), 255);
       if AnsiUpperCase(FGroupMas[FCurrentGroup].Elements[i].Id)=AnsiUpperCase(Id) then
         begin
           Result:=FGroupMas[FCurrentGroup].Elements[i].Value;
           Break;
         end;
      end;
end;

function TCustomTable.GetGroupCount: Integer;
begin
  Result:=Length(FGroupMas);
end;

function TCustomTable.GetGroupName(Index: Integer): string;
begin
  Result:=FGroupMas[Index].Name;
end;

procedure TCustomTable.LoadFromFile(FileName: string);
begin
  // ����� ����� xml-���� ������� �� ���� ������ ������ � ����� �� � LoadFromXML
end;

procedure TCustomTable.LoadFromXML(XML: string);
var
  Doc: IXmlDocument;
  Node: IXmlNode;
  i, j, k, c, c1: Integer;
  Attr: Variant;
  El: TElement;
  st2: string;

begin
  // � ������� �������� ������ ���� �������� ����������� ���������� �� XML ������
  inherited;
  Clear;
  Doc:=CreateXMLDoc;
  Doc.LoadXML(XML);
  //c:=Doc.DocumentElement.ChildNodes.Length; // ���-�� ��������� �����
  //ShowMessage(Doc.DocumentElement.NodeName);
  //c:=Doc.DocumentElement.ChildNodes.Length; // ���-�� ��������� �����
  //c:=Doc.DocumentElement.; // ���-�� ��������� �����
  c:=0;
  for i := 0 to Doc.DocumentElement.ChildNodes.Length - 1 do
    begin
      if(Doc.DocumentElement.ChildNodes.Item[i].NodeType = ELEMENT_NODE) then
        Inc(c);
    end;

  if c=0 then exit;
  SetGroupCount(c);
  FDefaultGroup:=0;
  // ������ ��� �����
   c:= 0;
  for i:=0 to Doc.DocumentElement.ChildNodes.Length - 1 do
    begin
      Node:=Doc.DocumentElement.ChildNodes.Item[i]; // ������ �� ���������� ������
      if (Node.NodeType = ELEMENT_NODE) then
        begin
          Attr:=Node.Attributes.GetNamedItem('NAME').NodeValue;
          SetGroupName(c, Attr);
          Attr:=Node.Attributes.GetNamedItem('DEFAULT').NodeValue;
          if Attr='TRUE' then
            begin
              SetGroupDefaultness(c, true);
              FDefaultGroup:=c;
            end
          else
            SetGroupDefaultness(c, false);

          SetElementCount(c, 5000);
          k:= 0;
          for j:=0 to Node.ChildNodes.Length - 1 do
            begin
              if(Node.ChildNodes.Item[j].NodeType = ELEMENT_NODE) then
                if (Node.ChildNodes.Item[j].NodeName= FNodeName) then
                  begin
                    Attr:=Node.ChildNodes.Item[j].Attributes.GetNamedItem('ID').NodeValue;
                    if Attr<>'NULL' then El.Id:=Attr;
                    Attr:=Node.ChildNodes.Item[j].Attributes.GetNamedItem('VALUE').NodeValue;
                    if Attr<>'NULL' then El.Value:=Attr;
                    SetElementValue(c, k, El);
                    Inc( k );
                  end;
            end;
          SetElementCount(c, k);
          Inc(c);
        end;
    end;
  FCurrentGroup:=FDefaultGroup;
end;

procedure TCustomTable.LoadNodeFromFile(FileName, NodeName: string);
var
  Doc: IXmlDocument;
  Node: IXmlNode;
  XMLst: string;
begin
  Clear;
  //Doc:=CreateXMLDocument;
  Doc:=CreateXMLDoc;
  Doc.Load(FileName);
  Node:=nil;
  Node:=Doc.DocumentElement.SelectSingleNode(NodeName);
  if Assigned(Node) then
  begin
    XMLst:=Node.XML;
    LoadFromXML(XMLst);
  end;
end;

procedure TCustomTable.ResetToDefault;
begin
  FCurrentGroup:=FDefaultGroup;
end;

procedure TCustomTable.SetElementCount(GroupNum, Value: Integer);
begin
  SetLength(FGroupMas[GroupNum].Elements, Value);
end;

procedure TCustomTable.SetElementValue(GroupNum, ElementNum: Integer;
  Value: TElement);
begin
  FGroupMas[GroupNum].Elements[ElementNum]:=Value;
end;

procedure TCustomTable.SetGroup(Name: string);
var
  i: Integer;
begin
  for i:=0 to Length(FGroupMas)-1 do
    if FGroupMas[i].Name=Name then
      begin
        FCurrentGroup:=i;
        Break;
      end;
end;

procedure TCustomTable.SetGroupCount(Value: Integer);
begin
  SetLength(FGroupMas, Value);
end;

procedure TCustomTable.SetGroupDefaultness(GroupNum: Integer;
  Value: Boolean);
begin
  FGroupMas[GroupNum].Default:=Value;
end;

procedure TCustomTable.SetGroupName(GroupNum: Integer; Value: string);
begin
  FGroupMas[GroupNum].Name:=Value;
end;

{ TColorTable }

function TColorTable.GetColorByID(id: string): TColor;
begin
  result:=self.GetElementByID(id);
end;

function TColorTable.GetNodeName: string;
begin
  Result:= 'COLOR';
end;

procedure TColorTable.LoadFromFile(FileName: string);
begin
  LoadNodeFromFile(FileName, 'COLORS');
end;

(*procedure TColorTable.LoadFromXML(XML: TXmlString);
var
  Doc: IXmlDocument;
  Node: IXmlNode;
  i, j, c, c1: Integer;
  Attr: Variant;
  El: TElement;
begin
  {inherited;
  Clear;
  Doc:=CreateXMLDocument;
  Doc.LoadXML(XML);
  c:=Doc.DocumentElement.ChildNodes.Count; // ���-�� ��������� �����
  if c=0 then exit;
  SetGroupCount(c);
  FDefaultGroup:=0;
  // ������ ��� �����
  for i:=0 to c-1 do
    begin
      Node:=Doc.DocumentElement.ChildNodes[i]; // ������ �� ���������� ������
      Attr:=Node.GetVarAttr('NAME','NULL');
      SetGroupName(i, Attr);
      Attr:=Node.GetVarAttr('DEFAULT','NULL');
      if Attr='TRUE' then
        begin
          SetGroupDefaultness(i, true);
          FDefaultGroup:=i;
        end
      else
        SetGroupDefaultness(i, false);
      c1:=Node.ChildNodes.Count; // ���-�� ���������� � i-��� ������
      SetElementCount(i, c1);
      for j:=0 to c1-1 do
         begin
           Attr:=Node.ChildNodes[j].GetVarAttr('ID','NULL');
           if Attr<>'NULL' then
             El.Id:=Attr;
           Attr:=Node.ChildNodes[j].GetVarAttr('VALUE','NULL');
           if Attr<>'NULL' then
             El.Value:=Attr;
           SetElementValue(i, j, El);
         end;
    end;
  FCurrentGroup:=FDefaultGroup;  }
end;     *)

{ TLanguageTable }

constructor TLanguageTable.Create;
begin
  inherited Create( AOwner );
  ExcList:= TStringList.Create;
end;

destructor TLanguageTable.Destroy;
begin
  ExcList.SaveToFile( DataFileDir + 'nottranslated.txt' );
  inherited;
end;

function TLanguageTable.GetCaptionByID(id: string): string;
var
  st: string;

begin

//  Result:= 'xxx';
//  Exit;

 //st:=GetElementByID(id);
  result:=GetElementByID(id);
  if Result = '' then
  begin
    if ExcList.IndexOf(id) = -1 then ExcList.Add( id );
    Result:= id;
  end;
  //Result:=St;
end;

function TLanguageTable.GetNodeName: string;
begin
  Result:= 'CAPTION';
end;

procedure TLanguageTable.LoadFromFile(FileName: string);
begin
  inherited;
  LoadNodeFromFile(FileName, 'LANGUAGES');
end;

(*procedure TLanguageTable.LoadFromXML(XML: TXmlString);
begin
  inherited;
  //
end; *)

type
  ProcTranslateInterface = procedure();

procedure TLanguageTable.TranslateInterfaces();
var
  i: Integer;
  pi: ProcTranslateInterface;
  p: Pointer;

begin
  for i:= 0 to Application.ComponentCount - 1 do
  if Application.Components[i] is TForm then
  begin
    p:= Application.Components[i].MethodAddress( 'TranslateInterface' );
    if p <> nil then
    begin
      @pi:= p;
      pi();
    end;
  end;
end;


{ TFakeLanguageTable }

function TFakeLanguageTable.GetCaptionByID(id: string): string;
begin
  Result:= ID;
end;

{ TLng }

class function TLng.Caption(Index: string): string;
begin
  if Assigned( LngInstanse.LanguageTable ) then
    Result:= LngInstanse.LanguageTable.Caption[Index]
  else
    Result:= Index;
end;

constructor TLng.Create;
begin
  inherited;
  LanguageTable:= nil;
end;

class procedure TLng.SetLanguageTable(LanguageTable: TLanguageTable);
begin
  LngInstanse.LanguageTable:= LanguageTable;
end;

initialization

//  LngInstanse:= TLng.Create;

finalization

//  LngInstanse.Free;

end.

