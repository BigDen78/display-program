object RecProgressForm: TRecProgressForm
  Left = 337
  Top = 170
  AlphaBlendValue = 128
  BorderIcons = []
  BorderStyle = bsDialog
  BorderWidth = 12
  Caption = #1055#1086#1078#1072#1083#1091#1081#1089#1090#1072', '#1078#1076#1080#1090#1077#8230
  ClientHeight = 18
  ClientWidth = 653
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PB: TProgressBar
    Left = 120
    Top = 0
    Width = 460
    Height = 18
    Align = alClient
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 120
    Height = 18
    Align = alLeft
    BevelOuter = bvNone
    Caption = #1046#1076#1080#1090#1077', '#1080#1076#1077#1090' '#1088#1072#1089#1089#1095#1077#1090':'
    Color = clCaptionText
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 585
    Top = 0
    Width = 68
    Height = 18
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object CancelButton: TButton
      Left = 5
      Top = 3
      Width = 57
      Height = 20
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 580
    Top = 0
    Width = 5
    Height = 18
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 3
  end
end
