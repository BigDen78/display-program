{$I DEF.INC}
unit Avk11ViewUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, NoteBook, VideoViewWin, PictureViewWin, AudioViewWin,
  TeEngine, Series, TeeProcs, Chart, ComCtrls, Buttons, DisplayUnit, Math, MyTypes,
  ImgList, ConfigUnit, InfoBarUnit, InfoBarUnit2, LanguageUnit, TB2Item, TB2Dock, TB2Toolbar,
  Spin, AviconDataSource, AviconTypes, PNGImage, MarkedPoint, CommonUnit, FileInfoUnit, HeadScan;

type
{
  TRecInfo = record
    FileLen_Km: Single;
    WorkTime_Min: Single;
    UnCtrlZonLen: Integer;
    ObjCount: array [Low(TRecObjectType)..High(TRecObjectType)] of Integer;
  end;
}

  TMediaListItem = record
    Form: TForm;
    EventIdx: Integer;
  end;

  TFormMode = (fmMultichanel, fmSingleChannel);

  TAvk11ViewForm = class(TForm)
    ScrollBar1: TScrollBar;
    Button3: TButton;
    Button4: TButton;
    Timer1: TTimer;
    FilusX111W_HandScan: TPanel;
    Panel1: TPanel;
    ListBox1: TListBox;
    Panel2: TPanel;
    Panel3: TPanel;
    FileInfoMemo: TMemo;
    Panel4: TPanel;
    Memo1: TMemo;
    Image1: TImage;
    AnalyzeProgressBarPanel: TPanel;
    ProgressBar: TProgressBar;
    Panel5: TPanel;
    Panel6: TPanel;
    Button1: TButton;
    Memo2: TMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure SetProgressBarPosition(Position: Integer; Hide: Boolean);
    procedure SetProgressBarCaption(caption: String);
		procedure UpdateProgressBar;
    procedure DDMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormActivate(Sender: TObject);
    procedure AddToNotebook_(Sender: TObject);
    procedure SelHandScan(Sender: TObject; HSList: array of Integer; DataType: Integer; ViewForm: Pointer = nil);
    procedure SelMedia(Sender: TObject; MediaType: TMediaType; var Data: TMemoryStream; EventIdx: Integer);
    procedure SelNotebook(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Timer1Timer(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure ListBox1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure OnCloseMediaForm(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    FSavePage: Integer;
    FMouseDownFlag: Boolean;
    FOldX: Integer;
    FOldY: Integer;
    FDS: TAvk11DatSrc;
    FDisplay: TAvk11Display;
    FInfoBarForm: TInfoBarForm;
    FInfoBarForm2: TInfoBarForm2;
//    FNoteBookForm: TNoteBookForm;
    FADDtoNoteBook: Boolean;
    FSetSelectVideo: Boolean;
    FADDtoNordCo: Boolean;
    FSoundFlag: Boolean;
    FGShift: Extended;
    FSpeed: Integer;
    FFullScreenMode: Boolean;
    FNordCoMouseDisCoord: Integer;
    FFileName: string;
    ProgressBarHide : Boolean;
		ProgressBarPosition : Integer;
		ProgressBarCaption : String;
    FHandSForm_: Pointer;
    FHeadSForm_: Pointer;
    FFormMode: TFormMode;
    FMediaList: array of TMediaListItem;

    procedure DisplayModeToControl(Sender: TObject);
    procedure SetADDtoNoteBook(New: Boolean);
    procedure SetSelectVideo(New: Boolean);
    procedure SetADDtoNordCo(New: Boolean);
    procedure SetSoundFlag(New: Boolean);
{
    function GetAssignCount: Integer;
    function GetAssignItem(Index: Integer): TAvk11ViewForm;
}
  public
    OnChangeDisplayMode: TNotifyEvent;
    OnChangeADDtoNoteBook: TNotifyEvent;
    OnChangeSoundFlag: TNotifyEvent;
    FLastSelKM: Integer;
    FLastSelPk: Integer;
    FLastSelMetre: string;
    FLastSelCa: string;
    GotoLabel: TTBSubmenuItem;
    DeleteViewedFile: Boolean;

    Recognize: Boolean;
//    RecInfo: TRecInfo;
    A11RecObj: TObject;
    ShowRecRes: Boolean;
//    RecTimeLabel: string;

    procedure ShowGPSPath;
    procedure SetFocus_;
    function LoadFile(FileName: string): Boolean;
    procedure SetInfoBarForm(NewForm: TInfoBarForm);
    procedure SetInfoBarForm2(NewForm: TInfoBarForm2);
    procedure FileDateToControl;
    procedure MyActivate;
    procedure ShowRecStatistic(Big: Boolean);
{
    procedure AssignTo(View: TAvk11ViewForm);
    procedure DeleteAssign(Index: Integer); overload;
    procedure DeleteAssign(View: TAvk11ViewForm); overload;
}
    property DatSrc: TAvk11DatSrc read FDS write FDS;
    property Display: TAvk11Display read FDisplay;
//    property NoteBookForm: TNoteBookForm read FNoteBookForm write FNoteBookForm;
    property ADDtoNoteBook: Boolean read FADDtoNoteBook write SetADDtoNoteBook;
    property ADDtoNordCo: Boolean read FADDtoNordCo write SetADDtoNordCo;
    property SelectVideo: Boolean read FSetSelectVideo write SetSelectVideo;
    property SoundFlag: Boolean read FSoundFlag write SetSoundFlag;
    property HandSForm_: Pointer read FHandSForm_;
    property HeadSForm_: Pointer read FHeadSForm_;
    property FormMode: TFormMode read FFormMode;

//    property NordcoCSVFile: TNordcoCSVFile read FNordcoCSVFile;
{    property AssignCount: Integer read GetAssignCount;
    property AssignItem[Index: Integer]: TAvk11ViewForm read GetAssignItem; }
  end;

implementation

uses
  NewEntry, MainUnit, HandScan {$IFDEF VIEWBYKM}, DBConnecUnit {$ENDIF}, EVideoUnit;

{$R *.DFM}

procedure TAvk11ViewForm.FormCreate(Sender: TObject);
begin
  FHandSForm_:= nil;
  FHeadSForm_:= nil;
  DeleteViewedFile:= False;
  Recognize:= False;
  ShowRecRes:= False;

  FLastSelKM:= 0;
  FLastSelPk:= 0;
  FLastSelMetre:= '0';

  OnChangeDisplayMode:= nil;
  OnChangeADDtoNoteBook:= nil;
  OnChangeSoundFlag:= nil;

//  FNoteBookForm:= nil;
  FMouseDownFlag:= False;

  FDS:= TAvk11DatSrc.Create;
  FDisplay:= TAvk11Display.Create(Self);
  FDisplay.SetData(Self, ScrollBar1, FDS);
  FDisplay.Align:= alClient;
//  FDisplay.Align:= alLeft;
  FDisplay.OnMouseMove:= DDMouseMove;
  FDisplay.OnSelRailPoint:= AddToNotebook_;
  FDisplay.OnSelHandScan:= SelHandScan;
  FDisplay.OnSelNotebook:= SelNotebook;
  FDisplay.OnSelMedia:= SelMedia;

  {$IFDEF REC}
  FDisplay.OnSelRecResEvent:= MainForm.RecInt.FResForm.OnSelResItem;
  {$ENDIF REC}

  FDisplay.Visible:= True;

  FDisplay.DisplayConfig:= Config.DisplayConfig;
{  FDisplay.DisplayConfig.BScanViewZone[2].BegDelay:=
  FDisplay.DisplayConfig.BScanViewZone[2].EndDelay:=
  FDisplay.DisplayConfig.BScanViewZone[2].Dur:=
}
  FDisplay.EchoSizeByAmpl:= Config.EchoSizeByAmpl;
  FDisplay.EchoColorByAmpl:= Config.EchoColorByAmpl;
//  FDisplay.OnCanNotWork:= StopWork;
  FFullScreenMode:= False;

  FGShift:= 0;
  FSpeed:= 0;

  GotoLabel:= nil;

  AnalyzeProgressBarPanel.Visible := False;
//  ProgressBar.Visible := False;
//  ProgressBar.BringToFront;
  FileInfoMemo.BringToFront;
  SetLength(FMediaList, 0);
end;

procedure TAvk11ViewForm.SelHandScan(Sender: TObject; HSList: array of Integer; DataType: Integer; ViewForm: Pointer = nil);
var
  I: Integer;
  J: Integer;
  Flg: Boolean;
  HSForm: THandScanForm;
  HeadSForm: THeadScanForm;
  Avk11View: TAvk11ViewForm;

begin
  if Assigned(ViewForm) then Avk11View:= TAvk11ViewForm(ViewForm)
                        else Avk11View:= MainForm.ActiveViewForm;

  if DataType = 1 then
    for I:= 0 to High(HSList) do
    begin
      if Avk11View <> nil then
      begin
        Flg:= False;
        for J:= MainForm.MDIChildCount - 1 downto 0 do
          if (MainForm.MDIChildren[J] is THandScanForm) and
             (THandScanForm(MainForm.MDIChildren[J]).Tag = Integer(Self)) and
             (THandScanForm(MainForm.MDIChildren[J]).ScrollBar1.Tag = HSList[I]) then
          begin
            Flg:= True;
            Break;
          end;

        if not Flg then
        begin
          HSForm:= THandScanForm.Create(nil);
          HSForm.SetHSData(Avk11View.DatSrc.HSList[HSList[I]], MainForm.InfoBarForm, ExtractFileName(Avk11View.DatSrc.FileName), Avk11View.DatSrc, HSList[I]);
          HSForm.Tag:= Integer(Self);
          HSForm.ScrollBar1.Tag:= HSList[I];
          FHandSForm_:= HSForm;
        end;
      end;
    end;

  if DataType = 2 then
    for I:= 0 to High(HSList) do
    begin
      if Avk11View <> nil then
      begin
        Flg:= False;
        for J:= MainForm.MDIChildCount - 1 downto 0 do
          if (MainForm.MDIChildren[J] is THeadScanForm) and
             (THeadScanForm(MainForm.MDIChildren[J]).Tag = Integer(Self)) and
             (THeadScanForm(MainForm.MDIChildren[J]).Button4.Tag = HSList[I]) then
          begin
            Flg:= True;
            Break;
          end;

        if not Flg then
        begin
          HeadSForm:= THeadScanForm.Create(nil);
          HeadSForm.SetHScanData(@Avk11View.DatSrc.HeadScanList[HSList[I]], MainForm.InfoBarForm, ExtractFileName(Avk11View.DatSrc.FileName), Avk11View.DatSrc, HSList[I]);
          HeadSForm.Tag:= Integer(Self);
          HeadSForm.Button4.Tag:= HSList[I];
          FHeadSForm_:= HeadSForm;
        end;
      end;
    end;


(*
  for I:= 0 to High(HSList) do
  begin
    if Avk11View <> nil then
    begin
      Flg:= False;
      for J:= MainForm.MDIChildCount - 1 downto 0 do
        if (MainForm.MDIChildren[J] is THandScanForm) and
           (THandScanForm(MainForm.MDIChildren[J]).Tag = Integer(Self)) and
           (THandScanForm(MainForm.MDIChildren[J]).ScrollBar1.Tag = HSList[I]) then
        begin
          Flg:= True;
          Break;
        end;

      if not Flg then
      begin
        if DataType = 1 then
        begin
          HSForm:= THandScanForm.Create(nil);
          HSForm.SetHSData(Avk11View.DatSrc.HSList[HSList[I]], MainForm.InfoBarForm, ExtractFileName(Avk11View.DatSrc.FileName), Avk11View.DatSrc, HSList[I]);
          HSForm.Tag:= Integer(Self);
          HSForm.ScrollBar1.Tag:= HSList[I];
        end
        else
        begin
          HeadSForm:= THeadScanForm.Create(nil);
          HeadSForm.SetHScanData(@Avk11View.DatSrc.HeadScanList[HSList[I]], MainForm.InfoBarForm, ExtractFileName(Avk11View.DatSrc.FileName), Avk11View.DatSrc, HSList[I]);
          HeadSForm.Tag:= Integer(Self);
        end
      end;
    end;
  end;
  *)
end;

procedure TAvk11ViewForm.SelMedia(Sender: TObject; MediaType: TMediaType; var Data: TMemoryStream; EventIdx: Integer);
var
  fn: Ansistring;
  I: Integer;
  VideoForm: TVideoView;
  PictureForm: TPictureView;
  AudioView: TAudioView;
  cap_str: string;

function ResampleWave(SrcFN, DestFN: string): Boolean; // Convert Wave file from 32bit to 16bit
type
  TWave = packed record
    chunkId       : Cardinal;      // �������� ������� �RIFF� � ASCII ��������� (0x52494646)
    chunkSize     : Cardinal;      // ���������� ������ �������, ������� � ���� �������. ����� ������, ��� ������ ����� � 8, �� ����, ��������� ���� chunkId � chunkSize.
                                   // 4 + (8 + subchunk1Size) + (8 + subchunk2Size)
    format        : Cardinal;      // �������� ������� �WAVE� (0x57415645)
    subchunk1Id   : Cardinal;      // �������� ������� �fmt � (0x666d7420)
    subchunk1Size : Integer;       // ���������� ������ ����������, ������� � ���� �������. 16 ��� ������� PCM.
    audioFormat   : Word;          // ����� ������. ��� PCM = 1 (�� ����, �������� �����������). ��������, ������������ �� 1, ���������� ��������� ������ ������.
                                   // ������ ������ ����� �������� ����� [url]http://audiocoding.ru/wav_formats.txt[/url]
    numChannels   : Word;          // ���������� �������. ���� = 1, ������ = 2 � �.�.
    sampleRate    : Integer;       // ������� �������������. 8000 ��, 44100 �� � �.�.
    byteRate      : Integer;       // ���������� ����, ���������� �� ������� ��������������� (sampleRate * numChannels * bitsPerSample/8)
    blockAlign    : Word;          // ���������� ���� ��� ������ ������, ������� ��� ������ (numChannels * bitsPerSample/8)
    bitsPerSample : Word;          //   ���������� ��� � ������. ��� ���������� ��������� ��� �������� ��������. 8 ���, 16 ��� � �.�.
    subchunk2Id   : Cardinal;      // �������� ������� �data� (0x64617461)
    subchunk2Size : Cardinal;      // ���������� ���� � ������� ������ (numSamples * numChannels * bitsPerSample/8)
   //data          :                // ��������������� WAV-������
  end;

var
  i, j: Integer;
  in_wav: TWave;
  out_wav: TWave;
  FS: TFileStream;
  Out_FS: TFileStream;
  Data: Integer;
  BlockSize: Integer;
  dYs: array of Integer;
  dYs_: array of SmallInt;
  numSamples: Integer;
  Max_: Integer;

begin
  if not FileExists(SrcFN) then Exit;

  FS := TFileStream.Create(SrcFN, fmOpenRead);
  FS.Read(in_wav, SizeOf(in_wav));

  Result:= False;
  if in_wav.bitsPerSample = 32 then
  begin
    BlockSize := in_wav.blockAlign div in_wav.numChannels;
    SetLength(dYs, in_wav.subchunk2Size div in_wav.blockAlign);
    SetLength(dYs_, in_wav.subchunk2Size div in_wav.blockAlign);
    FS.Read(dYs[0], in_wav.subchunk2Size);

    Max_:= 0;
    numSamples:= 0;
    for i := 0 to High(dYs) do
    begin
      Max_:= Max(abs(dYs[i]), Max_);
      numSamples:= numSamples + 1;
    end;
    Max_:= Round(Max_ * 1.1);
    out_wav:= in_wav;
    out_wav.subchunk2Size:= numSamples * 2;      // ���������� ���� � ������� ������ (numSamples * numChannels * bitsPerSample/8)

    for i := 0 to High(dYs) do dYs_[i]:= Round((dYs[i] / Max_) * 32767);

    Out_FS:= TFileStream.Create(DestFN, fmCreate);
    Out_FS.Write(out_wav, SizeOf(out_wav));
    Out_FS.Write(dYs_[0], out_wav.subchunk2Size);
    Out_FS.Position:= 0;

    out_wav.bitsPerSample:= 16;            //   ���������� ��� � ������. ��� ���������� ��������� ��� �������� ��������. 8 ���, 16 ��� � �.�.
    out_wav.byteRate      := out_wav.sampleRate * 2; // ���������� ����, ���������� �� ������� ��������������� (sampleRate * numChannels * bitsPerSample/8)
    out_wav.blockAlign    := 2;          // ���������� ���� ��� ������ ������, ������� ��� ������ (numChannels * bitsPerSample/8)
    out_wav.chunkSize:= Out_FS.Size - 8;

    Out_FS.Write(out_wav, SizeOf(out_wav));
    Out_FS.Free;
    Result:= True;
  end;
  FS.Free;
end;


begin
  for I := 0 to High(FMediaList) do
    if FMediaList[I].EventIdx = EventIdx then
    begin
      FMediaList[I].Form.BringToFront;
      Exit;
    end;

  cap_str:=  ' / ' + ExtractFileName(FDS.FileName) + ' / ' + RealCrdToStr(FDS.DisToRealCoord(FDS.Event[EventIdx].DisCoord));
  if (MediaType = mtVideo) then
  begin
    fn:= DataFileDir + 'Video-' + IntToStr(EventIdx) + '.avi';
    Data.SaveToFile(fn);
    VideoForm:= TVideoView.Create(MainForm);
    VideoForm.Open_and_Play(fn, OnCloseMediaForm);
    VideoForm.Caption:= LangTable.Caption['EventsName:VideoMark'] + cap_str; // �����
    SetLength(FMediaList, Length(FMediaList) + 1);
    FMediaList[High(FMediaList)].Form:= VideoForm;
    FMediaList[High(FMediaList)].EventIdx:= EventIdx;
  end
  else
  if (MediaType = mtPhoto) then
  begin
    fn:= DataFileDir + 'Photo-' + IntToStr(EventIdx) + '.png';
    Data.SaveToFile(fn);
    PictureForm:= TPictureView.Create(MainForm);
    PictureForm.Open_and_Play(fn, OnCloseMediaForm);
    PictureForm.Caption:= LangTable.Caption['EventsName:PhotoMark'] + cap_str; // ����

    SetLength(FMediaList, Length(FMediaList) + 1);
    FMediaList[High(FMediaList)].Form:= PictureForm;
    FMediaList[High(FMediaList)].EventIdx:= EventIdx;
  end
  else
  if (MediaType = mtAudio) then
  begin
    fn:= DataFileDir + 'Audio-' + IntToStr(EventIdx) + '.wav';
    Data.SaveToFile(fn);
//    if ResampleWave(fn, fn + '.wav') then fn:= fn + '.wav';
    if ResampleWave(fn, DataFileDir + 'Audio-' + IntToStr(EventIdx) + '-resample.wav') then fn:= DataFileDir + 'Audio-' + IntToStr(EventIdx) + '-resample.wav';
    AudioView:= TAudioView.Create(MainForm);
    AudioView.Open_and_Play(fn, OnCloseMediaForm);
    AudioView.Caption:= LangTable.Caption['EventsName:AudioMark'] + cap_str;

    SetLength(FMediaList, Length(FMediaList) + 1);
    FMediaList[High(FMediaList)].Form:= AudioView;
    FMediaList[High(FMediaList)].EventIdx:= EventIdx;
  end;
end;

procedure TAvk11ViewForm.OnCloseMediaForm(Sender: TObject);
var
  I: Integer;

begin
  for I := 0 to High(FMediaList) do
    if FMediaList[I].Form = Sender then
    begin
      FMediaList[I].Form:= nil;
      FMediaList[I].EventIdx:= - 1;
    end;
end;

procedure TAvk11ViewForm.SelNotebook(Sender: TObject);
begin
  MainForm.OpenNotebook(Integer(Sender));
end;

procedure TAvk11ViewForm.SetInfoBarForm(NewForm: TInfoBarForm);
begin
  FInfoBarForm:= NewForm;
end;

procedure TAvk11ViewForm.SetInfoBarForm2(NewForm: TInfoBarForm2);
begin
  FInfoBarForm2:= NewForm;
end;

procedure TAvk11ViewForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  I: Integer;
//Index: Integer;
//  FileName: string;

begin
//Index:= 0;
{  if Assigned(FDS) then
    while True do
    begin
      if Index = 0 then FileName:= FDS.FileName + '.csv'
                   else FileName:= FDS.FileName + '(' + IntToStr(Index) + ').csv';
      if not FileExists(FileName) then
      begin }
//        FNordcoCSVFile.SaveToFile(FDS.FileName + '.csv');
{        Break;
      end;
      Index:= Index + 1;
    end;
 }
  if Assigned(FInfoBarForm) then FInfoBarForm.SetControl(nil, nil);
  if Assigned(FInfoBarForm2) then FInfoBarForm2.SetControl(nil, nil);

  for I:= MainForm.MDIChildCount - 1 downto 0 do
    if (MainForm.MDIChildren[I] is THandScanForm) and
       (THandScanForm(MainForm.MDIChildren[I]).Tag = Integer(Self)) then
      THandScanForm(MainForm.MDIChildren[I]).Close;

{$IFDEF VIEWBYKM}
  for I:= MainForm.MDIChildCount - 1 downto 0 do
    if (MainForm.MDIChildren[I] is TDBConnectForm) and
       (TDBConnectForm(MainForm.MDIChildren[I]).Tag = Integer(Self)) then
      TDBConnectForm(MainForm.MDIChildren[I]).Close;
      {$ENDIF}

  Button4.Visible:= False;
  Button3.Visible:= False;

  MainForm.ExcludeAssign(Self);
{
  if Assigned(NoteBookForm) then
  begin
    NoteBookForm.IamDead:= True;
    NoteBookForm.Close;
    NoteBookForm:= nil;
  end;
}
  {$IFDEF REC}
  MainForm.RecInt.MyClose(Self);
  {$ENDIF REC}

  FDisplay.Free;
  FDisplay:= nil;

  FDS.Free;
  FDS:= nil;

  DisplayModeToControl(nil);

  if DeleteViewedFile then DeleteFile(FFileName);

  if Assigned(FHandSForm_) then
  begin
    THandScanForm(FHandSForm_).Free;
    FHandSForm_:= nil;
  end;

  if Assigned(FHeadSForm_) then
  begin
    THandScanForm(FHeadSForm_).Free;
    FHeadSForm_:= nil;
  end;


  if not MainForm.MainFormClose then
    for I:= 0 to High(FMediaList) do
      if Assigned(FMediaList[I].Form) then
      begin
        FMediaList[I].Form.Tag:= 1;
        FMediaList[I].Form.Close;
        FMediaList[I].Form.Free;
      end;
{
  if Assigned(EVideoForm) and (EVideoForm.ViewForm = self) then
  begin
    EVideoForm.Close;
    EVideoForm.Free;
    EVideoForm:= nil;
  end;               }


  SetLength(FMediaList, 0);
  Self.Release;
end;

procedure TAvk11ViewForm.ListBox1Click(Sender: TObject);
var
  Index: Integer;
  Avk11View: TAvk11ViewForm;

begin
  if ListBox1.ItemIndex = - 1 then Exit;

  Avk11View:= MainForm.ActiveViewForm_;
  Index:= Integer(ListBox1.Items.Objects[ListBox1.ItemIndex]);
  if Index = 10000 then
  begin
    if Assigned(FHandSForm_) then
    begin
      THandScanForm(FHandSForm_).Free;
      FHandSForm_:= nil;
    end;
 {  if Assigned(FHeadSForm_) then
    begin
      THandScanForm(FHeadSForm_).Free;
      FHeadSForm_:= nil;
    end;  }

    FileInfoMemo.Visible:= True;
    FileInfoMemo.BringToFront;
  end
  else
  if Index < 1000 then
  begin
    FileInfoMemo.Visible:= False;

    if Assigned(FHandSForm_) then
    begin
      THandScanForm(FHandSForm_).Free;
      FHandSForm_:= nil;
    end;
 {  if Assigned(FHeadSForm_) then
    begin
      THandScanForm(FHeadSForm_).Free;
      FHeadSForm_:= nil;
    end;   }

    FHandSForm_:= THandScanForm.Create(Panel3);
    with THandScanForm(FHandSForm_) do
    begin
      FormStyle:= fsNormal;
      Parent:= Panel3;
      BorderStyle:= bsNone;
      Align:= alClient;
      SetHSData(Avk11View.DatSrc.HSList[Index], MainForm.InfoBarForm, ExtractFileName(Avk11View.DatSrc.FileName), Avk11View.DatSrc, Index);
      Tag:= Integer(Self);
      ScrollBar1.Tag:= Index;
    end;
  end
  else
  begin
    FileInfoMemo.Visible:= False;
    if Assigned(FHandSForm_) then
    begin
      THandScanForm(FHandSForm_).Free;
      FHandSForm_:= nil;
    end;
    Index:= Index - 1000;
    Image1.BringToFront;
    Image1.Picture.Assign(Avk11View.DatSrc.HSAScanList[Index].Image);

    with Avk11View.DatSrc.HSAScanList[Index] do
    begin
      Memo1.Lines.Clear;
      if Rail = rLeft then Memo1.Lines.Add(LangTable.Caption['Common:Rail'] + ': ' + LangTable.Caption['Common:Left'])
                      else Memo1.Lines.Add(LangTable.Caption['Common:Rail'] + ': ' + LangTable.Caption['Common:Right']);

//      Memo1.Lines.Add(Format(LangTable.Caption['Common:Channel'] + ': %d� %s', [FDS.Config.HandChannelByIdx[Channel].Angle, LangTable.Caption['Common:' + GetInspMethodTextId(FDS.Config.HandChannelByIdx[Channel].Method, False)]]));
      Memo1.Lines.Add(Format(LangTable.Caption['HandScan:Surface'] + ': %d', [ScanSurface]));
      Memo1.Lines.Add(Format(LangTable.Caption['Common:ChannelParams_SHORT:Att'] + ': %d ' + LangTable.Caption['Common:dB'], [Att]));
      Memo1.Lines.Add(Format(LangTable.Caption['Common:ChannelParams_SHORT:Ku'] + ': %d ' + LangTable.Caption['Common:dB'], [Ku]));
      Memo1.Lines.Add(Format(LangTable.Caption['Common:ChannelParams_SHORT:TVG'] + ': %d ' + LangTable.Caption['Common:us'], [TVG]));
      Memo1.Lines.Add(Format(LangTable.Caption['Common:ChannelParams_SHORT:PrismDelay'] + ': %3.1f ' + LangTable.Caption['Common:us'], [TimeDelay / 10]));
      Memo1.Lines.Add(Format(LangTable.Caption['Start gate'] + ': %d ' + LangTable.Caption['Common:us'], [StGate])); // ������ ������ ���, ���
      Memo1.Lines.Add(Format(LangTable.Caption['End gate'] + ': %d ' + LangTable.Caption['Common:us'], [EdGate])); // ����� ������ ���, ���

    end;
  end;
end;

function TAvk11ViewForm.LoadFile(FileName: string): Boolean;
var
  I, J: Integer;
  DC: TDisplayConfig;

begin
  FFileName:= FileName;
  Self.Caption:= FileName;

  Result:= FDS.LoadFromFile(FileName, False);
  if not Result then
  begin
    Result:= False;
    Exit;
  end;
  FDisplay.ApplyDisplayMode;
  FDisplay.SetDisplayColors(Config.ActivDColors);

{
  if not Result then
  begin
    ShowMessage(FDS.Log.Text);
    Self.Close;
    Exit;
  end;

  if (((FDS.Header.BUIVer1 = 3) and (FDS.Header.BUIVer2 >= 7)) or (FDS.Header.BUIVer1 > 3)) then
    if not FDS.BodyModify then
      if FDS.FixBags then
      begin
        FDS.BodyModify:= True;
        FDS.SkipTestDateTime:= True;
        FDS.Free;
        FDS:= TAvk11DatSrc.Create;
        Result:= FDS.LoadFromFile(FileName);
        FDS.ReAnalyze;
        FDisplay.SetData(Self, ScrollBar1, FDS);
      end;
}
{  case FDS.DataRail of
    r_Left: FDisplay.ViewMode:= ddLeftRail;
   r_Right: FDisplay.ViewMode:= ddRightRail;
    r_Both: FDisplay.ViewMode:= ddBothRail;
  end; }

//  FDisplay.LineCoord:= FDS.RSPFile;
//  FInfoBarForm.CoordMode:= FDS.RSPFile;

  FInfoBarForm.Repaint;
  FDisplay.FullRefresh;
//  FDisplay.Refresh;
  FormActivate(nil);

  Display.SetScrollBar(ScrollBar1);
(*
  if Display.DatSrc.Header.Reserved = 3 then
  begin
    DC:= Display.DisplayConfig;

    DC.BScanViewZone[4].EndDelay:= 184;
    DC.BScanViewZone[4].Dur:= DC.BScanViewZone[4].EndDelay - DC.BScanViewZone[4].BegDelay;

    Display.DisplayConfig:= DC;
  end;
  *)

  if FDS.Header.UsedItems[uiHandScanFile] <> 0 then
  begin
    FFormMode:= fmSingleChannel;

    FDisplay.Visible:= False;
    Button3.Visible:= False;
    Button4.Visible:= False;
    ScrollBar1.Visible := False;
    FilusX111W_HandScan.Visible := True;
    FilusX111W_HandScan.Align:= alClient;
    FillFileInfoText(FDS, FileInfoMemo);

    J:= 1;
    ListBox1.Items.AddObject(Format('%d. Information', [J]), Pointer(10000));
    Inc(J);

    for I := 0 to Length(FDS.HSList) - 1 do
    begin
      ListBox1.Items.AddObject(Format('%d. B-Scan', [J, FDS.HSList[I].Header.HandChNum]), Pointer(I));
      Inc(J);
    end;

    for I := 0 to Length(FDS.HSAScanList) - 1 do
    begin
      ListBox1.Items.AddObject(Format('%d. A-Scan', [J, FDS.HSAScanList[I].Channel]), Pointer(I + 1000));
      Inc(J);
    end;

    ListBox1.ItemIndex:= 0;
    FormActivate(nil);
  end else FFormMode:= fmMultichanel;
end;

procedure TAvk11ViewForm.FileDateToControl;
begin
  DisplayModeToControl(Self);
end;

procedure TAvk11ViewForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
var
  I: Integer;
  State: TKeyboardState;

begin

 // self.Caption:= IntToStr(Msg.CharCode);

  case Msg.CharCode of
    90: begin
          {$IFDEF AVIKON11}
          Display.SwitchReductionMode;
          {$ENDIF}
        end;
    88: begin
        { if FDS.DelayKoeff = 1 then FDS.DelayKoeff:= 3
                                else FDS.DelayKoeff:= 1;
          Display.Refresh;
          Handled:= True; }
        end;
    38: begin
          Display.NextZoom;
          Handled:= True;
        end;
    40: begin
          Display.PrevZoom;
          Handled:= True;
        end;
    37: begin
          if ShiftDown then Display.EasyLeft else Display.StepLeft;
          Handled:= True;
          MainForm.ListViewFlag:= False;
        end;
    39: begin
          if ShiftDown then Display.EasyRight else Display.StepRight;
          Handled:= True;
          MainForm.ListViewFlag:= False;
        end;
    34: begin
          Display.PageLeft;
          Handled:= True;
          MainForm.ListViewFlag:= False;
        end;
    33: begin
          Display.PageRight;
          Handled:= True;
          MainForm.ListViewFlag:= False;
        end;
   109: begin // -
          Timer1.Enabled:= True;
          FSpeed:= FSpeed - 1;
          if FSpeed < Low(SpeedList) then FSpeed:= Low(SpeedList);
        end;
   106: begin // *
          Timer1.Enabled:= True;
          FSpeed:= FSpeed * - 1;
        end;
   107: begin // +
          Timer1.Enabled:= True;
          FSpeed:= FSpeed + 1;
          if FSpeed > High(SpeedList) then FSpeed:= High(SpeedList);
        end;
    13: begin // Enter
          Timer1.Enabled:= not Timer1.Enabled;
          Timer1.Tag:= - 1;
        end;
    32: begin
          if not FFullScreenMode then
          begin
            FFullScreenMode:= True;
            MainForm.TBDock1.Visible:= False;
            MainForm.TBDock3.Visible:= False;
          end
          else
          begin
            FFullScreenMode:= False;
            MainForm.TBDock1.Visible:= True;
            MainForm.TBDock3.Visible:= True;
          end;
        end;
  end;

//  Self.Caption:= Format('������ �� ������: %3.1f; ����� �-���������: %d', [Display.DrawWidth_, Display.PixelCount_]);



  if Msg.CharCode <> 16 then
  begin
    FInfoBarForm.RefreshDat(FOldX, FOldY);
    FInfoBarForm2.RefreshDat(FOldX, FOldY);
    MainForm.DisplayModeToControl(Self);
  end;
end;

procedure TAvk11ViewForm.SetProgressBarPosition(Position: Integer; Hide: Boolean);
begin
	ProgressBarHide := Hide;
 	ProgressBarPosition := Position;
end;

procedure TAvk11ViewForm.SetProgressBarCaption(caption: String);
begin
  ProgressBarCaption := caption;
end;

procedure TAvk11ViewForm.UpdateProgressBar;
begin
  if not Assigned(AnalyzeProgressBarPanel) then
  begin
    exit;
  end;
  if ProgressBarCaption <> AnalyzeProgressBarPanel.Caption then
  begin
     AnalyzeProgressBarPanel.Caption := ProgressBarCaption;
     AnalyzeProgressBarPanel.Refresh;
  end;

	if ProgressBarHide then
  begin
  //	ProgressBar.Visible := False;
    AnalyzeProgressBarPanel.Visible := False;
    Self.FormResize(nil);
  end
  else
  begin
  	if not AnalyzeProgressBarPanel.Visible then
    begin
	  //	ProgressBar.Visible := True;
      AnalyzeProgressBarPanel.Visible := True;
      Self.FormResize(nil);
      Self.Refresh;
    end;
    ProgressBar.Position := ProgressBarPosition;
  end;
end;

procedure TAvk11ViewForm.Button1Click(Sender: TObject);
var
  I: Integer;
  R: TRail;
  Ch: Integer;
  S: string;

begin
// for I := 0 to DatSrc.EventCount - 1 do
(*
  Memo2.Lines.BeginUpdate;
  Memo2.Clear;

    for I := 0 to (Length(DatSrc.AKStateData_) {div 10}) - 1 do
    begin
{      S:= '';
      for R:= rLeft to rRight do
      begin
        if R = rLeft then S:= S + 'L:' else S:= S + ' R:';
        for Ch:= 0 to 15 do
          if DatSrc.ChFileIdxMask[Ch] then
            S:= S + IntToStr(Ord(DatSrc.AKStateData_[I].State[R][Ch] = True));
      end;}
      Memo2.Lines.Add(Format('%d - Rail: %d; Ch: %d; Crd: %d; State: %d', [I, Ord(DatSrc.AKStateData_[I].Rail), DatSrc.AKStateData_[I].Ch, DatSrc.AKStateData_[I].DisCrd, Ord(DatSrc.AKStateData_[I].State)]));
//      Memo2.Lines.Add(Format('%d - Crd: %d ��, State: %d; %s', [I, DatSrc.AKStateData_[I].DisCrd * DatSrc.Header.ScanStep div 100, Ord(DatSrc.AKStateData_[I].SummState), S { State[R, Ch]}]));
    end;

//  R:= rLeft;
//  Ch:= 4;
{  for R:= rLeft to rRight do
    for Ch:= 1 to 16 do
      for I := 0 to Length(DatSrc.AKStateData[R, Ch]) - 1 do
      begin
        Memo2.Lines.Add(Format('%d. Crd: %d ��, State: %d', [I, DatSrc.AKStateData[R, Ch, I].DisCrd * DatSrc.Header.ScanStep div 100, Ord(DatSrc.AKStateData[R, Ch, I].State)]));
        if DatSrc.AKStateData[R, Ch, I].State and (I > 0) then
          Memo2.Lines.Strings[Memo2.Lines.Count - 1]:= Memo2.Lines.Strings[Memo2.Lines.Count - 1] + Format(' DL = %3.1f ��', [(DatSrc.AKStateData[R, Ch, I].DisCrd - DatSrc.AKStateData[R, Ch, I - 1].DisCrd) * DatSrc.Header.ScanStep / 100 ]);
      end;
      }
{
 DatSrc.AKStateData[R, Ch, I].
 if DatSrc.AKStateData
 DatSrc.AKStateData_
}
//  DatSrc.get

//  Memo2.Clear;
//  for I := 0 to High(DatSrc.AKStateData_) do
//    Memo2.Lines.Add(Format('SysCrd: %d, State: %d', [ DatSrc.AKStateData_[I].DisCrd, Ord(DatSrc.AKStateData_[I]. SummState)]));


  // Memo2.Lines.EndUpdate;
                 *)
end;

procedure TAvk11ViewForm.DDMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if Active then
  begin
    FInfoBarForm.RefreshDat(X, Y);
    FInfoBarForm2.RefreshDat(X, Y);
  end;
  FOldX:= X;
  FOldY:= Y;
end;

procedure TAvk11ViewForm.FormActivate(Sender: TObject);
begin

  if Assigned(HandSForm_) then// ���� � ����� ����� ������� ����� ������� �� ��� ����� �������
  begin
    if ListBox1.ItemIndex < 1 then MainForm.DisplayModeToControl(nil)
                              else MainForm.DisplayModeToControl(HandSForm_);
    Exit;
  end;


 // MainForm.TestNilLinc;
  FileDateToControl;
  if Assigned(FInfoBarForm) then FInfoBarForm.SetControl(FDS, FDisplay);
  if Assigned(FInfoBarForm2) then FInfoBarForm2.SetControl(FDS, FDisplay);
  MainForm.DisplayModeToControl(Self);
end;

procedure TAvk11ViewForm.MyActivate;
begin
 // MainForm.TestNilLinc;
//  FileDateToControl;
  if Assigned(FInfoBarForm) then FInfoBarForm.SetControl(FDS, FDisplay);
//  MainForm.DisplayModeToControl(Self);
end;

procedure TAvk11ViewForm.DisplayModeToControl(Sender: TObject);
begin
  if Assigned(OnChangeDisplayMode) then OnChangeDisplayMode(Sender);
end;

procedure TAvk11ViewForm.SetSelectVideo(New: Boolean);
begin
  FSetSelectVideo:= New;
  if New then
  begin
    Display.MouseMode:= mmSelRailPoint;
    Display.SelRailPointCount:= 1;
  end  else if Display.SelRailPointCount = 0 then Display.MouseMode:= mmIdle;
end;

procedure TAvk11ViewForm.SetADDtoNoteBook(New: Boolean);
begin
  FADDtoNoteBook:= New;
  if New then
  begin
    Display.MouseMode:= mmSelRailPoint;
    Display.SelRailPointCount:= 1;
  end  else if Display.SelRailPointCount = 0 then Display.MouseMode:= mmIdle;
end;

procedure TAvk11ViewForm.SetADDtoNordCo(New: Boolean);
begin
  FADDtoNordCo:= New;
  if New then
  begin
    Display.MouseMode:= mmSelRailPoint;
    Display.SelRailPointCount:= 2;
  end  else if Display.SelRailPointCount = 0 then Display.MouseMode:= mmIdle;
end;

procedure TAvk11ViewForm.SetSoundFlag(New: Boolean);
begin
  FSoundFlag:= New;
  FDisplay.SoundFlag:= New;
  if New then MainForm.DisplayModeToControl(Self);
end;

procedure TAvk11ViewForm.SetFocus_;
begin
  if Button3.Tag = 0 then
  begin
    Button3.SetFocus;
    Button3.Tag:= 1;
  end
  else
  begin
    Button4.SetFocus;
    Button3.Tag:= 0;
  end;
end;

procedure TAvk11ViewForm.AddToNotebook_(Sender: TObject);
var
  Shift: TMouseButton;

begin
  Shift:= TMouseButton(Integer(Sender));

  if FSetSelectVideo then
  begin
    SelectVideo:= False;
    if Assigned(OnChangeADDtoNoteBook) then OnChangeADDtoNoteBook(Self);

    if (not Assigned(EVideoForm)) or (Assigned(EVideoForm) and (self <> EVideoForm.ViewForm)) then
    begin
      if Assigned(EVideoForm) then EVideoForm.Free;
//      ShowMessage('TEVideoForm.Create');
      EVideoForm:= TEVideoForm.Create(nil);
//      ShowMessage('TEVideoForm.Create - OK');
//      ShowMessage('EVideoForm.Open');
      if not EVideoForm.Open(ExtractFilePath(FFileName), ExtractFileName(FFileName), self) then
      begin
//        ShowMessage('��� ������'); // NOTranslate
        EVideoForm.Close;
        EVideoForm.Free;
        EVideoForm:= nil;
      end;
//      ShowMessage('EVideoForm.Open - OK');
    end
    else
//    ShowMessage('EVideoForm.GetImage');
    EVideoForm.GetImage(Display.MousePosDat.Rail = rLeft, Display.MousePosDat.MouseDisCoord);
//    ShowMessage('EVideoForm.GetImage - OK');

//    procedure SelectSide(UseLeft_: Boolean);
//    procedure SetPosInMs(Pos: LongInt);
  end;

  if FADDtoNoteBook then
  begin
    ADDtoNoteBook:= False;
    if Assigned(OnChangeADDtoNoteBook) then OnChangeADDtoNoteBook(Self);

    if (mbLeft = Shift) and Display.MousePosDat.ok then
    begin
      Display.GetMouseData(FOldX, FOldY);
      NewEntryForm:= TNewEntryForm.Create(nil);

      NewEntryForm.Prepare(Self, Display.MousePosDat.MouseDisCoord, Display.DatSrc.RailToPathRail(Display.MousePosDat.Rail) {, FDS.GetRailPathNumber_in_String});
      NewEntryForm.ShowModal;
      NewEntryForm.Free;

      Display.Refresh;

    end;
  end;

  if FADDtoNordCo and (Display.SelRailPointCount = 1) then
  begin
//    ShowMessage('St');
    FNordCoMouseDisCoord:= Display.MousePosDat.MouseDisCoord;
  end;

  if FADDtoNordCo and (Display.SelRailPointCount = 0) then
  begin
    FADDtoNordCo:= False;
    if Assigned(OnChangeADDtoNoteBook) then OnChangeADDtoNoteBook(Self);

    if (mbLeft = Shift) and Display.MousePosDat.ok then
    begin
      Display.GetMouseData(FOldX, FOldY);
      NewEntryForm:= TNewEntryForm.Create(nil);

      NewEntryForm.PrepareNordCo(Self, FNordCoMouseDisCoord, Display.MousePosDat.MouseDisCoord, Display.MousePosDat.Rail {, FDS.GetRailPathNumber_in_String});
      NewEntryForm.ShowModal;
      NewEntryForm.Free;

      Display.Refresh;

    end;
  end;


  if FSoundFlag then
  begin
    SoundFlag:= False;
    if Assigned(OnChangeSoundFlag) then OnChangeSoundFlag(Self);
  end;
end;

procedure TAvk11ViewForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = 88 then Display.ViewLabels:= False;
end;

procedure TAvk11ViewForm.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = 88 then Display.ViewLabels:= True;
end;

procedure TAvk11ViewForm.FormMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
var
  Step: Integer;

begin
  Step:= Round(Display.BScanWidth * Config.WheelStep * 10 / 100 * Display.Zoom / Display.DatSrc.Header.ScanStep);
  if Config.WheelDir then ScrollBar1.Position:= ScrollBar1.Position - Step
                     else ScrollBar1.Position:= ScrollBar1.Position + Step;
  Handled:= True;
end;

procedure TAvk11ViewForm.FormMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
var
  Step: Integer;

begin
  Step:= Round(Display.BScanWidth * Config.WheelStep * 10 / 100 * Display.Zoom / Display.DatSrc.Header.ScanStep);
  if Config.WheelDir then ScrollBar1.Position:= ScrollBar1.Position + Step
                     else ScrollBar1.Position:= ScrollBar1.Position - Step;
  Handled:= True;
end;

procedure TAvk11ViewForm.FormResize(Sender: TObject);
begin
//  Self.Caption:= Format('WW: %d - DF: %d', [Self.ClientWidth, FDisplay.SaveWidth]);
  if Assigned(FDisplay) then FDisplay.FullRefresh;
end;

procedure TAvk11ViewForm.Timer1Timer(Sender: TObject);
var
  T: Integer;
  DC: Integer;

begin
  T:= GetTickCount;
  if Timer1.Tag <> - 1 then
  begin
    FGShift:= FGShift + SpeedList[FSpeed] * (T - Timer1.Tag) / 1000 * (FDisplay.Zoom / 100) / (FDS.Header.ScanStep / 100);
    DC:= Trunc(FGShift);
    FGShift:= FGShift - DC;
    if FDisplay.CenterDisCoord < 0 then
    begin
      FGShift:= 0;
      FDisplay.CenterDisCoord:= 0;
    end
    else
    if FDisplay.CenterDisCoord > FDS.MaxDisCoord then
    begin
      FGShift:= 0;
      FDisplay.CenterDisCoord:= FDS.MaxDisCoord;
    end
    else FDisplay.CenterDisCoord:= Min(FDisplay.CenterDisCoord + DC, FDS.MaxDisCoord);

//    self.Caption:= format('%d', [SpeedList[FSpeed]]);
  end;
  Timer1.Tag:= T;
end;
{
procedure TAvk11ViewForm.StopWork(Sender: TObject);
begin
  MainForm.Timer2.Enabled:= True;
end;
}
procedure TAvk11ViewForm.FormDeactivate(Sender: TObject);
begin
  if Assigned(FInfoBarForm) then FInfoBarForm.SetControl(nil, nil);
end;

procedure TAvk11ViewForm.FormDestroy(Sender: TObject);
begin
  SetLength(FMediaList, 0);
end;

procedure TAvk11ViewForm.ShowRecStatistic(Big: Boolean);
{
var
  Total1: Integer;
  Total2: Integer;
  a: TRecObjectType;
  S: string;
  AA: TBigForm;
 // Txt: TStringL
}
begin
{
  with RecInfo do
  begin
    Total1:= 0;
    Total2:= 0;
    for a:= rotBJHead to rotBJBottom do Total1:= Total1 + ObjCount[a];
    for a:= rotFreeHead to rotFreeBottom do Total2:= Total2 + ObjCount[a];

    if BIG then
    begin

      with TBigForm.Create(nil) do
      begin
        Memo1.Clear;
        Memo1.SelAttributes.Size:= 12;
//        Memo1.Lines.Add(' ');
        Memo1.Lines.Add(' ����: ' + Copy(ExtractFileName(DatSrc.FileName), 1, 58));
        Memo1.Lines.Add(Format(' �������������: %3.1f ��', [FileLen_Km]));
        Memo1.Lines.Add(' ');
        Memo1.Lines.Add(' ������������ �������:');
        Memo1.SelAttributes.Color:= clGreen;
        Memo1.Lines.Add(Format('   �������� �����: %d ��', [RecInfo.ObjCount[rotBoltJoint]]));
        Memo1.SelAttributes.Color:= clGreen;
        Memo1.Lines.Add(Format('   ���������� ��������: %d ��', [RecInfo.ObjCount[rotSwitch] div 2]));
        Memo1.SelAttributes.Color:= clRed;
        Memo1.Lines.Add(Format('   ������� ��� �������: %d �� (%3.1f �� �� ��)', [Total1 + Total2, (Total1 + Total2) / (DatSrc.MaxDisCoord * DatSrc.Header.ScanStep / 100 / 1000000)]));
        Memo1.SelAttributes.Color:= clRed;
        Memo1.SelAttributes.Size:= 11;
        Memo1.Lines.Add(Format('     � ���� ��������� �����: %d �� (%3.1f �� �� ��)', [Total1, Total1 / (DatSrc.MaxDisCoord * DatSrc.Header.ScanStep / 100 / 1000000)]));
        Memo1.SelAttributes.Color:= clRed;
        Memo1.SelAttributes.Size:= 11;
        Memo1.Lines.Add(Format('       - � �������: %d ��', [ObjCount[rotBJHead]]));
        Memo1.SelAttributes.Color:= clRed;
        Memo1.SelAttributes.Size:= 11;
        Memo1.Lines.Add(Format('       - � �����: %d ��', [ObjCount[rotBJBody]]));
        Memo1.SelAttributes.Color:= clRed;
        Memo1.SelAttributes.Size:= 11;
        Memo1.Lines.Add(Format('       - � �������: %d ��', [ObjCount[rotBJBottom]]));
        Memo1.SelAttributes.Color:= clRed;
        Memo1.SelAttributes.Size:= 11;
        Memo1.Lines.Add(Format('     � ���� ��������� ������� ������: %d �� (%3.1f �� �� ��)', [Total2, Total2 / (DatSrc.MaxDisCoord * DatSrc.Header.ScanStep / 100 / 1000000) ]));
        Memo1.SelAttributes.Color:= clRed;
        Memo1.SelAttributes.Size:= 11;
        Memo1.Lines.Add(Format('       - � �������: %d ��', [ObjCount[rotFreeHead]]));
        Memo1.SelAttributes.Color:= clRed;
        Memo1.SelAttributes.Size:= 11;
        Memo1.Lines.Add(Format('       - � �����: %d ��', [ObjCount[rotFreeBody]]));
        Memo1.SelAttributes.Color:= clRed;
        Memo1.SelAttributes.Size:= 11;
        Memo1.Lines.Add(Format('       - � �������: %d ��', [ObjCount[rotFreeBottom]]));
        Memo1.SelAttributes.Color:= clRed;
        Memo1.SelAttributes.Size:= 12;
        Memo1.Lines.Add(Format('   �� ������������������� ��������: %d �� (����. %d �)', [ObjCount[rotUnCtrlZon], UnCtrlZonLen]));
        Memo1.Lines.Add(' ');
        Memo1.SelAttributes.Size:= 10;
        Memo1.Lines.Add(Format(' ����� �������: %3d ��� %3d ���', [Trunc(WorkTime_Min), Round(Frac(WorkTime_Min) * 60)]));
        Memo1.SelAttributes.Size:= 10;
        Memo1.Lines.Add(Format(' �������� �������: %3.1f ��/���', [(DatSrc.MaxDisCoord * DatSrc.Header.ScanStep / 100 / 1000000) / (WorkTime_Min / 60)]));
        Start(S);
        Release;
      end;

    end
    else
    begin
      S:= ' ����: ' + ExtractFileName(DatSrc.FileName) + #10   +
          Format(' �������������: %3.1f ��' + #10  , [FileLen_Km]) +
          Format(' ����� �������: %3d ��� %3d ���' + #10  , [Trunc(WorkTime_Min), Round(Frac(WorkTime_Min) * 60)]) +
                 '------------------------------------------------' + #10   +
                 ' ������������ �������:' + #10   +
          Format('   � ���� ��������� �����: %d �� (%3.1f �� �� ��)' + #10  , [Total1, Total1 / (DatSrc.MaxDisCoord * DatSrc.Header.ScanStep / 100 / 1000000)]) +
          Format('    - � �������: %d ��' + #10  , [ObjCount[rotBJHead]]) +
          Format('    - � �����: %d ��' + #10  , [ObjCount[rotBJBody]]) +
          Format('    - � �������: %d ��' + #10  , [ObjCount[rotBJBottom]]) +
          Format('   � ���� ��������� ������� ������: %d �� (%3.1f �� �� ��)' + #10  , [Total2, Total2 / (DatSrc.MaxDisCoord * DatSrc.Header.ScanStep / 100 / 1000000) ]) +
          Format('    - � �������: %d ��' + #10  , [ObjCount[rotFreeHead]]) +
          Format('    - � �����: %d ��' + #10  , [ObjCount[rotFreeBody]]) +
          Format('    - � �������: %d ��' + #10  , [ObjCount[rotFreeBottom]]) +
          Format('�� ������������������� ��������: %d ��' + #10  , [ObjCount[rotUnCtrlZon]]) +
          Format('�������� ������: %d ��' + #10  , [RecInfo.ObjCount[rotBoltJoint]]) +
          Format('���������� ���������: %d ��' + #10  , [RecInfo.ObjCount[rotSwitch] div 2]);

      ShowMessage(S);
    end;
  end;
  }
end;

procedure TAvk11ViewForm.ShowGPSPath;
{type
  TShowPathOnMap = function(form : TForm; list: TList) : integer; stdcall;

var
  I: Integer;
  pData: PEventData;
  ID: Byte;
  count : integer;

  sLat: Single;
  sLon: Single;
  dwLat: DWORD;
  dwLon: DWORD;

  sLat_: Single;
  sLon_: Single;
  list : TList;  }

begin      {
  list := TList.Create;
  for I := 0 to FDS.EventCount do
  begin
   if (FDS.Event[I].ID = EID_Stolb) then
    begin
      inc(count);
      list.Add(TMarkedPoint.Create(InvertGPSSingle(pGPSCoord(pData)^.Lat) * 180 / pi, InvertGPSSingle(pGPSCoord(pData)^.Lon) * 180 / pi, '������� ���������� ' + IntToStr(count)));
    end;

    if (FDS.Event[I].ID = EID_GPSCoord) or (FDS.Event[I].ID = EID_GPSCoord2) then
    begin
      FDS.GetEventData(I, ID, pData);
      sLat:= pedGPSCoord2(pData)^.Lat;
      sLon:= pedGPSCoord2(pData)^.Lon;
      list.Add(TMarkedPoint.Create(sLat, sLon));
    end;
  end;
  ShowPathOnMap(nil, list);
  list.Free;   }
end;


    {
procedure TAvk11ViewForm.AssignTo(View: TAvk11ViewForm);
begin
  FDisplay.AssignTo(View.Display);
end;

procedure TAvk11ViewForm.DeleteAssign(Index: Integer);
begin
  FDisplay.DeleteAssign(Index);
end;

procedure TAvk11ViewForm.DeleteAssign(View: TAvk11ViewForm);
begin
  FDisplay.DeleteAssign(View.Display);
end;

function TAvk11ViewForm.GetAssignCount: Integer;
begin
  Result:= FDisplay.AssignCount;
end;

function TAvk11ViewForm.GetAssignItem(Index: Integer): TAvk11ViewForm;
begin
  Result:= TAvk11ViewForm(FDisplay.AssignItem[Index].Owner);
end;
}

end.



{
    FDS.HSAScanList[I].
      X:= 40;
      Y:= 20;
      Brush.Style:= bsClear;
      if HSData.Header.Rail = rLeft then S:= LangTable.Caption['Common:Rail'] + ': ' + LangTable.Caption['Common:Left']
                                    else S:= LangTable.Caption['Common:Rail'] + ': ' + LangTable.Caption['Common:Right'];
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

      // Number

      //S:= Format(LangTable.Caption['Common:Channel'] + ': %d', [HSData.Header.HandChNum]);
      S:= Format(LangTable.Caption['Common:Channel'] + ': %d� %s', [FDS.Config.HandChannelByIdx[HSData.Header.HandChNum].Angle, LangTable.Caption['Common:' + GetInspMethodTextId(FDS.Config.HandChannelByIdx[HSData.Header.HandChNum].Method, False)]]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;


      S:= Format(LangTable.Caption['HandScan:Surface'] + ': %d', [HSData.Header.ScanSurface]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

      S:= Format(LangTable.Caption['Common:ChannelParams_SHORT:Att'] + ': %d ' + LangTable.Caption['Common:dB'], [HSData.Header.Att]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;


      S:= Format(LangTable.Caption['Common:ChannelParams_SHORT:Ku'] + ': %d ' + LangTable.Caption['Common:dB'], [HSData.Header.Ku]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

      S:= Format(LangTable.Caption['Common:ChannelParams_SHORT:TVG'] + ': %d ' + LangTable.Caption['Common:us'], [HSData.Header.TVG]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

      if HSData.PrismDelayFlag then
      begin
        S:= Format(LangTable.Caption['Common:ChannelParams_SHORT:PrismDelay'] + ': %3.1f ' + LangTable.Caption['Common:us'], [HSData.PrismDelay / 10]);
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;
      end;

      S:= Format(LangTable.Caption['HandScan:ScanTime'] + ': %d ���', [Round(Length(HSData.Samples) * 5 / 1000)]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

}
