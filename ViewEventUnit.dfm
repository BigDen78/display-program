object ViewEventForm: TViewEventForm
  Left = 254
  Top = 413
  BorderStyle = bsSizeToolWin
  Caption = #1057#1086#1073#1099#1090#1080#1103' '#1092#1072#1081#1083#1072
  ClientHeight = 406
  ClientWidth = 830
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShortCut = FormShortCut
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 387
    Width = 830
    Height = 19
    Panels = <>
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 830
    Height = 387
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = #1057#1087#1080#1089#1086#1082' '#1089#1086#1073#1099#1090#1080#1081' '#1092#1072#1081#1083#1072
      object ListView1: TListView
        Left = 0
        Top = 0
        Width = 822
        Height = 359
        Align = alClient
        Columns = <
          item
            Caption = 'No'
          end
          item
            Caption = 'ID'
            Width = 55
          end
          item
            Caption = 'Offset'
            Width = 95
          end
          item
            Caption = #1057#1086#1073#1099#1090#1080#1077
            Width = 320
          end
          item
            Caption = 'Param'
            Width = 150
          end
          item
            Caption = 'SysCoord'
            Width = 70
          end
          item
            Caption = 'DisCoord'
            Width = 70
          end
          item
            Caption = 'BS Delay'
            Width = 100
          end
          item
            Caption = 'BS Ampl'
            Width = 100
          end
          item
            Caption = 'D1'
          end
          item
            Caption = 'D2'
          end>
        ReadOnly = True
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        OnDblClick = ListView1DblClick
      end
    end
    object TabSheet4: TTabSheet
      Caption = #1042#1099#1073#1086#1088' '#1086#1090#1086#1073#1088#1072#1078#1072#1077#1084#1099#1093' '#1089#1086#1073#1099#1090#1080#1081
      ImageIndex = 3
      object CheckListBox1: TCheckListBox
        Left = 0
        Top = 41
        Width = 822
        Height = 318
        OnClickCheck = CheckListBox1ClickCheck
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 822
        Height = 41
        Align = alTop
        TabOrder = 1
        object Button2: TButton
          Left = 3
          Top = 7
          Width = 89
          Height = 25
          Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1077
          TabOrder = 0
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 107
          Top = 7
          Width = 89
          Height = 25
          Caption = #1057#1085#1103#1090#1100' '#1074#1089#1077
          TabOrder = 1
          OnClick = Button3Click
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1057#1086#1073#1099#1090#1080#1103' '#1092#1072#1081#1083#1072
      ImageIndex = 1
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 822
        Height = 359
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      TabVisible = False
      object Chart1: TChart
        Left = 0
        Top = 109
        Width = 822
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        View3D = False
        View3DWalls = False
        Align = alBottom
        TabOrder = 0
        Visible = False
        object Series1: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series2: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 822
        Height = 33
        Align = alTop
        TabOrder = 1
        Visible = False
        object Button1: TButton
          Left = 4
          Top = 4
          Width = 75
          Height = 25
          Caption = 'Test'
          TabOrder = 0
          OnClick = Button1Click
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = #1057#1082#1086#1088#1086#1089#1090#1100
      ImageIndex = 4
      object Chart2: TChart
        Left = 0
        Top = 0
        Width = 822
        Height = 359
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        View3D = False
        Align = alClient
        TabOrder = 0
        object Series3: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          LinePen.Width = 3
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series4: TPointSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
  end
end
