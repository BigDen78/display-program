object HeadScanForm: THeadScanForm
  Left = 249
  Top = 127
  Caption = #1056#1091#1095#1085#1086#1077' '#1089#1082#1072#1085#1080#1088#1086#1074#1072#1085#1080#1077
  ClientHeight = 577
  ClientWidth = 1123
  Color = clBtnFace
  Constraints.MinHeight = 350
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnPaint = PaintBoxPaint
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Button3: TButton
    Left = 12
    Top = 17
    Width = 0
    Height = 9
    Caption = 'Button3'
    TabOrder = 0
  end
  object Button4: TButton
    Left = 12
    Top = 9
    Width = 0
    Height = 9
    Caption = 'Button4'
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1123
    Height = 577
    Align = alClient
    BevelOuter = bvNone
    FullRepaint = False
    TabOrder = 2
    OnResize = Panel1Resize
    object HeadInfoPaintBox: TPaintBox
      Left = 0
      Top = 0
      Width = 330
      Height = 577
      Align = alLeft
      ExplicitLeft = -6
    end
    object ScrollBox1: TScrollBox
      Left = 330
      Top = 0
      Width = 793
      Height = 577
      HorzScrollBar.Tracking = True
      VertScrollBar.Tracking = True
      Align = alClient
      TabOrder = 0
      OnResize = ScrollBox1Resize
      object PaintBox1: TPaintBox
        Left = 56
        Top = 17
        Width = 657
        Height = 362
        OnPaint = PaintBox1Paint
      end
    end
  end
end
