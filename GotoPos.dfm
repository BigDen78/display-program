object GotoForm: TGotoForm
  Left = 332
  Top = 393
  BorderStyle = bsToolWindow
  Caption = #1055#1077#1088#1077#1093#1086#1076' '#1085#1072' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1091
  ClientHeight = 127
  ClientWidth = 275
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = NewCoordKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Label13: TLabel
    Left = 8
    Top = 11
    Width = 97
    Height = 13
    Caption = #1053#1086#1074#1072#1103' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1072':'
  end
  object Panel1: TPanel
    Left = 6
    Top = 28
    Width = 262
    Height = 57
    BevelInner = bvLowered
    TabOrder = 3
    object Label1: TLabel
      Left = 136
      Top = 21
      Width = 14
      Height = 13
      Caption = #1082#1084
    end
    object Edit1: TEdit
      Left = 10
      Top = 18
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'Edit1'
      OnChange = Edit1Change
    end
  end
  object Panel2: TPanel
    Left = 6
    Top = 28
    Width = 262
    Height = 57
    BevelInner = bvLowered
    TabOrder = 0
    object Label2: TLabel
      Left = 75
      Top = 12
      Width = 14
      Height = 13
      Caption = #1082#1084
    end
    object Label11: TLabel
      Left = 160
      Top = 12
      Width = 12
      Height = 13
      Caption = #1087#1082
    end
    object Label12: TLabel
      Left = 242
      Top = 12
      Width = 8
      Height = 13
      Caption = #1084
    end
    object cbKm: TComboBox
      Left = 7
      Top = 9
      Width = 64
      Height = 21
      Style = csDropDownList
      DropDownCount = 11
      TabOrder = 0
      OnChange = cbKmChange
      OnKeyPress = NewCoordKeyPress
    end
    object cbPk: TComboBox
      Left = 104
      Top = 9
      Width = 52
      Height = 21
      Style = csDropDownList
      DropDownCount = 11
      TabOrder = 1
      OnChange = cbPkChange
      OnKeyPress = NewCoordKeyPress
    end
    object Edit2: TEdit
      Left = 188
      Top = 9
      Width = 49
      Height = 21
      TabOrder = 2
      Text = '0'
      OnChange = Edit2Change
      OnKeyPress = NewCoordKeyPress
    end
    object Panel6: TPanel
      Left = 7
      Top = 33
      Width = 227
      Height = 22
      BevelOuter = bvNone
      TabOrder = 3
      object Label6: TLabel
        Left = 135
        Top = 0
        Width = 60
        Height = 13
        Align = alRight
        Caption = #1044#1080#1072#1087#1072#1079#1086#1085':  '
        Layout = tlCenter
      end
      object Label7: TLabel
        Left = 195
        Top = 0
        Width = 32
        Height = 13
        Align = alRight
        Caption = 'Label4'
        Layout = tlCenter
      end
    end
  end
  object Button1: TButton
    Left = 32
    Top = 94
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 1
    OnClick = Button1Click
    OnKeyPress = NewCoordKeyPress
  end
  object Button2: TButton
    Left = 148
    Top = 94
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 2
    OnKeyPress = NewCoordKeyPress
  end
end
