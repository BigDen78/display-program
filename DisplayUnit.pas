﻿{$I DEF.INC}
unit DisplayUnit; { Language 8 }

interface

uses
  Classes, Windows, Types, Graphics, StdCtrls, Controls, CanvasDrawControl,
  Forms, ExtCtrls, Messages, SysUtils, MyTypes, BoxerUnit,  LanguageUnit,
  ConfigUnit, Dialogs, Lupa, CanvasDrawControl2, ComCtrls, FiltrUnit, AviconTypes,
  AviconDataSource, DataFileConfig, EventListUnit, RecProgressUnit;

type
  TImageForm = class(TForm)
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    Image10: TImage;
    Image11: TImage;
    Image12: TImage;
    Image13: TImage;
    Image14: TImage;
    Image15: TImage;
    Image16: TImage;
    Image17: TImage;
    Image18: TImage;
    NotebookImage: TImage;
    InfoImage: TImage;
    HandScanImage: TImage;
    Image19: TImage;
    Image20: TImage;
    RecImage: TImage;
    Image21: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TCrdStateSet = (csOnScreen, csOffScreen, csMoved, csLeftMoved, csRightMoved);
  TCrdState = set of TCrdStateSet;

  TMouseMode = (mmIdle, mmSelRailPoint, mmSetRect);

  TBScanLine = record
    Rail: TRail; // Нить
    Line: Integer; // Номер линии в BScanLineItem
    RailLine: Integer; // Номер линии в BScanLineItem

    ScanChToIndex: array [0..16] of Integer; // Масив связи номера канала сканирования и индексом масивов: ScanChNumList, ScanChBScanGateMin, ScanChBScanGateMax, ScanChBScanGateLen
    ScanChNumList: TIntegerDynArray; // Список каналов сканирования - содержить номера каналов В-разветки (в файле регистрации)
    ScanChBScanGateMin: TIntegerDynArray; //
    ScanChBScanGateMax: TIntegerDynArray; //
    ScanChBScanGateLen: TIntegerDynArray; //
    EvalChNumList: TIntegerDynArray; // Список каналов оценки - содержить номера каналов оценки


    // ScanChOrder: TIntegerDynArray;
    MirrorShadow: Boolean; // Флаг указывающий что на данной линии есть канал ЗТМ
    MirrorShadowScanChNum: array [0 .. 1] of Integer;
    // Канал 0 град - Номер ЗТМ канала сканирования
    MirrorShadowEvalChNum: array [0 .. 1] of Integer;
    // Канал 0 град - Номер ЗТМ канала оценки
    ZerroProbEchoEvalChNum: array [0 .. 1] of Integer;
    // Канал 0 град - Номер ЭХО канала оценки

    BScanRt: TRect; // Зона вывода В-развертки
    ChInfoRt: array [0 .. 3] of TRect; // Зоны вывода Информации о каналах - 0: Направление; 1: Алфа/Гамма; 2: Метод; 3: Зона
    ChSettRt: array [0 .. 3] of TRect; // Зоны вывода Настроек каналов - 0: Ку; 1: АТТ; 2: ВРЧ; 3: 2Тп
    ChGateRt: array [TRail] of TRect; // Зоны вывода стробов

    YStep: Single;
    XStep: Single;

  end;

  TBScanLineList = array of TBScanLine;

  TDrawZoneItem = record
    StartDC: Integer;
    EndDC: Integer;
  end;

  TDrawZone = array of TDrawZoneItem;

  {
    TDrawRect = record
    Rail: TRail;
    //   TapeIdx: Integer;
    Ch: array [1..20] of Integer;
    ChCount: Integer;
    Rt: array [1..3] of TRect;
    RtCount: Integer;
    Size: Integer;
    //    Flag: Boolean;
    end;

    TDrawRectList = array of TDrawRect;
    }
  TRailRect = record
    Height: Integer;  // Высота зоны вывода сигналов одного рельса в виде рельса
    RailHeight: Integer; // Высота рельса в точках
    RailHeight_mm: Integer; // Высота рельса в милиметрах
    RailHead: Integer;  // Высота головки рельса в точках
    RailBase: Integer;  // Высота подошвы рельса в точках
    BackTime: Integer;  // Задержка точки отражения канала 42 от подошвы рельса
  end;

  TAScanItem = record
    Delay: Single;
    Ampl: Integer;
    Color: TColor;
  end;

  TCurEchoList = record
    DisCrd: Integer;
    Echo: TCurEcho;
//    CurAKState: array [TRail, 0..255] of Boolean;
//    CurAKExists: array [TRail, 0..255] of Boolean;
  end;

  TMousePosDat = record
    X, Y: Integer;
    Params: TMiasParams;
    CrdParams: TCrdParams;
    Time: string;
    Speed: string;
    Sound: array [TRail, 0 .. 9] of Boolean;
    MouseDisCoord: Integer;
    // MouseRail: TRail;
    // AScanArr: array [1..4] of array of TAScanItem;
    CurEchoList: array of TCurEchoList;

    ScanChNumList: TIntegerDynArray; // Список каналов сканирования
    EvalChNumList: TIntegerDynArray; // Список каналов оценки
    BScanRt: TRect; // Зона вывода В-развертки выбранная мышью
    Delay: array [0 .. 3] of Single; // Задержки в мкс
    Depth: array [0 .. 3] of Single; // Глубина в мм

    Ch1_: Integer;
    Ch2_: Integer;
    Ch1_Idx: Integer;
    Ch2_Idx: Integer;
    D1: Integer;
    D2: Integer;
    // HH: Integer;

    RailTop: Integer;
    RailBottom: Integer;

    Ok: Boolean;
    Rail: TRail;
    MirrorShadow: Boolean;
    MirrorShadowEvalChNum: Integer;
  end;

  PMousePosDat = ^TMousePosDat;

  TInfoRectTyp = (rtInfo, rtHandScan, rtNotebook, rtRec, rtAirBrush, rtMedia, rtAlarm, rtRailHeadScaner);

  TInfoRectItem = record
    Rt: TRect;
    Text: string;
    Typ: TInfoRectTyp;

    Tag_: Integer;
    EventIdx: Integer;
    EventID: Integer;

    CanGroup: Boolean;
  end;

  TSelHandScanEvent = procedure(Sender: TObject; HSList: array of Integer; DataType: Integer; ViewForm: Pointer = nil) of object;
  TSelMedia = procedure(Sender: TObject; MediaType: TMediaType; var Data: TMemoryStream; EventIdx: Integer) of object;

  TInfoRectList = array of TInfoRectItem;

  TDispRectsItem = record
    Rt: TRect;
    Idx: Integer;
  end;

  TDispRects = array of TDispRectsItem;

  TAmplColor1 = array [0 .. 255] of TColor;
  TAmplColor2 = array [1 .. 2] of TAmplColor1;

  TAvk11Display = class;

  TExtDraw = procedure(Disp: TAvk11Display; Mode: Integer) of object;
  TExtDraw2 = procedure (Disp: TAvk11Display; Rail: RRail; OutRect: TRect; StartDisCoord, EndDisCoord, Index: Integer) of object;

  TAvk11Display = class(TCanvasDrawControl2)
    // TAvk11Display = class( TCanvasDrawControl  )
    // TAvk11Display = class( TDirectDrawControl  )
  private
    FBorderStyle: TBorderStyle;
    FDatSrc: TAvk11DatSrc;

    // Расчет зон вывода
    FBScanBox: TBoxer; // Зоны вывода В-развертки
    FChInfoBox: TBoxer; // Зоны вывода Информации о каналах
    FChSettBox: TBoxer; // Зоны вывода Настроек каналов
    FChGateBox: array [TRail] of TBoxer; // Зоны вывода стробов

    // Зоны вывода
    FBScanLines: TBScanLineList; // Линии В-развертки

    FScrollBar: TScrollBar;
    FStartScrX: Integer;

    FLastDrawX1: array [TRail] of Integer;
    FLastDrawX2: array [0 .. 16, TRail] of Integer;
    FLastDrawY2: array [0 .. 16, TRail] of Integer;
    // FBScanRtIdx: array [TRail] of Integer;
    FRailSide: array [TRail] of TRect;
    BottomSignalDelay: array [TRail] of Integer;
    FEndScrX: Integer;
    FStartDisCoord: Integer;
    FEndDisCoord: Integer;
    FScanChNumToBScanLine: array [rLeft .. rRight, 0 .. 255] of Integer; // Масив связи нити и номера канала с индексом в масиве BScanLine

    FRailChem: array [TRail, 0 .. 2000] of TRailRect;
    FReqCoord: Integer;
    FSetRail: TRail;
    FSetCh1: Integer;
    FSetCh2: Integer;
    FGetIdx: Integer;
    // FSaveViewMode: TDisplayMode;
    FSaveViewMode: TBScanViewMode;
    FSaveViewRail: TRail;

    FFCoordRect: TRect; // Зона вывода линейки координат
    FChGateRect: array [TRail] of TRect; // Зона вывода линейки - координат под стробами

    {
      FFCoordRect0: TRect;
      FFCoordRect1: TRect;
      FFCoordRect2: TRect; }
    FCenterDisCoord: Integer;

    FDM: TDisplayMode;

    FMouseMode: TMouseMode;
    FSelRailPointCount: Integer;
    FMouseDownFlag: Boolean;
    FOldX: Integer;
    FOldY: Integer;
    FPicIdx: array [0 .. 255] of Integer;
    FLineCoord: Boolean;
    FDispConf: TDisplayConfig;
    FDispCols: TDisplayColors;
    FFilterMode: TFilterMode;
    FDrawMode: Integer;
    FDrawSameCoordMode: Boolean;

    FNaezdChByAmpl: TAmplColor2; // Наезжающие каналы, цвет №1 и цвет №2 по амплитуде
    FOtezdChByAmpl: TAmplColor2; // Отъезжающие каналы, цвет №1 и цвет №2 по амплитуде

    FSoundFlag: Boolean;
    FSoundOk: Boolean;
    FScreenFusy: array [0 .. 3000] of Boolean;
    FDrawRect: TRect;
//    FSideViewZoom: array [rLeft .. rRight] of Single;
//    FEchoSize: Integer;
    FEchoSizeByAmpl: Boolean;
    FEchoColorByAmpl: Boolean;
    FRulerText1: string;
    FRulerText2: string;
    FOnScrText: string;
    FViewLabels: Boolean;
    FAssignList: TList;
    FLupaForm: TLupaForm;
    FBeforeSgnExtDraw: TExtDraw;
    FAfterSgnExtDraw: TExtDraw;
    FExtendedViewDraw: TExtDraw2;

    FExtendedView: Boolean;
    FExtendedViewList: array of Integer;
    FExtendedViewTopIdx: Integer;
    FExtendedViewBtmIdx: Integer;

    FDrawCount: Integer;

    procedure WMGetDlgCode(var Msg: TWMGetDlgCode);
    message WM_GETDLGCODE;
    procedure WMEraseBkgnd(var message: TWMEraseBkgnd);
    message WM_ERASEBKGND;
    // procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    // procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;

  protected
    FFFF: Boolean;
    FOnChangeMouseMode: TNotifyEvent;
    FOnSelRailPoint: TNotifyEvent;
    FOnSetRect: TNotifyEvent;
    FOnSelMedia: TSelMedia;
    FOnSelHandScan: TSelHandScanEvent;
    FOnSelRecResEvent: TNotifyEvent;
    FOnSelNotebook: TNotifyEvent;
    // FDisplayMode: TDisplayMode;
    FStartRail: TRail;
    FEndRail: TRail;
    FStartLine: Integer;
    FEndLine: Integer;

    FCentCrd: Integer; // Выделение пачки
    FDelay1: Integer;
    FDelay2: Integer;
    FCh1: Integer;
    FCh2: Integer;
    // FSelChIdx: Integer;
    FRail: TRail;
    FEcho: array [1 .. 2, -200 .. 200, 0 .. 255] of Integer;
    // FPBPoints: array of TPoint;

    FFiltr: TFiltr;
    FBSignal: TList;
    FBMList: Pointer;
    FDrawAirBrushIdx: array of Integer;
    FDrawAlarmBrushIdx: array of Integer;

    FBSALastCoord: array [0 .. 16, rLeft .. rRight] of Integer;
    FBSALastVal: array [0 .. 16, rLeft .. rRight] of Integer;

    FOldMouseX: Integer;
    FOldMouseY: Integer;
    FBScanWidth: Integer;

    function TestScrFusy(X1, X2: Integer): Boolean;
    procedure DrawBSAmplSample(RtIdx: Integer; R: TRail; DrawX: Integer; LastCrd: Boolean; Ampl: Integer);
    function DrawBScanSample(StartDisCoord: Integer): Boolean;
    // function DrawBScanSample2(StartDisCoord: Integer): Boolean;
    function DrawRailViewSample(StartDisCoord: Integer): Boolean;
    function RailSideViewSample(StartDisCoord: Integer): Boolean;
    procedure DrawBackMotionZone(StartDisCoord, StartX, EndX, Mode: Integer);
    procedure CreateImage(StartDisCoord, StartX, EndX: Integer);
    function DrawRuler(StartDisCoord, StartX, EndX: Integer): string;
    procedure DrawCoordGrid(StartDisCoord, StartX, EndX: Integer);
    procedure DrawEvents(StartDisCoord, StartX, EndX: Integer; DrawHC: Boolean = False);
    procedure DrawLongEvents(StartDisCoord, StartX, EndX: Integer);
    function CalcPixelSize(Idx, Ampl: Integer): TPoint;
    procedure ChangeScrollBar(Sender: TObject);

    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure DblClick; override;

    procedure CreateParams(var Params: TCreateParams); override;
    // procedure Resize123; // override;
    procedure SetDatSrc(New: TAvk11DatSrc);

    // function GetMaxViewCoord: Integer;
    procedure SetZoom(New: Integer);
    procedure SetCenterDisCoord(New: Integer);
    procedure SetReduction(New: Integer);
    procedure SetReductionMode(New: Integer);
    procedure SetRailView(New: Boolean);
    procedure SetAmplTh(New: Integer);
    procedure SetAmplDon(New: Boolean);
    procedure SetViewChannel(New: TViewChannel);
    procedure SetSoundFlag(NewFlag: Boolean);
    procedure SetShowChSettings(NewState: Boolean);
    procedure SetShowChGate(NewState: Boolean);
    procedure SetShowChInfo(NewState: Boolean);

    function GetSkipBM: Boolean;
    function GetViewAC: Boolean;
    procedure SetSkipBM(NewState: Boolean);
    procedure SetViewAC(NewState: Boolean);
    procedure ReEchoSize;
    procedure SetMouseMode(New: TMouseMode);
    procedure DDLostData(Sender: TObject);
    function GetAssignCount: Integer;
    function GetAssignItem(Index: Integer): TAvk11Display;
    procedure DoAssignList;
    function LoadBPEcho(StartDisCoord: Integer): Boolean;
    procedure GetBP(X, Y: Integer);

    // function GetEvalChColor(EvalChNum: Integer): TColor;
    function DisplayMode_ShowChGate: Boolean;
    function DisplayMode_ShowChSettings: Boolean;
    function DisplayMode_ShowChInfo: Boolean;
    function GetBBUFER: TBitMap;

  public

    FullHint: string;
    FullHintDisCoord: Integer;
    ViewACState: Boolean;
    DrawWidth_: Single;
//        PixelCount_: Integer;

    ShowSensor1DataFlag: Boolean;
    FMaxReduce: array [0 .. 2] of Integer;
    // ChNumColors: array [TRail] of array of TAmplColor1; // Список цветов для каналов оценки. Предпологается что каналы сканирования совпадают
    // // с каналами оценки, только некоторых может не быть. Разделение на нити сделано из за
    // // зависимости канала от нити см. Gran. Цвета расчитаны для разных амплитуд

    EvalChNumColors2: array [0 .. MaxChannel_] of TAmplColor1;
    // Список цветов для каналов оценки.

    HeightToSmall: Boolean;

    SaveWidth: Integer;
    CoordRectRowIdx: Integer;

    piNotebook: Integer;
    piInfo: Integer;
    piFail: Integer;
    piOK: Integer;
    piHandScan: Integer;
    piRecImage: Integer;

    InfoRect: TInfoRectList;
    LastDrawWidth: Integer;
    MousePosDat: TMousePosDat;
    Show0CoordEvent: Boolean;
    Show0EchoCountEvent: Boolean;

    BoundsRect: TRect;
    BoundsRect_: array [TRail] of TRect;
{$IFDEF REC}
    DispRects: TDispRects;
    ShowIndex: Integer;
{$ENDIF}
    MetallSensorValue0: Boolean;
    MetallSensorValue1: Boolean;
    MetallSensorZoneState: Boolean;
    UnstableBottomSignalState: Boolean;
    ZerroProbeModeState: Boolean;
    PaintSystemStateState: Boolean;
    PaintSystemViewResState: Boolean;
    PaintSystemViewResDebug: Boolean;
//    AutomaticSearchResState: Boolean;

    DebugView1: Boolean;
    DebugView2: Boolean;

    procedure ReCalcOutRect;
    procedure ApplyDisplayMode;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetData(Parent: TWinControl; ScrollBar: TScrollBar; DataSource: TAvk11DatSrc);
    procedure SetRightCoord(New: Integer);
    procedure SetLeftCoord(New: Integer);
    procedure FullRefresh;
    procedure Refresh;
    procedure ShowMark(MarkIdx, ShowPos: Integer);
    procedure FiltrCalc;
    function GetMouseData(X, Y: Integer; Dat: PMousePosDat = nil): Boolean;
    procedure GetMouseRect(Rt: TRect; var DisCoord1, DisCoord2, Ch1Delay1, Ch1Delay2, Ch2Delay1, Ch2Delay2: Integer; var Ch1, Ch2: Integer; var SameRect: Boolean);
    procedure MaximizeTape(X, Y: Integer);
    function MouseToDisCoord(X, Y: Integer): Integer;
    procedure AddToScrFusy(X1, X2: Integer);
    procedure EasyLeft;
    procedure EasyRight;
    procedure StepLeft;
    procedure StepRight;
    procedure PageLeft;
    procedure PageRight;
    procedure NextZoom;
    procedure PrevZoom;

    function GetScrCrd(DisCoord: Integer; var State: TCrdState): Integer;
    function GetScrReg(Range: TRange; var State: TCrdState): TMinMax;
    function GetTapeBounds(R: TRail; Tape: Integer; var State: TCrdState): TMinMax;
    function GetRailBounds(R: TRail; var State: TCrdState): TMinMax;
    function GetY(R: TRail; Ch, Delay: Integer; var State: TCrdState): Integer;
    function GetCoordLineY(R: TRail): Integer;
    function ExtendedViewListItem(Index: Integer): Integer;
    function ExtendedViewListCount: Integer;

    procedure SetViewMode(New: TBScanViewMode; ViewRail: TRail; ViewLine: Integer); overload;
    procedure SetViewMode(New: TBScanViewMode; ViewRail: TRail); overload;
    procedure SetViewMode(New: TBScanViewMode); overload;

    function EchoTable(StartDisCoord: Integer): Boolean;
    procedure SwitchReductionMode;
    // function GetTapeRect(Rail: TRail; Idx: Integer): TRect;
    procedure SetAll(CenterDisCoord: Integer; DisplayMode: TDisplayMode);
    procedure SetScrollBar(New: TScrollBar);
    procedure SetDisplayColors(New_: TDisplayColors);
    procedure SetDispConf(New: TDisplayConfig);
    procedure SetViewLabels(NewSate: Boolean);
    procedure AssignTo(View: TAvk11Display);
    function EvalChNumToName(Num: Integer): string;
//    function CID_GateIndex_To_Name(CID, GateIndex: Integer): string;

    function TestAssign(View: TAvk11Display): Boolean;
    procedure DeleteAssign(Index: Integer); overload;
    procedure DeleteAssign(View: TAvk11Display); overload;
    function DisToScr(DisCoord: Int64): Int64;
    property DatSrc: TAvk11DatSrc read FDatSrc write FDatSrc;
    property DisplayConfig: TDisplayConfig read FDispConf write SetDispConf;
    property Zoom: Integer read FDM.Zoom write SetZoom;
    property CenterDisCoord: Integer read FCenterDisCoord write SetCenterDisCoord;
    property Reduction: Integer read FDM.Reduction write SetReduction;
    property ReductionMode: Integer read FDM.ReductionMode write SetReductionMode;
    property EchoSizeByAmpl: Boolean read FEchoSizeByAmpl write FEchoSizeByAmpl;
    property EchoColorByAmpl: Boolean read FEchoColorByAmpl write FEchoColorByAmpl;
//    property EchoSize: Integer read FEchoSize write FEchoSize;
    property FilterMode: TFilterMode read FFilterMode write FFilterMode;
    property RailView: Boolean read FDM.RailView write SetRailView;
    property AmplTh: Integer read FDM.AmplTh write SetAmplTh;
    property AmplDon: Boolean read FDM.AmplDon write SetAmplDon;
    property ViewMode: TBScanViewMode read FDM.ViewMode;
    property ViewRail: TRail read FDM.ViewRail;
    property ViewLine: Integer read FDM.ViewLine;
    property ViewChannel: TViewChannel read FDM.ViewChannel write SetViewChannel;
    property LineCoord: Boolean read FLineCoord write FLineCoord;
    property StartDisCoord: Integer read FStartDisCoord;
    property EndDisCoord: Integer read FEndDisCoord;
    property ViewLabels: Boolean read FViewLabels write SetViewLabels;
    property SkipBackMotion: Boolean read GetSkipBM write SetSkipBM;
    property ViewAC: Boolean read GetViewAC write SetViewAC;
    property MouseMode: TMouseMode read FMouseMode write SetMouseMode;
    property SelRailPointCount: Integer read FSelRailPointCount write FSelRailPointCount;
    property OnChangeMouseMode: TNotifyEvent read FOnChangeMouseMode write FOnChangeMouseMode;
    property OnSelRailPoint: TNotifyEvent read FOnSelRailPoint write FOnSelRailPoint;
    property OnSetRect: TNotifyEvent read FOnSetRect write FOnSetRect;
    property OnSelHandScan: TSelHandScanEvent read FOnSelHandScan write FOnSelHandScan;
    property OnSelMedia: TSelMedia read FOnSelMedia write FOnSelMedia;
    property OnSelRecResEvent: TNotifyEvent read FOnSelRecResEvent write FOnSelRecResEvent;
    property OnSelNotebook: TNotifyEvent read FOnSelNotebook write FOnSelNotebook;
    property SoundFlag: Boolean read FSoundFlag write SetSoundFlag;
    property AssignCount: Integer read GetAssignCount;
    property AssignItem[Index: Integer]: TAvk11Display read GetAssignItem;
    property BeforeSgnExtDraw: TExtDraw read FBeforeSgnExtDraw write FBeforeSgnExtDraw;
    property AfterSgnExtDraw: TExtDraw read FAfterSgnExtDraw write FAfterSgnExtDraw;
    property ExtendedViewDraw: TExtDraw2 read FExtendedViewDraw write FExtendedViewDraw;
    property Filtr: TFiltr read FFiltr write FFiltr;
    property DisplayMode: TDisplayMode read FDM write FDM;
    property ShowChSettings: Boolean read FDM.ShowChSettings write SetShowChSettings;
    property ShowChGate: Boolean read FDM.ShowChGate write SetShowChGate;
    property ShowChInfo: Boolean read FDM.ShowChInfo write SetShowChInfo;
    property BBUFER: TBitMap read GetBBUFER;
    procedure SetExtendedView(Percents: array of Integer);
    property DispConf: TDisplayConfig read FDispConf;
    property DispCols: TDisplayColors read FDispCols;
    property DrawCount: Integer read FDrawCount;
    property DrawSameCoordMode: Boolean read FDrawSameCoordMode write FDrawSameCoordMode;

  published

    property ScrollBar: TScrollBar read FScrollBar write SetScrollBar;
    property Align;
    property Enabled;
    property TabOrder;
    property TabStop default False;
    property Visible;
    property BScanWidth: Integer read FBScanWidth;
  end;

implementation

{$R *.dfm}

uses
  Math, MainUnit, DebugFormUnit, RecData, Avk11ViewUnit;

type
  CID_GIdx_To_Text_Item = record
    CID: Integer;
    GIdx: Integer;
    Name: string;
  end;

const
  FAssignNow: Boolean = False;

  RPts_m = 21;
  RPts: array [0 .. RPts_m] of TPoint = ((X: 75 - 75; Y: 0), (X: 97 - 75; Y: 0), (X: 102 - 75; Y: 1), (X: 104 - 75; Y: 2), (X: 109 - 75; Y: 7), (X: 111 - 75; Y: 12), (X: 112 - 75; Y: 25), (X: 112 - 75; Y: 33), (X: 110 - 75; Y: 36), (X: 107 - 75; Y: 37), (X: 91 - 75; Y: 41), // 10
    (X: 88 - 75; Y: 43), (X: 85 - 75; Y: 47), (X: 84 - 75; Y: 51), (X: 84 - 75; Y: 133), (X: 85 - 75; Y: 140), (X: 90 - 75; Y: 149), (X: 94 - 75; Y: 153), (X: 102 - 75; Y: 157), (X: 150 - 75; Y: 169), (X: 150 - 75; Y: 180), // 20
    (X: 75 - 75; Y: 180));

  FEchoWidth: array [0 .. 4] of Integer = (1, 1, 2, 2, 3);
  FEchoHeight: array [0 .. 4] of Integer = (1, 2, 2, 3, 3);

var
  CID_GIdx_To_Text: array of CID_GIdx_To_Text_Item;
  // ---------< TAvk11Display >---------------------------------------------------

procedure TAvk11Display.WMEraseBkgnd(var message: TWMEraseBkgnd);
// Отключает перерисовку фона формы
begin
  Message.Result := 1;
end;

procedure TAvk11Display.AddToScrFusy(X1, X2: Integer);
begin
    if X1 < 0 then X1:= 0;
    if X2 < 0 then Exit;
    if X1 > 3000 then Exit;
    if X2 > 3000 then X2:= 3000;
    FillChar(FScreenFusy[X1], (X2 - X1 + 1) * SizeOf(Boolean), $FF);
end;

function TAvk11Display.TestScrFusy(X1, X2: Integer): Boolean;
var
  X: Integer;

begin
    Result:= True;
    if X1 < 0 then X1:= 0;
    if X2 < 0 then Exit;
    if X1 > 3000 then Exit;
    if X2 > 3000 then X2:= 3000;
    X:= X1;
    repeat
    if FScreenFusy[X] then
    begin
    Result:= False;
    Exit;
    end;
    Inc(X, 10);
    until X > X2;
end;

procedure TAvk11Display.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  J, I: Integer;
  Flg: Boolean;
  Flg1: Boolean;
  MP: TPoint;
  RGN: HRGN;

  F: array of Integer;

begin
  if (FOldMouseY = Y) and (FOldMouseX = X) then Exit;
  FOldMouseX := X;
  FOldMouseY := Y;

  inherited MouseMove(Shift, X, Y);

  // - Отметки краскопульта
  SetLength(F, 0);
  if Assigned(FOnSelHandScan) then
    for I := 0 to High(InfoRect) do
      if PtInRect(InfoRect[I].Rt, Point(X, Y)) and (InfoRect[I].Typ = rtAirBrush) then
      begin
        SetLength(F, Length(F) + 1);
        F[ High(F)] := InfoRect[I].Tag_;
      end;

  Flg := True;
  for I := 0 to High(F) do
  begin
    Flg1 := False;
    for J := 0 to High(FDrawAirBrushIdx) do
      if F[I] = FDrawAirBrushIdx[J] then
      begin
        Flg1 := True;
        Break;
      end;
    if not Flg1 then
    begin
      Flg := False;
      Break;
    end;
  end;

  if not Flg then
  begin
    SetLength(FDrawAirBrushIdx, Length(F));
    Move(F[0], FDrawAirBrushIdx[0], Length(F) * 4);
    SetLength(F, 0);
    FFFF := True;
    Refresh;
  end
  else if Length(F) = 0 then
  begin
    SetLength(FDrawAirBrushIdx, 0);
    if FFFF then
    begin
      Refresh;
      FFFF := False;
    end;
  end;

  // - Отметки АСД
  SetLength(F, 0);
  if Assigned(FOnSelHandScan) then
    for I := 0 to High(InfoRect) do
      if PtInRect(InfoRect[I].Rt, Point(X, Y)) and (InfoRect[I].Typ = rtAlarm) then
      begin
        SetLength(F, Length(F) + 1);
        F[ High(F)] := InfoRect[I].Tag_;
      end;

  Flg := True;
  for I := 0 to High(F) do
  begin
    Flg1 := False;
    for J := 0 to High(FDrawAlarmBrushIdx) do
      if F[I] = FDrawAlarmBrushIdx[J] then
      begin
        Flg1 := True;
        Break;
      end;
    if not Flg1 then
    begin
      Flg := False;
      Break;
    end;
  end;

  if not Flg then
  begin
    SetLength(FDrawAlarmBrushIdx, Length(F));
    Move(F[0], FDrawAlarmBrushIdx[0], Length(F) * 4);
    SetLength(F, 0);
    FFFF := True;
    Refresh;
  end
  else if Length(F) = 0 then
  begin
    SetLength(FDrawAlarmBrushIdx, 0);
    if FFFF then
    begin
      Refresh;
      FFFF := False;
    end;
  end;

  /// /////////////////

  if FMouseMode = mmSelRailPoint { FDisplay.DrawCentrCur } then
  begin
    GetMouseData(X, Y);

    { FBScanLines[0].Rail = MousePosDat.Rail
      FBScanLines[0].BScanRt
      FBScan }

    CentrCurTop := Point(X, MousePosDat.RailTop + 5);
    // GetTapeRect(MousePosDat.Rail, 1).Top + 5);
    CentrCurHeight := MousePosDat.RailBottom - MousePosDat.RailTop - 10; // GetTapeRect(MousePosDat.Rail, 4).Bottom - GetTapeRect(MousePosDat.Rail, 1).Top - 10;
    Flip;
  end;

  if FMouseMode = mmIdle then
  begin
    if FDrawSameCoordMode then
    begin
      DrawSameCoord:= True;
      SetLength(SameCoord, 0);
      FDatSrc.DisToDisCoords(MousePosDat.MouseDisCoord, SameCoord);
      for I := 0 to High(SameCoord) do
        SameCoord[I]:= FStartScrX + DisToScr(SameCoord[I] - FStartDisCoord);

      // CentrCurTop := Point(X, MousePosDat.RailTop + 5);
      // GetTapeRect(MousePosDat.Rail, 1).Top + 5);
      // CentrCurHeight := MousePosDat.RailBottom - MousePosDat.RailTop - 10; // GetTapeRect(MousePosDat.Rail, 4).Bottom - GetTapeRect(MousePosDat.Rail, 1).Top - 10;

      CentrCurTop := Point(X, FBScanLines[0].BScanRt.Top);
      CentrCurHeight := FBScanLines[High(FBScanLines)].BScanRt.Bottom;
      Flip;
    end;
  end;

  if FMouseDownFlag and (FMouseMode = mmIdle) and ((FOldX <> X) or (FOldY <> Y)) then
  begin
    FMouseDownFlag := False;
    SetMouseMode(mmSetRect);
    // DrawSelRect:= True;
    SelRect.Left := X;
    SelRect.Top := Y;
    SelRect.Right := X;
    SelRect.Bottom := Y;
    Flip;
  end
  else if FMouseMode = mmSetRect then
  begin
    SelRect.Right := X;
    SelRect.Bottom := Y;
    Flip;
  end;

  if Assigned(FLupaForm) { and (not FRailView) } then
  begin
    GetMouseData(X, Y);
    FLupaForm.SetData(@MousePosDat, FDatSrc, FDM.Reduction, FDM.ReductionMode, Self);
    FLupaForm.SetPos;
  end;

{$IFDEF REC}
  J := ShowIndex;
  ShowIndex := -1;
  for I := 0 to High(DispRects) do
  begin
    RGN := CreateRectRgn(DispRects[I].Rt.Left, DispRects[I].Rt.Top, DispRects[I].Rt.Right, DispRects[I].Rt.Bottom);
    if PtInRegion(RGN, X, Y) then
    begin
      ShowIndex := DispRects[I].Idx;
      Flg := True;
    end;
    DeleteObject(RGN);
    if Flg then
      Break;
  end;
  if J <> ShowIndex then
    Refresh;
{$ENDIF}
end;

procedure TAvk11Display.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if mbLeft = Button then
  begin
    if FMouseMode = mmSelRailPoint then
    begin
      FSelRailPointCount := FSelRailPointCount - 1;
      if FSelRailPointCount = 0 then
        SetMouseMode(mmIdle);
      if Assigned(FOnSelRailPoint) then
        FOnSelRailPoint(Pointer(Byte(Button)));
      Exit;
    end;

    FOldX := X;
    FOldY := Y;
    FMouseDownFlag := True;
  end;

  if (Button = mbRight) or (Button = mbMiddle) then
    if PtInRect(BoundsRect, Point(X, Y)) and (not DisplayMode.RailView) then
    begin
      if not Assigned(FLupaForm) then
        FLupaForm := TLupaForm.Create(nil);

      case Button of
        mbRight:
          FLupaForm.Mode := 0;
        mbMiddle:
          FLupaForm.Mode := 1;
      end;

      if FLupaForm.SetData(@MousePosDat, FDatSrc, FDM.Reduction, FDM.ReductionMode, Self) then
      begin
        FLupaForm.SetPos;
        FLupaForm.Visible := True;
      end
      else
      begin
        FLupaForm.Free;
        FLupaForm := nil;
      end;
    end;

  inherited MouseDown(Button, Shift, X, Y);
end;

procedure TAvk11Display.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  I: Integer;
  R: TRail;
  Ch1, Ch2: Integer;
  HSList1: array of Integer;
  HSList2: array of Integer;
  Crd1, Crd2: Integer;
  tmp: TEventListForm;
  CrdParams: TCrdParams;
  tmp_arr: array of TPoint;
  Avk11View_: TAvk11ViewForm;

begin
  FMouseDownFlag := False;
  if FMouseMode = mmSetRect then
  begin
    SetMouseMode(mmIdle);
    if Assigned(FOnSetRect) then
      FOnSetRect(Self);
    Refresh;
{$IFDEF RECDEBUG2}
    GetMouseData(FOldX, FOldY);
    Crd1 := MousePosDat.MouseDisCoord;
    GetMouseData(X, Y);
    Crd2 := MousePosDat.MouseDisCoord;

    with TTestForm2.Create(nil) do
    begin

      // SetData(FDatSrc, r_Right, 2335930, 2335991, 2335930, 2335991, 7);
      // SetData(FDatSrc, r_Left, 2973007, 2973036, 2973007, 2973036, 6);

      SetData(FDatSrc, MousePosDat.Rail, FDatSrc.Event[6069].Event.DisCoord, FDatSrc.Event[6076].Event.DisCoord, FDatSrc.Event[6069].Event.DisCoord, FDatSrc.Event[6076].Event.DisCoord,
        { FStartDisCoord, FEndDisCoord, } MousePosDat.Ch2_
        { , (Crd2 + Crd1) div 2 } );
      (*
        case FViewChannel of
        vcOtezd: SetData(FDatSrc, MousePosDat.Rail, Crd1, Crd2, Crd1, Crd2, {FStartDisCoord, FEndDisCoord,} MousePosDat.Ch2_ {, (Crd2 + Crd1) div 2});
        vcAll,
        vcNaezd: SetData(FDatSrc, MousePosDat.Rail, Crd1, Crd2, Crd1, Crd2, {FStartDisCoord, FEndDisCoord,} MousePosDat.Ch1_ {, (Crd2 + Crd1) div 2});
        end;
        *)
      ShowModal;
      Free;
    end;
{$ENDIF}
  end;

  Avk11View_:= MainForm.ActiveViewForm;
  SetLength(HSList1, 0);
  if Button in [mbLeft] then
    if Assigned(FOnSelHandScan) then
    begin
      for I := 0 to High(InfoRect) do
        if PtInRect(InfoRect[I].Rt, Point(X, Y)) and (InfoRect[I].Typ = rtHandScan) then
        begin
          SetLength(HSList1, Length(HSList1) + 1);
          HSList1[ High(HSList1)] := InfoRect[I].Tag_;
//          ShowMessage('Hand Scan ' + IntToStr(InfoRect[I].Tag_));
        end;
    end;

  SetLength(HSList2, 0);
  if Button in [mbLeft] then
    if Assigned(FOnSelHandScan) then
    begin
      for I := 0 to High(InfoRect) do
        if PtInRect(InfoRect[I].Rt, Point(X, Y)) and (InfoRect[I].Typ = rtRailHeadScaner) then
        begin
          SetLength(HSList2, Length(HSList2) + 1);
          HSList2[ High(HSList2)] := InfoRect[I].Tag_;
//          ShowMessage('Head Scan ' + IntToStr(InfoRect[I].Tag_));
        end;

    end;

    if Length(HSList1) <> 0 then
    begin
      FOnSelHandScan(Self, HSList1, 1{rtHandScan});
      SetLength(HSList1, 0);
    end;

    if Length(HSList2) <> 0 then
    begin
      FOnSelHandScan(Self, HSList2, 2{rtRailHeadScaner}, Avk11View_);
      SetLength(HSList2, 0);
    end;


{$IFDEF REC}
  if Assigned(FOnSelRecResEvent) then
  begin
    for I := 0 to High(InfoRect) do
      if PtInRect(InfoRect[I].Rt, Point(X, Y)) and (InfoRect[I].Typ = rtRec) then
      begin
        FOnSelRecResEvent(Pointer(InfoRect[I].Tag_));
        Break;
      end;
  end;
{$ENDIF}
  if Button in [mbLeft] then
    if Assigned(FOnSelNotebook) then
      for I := 0 to High(InfoRect) do
        if PtInRect(InfoRect[I].Rt, Point(X, Y)) and (InfoRect[I].Typ = rtNotebook) then
          FOnSelNotebook(Pointer(InfoRect[I].Tag_));


  if Button in [mbLeft] then
    if Assigned(FOnSelMedia) then
    begin
      SetLength(tmp_arr, 0);
      for I := 0 to High(InfoRect) do
        if PtInRect(InfoRect[I].Rt, Point(X, Y)) and (InfoRect[I].Typ = rtMedia) then
        begin
          SetLength(tmp_arr, Length(tmp_arr) + 1);
          tmp_arr[High(tmp_arr)].X:= InfoRect[I].Tag_;
          tmp_arr[High(tmp_arr)].Y:= InfoRect[I].EventIdx;
        end;

      for I := 0 to High(tmp_arr) do
        FOnSelMedia(nil, TMediaType(FDatSrc.FMediaList[tmp_arr[I].X].DataType), FDatSrc.FMediaList[tmp_arr[I].X].Data, tmp_arr[I].Y);

      SetLength(tmp_arr, 0);
    end;

  if Button in [mbRight, mbMiddle] then
  begin
    if Assigned(FLupaForm) then
    begin
      FLupaForm.Free;
      FLupaForm := nil;
    end;

    if (Button in [mbRight]) and (FullHint <> '') then
    begin
      tmp := TEventListForm.Create(Self);

      FDatSrc.DisToCoordParams(FullHintDisCoord, CrdParams);
      tmp.SetData(FullHint, RealCrdToStr(CrdParamsToRealCrd(CrdParams, FDatSrc.Header.MoveDir), 1));
      tmp.Visible := True;
    end;

    {
      if FSaveDisState then
      begin
      FSaveDisState:= False;
      FZoom:= FSaveDisState_Zoom;
      SetCenterDisCoord(FSaveDisState_DisCrd);
      end
      else
      begin
      FSaveDisState:= True;
      FSaveDisState_Zoom:= FZoom;
      FSaveDisState_DisCrd:= FCenterDisCoord;
      FZoom:= 500;
      SetCenterDisCoord(MouseToDisCoord(X, Y));
      end;
      }
  end;
  // GetBP(X, Y);
  inherited MouseUp(Button, Shift, X, Y);
end;

function TAvk11Display.LoadBPEcho(StartDisCoord: Integer): Boolean;
var
  I, C, D: Integer;

begin
  (*
    C:= FDatSrc.CurDisCoord - FCentCrd;

    //  for I:= 1 to FDatSrc.CurEcho[FRail, FCh1].Count do
    begin
    if FReduction = 0 then D:= 0
    else D:= (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((FDispConf.ReducePos[FReductionMode, FReduction, FCh1] + FDispConf.ReduceDel[FReductionMode, FReduction, FCh1] * FDatSrc.CurEcho[FRail, FCh1].Delay[I]) / (FDatSrc.Header.ScanStep / 100));
    if (C + D >= - 200) and (C + D <= 200) then
    FEcho[1, C + D, FDatSrc.CurEcho[FRail, FCh1].Delay[I]]:= 1 + FDatSrc.CurEcho[FRail, FCh1].Ampl[I];
    end;

    for I:= 1 to FDatSrc.CurEcho[FRail, FCh2].Count do
    begin
    if FReduction = 0 then D:= 0
    else D:= (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((FDispConf.ReducePos[FReductionMode, FReduction, FCh2] + FDispConf.ReduceDel[FReductionMode, FReduction, FCh2] * FDatSrc.CurEcho[FRail, FCh2].Delay[I]) / (FDatSrc.Header.ScanStep / 100));
    if (C + D >= - 200) and (C + D <= 200) then
    FEcho[2, C + D, FDatSrc.CurEcho[FRail, FCh2].Delay[I]]:= 1 + FDatSrc.CurEcho[FRail, FCh2].Ampl[I];
    end; *)
  Result := True;
end;

procedure TAvk11Display.GetBP(X, Y: Integer);
(*
  type
  MyArr = array of TPoint;

  var
  X_, Y_, I, X2, Y2, D, D2: Integer;
  Pts1: MyArr;
  Pts2: MyArr;

  procedure CreatePointList(var Pts: MyArr; Steps: Integer; StartX, StartY: Integer);
  const
  Dir: array [1..4] of TPoint = ((X: - 1; Y: 0), (X: 0; Y: - 1), (X: 1; Y: 0), (X: 0; Y: 1));

  var
  I, J, K, L, M: Integer;

  begin
  SetLength(Pts, Length(Pts) + 1);
  Pts[High(Pts)].X:= StartX;
  Pts[High(Pts)].Y:= StartY;
  K:= 1;
  J:= 1;
  L:= 0;
  for I:= 0 to Steps do
  begin
  for M:= 1 to K do
  begin
  StartX:= StartX + Dir[J].X;
  StartY:= StartY + Dir[J].Y;
  with TPanel.Create(nil) do
  begin
  SetLength(Pts, Length(Pts) + 1);
  Pts[High(Pts)].X:= StartX;
  Pts[High(Pts)].Y:= StartY;
  end;
  end;
  Inc(L);
  if L = 2 then
  begin
  L:= 0;
  Inc(K);
  end;
  Inc(J);
  if J = 5 then J:= 1;
  end;
  end;
  *)
begin
  (*
    SetLength(FPBPoints, 0);
    if not GetMouseData(X, Y) then Exit;
    FillChar(FEcho[1, -200, 0], 2 * 401 * 256 * 4, 0);

    FRail:= MousePosDat.Rail;
    FCentCrd:= MousePosDat.MouseDisCoord;
    FCh1:= MousePosDat.Ch1_;
    FCh2:= MousePosDat.Ch2_;
    if FCh1 in [0, 1] then
    begin
    FDelay1:= MousePosDat.D1 * 3;
    FDelay2:= MousePosDat.D2 * 3;
    end
    else
    begin
    FDelay1:= MousePosDat.D1;
    FDelay2:= MousePosDat.D2;
    end;

    FDatSrc.LoadData(FCentCrd - Round(300 / (FDatSrc.Header.ScanStep / 100)) - FMaxReduce[FReduction],
    FCentCrd + Round(300 / (FDatSrc.Header.ScanStep / 100)) + FMaxReduce[FReduction], 0, LoadBPEcho);

    with TTestForm1.Create(nil) do
    begin
    PaintBox1.Canvas.Brush.Color:= clWhite;
    PaintBox1.Canvas.FillRect(Rect(0, 0, 401, 256));
    FSelChIdx:= 1;
    for X_:= - 200 to 200 do
    for Y_:= 0 to 255 do
    if FEcho[FSelChIdx, X_, Y_] <> 0 then PaintBox1.Canvas.Pixels[X_ + 200, 255 - Y_]:= clBlack;
    PaintBox2.Canvas.Brush.Color:= clWhite;
    PaintBox2.Canvas.FillRect(Rect(0, 0, 401, 256));
    FSelChIdx:= 2;
    for X_:= - 200 to 200 do
    for Y_:= 0 to 255 do
    if FEcho[FSelChIdx, X_, Y_] <> 0 then PaintBox2.Canvas.Pixels[X_ + 200, 255 - Y_]:= clBlack;

    // Выбор канала

    CreatePointList(Pts1, 36, 0, FDelay1);
    CreatePointList(Pts2, 36, 0, FDelay2);

    FSelChIdx:= - 1;
    for I:= 0 to High(Pts1) do
    begin
    if FEcho[1, Pts1[I].X, Pts1[I].Y] <> 0 then
    begin
    FSelChIdx:= 1;
    X2:= Pts1[I].X;
    Y2:= Pts1[I].Y;
    Break;
    end;

    if FEcho[2, Pts2[I].X, Pts2[I].Y] <> 0 then
    begin
    FSelChIdx:= 2;
    X2:= Pts2[I].X;
    Y2:= Pts2[I].Y;
    Break;
    end;
    end;
    if FSelChIdx = - 1 then Exit;

    Button1.Caption:= IntToStr(FSelChIdx);

    SetLength(FPBPoints, 1);
    FPBPoints[0].X:= X2;
    FPBPoints[0].Y:= Y2;


    X:= X2;
    Y:= Y2;

    for I:= 1 to 2 do
    repeat
    X2:= X;
    Y2:= Y;

    if I = 1 then
    begin
    D:= 1;
    if (FSelChIdx = 1) then D2:= - 1
    else D2:= 1;
    end
    else
    begin
    D:= - 1;
    if FSelChIdx = 1 then D2:= 1
    else D2:= - 1;
    end;

    if FEcho[FSelChIdx, X + D, Y         ] <> 0 then X:= X + D else
    if FEcho[FSelChIdx, X + D, Y + 1 * D2] <> 0 then begin X:= X + D; Y:= Y + 1 * D2; end else
    if FEcho[FSelChIdx, X + D, Y + 2 * D2] <> 0 then begin X:= X + D; Y:= Y + 2 * D2; end else

    begin
    if I = 1 then D:= 2
    else D:= - 2;
    if FEcho[FSelChIdx, X + D, Y         ] <> 0 then X:= X + D else
    if FEcho[FSelChIdx, X + D, Y + 1 * D2] <> 0 then begin X:= X + D; Y:= Y + 1 * D2; end else
    if FEcho[FSelChIdx, X + D, Y + 2 * D2] <> 0 then begin X:= X + D; Y:= Y + 2 * D2; end else
    if FEcho[FSelChIdx, X + D, Y + 3 * D2] <> 0 then begin X:= X + D; Y:= Y + 3 * D2; end else

    begin
    if I = 1 then D:= 3
    else D:= - 3;
    if FEcho[FSelChIdx, X + D, Y         ] <> 0 then X:= X + D else
    if FEcho[FSelChIdx, X + D, Y + 1 * D2] <> 0 then begin X:= X + D2; Y:= Y + 1 * D2; end else
    if FEcho[FSelChIdx, X + D, Y + 2 * D2] <> 0 then begin X:= X + D2; Y:= Y + 2 * D2; end else
    if FEcho[FSelChIdx, X + D, Y + 3 * D2] <> 0 then begin X:= X + D2; Y:= Y + 3 * D2; end else
    if FEcho[FSelChIdx, X + D, Y + 4 * D2] <> 0 then begin X:= X + D2; Y:= Y + 4 * D2; end;
    end;
    end;

    if (X2 <> X) or (Y2 <> Y) then
    begin
    SetLength(FPBPoints, Length(FPBPoints) + 1);
    FPBPoints[High(FPBPoints)].X:= X;
    FPBPoints[High(FPBPoints)].Y:= Y;

    end;
    until ((X2 = X) and (Y2 = Y)) or (X < - 190) or (X > 190) or (Y < 5) or (Y > 250);

    for I:= 0 to High(FPBPoints) do
    begin
    PaintBox1.Canvas.Pixels[FPBPoints[I].X + 200, 255 - FPBPoints[I].Y - 4]:= clRed;
    PaintBox1.Canvas.Pixels[FPBPoints[I].X + 200, 255 - FPBPoints[I].Y + 4]:= clRed;
    end;

    //    ShowModal;
    Free;
    end;
    Refresh;
    *)
end;

procedure TAvk11Display.DblClick;
begin
  if not RailView then
    MaximizeTape(FOldX, FOldY);
  inherited DblClick;
end;

{
  procedure TAvk11View.CMMouseLeave(var Message: TMessage);
  begin
  //  pDelay.Caption:= ' Задержка: ---';
  end;
}
procedure TAvk11Display.WMGetDlgCode(var Msg: TWMGetDlgCode);
begin
  Msg.Result := DefWindowProc(Handle, Msg.Msg, TMessage(Msg).wParam, TMessage(Msg).lParam); // запрашиваем какие сообщения уже направляются нашему контролу
  Msg.Result := Msg.Result or DLGC_WANTARROWS;
  // добавляем к этому нажатия стрелок
end;

procedure TAvk11Display.CreateParams(var Params: TCreateParams);
const
  BorderStyles: array [TBorderStyle] of Longint = (0, WS_BORDER);

begin
  inherited CreateParams(Params);
  with Params do
  begin
    WindowClass.Style := WindowClass.Style or CS_HREDRAW or CS_VREDRAW;
    Style := Style or Longword(BorderStyles[FBorderStyle]);
  end;
end;
{
  procedure TAvk11View.CMFocusChanged(var Message: TCMFocusChanged);
  var
  Active: Boolean;
  begin
  with Message do Active := (Sender = Self);
  if Active <> FActive then
  begin
  FActive := Active;
  //  if FShowFocus then Invalidate;
  end;
  inherited;
  end;

  procedure TAvk11View.CMEnabledChanged(var Message: TMessage);
  begin
  inherited;
  //  Invalidate;
  end;

  procedure TAvk11View.CMTextChanged(var Message: TMessage);
  begin
  inherited;
  //  Invalidate;
  end;

  procedure TAvk11View.CMDialogChar(var Message: TCMDialogChar);
  begin
  if IsAccel(Message.CharCode, Caption) and CanFocus then begin
  SetFocus;
  Message.Result := 1;
  end;
  end;
}

constructor TAvk11Display.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FDrawSameCoordMode:= False;
  FDrawCount:= 0;

  ViewACState:= false;
  ShowSensor1DataFlag := False;
  FFFF := False;
  FDrawAirBrushIdx := 0;
  FDrawAlarmBrushIdx := 0;
  FFiltr := nil;
  FBMList := nil;
  FBSignal := TList.Create;

  FBeforeSgnExtDraw := nil;
  FAfterSgnExtDraw := nil;
  FExtendedViewDraw:= nil;
  FFilterMode := moNoCalc;

  MetallSensorValue0 := False;
  MetallSensorValue1 := False;


  // RecResList:= nil;
  // FSaveDisState:= False;
  // FViewZoneSel:= False;
  // ViewZonesOnlyOne:= False;
  // ShowMarkMode:= mmHide;
  // DrawZoneReset;

  FMouseMode := mmIdle;
  FLupaForm := nil;
  Show0CoordEvent := False;
  Show0EchoCountEvent := False;

  OnLostData := DDLostData;
  Visible := False;
  ControlStyle := [csClickEvents, csSetCaption, csCaptureMouse,
  { csOpaque, } csDoubleClicks];
  Width := 50;
  Height := 60;
  TabStop := True;
  Self.BevelOuter := bvNone;
  Self.BevelInner := bvNone;

  FEchoSizeByAmpl := False;
  FEchoColorByAmpl := False;
  // Создание классов расчета зон вывода
  FBScanBox := TBoxer.Create;
  FChInfoBox := TBoxer.Create;
  FChSettBox := TBoxer.Create;
  FChGateBox[rLeft] := TBoxer.Create;
  FChGateBox[rRight] := TBoxer.Create;

  Zoom := 500;
  CenterDisCoord := 0;

  FDM.AmplTh := 3;
  if (ConfigUnit.Config.ProgramMode = pmGEISMAR) then FDM.AmplTh := 6; // Для Жейсмар порог отображния, по умолчанию - 0 дБ

  FDM.Reduction := 0;
  FDM.ReductionMode := 1;
  FDM.RailView := False;
  FDM.AmplDon := False;
  SoundFlag := False;
  FStartDisCoord := -1;
  FEndDisCoord := -1;

  // SetViewMode(ddBothRail);
  FLineCoord := False;
  FOnSelHandScan := nil;
  FOnSelNotebook := nil;

  FViewLabels := True;

  FAssignList := TList.Create;

  FDM.ShowChGate := Config.ShowChGate;
  FDM.ShowChSettings := Config.ShowChSettings;
  FDM.ShowChInfo := Config.ShowChInfo;
end;

destructor TAvk11Display.Destroy;
var
  I: Integer;

begin
  FinDirectDraw;
  inherited Destroy;

  FBScanBox.Free;
  FChInfoBox.Free;
  FChSettBox.Free;
  FChGateBox[rLeft].Free;
  FChGateBox[rRight].Free;

  FBScanBox := nil;
  FChInfoBox := nil;
  FChSettBox := nil;
  FChGateBox[rLeft] := nil;
  FChGateBox[rRight] := nil;

  FBSignal.Free;
  FBSignal:= nil;


  // SetLength(FDrawZone, 0);
  SetLength(InfoRect, 0);
  // for I:= 0 to High(ViewZones) do SetLength(ViewZones[I].Items, 0);
  // SetLength(ViewZones, 0);
  FAssignList.Free;
end;

procedure TAvk11Display.FiltrCalc;
var
  RecProgForm: TRecProgressForm;
  ZPChList: array of Integer;
  NoZPChList: array of Integer;

begin

//    if not Assigned(FBSignal) then
    if FBSignal.Count = 0 then
    begin
      RecProgForm:= TRecProgressForm.Create(nil);
      RecProgForm.SetMode(1);
      RecProgForm.Visible:= True;
      RecProgForm.BringToFront;
      RecProgForm.Caption:= LangTable.Caption['Common:Wait'];
      RecProgForm.Refresh;

      if CompareMem(@FDatSrc.Header.DeviceID, @EGO_USWScheme1, 8) then
      begin
        RecProgForm.SetDiap(0, 25);
        FBSignal.Add(TBSignal.Create);
        TBSignal(FBSignal[0]).OnProgress:= RecProgForm.OnProgress;
        TBSignal(FBSignal[0]).Ch:= 1;
        TBSignal(FBSignal[0]).Calc(FDatSrc, 0, Self.DatSrc.MaxDisCoord);

        RecProgForm.SetDiap(25, 50);
        FBSignal.Add(TBSignal.Create);
        TBSignal(FBSignal[1]).OnProgress:= RecProgForm.OnProgress;
        TBSignal(FBSignal[1]).Ch:= 11;
        TBSignal(FBSignal[1]).Calc(FDatSrc, 0, Self.DatSrc.MaxDisCoord);
      end
      else
      begin
        RecProgForm.SetDiap(0, 50);
        FBSignal.Add(TBSignal.Create);
        TBSignal(FBSignal[0]).OnProgress:= RecProgForm.OnProgress;
        TBSignal(FBSignal[0]).Ch:= 1;
        TBSignal(FBSignal[0]).Calc(FDatSrc, 0, Self.DatSrc.MaxDisCoord);
      end;


      FBMList:= TBMList.Create;
      TBMList(FBMList).Calc(Self.DatSrc);

      RecProgForm.SetDiap(50, 100);
      FFiltr:= TFiltr.Create;
      FFiltr.OnProgress:= RecProgForm.OnProgress;


      if CompareMem(@FDatSrc.Header.DeviceID, @EGO_USWScheme1, 8) then
      begin
        SetLength(ZPChList, 2);
        ZPChList[0] := 1;
        ZPChList[1] := 11;
        SetLength(NoZPChList, 8);
        NoZPChList[0]:= 2;
        NoZPChList[1]:= 3;
        NoZPChList[2]:= 4;
        NoZPChList[3]:= 5;
        NoZPChList[4]:= 6;
        NoZPChList[5]:= 7;
        NoZPChList[6]:= 12;
        NoZPChList[7]:= 13;
      end
      else
      if CompareMem(@FDatSrc.Header.DeviceID, @VMT_US_BigWP, 8) or
         CompareMem(@FDatSrc.Header.DeviceID, @FilusX17DW, 8) then
      begin
        SetLength(ZPChList, 3);
        ZPChList[0] := 1;
        ZPChList[1] := 2;
        ZPChList[2] := 3;
        SetLength(NoZPChList, 8);
        NoZPChList[0]:= 4;
        NoZPChList[1]:= 5;
        NoZPChList[2]:= 6;
        NoZPChList[3]:= 7;
        NoZPChList[4]:= 10;
        NoZPChList[5]:= 11;
        NoZPChList[6]:= 12;
        NoZPChList[7]:= 13;
      end
      else
      begin
        SetLength(ZPChList, 1);
        ZPChList[0] := 1;
        SetLength(NoZPChList, 6);
        NoZPChList[0]:= 2;
        NoZPChList[1]:= 3;
        NoZPChList[2]:= 4;
        NoZPChList[3]:= 5;
        NoZPChList[4]:= 6;
        NoZPChList[5]:= 7;
      end;

      FFiltr.Calc(Self.DatSrc, FBSignal, FBMList, 0, Self.DatSrc.MaxDisCoord, ZPChList, NoZPChList);
      RecProgForm.Free;
      FFiltr.OnProgress:= nil;

      FilterMode:= moMark;
    end
    else
      case FFilterMode of
      moOff: FilterMode:= moMark;
      moMark: FilterMode:= moHide;
      moHide: FilterMode:= moOff;
      end;
    Refresh;
end;

procedure TAvk11Display.DDLostData(Sender: TObject);
begin
  Refresh;
end;

procedure TAvk11Display.SetData(Parent: TWinControl; ScrollBar: TScrollBar; DataSource: TAvk11DatSrc);
var
  I: Integer;
  ImageForm: TImageForm;
  BMP: TBitMap;
  R: Integer;

  procedure GreateBG(Color: TColor; var BMP1: TBitMap);
  var
    BMP2: TBitMap;
    X, Y: Integer;

  begin
    BMP1 := TBitMap.Create;
    BMP1.Width := 2000;
    BMP1.Height := 2000;

    BMP2 := TBitMap.Create;
    BMP2.Width := 20;
    BMP2.Height := 20;

    for X := 0 to 10 do
      for Y := 0 to 10 do
        BMP2.Canvas.Pixels[X * 2, Y * 2] := Color;

    for X := 0 to 100 do
      for Y := 0 to 100 do
        BMP1.Canvas.Draw(X * 20, Y * 20, BMP2);

    BMP2.Free;
  end;

begin
  Self.Parent := Parent;
  Self.DatSrc := DataSource;
  (*
    {$IFDEF AVIKON16}
    case DataSource.Header.Reserved of
    1: FReductionMode:= 1;
    2: FReductionMode:= 2;
    end;
    {$ENDIF}
    *)

  if Parent <> nil then
  begin
    Self.Width := Parent.Width;
    Self.Height := Parent.Height;
  end;

  InitDirectDraw;
  LoadFont(LangTable.Caption['General:FontName'], 15, False, FDispCols.Text, FDispCols.Fon, False, True);
  LoadFont(LangTable.Caption['General:FontName'], 15, False, FDispCols.Text, FDispCols.Event, True, True);
  LoadFont(LangTable.Caption['General:FontName'], 15, False, FDispCols.Text, FDispCols.Defect, True, True);
  LoadFont(LangTable.Caption['General:FontName'], 15, False, FDispCols.Text, FDispCols.Defect, True, True);
  LoadFont(LangTable.Caption['General:FontName'], 15, False, FDispCols.Text, FDispCols.Defect, True, True);

  MixColor := False;
  // MixColor1:= GetDDColor(clRed);
  // MixColor2:= GetDDColor(clBlue);
  // ResColor:=  GetDDColor(clBlack);

  SetScrollBar(ScrollBar);

  {
    ImageForm:= TImageForm.Create(nil);
    for I:= 0 to 255 do FPicIdx[I]:= - 1;
    FPicIdx[EID_HandScan    ]:= AddPic(ImageForm.Image5. Picture.Bitmap, True); // Ручник
    FPicIdx[EID_Sens        ]:= AddPic(ImageForm.Image12.Picture.Bitmap, True); // Изменение условной чувствительности
    FPicIdx[EID_Att         ]:= AddPic(ImageForm.Image2 .Picture.Bitmap, True); // Изменение положения нуля аттенюатора (0 дБ условной чувствительности)
    FPicIdx[EID_VRU         ]:= AddPic(ImageForm.Image16.Picture.Bitmap, True); // Изменение ВРЧ
    FPicIdx[EID_StStr       ]:= AddPic(ImageForm.Image13.Picture.Bitmap, True); // Изменение положения начала строба
    FPicIdx[EID_EndStr      ]:= AddPic(ImageForm.Image13.Picture.Bitmap, True); // Изменение положения конца строба
    FPicIdx[EID_HeadPh      ]:= AddPic(ImageForm.Image18.Picture.Bitmap, True); // Список включенных наушников
    FPicIdx[EID_Mode        ]:= AddPic(ImageForm.Image7 .Picture.Bitmap, True); // Изменение режима
    FPicIdx[EID_2Tp         ]:= AddPic(ImageForm.Image1 .Picture.Bitmap, True); // Изменение 2Тп
    FPicIdx[EID_ZondImp     ]:= AddPic(ImageForm.Image17.Picture.Bitmap, True); // Изменение уровня зондирующего импульса
    FPicIdx[EID_Stolb       ]:= AddPic(ImageForm.Image11.Picture.Bitmap, True); // Отметка координаты
    FPicIdx[EID_Strelka     ]:= AddPic(ImageForm.Image1 .Picture.Bitmap, True); // Номер стрелочного перевода + Время
    FPicIdx[EID_DefLabel    ]:= AddPic(ImageForm.Image4 .Picture.Bitmap, True); // Метка дефекта + Время
    FPicIdx[EID_TextLabel   ]:= AddPic(ImageForm.Image4 .Picture.Bitmap, True); // Особая отметка (текстовая) + Время
    FPicIdx[EID_StBoltStyk  ]:= AddPic(ImageForm.Image3 .Picture.Bitmap, True); // Нажатие кнопки БС + Время
    FPicIdx[EID_EndBoltStyk ]:= AddPic(ImageForm.Image3 .Picture.Bitmap, True); // Оптускание кнопки БС + Время
    FPicIdx[EID_Time        ]:= AddPic(ImageForm.Image4 .Picture.Bitmap, True); // Отметка времени
    FPicIdx[EID_StLineLabel ]:= AddPic(ImageForm.Image1 .Picture.Bitmap, True); // Начало протяженной метки + Время
    FPicIdx[EID_EndLineLabel]:= AddPic(ImageForm.Image1 .Picture.Bitmap, True); // Конец протяженной метки + Время
    FPicIdx[EID_StSwarStyk  ]:= AddPic(ImageForm.Image14.Picture.Bitmap, True); // Нажатие кнопки сварной стык + Время
    FPicIdx[EID_EndSwarStyk ]:= AddPic(ImageForm.Image14.Picture.Bitmap, True); // Отпускание кнопки сварной стык + Время
    FPicIdx[EID_OpChange    ]:= AddPic(ImageForm.Image9 .Picture.Bitmap, True); // Смена оператора
    FPicIdx[EID_PathChange  ]:= AddPic(ImageForm.Image10.Picture.Bitmap, True); // Смена номера пути
    FPicIdx[EID_MovDirChange]:= AddPic(ImageForm.Image8 .Picture.Bitmap, True); // Смена направления движения
    FPicIdx[EID_SezdNo      ]:= AddPic(ImageForm.Image1 .Picture.Bitmap, True); // Номер съезда
    }
  ImageForm := TImageForm.Create(nil);
  piNotebook := AddPic(ImageForm.NotebookImage.Picture.Bitmap, True);
  piFail := AddPic(ImageForm.Image4.Picture.Bitmap, True);
  piInfo := AddPic(ImageForm.InfoImage.Picture.Bitmap, True);
  piHandScan := AddPic(ImageForm.HandScanImage.Picture.Bitmap, True);
  piRecImage := AddPic(ImageForm.RecImage.Picture.Bitmap, True);
  piOK := AddPic(ImageForm.Image21.Picture.Bitmap, True);
  // piDefImage:= AddPic(ImageForm.RecImage.Picture.Bitmap, True);

  AddPic(ImageForm.Image19.Picture.Bitmap, True);
  AddPic(ImageForm.Image20.Picture.Bitmap, True);
  {
    GreateBG($004AFF7C, BMP);
    I:= AddPic(BMP, True);
    BMP.Free;

    GreateBG($006A66FF, BMP);
    I:= AddPic(BMP, True);
    BMP.Free;

    GreateBG(clYellow, BMP);
    I:= AddPic(BMP, True);
    BMP.Free;

    GreateBG(clBlack, BMP);
    I:= AddPic(BMP, True);
    BMP.Free;
    }
  ImageForm.Free;
end;

function TAvk11Display.GetSkipBM: Boolean;
begin
  Result := FDatSrc.SkipBackMotion;
end;

function TAvk11Display.GetViewAC: Boolean;
begin
  Result := ViewACState;
end;

procedure TAvk11Display.SetViewAC(NewState: Boolean);
begin
  ViewACState:= NewState;
end;

procedure TAvk11Display.SetSkipBM(NewState: Boolean);
var
  NewCoord: Integer;

begin
  if NewState = FDatSrc.SkipBackMotion then
    Exit;

  if NewState then
    NewCoord := FDatSrc.DisToSysCoord(FCenterDisCoord);

  FDatSrc.SkipBackMotion := NewState;

  if not NewState then
    NewCoord := FDatSrc.SysToDisCoord(FCenterDisCoord);

  SetScrollBar(FScrollBar);
  SetCenterDisCoord(NewCoord);
  Refresh;
end;

procedure TAvk11Display.SetExtendedView(Percents: array of Integer);
var
  I: Integer;

begin
  FExtendedView:= Length(Percents) <> 0;
  SetLength(FExtendedViewList, Length(Percents));
  for I:= 0 to Length(FExtendedViewList) - 1 do
    FExtendedViewList[I]:= Percents[I];
  Resize;
end;

procedure TAvk11Display.SetViewMode(New: TBScanViewMode; ViewRail: TRail; ViewLine: Integer);
begin
  FDM.ViewMode := New;
  FDM.ViewRail := ViewRail;
  FDM.ViewLine := ViewLine;
end;

procedure TAvk11Display.SetViewMode(New: TBScanViewMode; ViewRail: TRail);
begin
  FDM.ViewMode := New;
  FDM.ViewRail := ViewRail;
end;

procedure TAvk11Display.SetViewMode(New: TBScanViewMode);
begin
  FDM.ViewMode := New;
end;


procedure TAvk11Display.SetZoom(New: Integer);
begin
  FDM.Zoom := New;
  ReEchoSize;
end;

procedure TAvk11Display.SetCenterDisCoord(New: Integer);
begin
  FCenterDisCoord := New;
  if Assigned(FScrollBar) then
    FScrollBar.Position := FCenterDisCoord;
end;

procedure TAvk11Display.SetRightCoord(New: Integer);
begin
  FCenterDisCoord := New - LastDrawWidth * FDM.Zoom div FDatSrc.Header.ScanStep div 4;
  // Refresh;
end;

procedure TAvk11Display.SetLeftCoord(New: Integer);
begin
  FCenterDisCoord := New + LastDrawWidth * FDM.Zoom div FDatSrc.Header.ScanStep div 4;
  // Refresh;
end;

procedure TAvk11Display.SetReduction(New: Integer);
begin
  FDM.Reduction := New;
  DoAssignList;
end;

procedure TAvk11Display.SetReductionMode(New: Integer);
begin
  FDM.ReductionMode := New;
  DoAssignList;
end;

procedure TAvk11Display.SetViewChannel(New: TViewChannel);
begin
  FDM.ViewChannel := New;
  DoAssignList;
end;

procedure TAvk11Display.SetSoundFlag(NewFlag: Boolean);
begin
  FSoundFlag := NewFlag;
  if NewFlag then
  begin
    SetMouseMode(mmSelRailPoint);
    SetReduction(0);
    SetRailView(False);
    Refresh;
  end
  else
  begin
    SetMouseMode(mmIdle);
    StopTone;
  end;
end;

procedure TAvk11Display.SetRailView(New: Boolean);
begin
  FDM.RailView := New;
  if FDatSrc.Config.MaxRail = rLeft then
    FDM.ViewMode := vmOneRailLines;

  // SetViewMode(FDM.ViewMode);
  DoAssignList;
end;

procedure TAvk11Display.SetAmplTh(New: Integer);
begin
  FDM.AmplTh := New;
  DoAssignList;
end;

procedure TAvk11Display.SetAmplDon(New: Boolean);
begin
  FDM.AmplDon := New;
  DoAssignList;
end;

procedure TAvk11Display.SetShowChSettings(NewState: Boolean);
begin
  FDM.ShowChSettings := NewState;
  FullRefresh;
end;

(*
  function TAvk11Display.GetShowChGate: Boolean;
  begin
  {  if not HeightToSmall then} Result:= FDM.ShowChGate
  //                       else Result:= FSaveViewMode.ShowChGate;
  end;

  function TAvk11Display.GetShowChInfo: Boolean;
  begin
  {  if not HeightToSmall then} Result:= FDM.ShowChInfo
  //                       else Result:= FSaveViewMode.ShowChInfo;
  end;
*)
procedure TAvk11Display.SetShowChGate(NewState: Boolean);
begin
  // if not SaveSizeFlag then
  // begin
  FDM.ShowChGate := NewState;
  FullRefresh;
  // end
  // else FSaveViewMode.ShowChGate:= NewState;
end;

procedure TAvk11Display.SetShowChInfo(NewState: Boolean);
begin
  // if not SaveSizeFlag then
  // begin
  FDM.ShowChInfo := NewState;
  FullRefresh;
  // end
  // else FSaveViewMode.ShowChInfo:= NewState;
end;

procedure TAvk11Display.SetMouseMode(New: TMouseMode);
begin
  FMouseMode := New;
  case New of
    mmIdle:
      begin
        DrawSameCoord:= True;
        DrawCentrCur := False;
        DrawSelRect := False;
        Flip;
      end;
    mmSelRailPoint:
      DrawCentrCur := True;
    mmSetRect:
      DrawSelRect := True;
  end;
  if Assigned(FOnChangeMouseMode) then
    FOnChangeMouseMode(Self);
  DoAssignList;
end;

function TAvk11Display.DisplayMode_ShowChGate: Boolean;
begin
  if DisplayMode.RailView then
    Result := False
  else
    Result := DisplayMode.ShowChGate;
end;

function TAvk11Display.DisplayMode_ShowChSettings: Boolean;
begin
  if DisplayMode.RailView then
    Result := False
  else
    Result := DisplayMode.ShowChSettings;
end;

function TAvk11Display.DisplayMode_ShowChInfo: Boolean;
begin
  if DisplayMode.RailView then
    Result := False
  else
    Result := DisplayMode.ShowChInfo;
end;

procedure TAvk11Display.ReCalcOutRect;
var
  R_: TRail;
  RTop, RBottom: TRail;
  I, J, Ch, DrawCol: Integer;
  Line: Integer;
  Line2: Integer;
  FullLine: Integer;
  Item: Integer;

  ScanChIdx: Integer;
  EvalChListIdx: Integer;
  EvalChList: TIntegerDynArray;

  First_: Integer;
  Second_: Integer;
  tmp: Integer;
  RailLine: Integer;

  // LeftGateColIdx: Integer;
  // RightGateColIdx: Integer;
  // BScanColIdx: Integer;
  // InfoColIdx: Integer;
  {
    StartRail: TRail;
    EndRail: TRail;
    StartLine: Integer;
    EndLine: Integer;
    }
begin

  if not Assigned(FBScanBox) then Exit;
  if not Assigned(FDatSrc) then Exit;

  // BScanColIdx:= 0;
  // LeftGateColIdx:= 0;
  // RightGateColIdx:= 0;
  // InfoColIdx:= 0;
  // if FDM.ShowChGate then
  // begin
  // LeftGateColIdx:= 0;
  // BScanColIdx:= 1;
  // RightGateColIdx:= 2;
  // end;
  // if FDM.ShowChSettings then
  // begin
  // Inc(BScanColIdx);
  // Inc(LeftGateColIdx);
  // Inc(RightGateColIdx);
  // end;

  SaveWidth := Width;

  if (not HeightToSmall) and (ViewMode <> vmOneLine) and (Height < 500) { and
    (FDM.ShowChSettings or FDM.ShowChInfo) } then
  begin
    // SaveShowChSettings:= FDM.ShowChSettings;
    // SaveShowChInfo:= FDM.ShowChInfo;
    HeightToSmall := True;
    // FDM.ShowChSettings:= False;
    // FDM.ShowChInfo:= False;
    ApplyDisplayMode;
  end;

  if HeightToSmall and (ViewMode <> vmOneLine) and (Height > 500) then
  begin
    HeightToSmall := False;
    // FDM.ShowChSettings:= SaveShowChSettings;
    // FDM.ShowChInfo:= SaveShowChInfo;
    ApplyDisplayMode;
  end;

  // Установка размеров зон вывода
  FChInfoBox.SetSizeHeight(Height - 2, 0);
  FChSettBox.SetSizeHeight(Height - 2, 0);
  FChGateBox[rLeft].SetSizeHeight(Height - 2, 0);
  FChGateBox[rRight].SetSizeHeight(Height - 2, 0);
  FBScanBox.SetSize(Width - 2 - (FChInfoBox.ColPixelWidth + FChSettBox.ColPixelWidth + 2 * FChGateBox[rLeft].ColPixelWidth), Height - 2, 0);
  FBScanWidth := Width - 2 - (FChInfoBox.ColPixelWidth + FChSettBox.ColPixelWidth + 2 * FChGateBox[rLeft].ColPixelWidth);

  {
    if FDM.ShowChSettings or FDM.ShowChGate then
    begin
    FullLine:= 0;
    FChInfoBox.SetSize(FDrawBox.GetRect(InfoColIdx, FullLine).Right - FDrawBox.GetRect(InfoColIdx, FullLine).Left, 100, 1);
    end;

    if FDM.ShowChSettings or FDM.ShowChGate then
    begin
    FullLine:= 0;
    FChSettBox.SetSize(FDrawBox.GetRect(InfoColIdx, FullLine).Right - FDrawBox.GetRect(InfoColIdx, FullLine).Left, 100, 1);
    end;
    }                                              // Заполнение зон вывода
  SetLength(FBScanLines, 0);

  if not DisplayMode.RailView then
  begin
    FullLine := 0;
    for Line := FStartLine to FEndLine do // Строка В-развертки
    begin
      if FullLine = CoordRectRowIdx then Inc(FullLine);

      SetLength(FBScanLines, Length(FBScanLines) + 1);

      if Length(FBScanLines) > 1 then
        if FDatSrc.Config.BScanLineItem[Line].R <> FBScanLines[High(FBScanLines) - 1].Rail then RailLine:= 0;

      FBScanLines[ High(FBScanLines)].Rail := FDatSrc.Config.BScanLineItem[Line].R;

      // if FDatSrc.Header.UsedItems[uiWorkRailTypeA] = 1 // Контролируемая нить - для однониточных приборов
      // then FBScanLines[High(FBScanLines)].Rail:= rLeft
      // else FBScanLines[High(FBScanLines)].Rail:= rRight;

      FBScanLines[ High(FBScanLines)].Line := Line;
      FBScanLines[ High(FBScanLines)].RailLine:= RailLine;
      RailLine:= RailLine + 1;
      FBScanLines[ High(FBScanLines)].MirrorShadow := False;
      FBScanLines[ High(FBScanLines)].MirrorShadowEvalChNum[0] := -1;
      FBScanLines[ High(FBScanLines)].MirrorShadowEvalChNum[1] := -1;
      FBScanLines[ High(FBScanLines)].MirrorShadowScanChNum[0] := -1;
      FBScanLines[ High(FBScanLines)].MirrorShadowScanChNum[1] := -1;
      FBScanLines[ High(FBScanLines)].ZerroProbEchoEvalChNum[0] := -1;
      FBScanLines[ High(FBScanLines)].ZerroProbEchoEvalChNum[1] := -1;

      if DisplayMode_ShowChGate { and not HeightToSmall } then
      begin
        FBScanLines[ High(FBScanLines)].ChGateRt[rLeft] := StepIn(FChGateBox[rLeft].GetRect(0, FullLine));
        FBScanLines[ High(FBScanLines)].ChGateRt[rRight] := StepIn(FChGateBox[rRight].GetRect(0, FullLine));
      end;

      if DisplayMode_ShowChSettings and not HeightToSmall then
        for I := 0 to 3 do
          FBScanLines[ High(FBScanLines)].ChSettRt[I] := StepIn(FChSettBox.GetRect(I, FullLine));

      if DisplayMode_ShowChInfo and not HeightToSmall then
        for I := 0 to 3 do
          FBScanLines[ High(FBScanLines)].ChInfoRt[I] := StepIn(FChInfoBox.GetRect(I, FullLine));


      // Перенос списка каналов

      SetLength(FBScanLines[ High(FBScanLines)].ScanChNumList, Length(FDatSrc.Config.BScanLineItem[Line].Items));
      SetLength(FBScanLines[ High(FBScanLines)].ScanChBScanGateMin, Length(FDatSrc.Config.BScanLineItem[Line].Items));
      SetLength(FBScanLines[ High(FBScanLines)].ScanChBScanGateMax, Length(FDatSrc.Config.BScanLineItem[Line].Items));
      SetLength(FBScanLines[ High(FBScanLines)].ScanChBScanGateLen, Length(FDatSrc.Config.BScanLineItem[Line].Items));
      // SetLength(FBScanLines[High(FBScanLines)].ScanChOrder, Length(FDatSrc.Config.BScanLineItem[Line].Items));

      FBScanLines[ High(FBScanLines)].BScanRt := StepIn(FBScanBox.GetRect(0, FullLine));
      for Item := 0 to High(FDatSrc.Config.BScanLineItem[Line].Items) do
      begin
        ScanChIdx := FDatSrc.Config.BScanLineItem[Line].Items[Item].ScanChIdx;
        if ScanChIdx = - 1 then Continue;

        // Перенос каналов сканирования
        with FBScanLines[ High(FBScanLines)] do
        begin
          // SetLength(ScanChNumList, Length(ScanChNumList) + 1);
          // ScanChOrder[Item]:= FDatSrc.Config.BScanLineItem[Line].Items[Item].Order;
          ScanChNumList[Item] := FDatSrc.Config.ScanChannelByIdx[ScanChIdx].BS_Ch_File_Number;
          ScanChBScanGateMin[Item] := FDatSrc.Config.ScanChannelByIdx[ScanChIdx].BScanGateMin;
          ScanChBScanGateMax[Item] := FDatSrc.Config.ScanChannelByIdx[ScanChIdx].BScanGateMax;
          ScanChBScanGateLen[Item] := FDatSrc.Config.ScanChannelByIdx[ScanChIdx].BScanDuration;
          ScanChToIndex[FDatSrc.Config.ScanChannelByIdx[ScanChIdx].BS_Ch_File_Number]:= Item;
        end;

        // Перенос каналов оценки
        EvalChList := FDatSrc.Config.GetEvalsChIdxByScanChIdx(ScanChIdx);

        with FBScanLines[ High(FBScanLines)] do
          for EvalChListIdx := 0 to High(EvalChList) do
          begin
            SetLength(EvalChNumList, Length(EvalChNumList) + 1);
            EvalChNumList[ High(EvalChNumList)] := FDatSrc.Config.EvalChannelByIdx[EvalChList[EvalChListIdx]].Number;
            // Определение наличия ЗТМ канала
            if FDatSrc.Config.EvalChannelByIdx[EvalChList[EvalChListIdx]].Method = imMirrorShadow then
            begin
              MirrorShadow := True;
              if FBScanLines[ High(FBScanLines)].MirrorShadowEvalChNum[0] = -1 then
                FBScanLines[ High(FBScanLines)].MirrorShadowEvalChNum[0] := EvalChNumList[ High(EvalChNumList)]
              else
                FBScanLines[ High(FBScanLines)].MirrorShadowEvalChNum[1] := EvalChNumList[ High(EvalChNumList)];
            end
            else
            begin
              if FBScanLines[ High(FBScanLines)].ZerroProbEchoEvalChNum[0] = -1 then
                FBScanLines[ High(FBScanLines)].ZerroProbEchoEvalChNum[0] := EvalChNumList[ High(EvalChNumList)]
              else
                FBScanLines[ High(FBScanLines)].ZerroProbEchoEvalChNum[1] := EvalChNumList[ High(EvalChNumList)];
            end;
          end;
      end;
      Inc(FullLine);
    end;
  end
  else
  begin
    // FullLine:= 0;
    // FStartRail: TRail;
    // FEndRail: TRail;

    for R_ := FStartRail to FEndRail do // Строка В-развертки
    begin
      if FullLine = CoordRectRowIdx then
        Inc(FullLine);

      SetLength(FBScanLines, Length(FBScanLines) + 1);
      FBScanLines[ High(FBScanLines)].Rail := R_;
      if (FStartRail = rLeft) then
        FBScanLines[ High(FBScanLines)].Line := Ord(R_)
      else
        FBScanLines[ High(FBScanLines)].Line := 0;
      // FBScanLines[High(FBScanLines)].MirrorShadow:= False;
      FBScanLines[ High(FBScanLines)].MirrorShadowEvalChNum[0] := -1;
      FBScanLines[ High(FBScanLines)].MirrorShadowEvalChNum[1] := -1;
      FBScanLines[ High(FBScanLines)].MirrorShadowScanChNum[0] := -1;
      FBScanLines[ High(FBScanLines)].MirrorShadowScanChNum[1] := -1;
      FBScanLines[ High(FBScanLines)].ZerroProbEchoEvalChNum[0] := -1;
      FBScanLines[ High(FBScanLines)].ZerroProbEchoEvalChNum[1] := -1;

      FBScanLines[ High(FBScanLines)].MirrorShadow := True;
      FBScanLines[ High(FBScanLines)].BScanRt := StepIn(FBScanBox.GetRect(0, FullLine));

      for Line2 := 0 to FDatSrc.Config.BScanLinesCount - 1 do
        for Item := 0 to High(FDatSrc.Config.BScanLineItem[Line2].Items) do
          if FDatSrc.Config.BScanLineItem[Line2].R = R_ then
          begin
            ScanChIdx := FDatSrc.Config.BScanLineItem[Line2].Items[Item].ScanChIdx;

            with FBScanLines[ High(FBScanLines)] do
              if ScanChIdx <> - 1 then
              begin
                SetLength(ScanChNumList, Length(ScanChNumList) + 1);
                ScanChNumList[ High(ScanChNumList)] := FDatSrc.Config.ScanChannelByIdx[ScanChIdx].BS_Ch_File_Number;
                if Item >= 2 then
                  ScanChNumList[ High(ScanChNumList)] := -ScanChNumList[ High(ScanChNumList)];
              end;

            // Перенос каналов оценки
            EvalChList := FDatSrc.Config.GetEvalsChIdxByScanChIdx(ScanChIdx);

            with FBScanLines[ High(FBScanLines)] do
              for EvalChListIdx := 0 to High(EvalChList) do
              begin
                SetLength(EvalChNumList, Length(EvalChNumList) + 1);
                EvalChNumList[ High(EvalChNumList)] := FDatSrc.Config.EvalChannelByIdx[EvalChList[EvalChListIdx]].Number;
                if FDatSrc.Config.EvalChannelByIdx[EvalChList[EvalChListIdx]].Method = imMirrorShadow then
                begin
                  MirrorShadow := True;

                  if FBScanLines[ High(FBScanLines)].MirrorShadowEvalChNum[0] = -1 then
                    FBScanLines[ High(FBScanLines)].MirrorShadowEvalChNum[0] := EvalChNumList[ High(EvalChNumList)]
                  else
                    FBScanLines[ High(FBScanLines)].MirrorShadowEvalChNum[1] := EvalChNumList[ High(EvalChNumList)];

                  if FBScanLines[ High(FBScanLines)].MirrorShadowScanChNum[0] = -1 then
                    FBScanLines[ High(FBScanLines)].MirrorShadowScanChNum[0] := FDatSrc.Config.ScanChannelByIdx[ScanChIdx].BS_Ch_File_Number
                  else
                    FBScanLines[ High(FBScanLines)].MirrorShadowScanChNum[1] := FDatSrc.Config.ScanChannelByIdx[ScanChIdx].BS_Ch_File_Number;

                  FDatSrc.Config.GetSecondEvalByEval(EvalChList[EvalChListIdx], tmp);

                  if FBScanLines[ High(FBScanLines)].ZerroProbEchoEvalChNum[0] = -1 then
                    FBScanLines[ High(FBScanLines)].ZerroProbEchoEvalChNum[0] := FDatSrc.Config.EvalChannelByIdx[tmp].Number
                  else
                    FBScanLines[ High(FBScanLines)].ZerroProbEchoEvalChNum[1] := FDatSrc.Config.EvalChannelByIdx[tmp].Number;

                end;
                { if FDatSrc.Config.EvalChannelByIdx[EvalChList[EvalChListIdx]].Method = imEcho then
                  begin
                  if FBScanLines[High(FBScanLines)].ZerroProbEchoEvalChNum[0] = - 1
                  then FBScanLines[High(FBScanLines)].ZerroProbEchoEvalChNum[0]:= EvalChNumList[High(EvalChNumList)]
                  else FBScanLines[High(FBScanLines)].ZerroProbEchoEvalChNum[1]:= EvalChNumList[High(EvalChNumList)];
                  end; }
              end;
          end;
      Inc(FullLine);
    end;
  end;
  /// ///////////////////////////////////////////////////

  for I := 0 to Length(FBScanLines) - 1 do // Строка В-развертки
  begin
    J := 0;

    // FBScanBox: TBoxer;
    // FChInfoBox: TBoxer;
    // FChSettBox: TBoxer;
    // FChGateBox: array [TRail] of TBoxer;

    if DisplayMode_ShowChInfo and not HeightToSmall then
      J := FChInfoBox.GetBounds.Right;
    if DisplayMode_ShowChSettings and not HeightToSmall then
    begin
      FChSettBox.StartX := J;
      J := J + FChSettBox.GetBounds.Right;
    end;
    if DisplayMode_ShowChGate { and not HeightToSmall } then
    begin
      FChGateBox[rLeft].StartX := J;
      J := J + FChGateBox[rLeft].GetBounds.Right;
    end;
    FBScanBox.StartX := J;
    J := J + FBScanBox.GetBounds.Right;
    if DisplayMode_ShowChGate { and not HeightToSmall } then
      FChGateBox[rRight].StartX := J;
  end;

  /// ///////////////////////////////////////////////////

  FFCoordRect := StepIn(FBScanBox.GetRect(0, CoordRectRowIdx));
  FFCoordRect.Left := FFCoordRect.Left + 1;

  if DisplayMode_ShowChGate { and not HeightToSmall } then
  begin
    FChGateRect[rLeft] := StepIn(FChGateBox[rLeft].GetRect(0, CoordRectRowIdx));
    FChGateRect[rRight] := StepIn(FChGateBox[rRight].GetRect(0, CoordRectRowIdx));
  end;
  { else
    begin
    FChGateRect[rLeft]:= FFCoordRect;
    FChGateRect[rRight]:= FFCoordRect;
    FChGateRect[rLeft].Right:= FChGateRect[rLeft].Left + 35;
    FChGateRect[rRight].Left:= FChGateRect[rRight].Right - 35;
    FFCoordRect.Left:= FChGateRect[rLeft].Right;
    FFCoordRect.Right:= FChGateRect[rRight].Left;
    end; }

  // if FDM.ShowChSettings then FChSettRect:= StepIn(FChSettBox.GetRect(0, CoordRectRowIdx));
  // if FDM.ShowChInfo then FChInfoRect:= StepIn(FChInfoBox.GetRect(0, CoordRectRowIdx));

  for R_ := rLeft to rRight do
    for I := 0 to 255 do
      FScanChNumToBScanLine[R_, I] := -1;

  for I := 0 to High(FBScanLines) do
    for J := 0 to High(FBScanLines[I].ScanChNumList) do
      FScanChNumToBScanLine[FBScanLines[I].Rail, Abs(FBScanLines[I].ScanChNumList[J])] := I;

  BoundsRect := Rect(FBScanLines[0].BScanRt.Left, FBScanLines[0].BScanRt.Top, FBScanLines[0].BScanRt.Right, FBScanLines[ High(FBScanLines)].BScanRt.Bottom);

  for R_ := rLeft to rRight do
  begin
    First_ := -1;
    Second_ := -1;
    for I := 0 to High(FBScanLines) do
    begin
      if (First_ = -1) and (FBScanLines[I].Rail = R_) then
        First_ := FBScanLines[I].BScanRt.Top;
      if FBScanLines[I].Rail = R_ then
        Second_ := FBScanLines[I].BScanRt.Top;
    end;
    BoundsRect_[R_].Left := FBScanLines[0].BScanRt.Left;
    BoundsRect_[R_].Right := FBScanLines[0].BScanRt.Right;
    BoundsRect_[R_].Top := First_;
    BoundsRect_[R_].Bottom := Second_;
  end;

  // FDatSrc.Config.BScanLinesCount
  // FDrawBox.SetSize(Width - 2, Height - 2, 0);
  {
    if FDatSrc.SideToRail(r_Left) = rLeft then
    begin
    }
  RTop := rLeft;
  RBottom := rRight;
  { end
    else
    begin
    RTop:= rRight;
    RBottom:= rLeft;
    end;
    }
  ReEchoSize;
end;

procedure TAvk11Display.ReEchoSize;
var
  I: Integer;
  J: Integer;

begin

  for I := 0 to High(FBScanLines) do
    // for J:= 0 to High(FBScanLines[I].ScanChNumList) do
    // if FBScanLines[I].ScanChNumList Channel in [0..9] then

    if Length(FBScanLines[I].ScanChNumList) <> 0 then

      with FDatSrc.Config.ScanChannelByNum[FBScanLines[I].ScanChNumList[0]] do
      begin

        // if FDispConf.BScanViewZone[FBScanLines[I].Channel].Dur <> 0 then
        if BScanDuration <> 0 then FBScanLines[I].YStep := (FBScanLines[I].BScanRt.Bottom - FBScanLines[I].BScanRt.Top) / BScanDuration
                              else FBScanLines[I].YStep := 1;
        // BScanGateMin) /
        if FDM.Zoom <> 0 then
          FBScanLines[I].XStep := FDatSrc.Header.ScanStep / FDM.Zoom;
      end;
end;

function TAvk11Display.GetBBUFER: TBitMap;
begin
  // Result:= BackBuffer;
end;

procedure TAvk11Display.ApplyDisplayMode;
var
  DivVal: Integer;
  I, J, CL: Integer;

  RailCount: Integer;
  BScanLinesCount: Integer;
  // Percent: Integer;

  Line: Integer;
  FullLine: Integer;
  Item: Integer;

  R: TRail;
  ScanChIdx: Integer;
  EvalChListIdx: Integer;
  EvalChList: TIntegerDynArray;

  LeftGateColIdx: Integer;
  RightGateColIdx: Integer;
  BScanColIdx: Integer;
  InfoColIdx: Integer;
  Flg: Boolean;

  Pos: Single;

begin

  with FDatSrc.Config do
  begin
    FMaxReduce[0] := 0;
    I := 1;
    { FMaxReduce[I]:= 0;
      for ScanChIdx:= 0 to ScanChannelCount - 1 do
      begin
      Pos:= GetRealCrd_inMM(ScanChannelByIdx[ScanChIdx], (ScanChannelByIdx[ScanChIdx].BScanGateMin + ScanChannelByIdx[ScanChIdx].BScanGateMax) div 2);
      if ScanChannelByIdx[ScanChIdx].EnterAngle > 0 then Pos:= ScanChannelByIdx[ScanChIdx].Position + Pos
      else Pos:= ScanChannelByIdx[ScanChIdx].Position - Pos;

      FMaxReduce[I]:= Max(FMaxReduce[I], Round(Abs(Pos + ScanChannelByIdx[ScanChIdx].Delta) / (FDatSrc.Header.ScanStep / 100)));
      end; }
    I := 2;
    FMaxReduce[I] := 0;
    for ScanChIdx := 0 to ScanChannelCount - 1 do
    begin
 //     if ScanChannelByIdx[ScanChIdx] = - 1 then Continue;
      Pos := GetRealCrd_inMM(ScanChannelByIdx[ScanChIdx], ScanChannelByIdx[ScanChIdx].BScanGateMin, FDatSrc.isEGOUSW_BHead);
      FMaxReduce[I] := Max(FMaxReduce[I], Abs(Round(Pos / (FDatSrc.Header.ScanStep / 100))));
      Pos := GetRealCrd_inMM(ScanChannelByIdx[ScanChIdx], ScanChannelByIdx[ScanChIdx].BScanGateMax, FDatSrc.isEGOUSW_BHead);
      FMaxReduce[I] := Max(FMaxReduce[I], Abs(Round(Pos / (FDatSrc.Header.ScanStep / 100))));
    end;
    FMaxReduce[1] := FMaxReduce[2];
    // FMaxReduce[1]:= FMaxReduce[1] * 2;
    // FMaxReduce[1]:= FMaxReduce[2] * 2;
  end;

  {
    if (FDM.ViewMode = ddBothRail) then DisplayMode.BScanViewMode:= vmAllLines;
    if (New = ddLeftRail) then
    begin
    DisplayMode.BScanViewMode:= vmOneRailLines;
    DisplayMode.ViewRail:= rLeft;
    end;
    if (New = ddRightRail) then
    begin
    DisplayMode.BScanViewMode:= vmOneRailLines;
    DisplayMode.ViewRail:= rRight;
    end; }

  // vmOneLine
  // DisplayMode.ViewRail:= True;
  // DisplayMode.ViewLine:= True;

  // DisplayMode_ShowChannelInfo:= True;
  // DisplayMode_ShowGate:= True;

  if not Assigned(FDatSrc) then Exit;
  if not Assigned(FDatSrc.Config) then Exit;

  // FViewMode:= New;
  BScanLinesCount := 1; // Подсчет количества строк В-развертки
  if FDM.RailView then
  begin
    CL := 40;
    case DisplayMode.ViewMode of
      vmAllLines:
        begin
          FStartRail := FDatSrc.Config.MinRail;
          FEndRail := FDatSrc.Config.MaxRail;
          FStartLine := 0;
          FEndLine := 1 { FDatSrc.Config.BScanLinesCount - 1 } ;
          BScanLinesCount := 2; // Подсчет количества строк В-развертки
        end;
      vmOneRailLines:
        begin
          FStartRail := DisplayMode.ViewRail;
          FEndRail := DisplayMode.ViewRail;

          FStartLine := 0;
          FEndLine := 0;
          BScanLinesCount := 1; // Подсчет количества строк В-развертки

          { for I := 0 to FDatSrc.Config.BScanLinesCount - 1 do
            if FDatSrc.Config.BScanLineItem[I].R = DisplayMode.ViewRail then
            begin
            FStartLine:= I;
            Break;
            end;
            for I := FDatSrc.Config.BScanLinesCount - 1 downto 0 do
            if FDatSrc.Config.BScanLineItem[I].R = DisplayMode.ViewRail then
            begin
            FEndLine:= I;
            Break;
            end; }
        end;
      { vmOneLine: begin
        FStartRail:= DisplayMode.ViewRail;
        FEndRail:= DisplayMode.ViewRail;
        FStartLine:= DisplayMode.ViewLine;
        FEndLine:= DisplayMode.ViewLine;
        end; }
    end;

    FBScanBox.Clear;
    FChInfoBox.Clear;
    FChSettBox.Clear;
    FChGateBox[rLeft].Clear;
    FChGateBox[rRight].Clear;

    Flg := True;

    for Line := FStartLine to FEndLine do // Строка В-развертки
    begin
      FBScanBox.AddRow(ctPercent, 100 / BScanLinesCount);
      { FChInfoBox.AddRow(ctPercent, 100 / BScanLinesCount);
        FChSettBox.AddRow(ctPercent, 100 / BScanLinesCount);
        FChGateBox[rLeft].AddRow(ctPercent, 100 / BScanLinesCount);
        FChGateBox[rRight].AddRow(ctPercent, 100 / BScanLinesCount);
        }
      if (Line <> FEndLine) or (FStartLine = FEndLine) then
      begin
        CoordRectRowIdx := FBScanBox.AddRow(ctPixel, CL);
        FChInfoBox.AddRow(ctPixel, CL);
        FChSettBox.AddRow(ctPixel, CL);
        FChGateBox[rLeft].AddRow(ctPixel, CL);
        FChGateBox[rRight].AddRow(ctPixel, CL);
      end;

    end;

    // HeightToSmall:= True;
    (*
      if DisplayMode_ShowChInfo and not HeightToSmall then
      begin
      FChInfoBox.AddCol(caMain, ctPixel, 25); // Направление
      FChInfoBox.AddCol(caMain, ctPixel, 40); // Альфа/Гамма
      FChInfoBox.AddCol(caMain, ctPixel, 40); // Метод
      FChInfoBox.AddCol(caMain, ctPixel, 40); // Зона
      //    FDrawBox.AddCol(caMain, ctPixel, FChInfoBox.ColPixelWidth);
      end;

      if DisplayMode_ShowChSettings and not HeightToSmall then
      begin
      FChSettBox.AddCol(caMain, ctPixel, 23); // Ку
      FChSettBox.AddCol(caMain, ctPixel, 23); // Атт
      FChSettBox.AddCol(caMain, ctPixel, 23); // ВРЧ
      FChSettBox.AddCol(caMain, ctPixel, 33); // 2Тп
      //    FDrawBox.AddCol(caMain, ctPixel, FChSettBox.ColPixelWidth);
      end;

      if DisplayMode_ShowChGate {and not HeightToSmall} then FChGateBox[rLeft].AddCol(caMain, ctPixel, 35);
      *)
    FBScanBox.AddCol(caMain, ctPercent, 100);

    // if DisplayMode_ShowChGate {and not HeightToSmall} then FChGateBox[rRight].AddCol(caMain, ctPixel, 35);

  end
  else
  /// ////////////////////////////////////////////////////////////////////////////////////////////////////////
  begin
    CL := 50;
    case DisplayMode.ViewMode of
      vmAllLines:
        begin
          FStartRail := FDatSrc.Config.MinRail;
          FEndRail := FDatSrc.Config.MaxRail;
          FStartLine := 0;
          FEndLine := FDatSrc.Config.BScanLinesCount - 1;
        end;
      vmOneRailLines:
        begin
          FStartRail := DisplayMode.ViewRail;
          FEndRail := DisplayMode.ViewRail;
          for I := 0 to FDatSrc.Config.BScanLinesCount - 1 do
            if FDatSrc.Config.BScanLineItem[I].R = DisplayMode.ViewRail then
            begin
              FStartLine := I;
              Break;
            end;
          for I := FDatSrc.Config.BScanLinesCount - 1 downto 0 do
            if FDatSrc.Config.BScanLineItem[I].R = DisplayMode.ViewRail then
            begin
              FEndLine := I;
              Break;
            end;
        end;
      vmOneLine:
        begin
          FStartRail := DisplayMode.ViewRail;
          FEndRail := DisplayMode.ViewRail;
          FStartLine := DisplayMode.ViewLine;
          FEndLine := DisplayMode.ViewLine;
        end;
    end;

    BScanLinesCount := 0; // Подсчет количества строк В-развертки
    for Line := FStartLine to FEndLine do // Строка В-развертки
      BScanLinesCount := BScanLinesCount + 1;

    FBScanBox.Clear;
    FChInfoBox.Clear;
    FChSettBox.Clear;
    FChGateBox[rLeft].Clear;
    FChGateBox[rRight].Clear;

    Flg := True;

    for Line := FStartLine to FEndLine do // Строка В-развертки
    begin
      FBScanBox.AddRow(ctPercent, 100 / BScanLinesCount);
      FChInfoBox.AddRow(ctPercent, 100 / BScanLinesCount);
      FChSettBox.AddRow(ctPercent, 100 / BScanLinesCount);
      FChGateBox[rLeft].AddRow(ctPercent, 100 / BScanLinesCount);
      FChGateBox[rRight].AddRow(ctPercent, 100 / BScanLinesCount);

      if ((Line <> FEndLine) and (FDatSrc.Config.BScanLineItem[Line].R <> FDatSrc.Config.BScanLineItem[Line + 1].R) and (FDatSrc.Config.BScanLineItem[FStartLine].R <> FDatSrc.Config.BScanLineItem[FEndLine].R)) or

        ((Line = FEndLine) and (FDatSrc.Config.BScanLineItem[FStartLine].R = FDatSrc.Config.BScanLineItem[FEndLine].R)) then
      begin
        CoordRectRowIdx := FBScanBox.AddRow(ctPixel, CL);
        FChInfoBox.AddRow(ctPixel, CL);
        FChSettBox.AddRow(ctPixel, CL);
        FChGateBox[rLeft].AddRow(ctPixel, CL);
        FChGateBox[rRight].AddRow(ctPixel, CL);
      end;

    end;

    if DisplayMode_ShowChInfo and not HeightToSmall then
    begin
      FChInfoBox.AddCol(caMain, ctPixel, 25); // Направление
      FChInfoBox.AddCol(caMain, ctPixel, 40); // Альфа/Гамма
      FChInfoBox.AddCol(caMain, ctPixel, 40); // Метод
      FChInfoBox.AddCol(caMain, ctPixel, 40); // Зона
      // FDrawBox.AddCol(caMain, ctPixel, FChInfoBox.ColPixelWidth);
    end;

    if DisplayMode_ShowChSettings and not HeightToSmall then
    begin
      FChSettBox.AddCol(caMain, ctPixel, 23); // Ку
      FChSettBox.AddCol(caMain, ctPixel, 23); // Атт
      FChSettBox.AddCol(caMain, ctPixel, 23); // ВРЧ
      FChSettBox.AddCol(caMain, ctPixel, 33); // 2Тп
      // FDrawBox.AddCol(caMain, ctPixel, FChSettBox.ColPixelWidth);
    end;

    if DisplayMode_ShowChGate { and not HeightToSmall } then
      FChGateBox[rLeft].AddCol(caMain, ctPixel, 35);

    FBScanBox.AddCol(caMain, ctPercent, 100);

    if DisplayMode_ShowChGate { and not HeightToSmall } then
      FChGateBox[rRight].AddCol(caMain, ctPixel, 35);

  end;

  ReCalcOutRect;

  Exit;

  (*
    BScanColIdx:= 0;
    LeftGateColIdx:= 0;
    RightGateColIdx:= 0;
    InfoColIdx:= 0;
    if DisplayMode_ShowGate then
    begin
    LeftGateColIdx:= 0;
    BScanColIdx:= 1;
    RightGateColIdx:= 2;
    end;
    if DisplayMode_ShowChannelInfo then
    begin
    Inc(BScanColIdx);
    Inc(LeftGateColIdx);
    Inc(RightGateColIdx);
    end;

    FDrawBox.SetSize(Width - 2, Height - 2, 0);
    {
    SetLength(FBScanLines, 0);
    for R := FDatSrc.Config.MinRail to FDatSrc.Config.MaxRail do
    for Line:= 0 to FDatSrc.Config.BScanLinesCount - 1 do         // Строка В-развертки
    begin
    if R = FDatSrc.Config.MinRail then FullLine:= Line
    else FullLine:= Line + FDatSrc.Config.BScanLinesCount + 1;

    for Item:= 0 to FDatSrc.Config.BScanLineItemCount[Line] - 1 do
    begin
    ScanChIdx:= FDatSrc.Config.BScanLineItem[Line, Item].BScanChIdx;
    EvalChList:= FDatSrc.Config.GetEvalsChIdxByScanChIdx(ScanChIdx);

    for EvalChListIdx:= 0 to High(EvalChList) do
    begin
    SetLength(FBScanLines, Length(FBScanLines) + 1);
    FBScanLines[High(FBScanLines)].DrawRect:= StepIn(FDrawBox.GetRect(1, FullLine));
    FBScanLines[High(FBScanLines)].LeftRect:= StepIn(FDrawBox.GetRect(0, FullLine));
    FBScanLines[High(FBScanLines)].RightRect:= StepIn(FDrawBox.GetRect(2, FullLine));
    FBScanLines[High(FBScanLines)].TapeIdx:= 0;
    FBScanLines[High(FBScanLines)].Rail:= R;
    FBScanLines[High(FBScanLines)].ScanChannel:= FDatSrc.Config.ScanChannel[ScanChIdx].Number;
    with FBScanLines[High(FBScanLines)] do
    begin
    SetLength(EvalChannels, Length(EvalChannels) + 1);
    EvalChannels[High(EvalChannels)]:= FDatSrc.Config.EvalChannel[EvalChList[EvalChListIdx]].Number;
    end;
    end;
    end;
    end;
    }


    if DisplayMode_ShowChannelInfo then
    begin
    FullLine:= 0;
    FInfoBox.SetSize(FDrawBox.GetRect(InfoColIdx, FullLine).Right - FDrawBox.GetRect(InfoColIdx, FullLine).Left, 100, 1);
    end;

    SetLength(FBScanLines, 0);

    FullLine:= 0;
    for R:= DisplayMode.StartRail to DisplayMode.EndRail do
    for Line:= DisplayMode.StartLine to DisplayMode.EndLine do         // Строка В-развертки
    //  for R := FDatSrc.Config.MinRail to FDatSrc.Config.MaxRail do
    //    for Line:= 0 to FDatSrc.Config.BScanLinesCount - 1 do         // Строка В-развертки
    begin
    //      if R = FDatSrc.Config.MinRail then FullLine:= Line
    //                                    else FullLine:= Line + FDatSrc.Config.BScanLinesCount + 1;

    if FullLine = CoordRectRowIdx then Inc(FullLine);

    SetLength(FBScanLines, Length(FBScanLines) + 1);

    FBScanLines[High(FBScanLines)].Rail:= R;
    if DisplayMode_ShowGate then
    begin
    FBScanLines[High(FBScanLines)].LeftRect:= StepIn(FDrawBox.GetRect(LeftGateColIdx, FullLine));
    FBScanLines[High(FBScanLines)].RightRect:= StepIn(FDrawBox.GetRect(RightGateColIdx, FullLine));
    end;
    if DisplayMode_ShowChannelInfo then
    FBScanLines[High(FBScanLines)].InfoRect:= StepIn(FDrawBox.GetRect(InfoColIdx, FullLine));
    FBScanLines[High(FBScanLines)].BScanRect:= StepIn(FDrawBox.GetRect(BScanColIdx, FullLine));

    //      FBScanLines[High(FBScanLines)].DrawRect.TopLeft:= FBScanLines[High(FBScanLines)].LeftRect.TopLeft;
    //      FBScanLines[High(FBScanLines)].DrawRect.BottomRight:= FBScanLines[High(FBScanLines)].RightRect.BottomRight;
    //      FBScanLines[High(FBScanLines)].TapeIdx:= 0;

    for Item:= 0 to FDatSrc.Config.BScanLineItemCount[Line] - 1 do
    begin
    ScanChIdx:= FDatSrc.Config.BScanLineItem[Line, Item].BScanChIdx;

    with FBScanLines[High(FBScanLines)] do
    begin
    SetLength(ScanChNumList, Length(ScanChNumList) + 1);
    ScanChNumList[High(ScanChNumList)]:= FDatSrc.Config.ScanChannel[ScanChIdx].Number;
    end;

    EvalChList:= FDatSrc.Config.GetEvalsChIdxByScanChIdx(ScanChIdx);

    with FBScanLines[High(FBScanLines)] do
    for EvalChListIdx:= 0 to High(EvalChList) do
    begin
    SetLength(EvalChNumList, Length(EvalChNumList) + 1);
    EvalChNumList[High(EvalChNumList)]:= FDatSrc.Config.EvalChannel[EvalChList[EvalChListIdx]].Number;
    end;
    end;
    Inc(FullLine);
    end;


    //  FFCoordRect:= StepIn(FDrawBox.GetRect(1, CoordRectRowIdx));
    //                    FFCoordRect.Left:= StepIn(FDrawBox.GetRect(0, J)).Left;
    //                    FFCoordRect.Right:= StepIn(FDrawBox.GetRect(2, J)).Right;

    *)
  (*
    FDrawBox.Clear;
    if not FDM.RailView then
    begin
    case FViewMode of
    ddBothRail: begin
    FDrawBox.AddRow(ctPercent, 12);
    FDrawBox.AddRow(ctPercent, 12);
    FDrawBox.AddRow(ctPercent, 13);
    FDrawBox.AddRow(ctPercent, 13);
    FDrawBox.AddRow(ctPixel, CL);
    FDrawBox.AddRow(ctPercent, 12);
    FDrawBox.AddRow(ctPercent, 12);
    FDrawBox.AddRow(ctPercent, 13);
    FDrawBox.AddRow(ctPercent, 13);
    end;
    ddLeftRail,
    ddRightRail: begin
    FDrawBox.AddRow(ctPercent, 25);
    FDrawBox.AddRow(ctPercent, 25);
    FDrawBox.AddRow(ctPixel, CL);
    FDrawBox.AddRow(ctPercent, 25);
    FDrawBox.AddRow(ctPercent, 25);
    end;
    ddLeftTape1,
    ddLeftTape2,
    ddLeftTape3,
    ddLeftTape4,
    ddRightTape1,
    ddRightTape2,
    ddRightTape3,
    ddRightTape4: begin
    FDrawBox.AddRow(ctPercent, 100);
    FDrawBox.AddRow(ctPixel, CL);
    end;
    end;
    FDrawBox.AddCol(caMain, ctPixel, 35);
    FDrawBox.AddCol(caMain, ctPercent, 100);
    FDrawBox.AddCol(caMain, ctPixel, 35);
    end
    else
    begin
    if (FViewMode <> ddLeftRail) and (FViewMode <> ddRightRail) then FViewMode:= ddBothRail;

    case FViewMode of
    ddBothRail: begin
    FDrawBox.AddRow(ctPercent, 50);
    FDrawBox.AddRow(ctPixel, CL);
    FDrawBox.AddRow(ctPercent, 50);
    DivVal:= 10;
    end;
    ddLeftRail,
    ddRightRail: begin
    FDrawBox.AddRow(ctPercent, 100);
    FDrawBox.AddRow(ctPixel, CL);
    DivVal:= 5;
    end;
    end;
    FDrawBox.AddCol(caMain, ctPercent, 100);
    FDrawBox.AddCol(caMain, ctPixel, Height div DivVal);
    end;
    *)
  (*
    for R:= rLeft to rRight do
    for I:= 0 to 15 do
    FScanChNumToBScanLine[R, I]:= - 1;

    for I:= 0 to High(FBScanLines) do
    for J:= 0 to High(FBScanLines[I].ScanChNumList) do
    //if (FBScanLines[I].Channel <> - 1) and (FBScanLines[I].Channel <> - 10) then
    FScanChNumToBScanLine[FBScanLines[I].Rail, FBScanLines[I].ScanChNumList[J]]:= I;
    *)
  // Refresh;
end;

{
  function TAvk11Display.ViewCoordToDisCoord(ViewCoord: Integer): Integer;
  begin
  Result:= ViewCoord;
  end;
}
procedure TAvk11Display.SwitchReductionMode;
begin
  case FDM.ReductionMode of
    1: FDM.ReductionMode := 2;
    2: FDM.ReductionMode := 1;
  end;
  Refresh;
end;

{
  function TAvk11Display.DisCoordToViewCoord(DisCoord: Integer): Integer;
  begin
  Result:= DisCoord;
  end;

  function TAvk11Display.GetViewData(ViewCoord: Integer; EndViewZone, EndDisCoord: Integer): Integer;
  begin
  //
  end;
}
// procedure TAvk11Display.SetAll(CenterDisCoord, Zoom, Reduction, AmplTh: Integer; RailView, AmplDon: Boolean; FViewMode: TViewMode);
procedure TAvk11Display.SetAll(CenterDisCoord: Integer; DisplayMode: TDisplayMode);
var
  F: Boolean;

begin
  F := FDM.RailView <> DisplayMode.RailView;

  FDM := DisplayMode;
  SetRailView(DisplayMode.RailView);
  FCenterDisCoord := CenterDisCoord;

  if F then
  begin
    // RecalcOutRect;
    FullRefresh;
  end;

  {
    //  FCenterDisCoord:= CenterDisCoord;
    FDM.Zoom:= DisplayMode.Zoom;
    FDM.Reduction:= DisplayMode.Reduction;
    //  SetRailView(DisplayMode.RailView);
    FDM.AmplTh:= DisplayMode.AmplTh;
    FDM.AmplDon:= DisplayMode.AmplDon;
    FDM.ViewMode:= DisplayMode.ViewMode;
    }

  FScrollBar.OnChange := nil;
  FScrollBar.Position := FCenterDisCoord;
  FScrollBar.OnChange := ChangeScrollBar;
end;

procedure TAvk11Display.SetScrollBar(New: TScrollBar);
begin
  FScrollBar := New;
  FScrollBar.OnChange := ChangeScrollBar;
  if Assigned(New) and Assigned(FDatSrc) then
  begin
    // if (Length(FBScanLines) <> 0) and (FDatSrc.Header.ScanStep <> 0) then
    // FScrollBar.PageSize:= Abs(Round((FBScanLines[0].BScanRt.Right - FBScanLines[0].BScanRt.Left) * (FDM.Zoom / 100) / (FDatSrc.Header.ScanStep / 100)));
    FScrollBar.Max := Min(MaxInt, FDatSrc.MaxDisCoord + FScrollBar.PageSize);
  end;
end;

procedure TAvk11Display.SetDisplayColors(New_: TDisplayColors);
var
  R1, G1, B1: Integer;
  R2, G2, B2: Integer;
  CR, CG, CB: Integer;
  CurCol: Integer;
  I: Integer;
  Per: Single;

  Line: Integer;
  ChIdx: Integer;
  ChIdx_: Integer;

  ChNum: Integer;
  Ampl: Integer;
  Idx: Integer;

  Rail: TRail;
  EvalChList: TIntegerDynArray;

  DirPlusCount: Integer;
  DirMinusCount: Integer;

  tmp: Integer;
  DeviceID: TAviconID;

  MinAmplTH: Integer;

begin
  if not Assigned(FDatSrc.Config) then Exit;

  if NowInit then
  begin
    FDispCols := New_;

    R1 := (New_.Fon and $0000FF) shr 0;
    G1 := (New_.Fon and $00FF00) shr 8;
    B1 := (New_.Fon and $FF0000) shr 16;

    if FDatSrc.isEGOUSW_BHead then
    begin
      R2 := (New_.OtezdCh[1] and $0000FF) shr 0;
      G2 := (New_.OtezdCh[1] and $00FF00) shr 8;
      B2 := (New_.OtezdCh[1] and $FF0000) shr 16;
    end
    else
    begin
      R2 := (New_.NaezdCh[1] and $0000FF) shr 0;
      G2 := (New_.NaezdCh[1] and $00FF00) shr 8;
      B2 := (New_.NaezdCh[1] and $FF0000) shr 16;
    end;

    if not DatSrc.isA31 then MinAmplTH:= 3 else
    begin
      if DatSrc.Header.UsedItems[uiBScanTreshold_minus_6dB] <> 0 then MinAmplTH:= 3;
      if DatSrc.Header.UsedItems[uiBScanTreshold_minus_12dB] <> 0 then MinAmplTH:= 0;
    end;

    for I := MinAmplTH to 15 do
    begin
      Per := Config.MinColPer / 100 + ((I - MinAmplTH ) / 15) * ((100 - Config.MinColPer) / 100);
      CR := Round(R1 + (R2 - R1) * Per);
      CG := Round(G1 + (G2 - G1) * Per);
      CB := Round(B1 + (B2 - B1) * Per);
      CurCol := CR + CG * $100 + CB * $10000;
      FNaezdChByAmpl[1, I] := CurCol;
    end;

    // if FDatSrc.isEGOUSW and (FDatSrc.Header.ControlDirection = cd_Head_B_Tail_A) then
    if FDatSrc.isEGOUSW_BHead then
    begin
      R2 := (New_.NaezdCh[1] and $0000FF) shr 0;
      G2 := (New_.NaezdCh[1] and $00FF00) shr 8;
      B2 := (New_.NaezdCh[1] and $FF0000) shr 16;
    end
    else
    begin
      R2 := (New_.OtezdCh[1] and $0000FF) shr 0;
      G2 := (New_.OtezdCh[1] and $00FF00) shr 8;
      B2 := (New_.OtezdCh[1] and $FF0000) shr 16;
    end;

    // for I:= 0 to DatSrc.Header.AmplTH - 1 do FOtezdChByAmpl[I]:= 0;
    for I := MinAmplTH to 15 do
    begin
      Per := Config.MinColPer / 100 + ((I - MinAmplTH) / (15 - MinAmplTH )) * ((100 - Config.MinColPer) / 100);
      CR := Round(R1 + (R2 - R1) * Per);
      CG := Round(G1 + (G2 - G1) * Per);
      CB := Round(B1 + (B2 - B1) * Per);
      CurCol := CR + CG * $100 + CB * $10000;
      FOtezdChByAmpl[1, I] := CurCol;
    end;

    // ---------------------------------------------

    R1 := (New_.Fon and $0000FF) shr 0;
    G1 := (New_.Fon and $00FF00) shr 8;
    B1 := (New_.Fon and $FF0000) shr 16;

    // R2:= (New_.NaezdCh[2] and $0000FF) shr 0;
    // G2:= (New_.NaezdCh[2] and $00FF00) shr 8;
    // B2:= (New_.NaezdCh[2] and $FF0000) shr 16;

    // if FDatSrc.isEGOUSW and (FDatSrc.Header.ControlDirection = cd_Head_B_Tail_A) then

    if FDatSrc.isEGOUSW then
    begin
      if FDatSrc.isEGOUSW_BHead then
      begin
        R2 := (New_.OtezdCh[1] and $0000FF) shr 0;
        G2 := (New_.OtezdCh[1] and $00FF00) shr 8;
        B2 := (New_.OtezdCh[1] and $FF0000) shr 16;
      end
      else
      begin
        R2 := (New_.NaezdCh[1] and $0000FF) shr 0;
        G2 := (New_.NaezdCh[1] and $00FF00) shr 8;
        B2 := (New_.NaezdCh[1] and $FF0000) shr 16;
      end;
    end
    else if FDatSrc.isUSK then
    begin
      R2 := (New_.NaezdCh[1] and $0000FF) shr 0;
      G2 := (New_.NaezdCh[1] and $00FF00) shr 8;
      B2 := (New_.NaezdCh[1] and $FF0000) shr 16;
    end
    else
    begin
      R2 := (New_.NaezdCh[2] and $0000FF) shr 0;
      G2 := (New_.NaezdCh[2] and $00FF00) shr 8;
      B2 := (New_.NaezdCh[2] and $FF0000) shr 16;
    end;

    for I := MinAmplTH to 15 do
    begin
      Per := Config.MinColPer / 100 + ((I - MinAmplTH ) / 15) * ((100 - Config.MinColPer) / 100);
      CR := Round(R1 + (R2 - R1) * Per);
      CG := Round(G1 + (G2 - G1) * Per);
      CB := Round(B1 + (B2 - B1) * Per);
      CurCol := CR + CG * $100 + CB * $10000;
      FNaezdChByAmpl[2, I] := CurCol;
    end;
    // R2:= (New_.OtezdCh[2] and $0000FF) shr 0;
    // G2:= (New_.OtezdCh[2] and $00FF00) shr 8;
    // B2:= (New_.OtezdCh[2] and $FF0000) shr 16;

    if FDatSrc.isEGOUSW then
    begin
      if FDatSrc.isEGOUSW_BHead then
      begin
        R2 := (New_.NaezdCh[1] and $0000FF) shr 0;
        G2 := (New_.NaezdCh[1] and $00FF00) shr 8;
        B2 := (New_.NaezdCh[1] and $FF0000) shr 16;
      end
      else
      begin
        R2 := (New_.OtezdCh[1] and $0000FF) shr 0;
        G2 := (New_.OtezdCh[1] and $00FF00) shr 8;
        B2 := (New_.OtezdCh[1] and $FF0000) shr 16;
      end;
    end
    else if FDatSrc.isUSK then
    begin
      R2 := (New_.OtezdCh[1] and $0000FF) shr 0;
      G2 := (New_.OtezdCh[1] and $00FF00) shr 8;
      B2 := (New_.OtezdCh[1] and $FF0000) shr 16;
    end
    else
    begin
      R2 := (New_.OtezdCh[2] and $0000FF) shr 0;
      G2 := (New_.OtezdCh[2] and $00FF00) shr 8;
      B2 := (New_.OtezdCh[2] and $FF0000) shr 16;
    end;

    for I := MinAmplTH to 15 do
    begin
      Per := Config.MinColPer / 100 + ((I - MinAmplTH ) / (15 - MinAmplTH )) * ((100 - Config.MinColPer) / 100);
      CR := Round(R1 + (R2 - R1) * Per);
      CG := Round(G1 + (G2 - G1) * Per);
      CB := Round(B1 + (B2 - B1) * Per);
      CurCol := CR + CG * $100 + CB * $10000;
      FOtezdChByAmpl[2, I] := CurCol;
    end;
  end;

  Rail := rLeft;
  for ChIdx := 0 to FDatSrc.Config.EvalChannelCount - 1 do
  begin
    I := FDatSrc.Config.ChannelIDList[Rail, FDatSrc.Config.EvalChannelByIdx[ChIdx].Number];

    if FDatSrc.Config.EvalChannelByIdx[ChIdx].DirColor > 0 then
    begin
      EvalChNumColors2[I] := FNaezdChByAmpl[GetSurf(I)];
      DeviceID := FDatSrc.Config.DeviceID;
      if CompareMem(@DeviceID, @Avicon31Scheme1, 8) then EvalChNumColors2[I] := FNaezdChByAmpl[1];
      if CompareMem(@DeviceID, @Avicon31Estonia, 8) then EvalChNumColors2[I] := FNaezdChByAmpl[1];
      if CompareMem(@DeviceID, @VMT_US_BigWP, 8) then EvalChNumColors2[I] := FNaezdChByAmpl[1];
      if CompareMem(@DeviceID, @FilusX17DW, 8) then EvalChNumColors2[I] := FNaezdChByAmpl[1];
      if CompareMem(@DeviceID, @EGO_USWScheme2, 8) then EvalChNumColors2[I] := FNaezdChByAmpl[1];
      if CompareMem(@DeviceID, @Avicon14Wheel2, 8) then EvalChNumColors2[I] := FNaezdChByAmpl[1];
      if CompareMem(@DeviceID, @Avicon16Wheel2, 8) then EvalChNumColors2[I] := FNaezdChByAmpl[1];
    end
    else
    begin
      EvalChNumColors2[I] := FOtezdChByAmpl[GetSurf(I)];
      DeviceID := FDatSrc.Config.DeviceID;
      if CompareMem(@DeviceID, @Avicon15Scheme1, 8) and (FDatSrc.Config.EvalChannelByIdx[ChIdx].Method = imMirror) then EvalChNumColors2[I] := FOtezdChByAmpl[2];
      if CompareMem(@DeviceID, @Avicon15N8Scheme1, 8) and (FDatSrc.Config.EvalChannelByIdx[ChIdx].Method = imMirror) then EvalChNumColors2[I] := FOtezdChByAmpl[2];
      if CompareMem(@DeviceID, @Avicon31Scheme1, 8) then EvalChNumColors2[I] := FOtezdChByAmpl[1];
      if CompareMem(@DeviceID, @Avicon31Estonia, 8) then EvalChNumColors2[I] := FOtezdChByAmpl[1];
      if CompareMem(@DeviceID, @VMT_US_BigWP, 8) then EvalChNumColors2[I] := FOtezdChByAmpl[1];
      if CompareMem(@DeviceID, @FilusX17DW, 8) then EvalChNumColors2[I] := FOtezdChByAmpl[1];
      if CompareMem(@DeviceID, @EGO_USWScheme2, 8) then EvalChNumColors2[I] := FOtezdChByAmpl[1];
      if CompareMem(@DeviceID, @Avicon14Wheel2, 8) then EvalChNumColors2[I] := FOtezdChByAmpl[1];
      if CompareMem(@DeviceID, @Avicon16Wheel2, 8) then EvalChNumColors2[I] := FOtezdChByAmpl[1];
    end;
  end;

  {
    for Rail:= rLeft to rRight do
    SetLength(ChNumColors[Rail], FDatSrc.Config.MaxEvalChNumber + 1);
    }
  {
    for Line:= 0 to High(FBScanLines) do // Просматриваем строки В Развертики
    begin

    DirPlusCount:= 1;
    DirMinusCount:= 1;

    for ChIdx:= 0 to High(FBScanLines[Line].ScanChNumList) do // Просматриваем каналы развертки строки В-развертки
    begin
    ChNum:= Abs(FBScanLines[Line].ScanChNumList[ChIdx]); // Получаем канал сканирования
    EvalChList:= FDatSrc.Config.GetEvalsChIdxByScanChNum(ChNum); // Получаем список каналов оценки с которомаи связан данный канал сканирования

    for ChIdx_:= 0 to High(EvalChList) do // Просматриваем каналы оценки
    begin
    // В зависимости от направления-цвета и от Order берем либо наезжающий цвет, либо отъезжающий

    if FDatSrc.Config.EvalChannelByIdx[EvalChList[ChIdx_]].DirColor > 0 then
    begin
    for Ampl:= 0 to 15 do
    ChNumColors[FBScanLines[Line].Rail, FDatSrc.Config.EvalChannelByIdx[EvalChList[ChIdx_]].Number, Ampl]:= FNaezdChByAmpl[DirPlusCount, Ampl];
    if (FDatSrc.Config.ScanChannelByNum[ChNum].Gran <> _None) and
    (FDatSrc.Config.ScanChannelByNum[ChNum].Gran <> _Both) then Inc(DirPlusCount);
    end
    else
    if FDatSrc.Config.EvalChannelByIdx[EvalChList[ChIdx_]].DirColor < 0 then
    begin
    for Ampl:= 0 to 15 do
    ChNumColors[FBScanLines[Line].Rail, FDatSrc.Config.EvalChannelByIdx[EvalChList[ChIdx_]].Number, Ampl]:= FOtezdChByAmpl[DirMinusCount, Ampl];
    if (FDatSrc.Config.ScanChannelByNum[ChNum].Gran <> _None) and
    (FDatSrc.Config.ScanChannelByNum[ChNum].Gran <> _Both) then Inc(DirMinusCount);
    end;

    end;
    end;
    end;
    }
  ChangeFontPar(0, LangTable.Caption['General:FontName'], 15, FDispCols.Text, FDispCols.Fon);
  ChangeFontPar(1, LangTable.Caption['General:FontName'], 15, FDispCols.Text, FDispCols.Event);
  ChangeFontPar(2, LangTable.Caption['General:FontName'], 15, FDispCols.Text, FDispCols.Defect);

  MixColor := Config.UseThreeColor;
  TestColor1 := FDispCols.NaezdCh[1];
  TestColor2 := FDispCols.OtezdCh[1];
  ResColor := FDispCols.CrossedCh;
end;

procedure TAvk11Display.SetDispConf(New: TDisplayConfig);
begin
  FDispConf := New;
end;

procedure TAvk11Display.SetViewLabels(NewSate: Boolean);
begin
  FViewLabels := NewSate;
  Refresh;
end;

procedure TAvk11Display.SetDatSrc(New: TAvk11DatSrc);
begin
  FDatSrc := New;
end;

(*
  procedure TAvk11Display.Resize123;
  begin
  if not Assigned(FDatSrc) then Exit;
  if not Assigned(FDatSrc.FileBody) then Exit;

  inherited Resize;
  //  SetViewMode(FViewMode);
  RecalcOutRect;
  Refresh;
  end;
*)
function TAvk11Display.DisToScr(DisCoord: Int64): Int64;
begin
  if Assigned(FDatSrc) then
    Result := DisCoord * FDatSrc.Header.ScanStep div FDM.Zoom
  else
    Result := 0;
end;

function TAvk11Display.CalcPixelSize(Idx, Ampl: Integer): TPoint;
begin
  if FEchoSizeByAmpl then
  begin
    if Config.EchoSize = - 1 then
    begin
      Result.X := Ceil(0.2 + FBScanLines[Idx].XStep * 1.8 * (Ampl - 2) / 13);
      Result.Y := Ceil(0.2 + FBScanLines[Idx].YStep * 1.8 * (Ampl - 2) / 13);
    end
    else
    if Config.EchoSize = 0 then
    begin
      Result.X := Ceil(0.6 + FBScanLines[Idx].XStep * 2 * (Ampl - 2) / 13);
      Result.Y := Ceil(0.6 + FBScanLines[Idx].YStep * 2 * (Ampl - 2) / 13);
    end
    else
    if Config.EchoSize = 1 then
    begin
      Result.X := Ceil(1 + FBScanLines[Idx].XStep * 3 * (Ampl - 2) / 13);
      Result.Y := Ceil(1 + FBScanLines[Idx].YStep * 3 * (Ampl - 2) / 13);
    end;
  end
  else
  begin
    if Config.EchoSize = - 1 then
    begin
      Result.X := 1;
      Result.Y := 2;
    end
    else
    if Config.EchoSize = 0 then
    begin
      Result.X := 2;
      Result.Y := 3;
    end
    else
    if Config.EchoSize = 1 then
    begin
      Result.X := 3;
      Result.Y := 4;
    end;
  end
end;

procedure TAvk11Display.DrawBSAmplSample(RtIdx: Integer; R: TRail; DrawX: Integer; LastCrd: Boolean; Ampl: Integer);
var
  I, J, DrawY, DrawY2: Integer;
  MaxVal: Integer;
  ThVal: Integer;
  ThPos: Single;

begin
  ThPos := 1 / 8;
  MaxVal := 256;
  ThVal := Round(MaxVal * ThPos);

  with FBScanLines[RtIdx] do
  begin
    // SetClipRect(BScanRt);
    DrawY := -1;

    // if FDatSrc.Header.ContentFlg and cfBSAEcho <> 0 then // 1 бит - огибающая дс (сигналами);
    // begin
    // if FDatSrc.CurAmpl[R] <> 0 then DrawY:= DrawRect.Bottom - Abs(DrawRect.Bottom - DrawRect.Top) * FDatSrc.CurAmpl[R] div 255;
    // if FDatSrc.CurAmpl[R] > 0 then DrawY:= DrawRect.Bottom - Round(Abs(DrawRect.Bottom - DrawRect.Top) * (6 + 20 * log10(FDatSrc.CurAmpl[R] / ThVal)) / (6 + 18));
    // if FDatSrc.CurAmpl[R] > 0 then DrawY:= DrawRect.Bottom - Round(Abs(DrawRect.Bottom - DrawRect.Top) * (6 + 20 * log10(FDatSrc.CurAmpl[R] / ThVal)) / (6 + 18));
    // DrawY:= DrawRect.Bottom - Abs(DrawRect.Bottom - DrawRect.Top) * (FDatSrc.CurEcho[R, 1].Ampl[J] - FDatSrc.Header.AmplTH) div (15 - FDatSrc.Header.AmplTH);
    // end
    // else
    // if (FDatSrc.Header.ContentFlg and cfBSABlock = 0) and
    // (FDatSrc.Header.ContentFlg and cfBSAEcho = 0) then // Обычная огибающая дс
    // begin
    // for J:= 1 to FDatSrc.CurEcho[R, 1].Count do
    // if (FDatSrc.CurEcho[R, 1].Delay[J] / FDatSrc.DelayKoeff >= FDatSrc.CurParams.StStr[R, 0]) and
    // (FDatSrc.CurEcho[R, 1].Delay[J] / FDatSrc.DelayKoeff <= FDatSrc.CurParams.EdStr[R, 0]) and
    // (FDatSrc.CurEcho[R, 1].Ampl[J] >= 6) then // Ограничение влияния порога отображения на ДС
    //
    // begin

    // DrawY:= BScanRt.Bottom - Abs(BScanRt.Bottom - BScanRt.Top) * (Ampl {- FDatSrc.Header.AmplTH}) div (15 - 3{FDatSrc.Header.AmplTH});

    DrawY := Round(BScanRt.Bottom - Abs(BScanRt.Bottom - BScanRt.Top) * 0.25 - Abs(BScanRt.Bottom - BScanRt.Top) * 0.5 * (Ampl
        { - FDatSrc.Header.AmplTH } ) / (15 - 3 { FDatSrc.Header.AmplTH } ));

    // Break;
    // end;
    // end;

    if (DrawY <> -1) or (not LastCrd) then
    begin // Проверяем не первая ли это точка
      if FDatSrc.CurDisCoord - FBSALastCoord[RtIdx, R] <= 4 then
      // Если дырка меньше 5 тогда линию огибающей не прерываем
      begin
        if FLastDrawX2[RtIdx, R] - DrawX < 1300 then
          for I := FLastDrawX2[RtIdx, R] to DrawX do
            FastPoint(I, FLastDrawY2[RtIdx, R], 2, 2, FDispCols.EnvelopeBS);
        if (FLastDrawY2[RtIdx, R] <> -1) and (Max(FLastDrawY2[RtIdx, R], DrawY) - Min(FLastDrawY2[RtIdx, R], DrawY) < 1300) then
          for I := Min(FLastDrawY2[RtIdx, R], DrawY) to Max(FLastDrawY2[RtIdx, R], DrawY) do
            FastPoint(DrawX, I, 2, 2, FDispCols.EnvelopeBS);
      end
      else // Если дырка больше 4 тогда прерываем линию огибающей провалом до нуля
      begin
        DrawY2 := Round(BScanRt.Bottom + 3 - Abs(BScanRt.Bottom - BScanRt.Top) * 0.25);
        if (FLastDrawY2[RtIdx, R] <> -1) and (Max(FLastDrawY2[RtIdx, R], DrawY2) - Min(FLastDrawY2[RtIdx, R], DrawY2) < 1300) then
          for I := Min(FLastDrawY2[RtIdx, R], DrawY2) to Max(FLastDrawY2[RtIdx, R], DrawY2) do
            FastPoint(FLastDrawX2[RtIdx, R], I, 2, 2, FDispCols.EnvelopeBS);
        if DrawX - FLastDrawX2[RtIdx, R] < 1300 then
          for I := FLastDrawX2[RtIdx, R] to DrawX do
            FastPoint(I, DrawY2, 2, 2, FDispCols.EnvelopeBS);
        if Max(DrawY, DrawY2) - Min(DrawY, DrawY2) < 1300 then
          for I := Min(DrawY, DrawY2) to Max(DrawY, DrawY2) do
            FastPoint(DrawX, I, 2, 2, FDispCols.EnvelopeBS);
      end;
      FBSALastCoord[RtIdx, R] := FDatSrc.CurDisCoord;
      FLastDrawX2[RtIdx, R] := DrawX;
      FLastDrawY2[RtIdx, R] := DrawY;
    end;
  end;
end;

const
  q: Integer = 0;

function TAvk11Display.DrawBScanSample(StartDisCoord: Integer): Boolean;
var
  R: TRail;
  DX, I, J, K, MaxA, MaxI: Integer;
  DrawX: Integer;
  DrawY: Integer;
  Ch0Flg: Boolean;
  Line_: Integer;
  ChIdx: Integer;
  ChNum: Integer;
  DonAmpl: Integer;
  DonDelay: Integer;
  EvalChNum: Integer;
  tmp: TIntegerDynArray;
  Coeff: Integer;

begin
  if not Assigned(FDatSrc) then Exit;

  try
    DrawX := FStartScrX + (FDatSrc.CurDisCoord - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom except Exit;
  end;
  if Reduction <> 0 then Result := DrawX <= FEndScrX + Round(FMaxReduce[FDM.Reduction] / (Zoom / 100))
                    else Result := DrawX <= FEndScrX;

  // -------------------- Отрисовка эхо-сигналов -----------------------------------------------

{ Проверка коректности данных В-разветки в плане номеров каналов
  for R := rLeft to rRight do
    for ChNum := 0 to 15 do
      if (not (ChNum in [1, 2, 3, 4, 5, 6, 7, 10, 11])) and (FDatSrc.CurEcho[R, ChNum].Count <> 0) then
        ShowMessage('BScan Ch Error' + IntToStr(ChNum));
 }
  for Line_ := 0 to High(FBScanLines) do
  begin
    R := FBScanLines[Line_].Rail;

    // if R <> rLeft then
    // Continue;

    for ChIdx := 0 to High(FBScanLines[Line_].ScanChNumList) do
    begin
      ChNum := Abs(FBScanLines[Line_].ScanChNumList[ChIdx]);

//      if (ChNum <> 2) then Continue;
//     if (ChNum <> 2) and (ChNum <> 3) then Continue;
//      if (ChNum <> 10) and (ChNum <> 11) then Continue;

      with FDatSrc.Config.ScanChannelByNum[ChNum], FBScanLines[Line_] do
      begin
        SetClipRect(BScanRt);
        DonAmpl := -1;

        // Вырезка двойного донного по технологии Косенко
        {
          if CompareMem(@FDatSrc.Header.DeviceID, @Avicon15Scheme1, 8) and
          MirrorShadow and
          (FDatSrc.CurEcho[R, ChNum].Count = 2) and
          (FDatSrc.CurEcho[R, ChNum].Delay[1] / DelayMultiply > FDatSrc.CurParams.Par[R, 1].EndStr) and
          (FDatSrc.CurEcho[R, ChNum].Delay[2] / DelayMultiply > FDatSrc.CurParams.Par[R, 1].EndStr) then FDatSrc.FCurEcho[R, ChNum].Count:= 1;
          }

{        if ViewACState then
          if FDatSrc.CurAKExists[R, ChNum] then
          begin
            DrawY := BScanRt.Bottom - ChIdx * 5;
            if FDatSrc.CurAKState[R, ChNum] then FastPoint(DrawX, DrawY - 4, 3, 3, GetDDColor(clGreen))
                                            else FastPoint(DrawX, DrawY - 4, 3, 3, GetDDColor(clRed))

          end; }

{        if ViewACState then
          begin
            DrawY := BScanRt.Bottom - ChIdx * 5;
            if FDatSrc.CurAKExists[R, ChNum] then FastPoint(DrawX, DrawY - 4, 1, 3, GetDDColor(clGreen))
                                             else FastPoint(DrawX, DrawY - 4, 1, 3, GetDDColor(clRed))

          end; }

        for J := 1 to FDatSrc.CurEcho[R, ChNum].Count do
        begin
          DrawY := BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.CurEcho[R, ChNum].Delay[J] / DelayMultiply + BScanShift - BScanGateMin) / BScanDuration);
          if Config.InvertDelayAxis then DrawY := BScanRt.Top + BScanRt.Bottom - DrawY;

          if MirrorShadow then
          begin

            if (MirrorShadowEvalChNum[0] <> -1) and (FDatSrc.CurEcho[R, ChNum].Delay[J] / DelayMultiply >= FDatSrc.CurParams.Par[R, MirrorShadowEvalChNum[0]].StStr) and (FDatSrc.CurEcho[R, ChNum].Delay[J] / DelayMultiply <= FDatSrc.CurParams.Par[R, MirrorShadowEvalChNum[0]].EndStr) then
            begin
              Ch0Flg := True;
              // if FDatSrc.isEGOUSW and (FDatSrc.Header.ControlDirection = cd_Head_B_Tail_A)
              if FDatSrc.isEGOUSW_BHead then EvalChNum := ZerroProbEchoEvalChNum[0]
                                        else EvalChNum := MirrorShadowEvalChNum[0];
            end
            else if (MirrorShadowEvalChNum[1] <> -1) and (FDatSrc.CurEcho[R, ChNum].Delay[J] / DelayMultiply >= FDatSrc.CurParams.Par[R, MirrorShadowEvalChNum[1]].StStr) and (FDatSrc.CurEcho[R, ChNum].Delay[J] / DelayMultiply <= FDatSrc.CurParams.Par[R, MirrorShadowEvalChNum[1]].EndStr) then
            begin
              Ch0Flg := True;
              // if FDatSrc.isEGOUSW and (FDatSrc.Header.ControlDirection = cd_Head_B_Tail_A)
              if FDatSrc.isEGOUSW_BHead then EvalChNum := ZerroProbEchoEvalChNum[1]
                                        else EvalChNum := MirrorShadowEvalChNum[1];
            end
            else
            begin

              Ch0Flg := False;
              // if FDatSrc.isEGOUSW and (FDatSrc.Header.ControlDirection = cd_Head_B_Tail_A)
              if FDatSrc.isEGOUSW_BHead then EvalChNum := MirrorShadowEvalChNum[0]
                                        else EvalChNum := ZerroProbEchoEvalChNum[0];
            end;
            if not((FDM.ViewChannel = vcAll) or (FDM.ViewChannel = vcOtezd)) then Continue;
          end
          else
          begin
            Ch0Flg := False;
            if not ((FDM.ViewChannel = vcAll) or

                    ((Abs(TurnAngle) <> 90) and (FDM.ViewChannel = vcOtezd) and (EnterAngle < 0)) or
                    ((Abs(TurnAngle) <> 90) and (FDM.ViewChannel = vcNaezd) and (not(EnterAngle < 0))) or

                    ((Abs(TurnAngle) = 90) and (FDM.ViewChannel = vcOtezd) and (Gran = _UnWork)) or
                    ((Abs(TurnAngle) = 90) and (FDM.ViewChannel = vcNaezd) and (Gran = _Work))) then Continue;

            // EvalChNum:= EvalChNumList[0]; // ChNum;
            tmp := FDatSrc.Config.GetEvalsChIdxByScanChNum(ChNum);
            EvalChNum := tmp[0];
            SetLength(tmp, 0);

          end;

          // if not FEchoColorByAmpl then Color:= ChNumColors[R, EvalChNum, 15]
          // else Color:= ChNumColors[R, EvalChNum, FDatSrc.CurEcho[R, ChNum].Ampl[J]];

          if EvalChNum <> -1 then
            if not FEchoColorByAmpl then Color := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, EvalChNum], 15]
                                    else Color := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, EvalChNum], FDatSrc.CurEcho[R, ChNum].Ampl[J]];

          if FDatSrc.Config.ReductionConst then
          begin
            Coeff:= 1 - 2 * Ord(FDatSrc.isEGOUSW_BHead);
            case FDM.Reduction of
              0: DX := 0;
              1: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * FDatSrc.CurEcho[R, ChNum].Delay[J]) / (FDM.Zoom / 100));
              2: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * FDatSrc.CurEcho[R, ChNum].Delay[J]) / (FDM.Zoom / 100));
            end;
          end
          else
          case FDM.Reduction of
            0: DX := 0;
            1: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead) + FDatSrc.Config.ScanChannelByNum[ChNum].Delta) / (FDM.Zoom / 100));
// 2012           1: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round( GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
            2: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.CurEcho[R, ChNum].Delay[J], FDatSrc.isEGOUSW_BHead) { + FDatSrc.Config.ScanChannelByNum[ChNum].Delta}) / (FDM.Zoom / 100));
// 2012           2: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.CurEcho[R, ChNum].Delay[J], FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
          end;

           if Assigned(FFiltr) then
             case FFilterMode of
               moOff: ;
              moMark: case FDrawMode of
                        1: if not FFiltr.TestEcho(FDatSrc.CurDisCoord, R, ChNum, FDatSrc.CurEcho[R, ChNum].Ampl[J], FDatSrc.CurEcho[R, ChNum].Delay[J]) then Color:= FDispCols.FiltrEcho else Continue;
                        2: if not FFiltr.TestEcho(FDatSrc.CurDisCoord, R, ChNum, FDatSrc.CurEcho[R, ChNum].Ampl[J], FDatSrc.CurEcho[R, ChNum].Delay[J]) then Continue;
                      end;
              moHide: if not FFiltr.TestEcho(FDatSrc.CurDisCoord, R, ChNum, FDatSrc.CurEcho[R, ChNum].Ampl[J], FDatSrc.CurEcho[R, ChNum].Delay[J]) then Continue;
             end;

          if FDatSrc.CurEcho[R, ChNum].Ampl[J] >= FDM.AmplTh then
          // Ограничение влияния порога отображения на ДС
          begin
            FastPoint(DrawX + DX, DrawY, CalcPixelSize(FScanChNumToBScanLine[R, ChNum], FDatSrc.CurEcho[R, ChNum].Ampl[J]).X, CalcPixelSize(FScanChNumToBScanLine[R, ChNum], FDatSrc.CurEcho[R, ChNum].Ampl[J]).Y, GetDDColor(Color));
//            Inc(PixelCount_);
          end;

          if Ch0Flg and FDM.AmplDon then
          begin
            if FDatSrc.CurEcho[R, ChNum].Ampl[J] > DonAmpl then
            begin
              DonAmpl := FDatSrc.CurEcho[R, ChNum].Ampl[J];
              DonDelay := FDatSrc.CurEcho[R, ChNum].Delay[J];
            end;
          end;
        end;
        if DonAmpl <> -1 then DrawBSAmplSample(Line_, R, DrawX + DX, Result, DonAmpl);
      end;
    end;
  end;
end;

(*
  function TAvk11Display.DrawBScanSample2(StartDisCoord: Integer): Boolean;
  var
  R: TRail;
  Ch: Integer;
  I, J, K, MaxA, MaxI: Integer;
  DrawX: Integer;
  DrawY: Integer;
  Ch0Flg: Boolean;
  //  Tmp: TAvk11Header2;
  //  Tmp2: TAvk11Header2;
  Col: TColor;
  SaveCol: TColor;

  begin
  if not Assigned(FDatSrc) then Exit;

  try
  DrawX:= FStartScrX + (FDatSrc.CurDisCoord - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom
  except
  Exit;
  end;
  if Reduction <> 0 then Result:= DrawX <= FEndScrX + Round(FMaxReduce[FDM.Reduction] / (Zoom / 100))
  else Result:= DrawX <= FEndScrX;

  // -------------------- Отрисовка огибающей донного сигнала ------------------------------------
  (*
  if FDM.Reduction <> 0 then Inc(DrawX, Round(FDispConf.ReducePos[FDM.ReductionMode, FDM.Reduction, 1] / (FDM.Zoom / 100)));

  SaveCol:= FDispCols.EnvelopeBS;
  FDispCols.EnvelopeBS:= clRed;
  //  Tmp2:= FDatSrc.Header;
  //  Tmp:= Tmp2;
  //  Tmp.ContentFlg:= 0;
  //  FDatSrc.Header:= Tmp;

  if FAmplDon then
  for R:= rLeft to rRight do
  begin
  for I:= 0 to High(FBScanLines) do
  if (FBScanLines[I].Rail = R) and (FBScanLines[I].Channel = - 10) then
  begin
  DrawBSAmplSample(I, R, DrawX, Result);
  Break;
  end;
  end;
  * )
  //  FDatSrc.Header:= Tmp2;
  FDispCols.EnvelopeBS:= SaveCol;
  end;
*)

function TAvk11Display.DrawRailViewSample(StartDisCoord: Integer): Boolean;
var
  R: TRail;
  // RRR: TRect;
  ChNum: Integer;
  Flg: Boolean;
  Color: Int64;
  DrawX1: Integer;
  DrawX2: Integer;
  DrawY: Integer;
  DrawY2: Integer;
  DrawOK: Boolean;
  DX, I, J, K, H, NewDelay, Line, ChIdx: Integer;
  Ch0Flg { , Second } : Boolean;
  EvalChNum: Integer;
  tmp: TIntegerDynArray;
  Coeff: Integer;

begin
  if not Assigned(FDatSrc) then
    Exit;

  try
    DrawX1 := FStartScrX + (FDatSrc.CurDisCoord - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
  except
    Exit;
  end;
  if FDM.Reduction <> 0 then
    Result := DrawX1 <= FEndScrX + Round(FMaxReduce[FDM.Reduction] / (FDM.Zoom / 100))
  else
    Result := DrawX1 <= FEndScrX;

  for Line := 0 to High(FBScanLines) do
  begin
    R := FBScanLines[Line].Rail;
    for ChIdx := 0 to High(FBScanLines[Line].ScanChNumList) do
    begin

      ChNum := Abs(FBScanLines[Line].ScanChNumList[ChIdx]);

//     if not (ChNum in [2, 3, 13]) then continue;

      with FDatSrc.Config.ScanChannelByNum[ChNum], FBScanLines[Line], FBScanLines[Line].BScanRt do
      begin
        SetClipRect(FBScanLines[Line].BScanRt);

        // Вырезка двойного донного по технологии Косенко
        {
          if CompareMem(@FDatSrc.Header.DeviceID, @Avicon15Scheme1, 8) and
          MirrorShadow and
          (FDatSrc.CurEcho[R, ChNum].Count = 2) and
          (FDatSrc.CurEcho[R, ChNum].Delay[1] / DelayMultiply > FDatSrc.CurParams.Par[R, 1].EndStr) and
          (FDatSrc.CurEcho[R, ChNum].Delay[2] / DelayMultiply > FDatSrc.CurParams.Par[R, 1].EndStr) then FDatSrc.FCurEcho[R, ChNum].Count:= 1;
          }
        for J := 1 to FDatSrc.CurEcho[R, ChNum].Count do
        begin

          if (FDatSrc.CurEcho[R, ChNum].Ampl[J] >= FDM.AmplTh) and (FDatSrc.CurEcho[R, ChNum].Delay[J] / DelayMultiply >= BScanGateMin) and (FDatSrc.CurEcho[R, ChNum].Delay[J] / DelayMultiply <= BScanGateMax) then

          begin
            NewDelay := FDatSrc.CurEcho[R, ChNum].Delay[J];
            if FDatSrc.Config.ReductionConst then
            begin
              Coeff:= 1 - 2 * Ord(FDatSrc.isEGOUSW_BHead);
              case FDM.Reduction of
                0: DX := 0;
                1: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + (Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum]) * FDatSrc.CurEcho[R, ChNum].Delay[J]) / (FDM.Zoom / 100));
                2: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + (Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum]) * FDatSrc.CurEcho[R, ChNum].Delay[J]) / (FDM.Zoom / 100));
              end;
            end
            else
            case FDM.Reduction of
              0: DX := 0;
              1: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
              2: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.CurEcho[R, ChNum].Delay[J], FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
            end;

            DrawX2 := DrawX1 + DX;
            if (DrawX2 < FStartScrX) or (DrawX2 > FEndScrX) then Continue;
            if FRailChem[R, DrawX2].RailHeight = 0 then Continue;

            /// ////////////////

            begin

              if Abs(TurnAngle) = 90 then
              begin
//          DrawY := BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.CurEcho[R, ChNum].Delay[J] / DelayMultiply - BScanGateMin) / BScanDuration);
                DrawY := FBScanLines[Line].BScanRt.Top + FRailChem[R, DrawX2].RailHead * (NewDelay + BScanShift) div 60;
                //Continue;
              end
              else
              if (EnterAngle = 0) then
              begin
                DrawY := FBScanLines[Line].BScanRt.Top + Round(FRailChem[R, DrawX2].Height * NewDelay / DelayMultiply / 70);
                //Continue;
                // if ChIdx = 9 then Continue;
                // DrawY:= FBScanLines[Line].BScanRt.Top + NewDelay;
              end
              else if ((Abs(EnterAngle) = 58) and (Abs(TurnAngle) = 34)) or (Abs(EnterAngle) = 50) then
              begin
                case NewDelay of
                    0 .. 40: DrawY := FBScanLines[Line].BScanRt.Top + FRailChem[R, DrawX2].RailHead * NewDelay div 40;
                   41 .. 80: DrawY := FBScanLines[Line].BScanRt.Top + FRailChem[R, DrawX2].RailHead - FRailChem[R, DrawX2].RailHead * (NewDelay - 41) div 40;
                  81 .. 120: DrawY := FBScanLines[Line].BScanRt.Top + FRailChem[R, DrawX2].RailHead * (NewDelay - 81) div 40;
                end;
                //Continue;
              end
              else if (TurnAngle = 0) and ((Abs(EnterAngle) = 70) or (Abs(EnterAngle) = 65)) then
              begin
                DrawY := FBScanLines[Line].BScanRt.Top + FRailChem[R, DrawX2].RailHeight * NewDelay div (85 * 3);
                //Continue;
              end
              else if (TurnAngle = 0) and ((Abs(EnterAngle) = 42) or (Abs(EnterAngle) = 45)) then
              begin
                if NewDelay in [0 .. FRailChem[R, DrawX2].BackTime] then
                begin
                  DrawY := FBScanLines[Line].BScanRt.Top + Round(FRailChem[R, DrawX2].RailHeight * (NewDelay * 3.26 * Cos(Abs(EnterAngle) * pi / 180) / 2) / FRailChem[R, DrawX2].RailHeight_mm);
                //  DrawY := FBScanLines[Line].BScanRt.Top + FRailChem[R, DrawX2].RailHeight * NewDelay div FRailChem[R, DrawX2].BackTime
                //  Cur.BackTime := Round(2 * ((5.9 * (MSDelay / 2)) / Cos((90 - 42) * pi / 180)) / 3.26);
                //  DrawY := FBScanLines[Line].BScanRt.Top + FRailChem[R, DrawX2].Height * NewDelay div FRailChem[R, DrawX2].BackTime
                end
                else
                begin
                  H:= Round((NewDelay * 3.26 * Cos(Abs(EnterAngle) * pi / 180) / 2)) - FRailChem[R, DrawX2].RailHeight_mm;
                  DrawY := FBScanLines[Line].BScanRt.Top + FRailChem[R, DrawX2].RailHeight - Round(FRailChem[R, DrawX2].RailHeight * H / FRailChem[R, DrawX2].RailHeight_mm);
                  //DrawY := FBScanLines[Line].BScanRt.Top + FRailChem[R, DrawX2].Height - FRailChem[R, DrawX2].Height * (NewDelay - (FRailChem[R, DrawX2].BackTime + 1)) div FRailChem[R, DrawX2].BackTime;
                end;
              end;
            end;

            /// ////////////////

            if (FBScanLines[Line].MirrorShadowScanChNum[0] = ChNum) or (FBScanLines[Line].MirrorShadowScanChNum[1] = ChNum) then
            begin
              if (MirrorShadowEvalChNum[0] <> -1) and (NewDelay / DelayMultiply >= FDatSrc.CurParams.Par[R, FBScanLines[Line].MirrorShadowEvalChNum[0]].StStr) and (NewDelay / DelayMultiply <= FDatSrc.CurParams.Par[R, FBScanLines[Line].MirrorShadowEvalChNum[0]].EndStr) then
              begin
                Ch0Flg := True;
//                EvalChNum := FBScanLines[Line].MirrorShadowEvalChNum[0];
                if FDatSrc.isEGOUSW_BHead then EvalChNum := ZerroProbEchoEvalChNum[0]
                                          else EvalChNum := MirrorShadowEvalChNum[0];
              end
              else if (MirrorShadowEvalChNum[1] <> -1) and (NewDelay / DelayMultiply >= FDatSrc.CurParams.Par[R, FBScanLines[Line].MirrorShadowEvalChNum[1]].StStr) and (NewDelay / DelayMultiply <= FDatSrc.CurParams.Par[R, FBScanLines[Line].MirrorShadowEvalChNum[1]].EndStr) then
              begin
                Ch0Flg := True;
//                EvalChNum := FBScanLines[Line].MirrorShadowEvalChNum[1];
                if FDatSrc.isEGOUSW_BHead then EvalChNum := ZerroProbEchoEvalChNum[1]
                                          else EvalChNum := MirrorShadowEvalChNum[1];
              end
              else
              begin
                Ch0Flg := False;
//                EvalChNum := FBScanLines[Line].ZerroProbEchoEvalChNum[0];
                if FDatSrc.isEGOUSW_BHead then EvalChNum := MirrorShadowEvalChNum[0]
                                          else EvalChNum := ZerroProbEchoEvalChNum[0];
              end;
              if not((FDM.ViewChannel = vcAll) or (FDM.ViewChannel = vcOtezd)) then Continue;
            end
            else
            begin
              Ch0Flg := False;

              if not ((FDM.ViewChannel = vcAll) or

                      ((Abs(TurnAngle) <> 90) and (FDM.ViewChannel = vcOtezd) and (EnterAngle < 0)) or
                      ((Abs(TurnAngle) <> 90) and (FDM.ViewChannel = vcNaezd) and (not(EnterAngle < 0))) or

                      ((Abs(TurnAngle) = 90) and (FDM.ViewChannel = vcOtezd) and (Gran = _UnWork)) or
                      ((Abs(TurnAngle) = 90) and (FDM.ViewChannel = vcNaezd) and (Gran = _Work))) then Continue;


              tmp := FDatSrc.Config.GetEvalsChIdxByScanChNum(ChNum);
              EvalChNum := tmp[0];
              SetLength(tmp, 0);

            end;


            // if not FEchoColorByAmpl then Color:= ChNumColors2[R, EvalChNum, 15]
            // else Color:= ChNumColors2[R, EvalChNum, FDatSrc.CurEcho[R, ChNum].Ampl[J]];

            if not FEchoColorByAmpl then Color := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, EvalChNum], 15]
                                    else Color := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, EvalChNum], FDatSrc.CurEcho[R, ChNum].Ampl[J]];

            if Assigned(FFiltr) then
              case FFilterMode of
                 moOff: ;
                moMark: case FDrawMode of
                          1: if not FFiltr.TestEcho(FDatSrc.CurDisCoord, R, ChNum, FDatSrc.CurEcho[R, ChNum].Ampl[J], FDatSrc.CurEcho[R, ChNum].Delay[J]) then Color:= FDispCols.FiltrEcho else Continue;
                          2: if not FFiltr.TestEcho(FDatSrc.CurDisCoord, R, ChNum, FDatSrc.CurEcho[R, ChNum].Ampl[J], FDatSrc.CurEcho[R, ChNum].Delay[J]) then Continue;
                        end;
                moHide: if not FFiltr.TestEcho(FDatSrc.CurDisCoord, R, ChNum, FDatSrc.CurEcho[R, ChNum].Ampl[J], FDatSrc.CurEcho[R, ChNum].Delay[J]) then Continue;
              end;

//            FastPoint(DrawX2, DrawY, 2, 3, GetDDColor(Color));
            FastPoint(DrawX2, DrawY, CalcPixelSize(FScanChNumToBScanLine[R, ChNum], FDatSrc.CurEcho[R, ChNum].Ampl[J]).X, CalcPixelSize(FScanChNumToBScanLine[R, ChNum], FDatSrc.CurEcho[R, ChNum].Ampl[J]).Y, GetDDColor(Color));

          end;
        end;
      end;
    end;
  end;

  // -------------------- Отрисовка огибающей донного сигнала ------------------------------------

  // if FDM.Reduction <> 0 then Inc(DrawX1, Round(FDispConf.ReducePos[FDM.ReductionMode, FDM.Reduction, 1] / (FDM.Zoom / 100)));
  {
    if FAmplDon then
    for R:= rLeft to rRight do
    begin
    for I:= 0 to High(FBScanLines) do
    if (FBScanLines[I].Rail = R) and (FBScanLines[I].Channel = - 10) then
    begin
    DrawBSAmplSample(I, R, DrawX1, Result);
    Break;
    end;
    end; }
  (*

    if FAmplDon then
    for R:= r_Left to rRight do
    begin
    K:= - 1;
    for I:= 0 to High(FBScanLines) do
    if (FBScanLines[I].Rail = R) and (FBScanLines[I].Channel = - 10) then
    begin
    K:= I;
    Break;
    end;
    if K = - 1 then Continue;

    with FBScanLines[K] do
    begin
    DrawOK:= False;
    SetClipRect(DrawRect);

    DrawY:= - 1;
    for J:= 1 to FDatSrc.CurEcho[R, 1].Count do
    if (FDatSrc.CurEcho[R, 1].Delay[J] / FDatSrc.DelayKoeff >= FDatSrc.CurParams.StStr[R, 0]) and
    (FDatSrc.CurEcho[R, 1].Delay[J] / FDatSrc.DelayKoeff <= FDatSrc.CurParams.EdStr[R, 0]) then
    begin
    DrawY:= DrawRect.Bottom - Abs(DrawRect.Bottom - DrawRect.Top) * (FDatSrc.CurEcho[R, 1].Ampl[J] - FDatSrc.Header.AmplTH) div (15 - FDatSrc.Header.AmplTH);
    Break;
    end;

    if DrawY <> - 1 then
    begin
    if FLastDrawX2[R] <> - 1 then
    begin
    for I:= {FLastDrawX2[R]} DrawX1 to DrawX1 do FastPoint(I, FLastDrawY2[R], 2, 2, FDispCols.EnvelopeBS);
    //  if FLastDrawX2[R] + 1 = DrawX then
    for I:= Min(FLastDrawY2[R], DrawY) to Max(FLastDrawY2[R], DrawY) do FastPoint(DrawX1, I, 2, 2, FDispCols.EnvelopeBS);
    end;
    FLastDrawX2[R]:= DrawX1;
    FLastDrawY2[R]:= DrawY;
    end;

    end
    end; *)
end;

function TAvk11Display.RailSideViewSample(StartDisCoord: Integer): Boolean;
{ var
  R: TRail;
  Ch: Integer;
  I, J, K, L, C, C2: Integer;
  DrawX: Integer;
  DrawY: Integer;
  Color: Int64;
  DrawY2: Integer;
  Flg: Boolean;
  DrawOK: Boolean;
}
begin
  (*
    Result:= True;
    if not Assigned(FDatSrc) then Exit;

    L:= FDatSrc.CurDisCoord - StartDisCoord;
    {
    if FDM.Reduction <> 0 then Result:= DrawX <= FEndScrX + Round(FMaxReduce[FDM.Reduction] / (FDM.Zoom / 100))
    else Result:= DrawX <= FEndScrX;
    }
    if (L < 0) or (L > 2000) then Exit;

    C2:= StartDisCoord + FMaxReduce[FDM.Reduction] + 10;


    for R:= rLeft to rRight do
    begin
    if FRailChem[R, L].RailHeight = 0 then Continue;

    for Ch:= 0 to 15 do
    if FScanChNumToBScanLine[R, Ch] <> - 1 then
    begin
    with FBScanLines[FScanChNumToBScanLine[R, Ch]] do
    begin
    SetClipRect(FRailSide[R]);
    for J:= 1 to FDatSrc.CurEcho[R, Ch].Count do
    begin
    C:= (FDatSrc.CurDisCoord + (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((FDispConf.ReducePos[FDM.ReductionMode, FDM.Reduction, Ch] + FDispConf.ReduceDel[FDM.ReductionMode, FDM.Reduction, Ch] * ) / (FDatSrc.Header.ScanStep / 100)));

    C:= (FDatSrc.CurDisCoord +


    (1 - 2 * Ord(FDatSrc.BackMotion)) * Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.CurEcho[R, Ch].Delay[J]) / (FDatSrc.Header.ScanStep / 100));

    if (FDatSrc.CurEcho[R, Ch].Ampl[J] >= FDM.AmplTh) and
    (C >= C2 - 10) and (C <= C2 + 10) then
    begin
    with FDispConf.BScanViewZone[Ch] do
    case Ch of
    2, 3, 5: begin
    case FDatSrc.CurEcho[R, Ch].Delay[J] of
    0..40: begin
    DrawY:= FRailSide[R].Top  + Round(RPts[0].Y * FSideViewZoom[R] + (RPts[8].Y - RPts[0].Y) * FSideViewZoom[R] * FDatSrc.CurEcho[R, Ch].Delay[J] / 40);
    DrawX:= FRailSide[R].Left + Round(RPts[0].X * FSideViewZoom[R] + (RPts[8].X - RPts[0].X) * FSideViewZoom[R] * FDatSrc.CurEcho[R, Ch].Delay[J] / 40);
    end;
    41..80: begin
    DrawY:= FRailSide[R].Top  + Round(RPts[8].Y * FSideViewZoom[R] + (RPts[2].Y - RPts[8].Y) * FSideViewZoom[R] * (FDatSrc.CurEcho[R, Ch].Delay[J] - 40) / 40);
    DrawX:= FRailSide[R].Left + Round(RPts[8].X * FSideViewZoom[R] + (RPts[2].X - RPts[8].X) * FSideViewZoom[R] * (FDatSrc.CurEcho[R, Ch].Delay[J] - 40) / 40);
    end;
    81..120: begin
    DrawY:= FRailSide[R].Top  + Round(RPts[2].Y * FSideViewZoom[R] + (RPts[7].Y - RPts[2].Y) * FSideViewZoom[R] * (FDatSrc.CurEcho[R, Ch].Delay[J] - 80) / 40);
    DrawX:= FRailSide[R].Left + Round(RPts[2].X * FSideViewZoom[R] + (          - RPts[2].X) * FSideViewZoom[R] * (FDatSrc.CurEcho[R, Ch].Delay[J] - 80) / 40);
    end;
    end;
    end;
    4: begin
    DrawY:= BScanRt.Top + FRailChem[R, L].RailHeight * FDatSrc.CurEcho[R, Ch].Delay[J] div (85 * 3);
    DrawX:= FRailSide[R].Left + 1;
    end;
    6, 7: begin
    if FDatSrc.CurEcho[R, Ch].Delay[J] in [0..FRailChem[R, L].BackTime]
    then DrawY:= FRailSide[R].Top + FRailChem[R, L].RailHeight * FDatSrc.CurEcho[R, Ch].Delay[J] div FRailChem[R, L].BackTime
    else DrawY:= FRailSide[R].Top + FRailChem[R, L].RailHeight - FRailChem[R, L].RailHeight * (FDatSrc.CurEcho[R, Ch].Delay[J] - (FRailChem[R, L].BackTime + 1)) div FRailChem[R, L].BackTime;
    DrawX:= FRailSide[R].Left + 1;
    end;
    1, 0: begin
    //                         DrawY:= FRailSide[R].Top + Round(FRailChem[R, L].Height * FDatSrc.CurEcho[R, Ch].Delay[J] / FDatSrc.DelayKoeff / 70);
    DrawX:= FRailSide[R].Left + 1;
    end;
    end;

    //              if Ch = 1 then
    //              begin
    //                if (FDatSrc.CurEcho[R, Ch].Delay[J] / FDatSrc.DelayKoeff >= FDatSrc.CurParams.Par[R, 0].StStr) and
    //                   (FDatSrc.CurEcho[R, Ch].Delay[J] / FDatSrc.DelayKoeff <= FDatSrc.CurParams.Par[R, 0].EndStr) then
    //                begin
    //                  if FEchoColorByAmpl then Color:= FNaezdChByAmpl[FDatSrc.CurEcho[R, Ch].Ampl[J]]
    //                                      else Color:= FDispCols.NaezdCh;
    //                  if not ((FDM.ViewChannel = vcAll) or (FDM.ViewChannel = vcNaezd)) then Continue;
    //                end
    //                else
    //                begin
    //                  if FEchoColorByAmpl then Color:= FOtezdChByAmpl[FDatSrc.CurEcho[R, Ch].Ampl[J]]
    //                                      else Color:= FDispCols.OtezdCh;
    //                  if not ((FDM.ViewChannel = vcAll) or (FDM.ViewChannel = vcOtezd)) then Continue;
    //                end;
    //              end
    //              else
    begin
    if FEchoColorByAmpl then
    begin
    if Odd(Ch) then Color:= FOtezdChByAmpl[FDatSrc.CurEcho[R, Ch].Ampl[J]]
    else Color:= FNaezdChByAmpl[FDatSrc.CurEcho[R, Ch].Ampl[J]];
    end
    else
    begin
    if Odd(Ch) then Color:= FDispCols.OtezdCh1
    else Color:= FDispCols.NaezdCh1;
    end;
    if not ((FDM.ViewChannel = vcAll) or ((FDM.ViewChannel = vcOtezd) and Odd(Ch)) or ((FDM.ViewChannel = vcNaezd) and (not Odd(Ch)))) then Continue;
    end;

    //              FastPoint(DrawX, DrawY, 3 + FDispConf.PixelSize[FDatSrc.CurEcho[R, Ch].Ampl[J]].X, FDispConf.PixelSize[FDatSrc.CurEcho[R, Ch].Ampl[J]].Y, Color)
    FastPoint(DrawX, DrawY, 3 + CalcPixelSize(FScanChNumToBScanLine[R, Ch], FDatSrc.CurEcho[R, Ch].Ampl[J]).X,
    CalcPixelSize(FScanChNumToBScanLine[R, Ch], FDatSrc.CurEcho[R, Ch].Ampl[J]).Y, Color)



    end;
    end;
    end;
    end;
    end; *)
end;

procedure TAvk11Display.DrawBackMotionZone(StartDisCoord, StartX, EndX, Mode: Integer);
type
  TDBM = record
    X: Integer;
    BM: Boolean;
  end;

var
  R: TRail;
  I: Integer;
  X1: Integer;
  X2: Integer;
  Idx: Integer;
  Ch: Integer;
  BMFlag: Boolean;
  EndFlag: Boolean;
  DisCoord: Integer;
  DBM: array of TDBM;
  Col: DWord;

begin
  if not Assigned(FDatSrc) then Exit;

  if Mode = 1 then
  begin
    {
      for I:= 0 to High(FBScanLines) do
      begin
      if FBScanLines[I].Channel = - 10 then
      FillRect(StartX, FBScanLines[I].DrawRect.Top, EndX, FBScanLines[I].DrawRect.Bottom, FDispCols.Fon);
      end;
      }

    Col := FDispCols.Fon;
    // for R:= rLeft to rRight do
    for Idx := 0 to High(FBScanLines) do
      // if (FScanChNumToBScanLine[R, Ch] <> - 1) and Odd(Ch) then
      // with FBScanLines[FScanChNumToBScanLine[R, Ch]] do
      with FBScanLines[Idx] do
      { for Ch:= 0 to 15 do
        if (FScanChNumToBScanLine[R, Ch] <> - 1) and Odd(Ch) then

        with FBScanLines[FScanChNumToBScanLine[R, Ch]] do }
      begin
        // if (Abs(X1 - X2) >= 2) and (X1 <> EndX) then FillRect(X1-1, DrawRect.Top, X1, DrawRect.Bottom, FDispCols.Border);
        FillRect(BScanRt.Left, BScanRt.Top, BScanRt.Right, BScanRt.Bottom, Col);
      end;

  end
  else if Mode = 2 then
  begin
    SetLength(DBM, 1);
    DBM[0].X := StartX;
    DBM[0].BM := FDatSrc.GetBMStateFirst(StartDisCoord);

    repeat
      DisCoord := -MaxInt;
      EndFlag := not FDatSrc.GetBMStateNext(DisCoord, BMFlag);
      if DisCoord = -MaxInt then
        Break;

      X1 := StartX + DisToScr(DisCoord - StartDisCoord);
      if X1 > EndX then
        Break;
      SetLength(DBM, Length(DBM) + 1);
      DBM[ High(DBM)].X := X1;
      DBM[ High(DBM)].BM := BMFlag;
    until EndFlag;

    if DBM[ High(DBM)].X < EndX then
    begin
      X1 := StartX + DisToScr(DatSrc.MaxDisCoord - StartDisCoord);
      if X1 < EndX then
      begin
        SetLength(DBM, Length(DBM) + 1);
        DBM[ High(DBM)].X := X1;
        DBM[ High(DBM)].BM := False;
      end;
      SetLength(DBM, Length(DBM) + 1);
      DBM[ High(DBM)].X := EndX;
    end;

    Col := FDispCols.BackMotion;
    for I := 0 to High(DBM) - 1 do
    begin
      X1 := DBM[I].X;
      X2 := DBM[I + 1].X;

      if DBM[I].BM and (Abs(X1 - X2) < 2) then
        Continue;

      // if DBM[I].BM then Col:= FDispCols.BackMotion
      // else Col:= FDispCols.Fon;

      // for R:= rLeft to rRight do
      for Idx := 0 to High(FBScanLines) do
        // if (FScanChNumToBScanLine[R, Ch] <> - 1) and Odd(Ch) then
        // with FBScanLines[FScanChNumToBScanLine[R, Ch]] do
        with FBScanLines[Idx] do
        begin
          if DBM[I].BM then
          begin
            if (Abs(X1 - X2) >= 2) and (X1 <> EndX) then
              FillRect(X1 - 1, BScanRt.Top, X1, BScanRt.Bottom, FDispCols.Border);
            FillRect(X1, BScanRt.Top, X2, BScanRt.Bottom, Col);
          end
          else if (Abs(X1 - X2) >= 2) and (X1 <> EndX) and (I <> 0) then
            FillRect(X1, BScanRt.Top, X1, BScanRt.Bottom, FDispCols.Border);
          // TextOut(0, X1, 10, 0, IntToStr(I), True);
        end;
    end;

    SetLength(DBM, 0);
  end;

  {
    EndFlag:= False;
    BMFlag:= FDatSrc.GetBMStateFirst(StartDisCoord);
    DisCoord:= StartDisCoord;

    X1:= StartX;
    repeat
    X2:= StartX + DisToScr(DisCoord - StartDisCoord);
    if X2 > EndX then X2:= EndX;
    if BMFlag then Color:= FDispCols.Fon
    else Color:= FDispCols.BackMotion;

    for R:= r_Left to rRight do
    for Ch:= 1 to 7 do
    if (FChIdx[R, Ch] <> - 1) and Odd(Ch) then
    with FBScanLines[FChIdx[R, Ch]] do
    begin
    if X1 <> EndX then FillRect(X1-1, DrawRect.Top, X1, DrawRect.Bottom, FDispCols.Border);
    FillRect(X1, DrawRect.Top, X2, DrawRect.Bottom, Color);
    end;
    X1:= X2;
    until (not FDatSrc.GetBMStateNext(DisCoord, BMFlag)) or (X2 = EndX);

    if X2 <> EndX then
    begin
    if BMFlag then Color:= FDispCols.BackMotion
    else Color:= FDispCols.Fon;

    X2:= StartX + DisToScr(FDatSrc.MaxDisCoord - StartDisCoord);
    if X2 > EndX then X2:= EndX;

    if X1 <= EndX then
    for R:= r_Left to rRight do
    for Ch:= 1 to 7 do
    if (FChIdx[R, Ch] <> - 1) and Odd(Ch) then
    with FBScanLines[FChIdx[R, Ch]] do
    begin
    if X1 <> EndX then FillRect(X1-1, DrawRect.Top, X1, DrawRect.Bottom, FDispCols.Border);
    FillRect(X1, DrawRect.Top, X2, DrawRect.Bottom, Color);
    end;

    X1:= X2;
    X2:= EndX;
    Color:= FDispCols.Fon;
    if X1 <= EndX then
    if X1 <= EndX then
    for R:= r_Left to rRight do
    for Ch:= 1 to 7 do
    if (FChIdx[R, Ch] <> - 1) and Odd(Ch) then
    with FBScanLines[FChIdx[R, Ch]] do
    begin
    if X1 <> EndX then FillRect(X1-1, DrawRect.Top, X1, DrawRect.Bottom, FDispCols.Border);
    FillRect(X1, DrawRect.Top, X2, DrawRect.Bottom, Color);
    end;
    end;
    }
end;

function TAvk11Display.DrawRuler(StartDisCoord, StartX, EndX: Integer): string;
var
  MMStep: Integer;
  DCStep: Integer;
  SCRStep: Integer;
  X1, X2: Integer;
  EndFlag: Boolean;

begin
  if not Assigned(FDatSrc) then
    Exit;
  if not DisplayConfig.Ruler then
    Exit;

  if FDM.Reduction <> 0 then
    Inc(StartDisCoord, Round(FMaxReduce[FDM.Reduction] / (FDatSrc.Header.ScanStep / 100)));

  if FDM.Zoom < 100 then
  begin
    MMStep := 10;
    FRulerText1 := '1';
    FRulerText2 := LangTable.Caption['Common:cm'];
  end
  else if FDM.Zoom < 500 then
  begin
    MMStep := 100;
    FRulerText1 := '10';
    FRulerText2 := LangTable.Caption['Common:cm'];
  end
  else if FDM.Zoom < 3000 then
  begin
    MMStep := 1000;
    FRulerText1 := '1';
    FRulerText2 := LangTable.Caption['Common:m'];
  end
  else if FDM.Zoom < 10000 then
  begin
    MMStep := 2000;
    FRulerText1 := '2';
    FRulerText2 := LangTable.Caption['Common:m'];
  end
  else
  begin
    MMStep := 10000;
    FRulerText1 := '10';
    FRulerText2 := LangTable.Caption['Common:m'];
  end;

  DCStep := MMStep * 100 div FDatSrc.Header.ScanStep;
  SCRStep := MMStep * 100 div FDM.Zoom;

  X1 := FBScanLines[0].BScanRt.Left;
  X2 := FBScanLines[0].BScanRt.Left + DisToScr(DCStep - Trunc(DCStep * Frac(StartDisCoord / DCStep)));

  X1 := FFCoordRect.Left;
  X2 := FFCoordRect.Left + DisToScr(DCStep - Trunc(DCStep * Frac(StartDisCoord / DCStep)));

  // HLine(FFCoordRect.Left, FFCoordRect.Top + 18, FFCoordRect.Right - FFCoordRect.Left, 1, 0);
  // HLine(FFCoordRect.Left, FFCoordRect.Bottom - 19, FFCoordRect.Right - FFCoordRect.Left, 1, 0);

  EndFlag := Odd(Trunc(StartDisCoord / DCStep));
  repeat
    if EndFlag then
      FillRect(X1, FFCoordRect.Top
        { + 19 } , Min(X2, FBScanLines[0].BScanRt.Right), FFCoordRect.Bottom
        { - 20 } , FDispCols.Ruler);
    EndFlag := not EndFlag;
    X1 := X2;
    Inc(X2, SCRStep);
  until X1 > FFCoordRect.Right;
end;

procedure TAvk11Display.DrawCoordGrid(StartDisCoord, StartX, EndX: Integer);
var
  R: TRail;
  S1: string;
  Ch: Integer;
  Ch_Idx: Integer;
  DC, DC_: Integer;
  ShowLevel: Integer;
  MMStep: Integer;
  DCStep: Integer;
  X1, X2: Integer;
  EndFlag: Boolean;
  CoordParams: TCrdParams;
  MMShift: Integer;

begin
  if not Assigned(FDatSrc) then
    Exit;

  // --- Отрисовка координатной сетки -------------------------------------------

  if DisplayConfig.CoordNet then
  begin
    MMStep := 200 * (FDM.Zoom div 100);
    if MMStep = 0 then
      Exit;
    if MMStep < 500 then
      ShowLevel := 0
    else if MMStep < 1000 then
      ShowLevel := 1
    else
      ShowLevel := 2;

    DCStep := MMStep * 100 div Integer(FDatSrc.Header.ScanStep);
    DC := (StartDisCoord div DCStep - 1) * DCStep;

    repeat
      X1 := FBScanLines[0].BScanRt.Left + DisToScr(DC - StartDisCoord);
      if not(X1 < FBScanLines[0].BScanRt.Left) then
      begin
        if X1 > FBScanLines[0].BScanRt.Right then
          Break;

        if (DC >= 0) and (DC <= FDatSrc.MaxDisCoord) then
        begin
          for R := rLeft to rRight do
            for Ch_Idx := 0 to FDatSrc.Config.ScanChannelCount - 1 do
            begin
              Ch := FDatSrc.Config.ScanChannelByIdx[Ch_Idx].BS_Ch_File_Number;
              if FScanChNumToBScanLine[R, Ch] <> -1 then
                with FBScanLines[FScanChNumToBScanLine[R, Ch]] do
                  FillRect(X1, BScanRt.Top, X1, BScanRt.Bottom + 1, FDispCols.CoordNet);
            end;

          FDatSrc.DisToCoordParams(DC, CoordParams);
          S1 := RealCrdToStr(CrdParamsToRealCrd(CoordParams, FDatSrc.Header.MoveDir), ShowLevel);

          if TestScrFusy(X1 - TextWidth(0, S1) div 2, X1 + TextWidth(0, S1) div 2) then
          // TextOut(0, X1, (FFCoordRect.Top + FFCoordRect.Bottom) div 2, 1, S1, True);
          // DrawClippedText(X1, (FFCoordRect.Top + FFCoordRect.Bottom) div 2, 1, S1, True);
          ClippTextOut(0, X1, FFCoordRect.Top + 3 * (FFCoordRect.Bottom - FFCoordRect.Top) div 4, 1, S1, True, FFCoordRect);
        end;
      end;

      Inc(DC, DCStep);
    until False;
  end;
// Тестовая отрисовка для XXX.YYY
{
  if DisplayConfig.CoordNet then
  begin
    MMStep := 200 * (FDM.Zoom div 100);
    if MMStep = 0 then Exit;
    if MMStep < 500 then ShowLevel := 0
    else if MMStep < 1000 then ShowLevel := 1
      else ShowLevel := 2;

    DCStep := MMStep * 100 div Integer(FDatSrc.Header.ScanStep);
    DC := (StartDisCoord div DCStep - 1) * DCStep;

    repeat
      X1 := FBScanLines[0].BScanRt.Left + DisToScr(DC - StartDisCoord);
      if not(X1 < FBScanLines[0].BScanRt.Left) then
      begin
        if X1 > FBScanLines[0].BScanRt.Right then
          Break;

        if (DC >= 0) and (DC <= FDatSrc.MaxDisCoord) then
        begin
          for R := rLeft to rRight do
            for Ch_Idx := 0 to FDatSrc.Config.ScanChannelCount - 1 do
            begin
              Ch := FDatSrc.Config.ScanChannelByIdx[Ch_Idx].Number;
              if FScanChNumToBScanLine[R, Ch] <> -1 then
                with FBScanLines[FScanChNumToBScanLine[R, Ch]] do
                  FillRect(X1, BScanRt.Top, X1, BScanRt.Bottom + 1, FDispCols.CoordNet);
            end;

          FDatSrc.DisToCoordParams(DC, CoordParams);


          MMShift:= Round(1000 * Frac(CoordParams.CaCrdParams.ToLeftPostMM / 1000));

          DC_:= DC - MMShift * 100 div Integer(FDatSrc.Header.ScanStep);
          FDatSrc.DisToCoordParams(DC_, CoordParams);
          X1 := FBScanLines[0].BScanRt.Left + DisToScr(DC_ - StartDisCoord);


          S1 := RealCrdToStr(CrdParamsToRealCrd(CoordParams, FDatSrc.Header.MoveDir), ShowLevel);
          ClippTextOut(0, X1, FFCoordRect.Top + 3 * (FFCoordRect.Bottom - FFCoordRect.Top) div 4, 1, S1, True, FFCoordRect);
        end;
      end;

      Inc(DC, DCStep);
    until False;
  end;
}
  FOnScrText := Format('%3.1f', [(FBScanLines[0].BScanRt.Right - FBScanLines[0].BScanRt.Left) * FDM.Zoom / 100000]);
end;

procedure TAvk11Display.DrawLongEvents(StartDisCoord, StartX, EndX: Integer);
var
  I: Integer;
  J: Integer;
  DrawX: Integer;
  ID: Byte;
  pData: PEventData;
  StartIdx, EndIdx: Integer;
  Start_X: Integer;
  End_X: Integer;

begin
  if not Assigned(FDatSrc) then
    Exit;

  FDatSrc.GetEventIdx(StartDisCoord, FDatSrc.MaxDisCoord, StartIdx, EndIdx, 0);

  Start_X := 0;
  for I := StartIdx to EndIdx do
  begin
    DrawX := StartX + DisToScr(FDatSrc.Event[I].DisCoord - StartDisCoord);
    FDatSrc.GetEventData(I, ID, pData);
    {
      case ID of
      EID_StLineLabel:  Start_X:= DrawX;
      EID_EndLineLabel:  begin
      End_X:= DrawX;
      if Start_X < StartX then Start_X:= StartX;
      if Start_X > EndX then Start_X:= EndX;
      if End_X < StartX then End_X:= StartX;
      if End_X > EndX then End_X:= EndX;
      if Start_X = End_X then
      begin
      Inc(J);
      Continue;
      end;
      FillRect(Start_X, FFCoordRect.Top, End_X, FFCoordRect.Bottom, FDispCols.LongLab);
      //if FDatSrc.RSPFile then TextOut(0, (Start_X + End_X) div 2, (FFCoordRect.Top + FFCoordRect.Bottom) div 2, 1, LangTable.Caption['EventsName:HS:'] + IntToStr(J), False);
      Inc(J);
      AddToScrFusy(Start_X, End_X);
      end;
      end; }
  end;
end;

function GPSSingleToText(A: Single; Lat: Boolean; Mode: Integer = 1): string;
// Широта = Latitude
var
  D, M, S: Longint;
  SW: string;

begin
  try

    if Lat then // Широта = Latitude
    begin

      if (A >= -90) and (A <= 90) then
      begin
        if A >= 0 then
          SW := 'N'
        else
          SW := 'S';
        if Mode = 1 then
        begin
          D := Trunc(A);
          M := Trunc((A - D) * 60);
          S := Trunc((((A - D) * 3600) - M * 60) * 100);
          Result := Format('%s:%d°%d.%d', [SW, D, M, S]);
        end
        else
          Result := Format('%s%2.10f', [SW, A]);
      end
      else
        Result := '';
    end
    else
    begin
      if (A >= -180) and (A <= 180) then
      begin
        if A >= 0 then
          SW := 'E'
        else
          SW := 'W';
        if Mode = 1 then
        begin
          D := Trunc(A);
          M := Trunc((A - D) * 60);
          S := Trunc((((A - D) * 3600) - M * 60) * 100);
          Result := Format('%s:%d°%d.%d', [SW, D, M, S]);
        end
        else
          Result := Format('%s%2.10f', [SW, A]);
      end
      else
        Result := '';
    end;

  except

    Result := '';

  end;

end;

procedure TAvk11Display.DrawEvents(StartDisCoord, StartX, EndX: Integer; DrawHC: Boolean = False);
// const                                     //  0  1  2  3  4  5  6  7  8  9  10 11 12 13 14
// EvalChToScanCh: array [0..13] of Integer = (1, 1, 2, 3, 4, 5, 6, 7, 6, 7, 11,11,12,13);

var
  R: TRail;
  I, J, K, L, M, X: Integer;
  Crd: Integer;
  StartCrd: Integer;
  EndCrd: Integer;
  II: Integer;
  Line: Integer;
  DX1, DX2, X1, X2, Y1, Y2: Integer;
  DrawX: Integer;
  // DrawX_: Integer;
  DrawX1: Integer;
  DrawX2: Integer;
  LastSensor1DrawX: array [TRail] of Integer;
  Sensor1Rail: TRail;
  LeftSensor: array [TRail] of Boolean;
  LeftSensorEx: array [TRail] of Boolean;
  // Left State SensorEventIdx: array [TRail] of Integer;

  ID: Byte;
  Ch: Integer;
  pData: PEventData;
  StartIdx, EndIdx: Integer;
  S: string;
  Start_X: Integer;
  End_X: Integer;
  Flg: Boolean;
  Rt_: TRect;
  Idx: Integer;
  Side: TSide;
  LeftCurTop: TPoint;
  RightCurTop: TPoint;
  Height_: Integer;
  Col: Integer;
  ColR: Integer;
  ColG: Integer;
  ColB: Integer;
  // TmpFlg: Boolean;
  // ED: TFileEventData;
  RailText: string;
  Item: TScanChannelItem;
  ChNum: Integer;
  YYY: Integer;
  // Q: Integer;

  ModeText: string;
  RailText_: string;
  ChName: string;
  TimeName: string;
  TextTop: Integer;
  vvv: Integer;
  YYY_: Integer;

  Sensor1OutTop: Integer;
  Sensor1OutBottom: Integer;
  Sensor1OutLineStep: Integer;

  UnstableTop: array [TRail] of array [1 .. 12] of Integer;
  UnstableBottom: array [TRail] of array [1 .. 12] of Integer;

  CalcIndex: Integer;
  UseData: Boolean;

  OutRect: TRect;
  tmp: TRect;

  Temp_Y: array [rLeft..rRight, 0..16] of Integer;

  BMState: Boolean;
  Coeff: Integer;

//  tmp1: Integer;
//  tmp2: Integer;


  (*
    function GetRedShift(Ch, Delay, X: Integer): Integer;
    begin
    {
    if (FDM.ReductionMode = 0) or (FDM.Reduction = 0)
    then Result:= X
    else Result:= X + Round((FDispConf.ReducePos[FDM.ReductionMode, FDM.Reduction, Ch] + FDispConf.ReduceDel[FDM.ReductionMode, FDM.Reduction, Ch] * Delay) / (FDM.Zoom / 100));
    }
    case FDM.Reduction of
    0: Result:= X;
    1: Result:= X + (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], Delay) + FDatSrc.Config.ScanChannelByNum[ChNum].Delta) / (FDM.Zoom / 100));
    2: Result:= X + (1 - 2 * Ord(FDatSrc.BackMotion)) * Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], Delay) / (FDM.Zoom / 100));
    end;

    //    else Result:= Round(FDispConf.ReducePos[FDM.ReductionMode, FDM.Reduction, Ch] + FDispConf.ReduceDel[FDM.ReductionMode, FDM.Reduction, Ch] * Delay);
    //                                                                                                                     FDatSrc.Header.ScanStep div FDM.Zoom
    end;
    *)

  function MMToSysCrd(MM: Integer): Integer; // Пересчет милиметров в ШДП
  begin
    Result := Round(MM / (FDatSrc.Header.ScanStep / 100));
  end;

begin
  if not Assigned(FDatSrc) then
    Exit;

  DrawX := StartX + DisToScr(0 - StartDisCoord);
  if (DrawX >= StartX) and (DrawX <= EndX) then
    for R := rLeft to rRight do
      for Ch := 0 to High(FBScanLines) do
        with FBScanLines[Ch] do
          FillRect(DrawX - 1, BScanRt.Top, DrawX - 1, BScanRt.Bottom + 1, FDispCols.Border);

  DrawX := StartX + DisToScr(FDatSrc.MaxDisCoord - StartDisCoord);
  if (DrawX >= StartX) and (DrawX <= EndX) then
    for R := rLeft to rRight do
      for Ch := 0 to High(FBScanLines) do
        with FBScanLines[Ch] do
          FillRect(DrawX + 2, BScanRt.Top, DrawX + 1, BScanRt.Bottom + 1, FDispCols.Border);

  J := 1;
  FDatSrc.GetEventIdx(StartDisCoord, FDatSrc.MaxDisCoord, StartIdx, EndIdx, 0);
  SetLength(InfoRect, 0);

  // Отрисовка сигналов датчиков метала

  if FDatSrc.isEGOUSW or FDatSrc.isEGOUS or MainForm.ForceUGOUSW then
    if Config.ShowSensor1DataFlag then
    begin

      Sensor1OutTop := FFCoordRect.Top + (FFCoordRect.Bottom - FFCoordRect.Top) div 8 - 3;
      Sensor1OutBottom := FFCoordRect.Top + (FFCoordRect.Bottom - FFCoordRect.Top) div 8 + 3;
      Sensor1OutLineStep := (FFCoordRect.Bottom - FFCoordRect.Top) div 4;

      LastSensor1DrawX[rLeft] := Max(StartX, StartX - DisToScr(StartDisCoord));
      LastSensor1DrawX[rRight] := Max(StartX, StartX - DisToScr(StartDisCoord));

      LeftSensorEx[rLeft] := False;
      LeftSensorEx[rRight] := False;

      for I := StartIdx - 1 downto 0 do // Ищем первое событие датчика металла от начальной точки вывода к началу файла
      begin
        FDatSrc.GetEventData(I, ID, pData);
        if (ID = EID_Sensor1) then
        begin
          Sensor1Rail := pedSensor1(pData)^.Rail;
          if not LeftSensorEx[Sensor1Rail] then
          begin
            LeftSensor[Sensor1Rail] := pedSensor1(pData)^.State;
            LeftSensorEx[Sensor1Rail] := True;
          end;
          if (LeftSensorEx[rLeft]) and (LeftSensorEx[rRight]) then
            Break;
        end;
      end;

      if not LeftSensorEx[rLeft] then
        LeftSensor[rLeft] := FDatSrc.StartFileSensor1[rLeft];
      if not LeftSensorEx[rRight] then
        LeftSensor[rRight] := FDatSrc.StartFileSensor1[rRight];

      for I := StartIdx to EndIdx do
      begin
        DrawX := StartX + DisToScr(FDatSrc.Event[I].DisCoord - StartDisCoord);
        if Reduction <> 0 then
          DrawX := DrawX + Round(405 / (Zoom / 100));

        if (DrawX >= StartX) and (DrawX <= EndX) then
        begin
          FDatSrc.GetEventData(I, ID, pData);

          if ID = EID_Sensor1 then
          begin
            Sensor1Rail := pedSensor1(pData)^.Rail;

            if MetallSensorValue0 then
            begin
              Color := clRed;
              if LastSensor1DrawX[Sensor1Rail] = DrawX then DrawX:= DrawX + 2;
              if not LeftSensor[Sensor1Rail] then
                FillRect(LastSensor1DrawX[Sensor1Rail], Sensor1OutTop + Sensor1OutLineStep * Ord(Sensor1Rail), DrawX, Sensor1OutBottom + Sensor1OutLineStep * Ord(Sensor1Rail), Color);
            end;

            if MetallSensorValue1 then
            begin
              Color := clGreen;
              if LeftSensor[Sensor1Rail] then
                FillRect(LastSensor1DrawX[Sensor1Rail], Sensor1OutTop + Sensor1OutLineStep * Ord(Sensor1Rail), DrawX, Sensor1OutBottom + Sensor1OutLineStep * Ord(Sensor1Rail), Color);
            end;
            LastSensor1DrawX[Sensor1Rail] := DrawX;
            LeftSensor[Sensor1Rail] := pedSensor1(pData)^.State;
          end;
        end;
      end;

      // --------- Окончание отрисовки данных датчика металла ------------------------

      for R := rLeft to rRight do
      begin
        if MetallSensorValue0 then
        begin
          Color := clRed;
          if LastSensor1DrawX[Sensor1Rail] = DrawX then DrawX:= DrawX + 2;
          if FDatSrc.Sensor1DataExists[R] and (not LeftSensor[R]) then
            FillRect(LastSensor1DrawX[R], Sensor1OutTop + Sensor1OutLineStep * Ord(R), Min(EndX, StartX + DisToScr(FDatSrc.MaxDisCoord - StartDisCoord)), Sensor1OutBottom + Sensor1OutLineStep * Ord(R), Color);
        end;
        if MetallSensorValue1 then
        begin
          Color := clGreen;
          if FDatSrc.Sensor1DataExists[R] and (LeftSensor[R]) then
            FillRect(LastSensor1DrawX[R], Sensor1OutTop + Sensor1OutLineStep * Ord(R), Min(EndX, StartX + DisToScr(FDatSrc.MaxDisCoord - StartDisCoord)), Sensor1OutBottom + Sensor1OutLineStep * Ord(R), Color);
        end;
      end;
    end;

  /// --------------------------------------------

  for I := StartIdx to EndIdx do
  begin
    DrawX := StartX + DisToScr(FDatSrc.Event[I].DisCoord - StartDisCoord);

    if (DrawX > EndX + 100) then
      Break;

    if (DrawX >= StartX) and (DrawX <= EndX) then
    begin
      FDatSrc.GetEventData(I, ID, pData);

      // ---- Вывод текстового сообщения / отметки, вертикальная линия + текст -----

      S := '';
      UseData := False;

      case ID of
        EID_Stolb:
          with pCoordPost(pData)^ do
          begin
            TextTop := 7;
            if Km[0] = Km[1] then S := Format(LangTable.Caption['Common:pk'] + ' %d/%d (' + LangTable.Caption['Common:km'] + ' %d)', [Pk[0], Pk[1], Km[0]])
                             else S := Format(LangTable.Caption['Common:km'] + ' %d/%d ', [Km[0], Km[1]]);
            UseData := True;
          end;
        EID_StolbChainage:
          with pedCoordPostChainage(pData)^ do
          begin
            TextTop := 7;
            S := CaCrdToStr(FDatSrc.CoordSys, Val);
            UseData := True;
          end;
        EID_Switch:
          with pedSwitch(pData)^ do
          begin
            TextTop := 7;
            S := LangTable.Caption['Common:Pt'] + ': ' + GetString(Len, Text);
            UseData := True;
          end;
        EID_DefLabel:
          with pedDefLabel(pData)^ do
          begin
            if Rail = rLeft then
            begin
              TextTop := 7;
              S := LangTable.Caption['Common:Def.code'] + ' (' + LangTable.Caption['Common:Left'] + '): ';
            end
            else
            begin
              S := LangTable.Caption['Common:Def.code'] + ' (' + LangTable.Caption['Common:Right'] + '): ';
              TextTop := BoundsRect_[rRight].Top + 7;
            end;
            S := S + GetString(Len, Text);
            UseData := True;
          end;
        EID_TextLabel:
          with pedTextLabel(pData)^ do
          begin
            TextTop := 7;
            S := GetString(Len, Text);
            UseData := True;
          end;
        EID_ChangeOperatorName:
          with pedTextLabel(pData)^ do
          begin
            TextTop := 7;
            S := 'Оператор: ' + GetString(Len, Text);
            UseData := True;
          end;

        EID_OperatorRemindLabel:
          with pedOperatorRemindLabel(pData)^ do
          begin

            case LabelType of // Тип отметки:           //NoTranslate
              0: S:= 'Уведомление: код ' + GetString(Len, Text);
              1: S:= 'Дефект: ' + GetString(Len, Text) + Format(' L%d мм, H%d мм ', [Length, Depth]);
              2: S:= 'ОДР: ' + GetString(Len, Text) + Format(' L%d мм, H%d мм ', [Length, Depth]);
              3: S:= 'Подозрение на дефект: ' + GetString(Len, Text) + Format(' L%d мм, H%d мм ', [Length, Depth]);
            end;

            if Rail = rLeft then
            begin
              S := S + ' (' + LangTable.Caption['Common:Left'] + ')';
            end
            else
            begin
              S := S + ' (' + LangTable.Caption['Common:Right'] + ')';
//              TextTop := BoundsRect_[rRight].Top + 7;
            end;
            TextTop := 7;
            UseData := True;
          end;

        EID_SmallDate:
          with pedSmallDate(pData)^ do

          if DataType = 1 then
          begin
            S:= 'Необходимо перенастроить каналы контроля!';
            TextTop := 7;
            UseData := True;
          end;

        EID_EndFile:
          begin
            TextTop := 7;
            S := LangTable.Caption['EventsName:FileEnd'];
            UseData := True;
          end;
      end;

      // TmpFlg := False;
      if UseData then
      begin

        if FViewLabels
        {
          FViewLabels and (ID in [EID_Stolb,
          EID_StolbChainage,
          EID_Switch,
          EID_DefLabel,
          EID_TextLabel,
          EID_EndFile
          { EID_LongLabel,
          EID_AirBrushJob,
          EID_SezdNo,
          EID_StBoltStyk,
          EID_EndLineLabel,
          EID_EndBoltStyk,
          EID_StSwarStyk,
          EID_Time,
          EID_EndSwarStyk,
          EID_StLineLabel  ]) } then
        begin
          for R := rLeft to rRight do
            for Ch := 0 to High(FBScanLines) do
              with FBScanLines[Ch] do
                FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);

          Rt_ := Rect(DrawX - TextWidth(1, S) div 2, TextTop, DrawX + TextWidth(1, S) div 2 + 1, TextTop + TextHeight(1, S) + 5);
          if ID <> EID_DefLabel then FillRect_(Rt_, FDispCols.Event)
                                else FillRect_(Rt_, FDispCols.Defect);
          DrawRect(Rt_, 1, FDispCols.Border);
          TextOut(1 + Ord(ID = EID_DefLabel), DrawX, TextTop + 3, 5, S, True);
          // TmpFlg := True;
        end
        else
        begin

          SetLength(InfoRect, Length(InfoRect) + 1);
          with InfoRect[ High(InfoRect)] do
          begin
            CanGroup:= True;
            EventIdx := I;
            EventID := ID;
            YYY := FFCoordRect.Top + (FFCoordRect.Bottom - FFCoordRect.Top) div 4;
            Rt := Rect(DrawX - GetPicSize(piHandScan).cx div 2 - 1, YYY - GetPicSize(piHandScan).cy div 2 - 1, DrawX + GetPicSize(piHandScan).cx div 2 + 1, YYY + GetPicSize(piHandScan).cy div 2 + 1);

            if (ID <> EID_QualityCalibrationRec) and
               (ID <> EID_SensFail) then DrawPic(DrawX - GetPicSize(piInfo).cx div 2, YYY - GetPicSize(piHandScan).cy div 2, piInfo);

            if (ID = EID_QualityCalibrationRec) or
               (ID = EID_SensFail) or
               (ID = EID_Media) then FillChar(FScreenFusy[Max(0, DrawX - GetPicSize(piInfo).cx)], 2 * GetPicSize(piInfo).cx * SizeOf(Boolean), $FF);
            Typ := rtInfo;
            Tag_ := I;
            Text := S;
          end;
        end;
      end;

      // ---- Вывод иконки с всплывающей (при выборе мышью) текстовой информацией -----

      if ((ID = EID_EndFile) or
          (ID = EID_AirBrushTempOff) or
          (ID = EID_ZerroProbMode) or
          (ID = EID_EndFile) or
          (ID = EID_SetRailType) or
          (ID = EID_StBoltStyk) or
          (ID = EID_EndBoltStyk) or
//        (ID = EID_GPSCoord) or
//        (ID = EID_GPSState) or
          (ID = EID_Mode) or
          (ID = EID_AirBrushTempOff) or
          (ID = EID_ZerroProbMode) or
          (ID = EID_HeadPh)
        { or (ID = EID_Ku) or
           (ID = EID_Att) or
           (ID = EID_PrismDelay) or
           (ID = EID_TVG) or
           (ID = EID_StStr) or
           (ID = EID_EndStr) or
           (ID = EID_ChangeOperatorName) or
           (ID = EID_QualityCalibrationRec) or
           (ID = EID_SensFail) }) or MainForm.tbAllEventsOnLine.Checked then

      begin
        Flg := (FDatSrc.Event[I].DisCoord > 0) or Show0CoordEvent or (not (ID in [EID_Ku, EID_Att, EID_TVG, EID_StStr, EID_EndStr, EID_HeadPh, EID_PrismDelay]));
        if Flg then
        begin
          SetLength(InfoRect, Length(InfoRect) + 1);
          with InfoRect[ High(InfoRect)] do
          begin
            CanGroup:= True;
            EventIdx := I;
            EventID := ID;
            YYY := FFCoordRect.Top + (FFCoordRect.Bottom - FFCoordRect.Top) div 4;
            Rt := Rect(DrawX - GetPicSize(piHandScan).cx div 2 - 1, YYY - GetPicSize(piHandScan).cy div 2 - 1, DrawX + GetPicSize(piHandScan).cx div 2 + 1, YYY + GetPicSize(piHandScan).cy div 2 + 1);

            if (ID <> EID_QualityCalibrationRec) and
               (ID <> EID_SensFail) then DrawPic(DrawX - GetPicSize(piInfo).cx div 2, YYY - GetPicSize(piHandScan).cy div 2, piInfo);
//                                    else DrawPic(DrawX - GetPicSize(piInfo).cx div 2, YYY + GetPicSize(piHandScan).cy div 2, piFail);

            if (ID = EID_QualityCalibrationRec) or
               (ID = EID_SensFail) or
               (ID = EID_Media) then FillChar(FScreenFusy[Max(0, DrawX - GetPicSize(piInfo).cx)], 2 * GetPicSize(piInfo).cx * SizeOf(Boolean), $FF);

            Typ := rtInfo;
            Tag_ := I;
            case ID of
              EID_SetRailType: Text := LangTable.Caption['EventsName:RailTypeSelection'];
              EID_StBoltStyk:  Text := LangTable.Caption['EventsName:StartBoltjoint'];
              EID_EndBoltStyk: Text := LangTable.Caption['EventsName:EndBoltjoint'];
              // EID_Time:        Text := LangTable.Caption['EventsName:Time'];
              EID_EndFile:     Text := LangTable.Caption['EventsName:FileEnd'];
              EID_GPSCoord:    Text := Format(LangTable.Caption['EventsName:GPSCoord'], [GPSSingleToText(pedGPSCoord(pData)^.Lat, True), GPSSingleToText(pedGPSCoord(pData)^.Lon, False)]);
              EID_GPSState:    Text := LangTable.Caption['EventsName:GPSState'];
              // Состояние приемника GPS
              EID_Mode: begin
                          CanGroup:= False;
                          if FDatSrc.isA31 or FDatSrc.isA15New then
                            TimeName := Format('%d сек', [FDatSrc.Params[I].Time]);  // NoTranslate

                          if pedMode(pData)^.ModeIdx in [4 .. 7, 12]
                             then Text := LangTable.Caption['EventsName:Mode' + IntToStr(pedMode(pData)^.ModeIdx)] + ' ' + TimeName
                             else if pedMode(pData)^.ModeIdx in [0, 2] then // Сплошные каналы
                             begin
                               RailText_ := RailToStr(FDatSrc.RailToPathRail(pedMode(pData)^.Rail), 0);
                               ModeText := LangTable.Caption['EventsName:Mode' + IntToStr(pedMode(pData)^.ModeIdx)];
                               ChName := LangTable.Caption[FDatSrc.Config.EvalChannelByNum[pedMode(pData)^.EvalChNum].ShortName];
                             //  TimeName := Format('%d сек', [pedMode(pData)^.Time]);
                              { if pedMode(pData)^.Channel
                                 then } Text := Format(ModeText + ' (' + LangTable.Caption['Common:Rail'] + ': %s, ' + LangTable.Caption['Common:Channel'] + ': %s) ', [RailText_, ChName]) + TimeName;
                               //  else Text := ModeText;
                             end
                             else if pedMode(pData)^.ModeIdx in [1, 3] then // Ручные каналы
                             begin
                               RailText_:= LangTable.Caption['Common:Rail'] + ': ' + RailToStr(FDatSrc.RailToPathRail(pedMode(pData)^.Rail), 0);
                               ModeText := LangTable.Caption['EventsName:Mode' + IntToStr(pedMode(pData)^.ModeIdx)];
                               ChName := LangTable.Caption[FDatSrc.Config.HandChannelByNum[pedMode(pData)^.EvalChNum].ShortName];


                             {  if pedMode(pData)^.Channel
                                 then} Text := Format(ModeText + ' (' {+ RailText_ + ', '} + LangTable.Caption['Common:Channel'] + ': %s - %s)', [ChName, TimeName]);
                              //   else Text := ModeText;
                             end;
                        end;
              EID_AirBrushTempOff:  begin
                                      Text := LangTable.Caption['EventsName:PaintSystemState'] + ' ';
                                      if pedAirBrushTempOff(pData)^.State = 1
                                       then Text := Text + LangTable.Caption['Common:State0'] // Выкл
                                       else Text := Text + LangTable.Caption['Common:State1']; // Вкл
                                    end;
              EID_ZerroProbMode:    begin
                                      case TZerroProbMode(pedZerroProbMode(pData)^.Mode) of
                                        zpmBoth:  Text := LangTable.Caption['EventsName:WorkWPZerroProbe_Both']; // Работа 0° - Оба;
                                        zpmOnlyA: Text := LangTable.Caption['EventsName:WorkWPZerroProbe_OnlyA']; // Работа 0° - КПА;
                                        zpmOnlyB: Text := LangTable.Caption['EventsName:WorkWPZerroProbe_OnlyB']; // Работа 0° - КПВ;
                                      end;
                                    end;

  {              EID_QualityCalibrationRec:
                                    with pedQualityCalibrationRec(pData)^ do
                                    begin
                                      Text := 'Качество настройки каналов контроля - ';
                                      if RecType = 0 then Text := Text + 'Нет стыка'
                                      else
                                        case RecType of
                                          1: Text := Text + 'Неуд. по БС ';
                                          2: Text := Text + 'Неуд. по АТС ';
                                        end;
                                      Text := Text + RailToStr(FDatSrc.RailToPathRail(pedMode(pData)^.Rail), 0) + ' ';
                                      for L := 0 to Len - 1 do
                                        Text := Text + LangTable.Caption[FDatSrc.Config.EvalChannelByNum[Data[L]].ShortName].ShortName (*EvalChNumToName(Data[L])*) (*CID_GateIndex_To_Name(Data[L].CID, Data[L].GateIndex)* ) + ', ';
                                      Delete(Text, Length(Text) - 2, 2);
                                    end; }
            else
              begin
                RailText := Format(' (' + LangTable.Caption['Common:Rail'] + ': %s, ' + LangTable.Caption['Common:Channel'] + ': %s)', [RailToStr(FDatSrc.RailToPathRail(pedEvalChByteParam(pData)^.Rail), 0), LangTable.Caption[FDatSrc.Config.EvalChannelByNum[pedEvalChByteParam(pData)^.EvalCh].ShortName]]);

                case ID of
                                           0: Text:= 'Сигналы В-развертки';
                  EID_HandScan              : Text:= '[0x82] - Ручник';
                  EID_Ku                    : Text:= '[0x90] - Изменение условной чувствительности';
                  EID_Att                   : Text:= '[0x91] - Изменение аттенюатора';
                  EID_TVG                   : Text:= '[0x92] - Изменение ВРЧ';
                  EID_StStr                 : Text:= '[0x93] - Изменение положения начала строба';
                  EID_EndStr                : Text:= '[0x94] - Изменение положения конца строба';
                  EID_HeadPh                : Text:= '[0x95] - Список включенных наушников';
                  EID_Mode                  : Text:= '[0x96] - Изменение режима';
                  EID_SetRailType           : Text:= '[0x9B] - Настройка на тип рельса';
                  EID_PrismDelay            : Text:= '[0x9C] - Изменение 2Тп (word)';
                  EID_Stolb                 : Text:= '[0xA0] - Отметка координаты';
                  EID_Switch                : Text:= '[0xA1] - Номер стрелочного перевода';
                  EID_DefLabel              : Text:= '[0xA2] - Отметка дефекта';
                  EID_TextLabel             : Text:= '[0xA3] - Текстовая отметка';
                  EID_StBoltStyk            : Text:= '[0xA4] - Нажатие кнопки БС';
                  EID_EndBoltStyk           : Text:= '[0xA5] - Отпускание кнопки БС';
                  EID_Time                  : Text:= '[0xA6] - Отметка времени';
                  EID_StolbChainage         : Text:= '[0xA7] - Отметка координаты Chainage';
                  EID_ZerroProbMode         : Text:= '[0xA8] - Режим работы датчиков 0 град';
                  EID_LongLabel             : Text:= '[0xA9] - Протяженная отметка';
                  EID_SpeedState            : Text:= '[0xAA] - Скорость и Превышение скорости контроля';
                  EID_ChangeOperatorName    : Text:= '[0xAB] - Смена оператора (ФИО полностью)';
                  EID_AutomaticSearchRes    : Text:= '[0xAC] - «Значимые» участки рельсов, получаемые при автоматическом поиске';
                  EID_TestRecordFile        : Text:= '[0xAD] - Файл записи контрольного тупика';
                  EID_OperatorRemindLabel   : Text:= '[0xAE] - Отметки пути, заранее записанные в прибор для напоминания оператору (дефектные рельсы и другие)';
                  EID_QualityCalibrationRec : Text:= '[0xAF] - Качество настройки каналов контроля';
                  EID_Sensor1               : Text:= '[0xB0] - Сигнал датчика БС и Стрелки';
                  EID_AirBrush              : Text:= '[0xB1] - Краскоотметчик';
                  EID_PaintMarkRes          : Text:= '[0xB2] - Сообщение о выполнении задания на краско-отметку';
                  EID_AirBrushTempOff       : Text:= '[0xB3] - Временное отключение Краскоотметчика';
                  EID_AirBrush2             : Text:= '[0xB4] - Краскоотметчик с отладочной информацией';
                  EID_AlarmTempOff          : Text:= '[0xB5] - Временное отключение АСД по всем каналам';
                  EID_StartSwitchShunter    : Text:= '[0xB6] - Начало зоны стрелочного перевода';
                  EID_EndSwitchShunter      : Text:= '[0xB7] - Конец зоны стрелочного перевода';
                  EID_Temperature           : Text:= '[0xB8] - Температура';
                  EID_DebugData             : Text:= '[0xB9] - Отладочная информация';
                  EID_PaintSystemParams     : Text:= '[0xBA] - Параметры работы алгоритма краскопульта';
                  EID_UMUPaintJob           : Text:= '[0xBB] - Задание БУМ на краскоотметку';
                  EID_ACData                : Text:= '[0xBC] - Данные акустического контакта';
                  EID_SmallDate             : Text:= '[0xBD] - Малое универсальное событие';
                  EID_RailHeadScaner        : Text:= '[0xBE] - Данные уточняющего сканера головки рельса';
                  EID_SensAllowedRanges     : Text:= '[0xBF] - Таблица разрешенных диапазонов Ку';
                  EID_GPSCoord              : Text:= '[0xC0] - Географическая координата';
                  EID_GPSState              : Text:= '[0xC1] - Состояние приемника GPS';
                  EID_Media                 : Text:= '[0xC2] - Медиаданные';
                  EID_GPSCoord2             : Text:= '[0xC3] - Географическая координата со скоростью';
                  EID_NORDCO_Rec            : Text:= '[0xC4] - Запись NORDCO';
                  EID_SensFail              : Text:= '[0xC5] - Отклонение условной чувствительности от нормативного значения';
                  EID_AScanFrame            : Text:= '[0xC6] - Кадр А-развертки';
                  EID_MediumDate            : Text:= '[0xC7] - Среднее универсальное событие';
                  EID_BigDate               : Text:= '[0xC8] - Большое универсальное событие';
                  EID_CheckSum              : Text:= '[0xF0] - Контрольная сумма';
                  EID_SysCrd_SS             : Text:= '[0xF1] - Системная координата короткая';
                  EID_SysCrd_SF             : Text:= '[0xF4] - Полная системная координата с короткой ссылкой';
                  EID_SysCrd_FS             : Text:= '[0xF9] - Системная координата короткая';
                  EID_SysCrd_FF             : Text:= '[0xFC] - Полная системная координата с полной ссылкой';
                  EID_SysCrd_NS             : Text:= '[0xF2] - Системная координата новая, короткая';
                  EID_SysCrd_NF             : Text:= '[0xF3] - Системная координата новая, полная';
                  EID_SysCrd_UMUA_Left_NF   : Text:= '[0xF5] - Полная системная координата без ссылки БУМ A, левая сторона';
                  EID_SysCrd_UMUB_Left_NF   : Text:= '[0xF6] - Полная системная координата без ссылки БУМ В, левая сторона';
                  EID_SysCrd_UMUA_Right_NF  : Text:= '[0xF7] - Полная системная координата без ссылки БУМ A, правая сторона';
                  EID_SysCrd_UMUB_Right_NF  : Text:= '[0xF8] - Полная системная координата без ссылки БУМ В, правая сторона';
                  EID_EndFile               : Text:= '[0xFF] - Конец файла';
                end;
                if (ID = EID_PrismDelay) then
                begin
                  Text := LangTable.Caption['EventsName:Changeprobedelay'] + ': ' + IntToStr(pedEvalChWordParam(pData)^.Value) + LangTable.Caption['Common:us'] + ' (' + RailText + ')';
                end
                else
                  case ID of
                    EID_HeadPh:   Text := LangTable.Caption['EventsName:ChangeHeadphones'] + ': ' + LangTable.Caption['Common:State' + IntToStr(pedEvalChByteParam(pData)^.Value)] + ' ' + RailText;
                 //   EID_Ku:       Text := LangTable.Caption['EventsName:ChangeSensitivity'] + ': ' + IntToStr(pedEvalChByteParam(pData)^.Value) + ' ' + LangTable.Caption['Common:dB'] + RailText;
                 //   EID_Att:      Text := LangTable.Caption['EventsName:ChangeAttenuator'] + ': ' + IntToStr(pedEvalChByteParam(pData)^.Value) + ' ' + LangTable.Caption['Common:dB'] + RailText;
                 //   EID_TVG:      Text := LangTable.Caption['EventsName:ChangeTVG'] + ': ' + IntToStr(pedEvalChByteParam(pData)^.Value) + RailText;
                 //   EID_StStr:    Text := LangTable.Caption['EventsName:Changestrobestart'] + ': ' + IntToStr(pedEvalChByteParam(pData)^.Value) + ' ' + LangTable.Caption['Common:us'] + RailText;
                 //   EID_EndStr:   Text := LangTable.Caption['EventsName:Changestrobeend'] + ': ' + IntToStr(pedEvalChByteParam(pData)^.Value) + ' ' + LangTable.Caption['Common:us'] + RailText;
                 //   EID_SensFail: Text := LangTable.Caption['EventsName:SensFail'] + ': ' + IntToStr(pedSensFail(pData)^.DeltaKu) + ' ' + LangTable.Caption['Common:db'] + RailText;
                  end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;

  // Важные сообщения вывод сверху

  /// --------------------------------------------

  for I := StartIdx to EndIdx do
  begin
    DrawX := StartX + DisToScr(FDatSrc.Event[I].DisCoord - StartDisCoord);

    if (DrawX > EndX + 100) then  Break;

    if (DrawX >= StartX) and (DrawX <= EndX) then
    begin
      FDatSrc.GetEventData(I, ID, pData);

      // ---- Вывод иконки с всплывающей (при выборе мышью) текстовой информацией -----

      if (ID = EID_QualityCalibrationRec) or ((ID = EID_SensFail) and (pedSensFail(pData)^.DeltaKu <> 0)) then
      begin
        Flg := (FDatSrc.Event[I].DisCoord > 0) or Show0CoordEvent;
        if Flg then
        begin
          SetLength(InfoRect, Length(InfoRect) + 1);
          with InfoRect[ High(InfoRect)] do
          begin
            CanGroup:= True;
            EventIdx := I;
            EventID := ID;
            YYY := FFCoordRect.Top + 3 * (FFCoordRect.Bottom - FFCoordRect.Top) div 4;
            Rt := Rect(DrawX - GetPicSize(piHandScan).cx div 2 - 1, YYY - GetPicSize(piHandScan).cy div 2 - 1, DrawX + GetPicSize(piHandScan).cx div 2 + 1, YYY + GetPicSize(piHandScan).cy div 2 + 1);

           {
            if (ID <> EID_QualityCalibrationRec) and
               (ID <> EID_SensFail) then DrawPic(DrawX - GetPicSize(piInfo).cx div 2, YYY - GetPicSize(piHandScan).cy div 2, piInfo)
                                    else
          }

            FillChar(FScreenFusy[Max(0, DrawX - GetPicSize(piInfo).cx)], 2 * GetPicSize(piInfo).cx * SizeOf(Boolean), $FF);
            Typ := rtInfo;
            Tag_ := I;
            case ID of

              EID_QualityCalibrationRec:
                                    with pedQualityCalibrationRec(pData)^ do
                                    begin
                                      CanGroup:= False;
                                      Text := 'Качество настройки каналов контроля - ';  //NO_LANGUAGE
                                      if RecType = 0 then Text := Text + 'Нет стыка'                                    //NO_LANGUAGE
                                        else
                                        case RecType of
                                          0: Text := Text + 'Нет стыка ';                                         //NO_LANGUAGE
                                          1: Text := Text + 'Неуд. по БС ';                                         //NO_LANGUAGE
                                          2: Text := Text + 'Неуд. по АЛТС ';                                        //NO_LANGUAGE
                                          3: Text := Text + 'Удовл. по БС ';                                         //NO_LANGUAGE
                                          4: Text := Text + 'Удовл. по АЛТС ';                                        //NO_LANGUAGE
                                          5: Text := Text + 'АЛТС';                                        //NO_LANGUAGE
                                        end;
                                      Text := Text + ' ' + RailToStr(FDatSrc.RailToPathRail(Rail), 0) + ' ';
                                      for L := 0 to Len - 1 do
                                        Text := Text + LangTable.Caption[FDatSrc.Config.EvalChannelByNum[Data[L]].ShortName] {EvalChNumToName(Data[L])} {CID_GateIndex_To_Name(Data[L].CID, Data[L].GateIndex)} + ', ';
//                                        Text := Text + EvalChNumToName(Data[L]) {CID_GateIndex_To_Name(Data[L].CID, Data[L].GateIndex)} + ', ';
//                                        Text := Text + CID_GateIndex_To_Name(Data[L]., Data[L].GateIndex) + ', ';
                                      if Len <> 0 then Delete(Text, Length(Text) - 1, 2);

                                      if RecType in [1..2] then DrawPic(DrawX - GetPicSize(piInfo).cx div 2 + 2, YYY - GetPicSize(piHandScan).cy div 2 + 2, piFail)
                                                           else DrawPic(DrawX - GetPicSize(piInfo).cx div 2 + 2, YYY - GetPicSize(piHandScan).cy div 2 + 2, piOK);

                                    end;
            else
              begin
                DrawPic(DrawX - GetPicSize(piInfo).cx div 2 + 2, YYY - GetPicSize(piHandScan).cy div 2 + 2, piFail);
                RailText := Format(' (' + LangTable.Caption['Common:Rail'] + ': %s, ' + LangTable.Caption['Common:Channel'] + ': %s)', [RailToStr(FDatSrc.RailToPathRail(pedEvalChByteParam(pData)^.Rail), 0), LangTable.Caption[FDatSrc.Config.EvalChannelByNum[pedEvalChByteParam(pData)^.EvalCh].ShortName]]);

               { if (ID = EID_PrismDelay) then
                begin
                  Text := LangTable.Caption['EventsName:Changeprobedelay'] + ': ' + IntToStr(pedEvalChWordParam(pData)^.Value) + LangTable.Caption['Common:us'] + ' (' + RailText + ')';
                end
                else   }
                  case ID of
                {    EID_HeadPh:   Text := LangTable.Caption['EventsName:ChangeHeadphones'] + ': ' + LangTable.Caption['Common:State' + IntToStr(pedEvalChByteParam(pData)^.Value)] + ' ' + RailText;
                    EID_Ku:       Text := LangTable.Caption['EventsName:ChangeSensitivity'] + ': ' + IntToStr(pedEvalChByteParam(pData)^.Value) + ' ' + LangTable.Caption['Common:dB'] + RailText;
                    EID_Att:      Text := LangTable.Caption['EventsName:ChangeAttenuator'] + ': ' + IntToStr(pedEvalChByteParam(pData)^.Value) + ' ' + LangTable.Caption['Common:dB'] + RailText;
                    EID_TVG:      Text := LangTable.Caption['EventsName:ChangeTVG'] + ': ' + IntToStr(pedEvalChByteParam(pData)^.Value) + RailText;
                    EID_StStr:    Text := LangTable.Caption['EventsName:Changestrobestart'] + ': ' + IntToStr(pedEvalChByteParam(pData)^.Value) + ' ' + LangTable.Caption['Common:us'] + RailText;
                    EID_EndStr:   Text := LangTable.Caption['EventsName:Changestrobeend'] + ': ' + IntToStr(pedEvalChByteParam(pData)^.Value) + ' ' + LangTable.Caption['Common:us'] + RailText; }
                    EID_SensFail: begin
                                    CanGroup:= False;
                                    if pedSensFail(pData)^.DeltaKu <> 0
                                       then Text := LangTable.Caption['EventsName:SensFail'] + ': ' + IntToStr(pedSensFail(pData)^.DeltaKu) + ' ' + LangTable.Caption['Common:db'] + RailText;
//                                    else Text := LangTable.Caption['EventsName:SensNotFail'] + ' ' + RailText;
                                  end;
                  end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;

  // Текст от сюда перенесен в конец файла

  YYY := FFCoordRect.Top + (FFCoordRect.Bottom - FFCoordRect.Top) div 4;
  for I := 0 to FDatSrc.AlarmCount - 1 do
  // if not Boolean(FDatSrc.FAirBrushList_[I].Y) then
  begin

    { if GetSkipBM then }
    DrawX := StartX + DisToScr(FDatSrc.Alarm[I].DisCoord - StartDisCoord);
    // else DrawX:= StartX + DisToScr(FDatSrc.DisToSysCoord(FDatSrc.AirBrush[I].DisCoord) - StartDisCoord);

    DrawPic(DrawX - GetPicSize(piRecImage).cx div 2, YYY - GetPicSize(piHandScan).cy div 2, piRecImage);

    if (DrawX >= StartX) and (DrawX <= EndX) then
    begin
      SetLength(InfoRect, Length(InfoRect) + 1);
      with InfoRect[ High(InfoRect)] do
      begin
        CanGroup:= False;
        Rt := Rect(DrawX - GetPicSize(piHandScan).cx div 2 - 1, YYY
          { (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } - GetPicSize(piHandScan).cy div 2 - 1, DrawX + GetPicSize(piHandScan).cx div 2 + 1, YYY
          { (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } + GetPicSize(piHandScan).cy div 2 + 1);
        Text := Format('%s - DS: %d; Ch: %d', [LangTable.Caption['Common:AirBrushAlarm'], FDatSrc.Alarm[I].DisCoord, FDatSrc.Alarm[I].EvalCh]);

        // for M := 0 to FDatSrc.AirBrush[I].Count - 1 do
        // Text := Text + Format('%s%d. %s - ∆L %d mm', [Chr($0D) + Chr($0A), M + 1, EvalChNumToName(FDatSrc.AirBrush[I].Items[M].ScanCh), (FDatSrc.AirBrush[I].Items[M].EndDisCoord - FDatSrc.AirBrush[I].Items[M].StartDisCoord) * FDatSrc.Header.ScanStep div 100]);

        Tag_ := I;
        Typ := rtAlarm;
        // Rt := Rect(DrawX - GetPicSize(piNotebook).cx div 2 - 1, YYY
        // { + (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } - GetPicSize(piNotebook).cy div 2 - 1, DrawX + GetPicSize(piNotebook).cx div 2 + 1, YYY
        // { + (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } + GetPicSize(piNotebook).cy div 2 + 1);

        if FViewLabels then
          for Ch := 0 to High(FBScanLines) do
            with FBScanLines[Ch] do
              if Rail = FDatSrc.Alarm[I].Rail then
                FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);
      end;

      if not FDM.RailView then
      begin
        Self.SetClipRect(BoundsRect);
        for II := 0 to High(FDrawAlarmBrushIdx) do
          if I = FDrawAlarmBrushIdx[II] then
          begin

            // Отрисовка координаты на которой было выдано задание для БУМ на краскоотметку
            { if MainForm.PaintSystemDebugMode then
              begin
              DrawX := StartX + DisToScr(FDatSrc.FAirBrushList_[I].X  - MMToSysCrd(FDatSrc.AirBrushPos - FDatSrc.PaintSystemParams.AirBrush_Shift) - StartDisCoord);
              R := TRail(FDatSrc.AirBrush[I].Rail);
              for Ch := 0 to High(FBScanLines) do
              with FBScanLines[Ch] do
              if Rail = TRail(FDatSrc.AirBrush[I].Rail) then
              begin
              FillRect(DrawX - 1, BScanRt.Top, DrawX - 1, BScanRt.Bottom + 1, clFuchsia);
              FillRect(DrawX + 1, BScanRt.Top, DrawX + 1, BScanRt.Bottom + 1, clFuchsia);
              end;
              end; }
            {
              for J := 0 to FDatSrc.AirBrush[I].Count - 1 do
              begin
              Line := FScanChNumToBScanLine[FDatSrc.AirBrush[I].Rail, FDatSrc.AirBrush[I].Items[J].ScanCh];

              ChNum := FDatSrc.AirBrush[I].Items[J].ScanCh; }

            X1 := StartX + DisToScr(FDatSrc.Alarm[I].StDisCrd - StartDisCoord);
            X2 := StartX + DisToScr(FDatSrc.Alarm[I].EdDisCrd - StartDisCoord);

            // if (X1 > EndX) then Break;
            // if (X2 < StartX) then Continue;

            Line := FScanChNumToBScanLine[FDatSrc.Alarm[I].Rail, FDatSrc.Alarm[I].EvalCh];
            if Line <> -1 then
              with FBScanLines[Line] do
              begin
                SetClipRect(BScanRt);
                if Odd(FDatSrc.Alarm[I].EvalCh)
                  then Self.DrawRect(Rect(X1, BScanRt.Bottom - 5, X2, BScanRt.Bottom - 3 * (BScanRt.Bottom - BScanRt.Top) div 4), 1, EvalChNumColors2[FDatSrc.Config.ChannelIDList[FDatSrc.Alarm[I].Rail, FDatSrc.Alarm[I].EvalCh], 15])
                  else Self.DrawRect(Rect(X1, BScanRt.Top + 5, X2, BScanRt.Top + 3 * (BScanRt.Bottom - BScanRt.Top) div 4), 1, EvalChNumColors2[FDatSrc.Config.ChannelIDList[FDatSrc.Alarm[I].Rail, FDatSrc.Alarm[I].EvalCh], 15])
              end;
            ReSetClipRect();

            {
              if Line <> -1 then
              with FBScanLines[Line] do
              begin
              SetClipRect(BScanRt);
              X1 := FStartScrX + (Min(FDatSrc.AirBrush[I].Items[J].StartDisCoord, FDatSrc.AirBrush[I].Items[J].EndDisCoord) - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
              X2 := FStartScrX + (Max(FDatSrc.AirBrush[I].Items[J].StartDisCoord, FDatSrc.AirBrush[I].Items[J].EndDisCoord) - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;

              case FDM.Reduction of
              0: begin
              DX1 := 0;
              DX2 := 0;
              end;
              1: begin
              DX1 := Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead) + FDatSrc.Config.ScanChannelByNum[ChNum].Delta) / (FDM.Zoom / 100));
              DX2 := Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead) + FDatSrc.Config.ScanChannelByNum[ChNum].Delta) / (FDM.Zoom / 100));
              end;
              2: begin
              if FDatSrc.AirBrush[I].Items[J].StartDelay < FDatSrc.AirBrush[I].Items[J].EndDelay then
              begin
              DX1 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AirBrush[I].Items[J].StartDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
              DX2 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AirBrush[I].Items[J].EndDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
              end
              else
              begin
              DX1 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AirBrush[I].Items[J].EndDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
              DX2 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AirBrush[I].Items[J].StartDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
              end;
              end;
              end;

              Item := FDatSrc.Config.ScanChannelByNum[FDatSrc.AirBrush[I].Items[J].ScanCh];
              with Item do
              begin
              Y1 := BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.AirBrush[I].Items[J].StartDelay / DelayMultiply - BScanGateMin) / BScanDuration);
              Y2 := BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.AirBrush[I].Items[J].EndDelay / DelayMultiply - BScanGateMin) / BScanDuration);
              end;

              if Config.InvertDelayAxis then
              begin
              Y1 := BScanRt.Top + BScanRt.Bottom - Y1;
              Y2 := BScanRt.Top + BScanRt.Bottom - Y2;
              end;

              // Self.TextOut(1, X1, Y1, 5, IntToStr(FDatSrc.AirBrush[I].Items[J].StartDisCoord), True);
              OutRect := Rect(X1 + DX1 - 3, Min(Y1, Y2) - 3, X2 + DX2 + 3, Max(Y1, Y2) + 3);
              if (OutRect.Right <= EndX) and (OutRect.Left >= StartX) then Self.DrawRect(OutRect, 2, 0);

              //                   if { DebugView2 and  (J = 0) then
              //                     // Self.TextOut(0, X1 + DX1 - 3, Min(Y1, Y2) - 3, 1, IntToStr(FDatSrc.AirBrush[I].Items[J].Debug), False);
              //                     Self.TextOut(0, X1 + DX1 - 3, Min(Y1, Y2) - 3, 1, IntToStr(FDatSrc.FAirBrushList_[I]), False);
              end; }
          end;
      end;
      ReSetClipRect();
    end;
  end;

  /// //////////////////////////////////////////////////////////////////////////////////

  { EID_PrismDelay: Text:= LangTable.Caption['EventsName:Changeprobedelay'] + RailText + LangTable.Caption['EventsName:Channel:'] + IntToStr(ByteToCh(ID, pChParam(pData)^.ChannelRail)) + LangTable.Caption['EventsName:Value:'] + IntToStr(pChParam(pData)^.Value) + LangTable.Caption['EventsName:us)'];
    EID_ZondImp: Text:= LangTable.Caption['EventsName:ChangeBang('] + RailText + LangTable.Caption['EventsName:Channel:'] + IntToStr(ByteToCh(ID, pChParam(pData)^.ChannelRail)) + LangTable.Caption['EventsName:Value:'] + IntToStr(pChParam(pData)^.Value) + ')';
    EID_StLineLabel: Text:= LangTable.Caption['EventsName:StartLengthyLabel'];
    EID_EndLineLabel: Text:= LangTable.Caption['EventsName:EndLengthyLabel'];
    EID_StSwarStyk: Text:= LangTable.Caption['EventsName:StartWeldedjoint'];
    EID_EndSwarStyk: Text:= LangTable.Caption['EventsName:EndWeldedjoint'];
    EID_OpChange: Text:= LangTable.Caption['EventsName:ChangeOperator'];
    EID_PathChange: Text:= LangTable.Caption['EventsName:ChangeTracknumber'];
    EID_MovDirChange: Text:= LangTable.Caption['EventsName:ChangeMovingDirection'];
    EID_GPSCoord: Text:= Format('GPS Coord Lat: %f, Lon: %f', [InvertGPSSingle(pGPSCoord(pData)^.Lat) * 180 / pi, InvertGPSSingle(pGPSCoord(pData)^.Lon) * 180 / pi]);
    EID_GPSState: Text:= Format('GPS State Byte1: %d, Byte2: %d', [pGPSState(pData)^[1], pGPSState(pData)^[2]]);
    EID_LongLabel: Text:= 'EID_LongLabel'; }

  // --------- Отрисовка меток Ручного Контроля  ---------------------------------

  YYY := FFCoordRect.Top + (FFCoordRect.Bottom - FFCoordRect.Top) div 4;

  for I := StartIdx to EndIdx do
  begin
    DrawX := StartX + DisToScr(FDatSrc.Event[I].DisCoord - StartDisCoord);

    if (DrawX >= StartX) and (DrawX <= EndX) then
    begin
      FDatSrc.GetEventData(I, ID, pData);

      if (ID in [EID_HandScan, EID_RailHeadScaner]) { or
        ((ID = EID_Mode) and (pChgMode(pData)^ in [4, 60, 61, 62, 63, 64, 65, 66])) } then
      begin
        for R := rLeft to rRight do
          for Ch := 0 to High(FBScanLines) do
            // with FBScanLines[FChIdx[R, Ch]] do
            with FBScanLines[Ch] do
              FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);

        if ID in [EID_HandScan, EID_RailHeadScaner] then
        begin
          SetLength(InfoRect, Length(InfoRect) + 1);
          with InfoRect[ High(InfoRect)] do
          begin
            CanGroup:= False;
            EventIdx := I;
            EventID := ID;
            Rt := Rect(DrawX - GetPicSize(piHandScan).cx div 2 - 1, YYY
              { (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } - GetPicSize(piHandScan).cy div 2 - 1, DrawX + GetPicSize(piHandScan).cx div 2 + 1, YYY
              { (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } + GetPicSize(piHandScan).cy div 2 + 1);
            // AddToScrFusy(Rt.Left, Rt.Right);
            Typ := rtInfo;
            if ID = EID_RailHeadScaner then
            begin
              InfoRect[ High(InfoRect)].Text := LangTable.Caption['EventsName:ManualScan'] + ' (сканер головки рельса)'; //NO_LANGUAGE
              for K := 0 to High(FDatSrc.HeadScanList) do
                if FDatSrc.HeadScanList[K].EventIdx = I then
                begin
                  Tag_ := K;
                  Typ := rtRailHeadScaner;
                  Break;
                end;
            end
            else
            begin
              for K := 0 to High(FDatSrc.HSList) do
                if FDatSrc.HSList[K].EventIdx = I then
                begin
                  Tag_ := K;
                  Typ := rtHandScan;
                  Break;
                end;


              RailText_:= LangTable.Caption['Common:Rail'] + ': ' + RailToStr(FDatSrc.RailToPathRail(FDatSrc.HSList[K].Header.Rail), 0);
              ChName := LangTable.Caption[FDatSrc.Config.HandChannelByNum[FDatSrc.HSList[K].Header.HandChNum].ShortName];
              // NoTranslate
//              if FDatSrc.isA31 then TimeName := Format(LangTable.Caption['HandScan:ScanTime'] + ': %d сек', [Round(Length(FDatSrc.HSList[K].Samples) / 70{50})]) // NoTranslate
{                               else } TimeName := Format(LangTable.Caption['HandScan:ScanTime'] + ': %d сек', [Round(Length(FDatSrc.HSList[K].Samples) / 50)]); // NoTranslate

              InfoRect[ High(InfoRect)].Text := LangTable.Caption['EventsName:ManualScan'] + ' ' + RailText_ + ' ' + ChName + ' ' + TimeName;
            end;

          end;
//          InfoRect[ High(InfoRect)].Text:= '! ' + InfoRect[ High(InfoRect)].Text';
        end;

        // DrawPic(DrawX - GetPicSize(piHandScan).cx div 2, (FFCoordRect.Top + FFCoordRect.Bottom) div 2 - GetPicSize(piHandScan).cy div 2, piHandScan);
        DrawPic(DrawX - GetPicSize(piHandScan).cx div 2, YYY - GetPicSize(piHandScan).cy div 2, piHandScan);

        // AddToScrFusy(DrawX - GetPicSize(piHandScan).cx div 2, DrawX + GetPicSize(piHandScan).cx div 2);
      end;
    end;
  end;


  // --------- Отрисовка МедиаДанных  ---------------------------------

//  YYY := FFCoordRect.Top + (FFCoordRect.Bottom - FFCoordRect.Top) div 4;
  YYY := FFCoordRect.Top + 3 * (FFCoordRect.Bottom - FFCoordRect.Top) div 4;

  for I := StartIdx to EndIdx do
  begin
    DrawX := StartX + DisToScr(FDatSrc.Event[I].DisCoord - StartDisCoord);

    if (DrawX >= StartX) and (DrawX <= EndX) then
    begin
      FDatSrc.GetEventData(I, ID, pData);

      if (ID in [EID_Media]) then
      begin
        for R := rLeft to rRight do
          for Ch := 0 to High(FBScanLines) do
            // with FBScanLines[FChIdx[R, Ch]] do
            with FBScanLines[Ch] do
              FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);

        SetLength(InfoRect, Length(InfoRect) + 1);
        with InfoRect[ High(InfoRect)] do
        begin
          CanGroup:= False;
          Rt := Rect(DrawX - GetPicSize(piRecImage).cx div 2 - 1, YYY
            { (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } - GetPicSize(piRecImage).cy div 2 - 1, DrawX + GetPicSize(piRecImage).cx div 2 + 1, YYY
            { (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } + GetPicSize(piRecImage).cy div 2 + 1);
          // AddToScrFusy(Rt.Left, Rt.Right);
          // Text:= LangTable.Caption['EventsName:ManualScan'];
          FillChar(FScreenFusy[Max(0, DrawX - GetPicSize(piRecImage).cx)], 2 * GetPicSize(piRecImage).cx * SizeOf(Boolean), $FF);

          for K := 0 to High(FDatSrc.FMediaList) do
            if FDatSrc.FMediaList[K].EventIdx = I then
            begin
              case TMediaType(FDatSrc.FMediaList[K].DataType) of
                mtAudio:
                  Text := LangTable.Caption['EventsName:AudioMark'];
                // Аудио комментарий
                mtPhoto:
                  Text := LangTable.Caption['EventsName:PhotoMark']; // Фото
                mtVideo:
                  Text := LangTable.Caption['EventsName:VideoMark']; // Видео
              end;
              Tag_ := K;
              Typ := rtMedia;
              EventIdx := I;
              Break;
            end;
        end;
        DrawPic(DrawX - GetPicSize(piHandScan).cx div 2, YYY - GetPicSize(piHandScan).cy div 2, piRecImage);
      end;
    end;
  end;

  // --------- Отрисовка протяженных отметок -------------------------------

  // if DebugView1 then
  if MetallSensorZoneState or UnstableBottomSignalState then
  begin

    UnstableTop[rLeft][1] := FFCoordRect.Top + 1;
    UnstableBottom[rLeft][1] := FFCoordRect.Top + (FFCoordRect.Bottom - FFCoordRect.Top) div 8 - 1;
    UnstableTop[rLeft][11] := FFCoordRect.Top + (FFCoordRect.Bottom - FFCoordRect.Top) div 8 + 1;
    UnstableBottom[rLeft][11] := FFCoordRect.Top + 2 * (FFCoordRect.Bottom - FFCoordRect.Top) div 8 - 1;
    UnstableTop[rRight][1] := FFCoordRect.Top + 2 * (FFCoordRect.Bottom - FFCoordRect.Top) div 8 + 1;
    UnstableBottom[rRight][1] := FFCoordRect.Top + 3 * (FFCoordRect.Bottom - FFCoordRect.Top) div 8 - 1;
    UnstableTop[rRight][11] := FFCoordRect.Top + 3 * (FFCoordRect.Bottom - FFCoordRect.Top) div 8 + 1;
    UnstableBottom[rRight][11] := FFCoordRect.Top + 4 * (FFCoordRect.Bottom - FFCoordRect.Top) div 8 - 1;

    for I := 0 to FDatSrc.LLCount - 1 do
    begin

      X1 := StartX + DisToScr(FDatSrc.LL[I].StartDisCoord - StartDisCoord);
      X2 := StartX + DisToScr(FDatSrc.LL[I].EndDisCoord - StartDisCoord);

      if (X2 >= StartX) and (X1 <= EndX) then
      begin
        {
          if X1 < StartX then
          X1 := StartX;
          if X2 > EndX then
          X2 := EndX;
          }
        if (FDatSrc.LL[I].Type_ = 1) and UnstableBottomSignalState then
        // 1 - неустойчевый ДС
        begin
          Line := FScanChNumToBScanLine[FDatSrc.LL[I].Rail, FDatSrc.LL[I].ScanCh];
          if Line <> - 1 then
            with FBScanLines[Line] do
              FillRect(Max(StartX, X1), UnstableTop[FDatSrc.LL[I].Rail, FDatSrc.LL[I].ScanCh], Min(EndX, X2), UnstableBottom[FDatSrc.LL[I].Rail, FDatSrc.LL[I].ScanCh], clRed);
        end;
        if (FDatSrc.LL[I].Type_ = 2) and MetallSensorZoneState then
        // 2 - зона КЭ
        begin

          for Ch := 0 to High(FBScanLines) do
            if FBScanLines[Ch].Rail = FDatSrc.LL[I].Rail then
              with FBScanLines[Ch] do
              begin
                if X1 >= StartX then FillRect(X1, BScanRt.Top, X1 + 1, BScanRt.Bottom + 1, clGreen);
                if X2 <= EndX then FillRect(X2, BScanRt.Top, X2 + 1, BScanRt.Bottom + 1, clGreen);
              end;
        end;
      end;
    end;
  end;

  // --------- Отрисовка отметок алгоритма краскопульта --------------------------------
  // DEBUG
  if PaintSystemViewResState then
  begin
    for I := 0 to FDatSrc.AirBrushCount - 1 do
      if not FDatSrc.FAirBrushList[I].Skip then
      begin

        { if GetSkipBM then }
        DrawX := StartX + DisToScr(FDatSrc.AirBrush[I].Data.DisCoord - StartDisCoord);
        // else DrawX:= StartX + DisToScr(FDatSrc.DisToSysCoord(FDatSrc.AirBrush[I].DisCoord) - StartDisCoord);

        if (DrawX >= StartX) and (DrawX <= EndX) then
        begin
          DrawPic(DrawX - GetPicSize(piRecImage).cx div 2, YYY - GetPicSize(piHandScan).cy div 2
            { (FFCoordRect.Top + FFCoordRect.Bottom) div 2 - 10 } , piRecImage);

          SetLength(InfoRect, Length(InfoRect) + 1);
          with InfoRect[ High(InfoRect)] do
          begin
            CanGroup:= False;
            Text := LangTable.Caption['Common:AirBrushMark']; // + ' DS:' + IntToStr(FDatSrc.FAirBrushList_[I]); !!!!! // DEBUG

            for M := 0 to FDatSrc.AirBrush[I].Data.Count - 1 do
              Text := Text + Format('%s%d. %s - ∆L %d mm', [Chr($0D) + Chr($0A), M + 1, EvalChNumToName(FDatSrc.AirBrush[I].Data.Items[M].ScanCh), (FDatSrc.AirBrush[I].Data.Items[M].EndDisCoord - FDatSrc.AirBrush[I].Data.Items[M].StartDisCoord) * FDatSrc.Header.ScanStep div 100]);

            Tag_ := I;
            Typ := rtAirBrush;
            // AddToScrFusy(DrawX - GetPicSize(piHandScan).cx div 2, DrawX + GetPicSize(piHandScan).cx div 2);
            Rt := Rect(DrawX - GetPicSize(piNotebook).cx div 2 - 1, YYY
              { + (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } - GetPicSize(piNotebook).cy div 2 - 1, DrawX + GetPicSize(piNotebook).cx div 2 + 1, YYY
              { + (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } + GetPicSize(piNotebook).cy div 2 + 1);
            if FViewLabels then
              for Ch := 0 to High(FBScanLines) do
                with FBScanLines[Ch] do
                  if Rail = TRail(FDatSrc.AirBrush[I].Data.Rail) then
                    FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);
          end;
        end;

        if not FDM.RailView then
        begin
          Self.SetClipRect(BoundsRect);
          for II := 0 to High(FDrawAirBrushIdx) do
            if I = FDrawAirBrushIdx[II] then
            begin

              // Отрисовка координаты на которой было выдано задание для БУМ на краскоотметку
              if MainForm.PaintSystemDebugMode then
              begin
                DrawX := StartX + DisToScr(FDatSrc.AirBrush[I].PaintDisCoord - MMToSysCrd(FDatSrc.AirBrushPos - FDatSrc.PaintSystemParams.AirBrush_Shift) - StartDisCoord);
                R := TRail(FDatSrc.AirBrush[I].Data.Rail);
                for Ch := 0 to High(FBScanLines) do
                  with FBScanLines[Ch] do
                    if Rail = TRail(FDatSrc.AirBrush[I].Data.Rail) then
                    begin
                      FillRect(DrawX - 1, BScanRt.Top, DrawX - 1, BScanRt.Bottom + 1, clFuchsia);
                      FillRect(DrawX + 1, BScanRt.Top, DrawX + 1, BScanRt.Bottom + 1, clFuchsia);
                    end;

                DrawX := StartX + DisToScr(FDatSrc.AirBrush[I].SendCmdDisCoord - MMToSysCrd(FDatSrc.AirBrushPos - FDatSrc.PaintSystemParams.AirBrush_Shift) - StartDisCoord);
                R := TRail(FDatSrc.AirBrush[I].Data.Rail);
                for Ch := 0 to High(FBScanLines) do
                  with FBScanLines[Ch] do
                    if Rail = TRail(FDatSrc.AirBrush[I].Data.Rail) then
                    begin
                      FillRect(DrawX - 1, BScanRt.Top, DrawX - 1, BScanRt.Bottom + 1, clGreen);
                      FillRect(DrawX + 1, BScanRt.Top, DrawX + 1, BScanRt.Bottom + 1, clGreen);
                    end;

              end;

              for J := 0 to FDatSrc.AirBrush[I].Data.Count - 1 do
              begin
                Line := FScanChNumToBScanLine[FDatSrc.AirBrush[I].Data.Rail, FDatSrc.AirBrush[I].Data.Items[J].ScanCh];

                ChNum := FDatSrc.AirBrush[I].Data.Items[J].ScanCh;

                if Line <> -1 then
                  with FDatSrc.Config.ScanChannelByNum[ChNum], FBScanLines[Line] do

                  if (FDM.ViewChannel = vcAll) or

                    ((Abs(TurnAngle) <> 90) and (FDM.ViewChannel = vcOtezd) and (EnterAngle < 0)) or
                    ((Abs(TurnAngle) <> 90) and (FDM.ViewChannel = vcNaezd) and (not(EnterAngle < 0))) or

                    ((Abs(TurnAngle) = 90) and (FDM.ViewChannel = vcOtezd) and (Gran = _Work)) or
                    ((Abs(TurnAngle) = 90) and (FDM.ViewChannel = vcNaezd) and (Gran = _UnWork)) then


                  begin
                    SetClipRect(BScanRt);
                    X1 := FStartScrX + (Min(FDatSrc.AirBrush[I].Data.Items[J].StartDisCoord, FDatSrc.AirBrush[I].Data.Items[J].EndDisCoord) - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
                    X2 := FStartScrX + (Max(FDatSrc.AirBrush[I].Data.Items[J].StartDisCoord, FDatSrc.AirBrush[I].Data.Items[J].EndDisCoord) - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;

                    if FDatSrc.Config.ReductionConst then
                    begin
                      Coeff:= 1 - 2 * Ord(FDatSrc.isEGOUSW_BHead);
                      case FDM.Reduction of
                        0: begin
                             DX1 := 0;
                             DX2 := 0;
                           end;
                        1: begin
                             DX1 := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * FDatSrc.AirBrush[I].Data.Items[J].StartDelay) / (FDM.Zoom / 100));
                             DX2 := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * FDatSrc.AirBrush[I].Data.Items[J].EndDelay) / (FDM.Zoom / 100));
                           end;
                        2: begin
                             if (Odd(FDatSrc.AirBrush[I].Data.Items[J].ScanCh) xor FDatSrc.isEGOUSW_BHead) then
//                             if FDatSrc.AirBrush[I].Data.Items[J].StartDelay > FDatSrc.AirBrush[I].Data.Items[J].EndDelay then
                             begin
                               DX1 := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * FDatSrc.AirBrush[I].Data.Items[J].StartDelay) / (FDM.Zoom / 100));
                               DX2 := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * FDatSrc.AirBrush[I].Data.Items[J].EndDelay) / (FDM.Zoom / 100));
                             end
                             else
                             begin
                               DX1 := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * FDatSrc.AirBrush[I].Data.Items[J].EndDelay) / (FDM.Zoom / 100));
                               DX2 := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * FDatSrc.AirBrush[I].Data.Items[J].StartDelay) / (FDM.Zoom / 100));
                             end;
                           end;
                      end;
                    end
                    else
                    case FDM.Reduction of
                      0:  begin
                            DX1 := 0;
                            DX2 := 0;
                          end;
                      1:  begin
                            DX1 := Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead) + FDatSrc.Config.ScanChannelByNum[ChNum].Delta) / (FDM.Zoom / 100));
                            DX2 := Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead) + FDatSrc.Config.ScanChannelByNum[ChNum].Delta) / (FDM.Zoom / 100));
                          end;
                      2:  begin
                            if FDatSrc.AirBrush[I].Data.Items[J].StartDelay < FDatSrc.AirBrush[I].Data.Items[J].EndDelay then
                            begin
                              DX1 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AirBrush[I].Data.Items[J].StartDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
                              DX2 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AirBrush[I].Data.Items[J].EndDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
                            end
                            else
                            begin
                              DX1 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AirBrush[I].Data.Items[J].EndDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
                              DX2 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AirBrush[I].Data.Items[J].StartDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
                            end;
                          end;
                    end;

                    Item := FDatSrc.Config.ScanChannelByNum[FDatSrc.AirBrush[I].Data.Items[J].ScanCh];
                    with Item do
                    begin
                      Y1 := BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.AirBrush[I].Data.Items[J].StartDelay / DelayMultiply + BScanShift - BScanGateMin) / BScanDuration);
                      Y2 := BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.AirBrush[I].Data.Items[J].EndDelay / DelayMultiply + BScanShift - BScanGateMin) / BScanDuration);
                    end;

                    if Config.InvertDelayAxis then
                    begin
                      Y1 := BScanRt.Top + BScanRt.Bottom - Y1;
                      Y2 := BScanRt.Top + BScanRt.Bottom - Y2;
                    end;

                    // Self.TextOut(1, X1, Y1, 5, IntToStr(FDatSrc.AirBrush[I].Items[J].StartDisCoord), True);
                    OutRect := Rect(X1 + DX1 - 3, Min(Y1, Y2) - 3, X2 + DX2 + 3, Max(Y1, Y2) + 3);
                    if (OutRect.Right <= EndX) and (OutRect.Left >= StartX) then
                      Self.DrawRect(OutRect, 2, 0);

                    // if { DebugView2 and } (J = 0) then
                    // // Self.TextOut(0, X1 + DX1 - 3, Min(Y1, Y2) - 3, 1, IntToStr(FDatSrc.AirBrush[I].Items[J].Debug), False);
                    // Self.TextOut(0, X1 + DX1 - 3, Min(Y1, Y2) - 3, 1, IntToStr(FDatSrc.FAirBrushList_[I]), False);
                  end;
              end;
            end;
          ReSetClipRect();
        end;

        { if (FDatSrc.AirBrush[I].DisCoord >= FStartDisCoord) and (FDatSrc.AirBrush[I].DisCoord <= FEndDisCoord) then
          for J:= 0 to FDatSrc.AirBrush[I].Count - 1 do
          begin
          Line:= FScanChNumToBScanLine[FDatSrc.AirBrush[I].Rail, FDatSrc.AirBrush[I].Items[J].ScanCh];
          if Line <> - 1 then
          with FBScanLines[Line] do
          begin
          SetClipRect(BScanRt);
          X1:= FStartScrX + (FDatSrc.AirBrush[I].Items[J].StartDisCoord - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
          X2:= FStartScrX + (FDatSrc.AirBrush[I].Items[J].EndDisCoord - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;

          Item:= FDatSrc.Config.GetScanChByNumber(FDatSrc.AirBrush[I].Items[J].ScanCh);
          with Item do
          begin
          Y1:= BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.AirBrush[I].Items[J].StartDelay / DelayMultiply - BScanGateMin) / BScanDuration);
          Y2:= BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.AirBrush[I].Items[J].EndDelay / DelayMultiply - BScanGateMin) / BScanDuration);
          end;
          Self.TextOut(1, X1, Y1, 5, IntToStr(FDatSrc.AirBrush[I].Items[J].StartDisCoord), True);
          Self.DrawRect(Rect(X1, Y1, X2, Y2), 2, 0);
          end;
          end; }
      end;
  end;

  // --------- Отрисовка значимых мест найденных на БУИ --------------------------------

  if (Config.AutomaticSearchResState) and (not FDM.RailView) then
  begin

//    if FDatSrc.AutoSearchResCount <> 0 then
//      FDatSrc.GetBMStateFirst(FDatSrc.AutoSearchRes[0].CentrCoord, BMState);

    for I := 0 to FDatSrc.AutoSearchResCount - 1 do
      begin
//        DrawX := StartX + DisToScr(FDatSrc.AutoSearchRes[I].CentrCoord - StartDisCoord);
        begin


          Self.SetClipRect(BoundsRect);
            begin
              begin
                Line := FScanChNumToBScanLine[FDatSrc.AutoSearchRes[I].Rail, FDatSrc.AutoSearchRes[I].ScanCh];

                ChNum := FDatSrc.AutoSearchRes[I].ScanCh;

                if Line <> -1 then
                  with FBScanLines[Line] do
                  begin
                    SetClipRect(BScanRt);
                    X1 := FStartScrX + ((FDatSrc.AutoSearchRes[I].CentrCoord - FDatSrc.AutoSearchRes[I].CoordWidth div 2) - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
                    X2 := FStartScrX + ((FDatSrc.AutoSearchRes[I].CentrCoord + FDatSrc.AutoSearchRes[I].CoordWidth div 2) - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
                    BMState:= FDatSrc.GetBMStateFirst(FDatSrc.AutoSearchRes[I].CentrCoord);

                    if FDatSrc.Config.ReductionConst then
                    begin
                      Coeff:= 1 - 2 * Ord(FDatSrc.isEGOUSW_BHead);
                      case FDM.Reduction of
                        0: begin
                             DX1 := 0;
                             DX2 := 0;
                           end;
                        1: begin
                             DX1 := (1 - 2 * Ord(BMState)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * Min(FDatSrc.AutoSearchRes[I].StDelay, FDatSrc.AutoSearchRes[I].EdDelay)) / (FDM.Zoom / 100));
                             DX2 := (1 - 2 * Ord(BMState)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * Max(FDatSrc.AutoSearchRes[I].StDelay, FDatSrc.AutoSearchRes[I].EdDelay)) / (FDM.Zoom / 100));
                           end;
                        2: begin
                             DX1 := (1 - 2 * Ord(BMState)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * Min(FDatSrc.AutoSearchRes[I].StDelay, FDatSrc.AutoSearchRes[I].EdDelay)) / (FDM.Zoom / 100));
                             DX2 := (1 - 2 * Ord(BMState)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * Max(FDatSrc.AutoSearchRes[I].StDelay, FDatSrc.AutoSearchRes[I].EdDelay)) / (FDM.Zoom / 100));
                           end;
                      end;
                    end
                    else
                    case FDM.Reduction of
                      0: begin
                           DX1 := 0;
                           DX2 := 0;
                         end;
                      1: begin
                           DX1 := Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead) + FDatSrc.Config.ScanChannelByNum[ChNum].Delta) / (FDM.Zoom / 100));
                           DX2 := Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead) + FDatSrc.Config.ScanChannelByNum[ChNum].Delta) / (FDM.Zoom / 100));
                         end;
                      2: begin
                           if FDatSrc.AutoSearchRes[I].StDelay < FDatSrc.AutoSearchRes[I].EdDelay then
                           begin
                             DX1 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AutoSearchRes[I].StDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
                             DX2 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AutoSearchRes[I].EdDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
                           end
                           else
                           begin
                             DX1 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AutoSearchRes[I].EdDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
                             DX2 := Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.AutoSearchRes[I].StDelay, FDatSrc.isEGOUSW_BHead) / (FDM.Zoom / 100));
                           end;
                         end;
                    end;

                    Item := FDatSrc.Config.ScanChannelByNum[FDatSrc.AutoSearchRes[I].ScanCh];
                    with Item do
                    begin
                      Y1 := BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.AutoSearchRes[I].StDelay / DelayMultiply - BScanGateMin) / BScanDuration);
                      Y2 := BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.AutoSearchRes[I].EdDelay / DelayMultiply - BScanGateMin) / BScanDuration);
                    end;

                    if Config.InvertDelayAxis then
                    begin
                      Y1 := BScanRt.Top + BScanRt.Bottom - Y1;
                      Y2 := BScanRt.Top + BScanRt.Bottom - Y2;
                    end;

                    OutRect := Rect(X1 + DX1 - 3, Min(Y1, Y2) - 3, X2 + DX2 + 3, Max(Y1, Y2) + 3);
                    if (OutRect.Right <= EndX) and (OutRect.Left >= StartX) then Self.DrawRect(OutRect, 2, 0);
                  end;
              end;
            end;
          ReSetClipRect();
        end;
      end;
  end;

  // --------- Отрисовка АК --------------------------------

  if ViewACState and not FDM.RailView then
  begin

    for R:= rLeft to rRight do
      for Ch:= 1 to 16 do
        Temp_Y[R, Ch]:= - 1;

    J:= 0; // Поиск индекса с которого нужно начинать отрисовку
    for I := High(FDatSrc.AKStateData_) downto 0 do
      if FDatSrc.AKStateData_[I].DisCrd <= StartDisCoord then
      begin
        J:= I;
        Break;
      end;

    for I := J to High(FDatSrc.AKStateData_) do
    begin
      Crd:= FDatSrc.AKStateData_[I].DisCrd;

    //  if I = High(FDatSrc.AKStateData_) then EndCrd:= FDatSrc.MaxDisCoord
      //                                  else EndCrd:= FDatSrc.AKStateData_[I + 1].DisCrd;

      for R:= rLeft to rRight do    // Поиск и отрисовка начала и конца зоны отсутствия АК для кадого канала
        for Ch:= 0 to 15 do
          if FDatSrc.ChFileIdxMask[Ch] then
          begin
            if (not FDatSrc.AKStateData_[I].State) and
               (FDatSrc.AKStateData_[I].Rail = R) and
               (FDatSrc.AKStateData_[I].Ch = Ch) and (Temp_Y[R, Ch] = - 1) then
              Temp_Y[R, Ch]:= FDatSrc.AKStateData_[I].DisCrd; // Найдено начало зоны отсутствия АК

            if (FDatSrc.AKStateData_[I].State) and
               (FDatSrc.AKStateData_[I].Rail = R) and
               (FDatSrc.AKStateData_[I].Ch = Ch) and (Temp_Y[R, Ch] <> - 1) then // Найден конец зоны отсутствия АК
            begin
              StartCrd:= Temp_Y[R, Ch];
              EndCrd:= FDatSrc.AKStateData_[I].DisCrd;
              Temp_Y[R, Ch]:= - 1;
              Line := FScanChNumToBScanLine[R, Ch];

    //          ChNum := FDatSrc.AutoSearchRes[I].ScanCh;

              if Line <> -1 then
                with FBScanLines[Line] do
                begin
                  for K:= 0 to High(ScanChNumList) do
                    if ScanChNumList[K] = Ch then
                    begin
                      L:= K;
                      Break;
                    end;

                  SetClipRect(BScanRt);
                  X1 := FStartScrX + (StartCrd - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
                  X2 := FStartScrX + (EndCrd - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;

                  Y1 := BScanRt.Bottom - Round((L + 1) * ((BScanRt.Bottom - BScanRt.Top) / (Length({FBScanLines[Line].}ScanChNumList) + 1)));

                  Col:= EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, Ch], 15]; // Получение цвета канала контроля

                  // Увеличение яркости
                  ColR:= (Col and $FF0000) shr 16;
                  ColG:= (Col and $FF00) shr 8;
                  ColB:= Col and $FF;

                  ColR:= ColR + 85; if ColR > 255 then ColR:= 255;
                  ColG:= ColG + 85; if ColG > 255 then ColG:= 255;
                  ColB:= ColB + 85; if ColB > 255 then ColB:= 255;

                  Col:= (ColR shl 16) or (ColG shl 8) or ColB;

                  if (X2 > EndX) then X2:= EndX else Self.VLine(X2, Y1 - 4, 9, 2, Col);
                  if (X1 < StartX) then X1:= StartX else Self.VLine(X1, Y1 - 4, 9, 2, Col);
                  Self.HLine(X1, Y1, X2 - X1, 2, Col);

                end;
            end;
          end;
    end;
  end;

(*
    for I := J to High(FDatSrc.AKStateData) do
    begin
      Crd:= FDatSrc.AKStateData[I].DisCrd;

    //  if I = High(FDatSrc.AKStateData_) then EndCrd:= FDatSrc.MaxDisCoord
      //                                  else EndCrd:= FDatSrc.AKStateData_[I + 1].DisCrd;

      for R:= rLeft to rRight do    // Поиск и отрисовка начала и конца зоны отсутствия АК для кадого канала
        for Ch:= 0 to 15 do
          if FDatSrc.ChFileIdxMask[Ch] then
          begin
            if (not FDatSrc.AKStateData[I].State[R, Ch]) and (Temp_Y[R, Ch] = - 1) then
              Temp_Y[R, Ch]:= FDatSrc.AKStateData[I].DisCrd; // Найдено начало зоны отсутствия АК

            if (FDatSrc.AKStateData[I].State[R, Ch]) and (Temp_Y[R, Ch] <> - 1) then // Найден конец зоны отсутствия АК
            begin
              StartCrd:= Temp_Y[R, Ch];
              EndCrd:= FDatSrc.AKStateData[I].DisCrd;
              Temp_Y[R, Ch]:= - 1;
              Line := FScanChNumToBScanLine[R, Ch];

    //          ChNum := FDatSrc.AutoSearchRes[I].ScanCh;

              if Line <> -1 then
                with FBScanLines[Line] do
                begin
                  for K:= 0 to High(ScanChNumList) do
                    if ScanChNumList[K] = Ch then
                    begin
                      L:= K;
                      Break;
                    end;

                  SetClipRect(BScanRt);
                  X1 := FStartScrX + (StartCrd - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
                  X2 := FStartScrX + (EndCrd - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;

                  Y1 := BScanRt.Bottom - Round((L + 1) * ((BScanRt.Bottom - BScanRt.Top) / (Length({FBScanLines[Line].}ScanChNumList) + 1)));

                  Col:= EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, Ch], 15]; // Получение цвета канала контроля

                  // Увеличение яркости
                  ColR:= (Col and $FF0000) shr 16;
                  ColG:= (Col and $FF00) shr 8;
                  ColB:= Col and $FF;

                  ColR:= ColR + 85; if ColR > 255 then ColR:= 255;
                  ColG:= ColG + 85; if ColG > 255 then ColG:= 255;
                  ColB:= ColB + 85; if ColB > 255 then ColB:= 255;

                  Col:= (ColR shl 16) or (ColG shl 8) or ColB;

                  if (X2 > EndX) then X2:= EndX else Self.VLine(X2, Y1 - 4, 9, 2, Col);
                  if (X1 < StartX) then X1:= StartX else Self.VLine(X1, Y1 - 4, 9, 2, Col);
                  Self.HLine(X1, Y1, X2 - X1, 2, Col);

                end;
            end;
          end;
    end;
    ReSetClipRect();
  end;  *)

  // --------- Отрисовка НПКУч --------------------------------

  if ViewACState and not FDM.RailView then
  begin

    for R:= rLeft to rRight do
      for Ch:= 1 to 16 do
        Temp_Y[R, Ch]:= - 1;

    J:= 0; // Поиск индекса с которого нужно начинать отрисовку
    for I := High(FDatSrc.UCSData) downto 0 do
      if FDatSrc.UCSData[I].StDisCoord <> - 1 then

      if FDatSrc.UCSData[I].StDisCoord <= StartDisCoord then
      begin
        J:= I;
        Break;
      end;

    for I := J to High(FDatSrc.UCSData) do
    begin
      StartCrd:= FDatSrc.UCSData[I].stDisCoord;
      EndCrd:= FDatSrc.UCSData[I].EdDisCoord;
      X1 := FStartScrX + (StartCrd - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
      X2 := FStartScrX + (EndCrd - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
      for Ch := 0 to High(FBScanLines) do
          with FBScanLines[Ch] do
          begin
            if X2 > StartX then Self.VDashLine2(X2, BScanRt.Top, BScanRt.Bottom - BScanRt.Top, 2, FDispCols.Border);
            if X1 > StartX then Self.VDashLine2(X1, BScanRt.Top, BScanRt.Bottom - BScanRt.Top, 2, FDispCols.Border);
            tmp:= FFCoordRect;
            tmp.Left:= X1;
            tmp.Right:= X2;
            if tmp.Right > StartX then
            begin
              FillRect(Max(StartX, tmp.Left), tmp.Top, tmp.Right, tmp.Bottom, FDispCols.Defect);
              if FDatSrc.Debug then TextOut(1, tmp.Left, tmp.Top + 3, 5, IntToStr(I), True);
            end;
          end;
    end;
//    ReSetClipRect();
  end;

  // --------- Отрисовка HC --------------------------------
  if DrawHC and not FDM.RailView then
  begin
    X:= - 1;
    for I:= 0 to FDatSrc.HCCount - 1 do
    begin
      DrawX1:= StartX + DisToScr(FDatSrc.HC[I].StartDisCoord - StartDisCoord);
      DrawX2:= StartX + DisToScr(FDatSrc.HC[I].EndDisCoord - StartDisCoord);
      if DrawX2 < FBScanLines[1].BScanRt.Left then Continue;
      if DrawX1 < FBScanLines[1].BScanRt.Left then DrawX1:= FBScanLines[1].BScanRt.Left;

      if ((DrawX1 >= StartX) and (DrawX1 <= EndX)) or
      ((DrawX2 >= StartX) and (DrawX2 <= EndX)) then
      begin

        Line:= 0 {FScanChNumToBScanLine[FDatSrc.HC[I].Rail, 2]};

        if Line <> - 1 then
        with FBScanLines[Line] do
        begin
          SetClipRect(BScanRt);
          with Item do
          begin
            Y1:= BScanRt.Bottom - Round(FDatSrc.HC[I].Valut / 100 * (BScanRt.Bottom  - BScanRt.Top));
            Y2:= BScanRt.Bottom;
          end;
        //Self.VLine() (DrawX1, Y1, DrawX2 - DrawX1, 1, 0);
        //Self.HLine(DrawX1, Y1, DrawX2 - DrawX1, 1, 0);
        {         if (X <> - 1) and (X < DrawX1 - 1) then
        begin
        Self.FillRect(Rect(DrawX1, Y1, DrawX2, Y2), 0);
        end;
        }

        Self.FillRect(DrawX1, Y1, DrawX2, Y2, GetDDColor(ColorToRGB(clScrollBar)));
        Self.DrawLine(DrawX1, Y1, DrawX2, Y1, 2, GetDDColor(ColorToRGB(clBlack)));

        //DrawRect(Rect(DrawX1, Y1, DrawX2, Y2), 2, 0);
        X:= DrawX2;
        end;
      end;
      ReSetClipRect();
    end;

    if FDatSrc.HCCount <> 0 then
    begin

      for vvv := 1 to 10 do
      begin
        if vvv / 2 > FDatSrc.Header.HCThreshold then break;
        with FBScanLines[0], BScanRt do
        begin
          YYY_:= BScanRt.Bottom  - Round(((vvv / 2) * 100 / FDatSrc.Header.HCThreshold) / 100 * (BScanRt.Bottom  - BScanRt.Top));
          Self.TextOut(0, Left + 25, YYY_ + 15, 5, Format('%3.1f mm', [vvv/2]), True);
          HLine(Left + 35, YYY_, Right  - Left, 1, 0);
        end;
      end;
    end;
  end;

  // --------- Отрисовка меток Записей Блокнота ----------------------------------

  CalcIndex := 0;
  for I := 0 to FDatSrc.NotebookCount - 1 do

    if (FDatSrc.Notebook[I].ID = 2) and (ConfigUnit.Config.ProgramMode <> pmRADIOAVIONICA_MAV) or (FDatSrc.Notebook[I].ID = 3) and (ConfigUnit.Config.ProgramMode = pmRADIOAVIONICA_MAV) then

    begin
      if GetSkipBM then
        DrawX := StartX + DisToScr(FDatSrc.Notebook[I].SysCoord - StartDisCoord)
      else
        DrawX := StartX + DisToScr(FDatSrc.Notebook[I].DisCoord - StartDisCoord);

      if (DrawX >= StartX) and (DrawX <= EndX) then
      begin
        DrawPic(DrawX - GetPicSize(piNotebook).cx div 2, YYY - GetPicSize(piHandScan).cy div 2
          { (FFCoordRect.Top + FFCoordRect.Bottom) div 2 - 10 } , piNotebook);

        SetLength(InfoRect, Length(InfoRect) + 1);
        with InfoRect[ High(InfoRect)] do
        begin
          CanGroup:= False;
          Text := LangTable.Caption['Common:Record'] + ' - ' + HeaderStrToString(FDatSrc.Notebook[I].Defect)
          { + ' DC:' + IntToStr(FDatSrc.Nobebook[I].DisCoord) } ;
          Tag_ := CalcIndex;
          Typ := rtNotebook;
          // AddToScrFusy(DrawX - GetPicSize(piHandScan).cx div 2, DrawX + GetPicSize(piHandScan).cx div 2);
          Rt := Rect(DrawX - GetPicSize(piNotebook).cx div 2 - 1, YYY
            { (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } - GetPicSize(piNotebook).cy div 2 - 1, DrawX + GetPicSize(piNotebook).cx div 2 + 1, YYY
            { (FFCoordRect.Top + FFCoordRect.Bottom) div 2 } + GetPicSize(piNotebook).cy div 2 + 1);

          if FViewLabels then
          begin
            R := FDatSrc.RailToPathRail(TRail(DatSrc.Notebook[I].Rail));
            // for R:= rLeft to rRight do
            for Ch := 0 to High(FBScanLines) do
              if FBScanLines[Ch].Rail = R then
                with FBScanLines[Ch] do
                begin
                  // FillRect(DrawX - 1, DrawRect.Top, DrawX - 1, DrawRect.Bottom + 1, FDispCols.Border);
                  // FillRect(DrawX + 1, DrawRect.Top, DrawX + 1, DrawRect.Bottom + 1, FDispCols.Border);
                  FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);
                end;
          end;
        end;
      end;
      Inc(CalcIndex);
    end;

  // ----- Отладка -------------------------------------------

  if FDatSrc.Debug then
  begin
    CalcIndex := 0;
    TextTop:= 10;
    for I := 0 to High(FDatSrc.FACPoints) do
      begin
        DrawX := StartX + DisToScr(FDatSrc.FACPoints[I].DisCoord - StartDisCoord);

        if (DrawX >= StartX) and (DrawX <= EndX) then
        begin
          Text:= Format('Idx: %d; S:%d; BM:%d', [I, Ord(FDatSrc.FACPoints[I].State), Ord(FDatSrc.FACPoints[I].BackMotionFlag)]);
          TextOut(1, DrawX + TextWidth(1, Text) div 2 + 2, TextTop, 5, Text, True);
          TextTop:= TextTop + 15;
          for Ch := 0 to High(FBScanLines) do
            with FBScanLines[Ch] do
              FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);
        end;
      end;
  end;
end;

procedure TAvk11Display.CreateImage(StartDisCoord, StartX, EndX: Integer);
type
  TRt = record
    Rt: TRect;
    Flag: Boolean;
  end;

var
  R: TRail;
  I, II, Ch: Integer;
  DrawX: Integer;
  DrawX1: Integer;
  MSDelay: Integer;
  Cur: TRailRect;
  FHeight: Integer;
  LeftIdx: Integer;
  LoadShift: Integer;
  S: string;
  // TT: TEvalChannelsParams;
  S1: Integer;
  S2: Integer;
  DX, DY: Integer;
  X, Y: Integer;
  Flag: Boolean;

  // -----------------------------------------------

  AA, BB: Integer;
  J: Integer;
  K: Integer;
  DrawX2: Integer;
  Height_: Integer;
  Col: DWord;
  DrawY2: Integer;
  (*
    function GetRedShift(Ch, Delay, X: Integer): Integer;
    begin
    if (FDM.ReductionMode = 0) or (FDM.Reduction = 0) then Result:= X
    else Result:= X {+ Round((FDispConf.ReducePos[FDM.ReductionMode, FDM.Reduction, Ch] + FDispConf.ReduceDel[FDM.ReductionMode, FDM.Reduction, Ch] * Delay) / (FDM.Zoom / 100))};
    //    else Result:= Round(FDispConf.ReducePos[FDM.ReductionMode, FDM.Reduction, Ch] + FDispConf.ReduceDel[FDM.ReductionMode, FDM.Reduction, Ch] * Delay);
    //                                                                                                                     FDatSrc.Header.ScanStep div FDM.Zoom
    end;
    *)
var

  Rt: TRect;
  Rt_: TRect;
  Rects: array [0 .. 7] of TRt;
  DrawBuff: array of Byte;
  Line, q, H, W: Integer;
  Params: TMiasParams;
  StR, EdR, Rail: RRail;
  LinkArr: array [RRail] of Integer;

begin

//  PixelCount_ := 0;

  if not Assigned(FDatSrc) then Exit;
  if not NowInit then Exit;

  Inc(FDrawCount);

  FStartScrX := StartX;
  FEndScrX := EndX;
  FLastDrawX1[rLeft] := StartX;
  FLastDrawX1[rRight] := StartX;
  for Line := 0 to 16 do
  begin

    FLastDrawX2[Line, rLeft] := StartX;
    FLastDrawX2[Line, rRight] := StartX;
    FLastDrawY2[Line, rLeft] := 0;
    FLastDrawY2[Line, rRight] := 0;

    FLastDrawX2[Line, rLeft] := -1;
    FLastDrawX2[Line, rRight] := -1;
  end;

  if Reduction <> 0 then LoadShift := Round(FMaxReduce[FDM.Reduction] / (FDatSrc.Header.ScanStep / 100))
                    else LoadShift := 0;

  DrawBackMotionZone(StartDisCoord, StartX, EndX, 1);
  DrawBackMotionZone(StartDisCoord, StartX, EndX, 2);

  if Assigned(FBeforeSgnExtDraw) then FBeforeSgnExtDraw(Self, 1); // --- Отрисовка значемых мест ---
  DrawEvents(StartDisCoord, StartX, EndX, True);


  if FDM.RailView then
  begin // Очистка зон вывода вида с боку
    // FillRect_(FRailSide[rLeft], FDispCols.Fon);
    // FillRect_(FRailSide[rRight], FDispCols.Fon);
    FillChar(FRailChem[rLeft, 0], 2 * 2001 * SizeOf(TRailRect), 0);
    LeftIdx := FDatSrc.GetLeftEventIdx(StartDisCoord);
    // FDatSrc.GetParamFirst(StartDisCoord, Params);

    for Line := 0 to High(FBScanLines) do
    begin
      R := FBScanLines[Line].Rail;

      with FBScanLines[Line] do FHeight := Abs(BScanRt.Bottom - BScanRt.Top);
      for I := LeftIdx to FDatSrc.EventCount - 1 do
      begin

        DrawX := StartX + (FDatSrc.Event[I].DisCoord - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
        if Flag then
        begin
          DrawX1 := DrawX;
          Flag := False;
        end;

        S1 := FDatSrc.Params[I].Par[R, FBScanLines[Line].MirrorShadowEvalChNum[0]].StStr;
        S2 := FDatSrc.Params[I].Par[R, FBScanLines[Line].MirrorShadowEvalChNum[0]].EndStr;
        MSDelay := (S1 + S2) div 2;

        if (S1 = 0) or (S2 = 0) then Continue;

        // MSDelay:= (FDatSrc.Event[I].Params.StStr[R, 0] + FDatSrc.Event[I].Params.EdStr[R, 0]) div 2;

        // точки      мкс   max мкс
        Cur.RailHeight := Round(FHeight * MSDelay / 70); // Расчет высоты релься в точках по значению задержки донного сигнала
        Cur.RailHeight_mm:= Round(MSDelay * 5.9 / 2); // Расчет высоты релься в точках по значению задержки донного сигнала

//        FSideViewZoom[R] := Cur.RailHeight / 180;
        Cur.RailBase := Cur.RailHeight - Round(RPts[19].Y * Cur.RailHeight / 180);
        Cur.RailHead := Round(RPts[11].Y * Cur.RailHeight / 180);
        Cur.Height := FHeight;



//        Cur.BackTime := Round(0.70 * 2.95 * MSDelay); // Расчет (по значению задержки донного сигнала) момента отражения от подошвы для канала контроля шейки
//        Cur.BackTime := Round(2 * 1.41 * ((5.9 * MSDelay) / 2) / 3.26);
        Cur.BackTime := Round(2 * ((5.9 * (MSDelay / 2)) / Cos((90 - 42) * pi / 180)) / 3.26);
//        Cur.BackTime:= 120;
//        Cur.BackTime:= 152;

        for II := DrawX1 to DrawX do
          if (II >= 0) and (II <= 2000) then
            FRailChem[R, II] := Cur;

        DrawX1 := DrawX;
        if DrawX1 > EndX then Break;
      end;
    end;

    for Line := 0 to High(FBScanLines) do
    begin
      R := FBScanLines[Line].Rail;
      with FBScanLines[Line] do
        SetClipRect(BScanRt);
      for II := StartX to EndX do
        if FRailChem[R, II].RailHeight <> 0 then
        begin
          FastPoint(II, ClipRect.Top + FRailChem[R, II].RailHeight, 1, 2, 0);
          FastPoint(II, ClipRect.Top + FRailChem[R, II].RailHead, 1, 2, 0);
          FastPoint(II, ClipRect.Top + FRailChem[R, II].RailHeight - FRailChem[R, II].RailBase, 1, 2, 0);
        end;
    end;

    try
      if Assigned(FFiltr) then
        FFiltr.StartFilter(StartDisCoord);

      // for AA:= 100 to 200  BB

   //   FDatSrc.LoadData(StartDisCoord, MaxInt, LoadShift, DrawRailViewSample);

        try
          if Assigned(FFiltr) then
            FFiltr.StartFilter(StartDisCoord);
          case FFilterMode of
            moNoCalc, moOff, moHide:
              begin
                FDatSrc.LoadData(StartDisCoord, MaxInt, LoadShift, DrawRailViewSample);
                // FDatSrc.LoadData(StartDisCoord, MaxInt, LoadShift, DrawBScanSample2);
              end;
            moMark:
              begin
                FDrawMode := 1;
                FDatSrc.LoadData(StartDisCoord, MaxInt, LoadShift, DrawRailViewSample);
                FDrawMode := 2;
                if Assigned(FFiltr) then
                  FFiltr.StartFilter(StartDisCoord);
                FDatSrc.LoadData(StartDisCoord, MaxInt, LoadShift, DrawRailViewSample);
              end;
          end;
        except
        end;

      // FastPoint(102, 100, 20, 30, 0);
    except
    end;
    (*
      // --------------------------------------- Отрисовка вида сбоку - рельса ----------------------

      for R:= rLeft to rRight do
      for I:= 0 to 20 do
      begin
      SetClipRect(FRailSide[R]);

      FSideViewZoom[R]:= FRailChem[R, (StartX + EndX) div 2].RailHeight / 180;

      DX:= Round((RPts[I + 1].X - RPts[I].X) * FSideViewZoom[R]);
      DY:= Round((RPts[I + 1].Y - RPts[I].Y) * FSideViewZoom[R]);

      if Abs(DX) > Abs(DY) then
      begin
      if DX <> 0 then
      for II:= 0 to Abs(DX) do
      begin
      SetPoint(X, Y, 2, 2, 0);
      X:= FRailSide[R].Left + Round(RPts[I].X * FSideViewZoom[R]) + DX * II div Abs(DX);
      Y:= FRailSide[R].Top + Round(RPts[I].Y * FSideViewZoom[R]) + DY * II div Abs(DX) - 1;
      end
      end
      else
      begin
      if DY <> 0 then
      for II:= 0 to Abs(DY) do
      begin
      SetPoint(X, Y, 2, 2, 0);
      X:= FRailSide[R].Left + Round(RPts[I].X * FSideViewZoom[R]) + DX * II div Abs(DY);
      Y:= FRailSide[R].Top + Round(RPts[I].Y * FSideViewZoom[R]) + DY * II div Abs(DY) - 1;
      end;
      end;
      {
      textout(0, FRailSide[R].Left + Round(RPts[I].X * FSideViewZoom[R]),
      FRailSide[R].Top + Round(RPts[I].Y * FSideViewZoom[R]), 0, IntToStr(I), True);
      }
      end;

      // --------------------------------------- Отрисовка вида сбоку - сигналов -------------------

      DrawX:= FStartScrX + ((FStartDisCoord + FEndDisCoord) div 2 - 10 - FStartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
      VLine(DrawX, FDrawRect.Top, FDrawRect.Bottom - FDrawRect.Top, 1, 0);
      DrawX:= FStartScrX + ((FStartDisCoord + FEndDisCoord) div 2 + 10 - FStartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
      VLine(DrawX, FDrawRect.Top, FDrawRect.Bottom - FDrawRect.Top, 1, 0);

      StartP;
      if Assigned(FFiltr) then FFiltr.StartFilter(StartDisCoord);
      FDatSrc.LoadData((FStartDisCoord + FEndDisCoord) div 2 - FMaxReduce[FDM.Reduction] - 10,
      (FStartDisCoord + FEndDisCoord) div 2 + FMaxReduce[FDM.Reduction] + 10, 0, RailSideViewSample);
      *)
  end
  else
  begin
    // FLastDrawX2[rLeft]:= - 100;
    // FLastDrawX2[rRight]:= - 100;

    for q := 0 to 16 do
    begin

      FLastDrawY2[q, rLeft] := -1;
      FLastDrawY2[q, rRight] := -1;
      // FBSALastCoord[rLeft]:= Max(0, StartDisCoord);
      // FBSALastCoord[rRight]:= Max(0, StartDisCoord);
      FLastDrawX2[q, rLeft] := StartX;
      FLastDrawX2[q, rRight] := StartX;
    end;

//    PixelCount_ := 0;
{$IFDEF DIRECTMEMOUT}
    StartP;
{$ENDIF}
    try
      if Assigned(FFiltr) then
        FFiltr.StartFilter(StartDisCoord);
      case FFilterMode of
        moNoCalc, moOff, moHide:
          begin
            FDatSrc.LoadData(StartDisCoord, MaxInt, LoadShift, DrawBScanSample);
            // FDatSrc.LoadData(StartDisCoord, MaxInt, LoadShift, DrawBScanSample2);
          end;
        moMark:
          begin
            FDrawMode := 1;
            FDatSrc.LoadData(StartDisCoord, MaxInt, LoadShift, DrawBScanSample);
            FDrawMode := 2;
            if Assigned(FFiltr) then
              FFiltr.StartFilter(StartDisCoord);
            FDatSrc.LoadData(StartDisCoord, MaxInt, LoadShift, DrawBScanSample);
          end;
      end;
    except
    end;
{$IFDEF DIRECTMEMOUT}
    EndP;
{$ENDIF}
  end;

  {
    if FAmplDon and (FDatSrc.Header.ContentFlg and cfBSABlock <> 0) then // Точная огибающая дс через блоки
    begin
    StartP;
    for I:= 0 to High(FDatSrc.FBSAmplLink) do
    if (FEndDisCoord >= FDatSrc.FBSAmplLink[I].StDisCrd) and
    (FStartDisCoord <= FDatSrc.FBSAmplLink[I].EdDisCrd) then
    for R:= rLeft to rRight do
    begin
    case R of
    rLeft: FDatSrc.FileBody.Position:= FDatSrc.FBSAmplLink[I].Offset1;
    rRight: FDatSrc.FileBody.Position:= FDatSrc.FBSAmplLink[I].Offset2;
    end;

    with FBScanLines[FChIdx[R, 1]] do SetClipRect(DrawRect);
    SetLength(DrawBuff, FDatSrc.FBSAmplLink[I].Len);
    FDatSrc.FileBody.ReadBuffer(DrawBuff[0], FDatSrc.FBSAmplLink[I].Len);
    for J:= 0 to FDatSrc.FBSAmplLink[I].Len - 1 do
    begin
    DrawX:= FStartScrX + (FDatSrc.FBSAmplLink[I].StDisCrd + J - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
    with FBScanLines[FChIdx[R, 1]].DrawRect do
    FastPoint(DrawX, Top + Round(DrawBuff[J] * Abs(Top - Bottom) / 255), 1, 1, 0);
    end;
    end;
    EndP;
    SetLength(DrawBuff, 0);
    end;
    }

  SetClipRect(Rect(0, 0, 30000, 30000));

  FillChar(FScreenFusy[0], 3001 * SizeOf(Boolean), 0);
//  if Assigned(FAfterSgnExtDraw) then FAfterSgnExtDraw(Self, 3); // --- Отрисовка значемых мест ---

  if (not RailView) and (Assigned(FExtendedViewDraw)) then
  begin
    case ViewMode of
      vmAllLines: begin
                    StR:= r_Left;
                    EdR:= r_Right;
                  end;
  vmOneRailLines,
       vmOneLine: begin
                    if ViewRail = rLeft then
                    begin
                      StR:= r_Left;
                      EdR:= r_Left;
                    end
                    else
                    begin
                      StR:= r_Right;
                      EdR:= r_Right;
                    end;
                  end;              // ВОПРОС !!! - правильность данного места
{     ddRightRail: begin
                    StR:= r_Right;
                    EdR:= r_Right;
                  end;
     ddLeftTape1,
     ddLeftTape2,
     ddLeftTape3,
     ddLeftTape4: begin
                     StR:= r_Left;
                     EdR:= r_Left;
                   end;

    ddRightTape1,
    ddRightTape2,
    ddRightTape3,
    ddRightTape4: begin
                    StR:= r_Right;
                    EdR:= r_Right;
                  end; }
    end;

    LinkArr[r_Left]:= FExtendedViewTopIdx;
    LinkArr[r_Right]:= FExtendedViewBtmIdx;
//    for I:= 0 to ExtendedViewListCount - 1 do
//      for Rail:= StR to EdR do
//        FExtendedViewDraw(Self, Rail, StepIn(FBScanBox.GetRect(0, LinkArr[Rail] + I)), FStartDisCoord, FEndDisCoord, I);
  end;

/////////////////////////////////

  DrawRuler(StartDisCoord, StartX, EndX);
  // DrawLongEvents(StartDisCoord, StartX, EndX);
  DrawEvents(StartDisCoord, StartX, EndX);
  DrawCoordGrid(StartDisCoord, StartX, EndX);

  // Отрисовка расположения элементов системы краскопульта
  if MainForm.PaintSystemDebugMode and FDatSrc.PaintSystemParamsExists then
  begin

    DrawX := FStartScrX + DisToScr(FCenterDisCoord - StartDisCoord);
    for R := rLeft to rRight do
      for Ch := 0 to High(FBScanLines) do
        with FBScanLines[Ch] do
          FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);

    TextOut(1, DrawX, 10, 5, 'Center', False);

    DrawX := FStartScrX + DisToScr(FCenterDisCoord - FDatSrc.SensorPos * 100 div FDatSrc.Header.ScanStep - StartDisCoord);
    for R := rLeft to rRight do
      for Ch := 0 to High(FBScanLines) do
        with FBScanLines[Ch] do
          FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);

    TextOut(1, DrawX, 10, 5, 'Sensor', False);

    DrawX := FStartScrX + DisToScr(FCenterDisCoord - FDatSrc.AirBrushPos * 100 div FDatSrc.Header.ScanStep - StartDisCoord);
    for R := rLeft to rRight do
      for Ch := 0 to High(FBScanLines) do
        with FBScanLines[Ch] do
          FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);

    TextOut(1, DrawX, 10, 5, 'Paint', False);

    DrawX := FStartScrX + DisToScr(FCenterDisCoord - FDatSrc.PaintSystemParams.DecisionMakingBoundary * 100 div FDatSrc.Header.ScanStep - StartDisCoord);
    for R := rLeft to rRight do
      for Ch := 0 to High(FBScanLines) do
        with FBScanLines[Ch] do
          FillRect(DrawX, BScanRt.Top, DrawX, BScanRt.Bottom + 1, FDispCols.Border);

    TextOut(1, DrawX, 10, 5, 'Decision boundary', False);

  end;

  if not FDM.RailView then
  begin
    // TextOut(0, (FFCoordRect.Right + FBScanLines[0].DrawRect.Right) div 2, (FFCoordRect.Top + FFCoordRect.Bottom) div 2 - 5, 1, FOnScrText, False);

    FillRect(FFCoordRect.Left - 1, FFCoordRect.Top, FBScanLines[0].BScanRt.Left, FFCoordRect.Bottom, FDispCols.Fon);
    FillRect(FBScanLines[0].BScanRt.Right, FFCoordRect.Top, FFCoordRect.Right, FFCoordRect.Bottom, FDispCols.Fon);

    if DisplayMode_ShowChGate { and not HeightToSmall } then
    begin
      TextRect(0, GetTopHalfOfRect(FChGateRect[rLeft]), 1, FOnScrText, True);
      TextRect(0, GetBtmHalfOfRect(FChGateRect[rLeft]), 1, LangTable.Caption['Common:m/s'], True);

      TextRect(0, GetTopHalfOfRect(FChGateRect[rRight]), 1, FRulerText1, False);
      TextRect(0, GetBtmHalfOfRect(FChGateRect[rRight]), 1, FRulerText2, False);
    end;

    // TextRect(0, (FFCoordRect.Right + FBScanLines[0].BScanRt.Right) div 2, (FFCoordRect.Top + FFCoordRect.Bottom) div 2 - 5, 1, FOnScrText, True);
    // TextOut(0, (FFCoordRect.Right + FBScanLines[0].BScanRt.Right) div 2, (FFCoordRect.Top + FFCoordRect.Bottom) div 2 + 5, 1, LangTable.Caption['Common:m/s'], True);
    // TextOut(0, (FFCoordRect.Left + FBScanLines[0].BScanRt.Left) div 2, (FFCoordRect.Top + FFCoordRect.Bottom) div 2, 1, FRulerText1 + ' ' + FRulerText2, False);
  end;

  { else
    with FDrawBox.GetRect(1, 1) do
    if Right - Left > 40 then
    begin
    FillRect(Left, Top, Right, Bottom, FDispCols.Fon);
    DrawRect(FDrawBox.GetRect(1, 1), 1, FDispCols.Border);
    VLine(Left + (Right - Left) div 2, Top, Bottom - Top, 1, FDispCols.Border);
    TextOut(0, Left + 3 * (Right - Left) div 4, (FFCoordRect.Top + FFCoordRect.Bottom) div 2 - 5, 1, FOnScrText, True);
    TextOut(0, Left + 3 * (Right - Left) div 4, (FFCoordRect.Top + FFCoordRect.Bottom) div 2 + 5, 1, LangTable.Caption['Common:m/s'], True);
    TextOut(0, Left + (Right - Left) div 4, (FFCoordRect.Top + FFCoordRect.Bottom) div 2 - 5, 1, FRulerText1, True);
    TextOut(0, Left + (Right - Left) div 4, (FFCoordRect.Top + FFCoordRect.Bottom) div 2 + 5, 1, FRulerText2, True);
    end } ;

  // if Assigned(FAfterSgnExtDraw) then FAfterSgnExtDraw(Self, 2); // --- Отрисовка значемых мест ---

  // FillRect(10, 10, 100, 100, 0);

  {
    S:= '1222222222888';

    X:= 100;
    Y:= 200;
    TextOut(1, X, Y, 0, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(1, S), Y - TextHeight(1, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);

    X:= 200;
    Y:= 200;
    TextOut(1, X, Y, 1, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(1, S), Y - TextHeight(1, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);

    X:= 300;
    Y:= 200;
    TextOut(1, X, Y, 2, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(1, S), Y - TextHeight(1, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);

    X:= 400;
    Y:= 200;
    TextOut(1, X, Y, 3, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(1, S), Y - TextHeight(1, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);

    X:= 500;
    Y:= 200;
    TextOut(1, X, Y, 4, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(1, S), Y - TextHeight(1, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);

    X:= 600;
    Y:= 200;
    TextOut(1, X, Y, 5, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(1, S), Y - TextHeight(1, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);
    }
  {
    S:= '1222222222888';

    X:= 100;
    Y:= 200;
    TextOut(0, X, Y, 0, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(0, S), Y - TextHeight(0, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);

    X:= 200;
    Y:= 250;
    TextOut(0, X, Y, 1, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(0, S), Y - TextHeight(0, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);

    X:= 300;
    Y:= 300;
    TextOut(0, X, Y, 2, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(0, S), Y - TextHeight(0, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);

    X:= 400;
    Y:= 350;
    TextOut(0, X, Y, 3, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(0, S), Y - TextHeight(0, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);

    X:= 500;
    Y:= 400;
    TextOut(0, X, Y, 4, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(0, S), Y - TextHeight(0, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);

    X:= 600;
    Y:= 450;
    TextOut(0, X, Y, 5, S, True);
    DrawRect(Rect(X, Y, X + TextWidth(0, S), Y - TextHeight(0, S)), 1, 0);
    HLine(X - 25, Y, 50, 1, 0);
    VLine(X, Y - 25, 50, 1, 0);
    }

  {
    X:= 350;
    H:= 20;
    W:= 40;
    for R := rLeft to rRight do
    begin
    Y:= 50;
    for Ch := 0 to 11 do
    for q := 1 to 2 do
    begin
    TextRect(0, Rect(X, Y, X + W, Y + H), 1, Format('R: %d; Ch: %d; Idx: %d', [Ord(R), Ch, q]), False);
    FillRect(X + W * 2, Y, X + 3 * W, Y + H, ChNumColors[R, Ch, 15]);
    Y:= Y + H;
    end;
    X:= X + 2 * W + 100;
    end;
    }

end;

procedure TAvk11Display.FullRefresh;
begin
  if not Assigned(FDatSrc) then Exit;
  if not Assigned(FDatSrc.FileBody) then Exit;
  ApplyDisplayMode;
  ReCalcOutRect;
  Refresh;
end;

procedure TAvk11Display.Refresh;
var
  DrawWidth: Integer;
  R: TRail;
  DX, Y, X, Y1, Y2, I, J, K: Integer;
  A, B, C, D, E, F, G: Integer;
  H: Single;
  Ch: Integer;
  DrawCol: Integer;
  tmp: TRect;
  Params: TMiasParams;
  CoordParams: TCrdParams;
  // --------------------------------

  WorkRect: TRect;
  Color: Integer;

  // --------------------------------

  function GetSurfaceText(Rail: TRail; Gran: TGran): string;
  begin
    case Gran of
      _None: Result := '';
      _Work: Result := '' + LangTable.Caption['Common:WorkSurf_SHORT'];
      _UnWork: Result := '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
      _Work_Left__UnWork_Right: if Rail = rLeft then Result := '' + LangTable.Caption['Common:WorkSurf_SHORT']
                                                else Result := '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
      _UnWork_Left__Work_Right: if Rail = rLeft then Result := '' + LangTable.Caption['Common:UnWorkSurf_SHORT']
                                                else Result := '' + LangTable.Caption['Common:WorkSurf_SHORT'];
    end;
    // Result:= Result + IntToStr(Ord(Rail)) + IntToStr(Ord(Gran));
  end;

  procedure DrawStrobe;
  var
    I, J, L: Integer;
    Time: string;
    K: TBScanLine;

    procedure DrawStrob_(WorkRect: TRect; EvalChannel, Delay1, Delay2, Sens, Color, BGColor: Integer; Flg: Boolean; Mode: Integer; Text: string);
    // Mode = 1 Строб + Чувствительность с боку от него
    // Mode = 2 Строб + Чувствительность на нем в Левой половине
    // Mode = 3 Строб + Чувствительность на нем в Правой половине
    // Mode = 4 Строб + Чувствительность на нем по центру

    var
      X: Integer;
      TH: Integer;
      Len: Integer;
      DX: Integer;
      Y1: Integer;
      Y2: Integer;
      F1: Boolean;
      F2: Boolean;

    begin
      // Sens:= EvalChannel;
      case Mode of
       1: begin
            X := (WorkRect.Left + WorkRect.Right) div 2;
            DX := (WorkRect.Right - WorkRect.Left) div 6;
            if Flg then X := X { - DX } else X := X { + DX } ;
          end;
       2: begin
            if Flg then X := (WorkRect.Left + WorkRect.Right) div 2 - (WorkRect.Right - WorkRect.Left) div 4
                   else X := (WorkRect.Left + WorkRect.Right) div 2 + (WorkRect.Right - WorkRect.Left) div 4;
            DX := (WorkRect.Right - WorkRect.Left) div 6;
          end;
       3: begin
            if Flg then X := (WorkRect.Left + WorkRect.Right) div 2 + (WorkRect.Right - WorkRect.Left) div 4
                   else X := (WorkRect.Left + WorkRect.Right) div 2 - (WorkRect.Right - WorkRect.Left) div 4;
            DX := (WorkRect.Right - WorkRect.Left) div 6;
          end;
       4: begin
            X := (WorkRect.Left + WorkRect.Right) div 2;
            DX := (WorkRect.Right - WorkRect.Left) div 6;
            if Flg then X := X - DX
                   else X := X + DX;
          end;
      end;

      if not Config.InvertDelayAxis then
      begin
        Y1 := WorkRect.Bottom - Abs(WorkRect.Top - WorkRect.Bottom) * (Delay1 + FDatSrc.Config.GetScanChByEvalChNum(EvalChannel).BScanShift - FDatSrc.Config.GetScanChByEvalChNum(EvalChannel).BScanGateMin) div FDatSrc.Config.GetScanChByEvalChNum(EvalChannel).BScanDuration;
        Y2 := WorkRect.Bottom - Abs(WorkRect.Top - WorkRect.Bottom) * (Delay2 + FDatSrc.Config.GetScanChByEvalChNum(EvalChannel).BScanShift - FDatSrc.Config.GetScanChByEvalChNum(EvalChannel).BScanGateMin) div FDatSrc.Config.GetScanChByEvalChNum(EvalChannel).BScanDuration;
      end
      else
      begin
        Y1 := WorkRect.Top + Abs(WorkRect.Top - WorkRect.Bottom) * (Delay1 - FDatSrc.Config.GetScanChByEvalChNum(EvalChannel).BScanGateMin) div FDatSrc.Config.GetScanChByEvalChNum(EvalChannel).BScanDuration;
        Y2 := WorkRect.Top + Abs(WorkRect.Top - WorkRect.Bottom) * (Delay2 - FDatSrc.Config.GetScanChByEvalChNum(EvalChannel).BScanGateMin) div FDatSrc.Config.GetScanChByEvalChNum(EvalChannel).BScanDuration;
      end;

      F1 := True;
      F2 := True;
      if Y1 < WorkRect.Top then
      begin
        Y1 := WorkRect.Top;
        F1 := False;
      end;
      if Y1 > WorkRect.Bottom then
      begin
        Y1 := WorkRect.Bottom;
        F1 := False;
      end;
      if Y2 < WorkRect.Top then
      begin
        Y2 := WorkRect.Top;
        F2 := False;
      end;
      if Y2 > WorkRect.Bottom then
      begin
        Y2 := WorkRect.Bottom;
        F2 := False;
      end;

      TH := TextHeight(0, IntToStr(Sens)) div 2;

      if Abs(WorkRect.Bottom - Max(Y1, Y2)) > TH + 8 then
      begin
        VLine(X, Min(Y1, Y2), Abs(Y2 - Y1), 2, Color);
        if F1 then HLine(X - DX, Y1, DX * 2 + 2, 2, Color);
        if F2 then HLine(X - DX, Y2, DX * 2 + 2, 2, Color);
        TextOut(0, X, Max(Y1, Y2) + 4, 4, ' ' + IntToStr(Sens) + ' ', True)
      end
      else if Abs(WorkRect.Top - Min(Y1, Y2)) > TH + 8 then
      begin
        VLine(X, Min(Y1, Y2), Abs(Y2 - Y1), 2, Color);
        if F1 then HLine(X - DX, Y1, DX * 2 + 2, 2, Color);
        if F2 then HLine(X - DX, Y2, DX * 2 + 2, 2, Color);
        TextOut(0, X, Min(Y1, Y2) - 4, 5, ' ' + IntToStr(Sens) + ' ', True)
      end
      else // Чувствительность по центру
      begin
        TH := TextHeight(0, IntToStr(Sens));
        Len := Abs(Y2 - Y1);
        VLine(X, Min(Y1, Y2), (Len - TH) div 2, 2, Color);
        VLine(X, Min(Y1, Y2) + (Len - TH) div 2 + TH + 2, (Len - TH) div 2, 2, Color);
        if F1 then HLine(X - DX, Y1, DX * 2 + 2, 2, Color);
        if F2 then HLine(X - DX, Y2, DX * 2 + 2, 2, Color);
        TextOut(0, X, (Y1 + Y2) div 2, 1, ' ' + IntToStr(Sens) + ' ', True);
      end;

      {
        case Mode of
        1: if Flg then TextOut(0, X + DX div 2, (Y1 + Y2) div 2, 2, ' ' + IntToStr(Sens) + ' ', True)
        else TextOut(0, X - DX div 2, (Y1 + Y2) div 2, 3, ' ' + IntToStr(Sens) + ' ', True);
        2: begin
        TH:= TextHeight(0, 'w') div 2;
        TextOut(0, X, (Y1 + Y2) div 2 - TH + 1, 1, IntToStr(Sens), True);
        TextOut(0, X, (Y1 + Y2) div 2 + TH - 1, 1, Text, True);
        end;
        3: begin
        TH:= TextHeight(0, 'w') div 2;
        TextOut(0, X, (Y1 + Y2) div 2 - TH + 1, 1, IntToStr(Sens), True);
        TextOut(0, X, (Y1 + Y2) div 2 + TH - 1, 1, Text, True);
        end;
        end;
        }
      {
        VLine(X - 3, Min(Y1, Y2), Abs(Y2 - Y1), 8, BGColor);
        if F1 then HLine(X - DX - 3, Y1 - 1, DX * 2 + 2 + 6, 3, BGColor);
        if F2 then HLine(X - DX - 3, Y2 - 1, DX * 2 + 2 + 6, 3, BGColor);
        }
      {
        if Sens2 <> - 100 then
        begin
        if Flg then
        begin
        TextOut(0, X + DX div 2, (Y1 + Y2) div 2 - 10, 2, ' ' + IntToStr(Sens), True);
        TextOut(0, X + DX div 2, (Y1 + Y2) div 2 + 10, 2, ' ' + IntToStr(Sens2), True);
        end
        else
        begin
        TextOut(0, X - DX div 2, (Y1 + Y2) div 2 - 10, 3, ' ' + IntToStr(Sens), True);
        TextOut(0, X - DX div 2, (Y1 + Y2) div 2 + 10, 3, ' ' + IntToStr(Sens2), True);
        end;
        end
        else }

    end;

  var
    R: TRail;
    BScanRtIdx: Integer;
    Mode: Integer;
    Channel: Integer;
    EvalChNum: Integer;

  begin
    if not NowInit then
      Exit;

    FDatSrc.GetParamFirst(FCenterDisCoord, Params { , CoordParams, Time } );

    for R := FDatSrc.Config.MinRail to FDatSrc.Config.MaxRail do
      for I := 0 to FDatSrc.Config.EvalChannelCount - 1 do
      begin
        Channel := FDatSrc.Config.EvalChannelByIdx[I].ScanChNum;
        BScanRtIdx := FScanChNumToBScanLine[R, Channel];
        if BScanRtIdx <> -1 then
        begin
          // if FDatSrc.Config.EvalChannelByIdx[I].InfoSide = isLeft then

          if FDatSrc.Config.EvalChannels2[FDatSrc.Config.ChannelIDList[R, FDatSrc.Config.EvalChannelByIdx[I].Number]].InfoSide = isLeft then
          // if FDatSrc.Config.EvalChannelByIdx[I].InfoSide = isLeft then
          begin
            Color := FDispCols.NaezdCh[1];
            WorkRect := FBScanLines[BScanRtIdx].ChGateRt[rLeft];
            // case FDatSrc.Config.EvalChannelByIdx[I].InfoPos of
            case FDatSrc.Config.EvalChannels2[FDatSrc.Config.ChannelIDList[R, FDatSrc.Config.EvalChannelByIdx[I].Number]].InfoPos of
              ipLeft:   Mode := 2;
              ipCentre: Mode := 1;
              ipRight:  Mode := 3;
            end;
          end
          else
          begin
            Color := FDispCols.OtezdCh[1];
            WorkRect := FBScanLines[BScanRtIdx].ChGateRt[rRight];
            // case FDatSrc.Config.EvalChannelByIdx[I].InfoPos of
            case FDatSrc.Config.EvalChannels2[FDatSrc.Config.ChannelIDList[R, FDatSrc.Config.EvalChannelByIdx[I].Number]].InfoPos of

              ipLeft:   Mode := 3;
              ipCentre: Mode := 1;
              ipRight:  Mode := 2;
            end;
          end;

          EvalChNum := FDatSrc.Config.EvalChannelByIdx[I].Number;

          Color := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, EvalChNum], 15];

          // if FDatSrc.isEGOUSW and (FDatSrc.Header.ControlDirection = cd_Head_B_Tail_A) then
          if FDatSrc.isEGOUSW_BHead then
          begin
            if EvalChNum = 0 then  Color := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 1], 15];
            if EvalChNum = 1 then  Color := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 0], 15];
            if FDatSrc.isVMTUS_Scheme_1_or_2 then
            begin
              if EvalChNum = 10 then Color := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 11], 15];
              if EvalChNum = 11 then Color := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 10], 15];
            end;
          end;
          DrawStrob_(WorkRect, EvalChNum, Params.Par[R, EvalChNum].StStr, Params.Par[R, EvalChNum].EndStr, Params.Par[R, EvalChNum].Ku, Color, FDispCols.Fon, True, Mode, '');
          // Params.StStr[Rail, Channel], Params.EdStr[Rail, Channel], Params.Sens[Rail, Channel], Color, Odd(Channel));
        end;
      end;
  end;

var
  // XX, YY: Integer;
  State: TCrdState;
  S: string;
  EC: TEvalChannelItem;
  SC: TScanChannelItem;
  Rt: TRect;
  Par: TMiasParams;
  Flag: Boolean;
  q: TRect;
  Color_: TColor;
  ChNum: Integer;
  ChIdx: Integer;
  Coeff: Integer;

begin
  if Length(FBScanLines) = 0 then Exit;

  // ST:= GetTickCount;

  { XX:= 100;  YY:= 100;  TextOut(1, XX, YY, 0, 'MODE(0) 123.ABCD', False);  FillRect(XX - 2, YY - 2, XX + 2, YY + 2, GetDDColor(clRed));
    XX:= 400;  YY:= 100;  TextOut(1, XX, YY, 1, 'MODE(1) 123.ABCD', False);  FillRect(XX - 2, YY - 2, XX + 2, YY + 2, GetDDColor(clRed));
    XX:= 100;  YY:= 400;  TextOut(1, XX, YY, 2, 'MODE(2) 123.ABCD', False);  FillRect(XX - 2, YY - 2, XX + 2, YY + 2, GetDDColor(clRed));
    XX:= 400;  YY:= 400;  TextOut(1, XX, YY, 3, 'MODE(3) 123.ABCD', False);  FillRect(XX - 2, YY - 2, XX + 2, YY + 2, GetDDColor(clRed));
    XX:= 800;  YY:= 100;  TextOut(1, XX, YY, 4, 'MODE(4) 123.ABCD', False);  FillRect(XX - 2, YY - 2, XX + 2, YY + 2, GetDDColor(clRed));
    XX:= 800;  YY:= 400;  TextOut(1, XX, YY, 5, 'MODE(5) 123.ABCD', False);  FillRect(XX - 2, YY - 2, XX + 2, YY + 2, GetDDColor(clRed));
    exit; }

  if not Assigned(FDatSrc) then Exit;
  if FDatSrc.EventCount = 0 then Exit;
  if FDM.Zoom = 0 then FDM.Zoom := 10;

  for I := 0 to High(FBScanLines) do // Отрисовка границ и очистка зон
  begin
    DrawRect(StepOut(FBScanLines[I].ChGateRt[rLeft]), 1, FDispCols.Border);
    // Стробы
    DrawRect(StepOut(FBScanLines[I].ChGateRt[rRight]), 1, FDispCols.Border);
    FillRect_(FBScanLines[I].ChGateRt[rLeft], FDispCols.Fon);
    FillRect_(FBScanLines[I].ChGateRt[rRight], FDispCols.Fon);

    DrawRect(StepOut(FBScanLines[I].BScanRt), 1, FDispCols.Border);
    // Зона В-развертки
  end;

  FillRect_(FFCoordRect, FDispCols.Fon); // Зоны вывода линейки координат
  if DisplayMode_ShowChGate { and not HeightToSmall } then //
  begin
    FillRect_(FChGateRect[rLeft], FDispCols.Fon);
    FillRect_(FChGateRect[rRight], FDispCols.Fon);
  end;

  if DisplayMode_ShowChGate then
  begin
    DrawRect(StepOut(FChGateRect[rLeft]), 1, FDispCols.Border);
    DrawRect(StepOut(FChGateRect[rRight]), 1, FDispCols.Border);

    q := StepOut(FFCoordRect);
    DrawRect(q, 1, FDispCols.Border);
  end;

  /// /////////////////////////////////////////////

  if DisplayMode_ShowChInfo and not HeightToSmall then
  begin
    for I := 0 to High(FBScanLines) do // Величины
    begin
      R := FBScanLines[I].Rail;
      for K := 0 to 3 do
      begin
        Rt := FBScanLines[I].ChInfoRt[K];
        if Odd(K) then FillRect_(Rt, FDispCols.Fon)
                  else FillRect_(Rt, FDispCols.Ruler);
        DrawRect(StepOut(Rt), 1, FDispCols.Border);
      end;

      if Length(FBScanLines[I].EvalChNumList) <> 0 then H := (FBScanLines[I].ChInfoRt[0].Bottom - FBScanLines[I].ChInfoRt[0].Top) / Length(FBScanLines[I].EvalChNumList)
                                                   else H := (FBScanLines[I].ChInfoRt[0].Bottom - FBScanLines[I].ChInfoRt[0].Top);
      for J := 0 to High(FBScanLines[I].EvalChNumList) do
      begin
        EC := FDatSrc.Config.EvalChannelByNum[FBScanLines[I].EvalChNumList[J]];
        SC := FDatSrc.Config.ScanChannelByNum[EC.ScanChNum];
        for K := 0 to 3 do
        begin
          case K of
            0:
              begin { Направление }

                if not FDatSrc.isEGOUSW then
                begin
                  if SC.EnterAngle > 0 then S := LangTable.Caption['Common:ZoomInChannel_SHORT'];
                  if SC.EnterAngle < 0 then S := LangTable.Caption['Common:ZoomOutChannel_SHORT'];
                  if SC.EnterAngle = 0 then S := '-' + GetSurfaceText(FBScanLines[I].Rail, SC.Gran);
                end
                else
                if FDatSrc.isVMTUS_Scheme_1_or_2 then
                begin
                  if EGOUSW_WP_Data[SC.BS_Ch_File_Number] = 0 then S := 'A'
                                                              else S := 'B';
                end
                else
                if FDatSrc.isVMTUS_Scheme_4 then
                begin
                  if EGOUSW_BigWP_Data[SC.BS_Ch_File_Number] = 0 then S := 'A'
                                                                 else S := 'B';
                end;

              end;
            1:
              begin { Алфа/Гамма } // %d
                if SC.Gran <> _None { SC.TurnAngle <> 0 } then
                begin

                  if GetSurfaceText(FBScanLines[I].Rail, SC.Gran) = '' then S := Format('%d/%d', [Abs(SC.EnterAngle), Abs(SC.TurnAngle)])
                                                                       else S := Format('%d/%s', [Abs(SC.EnterAngle), GetSurfaceText(FBScanLines[I].Rail, SC.Gran)]);
                end
                else S := Format('%d', [Abs(SC.EnterAngle)]);

             //   S:= IntToStr(SC.Number {EC.ScanChNum}) + ' ' + IntToStr(FBScanLines[I].EvalChNumList[J]);

              end;
            2: S := LangTable.Caption['Common:' + GetInspMethodTextId(EC.Method, False)]; { Метод }
            3: S := LangTable.Caption['Common:' + GetInspZoneTextId(EC.Zone, False)]; { Зона }
          end;

          Rt := FBScanLines[I].ChInfoRt[K];
          Y := Rt.Top;
          Rt.Top := Round(Y + J * H);
          Rt.Bottom := Round(Y + J * H + H);

          Color_ := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, FBScanLines[I].EvalChNumList[J]], 15];

          if FDatSrc.isEGOUSW_BHead then
          begin
            if FBScanLines[I].EvalChNumList[J] = 0 then Color_ := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 1], 15];
            if FBScanLines[I].EvalChNumList[J] = 1 then Color_ := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 0], 15];
            if FDatSrc.isVMTUS_Scheme_1_or_2 then
            begin
              if FBScanLines[I].EvalChNumList[J] = 10 then Color_ := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 11], 15];
              if FBScanLines[I].EvalChNumList[J] = 11 then Color_ := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 10], 15]
            end;
          end;

          // TextRectColor(0, Rt, 0, S, True, ChNumColors[R, FBScanLines[I].EvalChNumList[J], 15]);
          // TextRectColor(0, Rt, 0, S, True, EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, FBScanLines[I].EvalChNumList[J]], 15]);
          TextRectColor(0, Rt, 0, S, True, Color_);

          // TextRect(0, Rt, 0, S, True);
        end;
      end;
    end;

    for K := 0 to 3 do // Заголовок таблицы
    begin
      case K of
        { Направление } 0: if FDatSrc.isEGOUSW then S := LangTable.Caption['Common:ChannelParams_SHORT:Direction_B']
                                               else S := LangTable.Caption['Common:ChannelParams_SHORT:Direction_A'];
         { Алфа/Гамма } 1: S := LangTable.Caption['Common:ChannelParams_SHORT:Angles'];
              { Метод } 2: S := LangTable.Caption['Common:ChannelParams_SHORT:Method'];
               { Зона } 3: S := LangTable.Caption['Common:ChannelParams_SHORT:Zone'];
      end;
      Rt := FChInfoBox.GetRect(K, CoordRectRowIdx);
      if Odd(K) then FillRect_(Rt, FDispCols.Fon)
                else FillRect_(Rt, FDispCols.Ruler);
      TextRect(1, Rt, 0, S, True);
      DrawRect(Rt, 1, FDispCols.Border);
    end;
  end;

  if DisplayMode_ShowChSettings and not HeightToSmall then
  begin
    FDatSrc.GetParamFirst(FCenterDisCoord, Par); // Величины
    for I := 0 to High(FBScanLines) do
    begin
      R := FBScanLines[I].Rail;
      for K := 0 to 3 do
      begin
        Rt := FBScanLines[I].ChSettRt[K];
        if Odd(K) then FillRect_(Rt, FDispCols.Fon)
                  else FillRect_(Rt, FDispCols.Ruler);
        DrawRect(StepOut(Rt), 1, FDispCols.Border);
      end;

      // if Length(FBScanLines[I].EvalChNumList) <> 0 then H:= (FBScanLines[I].ChInfoRt[0].Bottom - FBScanLines[I].ChInfoRt[0].Top) / Length(FBScanLines[I].EvalChNumList)
      // else H:= (FBScanLines[I].ChInfoRt[0].Bottom - FBScanLines[I].ChInfoRt[0].Top);
      if Length(FBScanLines[I].EvalChNumList) <> 0 then
        H := (FBScanLines[I].ChSettRt[0].Bottom - FBScanLines[I].ChSettRt[0].Top) / Length(FBScanLines[I].EvalChNumList)
      else
        H := (FBScanLines[I].ChSettRt[0].Bottom - FBScanLines[I].ChSettRt[0].Top);
      // H:= (FBScanLines[I].ChSettRt[0].Bottom - FBScanLines[I].ChSettRt[0].Top) / Length(FBScanLines[I].EvalChNumList);
      for J := 0 to High(FBScanLines[I].EvalChNumList) do
      begin
        EC := FDatSrc.Config.EvalChannelByNum[FBScanLines[I].EvalChNumList[J]];
        SC := FDatSrc.Config.ScanChannelByNum[EC.ScanChNum];
        for K := 0 to 3 do
        begin
          case K of
            0: S := IntToStr(Par.Par[FBScanLines[I].Rail, FBScanLines[I].EvalChNumList[J]].Ku); { Ку }
            1: S := IntToStr(Par.Par[FBScanLines[I].Rail, FBScanLines[I].EvalChNumList[J]].Att); { Атт }
            2: S := IntToStr(Par.Par[FBScanLines[I].Rail, FBScanLines[I].EvalChNumList[J]].TVG); { ВРЧ }
            3: S := Format('%3.1f', [Par.Par[FBScanLines[I].Rail, FBScanLines[I].EvalChNumList[J]].PrismDelay / 10]); { 2Тп }
          end;

          Rt := FBScanLines[I].ChSettRt[K];
          Y := Rt.Top;
          Rt.Top := Round(Y + J * H);
          Rt.Bottom := Round(Y + J * H + H);
          // TextRect(0, Rt, 0, S, True);
          // TextRectColor(0, Rt, 0, S, True, ChNumColors[R, FBScanLines[I].EvalChNumList[J], 15]);

          Color_ := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, FBScanLines[I].EvalChNumList[J]], 15];

          if FDatSrc.isEGOUSW_BHead then
          begin
            if FBScanLines[I].EvalChNumList[J] = 0 then Color_ := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 1], 15];
            if FBScanLines[I].EvalChNumList[J] = 1 then Color_ := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 0], 15];
            if FDatSrc.isVMTUS_Scheme_1_or_2 then
            begin
              if FBScanLines[I].EvalChNumList[J] = 10 then Color_ := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 11], 15];
              if FBScanLines[I].EvalChNumList[J] = 11 then Color_ := EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, 10], 15];
            end;
          end;

          TextRectColor(0, Rt, 0, S, True, Color_ { EvalChNumColors2[FDatSrc.Config.ChannelIDList[R, FBScanLines[I].EvalChNumList[J]], 15]});

        end;
      end;
    end;

    for K := 0 to 3 do // Заголовок таблицы
    begin
      case K of
        {  Ку } 0: S := LangTable.Caption['Common:ChannelParams_SHORT:Ku'];
        { Атт } 1: S := LangTable.Caption['Common:ChannelParams_SHORT:Att'];
        { ВРЧ } 2: S := LangTable.Caption['Common:ChannelParams_SHORT:TVG'];
        { 2Тп } 3: S := LangTable.Caption['Common:ChannelParams_SHORT:PrismDelay'];
      end;
      Rt := FChSettBox.GetRect(K, CoordRectRowIdx);
      if Odd(K) then FillRect_(Rt, FDispCols.Fon)
                else FillRect_(Rt, FDispCols.Ruler);
      TextRect(1, Rt, 0, S, True);
      DrawRect(Rt, 1, FDispCols.Border);
    end;

  end;
    {
    if FDatSrc.HCCount <> 0 then
    begin
    with FBScanLines[1].BScanRt do
    begin
    //      DrawRect(GetRect()
    //      FDispCols.Ruler

    HLine(Left, (Top + Bottom) div 2, Right  - Left, 1, FDispCols.Ruler);
    MoveTo(Left, (Top + Bottom) div 2);
    LineTo(Right, (Top + Bottom) div 2);

    //      FDatSrc.Header.HCThreshold

    end;

    end;
     }
  /// ///////////////////////////////////////////////////////////////
  (*
    for K := 0 to 7 do
    begin
    Rt:= FInfoBox.GetRect(K, 0);
    Rt.Top:= FFCoordRect0.Top;
    Rt.Bottom:= FFCoordRect0.Bottom;
    if Odd(K) then FillRect_(Rt, FDispCols.Fon)
    else FillRect_(Rt, FDispCols.Ruler);
    end;

    //////////////////////////////////////////////////////////////////

    for K := 0 to 7 do
    begin
    case K of
    { Направление } 0: S:= LangTable.Caption['Common:ChannelParams_SHORT:Direction'];
    { Алфа/Гамма  } 1: S:= LangTable.Caption['Common:ChannelParams_SHORT:Angles'];
    { Метод       } 2: S:= LangTable.Caption['Common:ChannelParams_SHORT:Method'];
    { Зона        } 3: S:= LangTable.Caption['Common:ChannelParams_SHORT:Zone'];
    { Ку          } 4: S:= LangTable.Caption['Common:ChannelParams_SHORT:Ku'];
    { Атт         } 5: S:= LangTable.Caption['Common:ChannelParams_SHORT:Att'];
    { ВРЧ         } 6: S:= LangTable.Caption['Common:ChannelParams_SHORT:TVG'];
    { 2Тп         } 7: S:= LangTable.Caption['Common:ChannelParams_SHORT:PrismDelay'];
    end;
    Rt:= FInfoBox.GetRect(K, 0);
    Rt.Top:= FFCoordRect0.Top;
    Rt.Bottom:= FFCoordRect0.Bottom;
    TextRect(1, Rt, 0, S, True);
    end;
    *)
  /// ///////////////////////////////////////////////////////////////

  LastDrawWidth := FBScanLines[0].BScanRt.Right - FBScanLines[0].BScanRt.Left + 1;
  DrawWidth := LastDrawWidth * FDM.Zoom div FDatSrc.Header.ScanStep;
  FStartDisCoord := FCenterDisCoord - DrawWidth div 2;
  FEndDisCoord := FCenterDisCoord + DrawWidth div 2;

  DrawWidth_ := DrawWidth * FDatSrc.Header.ScanStep / 100000;

  if DisplayMode_ShowChGate { and not HeightToSmall } then
    { if not FDM.RailView then } DrawStrobe;

  CreateImage(FStartDisCoord, FBScanLines[0].BScanRt.Left, FBScanLines[0].BScanRt.Right);

//  {$IFDEF SHOW_REDUCEPOS}
  if MainForm.SHOW_REDUCEPOS.Checked and (FDM.Reduction = 1) then
    for I := 0 to High(FBScanLines) do // Величины
    begin

    ///////////////////////////////////////////////////

      for ChIdx:= 0 to High(FBScanLines[I].ScanChNumList) do
      begin
        ChNum := Abs(FBScanLines[I].ScanChNumList[ChIdx]);

         if FDatSrc.Config.ReductionConst then
         begin
           Coeff:= 1 - 2 * Ord(FDatSrc.isEGOUSW_BHead);
           case FDM.Reduction of
  //           0: DX := 0;
             1: DX := Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * FDatSrc.CurEcho[R, ChNum].Delay[J]));
  //           2: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[FDM.Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[FDM.Reduction, ChNum] * FDatSrc.CurEcho[R, ChNum].Delay[J]) / (FDM.Zoom / 100));
           end;
         end
         else
         case FDM.Reduction of
  //         0: DX := 0;
           1: DX := Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead) + FDatSrc.Config.ScanChannelByNum[ChNum].Delta));
  //         2: DX := (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.CurEcho[R, ChNum].Delay[J], FDatSrc.isEGOUSW_BHead) { + FDatSrc.Config.ScanChannelByNum[ChNum].Delta}) / (FDM.Zoom / 100));
         end;
         Rt.Left:= FBScanLines[I].BScanRt.Left;
         Rt.Top:= FBScanLines[I].BScanRt.Top + ChIdx * 25;
         Rt.Right:= Rt.Left + 200;
         Rt.Bottom:= Rt.Top + 25;
         S:= Format('Ch: %d; DX: %d', [ChNum, DX]);
         TextRectColor(0, Rt, 0, S, True, Color_);
      end;
    end;
//  {$ENDIF}

  ///////////////////////////////////////////////


  { : Integer;                    // Выделение пачки
    FDelay1: Integer;
    FDelay2: Integer;
    FCh1: Integer;
    FCh2: Integer;
    FSelChIdx: Integer;
    FRail: TRail;
    FEcho: array [1..2, -200..200, 0..255] of Integer;

    for I:= 0 to High(FPBPoints) do
    begin
    XX:= GetScrCrd(FCentCrd + FPBPoints[I].X, State);
    if FSelChIdx = 1 then YY:= GetY(FRail, FCh1, FPBPoints[I].Y, State)
    else YY:= GetY(FRail, FCh2, FPBPoints[I].Y, State);
    FillRect(XX, YY + 3, XX + 1, YY + 3 + 1, GetDDColor(clBlack));
    FillRect(XX, YY - 3, XX + 1, YY - 3 + 1, GetDDColor(clBlack));
    end;

    }

  (*
    for R:= rLeft to rRight do
    for Ch:= 1 to 7 do
    if FChIdx[R, Ch] <> - 1 then
    with FBScanLines[FChIdx[R, Ch]] do
    begin
    TextOut(0, 0, DrawRect.Top, 0, Format('Точек на 1 шаг: %3.3f', [FBScanLines[FChIdx[R, Ch]].XStep]), False);
    TextOut(0, 0, DrawRect.Top + 15, 0, Format('Точек на 1 мкс: %3.3f', [FBScanLines[FChIdx[R, Ch]].YStep]), False);
    end;
    *)

  if DisplayMode_ShowChGate then
  begin
    Dec(q.Left);
    q.Bottom := (q.Bottom + q.Top) div 2;
    DrawRect(q, 1, FDispCols.Border);
  end;


  // DrawRect(Rect(0, 0, 100, 100), 10, clRed);

  Flip;
  {
    FLastDrawDisMode:= DisplayMode;
    FLastDrawSize.CX:= Width;
    FLastDrawSize.CY:= Height;
    }

  if Assigned(FScrollBar) then
  begin
    FScrollBar.PageSize := Round((FBScanLines[0].BScanRt.Right - FBScanLines[0].BScanRt.Left + 1) * (FDM.Zoom / 100) / (FDatSrc.Header.ScanStep / 100));
    if FScrollBar.PageSize = 0 then
      FScrollBar.PageSize := 1;
    FScrollBar.Max := Min(MaxInt, FDatSrc.MaxDisCoord + FScrollBar.PageSize);
    FScrollBar.SmallChange := Max(1, FScrollBar.PageSize div 100);
    FScrollBar.LargeChange := Min(32766, Max(1, FScrollBar.PageSize div 4));
  end;

  // Tmp1.BBUFER.Canvas.MoveTo(0, 0);
  // Tmp1.BBUFER.Canvas.LineTo(110, 110);

  // MainForm.Caption:= Format('%d ms', [GetTickCount - ST]);
end;

procedure TAvk11Display.ShowMark(MarkIdx, ShowPos: Integer);
var
  I, J: Integer;
  Rt: TRect;
  Cnt: Integer;
  Crd: Integer;
  Flg: Boolean;
  OnScrSize: Integer;
  DisCrd: Integer;
  D1: Integer;
  FHName1, S: string;

begin
  (*
    Crd:= (ViewZones[MarkIdx].Bounds[1, 1] + ViewZones[MarkIdx].Bounds[1, 2]) div 2;

    //  if MainForm.TBItem23.Checked then
    begin
    Cnt:= 3;
    repeat
    Flg:= True;
    OnScrSize:= Abs(ViewZones[MarkIdx].Bounds[1, 2] - ViewZones[MarkIdx].Bounds[1, 1]) * FDatSrc.Header.ScanStep div FDM.Zoom;
    if OnScrSize / LastDrawWidth > 0.75 then
    begin
    Dec(Cnt);
    Flg:= False;

    for I:= 1 to High(ZoomList) do
    if ZoomList[I - 1] = FDM.Zoom then
    begin
    FDM.Zoom:= ZoomList[I];
    Break;
    end;
    ReEchoSize;
    end;

    if OnScrSize / LastDrawWidth < 0.1 then
    begin
    Dec(Cnt);
    Flg:= False;
    for I:= 0 to High(ZoomList) - 1 do
    if ZoomList[I + 1] = FDM.Zoom then
    begin
    FDM.Zoom:= ZoomList[I];
    Break;
    end;
    ReEchoSize;
    end;
    until Flg or (Cnt = 0);
    end;

    //  RecInfoForm.visible:= MainForm.TBItem29.Checked and (ViewZones[MarkIdx].ID in [rotBJHead, rotBJBody, rotBJBottom, rotFreeHead, rotFreeBody,  rotFreeBottom]);


    //  MainForm.Panel3.visible:= RecInfoForm.visible;

    DisCrd:= (ViewZones[MarkIdx].Bounds[1, 2] + ViewZones[MarkIdx].Bounds[1, 1]) div 2;
    //  RecInfoForm.RichEdit1.Clear;
    //  RecInfoForm.RichEdit1.Lines.Add('Координата ');
    //  RecInfoForm.RichEdit1.Lines.Add(RealCoordToStr(DatSrc.DisToRealCoord(DisCrd), 2));
    {  RecInfoForm.RichEdit1.Lines.Add('');
    case ViewZones[MarkIdx].ID of
    rotBJHead    : S:= 'В зоне болтового стыка, в головке';
    rotBJBody    : S:= 'В зоне болтового стыка, в шейке';
    rotBJBottom  : S:= 'В зоне болтового стыка, в подошве';
    rotFreeHead  : S:= 'В основном металле рельса, Осмотреть в головке';
    rotFreeBody  : S:= 'В основном металле рельса, Осмотреть в шейке';
    rotFreeBottom: S:= 'В основном металле рельса, Осмотреть в подошве';
    rotBoltJoint : S:= 'Болтовые стыки';
    rotSwitch    : S:= 'Стрелочные переводы';
    rotUnCtrlZon : S:= 'Непроконтролированные участки';
    end;
    RecInfoForm.RichEdit1.Lines.Add('Объект: ' + S);

    case ViewZones[MarkIdx].ID of
    rotFreeHead: S:= 'Вероятность: ' + IntToStr(ViewZones[MarkIdx].P) + '%';
    rotFreeBody: S:= 'Вероятность: ' + IntToStr(ViewZones[MarkIdx].P) + '%';
    rotFreeBottom: S:= 'Вероятность: ' + IntToStr(ViewZones[MarkIdx].P) + '%';
    rotBJHead: S:= 'Вероятность: ' + IntToStr(ViewZones[MarkIdx].P) + '%';
    rotBJBody: S:= 'Вероятность: ' + IntToStr(ViewZones[MarkIdx].P) + '%';
    rotBJBottom: S:= 'Вероятность: ' + IntToStr(ViewZones[MarkIdx].P) + '%';
    rotUnCtrlZon: S:= Format('Протяженность: %3.1f м', [(ViewZones[MarkIdx].Bounds[1, 2] - ViewZones[MarkIdx].Bounds[1, 1]) * DatSrc.Header.ScanStep / 100000]);
    rotSwitch: S:= '';
    rotBoltJoint: S:= 'Количество БО: ' + IntToStr(Length(ViewZones[MarkIdx].HolePos));
    end;
    }
    //  RecInfoForm.RichEdit1.Lines.Add('Вероятность: ' + IntToStr(ViewZones[MarkIdx].P) + '%');
    //  RecInfoForm.RichEdit1.Lines.Add('');
    if ViewZones[MarkIdx].ID in [rotFreeHead, rotFreeBody, rotFreeBottom, rotBJHead, rotBJBody, rotBJBottom] then
    for J:= 0 to High(ViewZones[MarkIdx].Items) do
    with ViewZones[MarkIdx].Items[J] do
    begin
    D1:= (EdDelay + StDelay) div 2;
    case Ch of
    1: FHName1:= Format('%3.1f', [2.95 * D1]);
    2, 3,
    5: case Round(D1) of
    0..40: FHName1:= Format('%3.1f', [     35 * (D1 -   0) / 40]);
    41..75: FHName1:= Format('%3.1f', [36 - 33 * (D1 -  40) / 35]);
    76..100: FHName1:= Format('%3.1f', [ 3 + 12 * (D1 -  75) / 25]);
    101..135: FHName1:= Format('%3.1f', [15 + 10 * (D1 - 100) / 35]);
    end;
    4: FHName1:= Format('%3.1f', [D1 * 3.26 / 2 * Cos(70 * Pi / 180)]);
    6, 7,
    8, 9: FHName1:= Format('%3.1f', [D1 * 3.26 / 2 * Cos(42 * Pi / 180)]);
    end;
    //        RecInfoForm.RichEdit1.Lines.Add(Format('Пачка: %d', [J + 1]));
    //        RecInfoForm.RichEdit1.Lines.Add(Format('%d. Канал: %d', [J + 1, Ch]));
    //        RecInfoForm.RichEdit1.Lines.Add(Format('    dH: %d мкс', [Abs(EdDelay - StDelay)]));
    //        RecInfoForm.RichEdit1.Lines.Add(Format('    dL: %d мм', [Abs(EdDisCrd - StDisCrd) * DatSrc.Header.ScanStep div 100]));
    //        RecInfoForm.RichEdit1.Lines.Add(Format('     H: %s мм', [FHName1]));
    end;


    //  RecInfoForm.RichEdit1.Refresh;

    case ShowPos of
    - 1: SetLeftCoord(Crd);
    0: SetCenterDisCoord(Crd);
    1: SetRightCoord(Crd);
    end;

    FViewZoneSelIdx:= MarkIdx;
    FViewZoneSel:= True;
    Refresh;
    //  if MainForm.DemoFlag then Sleep(450)
    //                       else Sleep(75);
    FViewZoneSel:= False;
    Refresh;
    *)
end;

procedure TAvk11Display.ChangeScrollBar(Sender: TObject);
begin
  if FScrollBar.Tag <> 0 then
    Exit;
  FCenterDisCoord := FScrollBar.Position;
  if Self.Visible then
    Self.SetFocus;
  Refresh;

  DoAssignList;
end;

procedure TAvk11Display.EasyLeft;
begin
  if not Assigned(FDatSrc) then
    Exit;

  Dec(FCenterDisCoord, (LastDrawWidth * FDM.Zoom div FDatSrc.Header.ScanStep) div 150);
  if FCenterDisCoord < 0 then
    FCenterDisCoord := 0;
  FScrollBar.OnChange := nil;
  FScrollBar.Position := FCenterDisCoord;
  FScrollBar.OnChange := ChangeScrollBar;
  Refresh;

  DoAssignList;
end;

procedure TAvk11Display.EasyRight;
begin
  if not Assigned(FDatSrc) then
    Exit;

  Inc(FCenterDisCoord, (LastDrawWidth * FDM.Zoom div FDatSrc.Header.ScanStep) div 150);
  if FCenterDisCoord > FDatSrc.MaxDisCoord then
    FCenterDisCoord := FDatSrc.MaxDisCoord;
  FScrollBar.OnChange := nil;
  FScrollBar.Position := FCenterDisCoord;
  FScrollBar.OnChange := ChangeScrollBar;
  FCenterDisCoord := FScrollBar.Position;
  Refresh;

  DoAssignList;
end;

procedure TAvk11Display.StepLeft;
begin
  if not Assigned(FDatSrc) then
    Exit;

  Dec(FCenterDisCoord, (LastDrawWidth * FDM.Zoom div FDatSrc.Header.ScanStep) div 4);
  if FCenterDisCoord < 0 then
    FCenterDisCoord := 0;
  FScrollBar.OnChange := nil;
  FScrollBar.Position := FCenterDisCoord;
  FScrollBar.OnChange := ChangeScrollBar;
  Refresh;

  DoAssignList;
end;

procedure TAvk11Display.StepRight;
begin
  if not Assigned(FDatSrc) then
    Exit;

  Inc(FCenterDisCoord, (LastDrawWidth * FDM.Zoom div FDatSrc.Header.ScanStep) div 4);
  if FCenterDisCoord > FDatSrc.MaxDisCoord then
    FCenterDisCoord := FDatSrc.MaxDisCoord;

  FScrollBar.OnChange := nil;
  FScrollBar.Position := FCenterDisCoord;
  FScrollBar.OnChange := ChangeScrollBar;
  Refresh;
  DoAssignList;
end;

procedure TAvk11Display.PageLeft;
begin
  if not Assigned(FDatSrc) then
    Exit;

  Dec(FCenterDisCoord, (LastDrawWidth * FDM.Zoom div FDatSrc.Header.ScanStep));
  if FCenterDisCoord < 0 then
    FCenterDisCoord := 0;
  FScrollBar.OnChange := nil;
  FScrollBar.Position := FCenterDisCoord;
  FScrollBar.OnChange := ChangeScrollBar;
  Refresh;
  DoAssignList;
end;

procedure TAvk11Display.PageRight;
begin
  if not Assigned(FDatSrc) then
    Exit;

  Inc(FCenterDisCoord, (LastDrawWidth * FDM.Zoom div FDatSrc.Header.ScanStep));
  if FCenterDisCoord > FDatSrc.MaxDisCoord then
    FCenterDisCoord := FDatSrc.MaxDisCoord;
  FScrollBar.OnChange := nil;
  FScrollBar.Position := FCenterDisCoord;
  FScrollBar.OnChange := ChangeScrollBar;
  Refresh;
  DoAssignList;
end;

procedure TAvk11Display.NextZoom;
var
  I: Integer;

begin
  for I := 1 to High(ZoomList) do
    if ZoomList[I - 1] = FDM.Zoom then
    begin
      FDM.Zoom := ZoomList[I];
      Break;
    end;
  ReEchoSize;
  Refresh;
  DoAssignList;
end;

procedure TAvk11Display.PrevZoom;
var
  I: Integer;

begin
  for I := 0 to High(ZoomList) - 1 do
    if ZoomList[I + 1] = FDM.Zoom then
    begin
      FDM.Zoom := ZoomList[I];
      Break;
    end;
  ReEchoSize;
  Refresh;
  DoAssignList;
end;

procedure TAvk11Display.MaximizeTape(X, Y: Integer);
var
  I: Integer;
  RTop: RRail;
  RBottom: RRail;

begin

  if ViewMode = vmOneLine then
  begin
    HeightToSmall := False;
    SetViewMode(FSaveViewMode, FSaveViewRail);
    // ShowChSettings:= FSaveViewMode.ShowChSettings;
    // ShowChInfo:= FSaveViewMode.ShowChInfo;
    // FDM.ShowChSettings:= FSaveViewMode.ShowChSettings;
    // FDM.ShowChInfo:= FSaveViewMode.ShowChInfo;

    // ReCalcOutRect;
    // Refresh;
    ApplyDisplayMode;
    ReCalcOutRect;
    Refresh;
    Exit;
  end;

  { if FDatSrc.SideToRail(rLeft) = rLeft then
    begin }
  // RTop:= r_Left;
  // RBottom:= r_Right;
  { end
    else
    begin
    RTop:= rRight;
    RBottom:= rLeft;
    end;
    }

  for I := 0 to High(FBScanLines) do
    if PtInRect(FBScanLines[I].BScanRt, Point(X, Y)) then
    begin
      // FSaveViewMode:= DisplayMode;
      // FDM.ShowChSettings:= False;
      // FDM.ShowChInfo:= False;
      HeightToSmall := True;
      FSaveViewMode := FDM.ViewMode;
      FSaveViewRail := FDM.ViewRail;
      SetViewMode(vmOneLine, FBScanLines[I].Rail, FBScanLines[I].Line);
      // DisplayMode.ViewLine:= FBScanLines[I].Line;
      // DisplayMode.ViewRail:= FBScanLines[I].Rail;
      // SetViewMode(ddOneLine);
      ApplyDisplayMode;
      ReCalcOutRect;
      Refresh;
      Break;
    end;
  (*
    for I:= 0 to High(FBScanRts) do
    if PtInRect(FBScanRts[I].DrawRect, Point(X, Y)) then
    begin
    FSaveViewMode:= FViewMode;
    if FBScanRts[I].Rail = RTop then
    case FBScanRts[I].TapeIdx of
    0: SetViewMode(ddLeftTape1);
    1: SetViewMode(ddLeftTape2);
    2: SetViewMode(ddLeftTape3);
    3: SetViewMode(ddLeftTape4);
    end;

    if FBScanRts[I].Rail = RBottom then
    case FBScanRts[I].TapeIdx of
    0: SetViewMode(ddRightTape1);
    1: SetViewMode(ddRightTape2);
    2: SetViewMode(ddRightTape3);
    3: SetViewMode(ddRightTape4);
    end;

    Refresh;
    Exit;
    end; *)
end;

(*
  function TAvk11Display.GetTapeRect(Rail: TRail; Idx: Integer): TRect;
  const
  ConvArr: array [1..4] of Integer = (2, 4, 6, 1);

  begin

  //FDisplayMode
  {
  if FViewMode in [ddLeftTape1,
  ddLeftTape2,
  ddLeftTape3,
  ddLeftTape4,
  ddRightTape1,
  ddRightTape2,
  ddRightTape3,
  ddRightTape4] then Result:= FBScanLines[0].DrawRect
  else Result:= FBScanLines[FChIdx[Rail, ConvArr[Idx]]].DrawRect;}
  end;
  *)
procedure TAvk11Display.GetMouseRect(Rt: TRect; var DisCoord1, DisCoord2, Ch1Delay1, Ch1Delay2, Ch2Delay1, Ch2Delay2: Integer; var Ch1, Ch2: Integer; var SameRect: Boolean);
var
  I, T1, T2: Integer;
  LT: TMousePosDat;
  RB: TMousePosDat;

begin

  GetMouseData(Rt.Left, Rt.Top, @LT);
  GetMouseData(Rt.Right, Rt.Bottom, @RB);

  DisCoord1 := LT.MouseDisCoord;
  DisCoord2 := RB.MouseDisCoord;

  if (Length(LT.EvalChNumList) <> 0) and (Length(RB.EvalChNumList) <> 0) and LT.Ok and RB.Ok and (LT.Rail = RB.Rail) then
  begin
    SameRect := LT.EvalChNumList[0] = RB.EvalChNumList[0];
    Ch1Delay1 := Round(LT.Delay[0]);
    Ch1Delay2 := Round(RB.Delay[0]);
    Ch2Delay1 := Round(LT.Delay[0]);
    Ch2Delay2 := Round(RB.Delay[0]);
  end
  else
    SameRect := False;


  // DisCoord1:= MouseToDisCoord(Rt.Left, Rt.Top);
  // DisCoord2:= MouseToDisCoord(Rt.Right, Rt.Top);

  // if DisCoord2 = - MaxInt then DisCoord2:= DisCoord1;
  {
    T1:= - 1;
    T2:= - 1;

    for I:= 0 to High(FBScanLines) do
    if //(FBScanLines[I].Channel <> - 10) and
    (PtInRect(FBScanLines[I].BScanRt, Point(Rt.Left, Rt.Top))) then
    begin
    if PtInRect(FBScanLines[I].BScanRt, Point(Rt.Left, Rt.Top)) then
    if T1 = - 1 then T1:= I else T2:= I;
    end;
    }
  {
    for I:= 0 to High(FBScanLines) do
    begin
    if not Odd(I) then
    begin
    if PtInRect(FBScanLines[I].DrawRect, Point(Rt.Left, Rt.Top)) then T1:= I;
    end
    else if PtInRect(FBScanLines[I].DrawRect, Point(Rt.Left, Rt.Top)) then T2:= I;
    end;
    }
  // SameRect:= (T1 <> - 1) and (PtInRect(FBScanLines[T1].BScanRt, Point(Rt.Right, Rt.Bottom)));
  Ch1 := -1;
  Ch2 := -1;
  (*
    if not Config.InvertDelayAxis then
    begin
    if (T1 <> - 1) {and (FBScanLines[T1].Channel >= 0)} then
    with FBScanLines[T1].DrawRect, FDispConf.BScanViewZone[FBScanLines[T1].Channel] do
    begin
    Ch1Delay1:= Round(BegDelay +  (Bottom - Rt.Top) / (Bottom - Top) * (EndDelay - BegDelay));
    Ch1Delay2:= Round(BegDelay +  (Bottom - Rt.Bottom) / (Bottom - Top) * (EndDelay - BegDelay));
    Ch1:= FBScanLines[T1].Channel;
    end;

    if (T1 <> - 1) and (T2 <> - 1) and (FBScanLines[T1].Channel >= 0) and (FBScanLines[T2].Channel >= 0) then
    with FBScanLines[T2].DrawRect, FDispConf.BScanViewZone[FBScanLines[T2].Channel] do
    begin
    Ch2Delay1:= Round(BegDelay +  (Bottom - Rt.Top) / (Bottom - Top) * (EndDelay - BegDelay));
    Ch2Delay2:= Round(BegDelay +  (Bottom - Rt.Bottom) / (Bottom - Top) * (EndDelay - BegDelay));
    Ch2:= FBScanLines[T2].Channel;
    end;
    end
    else
    begin
    if (T1 <> - 1) and (FBScanLines[T1].Channel >= 0) then
    with FBScanLines[T1].DrawRect, FDispConf.BScanViewZone[FBScanLines[T1].Channel] do
    begin
    Ch1Delay1:= Round(BegDelay +  (Rt.Top - Top) / (Bottom - Top) * (EndDelay - BegDelay));
    Ch1Delay2:= Round(BegDelay +  (Rt.Bottom - Top) / (Bottom - Top) * (EndDelay - BegDelay));
    Ch1:= FBScanLines[T1].Channel;
    end;

    if (T1 <> - 1) and (T2 <> - 1) and (FBScanLines[T1].Channel >= 0) and (FBScanLines[T2].Channel >= 0) then
    with FBScanLines[T2].DrawRect, FDispConf.BScanViewZone[FBScanLines[T2].Channel] do
    begin
    Ch2Delay1:= Round(BegDelay +  (Rt.Top - Top) / (Bottom - Top) * (EndDelay - BegDelay));
    Ch2Delay2:= Round(BegDelay +  (Rt.Bottom - Top) / (Bottom - Top) * (EndDelay - BegDelay));
    Ch2:= FBScanLines[T2].Channel;
    end;
    end; *)
end;

function TAvk11Display.EchoTable(StartDisCoord: Integer): Boolean;
label Exit1;

type
  TTwoEchoItem = record
    DelayList: array [1 .. 16] of Integer;
    DelayCount: Integer end;

    TTwoEchoList = array [0 .. 9] of TTwoEchoItem;

  var
    R: TRail;
    Ch, Ch1, Ch2, I, J: Integer;
    Flag: Boolean;
    Color: TColor;
    Ch_: Integer;
    TwoEchoList: TTwoEchoList;
    Len: Integer;
    {
      procedure AddAScan1(ChIdx: Integer; Delay_: Single; Ampl: Integer; Color: TColor);
      begin
      with MousePosDat do
      begin
      SetLength(AScanArr[1], Length(AScanArr[1]) + 1);
      AScanArr[ChIdx, High(AScanArr[1])].Delay:= Delay_;
      AScanArr[ChIdx, High(AScanArr[1])].Ampl:= Ampl;
      AScanArr[ChIdx, High(AScanArr[1])].Color:= Color;
      end;
      end;

      procedure AddAScan1(Delay_: Extended; Ampl: Integer; Color: TColor);
      begin
      with MousePosDat do
      begin
      SetLength(AScanArr[1], Length(AScanArr[1]) + 1);
      AScanArr[1, High(AScanArr[1])].Delay:= Delay_;
      AScanArr[1, High(AScanArr[1])].Ampl:= Ampl;
      AScanArr[1, High(AScanArr[1])].Color:= Color;
      end;
      end;

      procedure AddAScan2(Delay_: Extended; Ampl: Integer; Color: TColor);
      begin
      with MousePosDat do
      begin
      SetLength(AScanArr[2], Length(AScanArr[2]) + 1);
      AScanArr[2, High(AScanArr[2])].Delay:= Delay_;
      AScanArr[2, High(AScanArr[2])].Ampl:= Ampl;
      AScanArr[2, High(AScanArr[2])].Color:= Color;
      end;
      end;
      }
  begin
    Result := True;
    // if Abs(FDatSrc.CurDisCoord - FReqCoord) > 2 then Exit;

    if FDatSrc.CurDisCoord <> FReqCoord then
      Exit;

    {
      Color:= clBlack;

      for I:= 1 to FDatSrc.CurEcho[FSetRail, FSetCh1].Count do
      with FDatSrc.CurEcho[FSetRail, FSetCh1] do
      if FSetCh1 in [0, 1] then
      begin
      if (Delay[I] / FDatSrc.DelayKoeff >= FDatSrc.CurParams.StStr[FSetRail, 0]) and
      (Delay[I] / FDatSrc.DelayKoeff <= FDatSrc.CurParams.EdStr[FSetRail, 0]) then AddAScan1(Delay[I] / FDatSrc.DelayKoeff, Ampl[I], clRed)
      else AddAScan2(Delay[I] / FDatSrc.DelayKoeff, Ampl[I], clBlue);
      end
      else AddAScan1(Delay[I], Ampl[I], clRed);

      for I:= 1 to FDatSrc.CurEcho[FSetRail, FSetCh2].Count do
      with FDatSrc.CurEcho[FSetRail, FSetCh2] do
      if FSetCh2 in [0, 1] then
      begin
      if (Delay[I] / FDatSrc.DelayKoeff >= FDatSrc.CurParams.StStr[FSetRail, 0]) and
      (Delay[I] / FDatSrc.DelayKoeff <= FDatSrc.CurParams.EdStr[FSetRail, 0]) then AddAScan1(Delay[I] / FDatSrc.DelayKoeff, Ampl[I], clRed)
      else AddAScan2(Delay[I] / FDatSrc.DelayKoeff, Ampl[I], clBlue);
      end
      else AddAScan2(Delay[I], Ampl[I], clBlue);
      }
    // if (FSetCh2 <> - 1) and (FDatSrc.CurDisCoord = FReqCoord) then
    // begin

    with MousePosDat do
    begin
      SetLength(CurEchoList, Length(CurEchoList) + 1);
      CurEchoList[ High(CurEchoList)].DisCrd := FDatSrc.CurDisCoord;
      Move(FDatSrc.CurEcho, CurEchoList[ High(CurEchoList)].Echo, SizeOf(TCurEcho));

//      Move(FDatSrc.CurAKState, CurEchoList[ High(CurEchoList)].CurAKState, SizeOf(FDatSrc.CurAKState));
//      Move(FDatSrc.CurAKExists, CurEchoList[ High(CurEchoList)].CurAKExists, SizeOf(FDatSrc.CurAKExists));

    end;

    {
      if (FDatSrc.CurDisCoord = FReqCoord) and (not FSoundOk) then
      with MousePosDat do
      begin
      FSoundOk:= True;

      for R:= rLeft to rRight do
      begin
      for Ch:= 0 to 9 do
      begin
      TwoEchoList[Ch].DelayCount:= 0;
      Sound[R, Ch]:= False;
      end; }
    {
      for Ch1:= 2 to 9 do
      begin
      if Ch1 in [2..7] then Ch2:= Ch1 else Ch2:= Ch1 - 2;
      for I:= 1 to FDatSrc.CurEcho[R, Ch2].Count do
      if (FDatSrc.CurEcho[R, Ch2].Ampl[I] >= 6) and
      (FDatSrc.CurEcho[R, Ch2].Delay[I] >= FDatSrc.CurParams.StStr[R, Ch1]) and
      (FDatSrc.CurEcho[R, Ch2].Delay[I] <= FDatSrc.CurParams.EdStr[R, Ch1]) then
      begin
      Inc(TwoEchoList[Ch1].DelayCount);
      TwoEchoList[Ch1].DelayList[TwoEchoList[Ch1].DelayCount]:= FDatSrc.CurEcho[R, Ch2].Delay[I];
      end;
      end;

      Ch1:= 0;
      Ch2:= 1;
      for I:= 1 to FDatSrc.CurEcho[R, Ch2].Count do
      if (FDatSrc.CurEcho[R, Ch2].Ampl[I] >= 6) and
      (FDatSrc.CurEcho[R, Ch2].Delay[I] / FDatSrc.DelayKoeff >= FDatSrc.CurParams.StStr[R, Ch1]) and
      (FDatSrc.CurEcho[R, Ch2].Delay[I] / FDatSrc.DelayKoeff <= FDatSrc.CurParams.EdStr[R, Ch1]) then
      begin
      Inc(TwoEchoList[Ch1].DelayCount);
      TwoEchoList[Ch1].DelayList[TwoEchoList[Ch1].DelayCount]:= Round(FDatSrc.CurEcho[R, Ch2].Delay[I] / FDatSrc.DelayKoeff);
      end;

      Ch1:= 1;
      Ch2:= 1;
      for I:= 1 to FDatSrc.CurEcho[R, Ch2].Count do
      if (FDatSrc.CurEcho[R, Ch2].Ampl[I] >= 6) and
      (FDatSrc.CurEcho[R, Ch2].Delay[I] / FDatSrc.DelayKoeff >= FDatSrc.CurParams.StStr[R, Ch1]) and
      (FDatSrc.CurEcho[R, Ch2].Delay[I] / FDatSrc.DelayKoeff <= FDatSrc.CurParams.EdStr[R, Ch1]) then
      begin
      Inc(TwoEchoList[Ch1].DelayCount);
      TwoEchoList[Ch1].DelayList[TwoEchoList[Ch1].DelayCount]:= Round(FDatSrc.CurEcho[R, Ch2].Delay[I]  / FDatSrc.DelayKoeff);
      end;

      for Ch:= 0 to 9 do Sound[R, Ch]:= TwoEchoList[Ch].DelayCount <> 0;

      if (Params.HeadPh[R, 0] = True)  and (Params.HeadPh[R, 1] = True)  and
      (Params.HeadPh[R, 2] = True)  and (Params.HeadPh[R, 3] = True)  and
      (Params.HeadPh[R, 4] = False) and (Params.HeadPh[R, 5] = False) and
      (Params.HeadPh[R, 6] = True)  and (Params.HeadPh[R, 7] = True)  and
      (Params.HeadPh[R, 8] = False) and (Params.HeadPh[R, 9] = False) then

      for Ch:= 6 to 7 do
      begin
      for I:= 1 to TwoEchoList[Ch].DelayCount do
      for J:= 1 to TwoEchoList[Ch].DelayCount do
      begin
      Len:= Abs(TwoEchoList[Ch].DelayList[I] - TwoEchoList[Ch].DelayList[J]);
      if (I <> J) and (Len in [4..16]) then
      begin
      Sound[R, Ch]:= True;
      goto Exit1;
      end;
      end;
      Exit1:
      end;

      if FSoundFlag and (R = Rail) then
      SetTone(Sound[R, 1] and Params.HeadPh[R, 1],                                              // 500Hz прерывистый
      (not Sound[R, 0]) and Params.HeadPh[R, 0],                                         // 500Hz
      (Sound[R, 6] and Params.HeadPh[R, 6]) or (Sound[R, 7] and Params.HeadPh[R, 7]) or  // 1000Hz
      (Sound[R, 8] and Params.HeadPh[R, 8]) or (Sound[R, 9] and Params.HeadPh[R, 9]),
      (Sound[R, 2] and Params.HeadPh[R, 2]) or (Sound[R, 3] and Params.HeadPh[R, 3]) or  // 2000Hz
      (Sound[R, 4] and Params.HeadPh[R, 4]) or (Sound[R, 5] and Params.HeadPh[R, 5]));

      end;
      end; }
  end;

  function TAvk11Display.GetMouseData(X, Y: Integer; Dat: PMousePosDat = nil): Boolean;

  var
    S: string;
    I, J, Ch: Integer;

    F: Boolean;
    Channel: string;
    Flag: Boolean;

    Params: TEvalChannelsParams;
    // CoordParams: TCrdParams;
    Time: string;

    Sp: Single;
    EvalChListIdx: Integer;

  begin
    if not Assigned(FDatSrc) then
      Exit;

    if Dat = nil then
      Dat := @MousePosDat;

    Dat^.Ok := False;
    Dat^.X := X;
    Dat^.Y := Y;
    Dat^.MouseDisCoord := MouseToDisCoord(X, Y);
    // SetLength(Dat^.AScanArr[1], 0);
    // SetLength(Dat^.AScanArr[2], 0);
    if Dat^.MouseDisCoord = -MaxInt then
    begin
      Result := False;
      Exit;
    end;
    with Dat^ do
    begin
      Result := True;
      FDatSrc.GetParamFirst(MouseDisCoord, Params);

      FDatSrc.DisToCoordParams(MouseDisCoord, CrdParams);
      Time := FDatSrc.GetTime(MouseDisCoord);
      Speed := FDatSrc.GetSpeed(MouseDisCoord, Sp);

      DebugForm.DebugMemo2.Clear;
      if FDatSrc.GetGPSCoord(MouseDisCoord).State then
        DebugForm.DebugMemo2.Lines.Add('State: OK')
      else
        DebugForm.DebugMemo2.Lines.Add('State: BAD');
      DebugForm.DebugMemo2.Lines.Add(Format('Lat: %3.6f', [FDatSrc.GetGPSCoord(MouseDisCoord).Lat]));
      DebugForm.DebugMemo2.Lines.Add(Format('Lon: %3.6f', [FDatSrc.GetGPSCoord(MouseDisCoord).Lon]));
      DebugForm.DebugMemo2.Lines.Add(Format('Speed: %3.6f', [FDatSrc.GetGPSCoord(MouseDisCoord).Speed]));

      for I := 0 to High(FBScanLines) do
        if PtInRect(FBScanLines[I].BScanRt, Point(X, Y))
        { and (FBScanLines[I].Channel <> - 10) } then
        begin
          Rail := FBScanLines[I].Rail;
          BScanRt := FBScanLines[I].BScanRt;
          ScanChNumList := FBScanLines[I].ScanChNumList;
          EvalChNumList := FBScanLines[I].EvalChNumList;
          Ok := True;

          // Line:= FBScanLines[I];
          // if Ch1_Idx = - 1 then Ch1_Idx:= I
          // else Ch2_Idx:= I;
        end;

      for I := 0 to High(FBScanLines) do // Определение положения зоны вывода для выбранной нити
        if Rail = FBScanLines[I].Rail then
        begin
          RailTop := FBScanLines[I].BScanRt.Top;
          Break;
        end;

      for I := High(FBScanLines) downto 0 do
        if Rail = FBScanLines[I].Rail then
        begin
          RailBottom := FBScanLines[I].BScanRt.Bottom;
          Break;
        end;

      // for I:= 0 to High(Line.EvalChNumList) do
      if not FDM.RailView then
      begin
        for I := 0 to High(ScanChNumList) do
          with BScanRt, FDatSrc.Config.ScanChannelByNum[ScanChNumList[I]] do
          begin
            if Config.InvertDelayAxis then
              Delay[I] := BScanGateMin + (Y - Top) / (Bottom - Top) * BScanDuration
            else
              Delay[I] := BScanGateMin + (Bottom - Y) / (Bottom - Top) * BScanDuration;
            if DelayMultiply = 1 then
              Delay[I] := Round(Delay[I]);

            if (EnterAngle = 0) then
            begin
              Depth[I] := 2.95 * Delay[I];
            end
            else if ((Abs(EnterAngle) = 58) and (Abs(TurnAngle) = 34)) or (Abs(EnterAngle) = 50) then
            begin
              case Round(Delay[I]) of
                0 .. 40:    Depth[I] := 35 * (Delay[I] - 0) / 40;
                41 .. 75:   Depth[I] := 36 - 33 * (Delay[I] - 40) / 35;
                76 .. 100:  Depth[I] := 3 + 12 * (Delay[I] - 75) / 25;
                101 .. 135: Depth[I] := 15 + 10 * (Delay[I] - 100) / 35;
              end;
            end
            else if (TurnAngle = 0) then Depth[I] := Delay[I] * 3.26 / 2 * Cos(Abs(EnterAngle) * Pi / 180);
          end;
      end
      else
      begin
        for I := 0 to High(FBScanLines) do // Определение положения зоны вывода для выбранной нити
          if Rail = FBScanLines[I].Rail then
            with BScanRt do
            begin
              for J := 0 to 3 do
                Delay[J] := -1;
              for J := 0 to 3 do
                Depth[J] := Round(210 * (Y - Dat^.BScanRt.Top) / (Dat^.BScanRt.Bottom - Dat^.BScanRt.Top));
            end;

        { FRailChem[R, ].RailHeight

          for I:= 0 to High(ScanChNumList) do
          with BScanRt, FDatSrc.Config.ScanChannelByNum[ScanChNumList[I]] do }

      end;

      // if FDatSrc.Header.UsedItems[uiWorkRailTypeA] = 1 // Контролируемая нить - для однониточных приборов
      // then Rail:= rLeft
      // else Rail:= rRight;

    end;

    FReqCoord := Dat^.MouseDisCoord;
    SetLength(Dat^.CurEchoList, 0);
    FDatSrc.LoadData(Dat^.MouseDisCoord - 200, Dat^.MouseDisCoord + 200, 0, EchoTable);

    with Dat^ do
    begin
      MirrorShadow := False;
      for EvalChListIdx := 0 to High(EvalChNumList) do
      begin
        // SetLength(EvalChNumList, Length(EvalChNumList) + 1);
        // EvalChNumList[High(EvalChNumList)]:= Number;
        if FDatSrc.Config.EvalChannelByNum[EvalChNumList[EvalChListIdx]].Method = imMirrorShadow then
        begin
          MirrorShadow := True;
          MirrorShadowEvalChNum := EvalChNumList[EvalChListIdx];
          Break;
        end;
      end;
    end;

    // Line.EvalChNumList[I]
    (*
      if (Ch1_Idx <> - 1) and (FBScanLines[Ch1_Idx].Channel <> - 10) then
      begin   {
      with FBScanLines[Ch1_Idx].DrawRect, FDispConf.BScanViewZone[FBScanLines[Ch1_Idx].Channel] do
      if not Config.InvertDelayAxis then D1:= Round(BegDelay +  (Bottom - Y) / (Bottom - Top) * (EndDelay - BegDelay))
      else D1:= Round(BegDelay + (Y - Top) / (Bottom - Top) * (EndDelay - BegDelay));

      if (Ch2_Idx <> - 1) and (Ch2_Idx <> 8) then
      with FBScanLines[Ch2_Idx].DrawRect, FDispConf.BScanViewZone[FBScanLines[Ch2_Idx].Channel] do
      if not Config.InvertDelayAxis then D2:= Round(BegDelay +  (Bottom - Y) / (Bottom - Top) * (EndDelay - BegDelay))
      else D2:= Round(BegDelay +  (Y - Top) / (Bottom - Top) * (EndDelay - BegDelay))
      else D2:= D1;

      if D1 = D2 then
      begin
      if Ch1_Idx in [5, 4] then HH:= Round(1.15 * D1);
      if Ch1_Idx in [7, 6] then HH:= Round(2.95 * D1);
      end;

      Rail:= FBScanLines[Ch1_Idx].Rail;
      if FBScanLines[Ch1_Idx].Channel < FBScanLines[Ch2_Idx].Channel then
      begin
      Ch1_:= FBScanLines[Ch1_Idx].Channel;
      Ch2_:= FBScanLines[Ch2_Idx].Channel;
      end
      else
      begin
      Ch2_:= FBScanLines[Ch1_Idx].Channel;
      Ch1_:= FBScanLines[Ch2_Idx].Channel;
      end;
      }
      FReqCoord:= MouseDisCoord;
      FSetRail:= Rail;
      FSetCh1:= Ch1_;
      FSetCh2:= Ch2_;
      FSoundOk:= False;
      end
      else
      begin
      Result:= False;
      end; }
      end; *)
  end;

  function TAvk11Display.MouseToDisCoord(X, Y: Integer): Integer;

  var
    I: Integer;

  begin
    Result := -MaxInt;
    for I := 0 to High(FBScanLines) do
      if PtInRect(FBScanLines[I].BScanRt, Point(X, Y)) then
      begin
        Result := FStartDisCoord + (FEndDisCoord - FStartDisCoord) * (X - FBScanLines[I].BScanRt.Left) div (FBScanLines[I].BScanRt.Right - FBScanLines[I].BScanRt.Left);
        Exit;
      end;
  end;

  function TAvk11Display.GetAssignCount: Integer;
  begin
    Result := FAssignList.Count;
  end;

  function TAvk11Display.GetAssignItem(Index: Integer): TAvk11Display;
  begin
    Result := TAvk11Display(FAssignList.Items[Index]);
  end;

  procedure TAvk11Display.AssignTo(View: TAvk11Display);
  begin
    if FAssignList.IndexOf(View) = -1 then
      FAssignList.Add(View);
  end;

  procedure TAvk11Display.DeleteAssign(Index: Integer);
  begin
    FAssignList.Delete(Index);
  end;

  function TAvk11Display.TestAssign(View: TAvk11Display): Boolean;
  begin
    Result := FAssignList.IndexOf(View) <> -1;
  end;

  procedure TAvk11Display.DeleteAssign(View: TAvk11Display);

  var
    I: Integer;

  begin
    if Self = nil then
      Exit;
    I := FAssignList.IndexOf(View);
    if I <> -1 then
      FAssignList.Delete(I);
  end;

  procedure TAvk11Display.DoAssignList;

  var
    I: Integer;
    RC: TRealCoord;
    Disp: TAvk11Display;
    SysCrd: Integer;
    DisCrd: Integer;

  begin
    if not Assigned(FDatSrc) then
      Exit;
    if FAssignNow then
      Exit;
    FAssignNow := True;
    RC := FDatSrc.DisToRealCoord(CenterDisCoord);
    for I := 0 to FAssignList.Count - 1 do
    begin
      Disp := TAvk11Display(FAssignList[I]);
      DisCrd := Disp.DatSrc.RealToDisCoord(RC);
      Disp.SetAll(DisCrd, DisplayMode);
      Disp.Refresh;
    end;
    FAssignNow := False;
  end;

  // - Для Внешней отрисовки -----------------------------------------------------

  function TAvk11Display.GetScrCrd(DisCoord: Integer; var State: TCrdState): Integer;
  begin
    Result := FStartScrX + (DisCoord - FStartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
    if (Result < FStartScrX) or (Result > FEndScrX) then
      State := [csOffScreen]
    else
      State := [csOnScreen];
  end;

  function TAvk11Display.GetScrReg(Range: TRange; var State: TCrdState): TMinMax;
  begin
    State := [csOffScreen];
    Result.Min := FStartScrX + (Range.StCrd - FStartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
    Result.Max := FStartScrX + (Range.EdCrd - FStartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom;
    if (Result.Min >= FStartScrX) and (Result.Min <= FEndScrX) and (Result.Max >= FStartScrX) and (Result.Max <= FEndScrX) then
      State := [csOnScreen]
    else
    begin
      if (Result.Min < FStartScrX) and (Result.Max >= FStartScrX) then
      begin
        Result.Min := FStartScrX;
        State := State - [csOffScreen] + [csLeftMoved] + [csMoved];
      end;
      if (Result.Max > FEndScrX) and (Result.Min <= FEndScrX) then
      begin
        Result.Max := FEndScrX;
        State := State - [csOffScreen] + [csRightMoved] + [csMoved];
      end;
      if (Result.Min < FStartScrX) and (Result.Max > FEndScrX) then
      begin
        Result.Min := FStartScrX;
        Result.Max := FEndScrX;
        State := [csMoved];
      end;
    end;
  end;

  function TAvk11Display.GetRailBounds(R: TRail; var State: TCrdState): TMinMax;
  var
    I: Integer;

  begin
    Result.Min:= MaxInt;
    Result.Max:= -MaxInt;
    for I := 0 to High(FBScanLines) do
      with FBScanLines[I] do
        if Rail = R then
        begin
          Result.Min:= Min(Result.Min, BScanRt.Top   );
          Result.Max:= Max(Result.Max, BScanRt.Bottom);
        end;
    if Result.Min = MaxInt then State:= [csOffScreen]
                           else State:= [csOnScreen];

{   if (FChIdx[R, 2] <> - 1) and (FChIdx[R, 1] <> - 1) then
      begin
      Result.Min:= FBScanLines[FChIdx[R, 2]].DrawRect.Top;
      Result.Max:= FBScanLines[FChIdx[R, 1]].DrawRect.Bottom;
      State:= [csOnScreen];
      end else State:= [csOffScreen]; }
  end;

  function TAvk11Display.GetTapeBounds(R: TRail; Tape: Integer; var State: TCrdState): TMinMax;
  var
    I: Integer;
 {
  const
    TapeToCh: array [0 .. 3] of Integer = (2, 4, 6, 1);
}
  begin

    Result.Min:= MaxInt;
    Result.Max:= -MaxInt;
    for I := 0 to High(FBScanLines) do
      with FBScanLines[I] do
        if (Rail = R) and (RailLine = Tape) then
        begin
          Result.Min:= Min(Result.Min, BScanRt.Top   );
          Result.Max:= Max(Result.Max, BScanRt.Bottom);
        end;
{     if FChIdx[R, TapeToCh[Tape]] <> - 1 then
      begin
      State:= [csOnScreen];
      Result.Min:= FBScanLines[FChIdx[R, TapeToCh[Tape]]].DrawRect.Top;
      Result.Max:= FBScanLines[FChIdx[R, TapeToCh[Tape]]].DrawRect.Bottom;
      end else State:= [csOffScreen];}
  end;

  function TAvk11Display.GetY(R: TRail; Ch, Delay: Integer; var State: TCrdState): Integer;
  begin
    if FScanChNumToBScanLine[R, Ch] <> - 1 then
    begin


      State:= [csOnScreen];
      with FBScanLines[FScanChNumToBScanLine[R, Ch]] do

        Result:= BScanRt.Bottom - Abs(BScanRt.Top - BScanRt.Bottom) * (Delay - ScanChBScanGateMin[ScanChToIndex[Ch]]) div ScanChBScanGateLen[ScanChToIndex[Ch]];

    end
    else State:= [csOffScreen];
  end;

  function TAvk11Display.GetCoordLineY(R: TRail): Integer;
  begin
    case R of
       rLeft: Result := FFCoordRect.Top + 8;
      rRight: Result := FFCoordRect.Bottom - 8;
      // r_Both: Result:= (FFCoordRect.Top + FFCoordRect.Bottom) div 2;
    end;
  end;

  function TAvk11Display.EvalChNumToName(Num: Integer): string;

  var
    I, J: Integer;
    EC: TEvalChannelItem;
    SC: TScanChannelItem;

  begin
    for I := 0 to High(FBScanLines) do // Величины
      for J := 0 to High(FBScanLines[I].EvalChNumList) do
        if FBScanLines[I].EvalChNumList[J] = Num then
        begin
          EC := FDatSrc.Config.EvalChannelByNum[FBScanLines[I].EvalChNumList[J]];
          SC := FDatSrc.Config.ScanChannelByNum[EC.ScanChNum];
          // Result:= LangTable.Caption['Common:ChannelParams_SHORT:Direction'] + ': ';
          Result := '';

          if not FDatSrc.isEGOUSW then
          begin
            if SC.EnterAngle > 0 then Result := Result + LangTable.Caption['Common:ZoomInChannel_SHORT'];
            if SC.EnterAngle < 0 then Result := Result + LangTable.Caption['Common:ZoomOutChannel_SHORT'];
            if SC.EnterAngle = 0 then Result := Result + '-';
          end
          else
          if FDatSrc.isVMTUS_Scheme_1_or_2 then
          begin
            if EGOUSW_WP_Data[SC.BS_Ch_File_Number] = 0 then Result := Result + 'A'
                                                        else Result := Result + 'B';
          end
          else
          if FDatSrc.isVMTUS_Scheme_4 then
           begin
            if EGOUSW_BigWP_Data[SC.BS_Ch_File_Number] = 0 then Result := Result + 'A'
                                                           else Result := Result + 'B';
          end;
         {
            if SC.EnterAngle > 0 then Result:= Result + LangTable.Caption['Common:ZoomInChannel_SHORT'];
            if SC.EnterAngle < 0 then Result:= Result + LangTable.Caption['Common:ZoomOutChannel_SHORT'];
            if SC.EnterAngle = 0 then Result:= Result + '-';
            }
          Result := Result + ' ';
          // Result:= Result + ', ' + LangTable.Caption['Common:ChannelParams_SHORT:Angles'] + ': ';
          if SC.TurnAngle <> 0 then
            Result := Result + Format('%d/%d', [Abs(SC.EnterAngle), SC.TurnAngle])
          else
            Result := Result + Format('%d', [Abs(SC.EnterAngle)]);
          // Result:= Result + ', ' + LangTable.Caption['Common:ChannelParams_SHORT:Method'] + ': ';
          Result := Result + ' ';
          Result := Result + LangTable.Caption['Common:' + GetInspMethodTextId(EC.Method, False)];
          // Result:= Result + ', ' + LangTable.Caption['Common:ChannelParams_SHORT:Zone'] + ': ';
          Result := Result + ' ';
          Result := Result + LangTable.Caption['Common:' + GetInspZoneTextId(EC.Zone, False)]; { Зона }
        end;
  end;

function TAvk11Display.ExtendedViewListCount: Integer;
begin
  if Length(FExtendedViewList) = 0 then
  begin
    Result:= 0;
    Exit;
  end
  else
  if Length(FExtendedViewList) =  2 then
  begin
    if Config.MagnetView then Result:= 2
                         else Result:= 1;
  end;
end;

function TAvk11Display.ExtendedViewListItem(Index: Integer): Integer;
begin
  if Length(FExtendedViewList) = 0 then
  begin
    Result:= 0;
    Exit;
  end
  else
  if Length(FExtendedViewList) =  2 then
  begin
    if Config.MagnetView then Result:= FExtendedViewList[Index]
                         else Result:= FExtendedViewList[1];
  end;
end;



(*
  function TAvk11Display.CID_GateIndex_To_Name(CID, GateIndex: Integer): string;

  var
    I: Integer;

  begin
    for I := 0 to High(CID_GIdx_To_Text) do
      if (CID_GIdx_To_Text[I].CID = CID) and (CID_GIdx_To_Text[I].GIdx = GateIndex) then
      begin
        Result := CID_GIdx_To_Text[I].Name;
        Exit;
      end;
//    Result := Format('CID: 0x%x Gate: %x;', [CID, GateIndex]);
  end;
*)

initialization

SetLength(CID_GIdx_To_Text, 10);
CID_GIdx_To_Text[0].CID := $17;
CID_GIdx_To_Text[0].GIdx := 1;
CID_GIdx_To_Text[0].Name := '70 Н';

CID_GIdx_To_Text[1].CID := $18;
CID_GIdx_To_Text[1].GIdx := 1;
CID_GIdx_To_Text[1].Name := '70 Н';
// $19, '42 Н'
CID_GIdx_To_Text[2].CID := $19;
CID_GIdx_To_Text[2].GIdx := 1;
CID_GIdx_To_Text[2].Name := '42 Н';
// $1A, '42 O'
CID_GIdx_To_Text[3].CID := $1A;
CID_GIdx_To_Text[3].GIdx := 1;
CID_GIdx_To_Text[3].Name := '42 O';
// $6, '58 РН'
CID_GIdx_To_Text[4].CID := $6;
CID_GIdx_To_Text[4].GIdx := 1;
CID_GIdx_To_Text[4].Name := '58 РН';
// $7, '58 НН'
CID_GIdx_To_Text[5].CID := $7;
CID_GIdx_To_Text[5].GIdx := 1;
CID_GIdx_To_Text[5].Name := '58 НН';
// $B, '58 РО'
CID_GIdx_To_Text[6].CID := $B;
CID_GIdx_To_Text[6].GIdx := 1;
CID_GIdx_To_Text[6].Name := '58 РО';
// $C, '58 НО'
CID_GIdx_To_Text[7].CID := $C;
CID_GIdx_To_Text[7].GIdx := 1;
CID_GIdx_To_Text[7].Name := '58 НО';
// $1, 'ЭХО'
CID_GIdx_To_Text[8].CID := $1;
CID_GIdx_To_Text[8].GIdx := 1;
CID_GIdx_To_Text[8].Name := '0 ЭХО';
// $1, 'ЭХО'
CID_GIdx_To_Text[9].CID := $1;
CID_GIdx_To_Text[9].GIdx := 2;
CID_GIdx_To_Text[9].Name := '0 ЗТМ';

end.




  /// //////////////////////////////////////////////////////////////////////////////////

  // Отладка - отрисовка АСД

  (*
    if not FDM.RailView then
    for I := StartIdx to EndIdx do
    if FDatSrc.Event[I].ID = EID_DebugData then
    begin
    FDatSrc.GetEventData(I, ID, pData);
    if pedDebugData(pData)^.Data[0] = 1 then
    begin
    X1 := StartX + DisToScr(pedDebugData(pData)^.Data[3] - StartDisCoord);
    X2 := StartX + DisToScr(pedDebugData(pData)^.Data[4] - StartDisCoord);

    if (X1 > EndX) then Break;
    if (X2 < StartX) then Continue;

    Line := FScanChNumToBScanLine[TRail(pedDebugData(pData)^.Data[1]), EvalChToScanCh[pedDebugData(pData)^.Data[2]]];
    if Line <> -1 then
    with FBScanLines[Line] do
    begin
    SetClipRect(BScanRt);
    if Odd(pedDebugData(pData)^.Data[2]) then Self.DrawRect(Rect(X1, BScanRt.Bottom - 5, X2, BScanRt.Bottom - 3 * (BScanRt.Bottom - BScanRt.Top) div 4), 1, EvalChNumColors2[FDatSrc.Config.ChannelIDList[TRail(pedDebugData(pData)^.Data[1]), pedDebugData(pData)^.Data[2]], 15])
    else Self.DrawRect(Rect(X1, BScanRt.Top + 5, X2, BScanRt.Top + 3 * (BScanRt.Bottom - BScanRt.Top) div 4), 1, EvalChNumColors2[FDatSrc.Config.ChannelIDList[TRail(pedDebugData(pData)^.Data[1]), pedDebugData(pData)^.Data[2]], 15])
    end;
    end;
    ReSetClipRect();
    end;
    *)

  // if PaintSystemViewResState then
  // begin


