object ConfigForm: TConfigForm
  Left = 400
  Top = 263
  ActiveControl = Button1
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1087#1088#1086#1075#1088#1072#1084#1084#1099
  ClientHeight = 514
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShortCut = FormShortCut
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 473
    Width = 371
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 281
      Top = 0
      Width = 90
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 8
        Top = 7
        Width = 75
        Height = 25
        Caption = 'OK'
        TabOrder = 0
        OnClick = Button1Click
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 371
    Height = 473
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 3
      Top = 3
      Width = 365
      Height = 467
      ActivePage = TabSheet5
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = #1054#1073#1097#1080#1077
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel10: TPanel
          Left = 13
          Top = 0
          Width = 331
          Height = 439
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Bevel4: TBevel
            Left = 0
            Top = 106
            Width = 331
            Height = 2
            Align = alTop
            ExplicitTop = 150
            ExplicitWidth = 332
          end
          object Bevel8: TBevel
            Left = 0
            Top = 155
            Width = 331
            Height = 2
            Align = alTop
            ExplicitTop = 195
            ExplicitWidth = 332
          end
          object Bevel2: TBevel
            Left = 0
            Top = 337
            Width = 331
            Height = 3
            Align = alTop
            ExplicitTop = 362
            ExplicitWidth = 329
          end
          object Bevel3: TBevel
            Left = 0
            Top = 42
            Width = 331
            Height = 2
            Align = alTop
            ExplicitWidth = 332
          end
          object Bevel6: TBevel
            Left = 0
            Top = 259
            Width = 331
            Height = 3
            Align = alTop
            ExplicitTop = 284
            ExplicitWidth = 329
          end
          object Bevel1: TBevel
            Left = 0
            Top = 449
            Width = 331
            Height = 2
            Align = alTop
            Visible = False
            ExplicitLeft = -6
            ExplicitTop = 519
          end
          object cbWorkLang: TComboBox
            Left = 0
            Top = 362
            Width = 331
            Height = 21
            Align = alTop
            Style = csDropDownList
            TabOrder = 0
            OnChange = cbWorkLangChange
            OnClick = ControlToConfig
            Items.Strings = (
              'Russian'
              'English')
          end
          object cbOpenMaxim: TCheckBox
            Left = 0
            Top = 179
            Width = 331
            Height = 20
            Align = alTop
            Caption = #1054#1090#1082#1088#1099#1074#1072#1090#1100' '#1092#1072#1081#1083#1099' '#1074' '#1088#1072#1079#1074#1077#1088#1085#1091#1090#1086#1084' '#1086#1082#1085#1077
            TabOrder = 1
            OnClick = ControlToConfig
          end
          object CheckBox2: TCheckBox
            Left = 242
            Top = 586
            Width = 87
            Height = 65
            Caption = #1042#1099#1073#1086#1088' '#1088#1077#1078#1080#1084#1072' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103' '#1087#1086' '#1082#1088#1091#1075#1091
            TabOrder = 2
            Visible = False
            WordWrap = True
            OnClick = ControlToConfig
          end
          object CheckBox1: TCheckBox
            Left = 0
            Top = 22
            Width = 331
            Height = 20
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Align = alTop
            Caption = #1055#1086#1076#1087#1080#1089#1080' '#1082' '#1082#1085#1086#1087#1082#1072#1084
            TabOrder = 3
            OnClick = ControlToConfig
          end
          object cbCoordNet: TCheckBox
            Left = 0
            Top = 219
            Width = 331
            Height = 20
            Align = alTop
            Caption = #1057#1077#1090#1082#1072' '#1082#1086#1086#1088#1076#1080#1085#1072#1090
            TabOrder = 4
            OnClick = ControlToConfig
          end
          object cbCoordType: TCheckBox
            Left = 242
            Top = 650
            Width = 65
            Height = 17
            Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1072' '#1086#1090' '#1076#1074#1091#1093' '#1089#1090#1086#1083#1073#1086#1074
            TabOrder = 5
            Visible = False
            OnClick = ControlToConfig
          end
          object cbRuler: TCheckBox
            Left = 0
            Top = 199
            Width = 331
            Height = 20
            Align = alTop
            Caption = #1051#1080#1085#1077#1081#1082#1072
            TabOrder = 6
            OnClick = ControlToConfig
          end
          object RadioButton2: TRadioButton
            Left = 0
            Top = 86
            Width = 331
            Height = 20
            Align = alTop
            Caption = 'Inverse'
            TabOrder = 7
            OnClick = ControlToConfig
          end
          object RadioButton1: TRadioButton
            Left = 0
            Top = 66
            Width = 331
            Height = 20
            Align = alTop
            Caption = 'Direct'
            Checked = True
            TabOrder = 8
            TabStop = True
            OnClick = ControlToConfig
          end
          object cbZeroProbeLineMode: TComboBox
            Left = 0
            Top = 130
            Width = 331
            Height = 21
            Align = alTop
            Style = csDropDownList
            TabOrder = 9
            OnChange = ControlToConfig
            Items.Strings = (
              'Both'
              'First'
              'Second')
          end
          object StaticText2: TPanel
            Left = 0
            Top = 0
            Width = 331
            Height = 22
            Align = alTop
            Alignment = taLeftJustify
            BevelOuter = bvNone
            Caption = 'Tool Bar'
            TabOrder = 10
            object Bevel11: TBevel
              Left = 6
              Top = 28
              Width = 330
              Height = 2
            end
          end
          object StaticText1: TPanel
            Left = 0
            Top = 44
            Width = 331
            Height = 22
            Align = alTop
            Alignment = taLeftJustify
            BevelOuter = bvNone
            Caption = 'Delay Axis'
            TabOrder = 11
          end
          object pZeroProbeTapes: TPanel
            Left = 0
            Top = 108
            Width = 331
            Height = 22
            Align = alTop
            Alignment = taLeftJustify
            BevelOuter = bvNone
            Caption = 'Zero probe tapes'
            TabOrder = 12
          end
          object Panel5: TPanel
            Left = 0
            Top = 151
            Width = 331
            Height = 4
            Align = alTop
            Alignment = taLeftJustify
            BevelOuter = bvNone
            TabOrder = 13
          end
          object Label1: TPanel
            Left = 0
            Top = 340
            Width = 331
            Height = 22
            Align = alTop
            Alignment = taLeftJustify
            BevelOuter = bvNone
            Caption = #1071#1079#1099#1082' '#1080#1085#1090#1077#1088#1092#1077#1081#1089#1072
            TabOrder = 14
          end
          object Label7: TPanel
            Left = 0
            Top = 157
            Width = 331
            Height = 22
            Align = alTop
            Alignment = taLeftJustify
            BevelOuter = bvNone
            Caption = #1056#1072#1079#1085#1086#1077
            TabOrder = 15
          end
          object pMeasMode: TPanel
            Left = 0
            Top = 383
            Width = 331
            Height = 23
            Align = alTop
            Alignment = taLeftJustify
            BevelOuter = bvNone
            Caption = 'Measurement system'
            TabOrder = 16
          end
          object cbMeasurementMode: TComboBox
            Left = 0
            Top = 406
            Width = 331
            Height = 21
            Align = alTop
            Style = csDropDownList
            TabOrder = 17
            OnChange = ControlToConfig
            Items.Strings = (
              'Millimeters '
              'Inches')
          end
          object Panel6: TPanel
            Left = 0
            Top = 284
            Width = 331
            Height = 34
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 18
            object lMouseShift: TLabel
              Left = 9
              Top = 2
              Width = 141
              Height = 26
              Caption = #1057#1076#1074#1080#1075' '#1076#1077#1092#1077#1082#1090#1086#1075#1088#1072#1084#1084#1099' '#1087#1088#1080' '#1087#1086#1074#1086#1088#1086#1090#1077' '#1082#1086#1083#1077#1089#1080#1082#1072
              WordWrap = True
            end
            object tbWheelStep: TTrackBar
              Left = 154
              Top = 2
              Width = 153
              Height = 27
              Ctl3D = False
              ParentCtl3D = False
              TabOrder = 0
              TabStop = False
              ThumbLength = 18
              OnChange = ControlToConfig
            end
          end
          object pMouse: TPanel
            Left = 0
            Top = 262
            Width = 331
            Height = 22
            Align = alTop
            Alignment = taLeftJustify
            BevelOuter = bvNone
            Caption = #1052#1099#1096#1100
            TabOrder = 19
          end
          object cbWheelDir: TCheckBox
            Left = 0
            Top = 318
            Width = 331
            Height = 19
            Align = alTop
            Caption = #1055#1088#1103#1084#1086#1077' '#1085#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077
            TabOrder = 20
            OnClick = ControlToConfig
          end
          object cbAddDatainCoord: TCheckBox
            Left = 0
            Top = 239
            Width = 331
            Height = 20
            Align = alTop
            Caption = 'Additional value at coordinate'
            TabOrder = 21
            Visible = False
            OnClick = ControlToConfig
          end
          object CheckBox3: TCheckBox
            Left = 0
            Top = 451
            Width = 331
            Height = 20
            Align = alTop
            Caption = #1055#1088#1080' '#1086#1090#1082#1088#1099#1090#1080#1080' '#1092#1072#1081#1083#1072' '#1079#1072#1087#1091#1089#1082#1072#1090#1100' '#1072#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1091#1102' '#1088#1072#1089#1096#1080#1092'-'#1082#1091
            TabOrder = 22
            Visible = False
            OnClick = ControlToConfig
          end
          object Label3: TPanel
            Left = 0
            Top = 427
            Width = 331
            Height = 22
            Align = alTop
            Alignment = taLeftJustify
            BevelOuter = bvNone
            Caption = #1040#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1072#1103' '#1088#1072#1089#1096#1080#1092#1088#1086#1074#1082#1072
            TabOrder = 23
            Visible = False
          end
        end
        object LeftPanel: TPanel
          Left = 0
          Top = 0
          Width = 13
          Height = 439
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Panel7: TPanel
            Left = 3
            Top = 7
            Width = 6
            Height = 6
            TabOrder = 0
          end
          object Panel8: TPanel
            Left = 3
            Top = 51
            Width = 6
            Height = 6
            TabOrder = 1
          end
          object Panel9: TPanel
            Left = 3
            Top = 51
            Width = 6
            Height = 6
            TabOrder = 2
          end
          object Panel12: TPanel
            Left = 3
            Top = 114
            Width = 6
            Height = 6
            TabOrder = 3
          end
          object Panel13: TPanel
            Left = 3
            Top = 163
            Width = 6
            Height = 6
            TabOrder = 4
          end
          object Panel14: TPanel
            Left = 3
            Top = 349
            Width = 6
            Height = 6
            TabOrder = 5
          end
          object Panel15: TPanel
            Left = 3
            Top = 392
            Width = 6
            Height = 6
            TabOrder = 6
          end
          object Panel17: TPanel
            Left = 3
            Top = 270
            Width = 6
            Height = 6
            TabOrder = 7
          end
        end
        object TPanel
          Left = 344
          Top = 0
          Width = 13
          Height = 439
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 2
        end
      end
      object TabSheet2: TTabSheet
        Caption = #1062#1074#1077#1090#1072
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel11: TPanel
          Left = -4
          Top = 3
          Width = 579
          Height = 555
          BevelOuter = bvNone
          TabOrder = 2
          object Bevel5: TBevel
            Left = 10
            Top = 10
            Width = 341
            Height = 2
          end
          object Bevel14: TBevel
            Left = 10
            Top = 93
            Width = 341
            Height = 2
          end
          object ChangePalName: TButton
            Left = 150
            Top = 23
            Width = 91
            Height = 27
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            TabOrder = 0
            OnClick = ChangePalNameClick
          end
          object cbPal: TComboBox
            Left = 16
            Top = 25
            Width = 128
            Height = 21
            Style = csDropDownList
            TabOrder = 1
            OnChange = cbPalChange
          end
          object NewPal: TButton
            Left = 253
            Top = 23
            Width = 92
            Height = 27
            Caption = #1053#1086#1074#1072#1103
            TabOrder = 2
            OnClick = NewPalClick
          end
          object DelPal: TButton
            Left = 150
            Top = 56
            Width = 91
            Height = 27
            Caption = #1059#1076#1072#1083#1080#1090#1100
            TabOrder = 3
            OnClick = DelPalClick
          end
          object Button4: TButton
            Left = 200
            Top = 221
            Width = 49
            Height = 17
            Caption = 'Set'
            TabOrder = 4
            Visible = False
            OnClick = Button4Click
          end
          object CopyPal: TButton
            Left = 253
            Top = 56
            Width = 92
            Height = 27
            Caption = #1044#1091#1073#1083#1080#1088#1086#1074#1072#1090#1100
            TabOrder = 5
            OnClick = CopyPalClick
          end
          object List: TListView
            Left = 9
            Top = 103
            Width = 342
            Height = 349
            Columns = <
              item
                Width = 250
              end
              item
              end
              item
              end
              item
                Width = 12
              end>
            ReadOnly = True
            RowSelect = True
            TabOrder = 6
            ViewStyle = vsReport
            OnCustomDrawSubItem = ListCustomDrawSubItem
            OnDblClick = ListDblClick
            OnMouseMove = ListMouseMove
          end
        end
        object Label5: TStaticText
          Left = 25
          Top = 88
          Width = 41
          Height = 17
          Caption = ' '#1062#1074#1077#1090#1072' '
          TabOrder = 1
        end
        object Label4: TStaticText
          Left = 25
          Top = 6
          Width = 53
          Height = 17
          Caption = ' '#1055#1072#1083#1080#1090#1088#1072' '
          TabOrder = 0
        end
      end
      object TabSheet3: TTabSheet
        Caption = #1054#1090#1086#1073#1088#1072#1078#1077#1085#1080#1077
        ImageIndex = 2
        TabVisible = False
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Bevel7: TBevel
          Left = 4
          Top = 13
          Width = 477
          Height = 2
        end
        object Bevel9: TBevel
          Left = 239
          Top = 13
          Width = 2
          Height = 342
        end
        object Label8: TLabel
          Left = 429
          Top = 89
          Width = 32
          Height = 13
          Caption = 'Label8'
        end
        object Label9: TLabel
          Left = 246
          Top = 67
          Width = 236
          Height = 15
          AutoSize = False
          Caption = #1071#1088#1082#1086#1089#1090#1100'  '#1089#1080#1075#1085#1072#1083#1072' '#1084#1080#1085#1080#1084#1072#1083#1100#1085#1086#1081' '#1072#1084#1087#1083#1080#1090#1091#1076#1099
          WordWrap = True
        end
        object Bevel10: TBevel
          Left = 248
          Top = 59
          Width = 228
          Height = 2
        end
        object clbViewEvent: TCheckListBox
          Left = 10
          Top = 27
          Width = 214
          Height = 319
          OnClickCheck = ControlToConfig
          ItemHeight = 13
          TabOrder = 0
        end
        object Label6: TStaticText
          Left = 16
          Top = 6
          Width = 135
          Height = 17
          Caption = ' '#1054#1090#1086#1073#1088#1072#1078#1072#1077#1084#1099#1077' '#1089#1086#1073#1099#1090#1080#1103' '
          TabOrder = 1
        end
        object cbThreeColor: TCheckBox
          Left = 251
          Top = 20
          Width = 229
          Height = 37
          Caption = #1055#1088#1080' '#1085#1072#1083#1086#1078#1077#1085#1080#1080' '#1101#1093#1086'-'#1089#1080#1075#1085#1072#1083#1086#1074' '#1080#1089#1087#1086#1083#1100'- '#1079#1086#1074#1072#1090#1100' '#1090#1088#1077#1090#1080#1081' '#1094#1074#1077#1090
          TabOrder = 2
          WordWrap = True
          OnClick = ControlToConfig
        end
        object StaticText3: TStaticText
          Left = 256
          Top = 7
          Width = 45
          Height = 17
          Caption = ' B Scan '
          TabOrder = 3
        end
        object TrackBar1: TTrackBar
          Left = 250
          Top = 85
          Width = 171
          Height = 23
          Max = 50
          Min = 10
          Position = 10
          TabOrder = 4
          TickMarks = tmBoth
          TickStyle = tsNone
          OnChange = TrackBar1Change
        end
      end
      object TabSheet4: TTabSheet
        Caption = #1056#1072#1079#1085#1086#1077
        ImageIndex = 3
        TabVisible = False
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Bevel12: TBevel
          Left = 4
          Top = 277
          Width = 477
          Height = 2
          Visible = False
        end
        object Bevel13: TBevel
          Left = 3
          Top = 170
          Width = 477
          Height = 2
          Visible = False
        end
        object Label10: TLabel
          Left = 8
          Top = 184
          Width = 144
          Height = 13
          Caption = #1057#1082#1086#1088#1086#1089#1090#1100':                       '#1082#1084'/'#1095
          Visible = False
        end
        object Label12: TLabel
          Left = 87
          Top = 31
          Width = 84
          Height = 13
          Caption = #1050#1072#1085#1072#1083' 0      -  '#1086#1090'  '
        end
        object Label13: TLabel
          Left = 87
          Top = 53
          Width = 78
          Height = 13
          Caption = #1050#1072#1085#1072#1083' 1      -  '#1086#1090
        end
        object Label14: TLabel
          Left = 87
          Top = 75
          Width = 78
          Height = 13
          Caption = #1050#1072#1085#1072#1083' 2, 3  -  '#1086#1090
        end
        object Label15: TLabel
          Left = 87
          Top = 97
          Width = 78
          Height = 13
          Caption = #1050#1072#1085#1072#1083' 4, 5  -  '#1086#1090
        end
        object Label16: TLabel
          Left = 87
          Top = 119
          Width = 78
          Height = 13
          Caption = #1050#1072#1085#1072#1083' 6, 7  -  '#1086#1090
        end
        object Label17: TLabel
          Left = 87
          Top = 141
          Width = 78
          Height = 13
          Caption = #1050#1072#1085#1072#1083' 8, 9  -  '#1086#1090
        end
        object Label11: TLabel
          Left = 250
          Top = 31
          Width = 43
          Height = 13
          Caption = #1076#1041'   - '#1076#1086' '
        end
        object Label18: TLabel
          Left = 365
          Top = 31
          Width = 13
          Height = 13
          Caption = #1076#1041
        end
        object Label19: TLabel
          Left = 250
          Top = 53
          Width = 43
          Height = 13
          Caption = #1076#1041'   - '#1076#1086' '
        end
        object Label20: TLabel
          Left = 365
          Top = 53
          Width = 13
          Height = 13
          Caption = #1076#1041
        end
        object Label21: TLabel
          Left = 250
          Top = 97
          Width = 43
          Height = 13
          Caption = #1076#1041'   - '#1076#1086' '
        end
        object Label22: TLabel
          Left = 250
          Top = 75
          Width = 43
          Height = 13
          Caption = #1076#1041'   - '#1076#1086' '
        end
        object Label23: TLabel
          Left = 365
          Top = 75
          Width = 13
          Height = 13
          Caption = #1076#1041
        end
        object Label24: TLabel
          Left = 365
          Top = 97
          Width = 13
          Height = 13
          Caption = #1076#1041
        end
        object Label25: TLabel
          Left = 250
          Top = 141
          Width = 43
          Height = 13
          Caption = #1076#1041'   - '#1076#1086' '
        end
        object Label26: TLabel
          Left = 250
          Top = 119
          Width = 43
          Height = 13
          Caption = #1076#1041'   - '#1076#1086' '
        end
        object Label27: TLabel
          Left = 365
          Top = 119
          Width = 13
          Height = 13
          Caption = #1076#1041
        end
        object Label28: TLabel
          Left = 365
          Top = 141
          Width = 13
          Height = 13
          Caption = #1076#1041
        end
        object ASUPCheckBox: TCheckBox
          Left = 4
          Top = 288
          Width = 97
          Height = 17
          Caption = #1042#1082#1083'.'
          TabOrder = 0
          Visible = False
          OnClick = ControlToConfig
        end
        object StaticText4: TStaticText
          Left = 14
          Top = 270
          Width = 91
          Height = 17
          Caption = ' '#1056#1072#1073#1086#1090#1072' '#1089' '#1040#1057#1059'-'#1055' '
          TabOrder = 1
          Visible = False
        end
        object StaticText5: TStaticText
          Left = 14
          Top = 164
          Width = 58
          Height = 17
          Caption = ' '#1050#1086#1085#1090#1088#1086#1083#1100' '
          TabOrder = 2
          Visible = False
        end
        object SpeedEdit: TEdit
          Left = 67
          Top = 181
          Width = 57
          Height = 21
          TabOrder = 3
          Visible = False
          OnChange = ControlToConfig
        end
        object Ch0MinEdit: TEdit
          Left = 182
          Top = 28
          Width = 63
          Height = 21
          TabOrder = 4
          OnChange = ControlToConfig
        end
        object Ch0MaxEdit: TEdit
          Left = 297
          Top = 28
          Width = 63
          Height = 21
          TabOrder = 5
          OnChange = ControlToConfig
        end
        object Ch1MinEdit: TEdit
          Left = 182
          Top = 50
          Width = 63
          Height = 21
          TabOrder = 6
          OnChange = ControlToConfig
        end
        object Ch1MaxEdit: TEdit
          Left = 297
          Top = 50
          Width = 63
          Height = 21
          TabOrder = 7
          OnChange = ControlToConfig
        end
        object Ch2MinEdit: TEdit
          Left = 182
          Top = 72
          Width = 63
          Height = 21
          TabOrder = 8
          OnChange = ControlToConfig
        end
        object Ch4MinEdit: TEdit
          Left = 182
          Top = 94
          Width = 63
          Height = 21
          TabOrder = 9
          OnChange = ControlToConfig
        end
        object Ch2MaxEdit: TEdit
          Left = 297
          Top = 72
          Width = 63
          Height = 21
          TabOrder = 10
          OnChange = ControlToConfig
        end
        object Ch4MaxEdit: TEdit
          Left = 297
          Top = 94
          Width = 63
          Height = 21
          TabOrder = 11
          OnChange = ControlToConfig
        end
        object Ch6MinEdit: TEdit
          Left = 182
          Top = 116
          Width = 63
          Height = 21
          TabOrder = 12
          OnChange = ControlToConfig
        end
        object Ch6MaxEdit: TEdit
          Left = 297
          Top = 116
          Width = 63
          Height = 21
          TabOrder = 13
          OnChange = ControlToConfig
        end
        object Ch8MinEdit: TEdit
          Left = 182
          Top = 138
          Width = 63
          Height = 21
          TabOrder = 14
          OnChange = ControlToConfig
        end
        object Ch8MaxEdit: TEdit
          Left = 297
          Top = 138
          Width = 63
          Height = 21
          TabOrder = 15
          OnChange = ControlToConfig
        end
        object StaticText6: TStaticText
          Left = 14
          Top = 6
          Width = 104
          Height = 17
          Caption = ' '#1063#1091#1074#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '
          TabOrder = 16
        end
      end
      object TabSheet5: TTabSheet
        Caption = #1056#1072#1079#1085#1086#1077
        ImageIndex = 4
        object Label2: TLabel
          Left = 3
          Top = 9
          Width = 216
          Height = 14
          AutoSize = False
          Caption = #1050#1072#1090#1072#1083#1086#1075' '#1088#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1103' '#1074#1080#1076#1077#1086' '#1092#1072#1081#1083#1086#1074':'
          WordWrap = True
        end
        object Label29: TLabel
          Left = 3
          Top = 102
          Width = 244
          Height = 13
          Caption = #1060#1086#1088#1084#1072#1090' '#1087#1091#1090#1077#1081#1089#1082#1086#1081' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1099' '#1086#1090#1095#1077#1090#1085#1099#1093' '#1092#1086#1088#1084' '
        end
        object Label30: TLabel
          Left = 3
          Top = 54
          Width = 311
          Height = 14
          AutoSize = False
          Caption = #1050#1072#1090#1072#1083#1086#1075' '#1088#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1103' '#1074#1080#1076#1077#1086' '#1087#1088#1086#1080#1075#1088#1099#1074#1072#1090#1077#1083#1103' VLC'
          WordWrap = True
        end
        object TBEditItem1: TEdit
          Left = 3
          Top = 29
          Width = 314
          Height = 21
          TabOrder = 0
        end
        object Button2: TButton
          Left = 320
          Top = 26
          Width = 23
          Height = 27
          Caption = '...'
          TabOrder = 1
          OnClick = Button2Click
        end
        object cbEstonisCoordSysytem: TComboBox
          Left = 3
          Top = 122
          Width = 128
          Height = 21
          Style = csDropDownList
          TabOrder = 2
          OnChange = cbEstonisCoordSysytemChange
          Items.Strings = (
            #1082#1080#1083#1086#1084#1077#1090#1088
            #1075#1077#1082#1090#1086#1084#1077#1090#1088
            #1084#1077#1090#1088)
        end
        object Edit1: TEdit
          Left = 3
          Top = 74
          Width = 314
          Height = 21
          TabOrder = 3
        end
        object Button3: TButton
          Left = 320
          Top = 71
          Width = 23
          Height = 27
          Caption = '...'
          TabOrder = 4
          OnClick = Button3Click
        end
      end
    end
  end
  object ColorDialog: TColorDialog
    Options = [cdFullOpen, cdAnyColor]
    Left = 244
    Top = 120
  end
end
