unit RecInfoUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TRecInfoForm = class(TForm)
    RichEdit1: TRichEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SetData;
  end;

var
  RecInfoForm: TRecInfoForm;

implementation

{$R *.dfm}

procedure TRecInfoForm.SetData;
begin
  //
end;

procedure TRecInfoForm.FormCreate(Sender: TObject);
begin
  RichEdit1.Lines.Clear;
end;

end.
