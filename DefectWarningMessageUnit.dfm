object DefectWarningMessageForm: TDefectWarningMessageForm
  Left = 530
  Top = 366
  VertScrollBar.Visible = False
  BorderStyle = bsDialog
  Caption = #1042#1085#1080#1084#1072#1085#1080#1077'!'
  ClientHeight = 180
  ClientWidth = 437
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    437
    180)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 5
    Width = 415
    Height = 48
    Anchors = [akLeft, akTop, akRight]
    Caption = 
      #1040#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1086#1077' '#1074#1099#1076#1077#1083#1077#1085#1080#1077' '#1089#1080#1075#1085#1072#1083#1086#1074' '#1086#1090' '#1074#1086#1079#1084#1086#1078#1085#1099#1093' '#1076#1077#1092#1077#1082#1090#1086#1074' '#1088#1077#1083#1100#1089#1086#1074' ' +
      #1086#1089#1091#1097#1077#1089#1090#1074#1083#1103#1077#1090#1089#1103' '#1042#1053#1045' '#1079#1086#1085' '#1073#1086#1083#1090#1086#1074#1099#1093' '#1089#1090#1099#1082#1086#1074' '#1080' '#1089#1090#1088#1077#1083#1086#1095#1085#1099#1093' '#1087#1077#1088#1077#1074#1086#1076#1086#1074'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label2: TLabel
    Left = 7
    Top = 56
    Width = 419
    Height = 32
    Anchors = [akLeft, akTop, akRight]
    Caption = 
      #1042' '#1079#1086#1085#1077' '#1089#1090#1099#1082#1086#1074' '#1074#1099#1076#1077#1083#1103#1102#1090#1089#1103' '#1089#1080#1075#1085#1072#1083#1099' '#1058#1054#1051#1068#1050#1054' '#1087#1088#1080' '#1087#1086#1076#1086#1079#1088#1077#1085#1080#1080' '#1085#1072' '#1076#1077#1092#1077#1082#1090 +
      #1099' 53.1 '#1074' '#1073#1086#1083#1090#1086#1074#1099#1093' '#1086#1090#1074#1077#1088#1089#1090#1080#1103#1093'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label3: TLabel
    Left = 6
    Top = 97
    Width = 419
    Height = 32
    Anchors = [akLeft, akTop, akRight]
    Caption = 
      #1042#1085#1080#1084#1072#1085#1080#1077'! '#1054#1082#1086#1085#1095#1072#1090#1077#1083#1100#1085#1086#1077' '#1088#1077#1096#1077#1085#1080#1077' '#1086' '#1085#1072#1083#1080#1095#1080#1080' '#1076#1077#1092#1077#1082#1090#1072' '#1087#1088#1080#1085#1080#1084#1072#1077#1090' '#1054#1055#1045#1056 +
      #1040#1058#1054#1056'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object Button1: TButton
    Left = 333
    Top = 146
    Width = 100
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 0
    OnClick = Button1Click
  end
  object CheckBox1: TCheckBox
    Left = 8
    Top = 150
    Width = 315
    Height = 17
    Anchors = [akLeft, akRight, akBottom]
    Caption = #1041#1086#1083#1100#1096#1077' '#1085#1077' '#1087#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1087#1088#1077#1076#1091#1087#1088#1077#1078#1076#1077#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
end
