{$I DEF.INC}
unit ConfigUnit; {Language 6}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,  LanguageUnit,
  StdCtrls, ComCtrls, ExtCtrls, IniFiles, MyTypes, CheckLst, CfgTablesUnit_2010, Generics.Collections,


               {
  , Mask, TB2Item, TB2Dock, TB2Toolbar, Registry,
  ImgList, FileCtrl, Buttons, Math, TB2ExtItems,
  AviconTypes,} ShellApi, ShlObj, ActiveX;


type
  // ����������� ���� ���������� �������, ��������� �����
  // ���������� ������ ������ L1737 ��� ��������� �
  // ��������� ������� ��������� autodecShow � ������ �������
  TAutodecShow = TDictionary<Integer, Boolean>;

  TConfigForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    ColorDialog: TColorDialog;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel10: TPanel;
    Bevel4: TBevel;
    Bevel8: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    cbWorkLang: TComboBox;
    cbOpenMaxim: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox1: TCheckBox;
    cbCoordNet: TCheckBox;
    cbCoordType: TCheckBox;
    cbRuler: TCheckBox;
    RadioButton2: TRadioButton;
    RadioButton1: TRadioButton;
    cbZeroProbeLineMode: TComboBox;
    StaticText2: TPanel;
    StaticText1: TPanel;
    pZeroProbeTapes: TPanel;
    Panel5: TPanel;
    Label1: TPanel;
    Label7: TPanel;
    TabSheet2: TTabSheet;
    Panel11: TPanel;
    Bevel5: TBevel;
    Bevel14: TBevel;
    ChangePalName: TButton;
    cbPal: TComboBox;
    NewPal: TButton;
    DelPal: TButton;
    Button4: TButton;
    CopyPal: TButton;
    List: TListView;
    Label5: TStaticText;
    Label4: TStaticText;
    TabSheet3: TTabSheet;
    Bevel7: TBevel;
    Bevel9: TBevel;
    Label8: TLabel;
    Label9: TLabel;
    Bevel10: TBevel;
    clbViewEvent: TCheckListBox;
    Label6: TStaticText;
    cbThreeColor: TCheckBox;
    StaticText3: TStaticText;
    TrackBar1: TTrackBar;
    TabSheet4: TTabSheet;
    Bevel12: TBevel;
    Bevel13: TBevel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label11: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    ASUPCheckBox: TCheckBox;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    SpeedEdit: TEdit;
    Ch0MinEdit: TEdit;
    Ch0MaxEdit: TEdit;
    Ch1MinEdit: TEdit;
    Ch1MaxEdit: TEdit;
    Ch2MinEdit: TEdit;
    Ch4MinEdit: TEdit;
    Ch2MaxEdit: TEdit;
    Ch4MaxEdit: TEdit;
    Ch6MinEdit: TEdit;
    Ch6MaxEdit: TEdit;
    Ch8MinEdit: TEdit;
    Ch8MaxEdit: TEdit;
    StaticText6: TStaticText;
    pMeasMode: TPanel;
    cbMeasurementMode: TComboBox;
    Panel6: TPanel;
    tbWheelStep: TTrackBar;
    lMouseShift: TLabel;
    LeftPanel: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Bevel6: TBevel;
    pMouse: TPanel;
    cbWheelDir: TCheckBox;
    Panel17: TPanel;
    Bevel11: TBevel;
    cbAddDatainCoord: TCheckBox;
    CheckBox3: TCheckBox;
    Bevel1: TBevel;
    Label3: TPanel;
    TabSheet5: TTabSheet;
    TBEditItem1: TEdit;
    Label2: TLabel;
    Button2: TButton;
    Label29: TLabel;
    cbEstonisCoordSysytem: TComboBox;
    Label30: TLabel;
    Edit1: TEdit;
    Button3: TButton;

    procedure lbPalColorsDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure ChangePalNameClick(Sender: TObject);
    procedure NewPalClick(Sender: TObject);
    procedure DelPalClick(Sender: TObject);
    procedure CopyPalClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure CreatePalList(Sender: TObject);
    procedure cbPalChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ControlToConfig(Sender: TObject);
    procedure OnChangeLanguage(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbWorkLangChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure ListCustomDrawSubItem(Sender: TCustomListView; Item: TListItem; SubItem: Integer; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure ListDblClick(Sender: TObject);
    procedure ListMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Button2Click(Sender: TObject);
    procedure cbEstonisCoordSysytemChange(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    ClickX: Integer;
    SkipFlg: Boolean;
    FOnChangeLanguage_: TNotifyEvent;

  public
    property OnChangeLanguage_: TNotifyEvent read FOnChangeLanguage_ write FOnChangeLanguage_;
  end;

type
  TMyFieldMode = (fmVisible, fmHide, fmTree);

  TProgramMode = (pmRADIOAVIONICA, pmGEISMAR, pmRADIOAVIONICA_MAV, pmRADIOAVIONICA_KZ);

  TMeasurementMode = (mmMillimeter, mmInches);

  T_CFG_ZeroProbeChannelsViewMode = (zpAll, zpFirst, zbSecond);

  TMyField = record
    Name: string;
    DataID: Integer;
    Mode: TMyFieldMode;
    Width: Integer;
    Enabled: Boolean;
  end;

  TFNStruc = record
    Idx: Integer;
    On_: Boolean;
  end;

  TConfig = class
  protected
    FOpenFilesHistory: TStringList; // ������� �������� ������
    FSaveAsHistory: TStringList; // ������� ���������� ������
    FDownLoadFilesHistory: TStringList; // ������� �������� ������
    FConfig: TINIFile;
    FConfigFileName: string;
  private
    function GetActivDCColors: TDisplayColors;
    function GetActivPCColors: TDisplayColors;
  public
    ViewThresholdValue: Integer; // ����� ����������� ��������
    FilterMode: Integer; // ������ ��� ������ �-���������: 1 - ����; 2 - �������; 3 - �������
    AskFlashName: Boolean;
    LastFlashDir: string;
    VideoLANDir: string;
    DisplayConfig: TDisplayConfig;
    BasePath: string;
    SavePath: string;
    Fields: array of TMyField;
    OpenMaximazed: Boolean;
    WorkLang: string;
    ProgramMode: TProgramMode;
    //LangMode: Integer;
    FNSpace: string;
    FNStruc: array [0..15] of TFNStruc;
    PalList: TPalList;
    ActivPal: Integer;
    EstonisCoordSysytem: Integer;
    CoordType: Boolean;
    InvertDelayAxis: Boolean;
    EchoSizeByAmpl: Boolean;
    EchoSize: Integer;
    EchoColorByAmpl: Boolean;
    ActivePageIndex: Integer;
    ButtonText: Boolean;
    ViewSelMetod: Boolean;
    UseThreeColor: Boolean;
    RepExportDir: string;
    VideoFilePath: string;
    MinColPer: Integer;
    LupaZoom: Integer;
    OnlyName: Boolean;
    SortDataID: Integer;
    SortDir: Boolean;
//    UsbLog: Boolean;
    SendIdx: Integer;
    ShowChSettings: Boolean;
    ShowChGate: Boolean;
    MarkOnBScan: Boolean;
    ShowChInfo: Boolean;
    WheelStep: Integer;
    WheelDir: Boolean;
    MeasurementMode: TMeasurementMode;
    ZeroProbeChannelsViewMode: T_CFG_ZeroProbeChannelsViewMode;
    ShowSensor1DataFlag: Boolean;
    MetalSensorValue0State: Boolean;
    MetalSensorValue1State: Boolean;
    MetallSensorZoneState: Boolean;
    UnstableBottomSignalState: Boolean;
    ZerroProbeModeState: Boolean;
    PaintSystemStateState: Boolean;
    PaintSystemViewResState: Boolean;
    PaintSystemViewResDebug: Boolean;
    AutomaticSearchResState: Boolean;

    isElementsCollectionShow: Boolean;
    isReflectionsShow       : Boolean;
    autodecShow : TAutodecShow;

// ������ ���������, �������� ��������
    isPureRailDefectShow    : Boolean;
    isConstructDefectShow   : Boolean;
    isBoltedJointsShow      : Boolean;
    isSwitchesShow          : Boolean;
    isEmptyBottomZonesShow  : Boolean;
    isPureHolesShow         : Boolean;



    // ��������������� !!!
    DfShowWarningMessage: Boolean;
    MagnetView: Boolean;
    MaxSpeed: Single;
    KuZones: array [1..6] of TMinMax;

    ShowEventIndex: Boolean;
    ShowUSBLog: Boolean;
    CDUVer: Boolean;

    constructor Create(ConfigFileName: string = ''); // �������� �������
    destructor Destroy; override; // �������� �������
    procedure Load;
    procedure Save;
    property ActivDColors: TDisplayColors read GetActivDCColors;
    property ActivPColors: TDisplayColors read GetActivPCColors;
    property OpenFilesHistory: TStringList read FOpenFilesHistory;
    property SaveAsHistory: TStringList read FSaveAsHistory;
    property DownLoadFilesHistory: TStringList read FDownLoadFilesHistory;
    procedure AddToOpenFilesHistory(FileName: string);
    procedure AddToSaveAsHistory(FileName: string);
    procedure AddToDownLoadFilesHistory(FileName: string);
    function GetProgramModeId: string;
    function GetMoveDirColor(Dir: Integer): TColor;
  end;

var
  Config: TConfig;

implementation

uses
  CommonUnit, AutodecUnit, MainUnit;

{$R *.DFM}

// -----[ TConfig ]-------------------------------------------------------------

constructor TConfig.Create(ConfigFileName: string = ''); // �������� �������
begin
  ShowEventIndex:= False;
  ShowUSBLog:= False;
  ActivePageIndex:= 0;
  if ConfigFileName = '' then FConfigFileName:= DataFileDir {ExtractFilePath(ParamStr(0))} + 'map.ini'; // ��� � ���� INI �����

  FConfig:= TINIFile.Create(FConfigFileName);
  FOpenFilesHistory:= TStringList.Create; // ������� �������� ������
  FSaveAsHistory:= TStringList.Create;
  FDownLoadFilesHistory:= TStringList.Create;

    // ��������������� !!!
  autodecShow := TAutodecShow.Create;
//  DfShowWarningMessage:= false;
  MagnetView:= false;

end;

destructor TConfig.Destroy; // �������� �������
begin
  FOpenFilesHistory.Free;
  FSaveAsHistory.Free;
  FDownLoadFilesHistory.Free;
  FConfig.Free;
  autodecShow.Free;
end;

function TConfig.GetActivDCColors: TDisplayColors;
begin
  Result:= PalList[ActivPal].DC;
end;

function TConfig.GetActivPCColors: TDisplayColors;
begin
  Result:= PalList[ActivPal].PC;
end;

procedure TConfig.Load;
const
  PointSize: array [0..15, 1..2] of Integer = ((1, 1), (1, 1), (1, 1), (1, 1), (2, 2), (2, 2), (2, 2), (2, 2), (3, 3), (3, 3), (3, 3), (3, 3), (4, 3) ,(4, 3) ,(4, 3), (4, 3));

var
  I, J, K: Integer;
  S: string;
  Tmp: TStringList;
  RedConst: array [1..4, 1..2, 0..7] of Single;
  boolVal: Boolean;

begin
  DecimalSeparator:= '.'; // ��������� ��������������� ������� ��� �������. �����
//  if FileExists(FConfigFileName) then // �������� INI �����
//  begin
  SetLength(Fields, FConfig.ReadInteger('Settings', 'ColCount', 0));
  for I:= 0 to High(Fields) do
  begin
//    Fields[I].Name:= FConfig.ReadString('Collumn', 'Name' + IntToStr(I), '');
    Fields[I].DataID:= FConfig.ReadInteger('Collumn', 'DataID' + IntToStr(I), 0);
    Fields[I].Mode:= TMyFieldMode(FConfig.ReadInteger('Collumn', 'Mode' + IntToStr(I), 0));
    Fields[I].Width:= FConfig.ReadInteger('Collumn', 'Width' + IntToStr(I), 0);
    J:= FConfig.ReadInteger('Collumn', 'Use' + IntToStr(I), 0);
    if J = 1 then Fields[I].Enabled:= true else Fields[I].Enabled:= false;
  end;

  if Length(Fields) = 0 then
  begin
    SetLength(Fields, 14);

//    Fields[0].Name  := '�������';
    Fields[0].DataID:= 11;
    Fields[0].Mode  := TMyFieldMode(0);
    Fields[0].Width := 152;

//    Fields[1].Name  := '�����';
    Fields[1].DataID:= 16;
    Fields[1].Mode  := TMyFieldMode(1);
    Fields[1].Width := 88;

  //  Fields[2].Name  := '����������� �������������';
    Fields[2].DataID:= -1;
    Fields[2].Mode  := TMyFieldMode(1);
    Fields[2].Width := 244;

//    Fields[3].Name  := '����';
    Fields[3].DataID:= 14;
    Fields[3].Mode  := TMyFieldMode(0);
    Fields[3].Width := 106;

//    Fields[4].Name  := '��������� ����������';
    Fields[4].DataID:= 12;
    Fields[4].Mode  := TMyFieldMode(0);
    Fields[4].Width := 156;

//    Fields[5].Name  := '�������� ����������';
    Fields[5].DataID:= 13;
    Fields[5].Mode  := TMyFieldMode(0);
    Fields[5].Width := 146;

//    Fields[6].Name  := '��� ���������';
    Fields[6].DataID:= 9;
    Fields[6].Mode  := TMyFieldMode(0);
    Fields[6].Width := 135;

//    Fields[7].Name  := '����������� ��������';
    Fields[7].DataID:= 15;
    Fields[7].Mode  := TMyFieldMode(1);
    Fields[7].Width := 247;

//    Fields[8].Name  := '����� �������';
    Fields[8].DataID:= 4;
    Fields[8].Mode  := TMyFieldMode(1);
    Fields[8].Width := 137;

//    Fields[9].Name  := '��� �����';
    Fields[9].DataID:= 1;
    Fields[9].Mode  := TMyFieldMode(1);
    Fields[9].Width := 670;

//    Fields[10].Name  := '���� � ����� �������';
    Fields[10].DataID:= 6;
    Fields[10].Mode  := TMyFieldMode(0);
    Fields[10].Width := 131;

//    Fields[11].Name  := '��� �����������';
    Fields[11].DataID:= 10;
    Fields[11].Mode  := TMyFieldMode(1);
    Fields[11].Width := 116;

//    Fields[12].Name  := '�������� �.�.';
    Fields[12].DataID:= 7;
    Fields[12].Mode  := TMyFieldMode(0);
    Fields[12].Width := 168;

//    Fields[13].Name  := '�������������';
    Fields[13].DataID:= 23;
    Fields[13].Mode  := TMyFieldMode(0);
    Fields[13].Width := 45;
  end;
{
  for I:= 0 to 7 do
  begin
    DisplayConfig.ReducePos[1, 1, I]:= FConfig.ReadInteger('Reduction const 1', 'ChannelPos ' + IntToStr(I), 0);
    DisplayConfig.ReduceDel[1, 1, I]:= FConfig.ReadFloat('Reduction const 1', 'ChannelDel ' + IntToStr(I), 0);
    DisplayConfig.ReducePos[1, 2, I]:= FConfig.ReadInteger('Reduction const 2', 'ChannelPos ' + IntToStr(I), 0);
    DisplayConfig.ReduceDel[1, 2, I]:= FConfig.ReadFloat('Reduction const 2', 'ChannelDel ' + IntToStr(I), 0);
    DisplayConfig.ReducePos[2, 1, I]:= FConfig.ReadInteger('Reduction const 3', 'ChannelPos ' + IntToStr(I), 0);
    DisplayConfig.ReduceDel[2, 1, I]:= FConfig.ReadFloat('Reduction const 3', 'ChannelDel ' + IntToStr(I), 0);
    DisplayConfig.ReducePos[2, 2, I]:= FConfig.ReadInteger('Reduction const 4', 'ChannelPos ' + IntToStr(I), 0);
    DisplayConfig.ReduceDel[2, 2, I]:= FConfig.ReadFloat('Reduction const 4', 'ChannelDel ' + IntToStr(I), 0);
  end;}
(*
  RedConst[1, 1, 0]:= - 28;
  RedConst[1, 1, 1]:= - 28;
  RedConst[1, 1, 2]:= - 70;
  RedConst[1, 1, 3]:= - 187;
  RedConst[1, 1, 4]:= - 100;
  RedConst[1, 1, 5]:= - 172;
  RedConst[1, 1, 6]:= - 76;
  RedConst[1, 1, 7]:= - 202;
  RedConst[1, 2, 0]:= 0;
  RedConst[1, 2, 1]:= 0;
  RedConst[1, 2, 2]:= 0;
  RedConst[1, 2, 3]:= 0;
  RedConst[1, 2, 4]:= 0;
  RedConst[1, 2, 5]:= 0;
  RedConst[1, 2, 6]:= 0;
  RedConst[1, 2, 7]:= 0;
  RedConst[2, 1, 0]:= 75;
  RedConst[2, 1, 1]:= 75;
  RedConst[2, 1, 2]:= 25;
  RedConst[2, 1, 3]:= - 75;
  RedConst[2, 1, 4]:= 50;
  RedConst[2, 1, 5]:= - 25;
  RedConst[2, 1, 6]:= - 50;
  RedConst[2, 1, 7]:= - 50;
  RedConst[2, 2, 0]:= 0;
  RedConst[2, 2, 1]:= 0;
  RedConst[2, 2, 2]:= 1.172;
  RedConst[2, 2, 3]:= - 1.172;
  RedConst[2, 2, 4]:= 1.532;
  RedConst[2, 2, 5]:= - 1.172;
  RedConst[2, 2, 6]:= 1.091;
  RedConst[2, 2, 7]:= - 1.091;
  RedConst[3, 1, 0]:= - 128;
  RedConst[3, 1, 1]:= - 128;
  RedConst[3, 1, 2]:= - 170;
  RedConst[3, 1, 3]:= - 87;
  RedConst[3, 1, 4]:= - 200;
  RedConst[3, 1, 5]:= - 72;
  RedConst[3, 1, 6]:= 25;
  RedConst[3, 1, 7]:= - 102;
  RedConst[3, 2, 0]:= 0;
  RedConst[3, 2, 1]:= 0;
  RedConst[3, 2, 2]:= 0;
  RedConst[3, 2, 3]:= 0;
  RedConst[3, 2, 4]:= 0;
  RedConst[3, 2, 5]:= 0;
  RedConst[3, 2, 6]:= 0;
  RedConst[3, 2, 7]:= 0;
  RedConst[4, 1, 0]:= - 25;
  RedConst[4, 1, 1]:= - 25;
  RedConst[4, 1, 4]:= - 50;
  RedConst[4, 1, 2]:= - 75;
  RedConst[4, 1, 5]:= 75;
  RedConst[4, 1, 6]:= 50;
  RedConst[4, 1, 7]:= 50;
  RedConst[4, 1, 3]:= 25;
  RedConst[4, 2, 0]:= 0;
  RedConst[4, 2, 1]:= 0;
  RedConst[4, 2, 2]:= 1.172;
  RedConst[4, 2, 3]:= - 1.172;
  RedConst[4, 2, 4]:= 1.532;
  RedConst[4, 2, 5]:= - 1.172;
  RedConst[4, 2, 6]:= 1.091;
  RedConst[4, 2, 7]:= - 1.091;
*)
{
  for K:= 1 to 4 do
    for J:= 1 to 2 do
      for I:= 0 to 7 do
        if J = 1 then
        begin
          if DisplayConfig.ReducePos[Ord((K - 1) and 2 <> 0) + 1, (K - 1) and 1 + 1, I] = 0 then
            DisplayConfig.ReducePos[Ord((K - 1) and 2 <> 0) + 1, (K - 1) and 1 + 1, I]:= Round(RedConst[K, J, I]);
        end
        else
        begin
          if DisplayConfig.ReduceDel[Ord((K - 1) and 2 <> 0) + 1, (K - 1) and 1 + 1, I] = 0 then
            DisplayConfig.ReduceDel[Ord((K - 1) and 2 <> 0) + 1, (K - 1) and 1 + 1, I]:= RedConst[K, J, I];
        end; }
{
  for I:= 0 to 7 do
  begin
    DisplayConfig.BScanViewZone[I].BegDelay:= FConfig.ReadInteger('ViewZones', 'Channel ' + IntToStr(I) + ' Min', 0);
    DisplayConfig.BScanViewZone[I].EndDelay:= FConfig.ReadInteger('ViewZones', 'Channel ' + IntToStr(I) + ' Max', 0);
    if DisplayConfig.BScanViewZone[I].BegDelay = 0 then DisplayConfig.BScanViewZone[I].BegDelay:= ViewZones[I, 1];
    if DisplayConfig.BScanViewZone[I].EndDelay = 0 then DisplayConfig.BScanViewZone[I].EndDelay:= ViewZones[I, 2];
    DisplayConfig.BScanViewZone[I].Dur:= DisplayConfig.BScanViewZone[I].EndDelay - DisplayConfig.BScanViewZone[I].BegDelay;
  end;
}
{  for I:= 0 to 15 do
  begin
    DisplayConfig.PixelSize[I].X:= FConfig.ReadInteger('PointSize', IntToStr(I) + 'X', 0);
    DisplayConfig.PixelSize[I].Y:= FConfig.ReadInteger('PointSize', IntToStr(I) + 'Y', 0);
  end;
  DisplayConfig.PixelSizeIdx:= 2;

  for I:= 0 to 15 do
  begin
    if DisplayConfig.PixelSize[I].X = 0 then DisplayConfig.PixelSize[I].X:= PointSize[I, 1];
    if DisplayConfig.PixelSize[I].Y = 0 then DisplayConfig.PixelSize[I].Y:= PointSize[I, 2];
  end;
}
  Tmp:= TStringList.Create;
  FConfig.ReadSections(Tmp);
  for I:= 0 to Tmp.Count - 1 do
    if Pos('Palette', Tmp.Strings[I]) <> 0 then
    begin
      J:= Length(PalList);
      SetLength(PalList, J + 1);
      PalList[J].Name          := FConfig.ReadString(Tmp.Strings[I], 'Name', 'New');
      PalList[J].DC.Fon        := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Signal Background ', ColorToString(clWhite  )));
      PalList[J].DC.Border     := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Border            ', ColorToString(clBlack  )));
      PalList[J].DC.BackMotion := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Back Motion Zone  ', ColorToString(clYellow )));
      PalList[J].DC.NaezdCh[1]   := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Run Over Channels1 ', ColorToString(clRed    )));
      PalList[J].DC.OtezdCh[1]   := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Drive Off Channels1', ColorToString(clBlue   )));
      PalList[J].DC.NaezdCh[2]   := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Run Over Channels2 ', ColorToString(clRed    )));
      PalList[J].DC.OtezdCh[2]   := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Drive Off Channels2', ColorToString(clBlue   )));
      PalList[J].DC.CrossedCh  := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Crossed Channels  ', ColorToString(clGreen  )));
      PalList[J].DC.Text       := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Labels            ', ColorToString(clBlack  )));
      PalList[J].DC.Event      := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Event             ', ColorToString(clYellow )));
      PalList[J].DC.Defect     := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Defect            ', ColorToString(clYellow )));
      PalList[J].DC.CoordNet   := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Coordinate grid   ', ColorToString(clSilver )));
      PalList[J].DC.Ruler      := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Ruler             ', ColorToString($00E8E8E8)));
      PalList[J].DC.EnvelopeBS := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Bottom Signal Ampl', ColorToString(clGreen  )));
      PalList[J].DC.LongLab    := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Extended Label    ', ColorToString(clRed    )));
      PalList[J].DC.FiltrEcho  := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Screen_Filtr Echo        ', ColorToString($00DBDBDB)));

      PalList[J].PC.Fon        := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Signal Background ', ColorToString(clWhite  )));
      PalList[J].PC.Border     := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Border            ', ColorToString(clBlack  )));
      PalList[J].PC.BackMotion := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Back Motion Zone  ', ColorToString(clYellow )));
      PalList[J].PC.NaezdCh[1]    := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Run Over Channels1 ', ColorToString(clRed    )));
      PalList[J].PC.OtezdCh[1]    := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Drive Off Channels1', ColorToString(clBlue   )));
      PalList[J].PC.NaezdCh[2]    := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Run Over Channels2 ', ColorToString(clRed    )));
      PalList[J].PC.OtezdCh[2]    := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Drive Off Channels2', ColorToString(clBlue   )));
      PalList[J].PC.CrossedCh  := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Crossed Channels  ', ColorToString(clGreen  )));
      PalList[J].PC.Text       := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Labels            ', ColorToString(clBlack  )));
      PalList[J].PC.Event      := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Event             ', ColorToString(clYellow )));
      PalList[J].PC.Defect     := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Defect            ', ColorToString(clYellow )));
      PalList[J].PC.CoordNet   := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Coordinate grid   ', ColorToString(clSilver )));
      PalList[J].PC.Ruler      := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Ruler             ', ColorToString($00E8E8E8)));
      PalList[J].PC.EnvelopeBS := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Bottom Signal Ampl', ColorToString(clGreen  )));
      PalList[J].PC.LongLab    := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Extended Label    ', ColorToString(clRed    )));
      PalList[J].PC.FiltrEcho  := StringToColor(FConfig.ReadString(Tmp.Strings[I], 'Printer_Filtr Echo        ', ColorToString($00DBDBDB)));
    end;

  if Length(PalList) = 0 then
  begin
    SetLength(PalList, 1);
    I:= 0;
    PalList[I].Name          := 'Auto';
    PalList[I].DC.Fon        := clWhite;
    PalList[I].DC.Border     := clBlack;
    PalList[I].DC.BackMotion := clYellow;
    PalList[I].DC.NaezdCh[1]   := clRed;
    PalList[I].DC.OtezdCh[1]   := clBlue;
    PalList[I].DC.NaezdCh[2]   := clRed;
    PalList[I].DC.OtezdCh[2]   := clBlue;
    PalList[I].DC.CrossedCh  := clGreen;
    PalList[I].DC.Text       := clBlack;
    PalList[I].DC.Event      := clYellow;
    PalList[I].DC.Defect     := clRed;
    PalList[I].DC.CoordNet   := clSilver;
    PalList[I].DC.Ruler      := $00E8E8E8;
    PalList[I].DC.EnvelopeBS := clGreen;
    PalList[I].DC.LongLab    := clRed;
    PalList[I].DC.FiltrEcho  := $00DBDBDB;
    PalList[I].PC.Fon        := clWhite;
    PalList[I].PC.Border     := clBlack;
    PalList[I].PC.BackMotion := clYellow;
    PalList[I].PC.NaezdCh[1]   := clRed;
    PalList[I].PC.OtezdCh[1]   := clBlue;
    PalList[I].PC.NaezdCh[2]   := clRed;
    PalList[I].PC.OtezdCh[2]   := clBlue;
    PalList[I].PC.CrossedCh  := clGreen;
    PalList[I].PC.Text       := clBlack;
    PalList[I].PC.Event      := clYellow;
    PalList[I].PC.Defect     := clRed;
    PalList[I].PC.CoordNet   := clSilver;
    PalList[I].PC.Ruler      := $00E8E8E8;
    PalList[I].PC.EnvelopeBS := clGreen;
    PalList[I].PC.LongLab    := clRed;
    PalList[I].PC.FiltrEcho  := $00DBDBDB;
  end;

  FConfig.ReadSection('RecentFiles', FOpenFilesHistory);
  I:= FOpenFilesHistory.Count;
  FOpenFilesHistory.Clear;
  for I:= 0 to I - 1 do
    FOpenFilesHistory.Add(FConfig.ReadString('RecentFiles', IntToStr(I), ''));

  FConfig.ReadSection('SaveAsHistory', FSaveAsHistory);
  I:= FSaveAsHistory.Count;
  FSaveAsHistory.Clear;
  for I:= 0 to I - 1 do
    FSaveAsHistory.Add(FConfig.ReadString('SaveAsHistory', IntToStr(I), ''));

  FConfig.ReadSection('DownLoadFilesHistory', FDownLoadFilesHistory);
  I:= FDownLoadFilesHistory.Count;
  FDownLoadFilesHistory.Clear;
  for I:= 0 to I - 1 do
    FDownLoadFilesHistory.Add(FConfig.ReadString('DownLoadFilesHistory', IntToStr(I), ''));

  Tmp.Clear;
  FConfig.ReadSection('DownloadFileNameStruc', Tmp);
  for I:= 0 to Tmp.Count - 1 do
  begin
    FNStruc[I].Idx:= StrToInt(Tmp.Strings[I]);
    FNStruc[I].On_:= FConfig.ReadInteger('DownloadFileNameStruc', Tmp.Strings[I], 0) = 1;
  end;

  if Tmp.Count <> 16 then
    for I:= 0 to 15 do
    begin
      FNStruc[I].Idx:= 9900001 + I;
      FNStruc[I].On_:= I < 4 ;
    end;
//  Tmp.Free;
{
  S := FConfig.ReadString('Settings', 'ViewEvents', '111111111111111111111111111111');
  for I:= $90 to $AD do
  begin
    J:= I - $90 + 1;
    if (J >= 1) and (J <= Length(S)) then DisplayConfig.ShowEvent[I]:= S[J] = '1';
  end;
  DisplayConfig.ShowEvent[$AF]:= True;

  if DisplayConfig.ShowEvent[$99] or
     DisplayConfig.ShowEvent[$9C] then
  begin
    DisplayConfig.ShowEvent[$99]:= True;
    DisplayConfig.ShowEvent[$9C]:= True;
  end;
      }
  AskFlashName            := FConfig.ReadBool('Settings', 'AskDownLoadFileName', True);
  LastFlashDir            := FConfig.ReadString('Settings', 'LastWorkDir', 'C:\BASE');
  VideoLANDir             := FConfig.ReadString('Settings', 'VideoLANDir', 'C:\');
  BasePath                := FConfig.ReadString('Settings', 'DataBaseDir', 'C:\BASE');
  SavePath                := FConfig.ReadString('Settings', 'SaveDir', 'C:');
  ActivPal                := FConfig.ReadInteger('Settings', 'ActivPal', 0);
  EstonisCoordSysytem     := FConfig.ReadInteger('Settings', 'EstonisCoordSysytem', 0);
  EchoSize                := FConfig.ReadInteger('Settings', 'EchoSize', 0);
  WorkLang                := FConfig.ReadString('Settings', 'WorkLanguage', 'English');
//  LangMode                := FConfig.ReadInteger('Settings', 'LanguageMode', 0);
  OpenMaximazed           := FConfig.ReadBool('Settings', 'OpenMaximazed', True);
  DisplayConfig.Ruler     := FConfig.ReadBool('Settings', 'ShowRuler', True);
  DisplayConfig.CoordNet  := FConfig.ReadBool('Settings', 'ShowCoordNet', True);
  DisplayConfig.AddDatainCoord := FConfig.ReadBool('Settings', 'AddDatainCoord', False);
  FNSpace                 := FConfig.ReadString('Settings', 'DawnloadFileNameSpace', '_');
  CoordType               := FConfig.ReadBool('Settings', 'CoordType', True);
  EchoSizeByAmpl          := FConfig.ReadBool('Settings', 'EchoSizeByAmpl', False);
  EchoColorByAmpl         := FConfig.ReadBool('Settings', 'EchoColorByAmpl', False);
  InvertDelayAxis         := FConfig.ReadBool('Settings', 'DelayAxis', False);
  ButtonText              := FConfig.ReadBool('Settings', 'ButtonText', True);
  ViewSelMetod            := FConfig.ReadBool('Settings', 'ViewSelMetod', True);
  UseThreeColor           := FConfig.ReadBool('Settings', 'UseThreeColor', False);
  RepExportDir            := FConfig.ReadString('Settings', 'RepExportDir', 'C:\');
  VideoFilePath           := FConfig.ReadString('Settings', 'VideoFilePath', 'C:\');
  MinColPer               := FConfig.ReadInteger('Settings', 'MinColPer', 25);
  LupaZoom                := FConfig.ReadInteger('Settings', 'LupaZoom', 100);
  OnlyName                := FConfig.ReadBool('Settings', 'OnlyName', True);
  ShowUSBLog                  := FConfig.ReadBool('Settings', 'UsbLog', True);
  SendIdx                 := FConfig.ReadInteger('Settings', 'SendIdx', 0);
  ProgramMode             := TProgramMode(FConfig.ReadInteger('Settings', 'ProgramMode', 0));
  ShowChSettings          := FConfig.ReadBool('Settings', 'ShowChSettings', True);
  ShowChGate              := FConfig.ReadBool('Settings', 'ShowChGate', True);
  ShowChInfo              := FConfig.ReadBool('Settings', 'ShowChInfo', True);
  MarkOnBScan             := FConfig.ReadBool('Settings', 'MarkOnBScan', True);
  SortDataID              := FConfig.ReadInteger('Settings', 'SortDataID', 0);
  SortDir                 := FConfig.ReadBool('Settings', 'SortDir', True);
  CDUVer                  := FConfig.ReadBool('Settings', 'CDUVer', False);

//  isElementsCollectionShow:= FConfig.ReadBool('Settings', 'isElementsCollectionShow', False);
  isElementsCollectionShow:= False;
  isReflectionsShow       := FConfig.ReadBool('Settings', 'isReflectionsShow', False);

  isPureRailDefectShow    := FConfig.ReadBool('Settings', 'isPureRailDefectShow', False);
  isConstructDefectShow   := FConfig.ReadBool('Settings', 'isConstructDefectShow', False);
  isBoltedJointsShow      := FConfig.ReadBool('Settings', 'isBoltedJointsShow', False);
  isSwitchesShow          := FConfig.ReadBool('Settings', 'isSwitchesShow', False);
  isEmptyBottomZonesShow  := FConfig.ReadBool('Settings', 'isEmptyBottomZonesShow', False);
  isPureHolesShow         := FConfig.ReadBool('Settings', 'isPureHolesShow', False);

  Tmp.Clear;
  autodecShow.Clear;
  FConfig.ReadSection('autoDecShow', Tmp);
  for I:= 0 to Tmp.Count - 1 do
  begin
    boolVal := FConfig.ReadBool('autoDecShow', Tmp.Strings[I], False);
    autodecShow.Add(StrToInt(Tmp.Strings[I]), boolVal);
  end;
  Tmp.Free;


  ShowSensor1DataFlag     := FConfig.ReadBool('Settings', 'ShowSensor1DataFlag', False);
  MetalSensorValue0State  := FConfig.ReadBool('Settings', 'MetalSensorValue0State', False);
  MetalSensorValue1State  := FConfig.ReadBool('Settings', 'MetalSensorValue1State', False);

  MetallSensorZoneState     := FConfig.ReadBool('Settings', 'MetallSensorZoneState', False);
  UnstableBottomSignalState := FConfig.ReadBool('Settings', 'UnstableBottomSignalState', False);
  ZerroProbeModeState       := FConfig.ReadBool('Settings', 'ZerroProbeModeState', False);
  PaintSystemStateState     := FConfig.ReadBool('Settings', 'PaintSystemStateState', False);
  PaintSystemViewResState   := FConfig.ReadBool('Settings', 'PaintSystemViewResState', False);
  PaintSystemViewResDebug   := FConfig.ReadBool('Settings', 'PaintSystemViewResDebug', False);
  AutomaticSearchResState   := FConfig.ReadBool('Settings', 'AutomaticSearchResState', False);


  WheelStep                 := FConfig.ReadInteger('Settings', 'WheelStep', 0);
  WheelDir                  := FConfig.ReadBool('Settings', 'WheelDir', True);
  MeasurementMode           := TMeasurementMode(FConfig.ReadInteger('Settings', 'MeasurementMode', 0));
  ZeroProbeChannelsViewMode := T_CFG_ZeroProbeChannelsViewMode(FConfig.ReadInteger('Settings', 'ZeroProbeChannelsViewMode', 0));
  ViewThresholdValue        := FConfig.ReadInteger('Settings', 'ViewThresholdValue', 0);
  FilterMode                := FConfig.ReadInteger('Settings', 'FilterMode', 1);

{  case LangMode of
    0: WorkLang:= 0;
    1: WorkLang:= 1;
  end; }

  MaxSpeed:= FConfig.ReadFloat('Settings', 'MaxSpeed', 4.1);
  KuZones[1].Min:= FConfig.ReadInteger('Settings', 'Ch0Min', 14);
  KuZones[1].Max:= FConfig.ReadInteger('Settings', 'Ch0Max', 20);
  KuZones[2].Min:= FConfig.ReadInteger('Settings', 'Ch1Min', 12);
  KuZones[2].Max:= FConfig.ReadInteger('Settings', 'Ch1Max', 18);
  KuZones[3].Min:= FConfig.ReadInteger('Settings', 'Ch2Min', 10);
  KuZones[3].Max:= FConfig.ReadInteger('Settings', 'Ch2Max', 16);
  KuZones[4].Min:= FConfig.ReadInteger('Settings', 'Ch3Min', 14);
  KuZones[4].Max:= FConfig.ReadInteger('Settings', 'Ch3Max', 20);
  KuZones[5].Min:= FConfig.ReadInteger('Settings', 'Ch4Min', 12);
  KuZones[5].Max:= FConfig.ReadInteger('Settings', 'Ch4Max', 18);
  KuZones[6].Min:= FConfig.ReadInteger('Settings', 'Ch5Min', 14);
  KuZones[6].Max:= FConfig.ReadInteger('Settings', 'Ch5Max', 20);
//  ASUP:= FConfig.ReadBool('Settings', 'UseASUP', True);
//  View_ASUP:= FConfig.ReadBool('Settings', 'View_ASUP', True);
//  View_Rec:= FConfig.ReadBool('Settings', 'View_Rec', True);
//  View_Kontr:= FConfig.ReadBool('Settings', 'View_Kontr', True);

//  ShowRecRes[0]:= FConfig.ReadBool('Settings', 'ShowRecRes[0]', True);
//  ShowRecRes[1]:= FConfig.ReadBool('Settings', 'ShowRecRes[1]', True);
//  ShowRecRes[2]:= FConfig.ReadBool('Settings', 'ShowRecRes[2]', True);
//  ShowRecRes[3]:= FConfig.ReadBool('Settings', 'ShowRecRes[3]', True);
//  ShowRecRes[4]:= FConfig.ReadBool('Settings', 'ShowRecRes[4]', True);
//  ShowRecRes[5]:= FConfig.ReadBool('Settings', 'ShowRecRes[5]', True);
//  ShowRecRes[6]:= FConfig.ReadBool('Settings', 'ShowRecRes[6]', True);
//  ShowRecRes[7]:= FConfig.ReadBool('Settings', 'ShowRecRes[7]', True);
//  ShowRecRes[8]:= FConfig.ReadBool('Settings', 'ShowRecRes[8]', True);
//  ShowRecRes[9]:= FConfig.ReadBool('Settings', 'ShowRecRes[9]', True);
//  ShowRecRes[10]:= FConfig.ReadBool('Settings', 'ShowRecRes[10]', True);
//  ShowRecRes[11]:= FConfig.ReadBool('Settings', 'ShowRecRes[11]', True);
//  ShowRecRes[12]:= FConfig.ReadBool('Settings', 'ShowRecRes[12]', True);
//  ShowRecRes[13]:= FConfig.ReadBool('Settings', 'ShowRecRes[13]', True);
//  ShowRecRes[14]:= FConfig.ReadBool('Settings', 'ShowRecRes[14]', True);

  DfShowWarningMessage := FConfig.ReadBool('Settings', 'DfShowWarningMessage', True);

  {$IFDEF AVIKON16}
//  WorkLang:= 1;
//  LangMode: Integer;
  {$ENDIF}
end;

procedure TConfig.Save;
var
  I, J: Integer;
  S: string;

begin
  DecimalSeparator:= '.'; // ��������� ��������������� ������� ��� �������. �����
  try
    for I:= 0 to High(Fields) do
    begin
      FConfig.WriteString('Collumn', 'Name' + IntToStr(I), Fields[I].Name);
      FConfig.WriteInteger('Collumn', 'DataID' + IntToStr(I), Fields[I].DataID);
      FConfig.WriteInteger('Collumn', 'Mode' + IntToStr(I), Ord(Fields[I].Mode));
      FConfig.WriteInteger('Collumn', 'Width' + IntToStr(I), Fields[I].Width);
      if Fields[I].Enabled then J:= 1 else J:= 0;
      FConfig.WriteInteger('Collumn', 'Use' + IntToStr(I), J);
    end;
    FConfig.WriteInteger('Settings', 'ColCount', Length(Fields));
{
    for I:= 0 to 7 do
    begin
      FConfig.WriteInteger('ViewZones', 'Channel ' + IntToStr(I) + ' Min', DisplayConfig.BScanViewZone[I].BegDelay);
      FConfig.WriteInteger('ViewZones', 'Channel ' + IntToStr(I) + ' Max', DisplayConfig.BScanViewZone[I].EndDelay);
    end;

    for I:= 0 to 15 do
    begin
      FConfig.WriteInteger('PointSize', IntToStr(I) + 'X', DisplayConfig.PixelSize[I].X);
      FConfig.WriteInteger('PointSize', IntToStr(I) + 'Y', DisplayConfig.PixelSize[I].Y);
    end;
}
    for I:= 0 to 30 do FConfig.EraseSection(Format('Palette %d', [I + 1]));

    for I:= 0 to High(PalList) do
    begin
      S:= 'Palette ' + IntToStr(I + 1);
      with PalList[I] do
      begin
        FConfig.WriteString(S, 'Name', Name);
        FConfig.WriteString(S, 'Screen_Signal Background ', ColorToString(PalList[I].DC.Fon       ));
        FConfig.WriteString(S, 'Screen_Border            ', ColorToString(PalList[I].DC.Border    ));
        FConfig.WriteString(S, 'Screen_Back Motion Zone  ', ColorToString(PalList[I].DC.BackMotion));
        FConfig.WriteString(S, 'Screen_Run Over Channels1 ', ColorToString(PalList[I].DC.NaezdCh[1]   ));
        FConfig.WriteString(S, 'Screen_Drive Off Channels1', ColorToString(PalList[I].DC.OtezdCh[1]   ));
        FConfig.WriteString(S, 'Screen_Run Over Channels2 ', ColorToString(PalList[I].DC.NaezdCh[2]   ));
        FConfig.WriteString(S, 'Screen_Drive Off Channels2', ColorToString(PalList[I].DC.OtezdCh[2]   ));
        FConfig.WriteString(S, 'Screen_Crossed Channels  ', ColorToString(PalList[I].DC.CrossedCh ));
        FConfig.WriteString(S, 'Screen_Labels            ', ColorToString(PalList[I].DC.Text      ));
        FConfig.WriteString(S, 'Screen_Event             ', ColorToString(PalList[I].DC.Event     ));
        FConfig.WriteString(S, 'Screen_Coordinate grid   ', ColorToString(PalList[I].DC.CoordNet  ));
        FConfig.WriteString(S, 'Screen_Ruler             ', ColorToString(PalList[I].DC.Ruler     ));
        FConfig.WriteString(S, 'Screen_Defect            ', ColorToString(PalList[I].DC.Defect    ));
        FConfig.WriteString(S, 'Screen_Bottom Signal Ampl', ColorToString(PalList[I].DC.EnvelopeBS));
        FConfig.WriteString(S, 'Screen_Extended Label    ', ColorToString(PalList[I].DC.LongLab   ));
        FConfig.WriteString(S, 'Screen_Filtr Echo        ', ColorToString(PalList[I].DC.FiltrEcho ));

        FConfig.WriteString(S, 'Printer_Signal Background ', ColorToString(PalList[I].PC.Fon       ));
        FConfig.WriteString(S, 'Printer_Border            ', ColorToString(PalList[I].PC.Border    ));
        FConfig.WriteString(S, 'Printer_Back Motion Zone  ', ColorToString(PalList[I].PC.BackMotion));
        FConfig.WriteString(S, 'Printer_Run Over Channels1 ', ColorToString(PalList[I].PC.NaezdCh[1]   ));
        FConfig.WriteString(S, 'Printer_Drive Off Channels1', ColorToString(PalList[I].PC.OtezdCh[1]   ));
        FConfig.WriteString(S, 'Printer_Run Over Channels2 ', ColorToString(PalList[I].PC.NaezdCh[2]   ));
        FConfig.WriteString(S, 'Printer_Drive Off Channels2', ColorToString(PalList[I].PC.OtezdCh[2]   ));
        FConfig.WriteString(S, 'Printer_Crossed Channels  ', ColorToString(PalList[I].PC.CrossedCh ));
        FConfig.WriteString(S, 'Printer_Labels            ', ColorToString(PalList[I].PC.Text      ));
        FConfig.WriteString(S, 'Printer_Event             ', ColorToString(PalList[I].PC.Event     ));
        FConfig.WriteString(S, 'Printer_Defect            ', ColorToString(PalList[I].PC.Defect    ));
        FConfig.WriteString(S, 'Printer_Coordinate grid   ', ColorToString(PalList[I].PC.CoordNet  ));
        FConfig.WriteString(S, 'Printer_Ruler             ', ColorToString(PalList[I].PC.Ruler     ));
        FConfig.WriteString(S, 'Printer_Bottom Signal Ampl', ColorToString(PalList[I].PC.EnvelopeBS));
        FConfig.WriteString(S, 'Printer_Extended Label    ', ColorToString(PalList[I].PC.LongLab   ));
        FConfig.WriteString(S, 'Printer_Filtr Echo        ', ColorToString(PalList[I].PC.FiltrEcho ));
      end;
    end;

    FConfig.EraseSection('RecentFiles');
    for I:= 0 to FOpenFilesHistory.Count - 1 do
      FConfig.WriteString('RecentFiles', IntToStr(I), FOpenFilesHistory.Strings[I]);

    FConfig.EraseSection('SaveAsHistory');
    for I:= 0 to FSaveAsHistory.Count - 1 do
      FConfig.WriteString('SaveAsHistory', IntToStr(I), FSaveAsHistory.Strings[I]);

    FConfig.EraseSection('DownLoadFilesHistory');
    for I:= 0 to DownLoadFilesHistory.Count - 1 do
      FConfig.WriteString('DownLoadFilesHistory', IntToStr(I), DownLoadFilesHistory.Strings[I]);

    FConfig.EraseSection('DownloadFileNameStruc');
    for I:= 0 to High(FNStruc) do
      FConfig.WriteInteger('DownloadFileNameStruc', IntToStr(FNStruc[I].Idx), Ord(FNStruc[I].On_));

    S:= '';
//    for I:= $90 to $AD do S:= S + IntToStr(Ord(DisplayConfig.ShowEvent[I]));
    FConfig.WriteString('Settings', 'ViewEvents', S);
    FConfig.WriteBool('Settings', 'AskDownLoadFileName', AskFlashName);
    FConfig.WriteString('Settings', 'LastWorkDir', LastFlashDir);
    FConfig.WriteString('Settings', 'VideoLANDir', VideoLANDir);
    FConfig.WriteString('Settings', 'DataBaseDir', BasePath);
    FConfig.WriteString('Settings', 'SaveDir', SavePath);
    FConfig.WriteInteger('Settings', 'ActivPal', ActivPal);
    FConfig.WriteInteger('Settings', 'EstonisCoordSysytem', EstonisCoordSysytem);
    FConfig.WriteInteger('Settings', 'EchoSize', EchoSize);
    FConfig.WriteString('Settings', 'WorkLanguage', WorkLang);
    FConfig.WriteBool('Settings', 'OpenMaximazed', OpenMaximazed);
    FConfig.WriteBool('Settings', 'ShowRuler', DisplayConfig.Ruler);
    FConfig.WriteBool('Settings', 'ShowCoordNet', DisplayConfig.CoordNet);
    FConfig.WriteBool('Settings', 'AddDatainCoord', DisplayConfig.AddDatainCoord);
    FConfig.WriteString('Settings', 'DawnloadFileNameSpace', FNSpace);
    FConfig.WriteBool('Settings', 'CoordType', CoordType);
    FConfig.WriteBool('Settings', 'EchoSizeByAmpl', EchoSizeByAmpl);
    FConfig.WriteBool('Settings', 'EchoColorByAmpl', EchoColorByAmpl);
    FConfig.WriteBool('Settings', 'DelayAxis', InvertDelayAxis);
    FConfig.WriteBool('Settings', 'ButtonText', ButtonText);
    FConfig.WriteBool('Settings', 'ViewSelMetod', ViewSelMetod);
    FConfig.WriteBool('Settings', 'UseThreeColor', UseThreeColor);
    FConfig.WriteString('Settings', 'RepExportDir', RepExportDir);
    FConfig.WriteString('Settings', 'VideoFilePath', VideoFilePath);
    FConfig.WriteInteger('Settings', 'MinColPer', MinColPer);
    FConfig.WriteInteger('Settings', 'LupaZoom', LupaZoom);
    FConfig.WriteInteger('Settings', 'LupaZoom', LupaZoom);
    FConfig.WriteBool('Settings', 'OnlyName', OnlyName);
    FConfig.WriteBool('Settings', 'UsbLog', ShowUSBLog);
    FConfig.WriteInteger('Settings', 'SendIdx', SendIdx);
    FConfig.WriteInteger('Settings', 'ProgramMode', Integer(ProgramMode));
    FConfig.WriteInteger('Settings', 'SortDataID', SortDataID);
    FConfig.WriteBool('Settings', 'SortDir', SortDir);
    FConfig.WriteBool('Settings', 'CDUVer', CDUVer);


//    FConfig.WriteBool('Settings', 'isElementsCollectionShow',  isElementsCollectionShow);
//    FConfig.WriteBool('Settings', 'isPureRailDefectShow',   isPureRailDefectShow  );
//    FConfig.WriteBool('Settings', 'isConstructDefectShow',  isConstructDefectShow );
//    FConfig.WriteBool('Settings', 'isBoltedJointsShow',     isBoltedJointsShow    );
//    FConfig.WriteBool('Settings', 'isSwitchesShow',         isSwitchesShow        );
//    FConfig.WriteBool('Settings', 'isEmptyBottomZonesShow', isEmptyBottomZonesShow);
//    FConfig.WriteBool('Settings', 'isPureHolesShow',        isPureHolesShow       );
    FConfig.WriteBool('Settings', 'isReflectionsShow',      isReflectionsShow     );

    FConfig.EraseSection('autoDecShow');
    for I in autodecShow.Keys do
    begin
      FConfig.WriteBool('autoDecShow', IntToStr(I), autodecShow[I]);
    end;

    FConfig.WriteBool('Settings', 'ShowSensor1DataFlag',     ShowSensor1DataFlag);
    FConfig.WriteBool('Settings', 'MetalSensorValue0State', MetalSensorValue0State);
    FConfig.WriteBool('Settings', 'MetalSensorValue1State', MetalSensorValue1State);

    FConfig.WriteBool('Settings', 'MetallSensorZoneState',     MetallSensorZoneState    );
    FConfig.WriteBool('Settings', 'UnstableBottomSignalState', UnstableBottomSignalState);
    FConfig.WriteBool('Settings', 'ZerroProbeModeState',       ZerroProbeModeState      );
    FConfig.WriteBool('Settings', 'PaintSystemStateState',     PaintSystemStateState    );
    FConfig.WriteBool('Settings', 'PaintSystemViewResState',   PaintSystemViewResState  );
    FConfig.WriteBool('Settings', 'PaintSystemViewResDebug',   PaintSystemViewResDebug  );
    FConfig.WriteBool('Settings', 'AutomaticSearchResState',   AutomaticSearchResState  );

    FConfig.WriteInteger('Settings', 'WheelStep', WheelStep);
    FConfig.WriteBool('Settings', 'WheelDir', WheelDir);

    FConfig.WriteInteger('Settings', 'MeasurementMode', Ord(MeasurementMode));
    FConfig.WriteInteger('Settings', 'ZeroProbeChannelsViewMode', Ord(ZeroProbeChannelsViewMode));
    FConfig.WriteInteger('Settings', 'ViewThresholdValue', ViewThresholdValue);
    FConfig.WriteInteger('Settings', 'FilterMode', FilterMode);

    FConfig.WriteBool('Settings', 'ShowChSettings', ShowChSettings);
    FConfig.WriteBool('Settings', 'ShowChGate', ShowChGate);
    FConfig.WriteBool('Settings', 'ShowChInfo', ShowChInfo);
    FConfig.WriteBool('Settings', 'MarkOnBScan', MarkOnBScan);

    FConfig.WriteFloat('Settings', 'MaxSpeed', MaxSpeed);
    FConfig.WriteInteger('Settings', 'Ch0Min', KuZones[1].Min);
    FConfig.WriteInteger('Settings', 'Ch0Max', KuZones[1].Max);
    FConfig.WriteInteger('Settings', 'Ch1Min', KuZones[2].Min);
    FConfig.WriteInteger('Settings', 'Ch1Max', KuZones[2].Max);
    FConfig.WriteInteger('Settings', 'Ch2Min', KuZones[3].Min);
    FConfig.WriteInteger('Settings', 'Ch2Max', KuZones[3].Max);
    FConfig.WriteInteger('Settings', 'Ch3Min', KuZones[4].Min);
    FConfig.WriteInteger('Settings', 'Ch3Max', KuZones[4].Max);
    FConfig.WriteInteger('Settings', 'Ch4Min', KuZones[5].Min);
    FConfig.WriteInteger('Settings', 'Ch4Max', KuZones[5].Max);
    FConfig.WriteInteger('Settings', 'Ch5Min', KuZones[6].Min);
    FConfig.WriteInteger('Settings', 'Ch5Max', KuZones[6].Max);

//    FConfig.WriteBool('Settings', 'UseASUP', ASUP);
//    FConfig.WriteBool('Settings', 'View_ASUP', View_ASUP);
//    FConfig.WriteBool('Settings', 'View_Rec', View_Rec);
//    FConfig.WriteBool('Settings', 'View_Kontr', View_Kontr);

//    FConfig.WriteBool('Settings', 'ShowRecRes[0]', ShowRecRes[0]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[1]', ShowRecRes[1]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[2]', ShowRecRes[2]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[3]', ShowRecRes[3]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[4]', ShowRecRes[4]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[5]', ShowRecRes[5]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[6]', ShowRecRes[6]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[7]', ShowRecRes[7]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[8]',  ShowRecRes[8]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[9]',  ShowRecRes[9]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[10]', ShowRecRes[10]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[11]', ShowRecRes[11]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[12]', ShowRecRes[12]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[13]', ShowRecRes[13]);
//    FConfig.WriteBool('Settings', 'ShowRecRes[14]', ShowRecRes[14]);

		FConfig.WriteBool('Settings', 'DfShowWarningMessage', DfShowWarningMessage);

    FConfig.UpdateFile;
  except
  end;
end;

procedure TConfig.AddToOpenFilesHistory(FileName: string);
var
  I: Integer;
  S: string;

begin
  I:= FOpenFilesHistory.IndexOf(FileName);
  if I = - 1 then FOpenFilesHistory.Add(FileName)
             else
             begin
               S:= FOpenFilesHistory.Strings[I];
               FOpenFilesHistory.Delete(I);
               FOpenFilesHistory.Add(S);
             end;
  while FOpenFilesHistory.Count > 10 do FOpenFilesHistory.Delete(0);
end;

procedure TConfig.AddToSaveAsHistory(FileName: string);
var
  I: Integer;
  S: string;

begin
  I:= FSaveAsHistory.IndexOf(FileName);
  if I = - 1 then FSaveAsHistory.Add(FileName)
             else
             begin
               S:= FSaveAsHistory.Strings[I];
               FSaveAsHistory.Delete(I);
               FSaveAsHistory.Add(S);
             end;
  while FSaveAsHistory.Count > 10 do FSaveAsHistory.Delete(0);
end;

procedure TConfig.AddToDownLoadFilesHistory(FileName: string);
begin
  FDownLoadFilesHistory.Add(FileName)
end;

function TConfig.GetProgramModeId: string;
begin
  case Config.ProgramMode of
            pmGEISMAR: Result:= ':Ge';
      pmRADIOAVIONICA: Result:= ':Ra';
  pmRADIOAVIONICA_MAV: Result:= ':RaMAV';
   pmRADIOAVIONICA_KZ: Result:= ':RaKZ';

  end;
end;

function TConfig.GetMoveDirColor(Dir: Integer): TColor;
begin
  if Dir > 0 then Result:= clRed
             else Result:= clGreen;
end;

// -----[ Form ]----------------------------------------------------------------

procedure TConfigForm.lbPalColorsDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Color: TColor;

begin
  TListBox(Control).Canvas.TextRect(Rect, Rect.Left, Rect.Top + 1, TListBox(Control).Items.Strings[Index]);
  Color:= TListBox(Control).Canvas.Brush.Color;
  if (cbPal.ItemIndex >= 0) and (cbPal.ItemIndex <= High(Config.PalList)) then
    case Index of
     0: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Fon;
     1: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Border;
     2: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.BackMotion;
     3: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.NaezdCh[1];
     4: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.OtezdCh[1];
     5: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.NaezdCh[2];
     6: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.OtezdCh[2];
     7: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.CrossedCh;
     8: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Text;
     9: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Event;
    10: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Defect;
    11: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.CoordNet;
    12: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Ruler;
    13: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.EnvelopeBS;
    14: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.LongLab;
    15: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.FiltrEcho;
    end else TListBox(Control).Canvas.Brush.Color:= clBlack;

  TListBox(Control).Canvas.FillRect(Classes.Rect(Rect.Right - 145, Rect.Top + 1, Rect.Right - 95, Rect.Bottom - 1));

  if (cbPal.ItemIndex >= 0) and (cbPal.ItemIndex <= High(Config.PalList)) then
    case Index of
     0: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Fon;
     1: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Border;
     2: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.BackMotion;
     3: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.NaezdCh[1];
     4: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.OtezdCh[1];
     5: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.NaezdCh[2];
     6: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.OtezdCh[2];
     7: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.CrossedCh;
     8: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Text;
     9: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Event;
    10: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Defect;
    11: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.CoordNet;
    12: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Ruler;
    13: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.EnvelopeBS;
    14: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.LongLab;
    15: TListBox(Control).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.FiltrEcho;
    end else TListBox(Control).Canvas.Brush.Color:= clBlack;

  TListBox(Control).Canvas.FillRect(Classes.Rect(Rect.Right  - 60, Rect.Top + 1, Rect.Right - 10, Rect.Bottom - 1));

  TListBox(Control).Canvas.Brush.Style:= bsClear;
  TListBox(Control).Canvas.Pen.Color:= clBlack;
  TListBox(Control).Canvas.Rectangle(Classes.Rect(Rect.Right -  60, Rect.Top + 1, Rect.Right - 10, Rect.Bottom - 1));
  TListBox(Control).Canvas.Rectangle(Classes.Rect(Rect.Right - 145, Rect.Top + 1, Rect.Right - 95, Rect.Bottom - 1));
  TListBox(Control).Canvas.Brush.Color:= Color;
end;

procedure TConfigForm.ChangePalNameClick(Sender: TObject);
var
  Value: string;

begin
  if (cbPal.ItemIndex >= 0) and (cbPal.ItemIndex <= High(Config.PalList)) then
  begin
    Value:= cbPal.Text;
    if InputQuery(LangTable.Caption['Config:Editing'], LangTable.Caption['Config:Enternewvalue'], Value) then Config.PalList[cbPal.ItemIndex].Name:= Value;
    CreatePalList(Self);
  end;
end;

procedure TConfigForm.NewPalClick(Sender: TObject);
var
  I: Integer;

begin
  I:= Length(Config.PalList);
  SetLength(Config.PalList, I + 1);
  Config.PalList[I].Name:= LangTable.Caption['Config:NewPalette']; // ����� �������

  Config.PalList[I].DC.Fon        := clWhite;
  Config.PalList[I].DC.Border     := clBlack;
  Config.PalList[I].DC.BackMotion := clYellow;
  Config.PalList[I].DC.NaezdCh[1]    := clRed;
  Config.PalList[I].DC.OtezdCh[1]    := clBlue;
  Config.PalList[I].DC.NaezdCh[2]    := clRed;
  Config.PalList[I].DC.OtezdCh[2]    := clBlue;
  Config.PalList[I].DC.CrossedCh  := clGreen;
  Config.PalList[I].DC.Text       := clBlack;
  Config.PalList[I].DC.Event      := clYellow;
  Config.PalList[I].DC.Defect     := clRed;
  Config.PalList[I].DC.CoordNet   := clSilver;
  Config.PalList[I].DC.Ruler      := $00E8E8E8;
  Config.PalList[I].DC.EnvelopeBS := clGreen;
  Config.PalList[I].DC.LongLab    := clRed;
  Config.PalList[I].DC.FiltrEcho  := $00DBDBDB;

  Config.PalList[I].PC.Fon        := clWhite;
  Config.PalList[I].PC.Border     := clBlack;
  Config.PalList[I].PC.BackMotion := clYellow;
  Config.PalList[I].PC.NaezdCh[1]    := clRed;
  Config.PalList[I].PC.OtezdCh[1]    := clBlue;
  Config.PalList[I].PC.NaezdCh[2]    := clRed;
  Config.PalList[I].PC.OtezdCh[2]    := clBlue;
  Config.PalList[I].PC.CrossedCh  := clGreen;
  Config.PalList[I].PC.Text       := clBlack;
  Config.PalList[I].PC.Event      := clYellow;
  Config.PalList[I].PC.Defect     := clRed;
  Config.PalList[I].PC.CoordNet   := clSilver;
  Config.PalList[I].PC.Ruler      := $00E8E8E8;
  Config.PalList[I].PC.EnvelopeBS := clGreen;
  Config.PalList[I].PC.LongLab    := clRed;
  Config.PalList[I].PC.FiltrEcho  := $00DBDBDB;

  CreatePalList(nil);
  cbPal.ItemIndex:= I;
  cbPal.OnChange(nil);
end;

procedure TConfigForm.DelPalClick(Sender: TObject);
var
  I: Integer;

begin
  if Length(Config.PalList) > 1 then
  begin
    if (cbPal.ItemIndex >= 0) and (cbPal.ItemIndex <= High(Config.PalList)) then
      if MessageBox(Handle, PChar(LangTable.Caption['Config:SureDelPal']), PChar(LangTable.Caption['Common:Attention']), 36) = IDYES then
      begin
        for I:= cbPal.ItemIndex to High(Config.PalList) - 1 do Config.PalList[I]:= Config.PalList[I + 1];
        SetLength(Config.PalList, Length(Config.PalList) - 1);
        CreatePalList(Self);
      end;
    if Config.ActivPal > High(Config.PalList) then Config.ActivPal:= High(Config.PalList);
  end;
end;

procedure TConfigForm.CopyPalClick(Sender: TObject);
var
  I: Integer;

begin
  if (cbPal.ItemIndex >= 0) and (cbPal.ItemIndex <= High(Config.PalList)) then
  begin
    I:= Length(Config.PalList);
    SetLength(Config.PalList, I + 1);
    Config.PalList[I]:= Config.PalList[cbPal.ItemIndex];
    Config.PalList[I].Name:= LangTable.Caption['Config:Copy'] + ' ' + Config.PalList[I].Name;
    CreatePalList(nil);
  end;
end;

procedure TConfigForm.CreatePalList(Sender: TObject);
var
  I, J: Integer;

begin
  I:= cbPal.ItemIndex;
  cbPal.Clear;
  for J:= 0 to High(Config.PalList) do cbPal.Items.Add(Config.PalList[J].Name);
  if Sender = nil then cbPal.ItemIndex:= I
                  else cbPal.ItemIndex:= High(Config.PalList);
  List.Refresh;
end;

function SelectDirCB(Wnd: HWND; uMsg: UINT; lParam, lpData: LPARAM): Integer stdcall;
begin
  if (uMsg = BFFM_INITIALIZED) and (lpData <> 0) then
    SendMessage(Wnd, BFFM_SETSELECTION, Integer(True), lpdata);
  result := 0;
end;


function MySelectDirectory(const Caption: string; const Root: WideString; var Directory: string): Boolean;
var
  WindowList: Pointer;
  BrowseInfo: TBrowseInfo;
  Buffer: PChar;
  OldErrorMode: Cardinal;
  RootItemIDList, ItemIDList: PItemIDList;
  ShellMalloc: IMalloc;
  IDesktopFolder: IShellFolder;
  Eaten, Flags: LongWord;

begin
  Result := False;
  if not DirectoryExists(Directory) then
    Directory := '';
  FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
  if (ShGetMalloc(ShellMalloc) = S_OK) and (ShellMalloc <> nil) then
  begin
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      RootItemIDList := nil;
      if Root <> '' then
      begin
        SHGetDesktopFolder(IDesktopFolder);
        IDesktopFolder.ParseDisplayName(Application.Handle, nil,
          POleStr(Root), Eaten, RootItemIDList, Flags);
      end;
      with BrowseInfo do
      begin
        hwndOwner := Application.Handle;
        pidlRoot := RootItemIDList;
        pszDisplayName := Buffer;
        lpszTitle := PChar(Caption);
        ulFlags := BIF_RETURNONLYFSDIRS or BIF_NEWDIALOGSTYLE;
        if Directory <> '' then
        begin
          lpfn := SelectDirCB;
          lParam := Integer(PChar(Directory));
        end;
      end;
      WindowList := DisableTaskWindows(0);
      OldErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS);
      try
        ItemIDList := ShBrowseForFolder(BrowseInfo);
      finally
        SetErrorMode(OldErrorMode);
        EnableTaskWindows(WindowList);
      end;
      Result :=  ItemIDList <> nil;
      if Result then
      begin
        ShGetPathFromIDList(ItemIDList, Buffer);
        ShellMalloc.Free(ItemIDList);
        Directory := Buffer;
      end;
    finally
      ShellMalloc.Free(Buffer);
    end;
  end;
end;


procedure TConfigForm.Button2Click(Sender: TObject);
var
  Dir: string;

begin
  Dir:= Config.VideoFilePath;
  if MySelectDirectory(LangTable.Caption['FileBase:Selectdirectory'], '', Dir) then
  begin
    TBEditItem1.Text:= Dir;
    Config.VideoFilePath:= Dir;
  end;
end;

procedure TConfigForm.Button3Click(Sender: TObject);
var
  Dir: string;

begin
  Dir:= Config.VideoLANDir;
  if MySelectDirectory(LangTable.Caption['FileBase:Selectdirectory'], '', Dir) then
  begin
    Edit1.Text:= Dir;
    Config.VideoLANDir:= Dir;
  end;
end;

procedure TConfigForm.Button4Click(Sender: TObject);
var
  I: Integer;

begin
{  if (cbPal.ItemIndex >= 0) and (cbPal.ItemIndex <= High(Config.PalList)) then
  begin
    Config.PalList[cbPal.ItemIndex].DC.Fon        := clWhite;
    Config.PalList[cbPal.ItemIndex].DC.Border     := clBlack;
    Config.PalList[cbPal.ItemIndex].DC.BackMotion := clYellow;
    Config.PalList[cbPal.ItemIndex].DC.NaezdCh1   := clRed;
    Config.PalList[cbPal.ItemIndex].DC.OtezdCh1   := clBlue;
    Config.PalList[cbPal.ItemIndex].DC.NaezdCh2   := clRed;
    Config.PalList[cbPal.ItemIndex].DC.OtezdCh2   := clBlue;
    Config.PalList[cbPal.ItemIndex].DC.CrossedCh  := clGreen;
    Config.PalList[cbPal.ItemIndex].DC.Text       := clBlack;
    Config.PalList[cbPal.ItemIndex].DC.Event      := clYellow;
    Config.PalList[cbPal.ItemIndex].DC.CoordNet   := clSilver;
    Config.PalList[cbPal.ItemIndex].DC.Ruler      := $00E8E8E8;
    Config.PalList[cbPal.ItemIndex].DC.EnvelopeBS := clGreen;
    Config.PalList[cbPal.ItemIndex].DC.LongLab    := clRed;
    List.Refresh;
  end;   }
end;

procedure TConfigForm.cbEstonisCoordSysytemChange(Sender: TObject);
begin
  Config.EstonisCoordSysytem:= cbEstonisCoordSysytem.ItemIndex;
end;

procedure TConfigForm.cbPalChange(Sender: TObject);
begin
  List.Refresh;
  if (cbPal.ItemIndex <> - 1) and (cbPal.ItemIndex >= 0) and (cbPal.ItemIndex <= High(Config.PalList)) then Config.ActivPal:= cbPal.ItemIndex;
end;
(*
procedure TConfigForm.ListDblClick(Sender: TObject);
var
  Color: TColor;

begin
  if (List.ItemIndex <> - 1) and
     (cbPal.ItemIndex >= 0) and
     (cbPal.ItemIndex <= High(Config.PalList)) then
  begin
    if ClickX > List.Width - 82 then
    begin
      case List.ItemIndex of
        0: Color:= Config.PalList[cbPal.ItemIndex].PC.Fon;            //
        1: Color:= Config.PalList[cbPal.ItemIndex].PC.Border;         //
        2: Color:= Config.PalList[cbPal.ItemIndex].PC.BackMotion;     //
        3: Color:= Config.PalList[cbPal.ItemIndex].PC.NaezdCh1;       //
        4: Color:= Config.PalList[cbPal.ItemIndex].PC.OtezdCh1;       //
        5: Color:= Config.PalList[cbPal.ItemIndex].PC.NaezdCh2;       //
        6: Color:= Config.PalList[cbPal.ItemIndex].PC.OtezdCh2;       //
        7: Color:= Config.PalList[cbPal.ItemIndex].PC.CrossedCh;      //
        8: Color:= Config.PalList[cbPal.ItemIndex].PC.Text;           //
        9: Color:= Config.PalList[cbPal.ItemIndex].PC.Event;          //
       10: Color:= Config.PalList[cbPal.ItemIndex].PC.Defect;         //
       11: Color:= Config.PalList[cbPal.ItemIndex].PC.CoordNet;       //
       12: Color:= Config.PalList[cbPal.ItemIndex].PC.Ruler;          //
       13: Color:= Config.PalList[cbPal.ItemIndex].PC.EnvelopeBS;     //
       14: Color:= Config.PalList[cbPal.ItemIndex].PC.LongLab;        //
       15: Color:= Config.PalList[cbPal.ItemIndex].PC.FiltrEcho;      //

      end;
    end
    else
    if ClickX > List.Width - 162 then
    begin
      case List.ItemIndex of
        0: Color:= Config.PalList[cbPal.ItemIndex].DC.Fon;
        1: Color:= Config.PalList[cbPal.ItemIndex].DC.Border;
        2: Color:= Config.PalList[cbPal.ItemIndex].DC.BackMotion;
        3: Color:= Config.PalList[cbPal.ItemIndex].DC.NaezdCh1;
        4: Color:= Config.PalList[cbPal.ItemIndex].DC.OtezdCh1;
        5: Color:= Config.PalList[cbPal.ItemIndex].DC.NaezdCh2;
        6: Color:= Config.PalList[cbPal.ItemIndex].DC.OtezdCh2;
        7: Color:= Config.PalList[cbPal.ItemIndex].DC.CrossedCh;
        8: Color:= Config.PalList[cbPal.ItemIndex].DC.Text;
        9: Color:= Config.PalList[cbPal.ItemIndex].DC.Event;
       10: Color:= Config.PalList[cbPal.ItemIndex].DC.Defect;
       11: Color:= Config.PalList[cbPal.ItemIndex].DC.CoordNet;
       12: Color:= Config.PalList[cbPal.ItemIndex].DC.Ruler;
       13: Color:= Config.PalList[cbPal.ItemIndex].DC.EnvelopeBS;
       14: Color:= Config.PalList[cbPal.ItemIndex].DC.LongLab;
       15: Color:= Config.PalList[cbPal.ItemIndex].DC.FiltrEcho;
      end;
    end
    else Exit;

    ColorDialog.Color:= Color;
    if ColorDialog.Execute then
    begin
//      Move(Config.PalList[cbPal.ItemIndex].DC, Save1, SizeOf(TDisplayColors));
//      Move(Config.PalList[cbPal.ItemIndex].PC, Save2, SizeOf(TDisplayColors));

      if ClickX > List.Width - 82 then
      begin
        case List.ItemIndex of
           0: Config.PalList[cbPal.ItemIndex].PC.Fon        := ColorDialog.Color;
           1: Config.PalList[cbPal.ItemIndex].PC.Border     := ColorDialog.Color;
           2: Config.PalList[cbPal.ItemIndex].PC.BackMotion := ColorDialog.Color;
           3: Config.PalList[cbPal.ItemIndex].PC.NaezdCh1   := ColorDialog.Color;
           4: Config.PalList[cbPal.ItemIndex].PC.OtezdCh1   := ColorDialog.Color;
           5: Config.PalList[cbPal.ItemIndex].PC.NaezdCh2   := ColorDialog.Color;
           6: Config.PalList[cbPal.ItemIndex].PC.OtezdCh2   := ColorDialog.Color;
           7: Config.PalList[cbPal.ItemIndex].PC.CrossedCh  := ColorDialog.Color;
           8: Config.PalList[cbPal.ItemIndex].PC.Text       := ColorDialog.Color;
           9: Config.PalList[cbPal.ItemIndex].PC.Event      := ColorDialog.Color;
          10: Config.PalList[cbPal.ItemIndex].PC.Defect     := ColorDialog.Color;
          11: Config.PalList[cbPal.ItemIndex].PC.CoordNet   := ColorDialog.Color;
          12: Config.PalList[cbPal.ItemIndex].PC.Ruler      := ColorDialog.Color;
          13: Config.PalList[cbPal.ItemIndex].PC.EnvelopeBS := ColorDialog.Color;
          14: Config.PalList[cbPal.ItemIndex].PC.LongLab    := ColorDialog.Color;
          15: Config.PalList[cbPal.ItemIndex].PC.FiltrEcho  := ColorDialog.Color;
        end;
      end
      else
      if ClickX > List.Width - 162 then
      begin
        case List.ItemIndex of
           0: Config.PalList[cbPal.ItemIndex].DC.Fon        := ColorDialog.Color;
           1: Config.PalList[cbPal.ItemIndex].DC.Border     := ColorDialog.Color;
           2: Config.PalList[cbPal.ItemIndex].DC.BackMotion := ColorDialog.Color;
           3: Config.PalList[cbPal.ItemIndex].DC.NaezdCh1   := ColorDialog.Color;
           4: Config.PalList[cbPal.ItemIndex].DC.OtezdCh1   := ColorDialog.Color;
           5: Config.PalList[cbPal.ItemIndex].DC.NaezdCh2   := ColorDialog.Color;
           6: Config.PalList[cbPal.ItemIndex].DC.OtezdCh2   := ColorDialog.Color;
           7: Config.PalList[cbPal.ItemIndex].DC.CrossedCh  := ColorDialog.Color;
           8: Config.PalList[cbPal.ItemIndex].DC.Text       := ColorDialog.Color;
           9: Config.PalList[cbPal.ItemIndex].DC.Event      := ColorDialog.Color;
          10: Config.PalList[cbPal.ItemIndex].DC.Defect     := ColorDialog.Color;
          11: Config.PalList[cbPal.ItemIndex].DC.CoordNet   := ColorDialog.Color;
          12: Config.PalList[cbPal.ItemIndex].DC.Ruler      := ColorDialog.Color;
          13: Config.PalList[cbPal.ItemIndex].DC.EnvelopeBS := ColorDialog.Color;
          14: Config.PalList[cbPal.ItemIndex].DC.LongLab    := ColorDialog.Color;
          15: Config.PalList[cbPal.ItemIndex].DC.FiltrEcho  := ColorDialog.Color;
        end;
      end;
    end;
  end;
  List.Refresh;
end;
*)
procedure TConfigForm.FormShow(Sender: TObject);
var
  I: Integer;
  Russian_: Boolean;
  English_: Boolean;
  Turkish_: Boolean;
  French_: Boolean;
  Spanish_: Boolean;
  Hungarian_: Boolean;
  German_: Boolean;

begin
  SkipFlg:= True;
  CreatePalList(Self);
  if (Config.ActivPal >= 0) and (Config.ActivPal < cbPal.Items.Count) then cbPal.ItemIndex:= Config.ActivPal;
  if (Config.EstonisCoordSysytem >= 0) and (Config.EstonisCoordSysytem < cbEstonisCoordSysytem.Items.Count) then cbEstonisCoordSysytem.ItemIndex:= Config.EstonisCoordSysytem;


  cbOpenMaxim.Checked:= Config.OpenMaximazed;

  cbWorkLang.Items.Clear;
  for I := 0 to LangTable.GroupsCount - 1 do
  begin

    Russian_  := LangTable.GroupName[I] = 'Russian';
    English_  := LangTable.GroupName[I] = 'English';
    Turkish_  := LangTable.GroupName[I] = 'Turkish';
    French_   := LangTable.GroupName[I] = 'French';
    Spanish_  := LangTable.GroupName[I] = 'Spanish';
    Hungarian_:= LangTable.GroupName[I] = 'Hungarian';
    German_   := LangTable.GroupName[I] = 'German';

    if ((Config.ProgramMode = pmRADIOAVIONICA_MAV) and (not French_) and (not Turkish_)) or
       ((Config.ProgramMode = pmGEISMAR) and (not Spanish_) and (not German_) and (not Hungarian_)) or
       ((Config.ProgramMode = pmRADIOAVIONICA) and (Russian_ {or English_}))  or
       ((Config.ProgramMode = pmRADIOAVIONICA_KZ) and (Russian_ {or English_})) then cbWorkLang.Items.Add(LangTable.GroupName[I]);

  end;
  cbWorkLang.Sorted:= true;
  cbWorkLang.ItemIndex:= cbWorkLang.Items.IndexOf(Config.WorkLang);
{
  clbFileNameStruc.Items.Clear;
  for I:= 0 to High(Config.FNStruc) do
  begin
    clbFileNameStruc.Items.AddObject(Language.GetCaption(Config.FNStruc[I].Idx), Pointer(Config.FNStruc[I].Idx));
    clbFileNameStruc.Checked[I]:= Config.FNStruc[I].On_;
  end;

  clbViewEvent.Checked[0]  := Config.DisplayConfig.ShowEvent[$90]; // ��������� �������� ����������������
  clbViewEvent.Checked[1]  := Config.DisplayConfig.ShowEvent[$91]; // ��������� ��������� ���� ����������� (0 �� �������� ����������������)
  clbViewEvent.Checked[2]  := Config.DisplayConfig.ShowEvent[$92]; // ��������� ���
  clbViewEvent.Checked[3]  := Config.DisplayConfig.ShowEvent[$93]; // ��������� ��������� ������ ������
  clbViewEvent.Checked[4]  := Config.DisplayConfig.ShowEvent[$94]; // ��������� ��������� ����� ������
  clbViewEvent.Checked[5]  := Config.DisplayConfig.ShowEvent[$95]; // ������ ���������� ���������
  clbViewEvent.Checked[6]  := Config.DisplayConfig.ShowEvent[$96]; // ��������� ������
  clbViewEvent.Checked[7]  := Config.DisplayConfig.ShowEvent[$99]; // ��������� 2��
  clbViewEvent.Checked[8]  := Config.DisplayConfig.ShowEvent[$9A]; // ��������� ������ ������������ ��������
  clbViewEvent.Checked[9]  := Config.DisplayConfig.ShowEvent[$9B]; // ��������� �� ��� ������
  clbViewEvent.Checked[10] := Config.DisplayConfig.ShowEvent[$A4]; // ������� ������ ��
  clbViewEvent.Checked[11] := Config.DisplayConfig.ShowEvent[$A5]; // ���������� ������ ��
  clbViewEvent.Checked[12] := Config.DisplayConfig.ShowEvent[$A6]; // ������� �������
  clbViewEvent.Checked[13] := Config.DisplayConfig.ShowEvent[$A7]; // ������ ����������� ����� + �����
  clbViewEvent.Checked[14] := Config.DisplayConfig.ShowEvent[$A8]; // ����� ����������� ����� + �����
  clbViewEvent.Checked[15] := Config.DisplayConfig.ShowEvent[$A9]; // ������� ������ ������� ���� + �����
  clbViewEvent.Checked[16] := Config.DisplayConfig.ShowEvent[$AA]; // ���������� ������ ������� ���� + �����
  clbViewEvent.Checked[17] := Config.DisplayConfig.ShowEvent[$AB]; // ����� ���������
  clbViewEvent.Checked[18] := Config.DisplayConfig.ShowEvent[$AC]; // ����� ������ ����
  clbViewEvent.Checked[19] := Config.DisplayConfig.ShowEvent[$AD]; // ����� ����������� ��������
}
  tbWheelStep.Position:= Config.WheelStep;
  cbWheelDir.Checked:= Config.WheelDir;
  CheckBox3.Checked:= Config.ShowUSBLog;
  cbMeasurementMode.ItemIndex:= Ord(Config.MeasurementMode);
  cbZeroProbeLineMode.ItemIndex:= Ord(Config.ZeroProbeChannelsViewMode);

  cbRuler.Checked          := Config.DisplayConfig.Ruler;
  cbCoordNet.Checked       := Config.DisplayConfig.CoordNet;
//  leDawdFileSpace.Text     := Config.FNSpace;
  cbCoordType.Checked      := Config.CoordType;
  cbAddDatainCoord.Checked := Config.DisplayConfig.AddDatainCoord;

  RadioButton2.Checked     := Config.InvertDelayAxis;
  RadioButton1.Checked     := not Config.InvertDelayAxis;
  CheckBox1.Checked        := Config.ButtonText;
  CheckBox2.Checked        := Config.ViewSelMetod;
  cbThreeColor.Checked     := Config.UseThreeColor;
  TrackBar1.Position       := Config.MinColPer;
  TrackBar1Change(nil);

  Ch0MinEdit.Text:= IntToStr(Config.KuZones[1].Min);
  Ch0MaxEdit.Text:= IntToStr(Config.KuZones[1].Max);
  Ch1MinEdit.Text:= IntToStr(Config.KuZones[2].Min);
  Ch1MaxEdit.Text:= IntToStr(Config.KuZones[2].Max);
  Ch2MinEdit.Text:= IntToStr(Config.KuZones[3].Min);
  Ch2MaxEdit.Text:= IntToStr(Config.KuZones[3].Max);
  Ch4MinEdit.Text:= IntToStr(Config.KuZones[4].Min);
  Ch4MaxEdit.Text:= IntToStr(Config.KuZones[4].Max);
  Ch6MinEdit.Text:= IntToStr(Config.KuZones[5].Min);
  Ch6MaxEdit.Text:= IntToStr(Config.KuZones[5].Max);
  Ch8MinEdit.Text:= IntToStr(Config.KuZones[6].Min);
  Ch8MaxEdit.Text:= IntToStr(Config.KuZones[6].Max);

  TBEditItem1.Text:= Config.VideoFilePath;
  Edit1.Text:= Config.VideoLANDir;

  SpeedEdit.Text:= Format('%3.2f', [Config.MaxSpeed]);
//  ASUPCheckBox.Checked:= Config.ASUP;

  SkipFlg:= False;
end;

procedure TConfigForm.ListCustomDrawSubItem(Sender: TCustomListView; Item: TListItem; SubItem: Integer; State: TCustomDrawState; var DefaultDraw: Boolean);
var
  Save: TColor;

function GetRect(Idx: Integer): TRect;
var
  I: Integer;

begin
   Result:= Item.DisplayRect(drBounds);
   Result.Left:= 0;
   for I := 0 to Idx - 1 do Result.Left:= Result.Left + TCustomListView(Sender).Column[I].Width;
   Result.Left:= Result.Left - 1;
   Result.Right:= Result.Left + TCustomListView(Sender).Column[Idx].Width;
   Inc(Result.Left, 2);
   Dec(Result.Right, 2);
   Inc(Result.Top, 1);
   Dec(Result.Bottom, 1);
end;

begin
  if (cbPal.ItemIndex >= 0) and (cbPal.ItemIndex <= High(Config.PalList)) then
  begin
    Save:= TListView(Sender).Canvas.Brush.Color;

    case Item.Index of
       0: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Fon;
       1: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Border;
       2: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.BackMotion;
       3: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.NaezdCh[1];
       4: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.OtezdCh[1];
       5: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.NaezdCh[2];
       6: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.OtezdCh[2];
       7: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.CrossedCh;
       8: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Text;
       9: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Event;
      10: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Defect;
      11: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.CoordNet;
      12: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.Ruler;
      13: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.EnvelopeBS;
      14: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.LongLab;
      15: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].DC.FiltrEcho;
      else TListView(Sender).Canvas.Brush.Color:= clBlack;
    end;

    TListView(Sender).Canvas.FillRect(GetRect(1));
    TListView(Sender).Canvas.Rectangle(GetRect(1));

    case Item.Index of
       0: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Fon;
       1: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Border;
       2: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.BackMotion;
       3: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.NaezdCh[1];
       4: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.OtezdCh[1];
       5: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.NaezdCh[2];
       6: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.OtezdCh[2];
       7: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.CrossedCh;
       8: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Text;
       9: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Event;
      10: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Defect;
      11: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.CoordNet;
      12: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.Ruler;
      13: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.EnvelopeBS;
      14: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.LongLab;
      15: TListView(Sender).Canvas.Brush.Color:= Config.PalList[cbPal.ItemIndex].PC.FiltrEcho;
      else TListView(Sender).Canvas.Brush.Color:= clBlack;
    end;

    TListView(Sender).Canvas.FillRect(GetRect(2));
    TListView(Sender).Canvas.Rectangle(GetRect(2));

    TListView(Sender).Canvas.Brush.Color:= Save;
  end;
end;

procedure TConfigForm.ListDblClick(Sender: TObject);
var
  Color: TColor;

function GetColumnBoundsRect(Idx: Integer): TPoint;
var
  I: Integer;

begin
   Result.X:= 0;
   for I := 0 to Idx - 1 do Result.X:= Result.X + TCustomListView(Sender).Column[I].Width;
   Result.Y:= Result.X + TCustomListView(Sender).Column[Idx].Width;
end;

begin
  if (List.ItemIndex <> - 1) and
     (cbPal.ItemIndex >= 0) and
     (cbPal.ItemIndex <= High(Config.PalList)) then
  begin
    if (ClickX >= GetColumnBoundsRect(2).X) and
       (ClickX <= GetColumnBoundsRect(2).Y) then
    begin
      case List.ItemIndex of
        0: Color:= Config.PalList[cbPal.ItemIndex].PC.Fon;            //
        1: Color:= Config.PalList[cbPal.ItemIndex].PC.Border;         //
        2: Color:= Config.PalList[cbPal.ItemIndex].PC.BackMotion;     //
        3: Color:= Config.PalList[cbPal.ItemIndex].PC.NaezdCh[1];       //
        4: Color:= Config.PalList[cbPal.ItemIndex].PC.OtezdCh[1];       //
        5: Color:= Config.PalList[cbPal.ItemIndex].PC.NaezdCh[2];       //
        6: Color:= Config.PalList[cbPal.ItemIndex].PC.OtezdCh[2];       //
        7: Color:= Config.PalList[cbPal.ItemIndex].PC.CrossedCh;      //
        8: Color:= Config.PalList[cbPal.ItemIndex].PC.Text;           //
        9: Color:= Config.PalList[cbPal.ItemIndex].PC.Event;          //
       10: Color:= Config.PalList[cbPal.ItemIndex].PC.Defect;         //
       11: Color:= Config.PalList[cbPal.ItemIndex].PC.CoordNet;       //
       12: Color:= Config.PalList[cbPal.ItemIndex].PC.Ruler;          //
       13: Color:= Config.PalList[cbPal.ItemIndex].PC.EnvelopeBS;     //
       14: Color:= Config.PalList[cbPal.ItemIndex].PC.LongLab;        //
       15: Color:= Config.PalList[cbPal.ItemIndex].PC.FiltrEcho;      //

      end;
    end
    else
    if (ClickX >= GetColumnBoundsRect(1).X) and
       (ClickX <= GetColumnBoundsRect(1).Y) then
    begin
      case List.ItemIndex of
        0: Color:= Config.PalList[cbPal.ItemIndex].DC.Fon;
        1: Color:= Config.PalList[cbPal.ItemIndex].DC.Border;
        2: Color:= Config.PalList[cbPal.ItemIndex].DC.BackMotion;
        3: Color:= Config.PalList[cbPal.ItemIndex].DC.NaezdCh[1];
        4: Color:= Config.PalList[cbPal.ItemIndex].DC.OtezdCh[1];
        5: Color:= Config.PalList[cbPal.ItemIndex].DC.NaezdCh[2];
        6: Color:= Config.PalList[cbPal.ItemIndex].DC.OtezdCh[2];
        7: Color:= Config.PalList[cbPal.ItemIndex].DC.CrossedCh;
        8: Color:= Config.PalList[cbPal.ItemIndex].DC.Text;
        9: Color:= Config.PalList[cbPal.ItemIndex].DC.Event;
       10: Color:= Config.PalList[cbPal.ItemIndex].DC.Defect;
       11: Color:= Config.PalList[cbPal.ItemIndex].DC.CoordNet;
       12: Color:= Config.PalList[cbPal.ItemIndex].DC.Ruler;
       13: Color:= Config.PalList[cbPal.ItemIndex].DC.EnvelopeBS;
       14: Color:= Config.PalList[cbPal.ItemIndex].DC.LongLab;
       15: Color:= Config.PalList[cbPal.ItemIndex].DC.FiltrEcho;
      end;
    end
    else Exit;

    ColorDialog.Color:= Color;
    if ColorDialog.Execute then
    begin
      if (ClickX >= GetColumnBoundsRect(2).X) and
         (ClickX <= GetColumnBoundsRect(2).Y) then
      begin
        case List.ItemIndex of
           0: Config.PalList[cbPal.ItemIndex].PC.Fon        := ColorDialog.Color;
           1: Config.PalList[cbPal.ItemIndex].PC.Border     := ColorDialog.Color;
           2: Config.PalList[cbPal.ItemIndex].PC.BackMotion := ColorDialog.Color;
           3: Config.PalList[cbPal.ItemIndex].PC.NaezdCh[1]   := ColorDialog.Color;
           4: Config.PalList[cbPal.ItemIndex].PC.OtezdCh[1]   := ColorDialog.Color;
           5: Config.PalList[cbPal.ItemIndex].PC.NaezdCh[2]   := ColorDialog.Color;
           6: Config.PalList[cbPal.ItemIndex].PC.OtezdCh[2]   := ColorDialog.Color;
           7: Config.PalList[cbPal.ItemIndex].PC.CrossedCh  := ColorDialog.Color;
           8: Config.PalList[cbPal.ItemIndex].PC.Text       := ColorDialog.Color;
           9: Config.PalList[cbPal.ItemIndex].PC.Event      := ColorDialog.Color;
          10: Config.PalList[cbPal.ItemIndex].PC.Defect     := ColorDialog.Color;
          11: Config.PalList[cbPal.ItemIndex].PC.CoordNet   := ColorDialog.Color;
          12: Config.PalList[cbPal.ItemIndex].PC.Ruler      := ColorDialog.Color;
          13: Config.PalList[cbPal.ItemIndex].PC.EnvelopeBS := ColorDialog.Color;
          14: Config.PalList[cbPal.ItemIndex].PC.LongLab    := ColorDialog.Color;
          15: Config.PalList[cbPal.ItemIndex].PC.FiltrEcho  := ColorDialog.Color;
        end;
      end
      else
      if (ClickX >= GetColumnBoundsRect(1).X) and
         (ClickX <= GetColumnBoundsRect(1).Y) then
      begin
        case List.ItemIndex of
           0: Config.PalList[cbPal.ItemIndex].DC.Fon        := ColorDialog.Color;
           1: Config.PalList[cbPal.ItemIndex].DC.Border     := ColorDialog.Color;
           2: Config.PalList[cbPal.ItemIndex].DC.BackMotion := ColorDialog.Color;
           3: Config.PalList[cbPal.ItemIndex].DC.NaezdCh[1]   := ColorDialog.Color;
           4: Config.PalList[cbPal.ItemIndex].DC.OtezdCh[1]   := ColorDialog.Color;
           5: Config.PalList[cbPal.ItemIndex].DC.NaezdCh[2]   := ColorDialog.Color;
           6: Config.PalList[cbPal.ItemIndex].DC.OtezdCh[2]   := ColorDialog.Color;
           7: Config.PalList[cbPal.ItemIndex].DC.CrossedCh  := ColorDialog.Color;
           8: Config.PalList[cbPal.ItemIndex].DC.Text       := ColorDialog.Color;
           9: Config.PalList[cbPal.ItemIndex].DC.Event      := ColorDialog.Color;
          10: Config.PalList[cbPal.ItemIndex].DC.Defect     := ColorDialog.Color;
          11: Config.PalList[cbPal.ItemIndex].DC.CoordNet   := ColorDialog.Color;
          12: Config.PalList[cbPal.ItemIndex].DC.Ruler      := ColorDialog.Color;
          13: Config.PalList[cbPal.ItemIndex].DC.EnvelopeBS := ColorDialog.Color;
          14: Config.PalList[cbPal.ItemIndex].DC.LongLab    := ColorDialog.Color;
          15: Config.PalList[cbPal.ItemIndex].DC.FiltrEcho  := ColorDialog.Color;
        end;
      end;
    end;
  end;
  List.Refresh;
end;

procedure TConfigForm.ListMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  ClickX:= X;
end;

procedure TConfigForm.ControlToConfig(Sender: TObject);
var
  I: Integer;
  Code: Integer;
  Res: Single;

function MyStrToInt(Str: string): Integer;
begin
  Val(Str, Result, Code);
end;

begin
  if SkipFlg then Exit;
  Config.OpenMaximazed:= cbOpenMaxim.Checked;

  if cbWorkLang.ItemIndex <> - 1 then Config.WorkLang:= cbWorkLang.Items[cbWorkLang.ItemIndex];
{
  for I:= 0 to clbFileNameStruc.Count - 1  do
  begin
    Config.FNStruc[I].Idx:= Integer(clbFileNameStruc.Items.Objects[I]);
    Config.FNStruc[I].On_:= clbFileNameStruc.Checked[I];
  end;

  Config.DisplayConfig.ShowEvent[$90]:= clbViewEvent.Checked[0];  // ��������� �������� ����������������
  Config.DisplayConfig.ShowEvent[$91]:= clbViewEvent.Checked[1];  // ��������� ��������� ���� ����������� (0 �� �������� ����������������)
  Config.DisplayConfig.ShowEvent[$92]:= clbViewEvent.Checked[2];  // ��������� ���
  Config.DisplayConfig.ShowEvent[$93]:= clbViewEvent.Checked[3];  // ��������� ��������� ������ ������
  Config.DisplayConfig.ShowEvent[$94]:= clbViewEvent.Checked[4];  // ��������� ��������� ����� ������
  Config.DisplayConfig.ShowEvent[$95]:= clbViewEvent.Checked[5];  // ������ ���������� ���������
  Config.DisplayConfig.ShowEvent[$96]:= clbViewEvent.Checked[6];  // ��������� ������
  Config.DisplayConfig.ShowEvent[$99]:= clbViewEvent.Checked[7];  // ��������� 2��
  Config.DisplayConfig.ShowEvent[$9A]:= clbViewEvent.Checked[8];  // ��������� ������ ������������ ��������
  Config.DisplayConfig.ShowEvent[$9B]:= clbViewEvent.Checked[9];  // ��������� �� ��� ������
  Config.DisplayConfig.ShowEvent[$A4]:= clbViewEvent.Checked[10]; // ������� ������ ��
  Config.DisplayConfig.ShowEvent[$A5]:= clbViewEvent.Checked[11]; // ���������� ������ ��
  Config.DisplayConfig.ShowEvent[$A6]:= clbViewEvent.Checked[12]; // ������� �������
  Config.DisplayConfig.ShowEvent[$A7]:= clbViewEvent.Checked[13]; // ������ ����������� ����� + �����
  Config.DisplayConfig.ShowEvent[$A8]:= clbViewEvent.Checked[14]; // ����� ����������� ����� + �����
  Config.DisplayConfig.ShowEvent[$A9]:= clbViewEvent.Checked[15]; // ������� ������ ������� ���� + �����
  Config.DisplayConfig.ShowEvent[$AA]:= clbViewEvent.Checked[16]; // ���������� ������ ������� ���� + �����
  Config.DisplayConfig.ShowEvent[$AB]:= clbViewEvent.Checked[17]; // ����� ���������
  Config.DisplayConfig.ShowEvent[$AC]:= clbViewEvent.Checked[18]; // ����� ������ ����
  Config.DisplayConfig.ShowEvent[$AD]:= clbViewEvent.Checked[19]; // ����� ����������� ��������
}
  Config.DisplayConfig.Ruler    := cbRuler.Checked;
  Config.DisplayConfig.AddDatainCoord    := cbAddDatainCoord.Checked;
  Config.DisplayConfig.CoordNet := cbCoordNet.Checked;
//  Config.FNSpace                := leDawdFileSpace.Text;
  Config.CoordType              := cbCoordType.Checked;
  Config.InvertDelayAxis        := RadioButton2.Checked;
  Config.ButtonText             := CheckBox1.Checked;
  Config.ViewSelMetod           := CheckBox2.Checked;
  Config.UseThreeColor          := cbThreeColor.Checked;
  Config.MinColPer              := TrackBar1.Position;

  Config.WheelStep                := tbWheelStep.Position;
  Config.WheelDir                 := cbWheelDir.Checked;

  Config.MeasurementMode          := TMeasurementMode(cbMeasurementMode.ItemIndex);
  Config.ZeroProbeChannelsViewMode:= T_CFG_ZeroProbeChannelsViewMode(cbZeroProbeLineMode.ItemIndex);
  Config.ShowUSBLog               := CheckBox3.Checked;


{
  Config.KuZones[1].Min:= MyStrToInt(Ch0MinEdit.Text);
  Config.KuZones[1].Max:= MyStrToInt(Ch0MaxEdit.Text);
  Config.KuZones[2].Min:= MyStrToInt(Ch1MinEdit.Text);
  Config.KuZones[2].Max:= MyStrToInt(Ch1MaxEdit.Text);
  Config.KuZones[3].Min:= MyStrToInt(Ch2MinEdit.Text);
  Config.KuZones[3].Max:= MyStrToInt(Ch2MaxEdit.Text);
  Config.KuZones[4].Min:= MyStrToInt(Ch4MinEdit.Text);
  Config.KuZones[4].Max:= MyStrToInt(Ch4MaxEdit.Text);
  Config.KuZones[5].Min:= MyStrToInt(Ch6MinEdit.Text);
  Config.KuZones[5].Max:= MyStrToInt(Ch6MaxEdit.Text);
  Config.KuZones[6].Min:= MyStrToInt(Ch8MinEdit.Text);
  Config.KuZones[6].Max:= MyStrToInt(Ch8MaxEdit.Text);
  Val(SpeedEdit.Text, Res, Code);
  if Code = 0 then Config.MaxSpeed:= Res;
  Config.ASUP:= ASUPCheckBox.Checked;
}
end;

procedure TConfigForm.OnChangeLanguage(Sender: TObject);
var
  I: Integer;
  Tmp: array of boolean;

procedure Add(S: string);
var
  Item: TListItem;

begin
  Item:= List.Items.Add;
  Item.Caption:= S;
  Item.SubItems.Add('');
  Item.SubItems.Add('');
end;

begin
{
  clbFileNameStruc.Items.Clear;
  for I:= 0 to High(Config.FNStruc) do
  begin
    clbFileNameStruc.Items.AddObject(Language.GetCaption(Config.FNStruc[I].Idx), Pointer(Config.FNStruc[I].Idx));
    clbFileNameStruc.Checked[I]:= Config.FNStruc[I].On_;
  end;
}
  Self.Caption                := LangTable.Caption['Config:ProgramSettings'];
  Self.Font.Name              := LangTable.Caption['General:FontName'];
  TabSheet1.Caption           := LangTable.Caption['Config:Common'];
  TabSheet2.Caption           := LangTable.Caption['Config:Colors'];
  TabSheet3.Caption           := LangTable.Caption['Config:Display'];
//  Label2.Caption              := LangTable.Caption['Config:ReadingFileName'];
//  leDawdFileSpace.EditLabel.Caption:= LangTable.Caption['Config:Spacer'];
//  Button2.Caption             := LangTable.Caption['Config:MoveUp'];
//  Button3.Caption             := LangTable.Caption['Config:MoveDown'];
  Label1.Caption              := LangTable.Caption['Config:ProgrammLanguage'];
  Label1.BringToFront;
//  Bevel11.SendToBack;
//  Label3.Caption              := LangTable.Caption['Config:Misc'];
  cbOpenMaxim.Caption         := LangTable.Caption['Config:Openfileinmaximazedwindows'];
  Label4.Caption              := LangTable.Caption['Config:Palette'];
  Label5.Caption              := LangTable.Caption['Config:Colors'];
  ChangePalName.Caption       := LangTable.Caption['Config:Name'];
  NewPal.Caption              := LangTable.Caption['Config:New'];
  DelPal.Caption              := LangTable.Caption['Config:Delete'];
  CopyPal.Caption             := LangTable.Caption['Config:Duplicate'];

  pZeroProbeTapes.Caption     := LangTable.Caption['Config:ZeroProbeTapes'];

  cbZeroProbeLineMode.Items.Clear;
  cbZeroProbeLineMode.Items.Add(LangTable.Caption['Config:ZeroProbeBothTapes']);
  cbZeroProbeLineMode.Items.Add(LangTable.Caption['Config:ZeroProbeFirstTapes']);
  cbZeroProbeLineMode.Items.Add(LangTable.Caption['Config:ZeroProbeSecondTapes']);

  pMouse.Caption              := LangTable.Caption['Config:Mouse'];
  lMouseShift.Caption         := LangTable.Caption['Config:MouseShift'];
  cbWheelDir.Caption          := LangTable.Caption['Config:WheelDir'];
  pMeasMode.Caption           := LangTable.Caption['Config:MeasureMode'];

  cbMeasurementMode.Items.Clear;
  cbMeasurementMode.Items.Add(LangTable.Caption['Config:MeasureMode_Millimeters']);
  cbMeasurementMode.Items.Add(LangTable.Caption['Config:MeasureMode_Inches']);

{
  cbMeasurementMode.Items.Clear;
  cbMeasurementMode.Items.Add(LangTable.Caption['Config:Duplicate']);
  cbMeasurementMode.Items.Add(LangTable.Caption['Config:Duplicate']);
  cbMeasurementMode.Items.Add(LangTable.Caption['Config:Duplicate']);
}
//  Panel7.Caption              := LangTable.Caption['Config:Name'];
//  Panel6.Caption              := LangTable.Caption['Config:Display'];
//  Panel5.Caption              := LangTable.Caption['Config:Printer'];

  List.Columns[0].Width       := 180;
  List.Columns[1].Width       := 60;
  List.Columns[2].Width       := 60;
  List.Columns[0].Caption     := LangTable.Caption['Config:Name'];
  List.Columns[1].Caption     := LangTable.Caption['Config:Display'];
  List.Columns[2].Caption     := LangTable.Caption['Config:Printer'];

  Label6.Caption              := LangTable.Caption['Config:Displayevents'];
  Label7.Caption              := LangTable.Caption['Config:Misc'];
  cbRuler.Caption             := LangTable.Caption['Config:Ruler'];
  cbCoordNet.Caption          := LangTable.Caption['Config:NetworkofCoordinates'];
//  cbCoordType.Caption         := LangTable.Caption['InfoBar_:Nearestmarks'];
  StaticText2.Caption         := LangTable.Caption['Config:ToolBar'];
  StaticText3.Caption         := LangTable.Caption['Config:BScan'];

  if Config.ProgramMode = pmRADIOAVIONICA_MAV then
  begin
    StaticText1.Caption         := LangTable.Caption['Config:DelayAxis_MAV'];
    RadioButton1.Caption        := LangTable.Caption['Config:Direct_MAV'];
    RadioButton2.Caption        := LangTable.Caption['Config:Inverse_MAV'];
  end
  else
  begin
    StaticText1.Caption         := LangTable.Caption['Config:DelayAxis'];
    RadioButton1.Caption        := LangTable.Caption['Config:Direct'];
    RadioButton2.Caption        := LangTable.Caption['Config:Inverse'];
  end;

  CheckBox1.Caption           := LangTable.Caption['Config:Showbuttontext'];
  CheckBox2.Caption           := LangTable.Caption['Config:Cyclechangeofviewmode'];
  cbThreeColor.Caption        := LangTable.Caption['Config:Usediferentcolorforintersectechosignals'];
  Label9.Caption              := LangTable.Caption['Config:Minimumaplitudesignalbright'];

{
  TabSheet4.Caption           := LangTable.Caption['Config:Miscellaneous'];
  StaticText6.Caption         := LangTable.Caption['Config:Sensativityzones']; // ' ���������������� ';
  Label12.Caption             := LangTable.Caption['Config:Channel0-from']; // '����� 0      -  ��';
  Label13.Caption             := LangTable.Caption['Config:Channel1-from']; // '����� 1      -  ��';
  Label14.Caption             := LangTable.Caption['Config:Channel2,3-from']; // '����� 2, 3  -  ��';
  Label15.Caption             := LangTable.Caption['Config:Channel4,5-from']; // '����� 4, 5  -  ��';
  Label16.Caption             := LangTable.Caption['Config:Channel6,7-from']; // '����� 6, 7  -  ��';
  Label17.Caption             := LangTable.Caption['Config:Channel8,9-from']; // '����� 8, 9  -  ��';
  Label11.Caption             := LangTable.Caption['Config:dB-to']; // '��   - �� ';
  Label19.Caption             := LangTable.Caption['Config:dB-to']; // '��   - �� ';
  Label22.Caption             := LangTable.Caption['Config:dB-to']; // '��   - �� ';
  Label21.Caption             := LangTable.Caption['Config:dB-to']; // '��   - �� ';
  Label26.Caption             := LangTable.Caption['Config:dB-to']; // '��   - �� ';
  Label25.Caption             := LangTable.Caption['Config:dB-to']; // '��   - �� ';
  Label18.Caption             := LangTable.Caption['Config:dB']; // '��';
  Label20.Caption             := LangTable.Caption['Config:dB']; // '��';
  Label23.Caption             := LangTable.Caption['Config:dB']; // '��';
  Label24.Caption             := LangTable.Caption['Config:dB']; // '��';
  Label27.Caption             := LangTable.Caption['Config:dB']; // '��';
  Label28.Caption             := LangTable.Caption['Config:dB']; // '��';
}

{
  List.Items.Clear;

  List.Items.Add(' ' + LangTable.Caption['Config:Defectogrambackground']); //  0 - 0060025=��� �������������=Defectogram background
  List.Items.Add(' ' + LangTable.Caption['Config:Dividingline']); //  1 - 0060026=������ ���������� �������=Dividing line
  List.Items.Add(' ' + LangTable.Caption['Config:Backmotionzone']); //  2 - 0060027=���� �������� �����=Back motion zone
  List.Items.Add(' ' + LangTable.Caption['Config:ZoominChannels1']); //  3 - 0060028=���������� ������=Zoom in Channels
  List.Items.Add(' ' + LangTable.Caption['Config:ZoomoutChannels1']); //  4 - 0060029=����������� ������=Zoom out Channels
  List.Items.Add(' ' + LangTable.Caption['Config:ZoominChannels2']); //  3 - 0060028=���������� ������=Zoom in Channels
  List.Items.Add(' ' + LangTable.Caption['Config:ZoomoutChannels2']); //  4 - 0060029=����������� ������=Zoom out Channels
  List.Items.Add(' ' + LangTable.Caption['Config:CrossedEchoSignals']); //  5 - 0060065=��������� ��� ��������=Crossed Echo Signals
  List.Items.Add(' ' + LangTable.Caption['Config:Text']); //  6 - 0060030=�������=Text
  List.Items.Add(' ' + LangTable.Caption['Config:Label']); //  7 - 0060031=�������=Label
  List.Items.Add(' ' + LangTable.Caption['Config:Defect']); //  8 - 0060063=������=Defect
  List.Items.Add(' ' + LangTable.Caption['Config:Coordinatesgrid']); //  9 - 0060032=����� ���������=Coordinates grid
  List.Items.Add(' ' + LangTable.Caption['Config:Ruler']); // 10 - 0060033=�������=Ruler
//  List.Items.Add(' ' + LangTable.Caption['Config:BSEnvelope']); // 11 - 0060034=��������� ��=Bottom signal amplitude envelope
//  List.Items.Add(' ' + LangTable.Caption['Config:LengthyLabel']); // 12 - 0060035=����������� �����=Lengthy Label
//  List.Items.Add(' ' + LangTable.Caption['Config:FilteredSignal']); // 13 - 060082=��������������e �������=Filtered Signal
 }
  List.Clear;

  Add(' ' + LangTable.Caption['Config:Defectogrambackground']); //  0 - 0060025=��� �������������=Defectogram background
  Add(' ' + LangTable.Caption['Config:Dividingline']); //  1 - 0060026=������ ���������� �������=Dividing line
  Add(' ' + LangTable.Caption['Config:Backmotionzone']); //  2 - 0060027=���� �������� �����=Back motion zone

  if Config.ProgramMode <> pmRADIOAVIONICA_MAV then
  begin
    Add(' ' + LangTable.Caption['Config:ZoominChannels1']); //  3 - 0060028=���������� ������=Zoom in Channels
    Add(' ' + LangTable.Caption['Config:ZoomoutChannels1']); //  4 - 0060029=����������� ������=Zoom out Channels
    Add(' ' + LangTable.Caption['Config:ZoominChannels2']); //  3 - 0060028=���������� ������=Zoom in Channels
    Add(' ' + LangTable.Caption['Config:ZoomoutChannels2']); //  4 - 0060029=����������� ������=Zoom out Channels
  end
  else
  begin
    Add(' ' + LangTable.Caption['Config:ZoominChannels1_MAV']); //  3 - 0060028=���������� ������=Zoom in Channels
    Add(' ' + LangTable.Caption['Config:ZoomoutChannels1_MAV']); //  4 - 0060029=����������� ������=Zoom out Channels
    Add(' ' + LangTable.Caption['Config:ZoominChannels2_MAV']); //  3 - 0060028=���������� ������=Zoom in Channels
    Add(' ' + LangTable.Caption['Config:ZoomoutChannels2_MAV']); //  4 - 0060029=����������� ������=Zoom out Channels
  end;

  Add(' ' + LangTable.Caption['Config:CrossedEchoSignals']); //  5 - 0060065=��������� ��� ��������=Crossed Echo Signals
  Add(' ' + LangTable.Caption['Config:Text']); //  6 - 0060030=�������=Text
  Add(' ' + LangTable.Caption['Config:Label']); //  7 - 0060031=�������=Label
  Add(' ' + LangTable.Caption['Config:Defect']); //  8 - 0060063=������=Defect
  Add(' ' + LangTable.Caption['Config:Coordinatesgrid']); //  9 - 0060032=����� ���������=Coordinates grid
  Add(' ' + LangTable.Caption['Config:Ruler']); // 10 - 0060033=�������=Ruler
  Add(' ' + LangTable.Caption['Config:BSEnvelope']); // 11 - 0060034=��������� ��=Bottom signal amplitude envelope
  Add(' ' + LangTable.Caption['Config:LengthyLabel']); // 12 - 0060035=����������� �����=Lengthy Label
  Add(' ' + LangTable.Caption['Config:FilteredSignal']); // 13 - 060082=��������������e �������=Filtered Signal

  SetLength(Tmp, 20 {clbViewEvent.Count} );
  for I:= 0 to clbViewEvent.Count - 1 do Tmp[I]:= clbViewEvent.Checked[I];
  clbViewEvent.Items.Clear;
//  for I:= 0060036 to 0060055 do clbViewEvent.Items.Add(Language.GetCaption(I));
  for I:= 0 to clbViewEvent.Count - 1 do clbViewEvent.Checked[I]:= Tmp[I];
  SetLength(Tmp, 0);

//  Bevel11.Visible:= Config.LangMode = 2;
//  Label1.Visible:= Config.LangMode = 2;
//  Bevel1.Visible:= Config.LangMode = 2;
//  Bevel15.Visible:= Config.LangMode = 2;
  Label1.BringToFront;
//  Bevel11.SendToBack;
//  cbWorkLang.Visible:= Config.LangMode = 2;

  if Config.ProgramMode = pmRADIOAVIONICA_MAV then pZeroProbeTapes.Caption:= Copy(pZeroProbeTapes.Caption, 1, 27);

end;

procedure TConfigForm.FormCreate(Sender: TObject);
var
  I: Integer;

begin
//  ShowMessage(IntToStr(LangTable.GroupsCount));

  if Config.ProgramMode = pmRADIOAVIONICA_MAV then
  begin
    pZeroProbeTapes.Enabled:= False;
    cbZeroProbeLineMode.Enabled:= False;
  end;

  OnChangeLanguage(nil);
  PageControl1.ActivePageIndex:= Config.ActivePageIndex;
{  case Config.LangMode of
    0: cbWorkLang.Enabled:= False;
    1: cbWorkLang.Enabled:= False;
    2: cbWorkLang.Enabled:= True;
  end; }

  {$IFDEF AVIKON16}
  TabSheet4.TabVisible:= False;
  Panel10.Left:= 4;
  {$ENDIF}
  {$IFDEF FILUSX17}
  TabSheet4.TabVisible:= False;
//  Panel10.Left:= 4;
  {$ENDIF}
end;

procedure TConfigForm.cbWorkLangChange(Sender: TObject);
begin
//  Language.WorkLang:= Config.WorkLang;
  LangTable.CurrentGroup:= Config.WorkLang;
  OnChangeLanguage(nil);
  if Assigned(FOnChangeLanguage_) then FOnChangeLanguage_(Self);
  FormShow(Sender);
end;

procedure TConfigForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Config.ActivePageIndex:= PageControl1.ActivePageIndex;
  Button1.Click;
end;

procedure TConfigForm.Button1Click(Sender: TObject);
var
  I: Integer;

begin
  for I:= 0 to High(Config.PalList) do
  if (Config.PalList[I].DC.BackMotion = Config.PalList[I].DC.CrossedCh) or
     (Config.PalList[I].PC.BackMotion = Config.PalList[I].PC.CrossedCh) then
  begin
    ShowMessage(LangTable.Caption['Messages:Colors"Backmotionzone"and"CrossedEchoSignals"mustbedifferent']);
    cbPal.ItemIndex:= I;
    Exit;
  end;
  ModalResult:= mrOk;
end;

procedure TConfigForm.TrackBar1Change(Sender: TObject);
begin
  Config.MinColPer:= TrackBar1.Position;
  Label8.Caption:= IntToStr(TrackBar1.Position) + '%';
end;

procedure TConfigForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if Msg.CharCode = 27 then Self.Close;
end;

end.



