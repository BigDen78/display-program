unit BaseCfg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst;

type
  TBaseCfgForm = class(TForm)
    CheckListBox1: TCheckListBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BaseCfgForm: TBaseCfgForm;

implementation

{$R *.dfm}

uses
  FileBaseUnit, ConfigUnit;

procedure TBaseCfgForm.FormCreate(Sender: TObject);
var
  I: Integer;

begin
  for I:= 0 to High(Config.Fields) do
    CheckListBox1.Checked[CheckListBox1.Items.Add(Config.Fields[I].Name)]:= Config.Fields[I].Mode = fmVisible;
end;

end.
