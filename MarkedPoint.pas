unit MarkedPoint;

interface

  type TMarkedPoint = class
    private
      _x   : double;
      _y   : double;
      _lbl : UnicodeString;

    public
      property x : double
        read _x write _x;
      property y : double
        read _y write _y;
      property lbl : UnicodeString
        read _lbl write _lbl;

    constructor Create(const __x : double = 0.0; const __y : double = 0.0; const __lbl : UnicodeString = '');
  end;

implementation

constructor TMarkedPoint.Create(const __x: Double; const __y: Double; const __lbl: UnicodeString);
begin
  self._x   := __x;
  self._y   := __y;
  self._lbl := __lbl;
end;

end.
