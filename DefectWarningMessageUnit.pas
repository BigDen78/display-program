unit DefectWarningMessageUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ConfigUnit;

type
  TDefectWarningMessageForm = class(TForm)
    Button1: TButton;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DefectWarningMessageForm: TDefectWarningMessageForm;

implementation

{$R *.dfm}

procedure TDefectWarningMessageForm.FormCreate(Sender: TObject);
begin
	CheckBox1.Checked := not Config.DfShowWarningMessage;
end;

procedure TDefectWarningMessageForm.Button1Click(Sender: TObject);
begin
	Config.DfShowWarningMessage := not CheckBox1.Checked;
  Close;
end;

end.

