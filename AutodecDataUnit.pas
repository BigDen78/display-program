unit AutodecDataUnit;

interface

uses MyTypes, ConfigUnit, Generics.Collections, AutodecTypes, AviconDataSource,
  DataFileConfig, AviconTypes;

type
  TIntegerArray = array of Integer;

  TAutodecData = class
  public
    classList: TClassList; // �������������
    // �������������� �������������� � ������� ����������
    classIdOrdered: TIntegerArray;
    data: array of TAutodecFileData; // ������ ��������������� ��� ������

    constructor Create;
    destructor Destroy; override;

    // ������� ��������� ��������
    procedure setProcessedCoords(coords: TMinMax);
    // �������� ������� ����� ����������� �����������
    function hasReadyResultFile(fileName: string): Boolean;
    // ������ ��������� �����
    procedure startProcess(fileName: string);
    function prcProgress(var ready: Boolean; var status: String): Integer;
    // �������� ������
    procedure loadData(fileName: string; datSrc: TAvk11DatSrc);
    function fileData(fileName: string): TAutodecFileData; overload;
    function fileData(fileName: string; var appended: Boolean):
      TAutodecFileData;
      overload;
    function readyFileData(fileName: string): TAutodecFileData;
    procedure clearData(fileName: string); overload;
    procedure clearData(); overload;
    procedure clearUnusedData(files: array of string);

  private
    procedure clearData(i: Integer); overload;
    procedure loadEntities(dataPointer: TAutodecFileData; datSrc: TAvk11DatSrc);
    function findData(filename: string): Integer;
  end;

function getRRail(rail: Integer): RRail;

implementation

uses SysUtils, AutodecUnit, cUnicodeCodecs, Math;

function getValue(value: Integer): Integer;
begin
  Result := round(value / 10);
end;

{  TAutodecData  }

constructor TAutodecData.Create;

  procedure fillClassFullList();
  var
    listF: function(): PInteger; cdecl;
    nameF: function(id: Integer): PAnsiChar; cdecl;
    shortNameF: function(id: Integer): PAnsiChar; cdecl;
    typeF: function(id: Integer): Integer; cdecl;
    groupF: function(id: Integer): Integer; cdecl;
    isDefectF: function(id: Integer): Boolean; cdecl;
    pointer: PInteger;
    classCount, i: Integer;
    classItem: TClassifierItem;

  begin
    @listF := getLibFunction('classList');
    @nameF := getLibFunction('className');
    @shortNameF := getLibFunction('classShortName');
    @typeF := getLibFunction('classTypeId');
    @groupF := getLibFunction('classGroupId');
    @isDefectF := getLibFunction('classIsDefect');
    classList := TClassList.Create;
    pointer := listF;
    classCount := pointer^;
    SetLength(classIdOrdered, classCount);
    for i := 0 to classCount - 1 do
    begin
      inc(pointer);
      classItem.id := pointer^;
      classItem.name := nameF(classItem.id);
      classItem.shortName := shortNameF(classItem.id);
      classItem.entityType := TEntityType(typeF(classItem.id));
      classItem.group := TClassGroup(groupF(classItem.id));
      classItem.isDefect := isDefectF(classItem.id);
      classList.Add(classItem.id, classItem);
      classIdOrdered[i] := classItem.id;
    end;
  end;

begin
  // ���������� ������ �������
  fillClassFullList();
end;

destructor TAutodecData.Destroy;
var
  i: Integer;
begin
  classList.Free;
  for i := Low(data) to High(data) do
  begin
    data[i].Free;
  end;
end;

function TAutodecData.hasReadyResultFile(fileName: string) : Boolean;
var
  func: function(path: PAnsiChar): Boolean; cdecl;
  dataPointer: TAutodecFileData;
begin
  @func := getLibFunction('hasReadyResultFile');
  Result := func(PAnsiChar(WideStringToUTF8String(fileName)));
end;

procedure TAutodecData.setProcessedCoords(coords: TMinMax);
var
  setProc: procedure(startCoord: Integer; finishCoord: Integer); cdecl;
begin
  @setProc := getLibFunction('setProcessedCoords');
  setProc(coords.Min, coords.Max);
end;

procedure TAutodecData.startProcess(fileName: string);
var
  // ������� ���������
  procFunc: procedure(path: PAnsiChar); cdecl;
  dataPointer: TAutodecFileData;
begin
  dataPointer := fileData(fileName);
  if dataPointer.ready or not elementsShowed() then
  begin
    Exit;
  end;
  @procFunc := getLibFunction('startProcess');
  procFunc(PAnsiChar(WideStringToUTF8String(fileName)));
end;

function TAutodecData.prcProgress(var ready: Boolean; var status: String): Integer;
var
  progressFunc: function(): Integer; cdecl;
  readyFunc: function(): Boolean; cdecl;
  statusFunc: function(): PAnsiChar; cdecl;
begin
  @progressFunc := getLibFunction('prcProgressValue');
  Result := progressFunc();
  @readyFunc := getLibFunction('prcReady');
  ready := readyFunc();
  @statusFunc := getLibFunction('prcStatus');
  status := statusFunc();
end;

procedure TAutodecData.loadData(fileName: string; datSrc: TAvk11DatSrc);
var
  result: Boolean;
  // ���������� ������� ������
  prepareFunc: function(): Boolean; cdecl;
  dataPointer: TAutodecFileData;
begin
  dataPointer := fileData(fileName);
  dataPointer.dataLoading := True;
  loadEntities(dataPointer, datSrc);
  dataPointer.dataLoading := False;
  dataPointer.ready := True;
end;

procedure TAutodecData.loadEntities(dataPointer: TAutodecFileData; datSrc:
  TAvk11DatSrc);
var
  entRail: RRail;
  entClassId: Integer;
  entity: TEntity;
  // ������� ��������� �� ���������
  iterFirstF: function(): Boolean; cdecl;
  iterDoneF: function(): Boolean; cdecl;
  iterNextF: function(): Boolean; cdecl;
  // ������� ��������� ��������� ��������
  idF: function(): Integer; cdecl;
  classIdF: function(): Integer; cdecl;
  railF: function(): Integer; cdecl;
  channelF: function(): Integer; cdecl;
  defLevelF: function(): Integer; cdecl;
  startF: function(): Int64; cdecl;
  finishF: function(): Int64; cdecl;
  expStartF: function(): Int64; cdecl;
  expFinishF: function(): Int64; cdecl;
  dublicateF: function(): Boolean; cdecl;
  backwardF: function(): Boolean; cdecl;
  hiddenF: function(): Boolean; cdecl;
  // ������� ��������� �� ��������
  arIterFirstF: function(): Boolean; cdecl;
  arIterDoneF: function(): Boolean; cdecl;
  arIterNextF: function(): Boolean; cdecl;
  // ������� ��������� ��������� �������
  arChannelF: function(): Integer; cdecl;
  arStartF: function(): Int64; cdecl;
  arFinishF: function(): Int64; cdecl;
  arExpStartF: function(): Int64; cdecl;
  arExpFinishF: function(): Int64; cdecl;
  arMinDelayF: function(): Integer; cdecl;
  arMaxDelayF: function(): Integer; cdecl;
  arStartDelayF: function(): Integer; cdecl;
  arFinishDelayF: function(): Integer; cdecl;

  procedure loadAreas(ent: PEntity);
  var
    area: TEntityArea;
    length: Integer;
  begin
    if not arIterFirstF() then
      Exit;
    length := 0;
    while not arIterDoneF() do
    begin
      with area do
      begin
        channelId := arChannelF();
        startSysCoordinate := arStartF();
        finishSysCoordinate := arFinishF();
        startDisCoordinate := arExpStartF();
        finishDisCoordinate := arExpFinishF();
        minMaxDelays.Min := arMinDelayF();
        minMaxDelays.Max := arMaxDelayF();
        startFinishDelays.Min := arStartDelayF();
        startFinishDelays.Max := arFinishDelayF();
      end;
      length := length + 1;
      SetLength(ent.areas, length);
      ent.areas[length - 1] := area;
      arIterNextF();
    end;
  end;

  procedure calcViewChannel(ent: PEntity);
  var
    i: Integer;
    channel: TScanChannelItem;
    viewChannel: TViewChannel;
    viewChannelResult: TViewChannel;
  begin
    for i := Low(ent.areas) to High(ent.areas) do
    begin
      channel := datSrc.Config.ScanChannelByNum[ent.areas[i].channelId];
      if ((Abs(channel.TurnAngle) <> 90) and (channel.EnterAngle < 0)) or
        ((Abs(channel.TurnAngle) = 90) and (channel.Gran = _UnWork)) or
        (Abs(channel.EnterAngle) = 0) then
        viewChannel := vcOtezd
      else
        viewChannel := vcNaezd;

      if i = Low(ent.areas) then
      begin
        viewChannelResult := viewChannel;
      end;
      if viewChannelResult <> viewChannel then
      begin
        ent.viewChannel := vcAll;
        Exit;
      end;

    end;
    ent.viewChannel := viewChannelResult;
  end;

begin
  dataPointer.entities.Clear;
  @iterFirstF := getLibFunction('entityIteratorFirst');
  @iterNextF := getLibFunction('entityIteratorNext');
  @iterDoneF := getLibFunction('entityIteratorDone');
  @idF := getLibFunction('entityId');
  @classIdF := getLibFunction('entityClassId');
  @railF := getLibFunction('entityRail');
  @channelF := getLibFunction('entityChannel');
  @defLevelF := getLibFunction('entityDefLevel');
  @startF := getLibFunction('entityStartCoord');
  @finishF := getLibFunction('entityFinishCoord');
  @expStartF := getLibFunction('entityStartExpandedCoord');
  @expFinishF := getLibFunction('entityFinishExpandedCoord');
  @dublicateF := getLibFunction('entityDublicate');
  @backwardF := getLibFunction('entityInBackwardInterval');
  @hiddenF := getLibFunction('entityInHiddenInterval');
  @arIterFirstF := getLibFunction('areaIteratorFirst');
  @arIterNextF := getLibFunction('areaIteratorNext');
  @arIterDoneF := getLibFunction('areaIteratorDone');
  @arChannelF := getLibFunction('areaChannel');
  @arStartF := getLibFunction('areaStartCoord');
  @arFinishF := getLibFunction('areaFinishCoord');
  @arExpStartF := getLibFunction('areaStartExpandedCoord');
  @arExpFinishF := getLibFunction('areaFinishExpandedCoord');
  @arMinDelayF := getLibFunction('areaMinDelay');
  @arMaxDelayF := getLibFunction('areaMaxDelay');
  @arStartDelayF := getLibFunction('areaStartDelay');
  @arFinishDelayF := getLibFunction('areaFinishDelay');

  if not iterFirstF() then
    Exit;

  while not iterDoneF() do
  begin
    entClassId := classIdF();
    entRail := getRRail(railF());
    dataPointer.Init(entClassId, entRail);

    with entity do
    begin
      id := idF();
      classId := entClassId;
      rail := entRail;
      channelId := channelF();
      defLevel := TDefLevel(defLevelF());
      startSysCoordinate := startF();
      finishSysCoordinate := finishF();
      startDisCoordinate := expStartF();
      finishDisCoordinate := expFinishF();
      isDublicate := dublicateF();
      inHiddenZone := hiddenF();
      inBackwardZone := backwardF();
    end;
    loadAreas(@entity);
    calcViewChannel(@entity);
    dataPointer.entities[entClassId].items[entRail].Add(entity);
    iterNextF();
  end;
end;

function TAutodecData.findData(fileName: string): Integer;
var
  i: Integer;
begin
  for i := Low(data) to High(data) do
  begin
    if (data[i] <> nil) and (data[i].fileName = fileName) and (data[i].fileName
      <> '') then
    begin
      Result := i;
      Exit;
    end;
  end;
  Result := -1;
end;

function TAutodecData.fileData(fileName: string): TAutodecFileData;
var
  i: Integer;
begin
  i := findData(fileName);
  if (i = -1) then
  begin
    Result := nil;
  end
  else
  begin
    Result := data[i];
  end;
end;

function TAutodecData.fileData(fileName: string; var appended: Boolean):
  TAutodecFileData;
var
  i: Integer;
begin
  i := findData(fileName);
  if (i = -1) then
  begin
    i := Length(data);
    SetLength(data, i + 1);
    data[High(data)] := TAutodecFileData.Create;
    result := data[High(data)];
    result.fileName := fileName;
    result.ready := False;
    appended := True;
  end
  else
  begin
    result := data[i];
    appended := False;
  end;
end;

function TAutodecData.readyFileData(fileName: string): TAutodecFileData;
var
  i: Integer;
begin
  i := findData(fileName);
  if (i = -1) or (data[i].dataLoading) or (not data[i].ready) then
    Result := nil
  else
    Result := data[i];
end;

procedure TAutodecData.clearData(i: Integer);
begin
  data[i].ready := False;
  if Assigned(data[i].entities) then
  begin
    data[i].entities.Free;
    data[i].entities := nil;
  end;

  data[i].fileName := '';
end;

procedure TAutodecData.clearData(fileName: string);
var
  i: Integer;
begin
  i := findData(filename);
  if (i <> -1) then
    clearData(i);
end;

procedure TAutodecData.clearUnusedData(files: array of string);
var
  i, j: integer;
  found: boolean;
begin
  for i := low(data) to high(data) do
  begin
    found := false;
    for j := low(files) to high(files) do
      if data[i].fileName = files[j] then
      begin
        found := true;
        break;
      end;

    if not found then
      clearData(i);
  end;
end;

procedure TAutodecData.clearData();
var
  i: Integer;
begin
  for i := Low(data) to High(Data) do
  begin
    if not data[i].dataLoading then
    begin
      clearData(i);
    end;
  end;
end;

function getRRail(rail: Integer): RRail;
begin
  if rail = -1 then
  begin
    Result := RRail.r_Both;
  end
  else
  begin
    Result := RRail(rail - 1);
  end;
end;

end.

