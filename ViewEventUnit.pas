unit ViewEventUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, MyTypes, Avk11ViewUnit, TeEngine, Series,
  ExtCtrls, TeeProcs, Chart, StdCtrls, AviconTypes, Buttons, CheckLst;

type
  TViewEventForm = class(TForm)
    StatusBar1: TStatusBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Chart1: TChart;
    Series1: TLineSeries;
    Series2: TLineSeries;
    Panel1: TPanel;
    Button1: TButton;
    ListView1: TListView;
    Memo1: TMemo;
    TabSheet4: TTabSheet;
    CheckListBox1: TCheckListBox;
    Panel2: TPanel;
    Button2: TButton;
    Button3: TButton;
    TabSheet5: TTabSheet;
    Chart2: TChart;
    Series3: TLineSeries;
    Series4: TPointSeries;
    procedure ListView1DblClick(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure Button1Click(Sender: TObject);
    function BlockOk(StartDisCoord: Integer): Boolean;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure CheckListBox1ClickCheck(Sender: TObject);
  private
    FVF: TAvk11ViewForm;
    StIdx: Integer;
    EndIdx: Integer;
  public
    procedure CreateList(VF: TAvk11ViewForm);
  end;

implementation

{$R *.dfm}

procedure TViewEventForm.CheckListBox1ClickCheck(Sender: TObject);
begin
  CreateList(FVF);
end;

procedure TViewEventForm.CreateList(VF: TAvk11ViewForm);
var
  I: Integer;
  New: TListItem;
  Speed: Single;
  Speed2: Integer;
  Val: string;
  Name: string;
  IDtmp: Byte;
  pData: PEventData;

begin

  FVF:= VF;
  ListView1.Items.Clear;
  ListView1.Items.BeginUpdate;

  Series3.Clear;

  for I:= 0 to FVF.DatSrc.EventCount - 1 do
//    if FVF.DatSrc.Event[I].Event.ID > 127 then
    begin


      if CheckListBox1.Items.IndexOfObject(Pointer(FVF.DatSrc.Event[I].ID)) <> - 1 then
        if not CheckListBox1.Checked[CheckListBox1.Items.IndexOfObject(Pointer(FVF.DatSrc.Event[I].ID))] then Continue;


      New:= ListView1.Items.Add;
      New.Caption:= IntToStr(I);
      New.SubItems.Add(IntToStr(FVF.DatSrc.Event[I].ID) + ' (' + Format('%x', [FVF.DatSrc.Event[I].ID]) + ')');
//      New.SubItems.Add(Id_To_EventName(FVF.DatSrc.Event[I].Event.ID, False, True));

      with FVF.DatSrc.Event[I] do
      begin
        Val:= Format('R: %d; Ch: %d; Val: ', [Data[1], Data[2]]);
        case ID of
           EID_Ku: Val:= Val + Format('%d', [ShortInt(Data[3])]);
          EID_Att: begin
                     Val:= Val + Format('%d', [ShortInt(Data[3])]);
               {      if ((FVF.DatSrc.Header.ID = $FF12) or (FVF.DatSrc.Header.ID = $FF14)) and
                        (FVF.DatSrc.EventsData[I].Event.Discoord > 0) then
                     begin
                       if Params_.Sens   [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])] + KuRek[ByteToCh(ID, Data[1])] > 60
                         then
                         begin
                           Params_.Sens   [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= 60 - ShortInt(Data[2])
                           Val:= Val + Format('; Sens Val: %d', [60 - ShortInt(Data[2])]);
                         end
                         else
                         begin
                           Params_.Sens   [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= KuRek[ByteToCh(ID, Data[1])];
                           Val:= Val + Format('; Sens Val: %d', [KuRek[ByteToCh(ID, Data[1])]]);
                         end
                     end; }
                   end;
          EID_TVG: Val:= Val + Format('%d', [Data[3]]);
        EID_StStr: Val:= Val + Format('%d', [Data[3]]);
       EID_EndStr: Val:= Val + Format('%d', [Data[3]]);
   EID_PrismDelay: Val:= Val + Format('%d', [Data[3]]);
     EID_GPSState: Val:= Format('State %d; UseSatCount: %d; AntennaConnected: %d', [Data[1], Data[2], Data[3]]);
         EID_Mode: begin
                     Val:= Format('%d', [Data[1]]);
                     case Data[1] or (Data[2] shl 8) of
                       0: Val:= '��������� ��������� ������';
                       1: Val:= '��������� ������� ������';
                       2: Val:= '������';
                       3: Val:= '������';
                       4: Val:= '����� �';
                       5: Val:= '����� �';
                       6: Val:= '����';
                       7: Val:= '�����';
                       8: Val:= '��������� 2�� ��������� ������';
                       9: Val:= '��������� 2�� ������� ������';
                      10: Val:= '������������ ������ (���)';
                      11: Val:= '������������ ����� (���)';
                      12: Val:= '������ � ���������� �������� ������� ������';
                     end;

          //           if Data[3] <> 0 then
                     begin
                        Val:= Val + Format(' Side: %d; Ch: %d; Time: %d', [Data[4], Data[5], FVF.DatSrc.Params[I].Time {Data[6] + Data[7] * $100}]);
                     end
            //         else Val:= Val + ' ��� ���. ������';

                   end;
{       EID_HeadPh: begin
                     Params_.HeadPh[SideToRail(sLeft), 0]:= (Data[1] and  32) <> 0; // ����� 0 - 1 byte / 5 bit
                     Params_.HeadPh[SideToRail(sLeft), 1]:= (Data[1] and  16) <> 0; // ����� 1 - 1 byte / 4 bit
                     Params_.HeadPh[SideToRail(sLeft), 2]:= (Data[1] and   8) <> 0; // ����� 2 - 1 byte / 3 bit
                     Params_.HeadPh[SideToRail(sLeft), 3]:= (Data[1] and   4) <> 0; // ����� 3 - 1 byte / 2 bit
                     Params_.HeadPh[SideToRail(sLeft), 4]:= (Data[1] and   2) <> 0; // ����� 4 - 1 byte / 1 bit
                     Params_.HeadPh[SideToRail(sLeft), 5]:= (Data[1] and   1) <> 0; // ����� 5 - 1 byte / 0 bit
                     Params_.HeadPh[SideToRail(sLeft), 6]:= (Data[2] and 128) <> 0; // ����� 6 - 2 byte / 7 bit
                     Params_.HeadPh[SideToRail(sLeft), 7]:= (Data[2] and  64) <> 0; // ����� 7 - 2 byte / 6 bit
                     Params_.HeadPh[SideToRail(sLeft), 8]:= (Data[2] and  32) <> 0; // ����� 8 - 2 byte / 5 bit
                     Params_.HeadPh[SideToRail(sLeft), 9]:= (Data[2] and  16) <> 0; // ����� 9 - 2 byte / 4 bit

                     Params_.HeadPh[SideToRail(sRight), 0]:= (Data[3] and  32) <> 0; // ����� 0 - 1 byte / 5 bit
                     Params_.HeadPh[SideToRail(sRight), 1]:= (Data[3] and  16) <> 0; // ����� 1 - 1 byte / 4 bit
                     Params_.HeadPh[SideToRail(sRight), 2]:= (Data[3] and   8) <> 0; // ����� 2 - 1 byte / 3 bit
                     Params_.HeadPh[SideToRail(sRight), 3]:= (Data[3] and   4) <> 0; // ����� 3 - 1 byte / 2 bit
                     Params_.HeadPh[SideToRail(sRight), 4]:= (Data[3] and   2) <> 0; // ����� 4 - 1 byte / 1 bit
                     Params_.HeadPh[SideToRail(sRight), 5]:= (Data[3] and   1) <> 0; // ����� 5 - 1 byte / 0 bit
                     Params_.HeadPh[SideToRail(sRight), 6]:= (Data[4] and 128) <> 0; // ����� 6 - 2 byte / 7 bit
                     Params_.HeadPh[SideToRail(sRight), 7]:= (Data[4] and  64) <> 0; // ����� 7 - 2 byte / 6 bit
                     Params_.HeadPh[SideToRail(sRight), 8]:= (Data[4] and  32) <> 0; // ����� 8 - 2 byte / 5 bit
                     Params_.HeadPh[SideToRail(sRight), 9]:= (Data[4] and  16) <> 0; // ����� 9 - 2 byte / 4 bit
                   end; }
          else Val:= '';
        end;
      end;
//      if FVF.DatSrc.Event[I].ID <> EID_Mode then Val:= '';

      Name:= '';
      case FVF.DatSrc.Event[I].ID of
        0: Name:= '������� �-���������';
        EID_HandScan              : Name:= 'EID_HandScan, 0x82 - ������';
        EID_Ku                    : Name:= 'EID_Ku, 0x90 - ��������� �������� ����������������';
        EID_Att                   : Name:= 'EID_Att, 0x91 - ��������� �����������';
        EID_TVG                   : Name:= 'EID_TVG, 0x92 - ��������� ���';
        EID_StStr                 : Name:= 'EID_StStr, 0x93 - ��������� ��������� ������ ������';
        EID_EndStr                : Name:= 'EID_EndStr, 0x94 - ��������� ��������� ����� ������';
        EID_HeadPh                : Name:= 'EID_HeadPh, 0x95 - ������ ���������� ���������';
        EID_Mode                  : Name:= 'EID_Mode, 0x96 - ��������� ������';
        EID_SetRailType           : Name:= 'EID_SetRailType, 0x9B - ��������� �� ��� ������';
        EID_PrismDelay            : Name:= 'EID_PrismDelay, 0x9C - ��������� 2�� (word)';
        EID_Stolb                 : Name:= 'EID_Stolb, 0xA0 - ������� ����������';
        EID_Switch                : Name:= 'EID_Switch, 0xA1 - ����� ����������� ��������';
        EID_DefLabel              : Name:= 'EID_DefLabel, 0xA2 - ������� �������';
        EID_TextLabel             : Name:= 'EID_TextLabel, 0xA3 - ��������� �������';
        EID_StBoltStyk            : Name:= 'EID_StBoltStyk, 0xA4 - ������� ������ ��';
        EID_EndBoltStyk           : Name:= 'EID_EndBoltStyk, 0xA5 - ���������� ������ ��';
        EID_Time                  : Name:= 'EID_Time, 0xA6 - ������� �������';
        EID_StolbChainage         : Name:= 'EID_StolbChainage, 0xA7 - ������� ���������� Chainage';
        EID_ZerroProbMode         : Name:= 'EID_ZerroProbMode, 0xA8 - ����� ������ �������� 0 ����';
        EID_LongLabel             : Name:= 'EID_LongLabel, 0xA9 - ����������� �������';
        EID_SpeedState            : begin
                                      Name:= 'EID_SpeedState, 0xAA - �������� � ���������� �������� ��������';
                                      FVF.DatSrc.GetEventData(I, IDtmp, pData);
                                      Series3.AddXY(FVF.DatSrc.Event[I].DisCoord, pedSpeedState(pData)^.Speed);
                                      if pedSpeedState(pData)^.State = 1 then Series4.AddXY(FVF.DatSrc.Event[I].DisCoord, pedSpeedState(pData)^.Speed, '', clRed)
                                                                         else Series4.AddXY(FVF.DatSrc.Event[I].DisCoord, pedSpeedState(pData)^.Speed, '', clGreen);

                                    end;
        EID_ChangeOperatorName    : Name:= 'EID_ChangeOperatorName, 0xAB - ����� ��������� (��� ���������)';
        EID_AutomaticSearchRes    : Name:= 'EID_AutomaticSearchRes, 0xAC - ��������� ������� �������, ���������� ��� �������������� ������';
        EID_TestRecordFile        : Name:= 'EID_TestRecordFile, 0xAD - ���� ������ ������������ ������';
        EID_OperatorRemindLabel   : Name:= 'EID_OperatorRemindLabel, 0xAE - ������� ����, ������� ���������� � ������ ��� ����������� ��������� (��������� ������ � ������)';
        EID_QualityCalibrationRec : Name:= 'EID_QualityCalibrationRec, 0xAF - �������� ��������� ������� ��������';
        EID_Sensor1               : Name:= 'EID_Sensor1, 0xB0 - ������ ������� �� � �������';
        EID_AirBrush              : Name:= 'EID_AirBrush, 0xB1 - ��������������';
        EID_PaintMarkRes          : Name:= 'EID_PaintMarkRes, 0xB2 - ��������� � ���������� ������� �� ������-�������';
        EID_AirBrushTempOff       : Name:= 'EID_AirBrushTempOff, 0xB3 - ��������� ���������� ���������������';
        EID_AirBrush2             : Name:= 'EID_AirBrush2, 0xB4 - �������������� � ���������� �����������';
        EID_AlarmTempOff          : Name:= 'EID_AlarmTempOff, 0xB5 - ��������� ���������� ��� �� ���� �������';
        EID_StartSwitchShunter    : Name:= 'EID_StartSwitchShunter, 0xB6 - ������ ���� ����������� ��������';
        EID_EndSwitchShunter      : Name:= 'EID_EndSwitchShunter, 0xB7 - ����� ���� ����������� ��������';
        EID_Temperature           : Name:= 'EID_Temperature, 0xB8 - �����������';
        EID_DebugData             : Name:= 'EID_DebugData, 0xB9 - ���������� ����������';
        EID_PaintSystemParams     : Name:= 'EID_PaintSystemParams, 0xBA - ��������� ������ ��������� ������������';
        EID_UMUPaintJob           : Name:= 'EID_UMUPaintJob, 0xBB - ������� ��� �� �������������';
        EID_ACData                : Name:= 'EID_ACData, 0xBC - ������ ������������� ��������';
        EID_SmallDate             : Name:= 'EID_SmallDate, 0xBD - ����� ������������� �������';
        EID_RailHeadScaner        : Name:= 'EID_RailHeadScaner, 0xBE - ������ ����������� ������� ������� ������';
        EID_SensAllowedRanges     : Name:= 'EID_SensAllowedRanges, 0xBF - ������� ����������� ���������� ��';
        EID_GPSCoord              : Name:= 'EID_GPSCoord, 0xC0 - �������������� ����������';
        EID_GPSState              : Name:= 'EID_GPSState, 0xC1 - ��������� ��������� GPS';
        EID_Media                 : Name:= 'EID_Media, 0xC2 - �����������';
        EID_GPSCoord2             : Name:= 'EID_GPSCoord2, 0xC3 - �������������� ���������� �� ���������';
        EID_NORDCO_Rec            : Name:= 'EID_NORDCO_Rec, 0xC4 - ������ NORDCO';
        EID_SensFail              : Name:= 'EID_SensFail, 0xC5 - ���������� �������� ���������������� �� ������������ ��������';
        EID_AScanFrame            : Name:= 'EID_AScanFrame, 0xC6 - ���� �-���������';
        EID_MediumDate            : Name:= 'EID_MediumDate, 0xC7 - ������� ������������� �������';
        EID_BigDate               : Name:= 'EID_BigDate, 0xC8 - ������� ������������� �������';
        EID_CheckSum              : Name:= 'EID_CheckSum, 0xF0 - ����������� �����';
        EID_SysCrd_SS             : Name:= 'EID_SysCrd_SS, 0xF1 - ��������� ���������� ��������';
        EID_SysCrd_SF             : Name:= 'EID_SysCrd_SF, 0xF4 - ������ ��������� ���������� � �������� �������';
        EID_SysCrd_FS             : Name:= 'EID_SysCrd_FS, 0xF9 - ��������� ���������� ��������';
        EID_SysCrd_FF             : Name:= 'EID_SysCrd_FF, 0xFC - ������ ��������� ���������� � ������ �������';
        EID_SysCrd_NS             : Name:= 'EID_SysCrd_NS, 0xF2 - ��������� ���������� �����, ��������';
        EID_SysCrd_NF             : Name:= 'EID_SysCrd_NF, 0xF3 - ��������� ���������� �����, ������';
        EID_SysCrd_UMUA_Left_NF   : Name:= 'EID_SysCrd_UMUA_Left_NF, 0xF5 - ������ ��������� ���������� ��� ������ ��� A, ����� �������';
        EID_SysCrd_UMUB_Left_NF   : Name:= 'EID_SysCrd_UMUB_Left_NF, 0xF6 - ������ ��������� ���������� ��� ������ ��� �, ����� �������';
        EID_SysCrd_UMUA_Right_NF  : Name:= 'EID_SysCrd_UMUA_Right_NF, 0xF7 - ������ ��������� ���������� ��� ������ ��� A, ������ �������';
        EID_SysCrd_UMUB_Right_NF  : Name:= 'EID_SysCrd_UMUB_Right_NF, 0xF8 - ������ ��������� ���������� ��� ������ ��� �, ������ �������';
        EID_EndFile               : Name:= 'EID_EndFile, 0xFF - ����� �����';
      end;

//      New.SubItems.Add(Val);
      New.SubItems.Add(Format('0x%x [%d]', [FVF.DatSrc.Event[I].OffSet, FVF.DatSrc.Event[I].OffSet]));
      New.SubItems.Add(Name);
      New.SubItems.Add(Val);
      New.SubItems.Add(IntToStr(FVF.DatSrc.Event[I].SysCoord));
      New.SubItems.Add(IntToStr(FVF.DatSrc.Event[I].DisCoord));

//      New.SubItems.Add(Format('%d / %d', [FVF.DatSrc.Event[I].Event.Pack[rLeft].Delay, FVF.DatSrc.Event[I].Event.Pack[rRight].Delay]));
//      New.SubItems.Add(Format('%d / %d', [FVF.DatSrc.Event[I].Event.Pack[rLeft].Ampl, FVF.DatSrc.Event[I].Event.Pack[rRight].Ampl]));

      if I <> 0 then
      begin
        New.SubItems.Add(IntToStr(Abs(FVF.DatSrc.Event[I - 0].SysCoord - FVF.DatSrc.Event[I - 1].SysCoord)));
        New.SubItems.Add(IntToStr(Abs(FVF.DatSrc.Event[I - 0].DisCoord - FVF.DatSrc.Event[I - 1].DisCoord)));
      end;

      New.Data:= Pointer(I);
    end;
  ListView1.Items.EndUpdate;
  ListView1.Repaint;


  for I:= 0 to FVF.DatSrc.MaxDisCoord div 100 do
  begin
    FVF.DatSrc.GetSpeed(I * 100, Speed);
    Series1.AddXY(I * 100, Speed);
    FVF.DatSrc.GetSpeed(I * 100, Speed);
    Series2.AddXY(I * 100, Speed);
  end;

end;

procedure TViewEventForm.ListView1DblClick(Sender: TObject);
begin
  if Assigned(ListView1.Selected) then
  begin
//    FVF.Display.CenterDisCoord:= FVF.DatSrc.Event[Integer(ListView1.Selected.Data)].Event.DisCoord;
//    FVF.Display.Refresh;
  end;
end;

procedure TViewEventForm.FormCreate(Sender: TObject);
begin

  CheckListBox1.Items.AddObject('EID_HandScan - ������', Pointer($82));
  CheckListBox1.Items.AddObject('EID_Ku - ��������� �������� ����������������', Pointer($90));
  CheckListBox1.Items.AddObject('EID_Att - ��������� �����������', Pointer($91));
  CheckListBox1.Items.AddObject('EID_TVG - ��������� ���', Pointer($92));
  CheckListBox1.Items.AddObject('EID_StStr - ��������� ��������� ������ ������', Pointer($93));
  CheckListBox1.Items.AddObject('EID_EndStr - ��������� ��������� ����� ������', Pointer($94));
  CheckListBox1.Items.AddObject('EID_HeadPh - ������ ���������� ���������', Pointer($95));
  CheckListBox1.Items.AddObject('EID_Mode - ��������� ������', Pointer($96));
  CheckListBox1.Items.AddObject('EID_SetRailType - ��������� �� ��� ������', Pointer($9B));
  CheckListBox1.Items.AddObject('EID_PrismDelay - ��������� 2�� (word)', Pointer($9C));
  CheckListBox1.Items.AddObject('EID_Stolb - ������� ����������', Pointer($A0));
  CheckListBox1.Items.AddObject('EID_Switch - ����� ����������� ��������', Pointer($A1));
  CheckListBox1.Items.AddObject('EID_DefLabel - ������� �������', Pointer($A2));
  CheckListBox1.Items.AddObject('EID_TextLabel - ��������� �������', Pointer($A3));
  CheckListBox1.Items.AddObject('EID_StBoltStyk - ������� ������ ��', Pointer($A4));
  CheckListBox1.Items.AddObject('EID_EndBoltStyk - ���������� ������ ��', Pointer($A5));
  CheckListBox1.Items.AddObject('EID_Time - ������� �������', Pointer($A6));
  CheckListBox1.Items.AddObject('EID_StolbChainage - ������� ���������� Chainage', Pointer($A7));
  CheckListBox1.Items.AddObject('EID_ZerroProbMode - ����� ������ �������� 0 ����', Pointer($A8));
  CheckListBox1.Items.AddObject('EID_LongLabel - ����������� �������', Pointer($A9));

////////////////////////////////////////
  CheckListBox1.Items.AddObject('EID_SpeedState - �������� � ���������� �������� ��������', Pointer($AA));
  CheckListBox1.Items.AddObject('EID_ChangeOperatorName - ����� ��������� (��� ���������)', Pointer($AB));
  CheckListBox1.Items.AddObject('EID_AutomaticSearchRes - ��������� ������� �������, ���������� ��� �������������� ������', Pointer($AC));
  CheckListBox1.Items.AddObject('EID_TestRecordFile - ���� ������ ������������ ������', Pointer($AD));
  CheckListBox1.Items.AddObject('EID_OperatorRemindLabel - ������� ����, ������� ���������� � ������ ��� ����������� ��������� (��������� ������ � ������)', Pointer($AE));
  CheckListBox1.Items.AddObject('EID_QualityCalibrationRec- �������� ��������� ������� ��������', Pointer($AF));
////////////////////////////////////////

  CheckListBox1.Items.AddObject('EID_Sensor1 - ������ ������� �� � �������', Pointer($B0));
  CheckListBox1.Items.AddObject('EID_AirBrush - ��������������', Pointer($B1));
  CheckListBox1.Items.AddObject('EID_PaintMarkRes - ��������� � ���������� ������� �� ������-�������', Pointer($B2));
  CheckListBox1.Items.AddObject('EID_AirBrushTempOff - ��������� ���������� ���������������', Pointer($B3));
  CheckListBox1.Items.AddObject('EID_AirBrush2 - �������������� � ���������� �����������', Pointer($B4));
  CheckListBox1.Items.AddObject('EID_AlarmTempOff - ��������� ���������� ��� �� ���� �������', Pointer($B5));

////////////////////////////////////////
  CheckListBox1.Items.AddObject('EID_StartSwitchShunter - ������ ���� ����������� ��������', Pointer($B6));
  CheckListBox1.Items.AddObject('EID_EndSwitchShunter - ����� ���� ����������� ��������', Pointer($B7));
  CheckListBox1.Items.AddObject('EID_Temperature - �����������', Pointer($B8));
  CheckListBox1.Items.AddObject('EID_DebugData - ���������� ����������', Pointer($B9));
  CheckListBox1.Items.AddObject('EID_PaintSystemParams - ��������� ������ ��������� ������������', Pointer($BA));
  CheckListBox1.Items.AddObject('EID_UMUPaintJob - ������� ��� �� �������������', Pointer($BB));
  CheckListBox1.Items.AddObject('EID_ACData - ������ ������������� ��������', Pointer($BC));
////////////////////////////////////////

  CheckListBox1.Items.AddObject('EID_SmallDate - ����� ������������� �������', Pointer($BD));
  CheckListBox1.Items.AddObject('EID_RailHeadScaner - ������ ����������� ������� ������� ������', Pointer($BE));
  CheckListBox1.Items.AddObject('EID_SensAllowedRanges - ������� ����������� ���������� ��', Pointer($BF));

  CheckListBox1.Items.AddObject('EID_GPSCoord - �������������� ����������', Pointer($C0));
  CheckListBox1.Items.AddObject('EID_GPSState - ��������� ��������� GPS', Pointer($C1));
  CheckListBox1.Items.AddObject('EID_Media- �����������', Pointer($C2));
  CheckListBox1.Items.AddObject('EID_GPSCoord2 - �������������� ���������� �� ���������', Pointer($C3));
  CheckListBox1.Items.AddObject('EID_NORDCO_Rec - ������ NORDCO', Pointer($C4));
  CheckListBox1.Items.AddObject('EID_SensFail - ���������� �������� ���������������� �� ������������ ��������', Pointer($C5));

  CheckListBox1.Items.AddObject('EID_AScanFrame - ���� �-���������', Pointer($C6));
  CheckListBox1.Items.AddObject('EID_MediumDate - ������� ������������� �������', Pointer($C7));
  CheckListBox1.Items.AddObject('EID_BigDate - ������� ������������� �������', Pointer($C8));

  CheckListBox1.Items.AddObject('EID_CheckSum - ����������� �����', Pointer($F0));
  CheckListBox1.Items.AddObject('EID_SysCrd_SS - ��������� ���������� ��������', Pointer($F1));
  CheckListBox1.Items.AddObject('EID_SysCrd_SF - ������ ��������� ���������� � �������� �������', Pointer($F4));
  CheckListBox1.Items.AddObject('EID_SysCrd_FS - ��������� ���������� ��������', Pointer($F9));
  CheckListBox1.Items.AddObject('EID_SysCrd_FF - ������ ��������� ���������� � ������ �������', Pointer($FC));
  CheckListBox1.Items.AddObject('EID_SysCrd_NS - ��������� ���������� �����, ��������', Pointer($F2));
  CheckListBox1.Items.AddObject('EID_SysCrd_NF - ��������� ���������� �����, ������', Pointer($F3));
  CheckListBox1.Items.AddObject('EID_SysCrd_UMUA_Left_NF - ������ ��������� ���������� ��� ������ ��� A, ����� �������', Pointer($F5));
  CheckListBox1.Items.AddObject('EID_SysCrd_UMUB_Left_NF - ������ ��������� ���������� ��� ������ ��� �, ����� �������', Pointer($F6));
  CheckListBox1.Items.AddObject('EID_SysCrd_UMUA_Right_NF - ������ ��������� ���������� ��� ������ ��� A, ������ �������', Pointer($F7));
  CheckListBox1.Items.AddObject('EID_SysCrd_UMUB_Right_NF - ������ ��������� ���������� ��� ������ ��� �, ������ �������', Pointer($F8));

  CheckListBox1.Items.AddObject('EID_EndFile - ����� �����', Pointer($FF));

  CheckListBox1.CheckAll(cbChecked);
end;

procedure TViewEventForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if Msg.CharCode = 27 then Self.ModalResult:= mrOK;
end;

function TViewEventForm.BlockOk(StartDisCoord: Integer): Boolean;
var
  I: Integer;
  S: string;

begin
//  for I:= 0 to FVF.DatSrc.EventCount - 1 do
(*  for I:= StIdx to EndIdx do
    if FVF.DatSrc.CurDisCoord = FVF.DatSrc.Event[I].Event.DisCoord then
    begin
      S:= Format('%d - %d / %d', [I, FVF.DatSrc.Pack[rLeft].Ampl, FVF.DatSrc.Pack[rRight].Ampl]);
      if ListView1.Items[I].SubItems.Count <> 11 then ListView1.Items[I].SubItems.Add(S)
                                                 else ListView1.Items[I].SubItems.Strings[10]:= S;
{      if I >= EndIdx then
      begin
        Result:= False;
        Exit;
      end; }
      Break;
    end;
*)
//  Result:= FVF.DatSrc.Event[EndIdx + 1].Event.DisCoord > FVF.DatSrc.CurDisCoord;
end;

procedure TViewEventForm.Button1Click(Sender: TObject);
var
  I: Integer;

begin
{  if Assigned(ListView1.Selected) then
  begin
    I:= Integer(ListView1.Selected.Data);
    StIdx:= I;
    EndIdx:= I + 100;
    FVF.DatSrc.LoadData(FVF.DatSrc.Event[I].Event.DisCoord, MaxInt, 0, BlockOk);
  end;  }
end;

procedure TViewEventForm.Button2Click(Sender: TObject);
begin
  CheckListBox1.CheckAll(cbChecked);
  CreateList(FVF);
end;

procedure TViewEventForm.Button3Click(Sender: TObject);
begin
  CheckListBox1.CheckAll(cbUnchecked);
  CreateList(FVF);
end;

end.
