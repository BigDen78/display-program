unit RecTypes;

interface

uses
  Types, Windows, Classes, SysUtils, Chart, Graphics, Math, MyTypes;

type
  TResType = (rt_OneHole,          // ��������� ���������
              rt_Plug,             // ��������
              rt_WeldedJoint,      // ������� ����
              rt_BoltJoint,        // �������� ����
              rt_FlangeRail,       // ����� �����
              rt_Blade,            // ������ ����������� ��������
              rt_Switch,           // �������
              rt_Frog,             // ����������
              rt_SupportingRail,   // ������ �����
              rt_Defect,           // ������
              rt_UCSite,           // ��������������������� �������
              rt_TEDoublePass,     // ��� �������� ������� �� ��
              rt_TESkipBJButton,   // �� ������ ������ ��
              rt_TESens,           // ��������� ����������������
              rt_TESpeed,          // ��������� �������� ��������
              rt_TEBSLost);        // ��������� ���������� - ������� ��

  TCrdMode = (cm_Normal, cm_Reduce);

  TMarkItem = record
    Used: Boolean;
    AllDelay: Boolean;
    Delay: TRange;
  end;

  TMarkList = array [1..7] of TMarkItem;


const
  ResNames: array [TResType] of string = ('��������� ���������',              
                                          '��������',                         
                                          '������� ����',                     
                                          '�������� ����',                    
                                          '����������',                       
                                          '������',                           
                                          '�������',                          
                                          '����������',                       
                                          '������ �����',
                                          '���������� �� ������',                           
                                          '��������������������� �������',    
                                          '��� �������� ������� �� ��',       
                                          '�� ������ ������ ��',              
                                          '��������� ����������������',
                                          '��������� �������� ��������',
                                          '���������� ������� �������');

  UsedRes: array [TResType] of Boolean = (False,  // ��������� ���������
                                          False,  // ��������
                                          False,  // ������� ����
                                          True,   // �������� ����
                                          False,  // ����� �����
                                          False,  // ������ ����������� ��������
                                          False,  // �������
                                          False,  // ����������
                                          False,  // ������ �����
                                          True,   // ������
                                          True,   // ��������������������� �������
                                          False,  // ��� �������� ������� �� ��
                                          False,  // �� ������ ������ ��
                                          True,   // ��������� ����������������
                                          True,   // ��������� �������� ��������
                                          True);  // ���������� ��

type
  TResTyp = (rt_VLine, rt_DblVLine, rt_Text, rt_Rect);

  THeadBodyInt  = record
    Head: Integer;
    Body: Integer;
  end;

  TAnalyzRange = array [r_Left..r_Right] of THeadBodyInt;

  TResFigZone = record
    R: RRail_;
//    Crd: TRange;
    Crd: array [0..2] of TRange;
    StTape: Integer;
    EdTape: Integer;
    Color: Integer;
  end;

  TResFigLine = record
    R: RRail_;
    Crd: Integer;
    StTape: Integer;
    EdTape: Integer;
    Color: Integer;
    Type_: Integer;
  end;

  TResFigPoint = record
    R: RRail_;
    Crd: Integer;
    Tape: Integer;
    Color: Integer;
    Text: string;
  end;


  TResPBZones = record
    R: RRail_;
    Ch: Integer;
    Crd: array [0..2] of TRange;
    Del: TRange;
    Color1: Integer;
    Color2: Integer;
  end;

  TResItem = record
    R: RRail_;
    Typ: TResType;
    Bounds: TRange;
    Len: Integer;

    Zones: array of TResFigZone;
    Lines: array of TResFigLine;
    Points: array of TResFigPoint;
    PBMark: array of TResPBZones;
    Params: string;

 //   FigsCount: Integer;
 //   Name: string [25];
 //   Coord: string [35];
  end;

  TRecRes = array of TResItem;

  procedure ChartXZone(Chart: TChart; X1, X2, Width: Integer; Color: TColor);
  procedure ChartYZone(Chart: TChart; Y1, Y2, Width: Integer; Color: TColor);
  procedure ChartText(Chart: TChart; X, Y: Extended; Text: string);
  procedure FixTime;
  procedure TimeToLog(Log: TStringList; Text: string);
  function ResTypeToGroup(Typ: TResType): string;
  function PointToLine(Pt, StLine, EdLine: TPoint): Integer;

implementation


const
  FixTimeVal: DWord = 0;

procedure ChartXZone(Chart: TChart; X1, X2, Width: Integer; Color: TColor);

function GetChartPos(X, Y: Integer): TPoint;
begin
  Result.X:= Chart.BottomAxis.CalcPosValue(X);
  Result.Y:= Chart.LeftAxis.CalcPosValue(Y);
end;

begin
  with Chart.Canvas do
  begin
    Pen.Style:= psClear;
    Brush.Color:= Color;
    FillRect(Rect(Chart.BottomAxis.CalcPosValue(X1),
                  Chart.LeftAxis.CalcPosValue(Chart.LeftAxis.Minimum),
                  Chart.BottomAxis.CalcPosValue(X2) + Width,
                  Chart.LeftAxis.CalcPosValue(Chart.LeftAxis.Maximum)));
  end;
end;

procedure ChartYZone(Chart: TChart; Y1, Y2, Width: Integer; Color: TColor);

function GetChartPos(X, Y: Integer): TPoint;
begin
  Result.X:= Chart.BottomAxis.CalcPosValue(X);
  Result.Y:= Chart.LeftAxis.CalcPosValue(Y);
end;

begin
  with Chart.Canvas do
  begin
    Pen.Style:= psClear;
    Brush.Color:= Color;
    FillRect(Rect(Chart.BottomAxis.CalcPosValue(Chart.BottomAxis.Minimum),
                  Chart.LeftAxis.CalcPosValue(Y1),
                  Chart.BottomAxis.CalcPosValue(Chart.BottomAxis.Maximum),
                  Chart.LeftAxis.CalcPosValue(Y2) + Width));
  end;
end;

procedure ChartText(Chart: TChart; X, Y: Extended; Text: string);
var
  Pt: TPoint;

function GetChartPos(X, Y: Extended): TPoint;
begin
  Result.X:= Chart.BottomAxis.CalcPosValue(X);
  Result.Y:= Chart.LeftAxis.CalcPosValue(Y);
end;

begin
  with Chart.Canvas do
  begin
    Font.Name:= 'Arial';
    Font.Height:= 12;
    Font.Color:= clBlack;
    Pt:= GetChartPos(X, Y);
    Pen.Color:= clBlack;
    Pen.Width:= 1;
    Brush.Style:= bsClear;
    MoveTo(Pt.X, Pt.Y);
    LineTo(Pt.X + 15, Pt.Y + 15);
    TextOut(Pt.X + 15, Pt.Y + 15, Text);
  end;
end;

procedure FixTime;
begin
  FixTimeVal:= GetTickCount;
end;

procedure TimeToLog(Log: TStringList; Text: string);
begin
  Log.Add(Format(Text + ' (%3.3f ���)', [(GetTickCount - FixTimeVal) / 1000]));
  FixTime;
end;

function ResTypeToGroup(Typ: TResType): string;
begin
  case Typ of
    rt_OneHole,          // ��������� ���������
    rt_Plug,             // ��������
    rt_WeldedJoint,      // ������� ����
    rt_BoltJoint,        // �������� ����
    rt_FlangeRail,       // ����� �����
    rt_Blade,            // ������ ����������� ��������
    rt_Switch,           // �������
    rt_Frog,             // ����������
    rt_SupportingRail:   // ������ �����
               Result:= '�������������� �������';

    rt_Defect:           // ������
               Result:= '���������� �� ������';

    rt_UCSite,           // ��������������������� �������
    rt_TEDoublePass,     // ��� �������� ������� �� ��
    rt_TESkipBJButton,   // �� ������ ������ ��
    rt_TESens,           // ��������� ����������������
    rt_TESpeed,          // ��������� �������� ��������
    rt_TEBSLost:
               Result:= '��������� ���������� ��������';
  end;
end;

function PointToLine(Pt, StLine, EdLine: TPoint): Integer;
var
  x_, y_: Integer;
  A: Integer;
  B: Integer;
  k: single;

begin
  if EdLine.X - StLine.X = 0 then Result:= 0
  else
  begin
    K:= (EdLine.Y - StLine.Y) / (EdLine.X - StLine.X);
    Result:= Pt.Y - (StLine.Y + Round(K * (Pt.X - StLine.X)));
  end;
end;

end.
