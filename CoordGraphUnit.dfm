object CoordGraphForm: TCoordGraphForm
  Left = 198
  Top = 121
  Caption = 'CoordGraphForm'
  ClientHeight = 576
  ClientWidth = 862
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Chart1: TChart
    Left = 0
    Top = 57
    Width = 862
    Height = 519
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    View3D = False
    Align = alClient
    TabOrder = 0
    ExplicitTop = 54
    object Dis_Crd: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      LinePen.Width = 2
      Pointer.HorizSize = 3
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.VertSize = 3
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Sys_Crd: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clBlue
      LinePen.Width = 2
      Pointer.HorizSize = 3
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.VertSize = 3
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object UMUA_Left: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = 4227327
      LinePen.Width = 2
      Pointer.HorizSize = 3
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.VertSize = 3
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object UMUB_Left: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clLime
      LinePen.Width = 2
      Pointer.HorizSize = 3
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.VertSize = 3
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object UMUA_Right: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clPurple
      LinePen.Width = 2
      Pointer.HorizSize = 3
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.VertSize = 3
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object UMUB_Right: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = clAqua
      LinePen.Width = 2
      Pointer.HorizSize = 3
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.VertSize = 3
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 57
    Align = alTop
    TabOrder = 1
    object Bevel1: TBevel
      Left = 101
      Top = 4
      Width = 9
      Height = 50
    end
    object Bevel2: TBevel
      Left = 399
      Top = 4
      Width = 9
      Height = 50
    end
    object DisCrd: TCheckBox
      Left = 8
      Top = 8
      Width = 89
      Height = 17
      Caption = 'Display Coord'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = DisCrdClick
    end
    object SysCrd: TCheckBox
      Left = 8
      Top = 31
      Width = 89
      Height = 17
      Caption = 'System Coord'
      Checked = True
      State = cbChecked
      TabOrder = 1
      OnClick = SysCrdClick
    end
    object AL: TCheckBox
      Left = 120
      Top = 8
      Width = 131
      Height = 17
      Caption = 'Sys Coord UMUA Left'
      TabOrder = 2
      OnClick = ALClick
    end
    object BL: TCheckBox
      Left = 120
      Top = 31
      Width = 131
      Height = 17
      Caption = 'Sys Coord UMUB Left'
      TabOrder = 3
      OnClick = BLClick
    end
    object AR: TCheckBox
      Left = 263
      Top = 8
      Width = 135
      Height = 17
      Caption = 'Sys Coord UMUA Right'
      TabOrder = 4
      OnClick = ARClick
    end
    object BR: TCheckBox
      Left = 263
      Top = 31
      Width = 135
      Height = 17
      Caption = 'Sys Coord UMUB Right'
      TabOrder = 5
      OnClick = BRClick
    end
    object cbShowPoints: TCheckBox
      Left = 414
      Top = 21
      Width = 109
      Height = 17
      Caption = 'Points'
      TabOrder = 6
      OnClick = cbShowPointsClick
    end
  end
end
