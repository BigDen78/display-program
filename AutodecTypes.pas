unit AutodecTypes;

interface

uses MyTypes, Generics.Collections;

type
  // ������ �������
  TClassGroup = (
    cgNull = 0, // ��� ������
    cgHole = 1, // ��������� ���������
    cgJoint = 2, // ����
    cgSwitch = 3, // �������
    cgDefect = 4, // ���������� �� ������
    cgZone = 5, // ���� ��������
    cgControlVilation = 6 // ��������� ���������� ��������
    );

  // ���� ���������
  TEntityType = (
    etNotAnalyzedZone = 1, // �� ������������� �������
    etBoltHole = 3, // �������� ���������
    etJoint = 4, // ����
    etSwitch = 5, // ���������� �������
    etDefect = 7, // ������
    etReflection = 8, // ����������
    etFeatureZone = 10, // ������ ����
    etClearZone = 12 // ������� ������� ������
    );

  // ����� �������������� ���������
  TClassifierItem = record
    id: integer;
    name: string;
    shortName: string;
    entityType: TEntityType;
    group: TClassGroup;
    isDefect: Boolean;
  end;
  TClassList = TDictionary<Integer, TClassifierItem>;

  // ������� ����������� ��������
  TDefLevel = (
    dlDelayRange = 1, // ������ ������ ������ (���������� ��� ���������)
    dlChannel = 2, // �� ������ ������ (���������� ��� ���������)
    dlChannelsDelayRange = 3, // ��������� ������� � ������������� �� ���������
    dlChannels = 4, // �� ������ ������, ��������� �������
    dlRail = 5, // �� ������ ������
    dlTrack = 6 // �� ��� ������
    );


  // ���� �������� ������ ������
  TEntityArea = record
    channelId: Integer;
    startSysCoordinate: Integer;
    finishSysCoordinate: Integer;
    startDisCoordinate: Integer;
    finishDisCoordinate: Integer;
    minMaxDelays: TMinMax;
    startFinishDelays: TMinMax;
  end;

  // ��� �������� (��������)
  TEntity = record
    id: Integer;
    classId: Integer;
    rail: RRail;
    channelId: Integer;
    defLevel: TDefLevel;
    startSysCoordinate: Integer;
    finishSysCoordinate: Integer;
    startDisCoordinate: Integer;
    finishDisCoordinate: Integer;
    isDublicate: Boolean;
    inHiddenZone: Boolean;
    inBackwardZone: Boolean;
    areas: array of TEntityArea;
    viewChannel: TViewChannel; // ����������� ������
  end;
  TEntityList = TList<TEntity>;
  PEntity = ^TEntity;

  TRailEntities = class
  public
    items: array[RRail] of TEntityList;
    constructor Create;
    destructor Destroy;
    procedure Init(rail: RRail);
  end;

  TAutodecFileData = class
  public
    fileName: string;
    dataLoading: boolean;
    entities: TDictionary<Integer, TRailEntities>;
    ready: boolean;
    lastDisCoords: TMinMax;
    calcScreenMode: Boolean; // ����� ������� ������ � �������� ������
    isLarge: Boolean;

    constructor Create;
    destructor Destroy;
    procedure Init(classId: Integer; rail: RRail);
  end;

implementation

{ TRailEntities }

constructor TRailEntities.Create;
var
  i: RRail;
begin
  for i := low(items) to high(items) do
  begin
    items[i] := nil;
  end;
end;

destructor TRailEntities.Destroy;
var
  i: RRail;
begin
  for i := low(items) to high(items) do
  begin
    if items[i] <> nil then
    begin
      items[i].Free;
    end;
  end;
end;

procedure TRailEntities.Init(rail: RRail);
begin
  if items[rail] = nil then
  begin
    items[rail] := TEntityList.Create;
  end;
end;


{ TAutodecData }

constructor TAutodecFileData.Create;
begin
  entities := TDictionary<Integer, TRailEntities>.Create;
  dataLoading := False;
  ready := False;
  calcScreenMode := False;
end;

destructor TAutodecFileData.Destroy;
var
  id: Integer;
  i: RRail;
begin
  if not Assigned(entities) then
    Exit;

  for id in entities.Keys do
  begin
    entities[id].Free;
  end;
  entities.Free;
end;

procedure TAutodecFileData.Init(classId: Integer; rail: RRail);
var
  item: TRailEntities;
begin
  if not Assigned(entities) then
    entities := TDictionary<Integer, TRailEntities>.Create;

  if not entities.ContainsKey(classId) then
  begin
    item := TRailEntities.Create;
    entities.Add(classId, item);
  end;
  entities[classId].Init(rail);
end;



end.
