object HandScanForm: THandScanForm
  Left = 249
  Top = 127
  Caption = #1056#1091#1095#1085#1086#1077' '#1089#1082#1072#1085#1080#1088#1086#1074#1072#1085#1080#1077
  ClientHeight = 588
  ClientWidth = 732
  Color = clBtnFace
  Constraints.MinHeight = 350
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnPaint = PaintBoxPaint
  OnResize = FormResize
  OnShortCut = FormShortCut
  PixelsPerInch = 96
  TextHeight = 13
  object Button3: TButton
    Left = 12
    Top = 17
    Width = 0
    Height = 9
    Caption = 'Button3'
    TabOrder = 0
  end
  object Button4: TButton
    Left = 12
    Top = 9
    Width = 0
    Height = 9
    Caption = 'Button4'
    TabOrder = 1
  end
  object ScrollBar1: TScrollBar
    Left = 0
    Top = 571
    Width = 732
    Height = 17
    Align = alBottom
    PageSize = 0
    TabOrder = 2
    OnChange = ScrollBar1Change
  end
  object Chart1: TChart
    Left = 0
    Top = 0
    Width = 732
    Height = 209
    Legend.Visible = False
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    View3D = False
    Align = alTop
    TabOrder = 3
    Visible = False
    object Series1: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      LinePen.Width = 4
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 209
    Width = 732
    Height = 362
    Align = alClient
    AutoSize = True
    BevelOuter = bvNone
    FullRepaint = False
    TabOrder = 4
    OnResize = Panel1Resize
    object PaintBox1: TPaintBox
      Left = 0
      Top = 0
      Width = 732
      Height = 362
      Align = alClient
      OnClick = PaintBox1Click
      OnPaint = PaintBox1Paint
      ExplicitTop = 6
    end
  end
end
