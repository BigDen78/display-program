unit FiltrUnit;

interface

uses
  MyTypes, AviconDataSource, Classes, Dialogs, SysUtils, Forms, LanguageUnit, AviconTypes;

type

  TFtrCh1GisItem = packed record
    ECount: Byte;
    StGlue: Integer;
    LastCrd: Integer;
    MaxLen: Byte;
  end;

  TFiltrData = packed record
    Reg: TRange;
    Ch1Gis: array [rLeft..rRight, 0..16] of array of array of TFtrCh1GisItem;
    Ch27Gis: array [rLeft..rRight, 0..16] of array of array of Byte;
  end;

  TFiltr = class
  private
    FFiltrIdx: Integer;         // ������� ������ ������ ����������
    FDatSrc: TAvk11DatSrc;
    FBSignal: TList;
    FBMList: Pointer;
    FIdx: array [rLeft..rRight, 0..16] of Integer;
    FCalcParam: TCalcFiltrParams;
    FThParam: TThFiltrParams;
    FDelayLen: array [0..16] of Integer;
    FAmplLen: Integer;
    FBMIdx: Integer;
    FLastSysCoord: Integer;
    FStCrd: Integer;
    FEdCrd: Integer;
    FZPChList: array of Integer;
    FNoZPChList: array of Integer;

  protected
    function DataNotify(StartDisCoord: Integer): Boolean;
    function GetPCalcParam: PCalcFiltrParams;
    function GetPThParam: PThFiltrParams;
    procedure ClearArray;

  public
    FFiltr: array of TFiltrData;
    FSummWinLen: Single;
    OnProgress: TNotifyEvent;


    constructor Create;
    destructor Destroy;

    function GetFiltr27(Idx: Integer; Rail: RRail_; Ch, AmplIdx, DelayIdx: Integer): Integer;
    function GetFiltr1ECount(Idx: Integer; Rail: RRail_; Ch, AmplIdx, DelayIdx: Integer): Integer;
    function GetFiltr1MaxLen(Idx: Integer; Rail: RRail_; Ch, AmplIdx, DelayIdx: Integer): Integer;

    procedure Calc(DatSrc: TAvk11DatSrc; BSignal: TList; BMList: Pointer; StCrd, EdCrd: Integer; ZPChList: array of Integer; NoZPChList: array of Integer);
    procedure ReCalc;
    function TestEcho(DisCoord: Integer; Rail: RRail_; Ch, Ampl, Delay: Integer): Boolean;
    function StartFilter(StartDisCoord: Integer): Integer;
    property PCalcParam: PCalcFiltrParams read GetPCalcParam;
    property PThParam: PThFiltrParams read GetPThParam;
  end;

implementation

uses
  Math, RecData, FiltrParamUnit, RecProgressUnit;

var
  EQ: Integer;


constructor TFiltr.Create;
begin
  OnProgress:= nil;
end;

destructor TFiltr.Destroy;
begin
  ClearArray;
end;

function TFiltr.GetPCalcParam: PCalcFiltrParams;
begin
  Result:= @FCalcParam;
end;

function TFiltr.GetPThParam: PThFiltrParams;
begin
  Result:= @FThParam;
end;

function TFiltr.DataNotify(StartDisCoord: Integer): Boolean;
var
  R: RRail_;
  I: Integer;
  ChIdx: Integer;
  Ch: Integer;
  DelIdx: Integer;
  AmplIdx: Integer;
  Flg: Boolean;

begin
//  if FDatSrc.CurSysCoord < FLastSysCoord then Exit;
//  FLastSysCoord:= FDatSrc.CurSysCoord;

//  if not FDatSrc.BackMotion then
  begin
    for R:= rLeft to rRight do
    begin
      for ChIdx:= 0 to High(FNoZPChList) do
      begin
        Ch:= FNoZPChList[ChIdx];
        with FDatSrc.CurEcho[R, Ch] do
          for I:= 1 to Count do
            if (Delay[I] >= FCalcParam.MinDelay[Ch]) and (Delay[I] <= FCalcParam.MaxDelay[Ch]) and
               (Ampl[I] >= FCalcParam.AmplMin) and (Ampl[I] <= FCalcParam.AmplMax) then
            begin
              DelIdx:= (Delay[I] - FCalcParam.MinDelay[Ch]) div FCalcParam.DelayStep[Ch];
              AmplIdx:= (Ampl[I] - FCalcParam.AmplMin) div FCalcParam.AmplStep;
              if FFiltr[FFiltrIdx].Ch27Gis[R, Ch, AmplIdx, DelIdx] < 255 then Inc(FFiltr[FFiltrIdx].Ch27Gis[R, Ch, AmplIdx, DelIdx]);
            end;
      end;

      for ChIdx:= 0 to High(FZPChList) do
      begin
        Ch:= FZPChList[ChIdx];
        with FDatSrc.CurEcho[R, Ch] do
          for I:= 1 to Count do
            if (Delay[I] >= FCalcParam.MinDelay[Ch]) and (Delay[I] <= FCalcParam.MaxDelay[Ch]) and
               (Ampl[I] >= FCalcParam.AmplMin) and (Ampl[I] <= FCalcParam.AmplMax) then
            begin
              DelIdx:= (Delay[I] - FCalcParam.MinDelay[Ch]) div FCalcParam.DelayStep[Ch];
              AmplIdx:= (Ampl[I] - FCalcParam.AmplMin) div FCalcParam.AmplStep;
              with FFiltr[FFiltrIdx].Ch1Gis[R, Ch, AmplIdx, DelIdx] do
              begin
                                                        // ��������� ����������
                if FDatSrc.CurDisCoord - LastCrd > FCalcParam.Ch1Len then
                begin
                  if ECount < 255 then Inc(ECount);
                  StGlue:= FDatSrc.CurDisCoord;
                end;
                LastCrd:= FDatSrc.CurDisCoord;
                MaxLen:= Min(255, Max(MaxLen, FDatSrc.CurDisCoord - StGlue));
              end;
            end;
      end;
    end;
  end;
  Result:= True;
end;

procedure TFiltr.ClearArray;
var
  R: RRail_;
  I, J: Integer;
  Ch: Integer;
  ChIdx: Integer;

begin
  for I:= 0 to High(FFiltr) do
    for R:= rLeft to rRight do
    begin
      for ChIdx:= 0 to High(FZPChList) do
      begin
        Ch:= FZPChList[ChIdx];
        for J:= 0 to High(FFiltr[I].Ch1Gis[R, Ch]) do SetLength(FFiltr[I].Ch1Gis[R, Ch, J], 0);
        SetLength(FFiltr[I].Ch1Gis[R, Ch], 0);
      end;
//      for Ch:= 2 to 7 do
      for ChIdx:= 0 to High(FNoZPChList) do
      begin
        Ch:= FNoZPChList[ChIdx];
        for J:= 0 to High(FFiltr[I].Ch27Gis[R, Ch]) do SetLength(FFiltr[I].Ch27Gis[R, Ch, J], 0);
        SetLength(FFiltr[I].Ch27Gis[R, Ch], 0);
      end;
    end;
end;

procedure TFiltr.Calc(DatSrc: TAvk11DatSrc; BSignal: TList; BMList: Pointer; StCrd, EdCrd: Integer; ZPChList: array of Integer; NoZPChList: array of Integer);
var
  R: RRail_;
  I, J, L, K: Integer;
  ChIdx: Integer;
  Ch: Integer;
  Reg_: TRange;

begin

  SetLength(FZPChList, Length(ZPChList));
  Move(ZPChList[0], FZPChList[0], Length(ZPChList) * SizeOf(Integer));
  SetLength(FNoZPChList, Length(NoZPChList));
  Move(NoZPChList[0], FNoZPChList[0], Length(NoZPChList) * SizeOf(Integer));

  if DatSrc.isFilus_X27_Scheme1 or DatSrc.isEGOUS_Scheme1 then
    LoadParamFromFile(ExtractFilePath(Application.ExeName) + 'cfg_1.dat', @FThParam, @FCalcParam) // Filus X27

  else

  if DatSrc.isFilus_X27W_Scheme1 or DatSrc.isEGOUS_WheelScheme1 or DatSrc.isVMTUS_Scheme_1 then
    LoadParamFromFile(ExtractFilePath(Application.ExeName) + 'cfg_2.dat', @FThParam, @FCalcParam) // Filus X27

  else

  if DatSrc.isFilus_X27W_Scheme2 or DatSrc.isEGOUS_WheelScheme2 or DatSrc.isVMTUS_Scheme_2 then
    LoadParamFromFile(ExtractFilePath(Application.ExeName) + 'cfg_3.dat', @FThParam, @FCalcParam)  // Filus X27

  else

  if DatSrc.isVMTUS_Scheme_4 or DatSrc.isFilus_X17DW then // ���� � ����� ���
    LoadParamFromFile(ExtractFilePath(Application.ExeName) + 'cfg_4.dat', @FThParam, @FCalcParam)

  else Exit;

// DOTO - ������ �� ����� ������

  FDatSrc:= DatSrc;
  FBSignal:= BSignal;
  FBMList:= BMList;
  FLastSysCoord:= 0;
  FStCrd:= Max(0, StCrd) - 2 * FCalcParam.CrdWinWidth;
  FEdCrd:= Min(DatSrc.MaxDisCoord, EdCrd) + 2 * FCalcParam.CrdWinWidth;

  SetLength(FFiltr, Ceil((FEdCrd - FStCrd + 1) / (FCalcParam.CrdWinWidth { / (DatSrc.Header.ScanStep / 100) } ))); // ������ ����������

  FAmplLen:= Ceil((1 + FCalcParam.AmplMax - FCalcParam.AmplMin) / FCalcParam.AmplStep);

  for I:= 0 to High(FFiltr) do                 // ������������� ������� ������� � ������������ � ����������� ����������
    for R:= rLeft to rRight do
    begin
      for ChIdx:= 0 to High(ZPChList) do
      begin
        Ch:= ZPChList[ChIdx];
        SetLength(FFiltr[I].Ch1Gis[R, Ch], FAmplLen);
        FDelayLen[Ch]:= Ceil((1 + FCalcParam.MaxDelay[Ch] - FCalcParam.MinDelay[Ch]) / FCalcParam.DelayStep[Ch]);
        for J:= 0 to FAmplLen - 1 do SetLength(FFiltr[I].Ch1Gis[R, Ch, J], FDelayLen[Ch]);
      end;

      for ChIdx:= 0 to High(NoZPChList) do
      begin
        Ch:= NoZPChList[ChIdx];
        SetLength(FFiltr[I].Ch27Gis[R, Ch], FAmplLen);
        FDelayLen[Ch]:= Ceil((1 + FCalcParam.MaxDelay[Ch] - FCalcParam.MinDelay[Ch]) / FCalcParam.DelayStep[Ch]);
        for J:= 0 to FAmplLen - 1 do SetLength(FFiltr[I].Ch27Gis[R, Ch, J], FDelayLen[Ch]);
      end;
    end;

  FBMIdx:= 0;
  for I:= 0 to High(FFiltr) do
  begin
    try
      if Assigned(OnProgress) then OnProgress( Pointer(100 * I div Length(FFiltr) ) );
    except
    end;
    FFiltrIdx:= I;
    FFiltr[I].Reg.StCrd:= FStCrd + I * FCalcParam.CrdWinWidth;
    FFiltr[I].Reg.EdCrd:= FStCrd + (I + 1) * FCalcParam.CrdWinWidth - 1;
    FDatSrc.LoadData(FFiltr[I].Reg.StCrd, FFiltr[I].Reg.EdCrd, 0, DataNotify);
  end;

  FSummWinLen:= FCalcParam.CrdWinWidth * FThParam.CrdWinCount / 1000;
end;

procedure TFiltr.ReCalc;
var
  RecProgForm: TRecProgressForm;

begin
  RecProgForm:= TRecProgressForm.Create(nil);
  RecProgForm.SetMode(1);
  RecProgForm.Visible:= True;
  RecProgForm.BringToFront;
  RecProgForm.Refresh;
  RecProgForm.SetDiap(50, 100);
  RecProgForm.Caption:= LangTable.Caption['Common:Wait'];
  OnProgress:= RecProgForm.OnProgress;
  ClearArray;

  Calc(FDatSrc, FBSignal, FBMList, FStCrd, FEdCrd, FZPChList, FNoZPChList);
  OnProgress:= nil;
  RecProgForm.Free;
end;

function TFiltr.StartFilter(StartDisCoord: Integer): Integer;
var
  R: RRail_;
  I, J: Integer;

begin
  Result:= - 1;
  if StartDisCoord < 0 then StartDisCoord:= 0;

  FSummWinLen:= FCalcParam.CrdWinWidth * FThParam.CrdWinCount / 1000;

  for I:= 0 to High(FFiltr) do
    if PtInRange(StartDisCoord, FFiltr[I].Reg) then
    begin
      FFiltrIdx:= I;
      Result:= I;
      Break;
    end;


  for J := 0 to FBSignal.Count - 1 do

    for R:= rLeft to rRight do
      for I:= 0 to High(TBSignal(FBSignal[J]).FBSPos[R]) - 1 do // ���������� ������� ������ ���� ������� �������
        if (StartDisCoord >= TBSignal(FBSignal[J]).FBSPos[R, I].Crd) and
           (StartDisCoord <= TBSignal(FBSignal[J]).FBSPos[R, I + 1].Crd) then
        begin
          FIdx[R, TBSignal(FBSignal[J]).Ch]:= I;
          Break;
        end;
end;

function TFiltr.GetFiltr27(Idx: Integer; Rail: RRail_; Ch, AmplIdx, DelayIdx: Integer): Integer;
var
  CIdx, AIdx, DIdx: Integer;
  CIdx_, AIdx_, DIdx_: Integer;
  I, J, K: Integer;

begin

  Result:= 0;
  for CIdx_:= Idx - (FThParam.CrdWinCount - 1) div 2 to Idx + (FThParam.CrdWinCount - 1) div 2 do
  begin
    CIdx:= CIdx_;
    if CIdx < 0 then CIdx:= 0;
    if CIdx > High(FFiltr) then CIdx:= High(FFiltr);

    for AIdx_:= AmplIdx - (FThParam.AmplWinCount - 1) div 2 to AmplIdx + (FThParam.AmplWinCount - 1) div 2 do
    begin
      AIdx:= AIdx_;
      if AIdx < 0 then AIdx:= 0;
      if AIdx > High(FFiltr[CIdx].Ch27Gis[Rail, Ch]) then AIdx:= High(FFiltr[CIdx].Ch27Gis[Rail, Ch]);

   //   if EQ = 135 then ShowMessage('!');

      if AIdx <> - 1 then
        for DIdx_:= DelayIdx - (FThParam.DlyWinCount - 1) div 2 to DelayIdx + (FThParam.DlyWinCount - 1) div 2 do
        begin
          DIdx:= DIdx_;
          if DIdx < 0 then DIdx:= 0;

          if DIdx > High(FFiltr[CIdx].Ch27Gis[Rail, Ch, AIdx]) then DIdx:= High(FFiltr[CIdx].Ch27Gis[Rail, Ch, AIdx]);
          Result:= Result + FFiltr[CIdx].Ch27Gis[Rail, Ch, AIdx, DIdx];

   //       inc(EQ);
        end;
    end;
  end;
end;

function TFiltr.GetFiltr1ECount(Idx: Integer; Rail: RRail_; Ch, AmplIdx, DelayIdx: Integer): Integer;
var
  CIdx, AIdx, DIdx: Integer;
  CIdx_, AIdx_, DIdx_: Integer;
  I, J, K: Integer;

begin
  Result:= 0;
  for CIdx_:= Idx - (FThParam.CrdWinCount - 1) div 2 to Idx + (FThParam.CrdWinCount - 1) div 2 do
  begin
    CIdx:= CIdx_;
    if CIdx < 0 then CIdx:= 0;
    if CIdx > High(FFiltr) then CIdx:= High(FFiltr);
    for AIdx_:= AmplIdx - (FThParam.AmplWinCount - 1) div 2 to AmplIdx + (FThParam.AmplWinCount - 1) div 2 do
    begin
      AIdx:= AIdx_;
      if AIdx < 0 then AIdx:= 0;
      if AIdx > High(FFiltr[CIdx].Ch1Gis[Rail, Ch]) then AIdx:= High(FFiltr[CIdx].Ch1Gis[Rail, Ch]);
      for DIdx_:= DelayIdx - (FThParam.DlyWinCount - 1) div 2 to DelayIdx + (FThParam.DlyWinCount - 1) div 2 do
      begin
        DIdx:= DIdx_;
        if DIdx < 0 then DIdx:= 0;
        if DIdx > High(FFiltr[CIdx].Ch1Gis[Rail, Ch, AIdx]) then DIdx:= High(FFiltr[CIdx].Ch1Gis[Rail, Ch, AIdx]);
        Result:= Result + FFiltr[CIdx].Ch1Gis[Rail, Ch, AIdx, DIdx].ECount;
      end;
    end;
  end;
end;

function TFiltr.GetFiltr1MaxLen(Idx: Integer; Rail: RRail_; Ch, AmplIdx, DelayIdx: Integer): Integer;
var
  CIdx, AIdx, DIdx: Integer;
  CIdx_, AIdx_, DIdx_: Integer;
  I, J, K: Integer;

begin
  Result:= 0;
  for CIdx_:= Idx - (FThParam.CrdWinCount - 1) div 2 to Idx + (FThParam.CrdWinCount - 1) div 2 do
  begin
    CIdx:= CIdx_;
    if CIdx < 0 then CIdx:= 0;
    if CIdx > High(FFiltr) then CIdx:= High(FFiltr);
    for AIdx_:= AmplIdx - (FThParam.AmplWinCount - 1) div 2 to AmplIdx + (FThParam.AmplWinCount - 1) div 2 do
    begin
      AIdx:= AIdx_;
      if AIdx < 0 then AIdx:= 0;
      if AIdx > High(FFiltr[CIdx].Ch1Gis[Rail, Ch]) then AIdx:= High(FFiltr[CIdx].Ch1Gis[Rail, Ch]);
      for DIdx_:= DelayIdx - (FThParam.DlyWinCount - 1) div 2 to DelayIdx + (FThParam.DlyWinCount - 1) div 2 do
      begin
        DIdx:= DIdx_;
        if DIdx < 0 then DIdx:= 0;
        if DIdx > High(FFiltr[CIdx].Ch1Gis[Rail, Ch, AIdx]) then DIdx:= High(FFiltr[CIdx].Ch1Gis[Rail, Ch, AIdx]);
        Result:= Result + FFiltr[CIdx].Ch1Gis[Rail, Ch, AIdx, DIdx].MaxLen;
      end;
    end;
  end;
end;

function TFiltr.TestEcho(DisCoord: Integer; Rail: RRail_; Ch, Ampl, Delay: Integer): Boolean;
var
  I, J: Integer;
  DelIdx: Integer;
  AmplIdx: Integer;
  ZPFlag: Boolean;
  MSMFlag: Boolean;

begin
  Result:= True;

  if (FFiltrIdx < 0) or (FFiltrIdx > High(FFiltr)) then Exit;                     // �������� ������� - ������ ����������
  if (Delay < FCalcParam.MinDelay[Ch]) or (Delay > FCalcParam.MaxDelay[Ch]) or           // ��������
     (Ampl < FCalcParam.AmplMin) or (Ampl > FCalcParam.AmplMax) then Exit;               // ���������

  if DisCoord > FFiltr[FFiltrIdx].Reg.EdCrd then
    Inc(FFiltrIdx);                  // ��������� �������� �� ������� ����������

  for J := 0 to FBSignal.Count - 1 do
  begin
    while (DisCoord > TBSignal(FBSignal[J]).FBSPos[Rail, FIdx[Rail, 1]].Crd) and
          (FIdx[Rail, 1] < High(TBSignal(FBSignal[J]).FBSPos[Rail])) do Inc(FIdx[Rail, TBSignal(FBSignal[J]).Ch]);

    if (Ch = TBSignal(FBSignal[J]).Ch) and      // ������ ������ �� �������
       (Delay >= TBSignal(FBSignal[J]).FBSPos[Rail, FIdx[Rail, TBSignal(FBSignal[J]).Ch]].MinDelay) and
       (Delay <= TBSignal(FBSignal[J]).FBSPos[Rail, FIdx[Rail, TBSignal(FBSignal[J]).Ch]].MaxDelay) then Exit;
  end;

  DelIdx:= (Delay - FCalcParam.MinDelay[Ch]) div FCalcParam.DelayStep[Ch];
  AmplIdx:= (Ampl - FCalcParam.AmplMin) div FCalcParam.AmplStep;

  ZPFlag:= False;
  MSMFlag:= False;
  for I := 0 to High(FZPChList) do
    if FZPChList[I] = Ch then
    begin
      if FDatSrc.isTwoWheels then ZPFlag:= Ch in [1, 11]; // 0 ���+��� - �������
      if FDatSrc.isVMTUS_Scheme_4 or
         FDatSrc.isFilus_X17DW then MSMFlag:= Ch in [1, 2, 3]; // 0 ���+���, 45(55)/90 ���+��� - ����, DB
      Break;
    end;

  if FDatSrc.isFilus_X27_Scheme1 or
     FDatSrc.isFilus_X27W_Scheme1 or
     FDatSrc.isEGOUS_WheelScheme1 or
     FDatSrc.isVMTUS_Scheme_1 then
  begin

    if ((Ch in [2..5, 12..13]) and
        (GetFiltr27(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1])) or

       ((Ch in [6, 7]) and                                                        // ���� ��
        (Delay >= FThParam.ChParam[Ch].BHMinDelay) and
        (Delay <= FThParam.ChParam[Ch].BHMaxDelay) and
        (GetFiltr27(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[2])) or

       ((Ch in [6, 7]) and                                                        // ��� ���� ��
        ((Delay < FThParam.ChParam[Ch].BHMinDelay) or
         (Delay > FThParam.ChParam[Ch].BHMaxDelay)) and
        (GetFiltr27(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1])) or

       ((ZPFlag) and
        (Delay >= FThParam.ChParam[Ch].BHMinDelay) and                            // ���� ��
        (Delay <= FThParam.ChParam[Ch].BHMaxDelay) and
        ((GetFiltr1ECount(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[2]) or
         (GetFiltr1MaxLen(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].LenTh[2]))) or

       ((ZPFlag) and
        ((Delay < FThParam.ChParam[Ch].BHMinDelay) or                             // ��� ���� ��
         (Delay > FThParam.ChParam[Ch].BHMaxDelay)) and
        ((GetFiltr1ECount(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1]) or
         (GetFiltr1MaxLen(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].LenTh[1]))) then Result:= False;
  end
  else
  if FDatSrc.isFilus_X27W_Scheme2 or
     FDatSrc.isEGOUS_WheelScheme2 or
     FDatSrc.isVMTUS_Scheme_2 then
  begin
    if ((Ch in [2..5, 10..13]) and
        (GetFiltr27(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1])) or

       ((Ch in [6, 7]) and                                                        // ���� ��
        (Delay >= FThParam.ChParam[Ch].BHMinDelay) and
        (Delay <= FThParam.ChParam[Ch].BHMaxDelay) and
        (GetFiltr27(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[2])) or

       ((Ch in [6, 7]) and                                                        // ��� ���� ��
        ((Delay < FThParam.ChParam[Ch].BHMinDelay) or
         (Delay > FThParam.ChParam[Ch].BHMaxDelay)) and
        (GetFiltr27(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1])) or

       ((ZPFlag) and
        (Delay >= FThParam.ChParam[Ch].BHMinDelay) and                            // ���� ��
        (Delay <= FThParam.ChParam[Ch].BHMaxDelay) and
        ((GetFiltr1ECount(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[2]) or
         (GetFiltr1MaxLen(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].LenTh[2]))) or

       ((ZPFlag) and
        ((Delay < FThParam.ChParam[Ch].BHMinDelay) or                             // ��� ���� ��
         (Delay > FThParam.ChParam[Ch].BHMaxDelay)) and
        ((GetFiltr1ECount(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1]) or
         (GetFiltr1MaxLen(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].LenTh[1]))) or

       ((MSMFlag) and False
//        ((Delay < FThParam.ChParam[Ch].BHMinDelay) or                             // ��� ���� ��
//         (Delay > FThParam.ChParam[Ch].BHMaxDelay)) and
{        ((GetFiltr1ECount(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1]) or
         (GetFiltr1MaxLen(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].LenTh[1]))} )
        then Result:= False;
  end
  else
  if FDatSrc.isVMTUS_Scheme_4 or
     FDatSrc.isFilus_X17DW then
  begin
    if ((Ch in [4..5, 10..13]) and
        (GetFiltr27(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1])) or

       ((Ch in [6, 7]) and                                                        // ���� ��
        (Delay >= FThParam.ChParam[Ch].BHMinDelay) and
        (Delay <= FThParam.ChParam[Ch].BHMaxDelay) and
        (GetFiltr27(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[2])) or

       ((Ch in [6, 7]) and                                                        // ��� ���� ��
        ((Delay < FThParam.ChParam[Ch].BHMinDelay) or
         (Delay > FThParam.ChParam[Ch].BHMaxDelay)) and
        (GetFiltr27(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1])) or

       ((ZPFlag) and
        (Delay >= FThParam.ChParam[Ch].BHMinDelay) and                            // ���� ��
        (Delay <= FThParam.ChParam[Ch].BHMaxDelay) and
        ((GetFiltr1ECount(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[2]) or
         (GetFiltr1MaxLen(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].LenTh[2]))) or

       ((ZPFlag) and
        ((Delay < FThParam.ChParam[Ch].BHMinDelay) or                             // ��� ���� ��
         (Delay > FThParam.ChParam[Ch].BHMaxDelay)) and
        ((GetFiltr1ECount(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1]) or
         (GetFiltr1MaxLen(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].LenTh[1]))) or

       ((MSMFlag) and False
//        ((Delay < FThParam.ChParam[Ch].BHMinDelay) or                             // ��� ���� ��
//         (Delay > FThParam.ChParam[Ch].BHMaxDelay)) and
{        ((GetFiltr1ECount(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].CountTh[1]) or
         (GetFiltr1MaxLen(FFiltrIdx, Rail, Ch, AmplIdx, DelIdx) / FSummWinLen > FThParam.ChParam[Ch].LenTh[1]))} )
        then Result:= False;
  end;

end;

initialization

   EQ:= 0;


end.
