object InfoBarForm2: TInfoBarForm2
  Left = 309
  Top = 186
  BorderStyle = bsNone
  Caption = 'InfoBarForm2'
  ClientHeight = 49
  ClientWidth = 158
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object FileInfoPanel: TPanel
    Left = 0
    Top = 0
    Width = 156
    Height = 49
    Align = alLeft
    BevelOuter = bvNone
    FullRepaint = False
    TabOrder = 0
    object Bevel3: TBevel
      Left = 105
      Top = 3
      Width = 2
      Height = 33
    end
    object SpeedPanel: TPanel
      Left = 48
      Top = 0
      Width = 58
      Height = 45
      BevelOuter = bvNone
      TabOrder = 0
      object Bevel12: TBevel
        Left = 2
        Top = 16
        Width = 52
        Height = 1
        Shape = bsTopLine
      end
      object Label2: TLabel
        Left = 5
        Top = 19
        Width = 24
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object Label26: TLabel
        Left = 4
        Top = -2
        Width = 58
        Height = 19
        AutoSize = False
        Caption = #1057#1082#1086#1088#1086#1089#1090#1100':'
        Layout = tlCenter
        WordWrap = True
      end
      object Label1: TLabel
        Left = 31
        Top = 19
        Width = 25
        Height = 18
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
      end
      object Bevel2: TBevel
        Left = 1
        Top = 2
        Width = 2
        Height = 37
      end
    end
    object TempPanel: TPanel
      Left = 107
      Top = 2
      Width = 53
      Height = 45
      BevelOuter = bvNone
      TabOrder = 1
      object Bevel6: TBevel
        Left = 50
        Top = 3
        Width = 2
        Height = 33
      end
      object Bevel5: TBevel
        Left = 81
        Top = 14
        Width = 58
        Height = 1
        Shape = bsTopLine
        Visible = False
      end
      object LDhV: TLabel
        Left = 80
        Top = 17
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        Visible = False
      end
      object LDhN: TLabel
        Left = 83
        Top = -3
        Width = 82
        Height = 17
        AutoSize = False
        Caption = 'Depth'
        Layout = tlCenter
        Visible = False
      end
      object Bevel4: TBevel
        Left = 2
        Top = 14
        Width = 45
        Height = 1
        Shape = bsTopLine
      end
      object LDyN: TLabel
        Left = 2
        Top = -3
        Width = 45
        Height = 17
        AutoSize = False
        Caption = #1058#1077#1084#1087'-'#1088#1072
        Layout = tlCenter
      end
      object LDyV: TLabel
        Left = 3
        Top = 17
        Width = 45
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
    end
    object TimePanel: TPanel
      Left = 1
      Top = 0
      Width = 47
      Height = 45
      BevelOuter = bvNone
      TabOrder = 2
      object Bevel11: TBevel
        Left = 0
        Top = 16
        Width = 43
        Height = 1
        Shape = bsTopLine
      end
      object Label40V: TLabel
        Left = -2
        Top = 19
        Width = 47
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object Label43: TLabel
        Left = 3
        Top = -3
        Width = 41
        Height = 22
        AutoSize = False
        Caption = #1042#1088#1077#1084#1103':'
        Layout = tlCenter
        WordWrap = True
      end
    end
  end
end
