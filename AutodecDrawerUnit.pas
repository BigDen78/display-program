unit AutodecDrawerUnit;

interface

uses
  AviconDataSource, DisplayUnit, AutodecTypes, AutodecDataUnit, MyTypes, Types,
  AviconTypes,
  Graphics, Generics.Collections, GraphUtil, RecData, ElementsCollection;

type

  // ��������� ���������
  TDrawOptions = record
    color: TColor;
    doDDColor: Boolean;
    width: Integer;
    yMinDelta: Integer;
    yMaxDelta: Integer;
  end;

const
  // ��������� ��������� �� ���������
  DefaultDrawOptions: TDrawOptions = (
    color: clBlack;
    doDDColor: false;
    width: 1; // ������ ����� (0 - ������ ������)
    yMinDelta: 0;
    yMaxDelta: - 1;
    );

type
  TDrawOptionsDic = TDictionary<TEntityType, TDrawOptions>;

  TRailLineInfo = record
    visible: Boolean;
    YY: TMinMax;
  end;

  TAutodecDrawer = class
  private
    drawOptions: TDrawOptionsDic;

    display: TAvk11Display; // �c���� �� ����� �����������
    datSrc: TAvk11DatSrc; // �c���� �� ����� ������ �������
    autodecData: TAutodecData; // �c���� �� ����� ������ ���������������
    reflectionRectangles: array of TRect;
    // �������������� ��� ��������� �����������

    badCoordOffset: Integer;
    scrXmin, scrXmax: Integer;

    // ������� ����� ��������� ����� (r_Both - ���� ����)
    railInfo: array[TRail] of TRailLineInfo;
  public
    constructor Create(autodecData: TAutodecData);
    destructor Destroy; override;
    // ������������� �����
    procedure init(display: TAvk11Display; datSrc:
      TAvk11DatSrc; badCoordOffset: Integer);
    // ��������� ��������
    procedure drawEntity(entity: PEntity);
    // ��������� �����������
    procedure drawReflections();
    // ��������� ����� ��� �������� ������� � ������
    procedure drawCurrentEntityLine(form: TElementsCollectionForm);
  end;

implementation

procedure swap(var a: Integer; var b: Integer);
var
  temp: Integer;
begin
  temp := a;
  a := b;
  b := temp;
end;

function GetLightedColor(const Color: TColor; const L: Word): TColor;
var
  cH, cL, cS: Word;
  Clr: Cardinal;
begin
  Clr := ColorToRGB(Color);
  ColorRGBToHLS(Clr, cH, cL, cS);
  Result := ColorHLSToRGB(cH, L, cS);
end;

constructor TAutodecDrawer.Create(autodecData: TAutodecData);
var
  opts: TDrawOptions;

begin
  self.autodecData := autodecData;
  drawOptions := TDrawOptionsDic.Create;
  opts := DefaultDrawOptions;
  //  etBoltHole
  opts.color := GetLightedColor(clBlue, 200);
  opts.doDDColor := False;
  opts.width := 1;
  opts.yMinDelta := 1;
  drawOptions.Add(etBoltHole, opts);
  //  etJoint
  opts.color := GetLightedColor(clBlue, 130);
  opts.doDDColor := False;
  opts.width := 1;
  opts.yMinDelta := 1;
  drawOptions.Add(etJoint, opts);
  //  etSwitch
  opts.color := GetLightedColor(clPurple, 80);
  opts.doDDColor := True;
  opts.width := 2;
  opts.yMinDelta := 0;
  drawOptions.Add(etSwitch, opts);
  //  etDefect - default
  opts.color := clBlack;
  opts.doDDColor := False;
  opts.width := 0;
  opts.yMinDelta := 0;
  drawOptions.Add(etDefect, opts);
  //  etReflection
  opts.color := GetLightedColor(clBlue, 210);
  opts.doDDColor := False;
  opts.width := 0;
  opts.yMinDelta := 0;
  drawOptions.Add(etReflection, opts);
end;

destructor TAutodecDrawer.Destroy;
begin
  drawOptions.Free;
end;

procedure TAutodecDrawer.init(display: TAvk11Display; datSrc:
  TAvk11DatSrc; badCoordOffset: Integer);
var
  state: TCrdState;
  opts: TDrawOptions;

  procedure calcScrRailsYBounds();
  var
    YY, YY1: TMinMax;
    rail: TRail;
  begin
    // ������ ������ ��� �����
    for rail in [rLeft, rRight] do
    begin
      railInfo[rail].YY := display.GetRailBounds(rail, state);
      railInfo[rail].visible := (not (csOffScreen in state)) and
        (display.ViewMode <> vmOneLine);
    end;
    // ������ ������ ��� ����� ����
    if display.ViewMode = vmAllLines then
      // ������ - ����: Disp.ViewMode = ddBothRail
    begin
      YY1 := display.GetRailBounds(rLeft, state); // ������ - ����: r_Left
      YY.Min := YY1.Min;
      YY1 := display.GetRailBounds(rRight, state); // ������ - ����: r_Right
      YY.Max := YY1.Max;
    end
    else if display.ViewMode = vmOneRailLines then
    begin
      YY := display.GetRailBounds(display.ViewRail, state)
    end;
    railInfo[rNotSet].YY := YY;
    railInfo[rNotSet].visible := not (csOffScreen in state);
  end;

begin
  self.display := display; // ���������� ���������� �� ����� �����������
  self.datSrc := datSrc; // ���������� ���������� �� ����� ������
  self.badCoordOffset := badCoordOffset;
  // ������ ��������� �������� ���������
  scrXMin := display.GetScrCrd(display.StartDisCoord, state);
  scrXmax := display.GetScrCrd(display.EndDisCoord, state);
  // ������ ������ ����� ��������� �����
  calcScrRailsYBounds();
  // ������� �����������
  SetLength(reflectionRectangles, 0);

  // set dynamic colors
  opts := drawOptions[etDefect];
  opts.color := GetLightedColor(display.DispCols.Defect, 220);
  drawOptions.AddOrSetValue(etDefect, opts);
end;

procedure TAutodecDrawer.drawEntity(entity: PEntity);
var
  entType: TEntityType;
  scrXX: TMinMax;
  state: TCrdState;

  function getOptions(classId: Integer): TDrawOptions;
  begin
    entType := autodecData.classList[classId].entityType;
    if drawOptions.ContainsKey(entType) then
    begin
      Result := drawOptions[entType];
      if Result.doDDColor then
      begin
        Result.color := display.GetDDColor(Result.color);
      end;
    end
    else
    begin
      Result := DefaultDrawOptions;
    end;
  end;

  function badOffset(coord: Integer): Integer;
  begin
    Result := coord - badCoordOffset;
  end;

  function norm(min: Integer; max: Integer): TMinMax;
  begin
    if (min > max) then
    begin
      swap(min, max);
    end;
    Result.Min := min;
    Result.Max := max;
  end;

  function getReducedCoord(channel: Integer; reduction: Integer;
    backMotion: Boolean; coord: Integer; delay: Integer): Integer;
  var
    Coeff: Integer;
  begin
    Coeff := 1 - 2 * Ord(datSrc.isEGOUSW_BHead);
    if reduction = 0 then
    begin
      Result := coord;
    end
    else
    begin
      Result := coord + (1 - 2 * Ord(backMotion)) * Round((Coeff *
        datSrc.Config.ReducePos[reduction, channel] + Coeff *
        datSrc.Config.ReduceDel[reduction, channel] * delay) /
        (datSrc.Header.ScanStep / 100));
    end;
    Result := badOffset(Result);
  end;

  function getReducedCoords(channel: Integer; reduction: Integer;
    backMotion: Boolean; coords: TMinMax; delays: TMinMax): TMinMax;
  begin
    Result.Min := getReducedCoord(channel, reduction, backMotion, coords.Min,
      delays.Min);
    Result.Max := getReducedCoord(channel, reduction, backMotion, coords.Max,
      delays.Max);
    Result := norm(Result.Min, Result.Max);
  end;

  function getReducedBounds(out coords: TMinMax; out tapes: TMinMax;
    out channels: TMinMax; out delays: TMinMax): Boolean;
  type
    TTapeInfo = record
      id: Integer;
      delays: TMinMax;
      channels: TMinMax;
    end;
  var
    i, j, tapeId: Integer;
    tapeIndexes: TMinMax;
    tapeArray: array of TTapeInfo;
    found: Boolean;
    areaCoords: TMinMax;

    function getTapeByChannel(channel: Integer): Integer;
    begin
      Result := ChList[channel].OutLine;
    end;

  begin

    coords.Min := High(Integer);
    coords.Max := Low(Integer);
    SetLength(tapeArray, 0);
    for i := Low(entity.areas) to High(entity.areas) do
    begin
      if datSrc.SkipBackMotion then
      begin
        areaCoords.Min := entity.areas[i].startSysCoordinate;
        areaCoords.Max := entity.areas[i].finishSysCoordinate;
      end
      else
      begin
        areaCoords.Min := entity.areas[i].startDisCoordinate;
        areaCoords.Max := entity.areas[i].finishDisCoordinate;
      end;
      areaCoords := getReducedCoords(entity.areas[i].channelId,
        display.Reduction, entity.inBackwardZone, areaCoords,
        entity.areas[i].startFinishDelays);

      if areaCoords.Min < coords.Min then
        coords.Min := areaCoords.Min;
      if areaCoords.Max > coords.Max then
        coords.Max := areaCoords.Max;
      tapeId := getTapeByChannel(entity.areas[i].channelId);
      found := False;
      for j := Low(tapeArray) to High(tapeArray) do
      begin
        if tapeArray[j].id = tapeId then
        begin
          found := true;
          break;
        end;
      end;
      if not found then
      begin
        j := Length(tapeArray);
        SetLength(tapeArray, j + 1);
        tapeArray[j].id := tapeId;
        tapeArray[j].delays := entity.areas[i].minMaxDelays;
        tapeArray[j].channels.Min := entity.areas[i].channelId;
        tapeArray[j].channels.Max := entity.areas[i].channelId;
      end
      else
      begin
        if (entity.areas[i].minMaxDelays.Min < tapeArray[j].delays.Min) then
        begin
          tapeArray[j].delays.Min := entity.areas[i].minMaxDelays.Min;
          tapeArray[j].channels.Min := entity.areas[i].channelId;
        end;
        if (entity.areas[i].minMaxDelays.Max > tapeArray[j].delays.Max) then
        begin
          tapeArray[j].delays.Max := entity.areas[i].minMaxDelays.Max;
          tapeArray[j].channels.Max := entity.areas[i].channelId;
        end;
      end;

    end;
    if Length(tapeArray) = 0 then
    begin
      Result := False;
      exit;
    end;
    tapeIndexes.Min := 0;
    tapeIndexes.Max := 0;
    for i := Low(tapeArray) to High(tapeArray) do
    begin
      if (tapeArray[i].id < tapeArray[tapeIndexes.Min].id) then
        tapeIndexes.Min := i;
      if (tapeArray[i].id > tapeArray[tapeIndexes.Max].id) then
        tapeIndexes.Max := i;
    end;
    Result := true;
    tapes.Min := tapeArray[tapeIndexes.Min].id;
    tapes.Max := tapeArray[tapeIndexes.Max].id;
    delays.Min := tapeArray[tapeIndexes.Min].delays.Min;
    delays.Max := tapeArray[tapeIndexes.Max].delays.Max;
    channels.Min := tapeArray[tapeIndexes.Min].channels.Min;
    channels.Max := tapeArray[tapeIndexes.Max].channels.Max;
  end;

  procedure fillReflections;
  var
    i: Integer;
    XX: TMinMax;
    YY: TMinMax;
    len: Integer;
  begin
    for i := Low(entity.areas) to High(entity.areas) do
    begin
      with entity.areas[i] do
      begin
        if datSrc.SkipBackMotion then
        begin
          XX.Min := startSysCoordinate;
          XX.Max := finishSysCoordinate;
        end
        else
        begin
          XX.Min := startDisCoordinate;
          XX.Max := finishDisCoordinate;
        end;
        XX := getReducedCoords(channelId, display.Reduction,
          entity.inBackwardZone, XX, startFinishDelays);
        XX := display.GetScrReg(Range(XX.Min, XX.Max), state);
        if not (csOffScreen in state) then
        begin
          YY.Min := display.GetY(TRail(entity.rail), channelId,
            minMaxDelays.Min, state);
          YY.Max := display.GetY(TRail(entity.rail), channelId,
            minMaxDelays.Max, state);
          len := Length(reflectionRectangles);
          SetLength(reflectionRectangles, len + 1);
          reflectionRectangles[len] := Rect(XX.Min, YY.Min, XX.Max, YY.Max + 1);
        end;
      end;
    end;
  end;

  function calcScrXX(): Boolean;
  var
    sysXX: TMinMax;
  begin
    if datSrc.SkipBackMotion then
    begin
      sysXX := norm(badOffset(entity.areas[0].startSysCoordinate),
        badOffset(entity.areas[0].finishSysCoordinate));
      scrXX := display.GetScrReg(Range(datSrc.SysToDisCoord(sysXX.Min),
        datSrc.SysToDisCoord(sysXX.Max)), state);
    end
    else
    begin
      sysXX := norm(badOffset(entity.areas[0].startDisCoordinate),
        badOffset(entity.areas[0].finishDisCoordinate));
      scrXX := display.GetScrReg(Range(sysXX.Min, sysXX.Max), state);
    end;
    Result := not (csOffScreen in state);
  end;

  procedure drawRect(rect: TRect; opts: TDrawOptions);
  begin
    if opts.width = 0 then
      display.FillRect(rect.Left, rect.Bottom, rect.Right, rect.Top, opts.color)
    else
      display.DrawRect(rect, opts.width, opts.color);
  end;

  procedure drawInChannel();
  var
    XX: TMinMax;
    YY: TMinMax;
  begin
    if not railInfo[TRail(entity.rail)].visible then
    begin
      Exit;
    end;
    if datSrc.SkipBackMotion then
    begin
      XX.Min := entity.areas[0].startSysCoordinate;
      XX.Max := entity.areas[0].finishSysCoordinate;
    end
    else
    begin
      XX.Min := entity.areas[0].startDisCoordinate;
      XX.Max := entity.areas[0].finishDisCoordinate;
    end;
    XX := getReducedCoords(entity.areas[0].channelId, display.Reduction,
      entity.inBackwardZone, XX, entity.areas[0].startFinishDelays);
    scrXX := display.GetScrReg(Range(XX.Min, XX.Max), state);
    if csOffScreen in state then
    begin
      Exit;
    end;
    YY.Min := display.GetY(TRail(entity.rail), entity.areas[0].channelId,
      entity.areas[0].minMaxDelays.Min, state);
    YY.Max := display.GetY(TRail(entity.rail), entity.areas[0].channelId,
      entity.areas[0].minMaxDelays.Max, state) + 1;
    drawRect(Rect(scrXX.Min, YY.Min, scrXX.Max, YY.Max),
      getOptions(entity.classId));

  end;

  procedure drawForChannels();
  var
    XX: TMinMax;
    YY: TMinMax;
    tapes: TMinMax;
    channels: TMinMax;
    delays: TMinMax;
    isVisible: Boolean;
    length: Integer;
    i: Integer;
  const
    MinLength = 30;

    procedure fit(var scrXX: TMinMax);
    begin
      if scrXX.Min < scrXmin then
        scrXX.Min := scrXmin;
      if scrXX.Max > scrXmax then
        scrXX.Max := scrXmax;
    end;

  begin
    if not railInfo[TRail(entity.rail)].visible then
    begin
      Exit;
    end;
    if datSrc.SkipBackMotion then
    begin
      XX.Min := entity.startSysCoordinate;
      XX.Max := entity.finishSysCoordinate;
    end
    else
    begin
      XX.Min := entity.startDisCoordinate;
      XX.Max := entity.finishDisCoordinate;
    end;
    XX := norm(XX.Min, XX.Max);
    isVisible := (XX.Max + 200 >= display.StartDisCoord) and (xx.Min -
      200 <= display.EndDisCoord);
    if isVisible and (getReducedBounds(XX, tapes, channels, delays)) then
    begin
      scrXX := display.GetScrReg(Range(XX.Min, XX.Max), state);
      if csOffScreen in state then
        Exit;
      scrXX := norm(scrXX.Min, scrXX.Max);
      length := scrXX.Max - scrXX.Min;
      if (length < MinLength) then
      begin
        scrXX.Min := scrXX.Min - Round((MinLength - length) / 2);
        scrXX.Max := scrXX.Max + Round((MinLength - length) / 2);
      end;
      fit(scrXX);
      for i := tapes.Min to tapes.Max do
      begin
        YY := display.GetTapeBounds(TRail(entity.rail), i, state);
        drawRect(Rect(scrXX.Min, YY.Min, scrXX.Max, YY.Max),
          getOptions(entity.classId));
      end;

    end;

  end;

  procedure drawForRailOrTrack(rail: TRail);
  var
    opts: TDrawOptions;
  begin
    if not railInfo[rail].visible then
    begin
      Exit;
    end;
    if calcScrXX() then
    begin
      opts := getOptions(entity.classId);
      drawRect(Rect(scrXX.Min, railInfo[rail].YY.Min + opts.yMinDelta,
        scrXX.Max,
        railInfo[rail].YY.Max + opts.yMaxDelta), opts);
    end;
  end;

begin
  // ���� ������� ����� ����������� ��� ��� �������� ����� � �������� ��������� � ���������� ����
  if (datSrc.SkipBackMotion and entity.inHiddenZone) then
  begin
    Exit;
  end;
  if (display.ViewChannel <> vcAll) and (entity.viewChannel <> vcAll) and
    (entity.viewChannel <> display.ViewChannel) then
  begin
    Exit;
  end;

  // ���� �������� �������� �������� - ���������� �������� ����������
  if autodecData.classList[entity.classId].isDefect then
  begin
    fillReflections();
  end;

  if (entity.defLevel = dlDelayRange) then
  begin
    drawInChannel();
  end;
  if (entity.defLevel = dlChannel) or (entity.defLevel =
    dlChannelsDelayRange) or (entity.defLevel = dlChannels) then
  begin
    drawForChannels();
  end;
  if (entity.defLevel = dlRail) then
  begin
    drawForRailOrTrack(TRail(entity.rail));
  end;
  if (entity.defLevel = dlTrack) then
  begin
    drawForRailOrTrack(rNotSet);
  end;
end;

procedure TAutodecDrawer.drawReflections;
var
  i: Integer;
begin
  for i := Low(reflectionRectangles) to High(reflectionRectangles) do
  begin
    display.DrawRect(reflectionRectangles[i], 1,
      display.GetDDColor(clGray));
  end;
end;

procedure TAutodecDrawer.drawCurrentEntityLine(form: TElementsCollectionForm);
var
  rail: RRail;
  XX, YY: TMinMax;
  x: Integer;
  state: TCrdState;
begin
  for rail := r_Left to r_Right do
  begin
    x := form.verticalLine(rail);
    if x >= 0 then
    begin
      // ��������� ������������ �����
      XX := display.GetScrReg(Range(x, x), state);
      YY := display.GetRailBounds(TRail(rail), state);
      display.VDashLine(XX.Min, YY.Min, YY.Max - YY.Min, 2,
        display.GetDDColor(TColor($F000F0)));
    end;
  end;

end;

end.

