object EVideoForm: TEVideoForm
  Left = 0
  Top = 0
  Caption = 'PasLibVlcPlayerDemo'
  ClientHeight = 621
  ClientWidth = 750
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 250
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  PopupMenu = PopupMenu
  Position = poScreenCenter
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PR: TPanel
    Left = 568
    Top = 0
    Width = 182
    Height = 532
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 182
      Height = 199
      Align = alTop
      TabOrder = 0
      object Button2: TButton
        Left = 90
        Top = 102
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 0
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 90
        Top = 133
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 1
        OnClick = Button3Click
      end
      object Button1: TButton
        Left = 90
        Top = 71
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 2
        OnClick = Button1Click
      end
      object NextFrameBtn: TButton
        Left = 90
        Top = 40
        Width = 75
        Height = 25
        Caption = 'Next Frame'
        TabOrder = 3
        OnClick = NextFrameClick
      end
      object SnapShotBtn: TButton
        Left = 90
        Top = 9
        Width = 75
        Height = 25
        Caption = 'SnapShot'
        TabOrder = 4
        OnClick = SnapShotClick
      end
      object PlayBtn: TButton
        Left = 8
        Top = 10
        Width = 75
        Height = 25
        Caption = 'Play'
        TabOrder = 5
      end
      object PauseBtn: TButton
        Left = 8
        Top = 41
        Width = 75
        Height = 25
        Caption = 'Pause'
        TabOrder = 6
        OnClick = PauseClick
      end
      object ResumeBtn: TButton
        Left = 8
        Top = 70
        Width = 75
        Height = 25
        Caption = 'Resume'
        TabOrder = 7
        OnClick = ResumeClick
      end
      object GetStateBtn: TButton
        Left = 8
        Top = 101
        Width = 75
        Height = 25
        Caption = 'Get state'
        TabOrder = 8
        OnClick = GetStateClick
      end
      object GetPosLenBtn: TButton
        Left = 8
        Top = 128
        Width = 75
        Height = 25
        Caption = 'Get pos, len'
        TabOrder = 9
        OnClick = GetPosLenClick
      end
      object VideoAdjustBtn: TButton
        Left = 9
        Top = 159
        Width = 75
        Height = 25
        Caption = 'Video Adjust'
        TabOrder = 10
        OnClick = VideoAdjustBtnClick
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 199
      Width = 182
      Height = 333
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object LogMemo: TMemo
        Left = 0
        Top = 0
        Width = 182
        Height = 333
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 568
    Height = 532
    Align = alClient
    BevelOuter = bvNone
    Caption = #1054#1078#1080#1076#1072#1085#1080#1077'...'
    TabOrder = 1
    object LeftLabel: TLabel
      Left = 423
      Top = 26
      Width = 44
      Height = 13
      Caption = 'LeftLabel'
      Visible = False
    end
    object RightLabel: TLabel
      Left = 423
      Top = 309
      Width = 50
      Height = 13
      Caption = 'RightLabel'
      Visible = False
    end
    object LeftPlayer: TPasLibVlcPlayer
      Left = 0
      Top = 0
      Width = 568
      Height = 532
      Align = alClient
      Visible = False
      OnMouseDown = LeftPlayerMouseDown
      AudioOutput = aoDummy
      SnapShotFmt = 'png'
      OnMediaPlayerTimeChanged = LeftPlayerMediaPlayerTimeChanged
    end
    object RightPlayer: TPasLibVlcPlayer
      Left = 0
      Top = 0
      Width = 568
      Height = 532
      Align = alClient
      Visible = False
      OnMouseDown = RightPlayerMouseDown
      AudioOutput = aoDummy
      SnapShotFmt = 'png'
      OnMediaPlayerTimeChanged = RightPlayerMediaPlayerTimeChanged
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 532
    Width = 750
    Height = 89
    Align = alBottom
    ScrollBars = ssVertical
    TabOrder = 2
    Visible = False
    ExplicitTop = 538
  end
  object PopupMenu: TPopupMenu
    Left = 416
    Top = 160
    object a1: TMenuItem
      Caption = 'a'
    end
    object b1: TMenuItem
      Caption = 'b'
    end
    object c1: TMenuItem
      Caption = 'c'
    end
    object d1: TMenuItem
      Caption = 'd'
    end
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '*.txt'
    FileName = 'PasLibVlcDemo.txt'
    Left = 492
    Top = 396
  end
  object LbPopupMenu: TPopupMenu
    Left = 488
    Top = 324
    object LbPmClear: TMenuItem
      Caption = 'Clear'
    end
    object LnPmSaveAs: TMenuItem
      Caption = 'Save as'
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 376
    Top = 296
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer2Timer
    Left = 424
    Top = 272
  end
end
