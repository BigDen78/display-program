unit AudioViewWin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, ComCtrls, StdCtrls, Buttons, MMsystem, MPlayer, pngimage;


type
  TAudioView = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Panel4: TPanel;
    SpeedButton1: TSpeedButton;
    Timer1: TTimer;
    MediaPlayer1: TMediaPlayer;
    Image1: TImage;
    SpeedButton2: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);

  private
    FOnCloseMediaForm: TNotifyEvent;
    FFileName: string;
//    FWavLength: LongInt;

  public
    procedure Open_and_Play(FN: string; OnCloseMediaForm: TNotifyEvent);
  end;

var
  AudioView: TAudioView;

implementation

{$R *.dfm}

procedure TAudioView.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  sndPlaySound(nil, 0);
  MediaPlayer1.Stop;
  MediaPlayer1.Close;
  if (self.Tag = 0) and Assigned(FOnCloseMediaForm) then FOnCloseMediaForm(self);
  Self.Release;
end;

procedure TAudioView.FormCreate(Sender: TObject);
begin
  FOnCloseMediaForm:= nil;
end;

procedure TAudioView.Open_and_Play(FN: string; OnCloseMediaForm: TNotifyEvent);
begin
  FOnCloseMediaForm:= OnCloseMediaForm;

  MediaPlayer1.FileName:= FN;
  MediaPlayer1.Open;
  MediaPlayer1.TimeFormat:= tfMilliseconds;
//  FWavLength:= MediaPlayer1.Length;
  MediaPlayer1.Play;
//  MediaPlayer1.Close;

  FFileName:= FN;
  Timer1Timer(nil);
  SpeedButton1Click(nil);
end;

procedure TAudioView.SpeedButton1Click(Sender: TObject);
var
  tmp: PWideChar;

begin
  MediaPlayer1.Play;
{
  tmp:= PWideChar(WideString(FFileName));
  sndPlaySound(tmp, SND_FILENAME or SND_ASYNC);
  Timer1.Tag:= 0; }
  Timer1.Enabled:= True;


//  ShowMessage(IntToStr(MediaPlayer1.Error));
//  ShowMessage(MediaPlayer1.ErrorMessage);
end;

procedure TAudioView.SpeedButton2Click(Sender: TObject);
begin
  MediaPlayer1.Pause;
//  sndPlaySound(nil, 0);
end;

procedure TAudioView.Timer1Timer(Sender: TObject);
var
  ValPos: Double;
  ValLen: Double;

begin
{
  if (Timer1.Tag + 1) * 0.25 >= FWavLength / 1000 then
  begin
    Timer1.Enabled:= False;
    Exit;
  end else Timer1.Tag:= Timer1.Tag + 1;
 }
  //��������� ������� � ����
  ValPos:= MediaPlayer1.Position / 1000 / (24 * 3600);
  ValLen:= MediaPlayer1.Length / 1000 / (24 * 3600);
  //������� ������ � ������� �� ����� � Label1 � Label2
  Label2.Caption:=FormatDateTime('hh:mm:ss',ValPos);
  Label3.Caption:=FormatDateTime('hh:mm:ss',ValLen);

end;

end.
