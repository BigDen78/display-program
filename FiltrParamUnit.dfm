object FiltrParamForm: TFiltrParamForm
  Left = 412
  Top = 167
  BorderStyle = bsToolWindow
  Caption = #1055#1072#1088#1084#1077#1090#1088#1099' '#1092#1080#1083#1100#1090#1088#1072#1094#1080#1080
  ClientHeight = 506
  ClientWidth = 785
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object bExpand: TSpeedButton
    Left = 338
    Top = 476
    Width = 23
    Height = 24
    Caption = '>>'
    OnClick = bExpandClick
  end
  object lError: TLabel
    Left = 104
    Top = 480
    Width = 150
    Height = 13
    Caption = #1054#1096#1080#1073#1082#1072' '#1087#1088#1080' '#1074#1074#1086#1076#1077' '#1079#1085#1072#1095#1077#1085#1080#1081' !'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object ParamGrid: TStringGrid
    Left = 5
    Top = 319
    Width = 356
    Height = 120
    ColCount = 8
    Ctl3D = False
    DefaultColWidth = 26
    DefaultRowHeight = 16
    RowCount = 7
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    ParentCtl3D = False
    TabOrder = 0
    OnDrawCell = ParamGridDrawCell
    OnSetEditText = ParamGridSetEditText
  end
  object Button1: TButton
    Left = 6
    Top = 475
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    OnClick = Button1Click
  end
  object eChLen: TLabeledEdit
    Left = 165
    Top = 451
    Width = 121
    Height = 14
    BorderStyle = bsNone
    EditLabel.Width = 143
    EditLabel.Height = 26
    EditLabel.Caption = #1050#1072#1085#1072#1083' 1, '#1048#1085#1090#1077#1088#1074#1072#1083' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103' '#1074' '#1086#1090#1089#1095#1077#1090#1072#1093' '#1044#1055
    EditLabel.WordWrap = True
    LabelPosition = lpLeft
    LabelSpacing = 8
    TabOrder = 2
    OnChange = ParamChange
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 77
    Width = 357
    Height = 44
    Caption = ' '#1050#1086#1086#1088#1076#1080#1085#1072#1090#1072' '
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 3
    object eWindowsWidth: TLabeledEdit
      Left = 211
      Top = 9
      Width = 121
      Height = 14
      BorderStyle = bsNone
      EditLabel.Width = 91
      EditLabel.Height = 13
      EditLabel.Caption = #1056#1072#1079#1084#1077#1088' '#1086#1082#1085#1072' ('#1084#1084')'
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 0
      OnChange = ParamChange
    end
    object eCrdWinCount: TLabeledEdit
      Left = 211
      Top = 25
      Width = 121
      Height = 14
      BorderStyle = bsNone
      EditLabel.Width = 187
      EditLabel.Height = 13
      EditLabel.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1086#1073#1098#1077#1076#1080#1085#1103#1077#1084#1099#1093' '#1086#1082#1086#1085' ('#1096#1090')'
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 1
      OnChange = ParamChange
    end
  end
  object GroupBox2: TGroupBox
    Left = 4
    Top = 123
    Width = 357
    Height = 75
    Caption = ' '#1040#1084#1087#1083#1080#1090#1091#1076#1072' '
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 4
    object eAmplStep: TLabeledEdit
      Left = 212
      Top = 41
      Width = 121
      Height = 14
      BorderStyle = bsNone
      EditLabel.Width = 95
      EditLabel.Height = 13
      EditLabel.Caption = #1056#1072#1079#1084#1077#1088' '#1086#1082#1085#1072'  ('#1091'.'#1077'.)'
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 0
      OnChange = ParamChange
    end
    object eMinAmpl: TLabeledEdit
      Left = 212
      Top = 9
      Width = 121
      Height = 14
      BorderStyle = bsNone
      EditLabel.Width = 97
      EditLabel.Height = 13
      EditLabel.Caption = #1052#1080#1085#1080#1084#1072#1083#1100#1085#1072#1103' ('#1091'.'#1077'.)'
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 1
      OnChange = ParamChange
    end
    object eMaxAmpl: TLabeledEdit
      Left = 212
      Top = 25
      Width = 121
      Height = 14
      BorderStyle = bsNone
      EditLabel.Width = 103
      EditLabel.Height = 13
      EditLabel.Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1072#1103' ('#1091'.'#1077'.)'
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 2
      OnChange = ParamChange
    end
    object eAmplWinCount: TLabeledEdit
      Left = 212
      Top = 57
      Width = 121
      Height = 14
      BorderStyle = bsNone
      EditLabel.Width = 190
      EditLabel.Height = 13
      EditLabel.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1086#1073#1098#1077#1076#1080#1085#1103#1077#1084#1099#1093' '#1086#1082#1086#1085' ('#1096#1090'.)'
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 3
      OnChange = ParamChange
    end
  end
  object GroupBox3: TGroupBox
    Left = 4
    Top = 200
    Width = 357
    Height = 115
    Caption = ' '#1047#1072#1076#1077#1088#1078#1082#1072' '
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 5
    object DelayGrid: TStringGrid
      Left = 6
      Top = 15
      Width = 342
      Height = 77
      ColCount = 8
      DefaultColWidth = 24
      DefaultRowHeight = 18
      RowCount = 4
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
      TabOrder = 0
      OnSetEditText = ParamGridSetEditText
    end
    object eDelayWinCount: TLabeledEdit
      Left = 222
      Top = 96
      Width = 121
      Height = 13
      BorderStyle = bsNone
      EditLabel.Width = 165
      EditLabel.Height = 13
      EditLabel.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1086#1073#1098#1077#1076#1080#1085#1103#1077#1084#1099#1093' '#1086#1082#1086#1085
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 1
      OnChange = ParamChange
    end
  end
  object GroupBox5: TGroupBox
    Left = 4
    Top = 4
    Width = 357
    Height = 72
    Caption = ' '#1053#1072#1089#1090#1088#1086#1081#1082#1072' '
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 6
    object ParamList: TComboBox
      Left = 8
      Top = 15
      Width = 341
      Height = 21
      AutoComplete = False
      TabOrder = 0
      OnChange = ParamChange
      OnClick = ParamListClick
    end
    object Button2: TButton
      Left = 8
      Top = 40
      Width = 105
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082'...'
      TabOrder = 1
      OnClick = Button2Click
    end
    object ReCalc: TButton
      Left = 227
      Top = 40
      Width = 121
      Height = 25
      Caption = #1055#1077#1088#1077#1072#1085#1072#1083#1080#1079#1080#1088#1086#1074#1072#1090#1100
      Enabled = False
      TabOrder = 2
      OnClick = ReCalcClick
    end
  end
  object Chart1: TChart
    Left = 368
    Top = 96
    Width = 409
    Height = 409
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    BottomWall.Brush.Color = clWhite
    Gradient.Direction = gdBottomTop
    Legend.Visible = False
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    View3D = False
    TabOrder = 7
  end
  object Panel1: TPanel
    Left = 368
    Top = 8
    Width = 409
    Height = 81
    TabOrder = 8
    object eRail: TLabeledEdit
      Left = 77
      Top = 22
      Width = 121
      Height = 14
      BorderStyle = bsNone
      EditLabel.Width = 25
      EditLabel.Height = 13
      EditLabel.Caption = #1053#1080#1090#1100
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 0
      Text = '0'
      OnChange = ParamChange
    end
    object eChannel: TLabeledEdit
      Left = 77
      Top = 38
      Width = 121
      Height = 14
      BorderStyle = bsNone
      EditLabel.Width = 31
      EditLabel.Height = 13
      EditLabel.Caption = #1050#1072#1085#1072#1083
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 1
      Text = '4'
      OnChange = ParamChange
    end
    object eCoord: TLabeledEdit
      Left = 77
      Top = 6
      Width = 121
      Height = 14
      BorderStyle = bsNone
      EditLabel.Width = 60
      EditLabel.Height = 13
      EditLabel.Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1072
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 2
      Text = '9216'
      OnChange = ParamChange
    end
    object bCalcGraph: TButton
      Left = 206
      Top = 24
      Width = 75
      Height = 25
      Caption = #1043#1088#1072#1092#1080#1082
      TabOrder = 3
      OnClick = bCalcGraphClick
    end
    object eAmpl: TLabeledEdit
      Left = 77
      Top = 54
      Width = 121
      Height = 14
      BorderStyle = bsNone
      EditLabel.Width = 55
      EditLabel.Height = 13
      EditLabel.Caption = #1040#1084#1087#1083#1080#1090#1091#1076#1072
      LabelPosition = lpLeft
      LabelSpacing = 8
      TabOrder = 4
      Text = '4'
      OnChange = ParamChange
    end
  end
end
