unit PublicFunc;

{ $DEFINE UseExBlocks}
{ ������������ ����������� �������, �� ���� ������� � ������ ����� 10 ����. `
  � ����� �������� ���� ������ ���� (� �� ������ ������� �������) ��������� `
  ����� �������.                                                            `
}


interface

uses Windows, Classes, SysUtils, Types,  Forms, Math, IniFiles, Messages, Dialogs, Graphics, MMSystem, SyncObjs;

const
  Copyright_ru = '��� "�������������", 2007';
  MadeIn_ru = '������, �����-���������';
  Copyright_eng = 'Radioavionika inc., 2007';
  MadeIn_eng = 'Russia, Saint-Petersburg';
  ProgrammedBy = 'Mike Keskinov';

type
  TBlockBase = array[1..10] of Byte;           // �������
  TBlockEx = array[1..500] of Byte;            // ����������� �������.

  {$IFDEF UseExBlocks}
  TBlock = TBlockEx;
  const UseExBlocks = True;
  {$ELSE}
  const UseExBlocks = False;
  type TBlock = TBlockBase;
  {$ENDIF}

  type TBl = TBlock;


  TBVBuffer = array[0..500] of Byte; // ���-�� ���� ������� (1�), �����-���� (1�: ��.���==����), ��������� ����� ������� (���. + ����., ����.)


  TDecodedEchos =
  record
    Count: Byte;
    Echo: array[0..7] of
    record
      T: Byte;
      A: Byte;
    end;
  end;

  TFastPainting = class
  private
    DrawTo: TBitMap;
    FScanLines: array of Pointer;
  public
    constructor Create( DrawTo: TBitMap );
    procedure FastPutPixel(X, Y: Integer);
    procedure FastFillRect( Rect: TRect );
  end;


  TBScanDecodedProc = procedure ( Rail, Takt: Byte; Echos: TDecodedEchos; AdditionalData: Integer ) of object;

  TAddLogProc = procedure ( Msg: string ) of object;

  TSoundVolume =
  record
    Left: Byte;
    Right: Byte;
  end;
  {
  TPoint3D = record
    X: Integer;
    Y: Integer;
    Z: Integer;
  end;
  }

  TMyConfig = class
  private
  protected
    procedure LoadList( Section, ItemID: string; List: TStringList );
    procedure SaveList( Section, ItemID: string; List: TStringList );
  public
    IniF: TIniFile;
    constructor Create( IniFileName: string );
    procedure Load; virtual; abstract;
    procedure Save; virtual; abstract;
    destructor Destroy; override;
  end;


// ������ � ������.
function CD( Ph: string = ''; CreateNew: Boolean = False ): Boolean;
function GetFullPath( RelativeOrFullPath: string = ''; BaseFolder: string = '' ): string;
function GetRelativePath( RelativeOrFullPath, BaseFolder: string ): string;
function AddSlash( Path: string ): string;
function ApplicationRoot: string;

// ������ ��.
procedure GetProgramVersion( out Major, Minor, Release, BuildNum: Integer );
function ProgramVersion: string;

// ������.
function MySelectDirectory(const Caption: string; const Root: WideString; var Directory: string): Boolean;
function TryHexToInt(const S: string; out Value: Integer): Boolean;
function BlockAsString(Block: TBlock): string;
function EncodeBlock( B0: Byte; B1: Byte = 0; B2: Byte = 0; B3: Byte = 0; B4: Byte = 0; B5: Byte = 0; B6: Byte = 0; B7: Byte = 0; B8: Byte = 0; B9: Byte = 0 ): TBlock;
function CompareBlocks( Block1, Block2: TBlock ): Boolean;
function TestBlock( Block: TBlock ): Boolean;
function TestProgramAlreadyStart( ProgramID: string ): Boolean;
function GetRBC( X: Single ): TColor; // RainBow.

// ������ � �������.
function TextWordWrap( Canvas: TCanvas; MaxWidth: Integer; Text: string ): TStringList;
function GetWords( Text: string ): TStringList;

// ������ �-���������.
function DecodeBScan( BV: TBVBuffer; ByteCount: Integer; OnDecode: TBScanDecodedProc; AdditionalData: Integer ): Boolean;
   // ���������� BV - ����� �-��������� �� ��� ��� ������� ����� ($70 ��� $71).
function DecodeAmplify( AmplifyFromBScan: Integer ): Integer;
function EncodeAmplify( AmplifyDB: Integer ): Integer;

//EGO-USW: �������������� ������ ���� � ����
function BUMtoRail(BUM:byte):byte;
//EGO-USW: �������������� ���� � ����� ���� � �������� �������� ������� � �������������
function RailToBUM(Rail:byte; Direction:boolean):byte;

// ����.
function GetSoundVolume: TSoundVolume;
procedure SetSoundVolume( V: TSoundVolume );

// ������ � ������� ������.
function GetFlashDrive( Mode: Integer; out FlashLetter: Char ): Boolean; // �������� Mode � ���� �������.
function GetFlashDriveNew( Mode: Integer; out FlashLetter: Char ): Boolean; // ����� ������ ���������, ����� ������ ������������ ������� �����
function DriveFixed( DriveLetter: Char ): Boolean;
function DriveExists( DriveLetter: Char ): Boolean;

procedure MyProcessMessages( Interval: Cardinal );
function ManufactureNumberAsString( Number: Integer ): string;


implementation
uses ActiveX, ShlObj, StdCtrls;

function ManufactureNumberAsString( Number: Integer ): string;
begin
  Result:= IntToStr( Number );
  while Length( Result ) < 5 do Result:= '0' + Result;
end;

procedure MyProcessMessages( Interval: Cardinal );
var
  T: Cardinal;

begin
  T:= GetTickCount;
  while GetTickCount - T < Interval do
    Application.ProcessMessages;
end;

function GetWords( Text: string ): TStringList;
var
  P: Integer;

begin
  Result:= TStringList.Create;
  repeat
    P:= Pos( ' ', Text );
    if P = 0 then
    begin
      Result.Add( Text );
      Exit;
    end else
    begin
      Result.Add( Copy( Text, 1, P - 1 ) );
      Text:= Copy( Text, P + 1, $FFFF );
    end;
  until False;
end;


function TextWordWrap( Canvas: TCanvas; MaxWidth: Integer; Text: string ): TStringList;
var
  Words: TStringList;
  i: Integer;
  S, S1: string;

begin
  Result:= TStringList.Create;
  Words:= GetWords( Text );
  for i:= 0 to Words.Count - 1 do
  begin
    if S <> '' then S1:= S + ' ' + Words[i] else S1:= Words[i];
    if Canvas.TextWidth( S1 ) > MaxWidth then
    begin
      Result.Add( S );
      S:= Words[i];
    end else
      S:= S1;
  end;
  if S <> '' then Result.Add( S );
  Words.Free;
end;

function GetRBC( X: Single ): TColor;
var
  R, G, B: Single;
  R0, G0, B0: Byte;
  V: Single;

  function GetV( X, Min, Size: Single ): Single;
  begin
    Result:= ( X - Min ) / Size;
  end;

begin
  X:= Max( 0, Min( 1, X ) );
  if ( X >= 0 ) and ( X < 1/4 ) then
  begin
    R:= 1;
    G:= GetV( X, 0, 1/4 );
    B:= 0;
  end else
  if ( X >= 1/4 ) and ( X < 2/4 ) then
  begin
    R:= 1 - GetV( X, 1/4, 1/4 );
    G:= 1;
    B:= 0;
  end else
  if ( X >= 2/4 ) and ( X < 3/4 ) then
  begin
    R:= 0;
    G:= 1;
    B:= GetV( X, 2/4, 1/4 );
  end else
  if ( X >= 3/4 ) and ( X <= 4/4 ) then
  begin
    R:= 0;
    G:= 1 - GetV( X, 3/4, 1/4 );
    B:= 1;
  end;{ else
  if ( X >= 4/5 ) and ( X < 5/5 ) then
  begin
    R:= GetV( X, 4/5, 1/5 );
    G:= 0;
    B:= 1;
  end; }

  R0:= Round( Max( 0, Min( 255, R * 255 ) ) );
  G0:= Round( Max( 0, Min( 255, G * 255 ) ) );
  B0:= Round( Max( 0, Min( 255, B * 255 ) ) );
  Result:= RGB( R0, G0, B0 );
end;


function TestProgramAlreadyStart( ProgramID: string ): Boolean;
var
  Wnd : hWnd;
  clss,buff,mainS: array [0..255] of Char;
  hMutex : integer;
begin
  //********************  �������������� ���������� ������� ********************
  // ������ "NameProgramm" ������� ���� ������
  StrCopy( mainS, PChar(ProgramID) ); SetWindowText(Application.Handle,mainS);
  hMutex:=CreateMutex(nil,TRUE, PChar( ProgramID + '_semafor' ) );       // ������� �������
  if GetLastError <> 0 then                             // ������ �������� - ������ ��� ������
  begin
    GetClassName(Application.Handle, clss,sizeof(clss)); // �������� ��� ������
    Wnd := GetWindow(Application.Handle, gw_HWndFirst);  // �������� ����� �� ����
    while Wnd <> 0 do
    begin
      if (Wnd<>Application.Handle)and(GetWindow(Wnd, gw_Owner)=0) then // ����� �� ���� �����
      begin
        GetClassName(Wnd,buff, sizeof(buff));    // �������� ����� ����
        if StrComp(buff,clss)=0 then             // ���������� � ����� �������
        begin
          GetWindowText(Wnd, buff, sizeof(buff)); // �������� ����� ���������� ����
          if StrComp(buff,mainS)=0 then           // ���������� � ����� ����
          begin
            SendMessage( Wnd, WM_SETFOCUS, Wnd, 0 );
            ShowWindow( Wnd, SW_MINIMIZE );
            ShowWindow( Wnd, SW_SHOWNORMAL );
          end;
        end;
      end;
    Wnd := GetWindow(Wnd, gw_hWndNext); // ����� ���������
  end;
  Result:= True; // ����� ��� ��������.
  Exit;
 end;
 Result:= False; // ����� ��� �� ��������.
end;


function EncodeBlock( B0: Byte; B1: Byte = 0; B2: Byte = 0; B3: Byte = 0; B4: Byte = 0; B5: Byte = 0; B6: Byte = 0; B7: Byte = 0; B8: Byte = 0; B9: Byte = 0 ): TBlock;
begin
  Result[1]:= B0;
  Result[2]:= B1;
  Result[3]:= B2;
  Result[4]:= B3;
  Result[5]:= B4;
  Result[6]:= B5;
  Result[7]:= B6;
  Result[8]:= B7;
  Result[9]:= B8;
  Result[10]:= B9;
end;

function TestBlock( Block: TBlock ): Boolean;
begin
  Result:= ( Block[2] and $F <= 8 ); 
end;

function CompareBlocks( Block1, Block2: TBlock ): Boolean;
var
  i: Integer;
  
begin
  Result:= False;
  if Block1[1] <> Block2[1] then Exit;
  if Block1[2] and $F <> Block2[2] and $F then Exit;
  for i:= 3 to Block1[2] and $F + 2 do
    if Block1[i] <> Block2[i] then Exit;
  Result:= True;
end;

{$IFDEF UseExBlocks}

function BlockAsString(Block: TBlock): string;
var
 Count: Integer;
 i: Integer;

begin
  Count:= Block[2] + 2;
  Result:= '';
  for i:= 1 to Count do
    Result:= Result + Format( '%.2x ', [ Block[i] ] );
  Result:= Trim( Result );
end;

{$ELSE}

function BlockAsString(Block: TBlock): string;
var
 Count: Integer;
 i: Integer;

begin
  Count:= Block[2] and $F + 2;
  Result:= '';
  for i:=1 to Min( 10, Count ) do
    Result:= Result + Format( '%.2x ', [ Block[i] ] );
  if Count > 10 then Result:= Result + ' <<< BAD LENGTH !';
  Result:= Trim( Result );
end;

{$ENDIF}




function ProgramVersion: string;
var
  Major, Minor, Release, BuildNum: Integer;
begin
  GetProgramVersion( Major, Minor, Release, BuildNum );
  if Release = 0 then
    Result:= Format( '%d.%d', [ Major, Minor ] )
  else
    Result:= Format( '%d.%d.%d', [ Major, Minor, Release ] );
end;

procedure GetProgramVersion( out Major, Minor, Release, BuildNum: Integer );
var
 Pt,Pt2: Pointer;
 Size,Size2: DWord;
begin
 Size := GetFileVersionInfoSize(PChar(ParamStr(0)),Size2);
 If Size > 0 then
   begin
     GetMem(Pt, Size);
     try
       GetFileVersionInfo(PChar(ParamStr(0)), 0,Size, PT);
      // ��������� ���������� � ������
       VerQueryValue(Pt,'\', Pt2, Size2);
       with TVSFixedFileInfo(Pt2^) do
         begin
           Major:= HiWord(dwFileVersionMS);
           Minor:= LoWord(dwFileVersionMS);
           Release:= HiWord(dwFileVersionLS);
           BuildNum:= LoWord(dwFileVersionLS);
         end;
     finally
       FreeMem(Pt);
     end;
   end;
end;


function TryHexToInt(const S: string; out Value: Integer): Boolean;
var
 j,
 i : integer;
 C : Char;
 Mn,
 V0,
 V : Integer;
begin
  TryHexToInt:=False;
  if Length(S)=0 then Exit;
  Value:=0;
  V:=0;
  try
    for i:= length(S) downto 1 do
    begin
      C:= UpCase(S[i]);
      if C in ['0'..'9'] then v0:= ord(C) - ord('0')
      else if C in ['A'..'F'] then v0:= 10 + ord(C) - ord('A') else Exit;
      Mn:=1;
      for j:= 1 to ( length(S) - i ) do
        Mn:= Mn * $10;
      V:= V + Mn * v0;
    end;
    Value:= V;
    TryHexToInt:= True;
  except
  end;
end;


function CD( Ph: string = ''; CreateNew: Boolean = False ): Boolean; 
begin
  Chdir( ExtractFilePath( Application.ExeName ) );
  {$I-}
  if Ph <> '' then
  begin
    if not DirectoryExists( Ph ) and CreateNew then MkDir( Ph );
    Chdir( Ph );
  end;
  Result:= IOResult = 0;
  {$I+}
end;

function GetFullPath( RelativeOrFullPath: string = ''; BaseFolder: string = '' ): string;
var
  CurDir: string;
  sdir:string;
  b:boolean;
begin
  CurDir:= GetCurrentDir;
  try
    sdir:={ExcludeTrailingBackslash(}ExtractFilePath( Application.ExeName );
    if BaseFolder = '' then {Chdir}b:=SetCurrentDir( sdir ) else ChDir( BaseFolder );
    if RelativeOrFullPath <> '' then ChDir( RelativeOrFullPath );
  except
  end;
  Result:= AddSlash( GetCurrentDir );
  {ChDir}b:=SetCurrentDir( CurDir );
end;

function GetRelativePath( RelativeOrFullPath, BaseFolder: string ): string;
begin
  BaseFolder:= AddSlash( BaseFolder );
  if Pos( BaseFolder, RelativeOrFullPath ) = 1 then Result:= Copy( RelativeOrFullPath, Length( BaseFolder ) + 1, $FFFF ) else Result:= RelativeOrFullPath;
end;

function ApplicationRoot: string;
begin
  Result:= AddSlash( ExtractFilePath( Application.ExeName ) );
end;

function AddSlash( Path: string ): string;
begin
  if ( Path = '' ) or ( Path[ Length(Path) ] = '\' ) then Result:= Path else Result:= Path + '\';
end;

function SelectDirCB(Wnd: HWND; uMsg: UINT; lParam, lpData: LPARAM): Integer stdcall;
begin
  if (uMsg = BFFM_INITIALIZED) and (lpData <> 0) then
    SendMessage(Wnd, BFFM_SETSELECTION, Integer(True), lpdata);
  result := 0;
end;



function MySelectDirectory(const Caption: string; const Root: WideString; var Directory: string): Boolean;
var
  WindowList: Pointer;
  BrowseInfo: TBrowseInfo;
  Buffer: PChar;
  OldErrorMode: Cardinal;
  RootItemIDList, ItemIDList: PItemIDList;
  ShellMalloc: IMalloc;
  IDesktopFolder: IShellFolder;
  Eaten, Flags: LongWord;

begin
  Result := False;
  if not DirectoryExists(Directory) then
    Directory := '';
  FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
  if (ShGetMalloc(ShellMalloc) = S_OK) and (ShellMalloc <> nil) then
  begin
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      RootItemIDList := nil;
      if Root <> '' then
      begin
        SHGetDesktopFolder(IDesktopFolder);
        IDesktopFolder.ParseDisplayName(Application.Handle, nil,
          POleStr(Root), Eaten, RootItemIDList, Flags);
      end;
      with BrowseInfo do
      begin
        hwndOwner := Application.Handle;
        pidlRoot := RootItemIDList;
        pszDisplayName := Buffer;
        lpszTitle := PChar(Caption);
        ulFlags := BIF_RETURNONLYFSDIRS or BIF_NEWDIALOGSTYLE;
        if Directory <> '' then
        begin
          lpfn := SelectDirCB;
          lParam := Integer(PChar(Directory));
        end;
      end;
      WindowList := DisableTaskWindows(0);
      OldErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS);
      try
        ItemIDList := ShBrowseForFolder(BrowseInfo);
      finally
        SetErrorMode(OldErrorMode);
        EnableTaskWindows(WindowList);
      end;
      Result :=  ItemIDList <> nil;
      if Result then
      begin
        ShGetPathFromIDList(ItemIDList, Buffer);
        ShellMalloc.Free(ItemIDList);
        Directory := Buffer;
      end;
    finally
      ShellMalloc.Free(Buffer);
    end;
  end;
end;


{ TMyConfig }

constructor TMyConfig.Create;
begin
  IniF:= TIniFile.Create( AddSlash( ExtractFilePath( Application.ExeName ) ) + IniFileName );
end;

destructor TMyConfig.Destroy;
begin
  Save;
  IniF.Free;
end;

procedure TMyConfig.LoadList( Section, ItemID: string; List: TStringList );
var
  i: Integer;
  Count: Integer;

begin
  Count:= IniF.ReadInteger( Section, 'Count', 0 );
  List.Clear;
  for i:= 1 to Count do
    List.Add( IniF.ReadString( Section, ItemID + '-' + IntToStr(i), '<�� ��������>' ) );
end;

procedure TMyConfig.SaveList( Section, ItemID: string; List: TStringList );
var
  i: Integer;

begin
  IniF.WriteInteger( Section, 'Count', List.Count );
  for i:= 1 to List.Count do
    IniF.WriteString( Section, ItemID + '-' + IntToStr(i), List[i-1] );
end;


function DecodeAmplify( AmplifyFromBScan: Integer ): Integer;
begin
  // Result:= AmplifyFromBScan * 2 - 12;        // -> ��� �������
  Result:= AmplifyFromBScan * 2 - 18;    // -> ��� ����.
end;

function EncodeAmplify( AmplifyDB: Integer ): Integer;
begin
//  Result:= ( AmplifyDB + 12 ) div 2;  // ��� �������.
  Result:= ( AmplifyDB + 18 ) div 2;  // ��� ����.
end;

function DecodeBScan( BV: TBVBuffer; ByteCount: Integer; OnDecode: TBScanDecodedProc; AdditionalData: Integer ): Boolean;
var
  RailTakt, RailTaktPrev: Integer;
  Rail, Takt: Byte;
  Ofs: Integer;           // �������� � ������� ������ ��� ������� ���� ��.
  BlockSize: Integer;     // ���������� ���� � ����� �� ���.
  A1, A2, A3, A4: Byte;
  T1, T2, T3, T4: Byte;
  Echos: TDecodedEchos;     // ��������������� �������.
  Complete: Boolean;


  function GetByte: Byte;
  begin
    if Ofs > High(BV) then Exit;
    Result:= BV[Ofs];
    Inc( Ofs );
  end;

  procedure AddEcho( T, A: Byte );
  begin
    if Echos.Count <= 7 then
    begin
      Echos.Echo[ Echos.Count ].T:= T;
      Echos.Echo[ Echos.Count ].A:= A;
      Inc( Echos.Count );
    end;
  end;

  procedure DecodeAmp( A: Byte; out A1, A2: Byte );
  begin
    A1:= A and $F0 shr 4;
    A2:= A and $0F;
  end;

begin
  Result:= True;
  Echos.Count:= 0;
  Ofs:= 0;
  RailTaktPrev:= -1;
  repeat   // ���� �� ������� ������ �� ����� ���� ��.

    if Ofs < ByteCount - 1 then
    begin
      BlockSize:= GetByte and $0F;
      RailTakt:= GetByte;
      Complete:= False;
    end else Complete:= True;

    if ( RailTaktPrev <> -1 ) and ( Complete or ( RailTaktPrev <> RailTakt ) ) then
    begin
      OnDecode( Rail, Takt, Echos, AdditionalData );
      Echos.Count:= 0;
    end;

    if Complete then Exit; // �����.

    RailTaktPrev:= RailTakt;

    Rail:= Ord( RailTakt and $80 = $80 );
    Takt:= RailTakt and $7F;

    case BlockSize of
    3:
      begin
        T1:= GetByte;
        DecodeAmp( GetByte, A1, A2 );
        AddEcho( T1, A1 );
      end;
    4:
      begin
        T1:= GetByte;
        T2:= GetByte;
        DecodeAmp( GetByte, A1, A2 );
        AddEcho( T1, A1 );
        AddEcho( T2, A2 );
      end;
    6:
      begin
        T1:= GetByte;
        T2:= GetByte;
        T3:= GetByte;
        DecodeAmp( GetByte, A1, A2 );
        DecodeAmp( GetByte, A3, A4 );
        AddEcho( T1, A1 );
        AddEcho( T2, A2 );
        AddEcho( T3, A3 );
      end;
    7:
      begin
        T1:= GetByte;
        T2:= GetByte;
        T3:= GetByte;
        T4:= GetByte;
        DecodeAmp( GetByte, A1, A2 );
        DecodeAmp( GetByte, A3, A4 );
        AddEcho( T1, A1 );
        AddEcho( T2, A2 );
        AddEcho( T3, A3 );
        AddEcho( T4, A4 );
      end;
    else
      begin
        Result:= False;
        Exit;
      end;
    end; // case.
  until False; // ���� �� ������ Data.

end;

//EGO-USW: �������������� ������ ���� � ����
function BUMtoRail(BUM:byte):byte;
begin
  Result:=BUM div 2;
end;

//EGO-USW: �������������� ���� � ����� ���� � �������� �������� ������� � �������������
function RailToBUM(Rail:byte; Direction:boolean):byte;
begin
  Result:=$FF;
  if Rail=0 then
    if Direction then Result:=0//A-forward
    else Result:=1
  else if Rail=1 then
    if Direction then Result:=2//A-forward
    else Result:=3
end;

function GetSoundVolume: TSoundVolume;
var
  W: PDWORD;
begin
  New( W );
  WaveOutGetVolume( 0, W );
  Result.Left:= Word(W^) * 100 div $FFFF;
  Result.Right:= hiWord(W^) * 100 div $FFFF;
  Dispose( W );
end;

procedure SetSoundVolume( V: TSoundVolume );
var
  W: Cardinal;
  L, R: Word;
begin
  R:= V.Right * $FFFF div 100;
  L:= V.Left * $FFFF div 100;
  W:= R shl 16 + L;
  WaveOutSetVolume( 0, W );
end;


function SearchFlash: string;
var
  i: Integer;
  S: string;
begin
  Result:= '';
  S:= 'X:';
  for i:= 0 to 23 do
  begin
    S[1]:= chr( Ord('C') + i );
    if GetDriveType( PChar(S) ) = DRIVE_REMOVABLE then Result:= Result + S[1];
  end;
end;


function DriveExists( DriveLetter: Char ): Boolean;
var
  S: string;
begin
  S:= DriveLetter + ':';
  Result:= GetDriveType( PChar(S) ) <> DRIVE_NO_ROOT_DIR;
end;

function DriveFixed( DriveLetter: Char ): Boolean;
var
  S: string;
begin
  S:= DriveLetter + ':';
  Result:= GetDriveType( PChar(S) ) = DRIVE_FIXED;
end;



function GetFlashDrive( Mode: Integer; out FlashLetter: Char ): Boolean;
{
  Mode:
    -1 = ����-�����������
    0 = ������������ ���� C:
    1 = ������������ ���� D:
    ...
}

var
  FlashDrives: string;

begin
  Result:= False;
  if Mode = -1 then
  begin
    FlashDrives:= SearchFlash;
    if Length( FlashDrives ) = 0 then Exit;
    FlashLetter:= FlashDrives[1];
  end else FlashLetter:= Chr( Ord('C') + Mode );
  if not DriveExists( FlashLetter ) then Exit;
  try
    ChDir( FlashLetter + ':\' );
  except
    Exit;
  end;
  Result:= True;
end;

function GetFlashDriveNew( Mode: Integer; out FlashLetter: Char ): Boolean;
{
  Mode:
    -1 = ����-�����������
    0 = ������������ ���� C:
    1 = ������������ ���� D:
    ...
}

function DiskInDrive(ADriveLetter : Char) : Boolean;
var
  SectorsPerCluster,
  BytesPerSector,
  NumberOfFreeClusters,
  TotalNumberOfClusters : Cardinal;
  OldMode:Word;

begin
  OldMode:=SetErrorMode(SEM_FAILCRITICALERRORS);
  Result := GetDiskFreeSpace(PChar(ADriveLetter+':\'),
  SectorsPerCluster,
  BytesPerSector,
  NumberOfFreeClusters,
  TotalNumberOfClusters);
  SetErrorMode(OldMode);
end;

var
  w: dword;
  Root: string;
  i: integer;

begin
  Result:= False;
  if Mode = -1 then
    begin
      w := GetLogicalDrives;
      Root := '#:\';
      for i := Ord('C') to Ord('X') do
        begin
          Root[1] := Char(i);
          if (W and (1 shl (i - Ord('A')))) > 0 then
            if DiskInDrive(Char(i)) and (GetDriveType(Pchar(Root)) = DRIVE_removable) then
              begin
                FlashLetter:= Char(i);
                Result:= True;
                Break;
              end;
        end;
    end
  else
    begin
      FlashLetter:= Chr( Ord('C') + Mode );
      Result:= DiskInDrive(FlashLetter);
    end;

  {if not DriveExists( FlashLetter ) then Exit;
  try
    ChDir( FlashLetter + ':\' );
  except
    Exit;
  end;}
  //Result:= True;
end;



{ TFastPainting }

constructor TFastPainting.Create(DrawTo: TBitMap);
var
  i: Integer;

begin
  Self.DrawTo:= DrawTo;
  SetLength( FScanLines, DrawTo.Height );
  for i:= 0 to DrawTo.Height - 1 do
    FScanLines[i]:= DrawTo.ScanLine[i];
end;

procedure TFastPainting.FastFillRect(Rect: TRect);
var
  x: Integer;
  y: Integer;

begin
  for y:= Rect.Top to Min( DrawTo.Height - 1, Rect.Bottom ) do
  for x:= Rect.Left to Min( DrawTo.Width - 1, Rect.Right ) do
    FastPutPixel( x, y );
end;

procedure TFastPainting.FastPutPixel(X, Y: Integer);
var
  P: PByteArray;

begin
 // if ( Y < Length(FScanLines) ) and ( X >= 0 ) and ( Y >= 0 ) then
try
  begin
    P:=FScanLines[Y];
    if P <> nil then
    begin
      P^[X*4]:= 255;
      P^[X*4+1]:= 0;
      P^[X*4+2]:= 0;
    end;
  end;

  except
  end;
end;

end.
