{$I DEF.INC}
unit Lupa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeEngine, Series, ExtCtrls, TeeProcs, Chart, TeeShape, MyTypes,
  ConfigUnit, FiltrUnit, LanguageUnit, AviconDataSource, AviconTypes, StdCtrls,
  Buttons, Grids;

type
  TLupaForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Chart2: TChart;
    Series2: TPointSeries;
    Series4: TChartShape;
    Chart1: TChart;
    Series3: TLineSeries;
    Series1: TChartShape;
    Ascan1Chart: TChart;
    Panel31: TPanel;
    Panel29: TPanel;
    Panel32: TPanel;
    Ascan2Chart: TChart;
    Panel30: TPanel;
    StaticText5: TStaticText;
    StaticText4: TStaticText;
    Panel5: TPanel;
    Ascan3Chart: TChart;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Ascan4Chart: TChart;
    Panel9: TPanel;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    Panel10: TPanel;
    Chart1_: TChart;
    Chart2_: TChart;
    StringGrid1: TStringGrid;
    Shape1: TShape;
    Panel11: TPanel;
    Panel12: TPanel;
    procedure Chart2MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Chart2MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Chart2MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
//    procedure MakeMScheme(Sender: TObject);
  private
    FDatSrc: TAvk11DatSrc;
//    FWorkR: TRail;
//    FWorkDelay: Integer;
//    FWorkCh: array [1..2] of Integer;
    FDisCoord: Integer;
    FScanStep: Single;
    FHalfDispWidth: Integer;
    FRed: Integer;
    FRedMode: Integer;
    FLastCoord: Integer;
    FLastVal: Integer;
    FDisplay: Pointer;
    FMPD: Pointer;
    FBSALastCoord: array [r_Left..r_Both] of Integer;
    FBSALastVal: array [r_Left..r_Both] of Integer;
    FLastDrawX1: array [RRail] of Integer;
    FLastDrawX2: array [RRail] of Integer;
//    LastValue: Single;
    LastCrd: Single;
//    Mode: Integer;

    FAScanElements: array [1..4] of array of TChartShape;

    function BlockOk(StartDisCoord: Integer): Boolean;
    procedure CreateImage;
    procedure ClearAScanElements;
//    procedure ClearAScan2;
    procedure AddAScanElement(Chart: TChart; ScanIdx: Integer; Delay: Extended; Ampl: Integer; Color: TColor; Percent: Integer = 100);
//    procedure AddAScan2(Delay: Extended; Ampl: Integer; Color: TColor);
    procedure EndAScan;
    procedure SetAScanDelayZone(Chart: TChart; Min1, Max1: Integer);

  public
    LastMode: Integer;
    Mode: Integer;
    ChartList: TList;

    procedure DeleteShapes;
    function SetData(MPD: Pointer; DS: TAvk11DatSrc; Reduction, ReductionMode: Integer; Display: Pointer): Boolean;
    procedure SetPos;
  end;

var
  LupaForm: TLupaForm;

implementation

{$R *.DFM}

uses
  Math, DisplayUnit, Types;

procedure KillSelection(StringGrid: TStringGrid);
var
  GridRect: TGridRect;

begin
  GridRect:= StringGrid.Selection;
  GridRect.Top:= 100;
  GridRect.Bottom:= 100;
  GridRect.Left:= 100;
  GridRect.Right:= 100;
  StringGrid.Selection:= GridRect;
end;

function CalcPixelLen(Src: Integer): Integer;
var
  ScreenDC: HDC;
  VertPixelsPerInch: Integer;
  HorzPixelsPerInch: Integer;

begin
  ScreenDC := GetDC(0);
  VertPixelsPerInch := GetDeviceCaps(ScreenDC, LogPixelsX);
  HorzPixelsPerInch := GetDeviceCaps(ScreenDC, LogPixelsY);

//  VertPixelsPerInch:= 96;

  if VertPixelsPerInch = 96 then Result:= Src;
  if VertPixelsPerInch = 120 then Result:= Round(Src * 1.25);
end;

function TLupaForm.BlockOk(StartDisCoord: Integer): Boolean;
var
  I, J: Integer;
//  ChIdx: Integer;
  Col: TColor;
  Shift: Real;
  Ch0Ampl: Integer;
  Ch0Idx: Integer;

  Idx, ChNum: Integer;
  MaxVal: Integer;
  ThVal: Integer;
  ThPos: Single;
  V: Single;

  EvalChListIdx: Integer;
  Crd: Single;
  Value: Single;
  ValueFlg: Boolean;
//  BSFlg: array [1..16] of Boolean;
//  MaxBSFlg: array [1..16] of Boolean;

  Ch0Flg: Boolean;
  tmp: TIntegerDynArray;
  EvalChNum: Integer;
  XPos: Integer;

  Coeff: Integer;

begin
  ThPos:= 1/8;
  MaxVal:= 256;
  ThVal:= Round(MaxVal * ThPos);

  if not Assigned(FDatSrc) then Exit;
    {
  try
    DrawX:= FStartScrX + (FDatSrc.CurDisCoord - StartDisCoord) * FDatSrc.Header.ScanStep div FDM.Zoom
  except
    Exit;
  end;
  if Reduction <> 0 then Result:= DrawX <= FEndScrX + Round(FMaxReduce[FDM.Reduction] / (Zoom / 100))
                    else Result:= DrawX <= FEndScrX;
     }

  Result:= true; // DrawX <= FEndScrX;

// -------------------- ��������� ���-�������� -----------------------------------------------

   with TMousePosDat(FMPD^) do
   if Length(ScanChNumList) <> 0 then
   begin

     for Idx := 0 to High(ScanChNumList) do
     begin
       ChNum:= ScanChNumList[Idx];

       with FDatSrc.Config.ScanChannelByNum[ChNum] do
       begin


        // ������� �������� ������� �� ���������� �������

        if CompareMem(@FDatSrc.Header.DeviceID, @Avicon15Scheme1, 8) and
           MirrorShadow and
           (FDatSrc.CurEcho[Rail, ChNum].Count = 2) and
           (FDatSrc.CurEcho[Rail, ChNum].Delay[1] / DelayMultiply > FDatSrc.CurParams.Par[Rail, 1].EndStr) and
           (FDatSrc.CurEcho[Rail, ChNum].Delay[2] / DelayMultiply > FDatSrc.CurParams.Par[Rail, 1].EndStr) then FDatSrc.FCurEcho[Rail, ChNum].Count:= 1;


           for J:= 1 to FDatSrc.CurEcho[Rail, ChNum].Count do
             if FDatSrc.CurEcho[Rail, ChNum].Ampl[J] >= TAvk11Display(FDisplay).AmplTh then
             begin
//               DrawY:= BScanRt.Bottom - Round(Abs(BScanRt.Top - BScanRt.Bottom) * (FDatSrc.CurEcho[R, ChNum].Delay[J] / DelayMultiply - BScanGateMin) / BScanDuration);
//               if Config.InvertDelayAxis then DrawY:= BScanRt.Top + BScanRt.Bottom - DrawY;

               if MirrorShadow then
               begin
                if (FDatSrc.CurEcho[Rail, ChNum].Delay[J] / DelayMultiply >= FDatSrc.CurParams.Par[Rail, MirrorShadowEvalChNum].StStr) and
                   (FDatSrc.CurEcho[Rail, ChNum].Delay[J] / DelayMultiply <= FDatSrc.CurParams.Par[Rail, MirrorShadowEvalChNum].EndStr) then
                 begin
                   Ch0Flg:= True;
                   if Idx < 2 then Col:= Config.ActivDColors.NaezdCh[1]
                              else Col:= Config.ActivDColors.NaezdCh[2];
//                   BSFlg[J]:= True;
                 end
                 else
                 begin
                   Ch0Flg:= False;
                   if Idx < 2 then Col:= Config.ActivDColors.OtezdCh[1]
                              else Col:= Config.ActivDColors.OtezdCh[2];

                 end;
                 if not ((TAvk11Display(FDisplay).DisplayMode.ViewChannel = vcAll) or
                         (TAvk11Display(FDisplay).DisplayMode.ViewChannel = vcOtezd)) then Continue;
               end
               else
               begin
                 Ch0Flg:= False;
                 if not ((TAvk11Display(FDisplay).DisplayMode.ViewChannel = vcAll) or
                        ((TAvk11Display(FDisplay).DisplayMode.ViewChannel = vcOtezd) and Odd(ChNum)) or
                        ((TAvk11Display(FDisplay).DisplayMode.ViewChannel = vcNaezd) and (not Odd(ChNum)))) then Continue;
//                 Col:= TAvk11Display(FDisplay).ChNumColors[Rail, ChNum, 15];

                 tmp:= FDatSrc.Config.GetEvalsChIdxByScanChNum(ChNum);
                 EvalChNum:= tmp[0];
                 SetLength(tmp, 0);

                 Col:= TAvk11Display(FDisplay).EvalChNumColors2[FDatSrc.Config.ChannelIDList[Rail, EvalChNum], 15];
//                       TAvk11Display(FDisplay).EvalChNumColors2[FDatSrc.Config.ChannelIDList[TMousePosDat(FMPD^).Rail, EC.Number], 15];
               end;

               if FDatSrc.CurEcho[Rail, ChNum].Ampl[J] >= TAvk11Display(FDisplay).DisplayMode.AmplTh then // ����������� ������� ������ ����������� �� ��
               begin
                 if FDatSrc.Config.ReductionConst then
                 begin
                   Coeff:= 1 - 2 * Ord(FDatSrc.isEGOUSW_BHead);
                   case TAvk11Display(FDisplay).Reduction of
                     0: Shift:= 0;
                     1: Shift:= (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[TAvk11Display(FDisplay).Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[TAvk11Display(FDisplay).Reduction, ChNum] * FDatSrc.CurEcho[Rail, ChNum].Delay[J]) {/ (FDM.Zoom / 100)});
                     2: Shift:= (1 - 2 * Ord(FDatSrc.BackMotion)) * Round((Coeff * FDatSrc.Config.ReducePos[TAvk11Display(FDisplay).Reduction, ChNum] + Coeff * FDatSrc.Config.ReduceDel[TAvk11Display(FDisplay).Reduction, ChNum] * FDatSrc.CurEcho[Rail, ChNum].Delay[J]) {/ (FDM.Zoom / 100)});
                   end;
                 end
                 else
                 case TAvk11Display(FDisplay).Reduction of
                   0: Shift:= 0;
                   1: Shift:= (1 - 2 * Ord(FDatSrc.BackMotion)) * Round( GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], (FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin + FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax) div 2, FDatSrc.isEGOUSW_BHead or FDatSrc.isVMT_US_BigWP_BHead) + FDatSrc.Config.ScanChannelByNum[ChNum].Delta {/ (TAvk11Display(FDisplay).DisplayMode.Zoom / 100)});
                   2: Shift:= (1 - 2 * Ord(FDatSrc.BackMotion)) * Round(GetRealCrd_inMM(FDatSrc.Config.ScanChannelByNum[ChNum], FDatSrc.CurEcho[Rail, ChNum].Delay[J], FDatSrc.isEGOUSW_BHead or FDatSrc.isVMT_US_BigWP_BHead) {/ (TAvk11Display(FDisplay).DisplayMode.Zoom / 100)});
                 end;

                 //FastPoint(DrawX + DX, DrawY, 2, 3, Color);

                 XPos:= Round((FDatSrc.CurDisCoord - FDisCoord) * FScanStep + Shift);

                 if (XPos >= Chart2.BottomAxis.Minimum) and (XPos <= Chart2.BottomAxis.Maximum) then
                  Series2.AddXY(XPos, FDatSrc.CurEcho[Rail, ChNum].Delay[J] / DelayMultiply, '', Col);
               end;
             end;
       end;
     end;

     ///////////////////////////////////////////////////////////////////////////////////


     if TMousePosDat(FMPD^).MirrorShadow then
     begin
//       for I:= 1 to 16 do BSFlg[I]:= False;
       for Idx := 0 to High(ScanChNumList) do
       begin
         ChNum:= ScanChNumList[Idx];
         Ch0Idx:= - 1;                                                  // ���� ������������ ������ �� ��� ���������
         with FDatSrc.Config.ScanChannelByNum[ChNum] do
           for J:= 1 to FDatSrc.CurEcho[Rail, ChNum].Count do
             if (FDatSrc.CurEcho[Rail, ChNum].Delay[J] / DelayMultiply >= FDatSrc.CurParams.Par[Rail, MirrorShadowEvalChNum].StStr) and
                (FDatSrc.CurEcho[Rail, ChNum].Delay[J] / DelayMultiply <= FDatSrc.CurParams.Par[Rail, MirrorShadowEvalChNum].EndStr) and
                ((Ch0Idx = - 1) or (FDatSrc.CurEcho[Rail, ChNum].Ampl[J] > Ch0Ampl)) then
             begin
               Ch0Ampl:= FDatSrc.CurEcho[Rail, ChNum].Ampl[J];
               Ch0Idx:= J;
             end;

         if Ch0Idx <> - 1 then
         begin
           Value:= FDatSrc.Config.AmplValue[FDatSrc.CurEcho[Rail, ChNum].Ampl[Ch0Idx]];
           ValueFlg:= True;

           Crd:= FDatSrc.CurDisCoord;

           if ValueFlg then
           begin
             if Crd - LastCrd <= 4 then            // ���� ����� ������ 5 ����� ����� ��������� �� ���������
             begin
               Series3.AddXY((LastCrd - FDisCoord) * FScanStep + Shift, Value);
               Series3.AddXY((Crd - FDisCoord) * FScanStep + Shift, Value);
             end
             else                                  // ���� ����� ������ 4 ����� ��������� ����� ��������� �������� �� ����
             begin
               Series3.AddXY((LastCrd - FDisCoord) * FScanStep + Shift, - 12);
               Series3.AddXY((Crd - FDisCoord) * FScanStep + Shift, - 12);
               Series3.AddXY((Crd - FDisCoord) * FScanStep + Shift, Value);
             end;
             LastCrd:= Crd;
           end;
         end;
         Break;
       end;
     end;


     ////////////////////////////////////////////////////////////////////////////////////


   end;


(*
  for ChIdx:= 1 to 2 do                                            // ����� ������� �������
  begin
    for I:= 1 to 16 do BSFlg[I]:= False;
//    if FWorkCh[ChIdx] in [0, 1] then Coeff:= 1/3
//                                else Coeff:= 1;


    TMousePosDat(FMPD^). Rail


    if FWorkCh[ChIdx] = 1 then                                     // �������� ������� ���������� � ����� 0 ������
    with FDatSrc.CurEcho[FWorkR, FWorkCh[ChIdx]] do
      for I:= 1 to Count do
        if (FDatSrc.CurEcho[FWorkR, 1].Delay[I] / FDatSrc.Config.ScanChannelByNum[1].DelayMultiply >= FDatSrc.CurParams.Par[FWorkR, 0].StStr) and
           (FDatSrc.CurEcho[FWorkR, 1].Delay[I] / FDatSrc.Config.ScanChannelByNum[1].DelayMultiply <= FDatSrc.CurParams.Par[FWorkR, 0].StStr) then BSFlg[I]:= True;

    for I:= 1 to 16 do MaxBSFlg[I]:= False;
*)
       (*

  end;

  ValueFlg:= False;

//  if (FDatSrc.Header.ContentFlg and cfBSAEcho <> 0) and (FDatSrc.CurAmpl[FWorkR] > 0) then // 1 ��� - ��������� �� (���������);
//  begin
//    Value:= 20 * log10(FDatSrc.CurAmpl[FWorkR] / ThVal);
//    ValueFlg:= True;
//  end
//  else

  if Ch0Idx <> - 1 then
  begin
    Value:= FDatSrc.Config.AmplValue[FDatSrc.CurEcho[FWorkR, 1].Ampl[Ch0Idx]];
    ValueFlg:= True;
  end;

  if FRed <> 0 then Shift:= (1 - 2 * Ord(FDatSrc.BackMotion)) * (Config.DisplayConfig.ReducePos[FRedMode, FRed, 1])
               else Shift:= 0;

  Crd:= FDatSrc.CurDisCoord;

  if ValueFlg then
  begin
    if Crd - LastCrd <= 4 then            // ���� ����� ������ 5 ����� ����� ��������� �� ���������
    begin
      Series3.AddXY((LastCrd - FDisCoord) * FScanStep + Shift, Value);
      Series3.AddXY((Crd - FDisCoord) * FScanStep + Shift, Value);
    end
    else                                  // ���� ����� ������ 4 ����� ��������� ����� ��������� �������� �� ����
    begin
      Series3.AddXY((LastCrd - FDisCoord) * FScanStep + Shift, - 12);
      Series3.AddXY((Crd - FDisCoord) * FScanStep + Shift, - 12);
      Series3.AddXY((Crd - FDisCoord) * FScanStep + Shift, Value);
    end;
    LastCrd:= Crd;
  end; *)
end;

procedure TLupaForm.CreateImage;
var
  Idx: Integer;
  Coeff: Single;
  GateMin: Integer;
  GateMax: Integer;
  Speed: Integer;
  State: Integer;
  SpeedExists: Boolean;

begin
  if not Assigned(FDatSrc) then Exit;

//  SpeedExists:= FDatSrc.GetSpeedState(FDisCoord, Speed, State);

//  if SpeedExists then
//
//    Chart2.Title.Text.Text:= RealCrdToStr(FDatSrc.DisToRealCoord(FDisCoord), 0) +
//                             ' (' + FDatSrc.GetTime(FDisCoord) + '/' + FDatSrc.GetTime(FDisCoord, 1) + ')' +
//                             ' ( t - ' + FloatToStr(FDatSrc.GetTemperature(FDisCoord)) + ' ����)' +
//                             ' ( v - ' + IntToStr(Speed) + ' ��/�)'
//    else

    Chart2.Title.Text.Text:= RealCrdToStr(FDatSrc.DisToRealCoord(FDisCoord), 0) { +
                             ' (' + FDatSrc.GetTime(FDisCoord) + '/' + FDatSrc.GetTime(FDisCoord, 1) + ')' +
                             ' ( t - ' + FloatToStr(FDatSrc.GetTemperature(FDisCoord)) + ' ����)'};

  //  Chart2.Title.Text.Text:= IntToStr(FHalfDispWidth);

  if FHalfDispWidth < 100 then Series2.Pointer.HorizSize:= 2
                          else Series2.Pointer.HorizSize:= 1;

 with TMousePosDat(FMPD^) do
 begin
   if not OK then Exit;

   GateMin:= MaxInt;
   GateMax:= - MaxInt;
   for Idx := 0 to High(ScanChNumList) do
     with FDatSrc.Config.ScanChannelByNum[ScanChNumList[Idx]] do
     begin
       GateMin:= Min(GateMin, BScanGateMin);
       GateMax:= Max(GateMax, BScanGateMax);
     end;
 end;

  SetAScanDelayZone(Ascan1Chart, GateMin, GateMax);
  SetAScanDelayZone(Ascan2Chart, GateMin, GateMax);
  SetAScanDelayZone(Ascan3Chart, GateMin, GateMax);
  SetAScanDelayZone(Ascan4Chart, GateMin, GateMax);

  with Config.DisplayConfig do
  begin
    Chart2.LeftAxis.SetMinMax(GateMin, GateMax);
    Series4.Y0:= GateMin;
    Series4.Y1:= GateMax;
  end;

  Chart2.BottomAxis.SetMinMax( - FHalfDispWidth, FHalfDispWidth);
  Series4.X0:= 0;
  Series4.X1:= 0;
  Series1.AddXY(0, - 6);
  Series1.AddXY(0, 18);
  Chart1.BottomAxis.SetMinMax( - FHalfDispWidth, FHalfDispWidth);

//  if FDatSrc.Header.ContentFlg and cfBSAEcho <> 0 then Chart1.LeftAxis.SetMinMax( - 1, 256)
//                                                  else
  Chart1.LeftAxis.SetMinMax( - 6.5, 18.5);
  Series2.Clear;

  if Assigned(TAvk11Display(FDisplay).Filtr) then
    TAvk11Display(FDisplay).Filtr.StartFilter(FDisCoord - 3 * Round(FHalfDispWidth * FScanStep));
  FLastCoord:= - 1;

  LastCrd:= (FDisCoord - 3 * Round(FHalfDispWidth * FScanStep) - FDisCoord) * FScanStep;
//  if FRed <> 0 then LastCrd:= LastCrd + (1 - 2 * Ord(FDatSrc.BackMotion)) * (Config.DisplayConfig.ReducePos[FRedMode, FRed, 1]);
  if FRed <> 0 then LastCrd:= LastCrd + TAvk11Display(FDisplay).FMaxReduce[FRed];

  FDatSrc.LoadData(FDisCoord - 15 * Round(FHalfDispWidth / FScanStep), FDisCoord + 15 * Round(FHalfDispWidth / FScanStep), 0, BlockOk);
end;

procedure TLupaForm.DeleteShapes;
var
  I: Integer;

begin
  for I := 0 to ChartList.Count - 1 do TChartShape(ChartList[I]).Free;
  ChartList.Clear;
end;

function TLupaForm.SetData(MPD: Pointer; DS: TAvk11DatSrc; Reduction, ReductionMode: Integer; Display: Pointer): Boolean;
var
  LBSDC, I, J, K_, Shift: Integer;
  K, Coeff: Extended;
//  CR: TRealCoord;
  Col1, Col2: TColor;
  Value: LongWord;
  Value1: LongWord;
  B: array [1..4] of Byte;
  TimeOut: LongWord;
  StartTime: LongWord;
  ChNum: Integer;
  Chart: TChart;
//  EC: TEvalChannelItem;
  SC: TScanChannelItem;
  S: string;

  X, X0, Y0, Y1, S_, A: Integer;
  Rt: TRect;
  Text: string;
  F: Boolean;

  RC: Integer;
  H: array [0..3] of Integer;
  Shape: TChartShape;
  PEPSize: Integer;
  X1, Q: Integer;
  Arr: TIntegerDynArray;
  StartX, EndX, StartY: Integer;
  PenWidth: Integer;

  tmp: TIntegerDynArray;
  EvalChNum: Integer;

const
  Colors_: array [0..12] of TColor = (clRed, clRed, clRed, clRed, clRed, clRed, clRed, clRed, clRed, clRed, clRed, clRed, clRed);

function GetSurfaceText(Rail: TRail; Gran: TGran): string;
begin
  case Gran of
                      _None: Result:= '';
                      _Work: Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT'];
                    _UnWork: Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
   _Work_Left__UnWork_Right: if Rail = rLeft then Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT']
                                             else Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
   _UnWork_Left__Work_Right: if Rail = rLeft then Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT']
                                             else Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT'];
  end;
//  Result:= Result + IntToStr(Ord(Rail)) + IntToStr(Ord(Gran));
end;

begin
  Result:= False;
  if not Assigned(MPD) then Exit;
  if not TMousePosDat(MPD^).Ok then Exit;

  FDisplay:= Display;
  FMPD:= MPD;
  FDisCoord:= TMousePosDat(FMPD^).MouseDisCoord;
  FDatSrc:= DS;
  FScanStep:= FDatSrc.Header.ScanStep / 100;

  Chart2.LeftAxis.Inverted:= Config.InvertDelayAxis;

  if Mode = 0 then
  begin
    Panel10.SendToBack;
    Chart2.Title.Text.Text:= ' ';
    Series1.Clear;
    Series3.Clear;

    ClearAScanElements;

    FRed:= Reduction;
    FRedMode:= ReductionMode;

  (*
    if not TMousePosDat(FMPD^).MirrorShadow then
    begin
  //    Chart2.Height:= 369;
      Self.Height:= 411;
      Chart1.Visible:= False;

      StaticText5.Visible:= True;
      Panel32.Visible:= True;
      Panel30.Visible:= True;
      Ascan2Chart.Visible:= True;
    end
    else
    begin
  //    Chart2.Height:= 369 - 120;
      Self.Height:= 506;
      Chart1.Visible:= True;

      StaticText5.Visible:= False;
      Panel32.Visible:= False;
      Panel30.Visible:= False;
      Ascan2Chart.Visible:= False;
    end;

    if Length(TMousePosDat(FMPD^).ScanChNumList) = 4 then
    begin
      Panel5.Visible:= True;
      Self.Width:= 636;
    end
    else
    begin
      Panel5.Visible:= False;
      Self.Width:= 636 - 164;
    end;
  *)

    if LastMode <> Length(TMousePosDat(FMPD^).ScanChNumList) then
      case Length(TMousePosDat(FMPD^).ScanChNumList) of
        1: begin
             Chart1.Visible:= True;
             StaticText5.Visible:= False;

             Panel32.Visible:= False;
             Panel30.Visible:= False;
             Ascan2Chart.Visible:= False;
             Chart2.Height:= CalcPixelLen(369 - 95);
             Panel4.Visible:= False;
             Self.Width:= CalcPixelLen(472 - 5);
           end;
        2: begin
             Chart1.Visible:= False;
             Panel4.Visible:= False;
             StaticText5.Visible:= True;
             Panel32.Visible:= True;
             Panel30.Visible:= True;
             Ascan2Chart.Visible:= True;
             Self.Width:= CalcPixelLen(472 - 5);
             Self.Height:= CalcPixelLen(411 + 21);
           end;
        3: begin
             Chart1.Visible:= False;
             Panel4.Visible:= True;
             StaticText5.Visible:= True;
             Panel32.Visible:= True;
             Panel30.Visible:= True;
             Ascan2Chart.Visible:= True;
             Self.Width:= CalcPixelLen(636 - 5);
             Self.Height:= CalcPixelLen(411 + 21);
             Ascan4Chart.Visible:= False;
             StaticText1.Visible:= False;
             Panel8.Visible:= False;
             Panel9.Visible:= False;
           end;
        4: begin
             Chart1.Visible:= False;
             Panel4.Visible:= True;
             StaticText5.Visible:= True;
             Panel32.Visible:= True;
             Panel30.Visible:= True;
             Ascan2Chart.Visible:= True;
             Self.Width:= CalcPixelLen(636 - 5);
             Self.Height:= CalcPixelLen(411 + 21);
           end;
      end;

    LastMode:= Length(TMousePosDat(FMPD^).ScanChNumList);

    if FRed <> 0 then
    begin
      Panel3.Visible:= False;
      Panel4.Visible:= False;
      Self.Width:= CalcPixelLen(306);
    end;

    for I := 0 to High(TMousePosDat(FMPD^).ScanChNumList) do
    begin

      SC:= FDatSrc.Config.ScanChannelByNum[TMousePosDat(FMPD^).ScanChNumList[I]];
//      SC:= FDatSrc.Config.ScanChannelByNum[EC.ScanChNum];

            { ����������� }
//      S:= LangTable.Caption['Common:ChannelParams_SHORT:Direction']; { ����������� }
      if FDatSrc.isEGOUSW  or FDatSrc.isVMT_US_BigWP then S:= LangTable.Caption['Common:ChannelParams_SHORT:Direction_B']
                                                     else S:= LangTable.Caption['Common:ChannelParams_SHORT:Direction_A'];

//      if SC.EnterAngle > 0 then S:= S + ' ' + LangTable.Caption['Common:ZoomInChannel_SHORT'];
//      if SC.EnterAngle < 0 then S:= S + ' ' + LangTable.Caption['Common:ZoomOutChannel_SHORT'];
//      if SC.EnterAngle = 0 then S:= S + ' -';

         if not (FDatSrc.isEGOUSW or FDatSrc.isVMT_US_BigWP) then
         begin
           if SC.EnterAngle > 0 then S:= S + ' ' + LangTable.Caption['Common:ZoomInChannel_SHORT'];
           if SC.EnterAngle < 0 then S:= S + ' ' + LangTable.Caption['Common:ZoomOutChannel_SHORT'];
           if SC.EnterAngle = 0 then S:= S + ' -';
         end
         else
         if FDatSrc.isVMTUS_Scheme_1_or_2 then
         begin
           if EGOUSW_WP_Data[SC.BS_Ch_File_Number] = 0 then S:= S +  'A' else S:= S +  'B';
         end
         else
         if FDatSrc.isVMTUS_Scheme_4 then
         begin
           if EGOUSW_BigWP_Data[SC.BS_Ch_File_Number] = 0 then S:= S +  'A' else S:= S +  'B';
         end;

      S:= S + '; ' + LangTable.Caption['Common:ChannelParams_SHORT:Angles'];    { ����/�����  }

            { ����/�����  }
      if (SC.TurnAngle <> 0) or (SC.Gran <> _None) then
      begin

                     if GetSurfaceText(TMousePosDat(MPD^).Rail, SC.Gran) = ''

                       then S:= S + Format(' %d/%d; ', [Abs(SC.EnterAngle), SC.TurnAngle])
                      // S:= Format('%d/%d', [Abs(SC.EnterAngle), Abs(SC.TurnAngle)])
                       else // S:= S + Format(' %d/%d; ', [Abs(SC.EnterAngle), SC.TurnAngle])
                       S:= S + Format(' %d/%s', [Abs(SC.EnterAngle), GetSurfaceText(TMousePosDat(MPD^).Rail, SC.Gran)]);

      end
  //    S:= S + Format(' %d/%d; ', [Abs(SC.EnterAngle), SC.TurnAngle])
                           else S:= S + Format(' %d; ', [Abs(SC.EnterAngle)]);
        (*
      S:= S + LangTable.Caption['Common:ChannelParams_SHORT:Method'];    { �����       }
      S:= S + LangTable.Caption['Common:' + GetInspMethodTextId(EC.Method, False)] + '; '; { �����       }
      S:= S + LangTable.Caption['Common:ChannelParams_SHORT:Zone'];      { ����        }
      S:= S + LangTable.Caption['Common:' + GetInspZoneTextId(EC.Zone, False)]; { ����       }
          *)

      case I of
        0: StaticText4.Caption:= S;
        1: StaticText5.Caption:= S;
        2: StaticText2.Caption:= S;
        3: StaticText1.Caption:= S;
      end;
    end;

    CreateImage;

    with TMousePosDat(FMPD^) do
    if Length(ScanChNumList) <> 0 then
    begin
      ClearAScanElements;
      if TAvk11Display(FDisplay).Reduction = 0 then
      begin

        for I := 0 to High(ScanChNumList) do
        begin
          ChNum:= ScanChNumList[I];

          tmp:= FDatSrc.Config.GetEvalsChIdxByScanChNum(ChNum);
          EvalChNum:= tmp[0];
          SetLength(tmp, 0);

          case I of
            0: Chart:= AScan1Chart;
            1: Chart:= AScan2Chart;
            2: Chart:= AScan3Chart;
            3: Chart:= AScan4Chart;
          end;
{
          if (DS.Header.UsedItems[uiAcousticContact] = 1) and (Length(CurEchoList) <> 0) then
          begin
            if CurEchoList[0].CurAKExists[Rail, ChNum] and (not CurEchoList[0].CurAKState[Rail, ChNum])
              then Chart.Color:= clRed
              else Chart.Color:= clWhite;
          end;
}
          for J:= 0 to High(CurEchoList) do
            with CurEchoList[J].Echo[Rail, ChNum] do
              for K_:= 1 to Count do

//                if not (TAvk11Display(FDisplay).DatSrc.Config.DeviceName = 'Avicon15Scheme1') then

                AddAScanElement(Chart, I + 1, Delay[K_] / FDatSrc.Config.ScanChannelByNum[ChNum].DelayMultiply, FDatSrc.Config.AmplValue[Ampl[K_]],
               // TAvk11Display(FDisplay).ChNumColors[Rail, ChNum, 15]);
                TAvk11Display(FDisplay).EvalChNumColors2[FDatSrc.Config.ChannelIDList[Rail, EvalChNum], 15] {, 100 - 10 * Abs(CurEchoList[J].DisCrd - MouseDisCoord)})
//              TAvk11Display(FDisplay).EvalChNumColors2[FDatSrc.Config.ChannelIDList[TMousePosDat(FMPD^).Rail, EC.Number], 15];

//                else
//                AddAScanElement(Chart, I + 1, Delay[K_] / FDatSrc.Config.ScanChannelByNum[ChNum].DelayMultiply, FDatSrc.Config.AmplValue[Ampl[K_]], Colors_[ChNum])
        end;

  //    for I:= 0 to High(AScanArr[2]) do AddAScan2(AScanArr[2, I].Delay, AScanArr[2, I].Ampl, AScanArr[2, I].Color);
        EndAScan;
      end;
    end;
  end
  else
  if Mode = 1 then
  begin
    Self.Width:= CalcPixelLen(525);
    Panel10.BringToFront;

    DeleteShapes;

    StartX:= -230;
    EndX:= 230;
    StartY:= 20;

    Chart1_.BottomAxis.SetMinMax(StartX, EndX);
    Chart1_.LeftAxis.SetMinMax(StartY, StartY - Chart1_.Height * (EndX - StartX) / Chart1_.Width);

    PEPSize:= 18;

    Shape:= TChartShape.Create(Chart1_);
    Shape.ParentChart:= Chart1_;
    Shape.Style:= chasLine;
    Shape.X0:= - 800;
    Shape.X1:= 800;
    Shape.Y0:= 0;
    Shape.Y1:= 0;
    ChartList.Add(Shape);

    Shape:= TChartShape.Create(Chart1_);
    Shape.ParentChart:= Chart1_;
    Shape.Style:= chasLine;
    Shape.X0:= - 800;
    Shape.X1:= 800;
    Shape.Y0:= -39{33};
    Shape.Y1:= -39{33};
    ChartList.Add(Shape);
                      {
    Shape:= TChartShape.Create(Chart1);
    Shape.ParentChart:= Chart1;
    Shape.Style:= chasLine;
    Shape.X0:= - 800;
    Shape.X1:= 800;
    Shape.Y0:= -45;
    Shape.Y1:= -45;
    ChartList.Add(Shape);
                       }
    Shape:= TChartShape.Create(Chart1_);
    Shape.ParentChart:= Chart1_;
    Shape.Style:= chasLine;
    Shape.X0:= - 800;
    Shape.X1:= 800;
    Shape.Y0:= -(180 - 11);
    Shape.Y1:= -(180 - 11);
    ChartList.Add(Shape);

    Shape:= TChartShape.Create(Chart1_);
    Shape.ParentChart:= Chart1_;
    Shape.Style:= chasLine;
    Shape.X0:= - 800;
    Shape.X1:= 800;
    Shape.Y0:= -180;
    Shape.Y1:= -180;
    ChartList.Add(Shape);

                                // ������� ����������� ��������

    Shape:= TChartShape.Create(Chart1_);
    Shape.ParentChart:= Chart1_;
    Shape.Style:= chasLine;
    Shape.X0:= 220;
    Shape.X1:= 180;
    Shape.Y0:= 10;
    Shape.Y1:= 10;
    ChartList.Add(Shape);

    Shape:= TChartShape.Create(Chart1_);
    Shape.ParentChart:= Chart1_;
    Shape.Style:= chasLine;
    Shape.X0:= 220;
    Shape.X1:= 215;
    Shape.Y0:= 10;
    Shape.Y1:= 15;
    ChartList.Add(Shape);

    Shape:= TChartShape.Create(Chart1_);
    Shape.ParentChart:= Chart1_;
    Shape.Style:= chasLine;
    Shape.X0:= 220;
    Shape.X1:= 215;
    Shape.Y0:= 10;
    Shape.Y1:= 5;
    ChartList.Add(Shape);


    with TAvk11Display(FDisplay).DatSrc.Config do
    begin

      for I := 0 to ScanChannelCount - 1 do
      begin
        Arr:= GetEvalsChIdxByScanChIdx(I);
        PenWidth:= 1;
        for J := 0 to High(TMousePosDat(FMPD^).ScanChNumList) do
          if TMousePosDat(FMPD^).ScanChNumList[J] = ScanChannelByIdx[I].BS_Ch_File_Number then
          begin
            PenWidth:= 3;
          end;


        if ((Abs(ScanChannelByIdx[I].EnterAngle) = 58) or
            (Abs(ScanChannelByIdx[I].EnterAngle) = 50)) and
           (EvalChannelByIdx[Arr[0]].Method <> imMirror) then
        begin
          RC:= 2;
          H[1]:= 33;
          H[2]:= 33;
          H[3]:= 33;
        end
        else
        if ((Abs(ScanChannelByIdx[I].EnterAngle) = 58) or
            (Abs(ScanChannelByIdx[I].EnterAngle) = 50)) and
           (EvalChannelByIdx[Arr[0]].Method = imMirror) then
        begin
          RC:= 2;
          H[1]:= 32;
          H[2]:= 16;
          H[3]:= - 15;
        end
        else
        if Abs(ScanChannelByIdx[I].EnterAngle) = 70 then
        begin
          RC:= 0;
          H[1]:= 55;
        end
        else
        if (Abs(ScanChannelByIdx[I].EnterAngle) in [42, 45]) and (Abs(ScanChannelByIdx[I].TurnAngle) = 0) then
        begin
          RC:= 1;
          H[1]:= 180;
          H[2]:= 20;
        end
        else
        if Abs(ScanChannelByIdx[I].EnterAngle) in [0] then
        begin
          RC:= 0;
          H[1]:= 180;
        end
        else
        if (Abs(ScanChannelByIdx[I].EnterAngle) in [42, 45]) and (Abs(ScanChannelByIdx[I].TurnAngle) = 90) then
        begin
          RC:= 0;
          H[1]:= 40;
        end;


        // ����� ��� �����
        X0:= ScanChannelByIdx[I].Position;
        Y0:= 0;

        for Q := 1 to RC + 1 do
        begin

          if ScanChannelByIdx[I].TurnAngle = 90 then A:= 0
           else A:= ScanChannelByIdx[I].EnterAngle;

          X1:= X0 + Round(H[Q] * Tan((A) * pi / 180));
          if Q = 1 then Y1:= Y0 - H[Q];
          if Q = 2 then Y1:= Y0 + H[Q];
          if Q = 3 then Y1:= Y0 - H[Q];

          Shape:= TChartShape.Create(Chart1_);
          Shape.ParentChart:= Chart1_;
          Shape.Style:= chasLine;
          Shape.Y0:= Y0;
          Shape.Y1:= Y1;

          if ScanChannelByIdx[I].Gran in [_Work, _Work_Left__UnWork_Right] then
          begin
            Shape.X0:= X0 - 2;
            Shape.X1:= X1 - 2;
          end
          else
          if ScanChannelByIdx[I].Gran in [_UnWork, _UnWork_Left__Work_Right] then
          begin
            Shape.X0:= X0 + 2;
            Shape.X1:= X1 + 2;
          end
          else
          begin
            Shape.X0:= X0;
            Shape.X1:= X1;
          end;

  //        if ComboBox2.ItemIndex <> I then Shape.Pen.Style:= psDot;

          Shape.Pen.Width:= PenWidth;

          if PenWidth <> 1 then Shape.Pen.Color:= TAvk11Display(FDisplay).EvalChNumColors2[FDatSrc.Config.ChannelIDList[TMousePosDat(MPD^).Rail, Arr[0] {ScanChannelByIdx[I].Number} ], 15]
///                                               TAvk11Display(FDisplay).EvalChNumColors2[FDatSrc.Config.ChannelIDList[TMousePosDat(FMPD^).Rail, EC.Number], 15];
                           else Shape.Pen.Color:= clBlack;
          ChartList.Add(Shape);

          X0:= X1;
          Y0:= Y1;
        end;

        Shape:= TChartShape.Create(Chart1_);
        Shape.ParentChart:= Chart1_;
        Shape.Style:= chasRectangle;
        Shape.X0:= ScanChannelByIdx[I].Position - PEPSize div 2;
        Shape.Y0:= 0;
        Shape.X1:= ScanChannelByIdx[I].Position + PEPSize div 2;
        Shape.Y1:= PEPSize;
        Shape.Pen.Width:= PenWidth;
  {            Shape.Brush.Color:= clBlack;
        Shape.Brush.Bitmap:= ParentChart:= Solid;
        Shape.Brush.Style:= bsSolid;}
  //            Shape.Text.Text:= IntToStr(Abs(ScanChannelByIdx[I].EnterAngle));
  //            Shape.Font.Height:= - 20;
        ChartList.Add(Shape);
      end;
    end;

  ////////////////////////////////////////////////////////////////////

    StartX:= -230;
    EndX:= 230;
    StartY:= 50;

    Chart2_.BottomAxis.SetMinMax(StartX, EndX);
    Chart2_.LeftAxis.SetMinMax(StartY, StartY - Chart2_.Height * (EndX - StartX) / Chart2_.Width);

    Shape:= TChartShape.Create(Chart2_);
    Shape.ParentChart:= Chart2_;
    Shape.Style:= chasLine;
    Shape.X0:= - 800;
    Shape.X1:= 800;
    Shape.Y0:= 75 / 2;
    Shape.Y1:= 75 / 2;
    ChartList.Add(Shape);

    Shape:= TChartShape.Create(Chart2_);
    Shape.ParentChart:= Chart2_;
    Shape.Style:= chasLine;
    Shape.X0:= - 800;
    Shape.X1:= 800;
    Shape.Y0:= - 75 / 2;
    Shape.Y1:= - 75 / 2;
    ChartList.Add(Shape);

    with TAvk11Display(FDisplay).DatSrc.Config do
    begin
      for I := 0 to ScanChannelCount - 1 do
      begin
        Arr:= GetEvalsChIdxByScanChIdx(I);

        PenWidth:= 1;
        for J := 0 to High(TMousePosDat(FMPD^).ScanChNumList) do
          if TMousePosDat(FMPD^).ScanChNumList[J] = ScanChannelByIdx[I].BS_Ch_File_Number then
          begin
            PenWidth:= 3;
          end;

        if ((Abs(ScanChannelByIdx[I].EnterAngle) = 58) or
            (Abs(ScanChannelByIdx[I].EnterAngle) = 50)) and
           (EvalChannelByIdx[Arr[0]].Method <> imMirror) then
        begin
          RC:= 1;
          H[1]:= 37;
          H[2]:= 74;
        end
        else
        if ((Abs(ScanChannelByIdx[I].EnterAngle) = 58) or
            (Abs(ScanChannelByIdx[I].EnterAngle) = 50)) and
           (EvalChannelByIdx[Arr[0]].Method = imMirror) then
        begin
          RC:= 2;
          H[1]:= 37;
          H[2]:= 16;
          H[3]:= - 20;
        end
        else
        if Abs(ScanChannelByIdx[I].EnterAngle) = 70 then
        begin
          RC:= 0;
          H[1]:= 150;
//          H[1]:= Round(55 / tan(30*pi/180));
        end
        else
        if Abs(ScanChannelByIdx[I].EnterAngle) in [42, 45] then
        begin
          RC:= 0;
          H[1]:= 180;
          // H[2]:= 40;
        end
        else
        if Abs(ScanChannelByIdx[I].EnterAngle) in [0] then
        begin
          RC:= 0;
          H[1]:= 180;
        end;

        X0:= ScanChannelByIdx[I].Position;
        Y0:= ScanChannelByIdx[I].CrossPosition;

        for Q := 1 to RC + 1 do
        begin

          if ScanChannelByIdx[I].TurnAngle <> 0 then
          begin
         {   if (Q = 3) and (EvalChannelByIdx[Arr[0]].Method = imMirror) then
            begin
              if ScanChannelByIdx[I].EnterAngle > 0 then X1:= X0 - Round(H[Q] / Tan((Abs(ScanChannelByIdx[I].TurnAngle) ) * pi / 180));
              if ScanChannelByIdx[I].EnterAngle < 0 then X1:= X0 + Round(H[Q] / Tan((Abs(ScanChannelByIdx[I].TurnAngle) ) * pi / 180));
            end
            else        }
            begin
              if ScanChannelByIdx[I].EnterAngle > 0 then X1:= X0 + Round(H[Q] / Tan((Abs(ScanChannelByIdx[I].TurnAngle) ) * pi / 180));
              if ScanChannelByIdx[I].EnterAngle < 0 then X1:= X0 - Round(H[Q] / Tan((Abs(ScanChannelByIdx[I].TurnAngle) ) * pi / 180));
            end;

            if (ScanChannelByIdx[I].Gran in [_Both, _Work {, _Work_Left__UnWork_Right}]) or
               ((TMousePosDat(MPD^).Rail = rLeft) and (ScanChannelByIdx[I].Gran = _Work_Left__UnWork_Right)) or
               ((TMousePosDat(MPD^).Rail = rRight) and (ScanChannelByIdx[I].Gran = _UnWork_Left__Work_Right)) then
            begin
              if Q = 1 then Y1:= Y0 - H[Q];
              if Q = 2 then Y1:= Y0 + H[Q];
              if Q = 3 then Y1:= Y0 - H[Q];
            end;
            if (ScanChannelByIdx[I].Gran in [_Both, _UnWork {, _UnWork_Left__Work_Right}]) or
               ((TMousePosDat(MPD^).Rail = rRight) and (ScanChannelByIdx[I].Gran = _Work_Left__UnWork_Right)) or
               ((TMousePosDat(MPD^).Rail = rLeft) and (ScanChannelByIdx[I].Gran = _UnWork_Left__Work_Right)) then
            begin
              if Q = 1 then Y1:= Y0 + H[Q];
              if Q = 2 then Y1:= Y0 - H[Q];
              if Q = 3 then Y1:= Y0 + H[Q];
            end;
          end
          else
          if ScanChannelByIdx[I].EnterAngle > 0 then
          begin
            X1:= X0 + H[Q];
            Y1:= Y0;
          end
          else
          if ScanChannelByIdx[I].EnterAngle < 0 then
          begin
            X1:= X0 - H[Q];
            Y1:= Y0;
          end
          else
          if ScanChannelByIdx[I].EnterAngle = 0 then
          begin
            X0:= X0 - 2;
            X1:= X0 + 3;
            Y1:= Y0;
          end;

          Shape:= TChartShape.Create(Chart2_);
          Shape.ParentChart:= Chart2_;
          Shape.Style:= chasLine;
          Shape.X0:= X0;
          Shape.Y0:= Y0;
          Shape.X1:= X1;
          Shape.Y1:= Y1;
          Shape.Pen.Width:= 2;
        //  Shape.Pen.Color:= Random(256*256*256);


          if PenWidth <> 1 then Shape.Pen.Color:= TAvk11Display(FDisplay).EvalChNumColors2[FDatSrc.Config.ChannelIDList[TMousePosDat(MPD^).Rail, Arr[0] { ScanChannelByIdx[I].Number} ], 15]
                           else Shape.Pen.Color:= clBlack;
          Shape.Pen.Width:= PenWidth;
          ChartList.Add(Shape);

          X0:= X1;
          Y0:= Y1;
        end;
      end;

      for I := 0 to ScanChannelCount - 1 do
      begin
        PenWidth:= 1;
        for J := 0 to High(TMousePosDat(FMPD^).ScanChNumList) do
          if TMousePosDat(FMPD^).ScanChNumList[J] = ScanChannelByIdx[I].BS_Ch_File_Number then
          begin
            PenWidth:= 3;
          end;

        Shape:= TChartShape.Create(Chart2_);
        Shape.ParentChart:= Chart2_;
        Shape.Style:= chasCircle;
        Shape.Pen.Width:= PenWidth;
        Shape.X0:= ScanChannelByIdx[I].Position - PEPSize div 2;
        Shape.Y0:= ScanChannelByIdx[I].CrossPosition - PEPSize div 2;
        Shape.X1:= ScanChannelByIdx[I].Position + PEPSize div 2;
        Shape.Y1:= ScanChannelByIdx[I].CrossPosition + PEPSize div 2;
        Shape.Pen.Width:= PenWidth;
        ChartList.Add(Shape);

{       Shape:= TChartShape.Create(Chart2_);
        Shape.ParentChart:= Chart2_;
        Shape.Style:= chasRectangle;;
        Shape.Pen.Width:= PenWidth;
        Shape.X0:= ScanChannelByIdx[I].Position - 55;
        Shape.Y0:= - 70 / 2;
        Shape.X1:= ScanChannelByIdx[I].Position + 55;
        Shape.Y1:= 70 / 2;
        Shape.Pen.Width:= PenWidth;
        ChartList.Add(Shape); }


      end;
    end;
    StringGrid1.Refresh;
  end;
  Result:= True;
end;

procedure TLupaForm.Chart2MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Self.Visible:= False;
end;

procedure TLupaForm.Chart2MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  Self.Visible:= False;
end;

procedure TLupaForm.Chart2MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Self.Visible:= False;
end;

procedure TLupaForm.FormMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  if WheelDelta < 0 then FHalfDispWidth:= Max(30, FHalfDispWidth - 25);
  if WheelDelta > 0 then FHalfDispWidth:= Min(350, FHalfDispWidth + 25);
  Config.LupaZoom:= FHalfDispWidth;
  CreateImage;
end;

procedure TLupaForm.SetPos;
var
  MP: TPoint;
  NewLeft, NewTop: Integer;

begin
  GetCursorPos(MP);
  if MP.X + 35 + Width >= Screen.Width then NewLeft:= MP.X - 35 - Width
                                       else NewLeft:= MP.X + 35;
  if MP.Y + 35 + Height >= Screen.Height then NewTop:= Screen.Height - Height
                                         else NewTop:= MP.Y + 35;

  if (Abs(NewLeft - Left) > 30) or (Abs(NewTop - Top) > 30) then
  begin
//    Self.Visible:= False;
    Left:= NewLeft;
    Top:= NewTop;
//    Self.Visible:= True;
  end;


{
  GetCursorPos(MP);
  if MP.X + 35 + Width >= Screen.Width then Left:= MP.X - 35 - Width
                                       else Left:= MP.X + 35;
  if MP.Y + 35 + Height >= Screen.Height then Top:= Screen.Height - Height
                                         else Top:= MP.Y + 35;
}
end;
procedure TLupaForm.StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  tf: TTextFormat;
  s: string;
  EC: TEvalChannelItem;
  SC: TScanChannelItem;
  I, J, Line: Integer;
  tmp: TIntegerDynArray;
  EvalChList: TIntegerDynArray;
  Par: TMiasParams;

function GetSurfaceText(Rail: TRail; Gran: TGran): string;
begin
  case Gran of
                      _None: Result:= '';
                      _Work: Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT'];
                    _UnWork: Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
   _Work_Left__UnWork_Right: if Rail = rLeft then Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT']
                                             else Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
   _UnWork_Left__Work_Right: if Rail = rLeft then Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT']
                                             else Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT'];
  end;
//  Result:= Result + IntToStr(Ord(Rail)) + IntToStr(Ord(Gran));
end;

begin

  if not Assigned(FMPD) then Exit;


  if (ARow = 0) then
  begin
    case ACol of
       0: if FDatSrc.isEGOUSW_BHead or FDatSrc.isVMT_US_BigWP_BHead
            then S:= LangTable.Caption['Common:ChannelParams_SHORT:Direction_B']  // ��
            else S:= LangTable.Caption['Common:ChannelParams_SHORT:Direction_A']; // ����
   //    0: S:= LangTable.Caption['Common:ChannelParams_SHORT:Direction'];    { ����������� }
       1: S:= LangTable.Caption['Common:ChannelParams_SHORT:Angles'];       { ����/�����  }
       2: S:= LangTable.Caption['Common:ChannelParams_SHORT:Method'];       { �����       }
       3: S:= LangTable.Caption['Common:ChannelParams_SHORT:Zone'];         { ����        }
       4: S:= LangTable.Caption['Common:ChannelParams_SHORT:Ku'];           { ��          }
       5: S:= LangTable.Caption['Common:ChannelParams_SHORT:Att'];          { ���         }
       6: S:= LangTable.Caption['Common:ChannelParams_SHORT:TVG'];          { ���         }
       7: S:= LangTable.Caption['Common:ChannelParams_SHORT:PrismDelay'];   { 2��         }
    end;
    StringGrid1.Canvas.Font.Color:= clBlack;
    StringGrid1.Canvas.TextRect(Rect, s, [tfCenter, tfVerticalCenter]);
  end
  else
  begin
    SetLength(EvalChList, 0);
    for Line := 0 to High(TMousePosDat(FMPD^).ScanChNumList) do
    begin
       tmp:= FDatSrc.Config.GetEvalsChIdxByScanChNum(TMousePosDat(FMPD^).ScanChNumList[Line]);
       for I := 0 to High(tmp) do
       begin
         SetLength(EvalChList, Length(EvalChList) + 1);
         EvalChList[High(EvalChList)]:= FDatSrc.Config.EvalChannelByIdx[tmp[I]].Number;
       end;
    end;

    if ARow - 1 <= High(EvalChList) then
//    for Line := 0 to High(EvalChList) do
    begin
      Line := ARow - 1;
      EC:= FDatSrc.Config.EvalChannelByNum[EvalChList[Line]];
      SC:= FDatSrc.Config.ScanChannelByNum[EC.ScanChNum];

      FDatSrc.GetParamFirst(TMousePosDat(FMPD^).MouseDisCoord, Par);   // ��������

      case ACol of
         0: begin { ����������� }

             if not (FDatSrc.isEGOUSW or FDatSrc.isVMT_US_BigWP) then
             begin
               if SC.EnterAngle > 0 then S:= LangTable.Caption['Common:ZoomInChannel_SHORT'];
               if SC.EnterAngle < 0 then S:= LangTable.Caption['Common:ZoomOutChannel_SHORT'];
               if SC.EnterAngle = 0 then S:= '-';
             end
             else
               if EGOUSW_WP_Data[SC.BS_Ch_File_Number] = 0 then S:= 'A' else S:= 'B';
             {
              if SC.EnterAngle > 0 then S:= LangTable.Caption['Common:ZoomInChannel_SHORT'];
              if SC.EnterAngle < 0 then S:= LangTable.Caption['Common:ZoomOutChannel_SHORT'];
              if SC.EnterAngle = 0 then S:= '-' + GetSurfaceText(TMousePosDat(FMPD^).Rail, SC.Gran);
              }
            end;
         1: begin { ����/�����  } // %d
              if SC.TurnAngle <> 0 then
              begin
                if GetSurfaceText(TMousePosDat(FMPD^).Rail, SC.Gran) = ''
                  then S:= Format('%d/%d', [Abs(SC.EnterAngle), Abs(SC.TurnAngle)])
                  else S:= Format('%d/%s', [Abs(SC.EnterAngle), GetSurfaceText(TMousePosDat(FMPD^).Rail, SC.Gran)]);
              end
              else S:= Format('%d', [Abs(SC.EnterAngle)]);
            end;
         2: S:= LangTable.Caption['Common:' + GetInspMethodTextId(EC.Method, False)]; { �����       }
         3: S:= LangTable.Caption['Common:' + GetInspZoneTextId(EC.Zone, False)]; { ����       }
         4: S:= IntToStr(Par.Par[TMousePosDat(FMPD^).Rail, EC.Number].Ku);                        { ��          }
         5: S:= IntToStr(Par.Par[TMousePosDat(FMPD^).Rail, EC.Number].Att);                       { ���         }
         6: S:= IntToStr(Par.Par[TMousePosDat(FMPD^).Rail, EC.Number].TVG);                       { ���         }
         7: S:= Format('%3.1f', [Par.Par[TMousePosDat(FMPD^).Rail, EC.Number].PrismDelay / 10]);  { 2��         }
      end;
    end;

    StringGrid1.Canvas.Font.Color:= TAvk11Display(FDisplay).EvalChNumColors2[FDatSrc.Config.ChannelIDList[TMousePosDat(FMPD^).Rail, EC.Number], 15];
    StringGrid1.Canvas.TextRect(Rect, s, [tfCenter, tfVerticalCenter]);
  end;


end;

{
procedure TLupaForm.MakeMScheme(Sender: TObject);
var
  I, J, X, X0, Y0, Y1, S: Integer;
  Rt: TRect;
  Text: string;
  F: Boolean;

begin
  X0:= 170;
  Y1:= 20;
  Y0:= 120;
  S:= 20;

  with PaintBox1.Canvas do
      with TAvk11Display(FDisplay).DatSrc.Config do
      begin
        Brush.Color:= clWhite;
        FillRect(Rect(0, 0, PaintBox1.Width, PaintBox1.Height));
        Pen.Color:= clBlack;

        MoveTo(10, 20);
        LineTo(100, 20);
        LineTo(100 - 10, 20 - 10);
        MoveTo(100, 20);
        LineTo(100 - 10, 20 + 10);


//        MoveTo((Rt.Right + Rt.Left) div 2, Rt.Bottom);
//        LineTo(Round((Rt.Right + Rt.Left) / 2 + 100 * cos((ScanChannelByIdx[I].EnterAngle-90) * pi / 180)),

        for I := 0 to ScanChannelCount - 1 do
        begin
          F:= False;
          for J := 0 to High(TMousePosDat(FMPD^).ScanChNumList) do
            if TMousePosDat(FMPD^).ScanChNumList[J] = ScanChannelByIdx[I].Number then
            begin
              F:= True;
              Break;
            end;

          if F then Pen.Width:= 4
               else Pen.Width:= 1;

          X:= X0 + ScanChannelByIdx[I].Position;
          Rt:= Rect(X, Y0, X + S, Y0 + S);
          Rectangle(Rt);
          Text:= IntToStr(ScanChannelByIdx[I].EnterAngle);
          TextOut(Rt.Left + (Rt.Right - Rt.Left - TextWidth(Text)) div 2, Rt.Top + (Rt.Bottom - Rt.Top - TextHeight(Text)) div 2, Text);

          MoveTo((Rt.Right + Rt.Left) div 2, Rt.Bottom);
          LineTo(Round((Rt.Right + Rt.Left) / 2 + 100 * cos((ScanChannelByIdx[I].EnterAngle-90) * pi / 180)),
                 Round(Rt.Bottom                 - 100 * sin((ScanChannelByIdx[I].EnterAngle-90) * pi / 180)));




          Ellipse(X, Y1, X + S, Y1 + S);

          MoveTo(X + S div 2, Y1 + S div 2);
          LineTo(Round((Rt.Right + Rt.Left) / 2 + 100 * cos((ScanChannelByIdx[I].TurnAngle-90) * pi / 180)),
                 Round(Rt.Bottom                 - 100 * sin((ScanChannelByIdx[I].TurnAngle-90) * pi / 180)));

        end;
      end;

end;
}
procedure TLupaForm.FormCreate(Sender: TObject);
begin
  KillSelection(StringGrid1);
  Mode:= 0;
  LastMode:= - 1;
  FHalfDispWidth:= Config.LupaZoom;

  Self.Font.Name              := LangTable.Caption['General:FontName'];

  Chart2.LeftAxis.Title.Caption:= LangTable.Caption['Common:us'];
  Chart2.BottomAxis.Title.Caption:= LangTable.Caption['Common:mm'];
  Chart1.LeftAxis.Title.Caption:= LangTable.Caption['Common:dB'];

  StaticText4.Caption         := LangTable.Caption['InfoBar:Zoomin:'];  // ����������:
  StaticText5.Caption         := LangTable.Caption['InfoBar:Zoomout:'];  // �����������:
  StaticText2.Caption         := LangTable.Caption['InfoBar:Zoomin:'];  // ����������:
  StaticText1.Caption         := LangTable.Caption['InfoBar:Zoomout:'];  // �����������:
  Panel31.Caption             := LangTable.Caption['Common:dB'];  // ��
  Panel32.Caption             := LangTable.Caption['Common:dB'];  // ��

  Panel6.Caption             := LangTable.Caption['Common:dB'];  // ��
  Panel8.Caption             := LangTable.Caption['Common:dB'];  // ��

  Panel29.Caption             := LangTable.Caption['Common:us'];  // ���
  Panel30.Caption             := LangTable.Caption['Common:us'];  // ���

  Panel7.Caption             := LangTable.Caption['Common:us'];  // ���
  Panel9.Caption             := LangTable.Caption['Common:us'];  // ���

  Panel12.Caption             := LangTable.Caption['Common:WorkSurf_SHORT'];  // ���
  Panel11.Caption             := LangTable.Caption['Common:UnWorkSurf_SHORT'];  // ���

  ChartList:= TList.Create;
end;

procedure TLupaForm.FormDestroy(Sender: TObject);
begin
  DeleteShapes;

end;

procedure TLupaForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  Chart2.Title.Text.Text:= IntToStr(Msg.CharCode);
  case Msg.CharCode of
    40: FHalfDispWidth:= Max(30, FHalfDispWidth - 25);
    38: FHalfDispWidth:= Min(350, FHalfDispWidth + 25);
  end;
  Config.LupaZoom:= FHalfDispWidth;
  CreateImage;
end;

procedure TLupaForm.ClearAScanElements;
var
  Chart: TChart;
  ScanIdx, ElementIdx: Integer;

begin
  for ScanIdx := 1 to 4 do
  begin

    case ScanIdx of
      1: Chart:= AScan1Chart;
      2: Chart:= AScan2Chart;
      3: Chart:= AScan3Chart;
      4: Chart:= AScan4Chart;
    end;

    for ElementIdx:= 0 to High(FAScanElements[ScanIdx]) do
      if Assigned(FAScanElements[ScanIdx, ElementIdx]) then
      begin
        FAScanElements[ScanIdx][ElementIdx].Free;
        FAScanElements[ScanIdx][ElementIdx]:= nil;
      end;

    SetLength(FAScanElements[ScanIdx], 2);
    FAScanElements[ScanIdx][0]:= TChartShape.Create(Chart);
    FAScanElements[ScanIdx][0].ParentChart:= Chart;
    FAScanElements[ScanIdx][0].Style:= chasTriangle;
    FAScanElements[ScanIdx][0].Brush.Color:= clBlack;
    FAScanElements[ScanIdx][0].X0:= 0;
    FAScanElements[ScanIdx][0].X1:= 0;
    FAScanElements[ScanIdx][0].Y0:= 0;
    FAScanElements[ScanIdx][0].Y1:= 0;

    FAScanElements[ScanIdx][1]:= TChartShape.Create(Chart);
    FAScanElements[ScanIdx][1].ParentChart:= Chart;
    FAScanElements[ScanIdx][1].Style:= chasHorizLine;
    FAScanElements[ScanIdx][1].Pen.Color:= clBlack;
    FAScanElements[ScanIdx][1].Pen.Style:= psDash;
    FAScanElements[ScanIdx][1].Pen.Width:= 3;
    FAScanElements[ScanIdx][1].X0:= 0;
    FAScanElements[ScanIdx][1].X1:= 1000;
    if Assigned(FDisplay) then
    begin
      FAScanElements[ScanIdx][1].Y0:= FDatSrc.Config.AmplValue[TAvk11Display(FDisplay).AmplTh];
      FAScanElements[ScanIdx][1].Y1:= FDatSrc.Config.AmplValue[TAvk11Display(FDisplay).AmplTh];
    end;
  end;
end;

procedure TLupaForm.AddAScanElement(Chart: TChart; ScanIdx: Integer; Delay: Extended; Ampl: Integer; Color: TColor; Percent: Integer = 100);
var
  R1, G1, B1: Integer;
  R2, G2, B2: Integer;
  CR, CG, CB: Integer;
  CurCol: Integer;
  Fon: TColor;

begin

  SetLength(FAScanElements[ScanIdx], Length(FAScanElements[ScanIdx]) + 1);

  Fon:= Ascan1Chart.Color;

  R1:= (Fon and $0000FF) shr 0;
  G1:= (Fon and $00FF00) shr 8;
  B1:= (Fon and $FF0000) shr 16;

  R2:= (ColorToRGB(Color) and $0000FF) shr 0;
  G2:= (ColorToRGB(Color) and $00FF00) shr 8;
  B2:= (ColorToRGB(Color) and $FF0000) shr 16;

  CR:= Round(R1 + (R2 - R1) * Percent / 100);
  CG:= Round(G1 + (G2 - G1) * Percent / 100);
  CB:= Round(B1 + (B2 - B1) * Percent / 100);

  Color:= CR + CG * $100 + CB * $10000;

  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])]:= TChartShape.Create(Chart);
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].ParentChart:= Chart;
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Style:= chasLine;
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Color:= Color;
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Brush.Style:= bsSolid;
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Brush.Color:= Color;
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Pen.Color:= Color;
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Pen.Width:= 3;
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].X0:= Round(Delay {- Ampl / 10});
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].X1:= Round(Delay {+ Ampl / 10});
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Y0:= - 12;
  FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Y1:= {DBValue[}Ampl{]};

end;

procedure TLupaForm.SetAScanDelayZone(Chart: TChart; Min1, Max1: Integer);
begin
  Chart.BottomAxis.SetMinMax(Min1, Max1);
end;

procedure TLupaForm.EndAScan;
var
  ScanIdx, ElementIdx: Integer;
  I: Integer;
  Chart: TChart;

begin
  for ScanIdx := 1 to 4 do
  begin
    SetLength(FAScanElements[ScanIdx], Length(FAScanElements[ScanIdx]) + 1);

    case ScanIdx of
      1: Chart:= AScan1Chart;
      2: Chart:= AScan2Chart;
      3: Chart:= AScan3Chart;
      4: Chart:= AScan4Chart;
    end;

  //  SetLength(AScan1, Length(AScan1) + 1);
    FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])]:= TChartShape.Create(Chart);
    FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].ParentChart:= Chart;
    FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Style:= chasHorizLine;
    FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Brush.Color:= clLime;
    FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Pen.Color:= clLime;
    FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].Pen.Width:= 3;
    FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].X0:= Ascan1Chart.BottomAxis.Minimum;
    FAScanElements[ScanIdx, High(FAScanElements[ScanIdx])].X1:= Ascan1Chart.BottomAxis.Maximum;
  end;
  //  AScan1[High(AScan1)].Y0:= DBValue[AmplThTrackBarPosition];
  //  AScan1[High(AScan1)].Y1:= DBValue[AmplThTrackBarPosition];
(*
  SetLength(AScan2, Length(AScan2) + 1);
  AScan2[High(AScan2)]:= TChartShape.Create(Ascan2Chart);
  AScan2[High(AScan2)].ParentChart:= Ascan2Chart;
  AScan2[High(AScan2)].Style:= chasHorizLine;
  AScan2[High(AScan2)].Brush.Color:= clLime;
  AScan2[High(AScan2)].Pen.Color:= clLime;
  AScan2[High(AScan2)].Pen.Width:= 3;
  AScan2[High(AScan2)].X0:= Ascan2Chart.BottomAxis.Minimum;
   AScan2[High(AScan2)].X1:= Ascan2Chart.BottomAxis.Maximum; *)
//  AScan2[High(AScan2)].Y0:= DBValue[AmplThTrackBarPosition];
//  AScan2[High(AScan2)].Y1:= DBValue[AmplThTrackBarPosition];
end;

end.





