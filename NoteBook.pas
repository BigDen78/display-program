﻿{$I DEF.INC}
unit NoteBook; {Language 3}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Grids, ComCtrls, Buttons, Printers, ImgList,
  CreateReportClass, LanguageUnit, Jpeg, TB2Item, TB2Dock, TB2Toolbar,
  TB2ExtItems, {FR_Class,} PGBarExport, GIFImage, OleServer, Clipbrd,
  Variants, AviconTypes, ShellApi, JCLUnicode, VPDFDoc;

type
  TNoteBookForm = class(TForm)
    ListView1: TListView;
    Panel10: TPanel;
    TBDock1: TTBDock;
    MainToolbar: TTBToolbar;
    SpeedButton1: TTBItem;
    SpeedButton41: TTBItem;
    ScrollBar1: TScrollBar;
    ImageList2: TImageList;
    Panel2: TPanel;
    Panel1: TPanel;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem1: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBItem4: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    Label1: TLabel;
    Bevel1: TBevel;
    Splitter1: TSplitter;
    NordcoPanel: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    LineLabel: TLabel;
    PathLabel: TLabel;
    Edit1_: TEdit;
    GPSStLatEdit: TEdit;
    GPSStLonEdit: TEdit;
    GPSEdLatEdit: TEdit;
    GPSEdLonEdit: TEdit;
    CommentEdit: TEdit;
    ComboBox1: TComboBox;
    SizeEdit: TEdit;
    Size2Edit: TEdit;
    Size3Edit: TEdit;
    CodeDefectEdit: TComboBox;
    LineEdit: TEdit;
    PathEdit: TEdit;
    SourceLabel: TLabel;
    SourceEdit: TEdit;
    StatusLabel: TLabel;
    StatusEdit: TEdit;
    StandartPanel: TPanel;
    Label4: TLabel;
    Label17: TLabel;
    Label6: TLabel;
    PosEdit: TEdit;
    Memo: TMemo;
    NameEdit: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CreateCells(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Panel3Resize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure PosEditKeyPress(Sender: TObject; var Key: Char);
    procedure NameEditKeyPress(Sender: TObject; var Key: Char);
    procedure Panel2Resize(Sender: TObject);
    procedure Panel8Resize(Sender: TObject);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure Edit7Change(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure DateEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure TabSheet2Resize(Sender: TObject);
    function GenerateReport(ViewPer: Integer): Boolean;
    procedure OnChangeLanguage(Sender: TObject);
    procedure SpeedButton41Click(Sender: TObject);
    procedure TBItem2Click(Sender: TObject);
    procedure TBItem3Click(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure ListView1DblClick(Sender: TObject);
    procedure TBItem4Click(Sender: TObject);
  private
    GetIndex: Integer;
//    Rpt: TCreateReportClass;

//    procedure ExportToJpgOnePage(Rep: TfrReport; PageNum :Integer; FileName :String; M, JpgQuality: Integer; ToClipBoard: Boolean = False);
//    procedure ExportToJpg(Rep: TfrReport; FileName: string; M, JpgQuality :Integer; Clipbrd: Boolean = False);

  public
    MyOwner: TForm;
    WaitFlag: Boolean;
    Buffer: TBitMap;
    SaveIndex: Integer;

    procedure RefreshData;
    procedure PutData(Index: Integer);
    procedure GetData(Index: Integer; Flag: Boolean = True);
    procedure Select(Index: Integer);
    procedure StoryChange;
  end;

implementation

uses
  NewEntry, MainUnit, Avk11ViewUnit, DisplayUnit, MyTypes, ConfigUnit, CommonUnit;

{$R *.DFM}

procedure TNoteBookForm.FormCreate(Sender: TObject);
begin
  Visible:= False;
  Panel8Resize(nil);
  GetIndex:= - 1;
//  TabSheet2.TabVisible:= False;

//  PosEdit.Enabled:= False;
  NameEdit.Enabled:= False;
  Memo.Enabled:= False;

  OnChangeLanguage(nil);
end;
                        {
procedure TNoteBookForm.ExportToJpg(Rep: TfrReport; FileName: string; M, JpgQuality :Integer; Clipbrd: Boolean = False);
var
  I: Integer;
  CurrentName: string;
  GPExportForm: TGPExportForm;

begin
  GPExportForm:= TGPExportForm.Create(nil);
  GPExportForm.Visible:= True;
  GPExportForm.BringToFront;
  GPExportForm.Caption:= LanguageUnit.LangTable.Caption['USB:Pleasewait...'];
  for I:=0 to Rep.Pages.Count - 1 Do
     begin
       CurrentName:=Format(FileName,[i+1]);
       ExportToJpgOnePage(Rep, i, CurrentName, M, JpgQuality, Clipbrd);
       GPExportForm.ProgressBar1.Position:= 50 + Round(((I + 1) / Rep.Pages.Count) * 50);
     end;
  GPExportForm.Free;
end;
                         } (*
procedure TNoteBookForm.ExportToJpgOnePage(Rep: TfrReport; PageNum: Integer; FileName: String; M ,JpgQuality: Integer; ToClipBoard: Boolean = False);
Const
    Lh = 0;// расстояние между строчками
    LeftField = 1;
    RightField = 1;
    TopField = 1;
    BottomField = 1;
    MaxWords = 20;
    PopravkaNaPolyaLeftA = 92; // Чтобы убрать левое поле поставить цифру 90
    PopravkaNaPolyaLeftB = 25;
    PopravkaNaPolyaUpA = 25;
    PopravkaNaPolyaUpB = 58;

Var
  Bmp                   :TBitMap;
  Jpg                   :TJPEGImage;
  i,j,k,TextX, TextY    :Integer;
  th,DeltaY,tw,TempH    :Integer;
  tx,ty,sn              :Integer;
  PopravkaNaPolyaLeft    :integer;
  PopravkaNaPolyaUp      :integer;
  h1,h2                 :Real;
  Obj                   :TfrView;
  ObjM                  :TfrMemoView;
  ObjL                  :TfrLineView;
  ObjP                  :TfrPictureView;
  r,rt                  :TRect;
  TempList              :TStringList;
  St1,St2               :String;
  LF                    :TLogFont;
  Fnt                   :HFont;
  Words                 :Array[0..MaxWords] of String[30];
  WordsCount,a,b        :Integer;
  Gif                   :TGifImage;

Begin
  Bmp:= TBitMap.Create;
  i:=PageNum;
  Bmp.Width:=LeftField;
  Bmp.Height:=TopField;
  For j:=0 to Rep.Pages[i].Objects.Count - 1 do
     Begin
       Obj:=Rep.Pages[i].Objects[j];
       // Если объект - мемо
       if Obj is TfrMemoView then
         Begin
           ObjM:= Obj as TfrMemoView;
           // Заносим в r размер прямоугольника в коором будет текст
           If ((i+1)=Rep.Pages.Count) and (TBItem1.Checked) Then
             Begin
               PopravkaNaPolyaLeft:=PopravkaNaPolyaLeftB;
               PopravkaNaPolyaUp:=PopravkaNaPolyaUpB;
             End
           Else
             Begin
               PopravkaNaPolyaLeft:=PopravkaNaPolyaLeftA;
               PopravkaNaPolyaUp:=PopravkaNaPolyaUpA;
             End;
           r.Left:=(ObjM.x-PopravkaNaPolyaLeft)*M;
           r.Top:=(ObjM.y-PopravkaNaPolyaUp)*M;
           r.Right:=(ObjM.x-PopravkaNaPolyaLeft)*M+ObjM.dx*M;
           r.Bottom:=(ObjM.y-PopravkaNaPolyaUp)*M+ObjM.dy*M;
           // Изменяем размер Bmp если это требуется
           tx:=r.Right-(Bmp.Width-LeftField);
           If tx>0 then
             Bmp.Width:=Bmp.Width+tx;
           ty:=r.Bottom-(Bmp.Height-TopField);
           If ty>0 then
             Bmp.Height:=Bmp.Height+ty;
           // Рисуем линии которые учавствуют в рамке вокруг текста
           If ObjM.FrameTyp and 8 = 8 then
             Begin
               Bmp.Canvas.MoveTo(r.Left,r.Top);
               Bmp.Canvas.LineTo(r.Right,r.Top);
             End;
           If ObjM.FrameTyp and 4 = 4 then
             Begin
               Bmp.Canvas.MoveTo(r.Left,r.Top);
               Bmp.Canvas.LineTo(r.Left,r.Bottom);
             End;
           If ObjM.FrameTyp and 1 = 1 then
             Begin
               Bmp.Canvas.MoveTo(r.Right,r.Top);
               Bmp.Canvas.LineTo(r.Right,r.Bottom);
             End;
           If ObjM.FrameTyp and 2 = 2 then
             Begin
               Bmp.Canvas.MoveTo(r.Left,r.Bottom);
               Bmp.Canvas.LineTo(r.Right,r.Bottom);
             End;
           // Устанавливаем параметры шрифта с учетом того что текст может быть повернут
           Bmp.Canvas.Font:=ObjM.Font;
           Bmp.Canvas.Font.Size:=Round(Bmp.Canvas.Font.Size*(M*0.9));
           // Проверяем влезает ли строчка в ячейку, если нет, то переформировываем лист

           // Новый алгоритм !!!!

           // Сперва находим все слова в строке
           If ObjM.Memo.Count>0 then
             Begin
               tw:=Bmp.Canvas.TextWidth(ObjM.Memo.Strings[0]);
               If tw>ObjM.dx*M then
                 Begin
                   For a:=0 to MaxWords Do
                      Words[a]:='';
                   WordsCount:=1;
                   St1:='';
                   St1:=ObjM.Memo.Strings[0];
                   For a:=1 to Length(St1) Do
                      Begin
                        If St1[a]=' ' then
                          Inc(WordsCount)
                        Else
                        Words[WordsCount-1]:=Words[WordsCount-1]+St1[a];
                      End;
                   // На основе найденных слов и заданной ширины ячейки формируем лист
                   TempList:=TStringList.Create;
                   St1:=Words[0];
                   For a:=1 to WordsCount - 1 Do
                      Begin
                        tw:=Bmp.Canvas.TextWidth(St1+' '+Words[a]);
                        If tw>ObjM.dx*M then
                          Begin
                            TempList.Add(St1);
                            St1:=Words[a];
                          End
                        Else
                          St1:=St1+' '+Words[a];
                      End;
                   TempList.Add(St1);
                   // переписываем новый список в объект
                   ObjM.Memo.Clear;
                   TempH:=0;
                   For k:=0 to TempList.Count - 1 Do
                      Begin
                        th:=Bmp.Canvas.TextHeight(TempList.Strings[k]);
                        TempH:=TempH+th;
                        If TempH<ObjM.dy*M then
                          ObjM.Memo.Add(TempList.Strings[k]);
                      End;
                   TempList.Free;
                   // переписываем новый список в объект (старый алгоритм)
                   {ObjM.Memo.Clear;
                   For k:=0 to TempList.Count - 1 Do
                      ObjM.Memo.Add(TempList.Strings[k]);
                   TempList.Free; }
                   St1:='';
                   St2:='';
                 End;
             End;
           // Старый алгоритм !
           {If ObjM.Memo.Count>0 then
             Begin
               tw:=Bmp.Canvas.TextWidth(ObjM.Memo.Strings[0]);
               If tw>ObjM.dx*M then
                 Begin
                   St1:=ObjM.Memo.Strings[0];
                   St2:=St1[1];
                   TempList:=TStringList.Create;
                   For k:=2 to Length(St1) do
                      Begin
                        If St1[k]=' ' then
                          Begin
                            TempList.Add(St2);
                            St2:='';
                          End
                        Else
                          St2:=St2+St1[k];
                      End;
                   TempList.Add(St2);
                   // переписываем новый список в объект
                   ObjM.Memo.Clear;
                   For k:=0 to TempList.Count - 1 Do
                      ObjM.Memo.Add(TempList.Strings[k]);
                   TempList.Free;
                 End;
                 St1:=ObjM.Memo.Strings[0];
             End;}
           // Расчитываем координты текста исходя из количества строк в мемо
           // и атрибуда "по центру"
           // сперва расчитаем выравнивание текста по высоте
           If ObjM.Adjust and 4 <> 4 then // Если тектс ориентирован нормально
             Begin
               // узнаем не вылезает ли текст из ячейки по высоте
               If ObjM.Memo.Count>0 then
                 th:=Bmp.Canvas.TextHeight(ObjM.Memo.Strings[0]);
               TempH:=0;
               For k:=0 to ObjM.Memo.Count - 1 Do
                  TempH:=TempH+th+Lh;
               If TempH>ObjM.dy*M then
                 Begin
                   TempList:=TStringList.Create;
                   For k:=0 to ObjM.Memo.Count - 1 Do
                      TempList.Add(ObjM.Memo.Strings[k]);
                   ObjM.Memo.Clear;
                   TempH:=0;
                   For k:=0 to TempList.Count - 1 Do
                      If (TempH+th)<ObjM.dy*M then
                          Begin
                            TempH:=TempH+th;
                            ObjM.Memo.Add(TempList.Strings[k]);
                          End;
                   TempList.Free;
                 End;
               TextY:=(ObjM.y-PopravkaNaPolyaUp)*M+Round((ObjM.dy*M-(TempH-Lh))/2);
               // теперь рисуем строчки, при этом выравнивания их по ширине
               For k:=0 to ObjM.Memo.Count - 1 Do
                  Begin
                    // проверяем а надо ли выравнивать по ширине
                    If ObjM.Adjust and 2 = 2 then
                      Begin
                        tw:=Bmp.Canvas.TextWidth(ObjM.Memo.Strings[k]);
                        TextX:=(ObjM.x-PopravkaNaPolyaLeft)*M+Round((ObjM.dx*M-tw)/2);
                      End
                    Else
                      TextX:=(ObjM.x-PopravkaNaPolyaLeft)*M+2;
                    Bmp.Canvas.TextOut(TextX,TextY+(th*k),ObjM.Memo.Strings[k]);
                  End;
             End
           Else // То есть если текст направлен под углом
             Begin
               // Создаем новый шрифт
               FillChar(LF, SizeOf(LF), 0) ;
               with LF do
                 begin
                   lfHeight := Round(((-ObjM.Font.Size/72)*ObjM.Font.PixelsPerInch)*(M*0.9));
                   lfWeight := fw_Normal;
                   If ObjM.Font.Style = [fsBold] then
                     lfWeight := fw_Bold;
                   lfUnderline := 0;
                   lfEscapement := 900;
                   StrPCopy(lfFaceName, ObjM.Font.Name);
                 end;
               Fnt := CreateFontIndirect(LF) ;
               Bmp.Canvas.Font.Handle := Fnt;
               // Просчитываем сначала по горизонтали
               If ObjM.Memo.Count>0 then
                 th:=Bmp.Canvas.TextHeight(ObjM.Memo.Strings[0]);
               TempH:=0;
               For k:=0 to ObjM.Memo.Count - 1 Do
               TempH:=TempH+th+Lh;
               TextX:=(ObjM.x-PopravkaNaPolyaLeft)*M+Round((ObjM.dx*M-(TempH-Lh))/2);
               // теперь рисуем строчки, при этом выравнивания их по ширине
               For k:=0 to ObjM.Memo.Count - 1 Do
                  Begin
                    // проверяем а надо ли выравнивать по ширине
                    If ObjM.Adjust and 2 = 2 then
                      Begin
                        tw:=Bmp.Canvas.TextWidth(ObjM.Memo.Strings[k]);
                        TextY:=(ObjM.y-PopravkaNaPolyaUp)*M+Round((ObjM.dy*M-tw)/2)+tw;
                      End
                    Else
                      TextX:=(ObjM.y-PopravkaNaPolyaUp)*M+2;
                    Bmp.Canvas.TextOut(TextX+(th*k),TextY,ObjM.Memo.Strings[k]);
                    //Bmp.Canvas.TextRect(rt,TextX+(th*k),TextY,ObjM.Memo.Strings[k]);
                  End;
              DeleteObject(Fnt);
             End;
         End;
       {// Если объект линия
       If Obj is TfrLineView then
         Begin
           ObjL:= Obj as TfrLineView;
           Bmp.Canvas.MoveTo(ObjL.x,ObjL.y);
           Bmp.Canvas.LineTo(ObjL.x+ObjL.dx,ObjL.y+ObjL.dy);
         End; }
       // Если объект картинка
       If Obj is TfrPictureView then
         Begin
           ObjP:= Obj as TfrPictureView;
           // Заносим в r размер картинки в прямоугольник
           r.Left:=(ObjP.x-PopravkaNaPolyaLeft)*M;
           r.Top:=(ObjP.y-PopravkaNaPolyaUp)*M;
           r.Right:=(ObjP.x-PopravkaNaPolyaLeft)*M+ObjP.dx*M;
           r.Bottom:=(ObjP.y-PopravkaNaPolyaUp)*M+ObjP.dy*M;
           // Изменяем размер Bmp если требуется
           tx:=r.Right-(Bmp.Width-LeftField);
           If tx>0 then
             Bmp.Width:=Bmp.Width+tx;
           ty:=r.Bottom-(Bmp.Height-TopField);
           If ty>0 then
             Bmp.Height:=Bmp.Height+ty;
           Bmp.Canvas.StretchDraw(r,ObjP.Picture.Graphic);
         End;
     End;
  Bmp.Width:=Bmp.Width+RightField;
  Bmp.Height:=Bmp.Height+BottomField;

  if ToClipBoard then
  begin
    Clipboard.Assign(Bmp);
  end
  else
  begin
    Gif:= TGIFImage.Create;
    Gif.Assign(Bmp);
    Gif.SaveToFile(FileName);
    Gif.Free;
  end;
{  Jpg:=TJPEGImage.Create;
  Jpg.Assign(Bmp);
  Jpg.CompressionQuality:=JpgQuality;
  Jpg.Compress;
  Jpg.SaveToFile(FileName);
  Jpg.Free;}

  Bmp.Free;
End;
               *)
procedure TNoteBookForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if GetIndex <> - 1 then PutData(GetIndex);

  ListView1.OnSelectItem:= nil;
//  TAvk11ViewForm(MyOwner).NoteBookForm:= nil;
  Self.OnActivate:= nil;
  Self.OnResize:= nil;
  Self.OnShow:= nil;
//  Self.Panel8.OnResize:= nil;
//  Self.TabSheet2.OnResize:= nil;
  Self.Release;
end;

procedure TNoteBookForm.CreateCells(Sender: TObject);
var
  I: Integer;
  ListItem: TListItem;
  Crd: TCrdParams;

  RC: TRealCoord;
  CrdParams: TCrdParams;

begin
  ListView1.Items.Clear;
  with TAvk11ViewForm(MyOwner).DatSrc do
    for I:= 0 to NotebookCount - 1 do
    with Notebook[I]^ do
    begin
      if (ID = 2) and (ConfigUnit.Config.ProgramMode <> pmRADIOAVIONICA_MAV) or
         (ID = 3) and (ConfigUnit.Config.ProgramMode = pmRADIOAVIONICA_MAV) then
      begin
        ListItem:= ListView1.Items.Add;
        ListItem.Caption:= IntToStr(I + 1);

        DisToCoordParams(DisCoord, CrdParams);
        RC:= CrdParamsToRealCrd(CrdParams, Header.MoveDir);
        ListItem.SubItems.Add(RealCrdToStr(RC));
        ListItem.SubItems.Add(HeaderStrToString(Defect));
        ListItem.Data:= Pointer(I);
      end;
   end;
end;

procedure TNoteBookForm.Panel3Resize(Sender: TObject);
begin
//  Panel5.Left:= (Panel3.Width - Panel5.Width) div 2;
end;

procedure TNoteBookForm.RefreshData;
begin
  CreateCells(nil);
end;

procedure TNoteBookForm.FormShow(Sender: TObject);
var
  I: Integer;

begin

  if Config.ProgramMode = pmRADIOAVIONICA_MAV then
  begin
    NordcoPanel.BringToFront;
    Height:= 553;
  end;
  if Config.ProgramMode <> pmRADIOAVIONICA_MAV then
  begin
    StandartPanel.BringToFront;
    Height:= 511;
  end;

  CreateCells(Sender);
  if ListView1.Items.Count <> 0 then ListView1.Selected:= ListView1.Items.Item[0];
  ListView1SelectItem(nil, nil, False);
end;

procedure TNoteBookForm.FormActivate(Sender: TObject);
begin
  MainForm.DisplayModeToControl(nil);
end;

procedure TNoteBookForm.PosEditKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then NameEdit.SetFocus;
end;

procedure TNoteBookForm.NameEditKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then Memo.SetFocus;
end;

procedure TNoteBookForm.Button2Click(Sender: TObject);
var
  I: Integer;

begin
  if ListView1.SelCount <> 0 then
  begin
    if Application.MessageBox(PChar(LanguageUnit.LangTable.Caption['Notebook:SureDel']), PChar(LanguageUnit.LangTable.Caption['Common:Attention']), MB_OKCANCEL + MB_DEFBUTTON1) <> IDOK then Exit;
    I:= ListView1.ItemIndex;
    TAvk11ViewForm(MyOwner).DatSrc.NotebookDelete(Integer(ListView1.Selected.Data));
    GetIndex:= - 1;
    CreateCells(Sender);
    if I < ListView1.Items.Count then ListView1.ItemIndex:= I
                                 else if ListView1.Items.Count <> 0 then ListView1.ItemIndex:= ListView1.Items.Count - 1 else ListView1.ItemIndex:=  - 1;
    ListView1SelectItem(nil, nil, false);
    MainForm.RepaintChildren;
  end;
end;

procedure TNoteBookForm.Panel2Resize(Sender: TObject);
begin
//  Panel7.Left:= (Panel2.Width - Panel7.Width) div 2;
end;

procedure TNoteBookForm.Panel8Resize(Sender: TObject);
begin
//  Panel9.Left:= (Panel8.Width - Panel9.Width) div 2;
end;

procedure TNoteBookForm.GetData(Index: Integer; Flag: Boolean = True);
var
  FGH: Integer;
  Crd: TCrdParams;
  Crd_: TRealCoord;

begin
  GetIndex:= Index;

  with TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^ do
  begin
    if Flag then
    begin
      if ID = 2 then
      begin
        Memo.Text:= HeaderBigStrToString(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.BlokNotText); // Привязка / Текст блокнота (кодировка win)
        NameEdit.Text:= HeaderStrToString(Defect);
        Crd_:= TAvk11ViewForm(MyOwner).DatSrc.DisToRealCoord(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.DisCoord);
        PosEdit.Text:= RealCrdToStr(Crd_);
      end;

      if ID = 3 then
      begin
        LineEdit.Text := HeaderStrToString(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.Line);
        PathEdit.Text := HeaderStrToString(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.Track);
        CodeDefectEdit.Text := HeaderStrToString(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.Defect);

        GPSStLatEdit.Text:= GPSSingleToText(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.LatitudeFrom, True);
        GPSStLonEdit.Text:= GPSSingleToText(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.LongitudeFrom, False);
        GPSEdLatEdit.Text:= GPSSingleToText(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.LatitudeTo, True);
        GPSEdLonEdit.Text:= GPSSingleToText(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.LongitudeTo, False);

        ComboBox1.ItemIndex := Ord(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.IsFailure);
        SizeEdit.Text  := Format('%3.1f', [TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.Size]);
        Size2Edit.Text := Format('%3.1f', [TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.Size2]);
        Size3Edit.Text := Format('%3.1f', [TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.Size3]);

        StatusEdit.Text:= HeaderStrToString(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.Status);
        SourceEdit.Text:= HeaderStrToString(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.Source);

        CommentEdit.Text := HeaderBigStrToString(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.BlokNotText);
      end;
    end;
  end;
end;

procedure TNoteBookForm.PutData(Index: Integer);
var
  I: Integer;
  Value: Extended;

begin
  with TAvk11ViewForm(MyOwner).DatSrc.Notebook[Index]^ do
  begin
    if ID = 1 then
    begin

    end;

    if (ID = 2) and (Config.ProgramMode <> pmRADIOAVIONICA_MAV) then
    begin
      BlokNotText:= StringToHeaderBigStr(Memo.Text); // Текст блокнота (кодировка win)
      Defect:= StringToHeaderStr(NameEdit.Text);
      for I:= 0 to ListView1.Items.Count - 1 do
        if Integer(ListView1.Items.Item[I].Data) = Index then
        begin
          ListView1.Items.Item[I].SubItems[1]:= NameEdit.Text;
          Break;
        end;
    end;

    if (ID = 3) and (Config.ProgramMode = pmRADIOAVIONICA_MAV) then
    begin

      Line:= StringToHeaderStr(LineEdit.Text);
      Track:= StringToHeaderStr(PathEdit.Text);
      Defect:= StringToHeaderStr(CodeDefectEdit.Text);
      IsFailure:= ComboBox1.ItemIndex = 1;

      if TryStrToFloat(SizeEdit.Text, Value) then Size:= Value;
      if TryStrToFloat(Size2Edit.Text, Value) then Size2:= Value;
      if TryStrToFloat(Size3Edit.Text, Value) then Size3:= Value;

      BlokNotText:= StringToHeaderBigStr(CommentEdit.Text);

      for I:= 0 to ListView1.Items.Count - 1 do
        if Integer(ListView1.Items.Item[I].Data) = Index then
        begin
          ListView1.Items.Item[I].SubItems[1]:= CodeDefectEdit.Text;
          Break;
        end;
    end;


  end;
end;

procedure TNoteBookForm.ListView1SelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
  SpeedButton1.Enabled:= ListView1.SelCount <> 0;
//  PosEdit.Enabled:= True;
  NameEdit.Enabled:= ListView1.SelCount <> 0;
  Memo.Enabled:= ListView1.SelCount <> 0;

  if GetIndex <> - 1 then
  begin
    PutData(GetIndex);
    TAvk11ViewForm(MyOwner).DatSrc.ModifyExData:= True;
    GetIndex:= - 1;
  end;
  if ListView1.SelCount <> 0 then
  begin
    GetData(Integer(ListView1.Selected.Data));
  end
  else
  begin

  end;

  if ListView1.Items.Count = 0 then
  begin
    PosEdit.Text:= '';
    NameEdit.Text:= '';
    Memo.Text:= '';
  end;
end;

procedure TNoteBookForm.Edit7Change(Sender: TObject);
var
  Code, I, J: Integer;

begin
{
  J:= Pos('.', Edit7.Text);
  if J <> 0 then
  begin
    J:= J + 1;
    I:= J;
    while (J <= Length(Edit7.Text)) and (Edit7.Text[J] in ['0'..'9']) do Inc(J);
    if J <> I then
    begin
      Val(Copy(Edit7.Text, I, J - I), I, Code);
      if (Code = 0) and ((I = 1) or (I = 2) or (I = 3)) then ComboBox1.ItemIndex:= I - 1;
    end;
  end;}
end;

procedure TNoteBookForm.RadioButton2Click(Sender: TObject);
begin
//  Edit2.Enabled:= not RadioButton2.Checked;
end;

procedure TNoteBookForm.RadioButton1Click(Sender: TObject);
begin
//  Edit2.Enabled:= RadioButton1.Checked;
end;

procedure TNoteBookForm.DateEdit1KeyPress(Sender: TObject; var Key: Char);
var
  I: Integer;

begin
  if Key = #13 then
    for I:= 0 to Self.ComponentCount - 1 do
      if Self.Components[I] is TWinControl then
        if ((Self.Components[I] as TWinControl).Tag = (Sender as TWinControl).Tag + 1) and (Self.Components[I] as TWinControl).Enabled then
          (Self.Components[I] as TWinControl).SetFocus;
end;

procedure TNoteBookForm.Select(Index: Integer);
var
  I: Integer;
begin
  if ListView1.Items.Count = 0 then Exit;
  for I:= 0 to ListView1.Items.Count - 1 do
    ListView1.Items.Item[I].Selected:= False;
  ListView1.Items.Item[Index].Selected:= True;
  ListView1SelectItem(nil, ListView1.Items.Item[Index], True);
  ListView1.Selected.MakeVisible(False);
end;

procedure TNoteBookForm.TabSheet2Resize(Sender: TObject);
begin
  if ListView1.SelCount <> 0 then
    GetData(Integer(ListView1.Selected.Data), False);
end;

procedure TNoteBookForm.StoryChange;
begin
  if GetIndex <> - 1 then PutData(GetIndex);
end;

procedure TNoteBookForm.SpeedButton41Click(Sender: TObject);
begin
//  Self.Refresh;
//  if not
  GenerateReport(100); // then Exit;
//  Self.Refresh;
//  Rpt.ShowReport;
//  FreeReport;
end;

procedure TNoteBookForm.TBItem3Click(Sender: TObject);
begin
  if not GenerateReport(100) then Exit;
end;

procedure TNoteBookForm.TBItem2Click(Sender: TObject);
var
  Dir: string;
  GPExportForm: TGPExportForm;

begin
  Dir:= Config.RepExportDir;
//  if MySelectDirectory(LanguageUnit.LangTable.Caption['FileBase:Selectdirectory'], '', Dir) then
  begin
    Config.RepExportDir:= Dir;
    Self.Refresh;
    GPExportForm:= TGPExportForm.Create(nil);
    GPExportForm.BringToFront;
    GPExportForm.Caption:= LanguageUnit.LangTable.Caption['USB:Pleasewait...'];
    GPExportForm.ProgressBar1.Position:=0;
    if GenerateReport(50) then
    begin
      Self.Refresh;
      //ExportToJpg(Rpt.DestRep, Dir + '\' + LanguageUnit.LangTable.Caption['NotebookRec:IR['] + ExtractFileName(TAvk11ViewForm(MyOwner).DatSrc.FileName) + LanguageUnit.LangTable.Caption['NotebookRec:]page%d.gif'], 2, 50);
//      FreeReport;
    end;
    GPExportForm.Free;
  end;
end;
(*
function TNoteBookForm.GenerateReport(ViewPer: Integer): Boolean;
var
  Tmp1: TAvk11Display;
  TempBMP: TBitMap;
  I, J: Integer;
  Crd: TCrdParams;
  Num: Integer;
  GPExportForm: TGPExportForm;
  s: string;
  HTMLStr: TWideStringList;
  Crd_: TRealCoord;
  Par: TStringList;
  Val: TStringList;
  PDF:TPrintPDF;

begin
  if TAvk11ViewForm(MyOwner).DatSrc.NotebookCount = 0 then Exit;
  Result:= False;
  if GetIndex <> - 1 then PutData(GetIndex);
  GPExportForm:= TGPExportForm.Create(nil);
  GPExportForm.BringToFront;
  GPExportForm.Caption:= LanguageUnit.LangTable.Caption['Common:Wait'];
  Par:= TStringList.Create;
  Val:= TStringList.Create;


  PDF:=TPrintPDF.Create(self); {Create TPrintPDF VCL}
  PDF.TITLE := 'Report'; {Set Doc Info}
  PDF.Compress:=true; {Use Compression: VCL Must compile with ZLIB comes with D3 above}
  PDF.PageWidth:= 612; {Set Page Size}
  PDF.PageHeight:= 792;
  PDF.FileName := ExtractFilePath(Application.exename) + 'report.pdf'; {Set Filename to save}
  PDF.BeginDoc; {Start Printing...}
  PDF.LineWidth:=1; {Set Linewidth for line}

  Num:= 0;
  with TAvk11ViewForm(MyOwner).DatSrc, Header do
    for J:= 0 to NotebookCount - 1 do
    begin
      GPExportForm.ProgressBar1.Position:=Round(((J + 1) / NotebookCount) * ViewPer);
      with Notebook[J]^ do
      if ID = 2 then // Тип: 1 - Ведомость контроля / 2 - Запись блокнота
      begin
        Par.Clear;
        Val.Clear;
        PDF.DrawLine(12,45,600,45); {Draw Line}
        PDF.Font.Name:=poHelveticaBold; {Set Font & Print Text}
        PDF.Font.Size:=20;
        PDF.TextOut(15,30, LangTable.Caption['FileBase:FileName'] + ': ' + ExtractFileName(Filename) );
        Par.Add(LangTable.Caption['InfoBar:Device']); // Название прибора
        Val.Add(LangTable.Caption['InfoBar:' + GetDeviceNameTextId(DeviceID) + ConfigUnit.Config.GetProgramModeId]);
        Par.Add(LangTable.Caption['InfoBar:Date'] + '/' + LangTable.Caption['InfoBar:Time']);
        Val.Add(DateTimeToStr(DT));
        if UsedItems[uiPathSectionName] = 1 then
        begin
          Par.Add(LangTable.Caption['InfoBar:PathSectionName']); // Название участка дороги
          Val.Add(HeaderStrToString(PathSectionName));
        end;

        if UsedItems[uiRailPathNumber] = 1  then
        begin
          Par.Add(LangTable.Caption['InfoBar:RailPathNumber']); // Номер ж/д пути
          Val.Add(Format('%d', [RailPathNumber]));
        end;

        if UsedItems[uiMoveDir] = 1 then // Направление движения
        begin
          Par.Add(LangTable.Caption['InfoBar:MoveDir']);
          if MoveDir > 0 then Val.Add(LangTable.Caption['InfoBar:MoveDirInc'])
                         else Val.Add(LangTable.Caption['InfoBar:MoveDirDec']);
        end;

        Par.Add(LangTable.Caption['Common:Rail']); // Нить
        if Rail = rLeft then Val.Add(LangTable.Caption['Common:Left'])
                        else Val.Add(LangTable.Caption['Common:Right']);

        Par.Add(LangTable.Caption['NotebookRec:Defectcode']); // Дефект
        Val.Add(Defect);

        Par.Add(LangTable.Caption['NotebookRec:Content']); // BlokNotText
        Val.Add(BlokNotText);

        Par.Add(LangTable.Caption['NotebookRec:Coordinate']); // coordinate

        Crd_:= TAvk11ViewForm(MyOwner).DatSrc.DisToRealCoord(TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.DisCoord);
        Val.Add(RealCrdToStr(Crd_));

        PDF.Font.Name:=poTimesRoman; {Set Font & Print Memo}
        PDF.Font.Size:=12;
        PDF.StringListOut(15,60, Par);

        PDF.Font.Name:=poTimesRoman;
        PDF.Font.Size:=12;
        PDF.StringListOut(300,60, Val);

        Inc(Num);

        Tmp1:= TAvk11Display.Create(self);
        Tmp1.Parent:= self;
        Tmp1.Visible:= False;
        Tmp1.SetData(Self, ScrollBar1, TAvk11ViewForm(MyOwner).DatSrc);
        Tmp1.DisplayConfig:= ConfigUnit.Config.DisplayConfig;
        Tmp1.Width:= 1024;
        Tmp1.Height:= 768;
        Tmp1.ApplyDisplayMode;
        Tmp1.ReCalcOutRect;
        Tmp1.SetDisplayColors(ConfigUnit.Config.ActivPColors);
        if TAvk11ViewForm(MyOwner).DatSrc.SkipBackMotion then Tmp1.CenterDisCoord:= SysCoord
                                                         else Tmp1.CenterDisCoord:= DisCoord;
        Tmp1.Zoom:= Zoom;
        Tmp1.Reduction:= Reduction;
        Tmp1.SetViewMode(TBScanViewMode(ViewMode), TRail(DrawRail), ViewLine);
        Tmp1.ShowChGate:= ShowChGate_;
        Tmp1.ShowChSettings:= ShowChSettings_;
        Tmp1.ShowChInfo:= ShowChInfo_;

        Tmp1.AmplTh:= AmplTh;
        Tmp1.AmplDon:= Boolean(AmplDon);
        Tmp1.FullRefresh;

        Tmp1.GetBitMap(TempBMP);
        PDF.BitmapOut(15,215,TempBMP);

        TempBMP.Free;
        TempBMP:= nil;
        Tmp1.Free;

        if J <> NotebookCount - 1 then PDF.NewPage; {Add New Page}
      end;
    end;


  PDF.EndDoc; {End Printing}
  PDF.Free; {FREE TPrintPDF VCL}
  Par.Free;
  Val.Free;
  GPExportForm.Free;
  Result:= True;
  ShellExecute(Handle, 'open', pchar(ExtractFilePath(Application.exename) + 'report.pdf'), '', '', sw_ShowNormal);
end;
*)

function TNoteBookForm.GenerateReport(ViewPer: Integer): Boolean;
var
  Tmp1: TAvk11Display;
  TempBMP: TBitMap;
  Y, I, J: Integer;
  Crd: TCrdParams;
  Num: Integer;
  GPExportForm: TGPExportForm;
  s: string;
  HTMLStr: TWideStringList;
  Crd_: TRealCoord;
  Par: TStringList;
  Val: TStringList;
  FN: string;
  FIndex: Integer;
  DelFlag: Boolean;
  VPDF: TVPDF;
  tmp: string;

function ReCalcX(Src: Integer): Integer;
begin
  Result:= Round(VPDF.CurrentPage.Width * Src / 595);
end;

function ReCalcY(Src: Integer): Integer;
begin
  Result:= Round(VPDF.CurrentPage.Height * Src / 842);
end;

begin //  590, 840

  if TAvk11ViewForm(MyOwner).DatSrc.NotebookCount = 0 then Exit;
  Result:= False;
  if GetIndex <> - 1 then PutData(GetIndex);
  GPExportForm:= TGPExportForm.Create(nil);
  GPExportForm.BringToFront;
  GPExportForm.Caption:= LanguageUnit.LangTable.Caption['Common:Wait'];
  Par:= TStringList.Create;
  Val:= TStringList.Create;

  VPDF:= TVPDF.Create(nil);
  VPDF.AutoLaunch := True;
  VPDF.NotEmbeddedFonts.Add('Arial');
  VPDF.StandardFontEmulation := true;
  VPDF.BeginDoc;                                          // Create PDF file

  FIndex:= 0;
  FN:= Format('%sreport%d.pdf', [DataFileDir, FIndex]);

  while FileExists(FN) do
  begin
    DelFlag:= DeleteFile(FN);
    if not DelFlag then
    begin
      Inc(FIndex);
      FN:= Format('%sreport%d.pdf', [ExtractFilePath(Application.exename), FIndex]);
    end else Break;
  end;

  VPDF.FileName:= FN; {Set Filename to save}

  {$IFNDEF ESTONIA}

  Num:= 0;
  with TAvk11ViewForm(MyOwner).DatSrc , Header do
    for J:= 0 to NotebookCount - 1 do
    begin
      GPExportForm.ProgressBar1.Position:=Round(((J + 1) / NotebookCount) * ViewPer);
      with Notebook[J]^ do

      if (ID = 2) and (ConfigUnit.Config.ProgramMode <> pmRADIOAVIONICA_MAV) or
         (ID = 3) and (ConfigUnit.Config.ProgramMode = pmRADIOAVIONICA_MAV) then

//      if ID in [2, 3] then // Тип: 1 - Ведомость контроля / 2 - Запись блокнота
      begin
        Par.Clear;
        Val.Clear;

        VPDF.CurrentPage.Canvas.MoveTo(ReCalcX(12), ReCalcY(45)); {Draw Line}
        VPDF.CurrentPage.Canvas.LineTo(ReCalcX(600), ReCalcY(45)); {Draw Line}
        VPDF.CurrentPage.SetFont('Arial', [], 21);  // Show page header
        VPDF.CurrentPage.UnicodeTextOut(ReCalcX(15), ReCalcY(2), 0, LangTable.Caption['FileBase:FileName'] + ': ' + ExtractFileName(Filename));
        VPDF.CurrentPage.SetFont('Arial', [], 12);  // Show page header
        Par.Add(LangTable.Caption['InfoBar:Device']); // Название прибора
        Val.Add(LangTable.Caption['InfoBar:' + GetDeviceNameTextId(DeviceID) + ConfigUnit.Config.GetProgramModeId]);
        Par.Add(LangTable.Caption['InfoBar:Date'] + '/' + LangTable.Caption['InfoBar:Time']);
        Val.Add(DateTimeToStr(DT));
        if UsedItems[uiPathSectionName] = 1 then
        begin
          Par.Add(LangTable.Caption['InfoBar:PathSectionName']); // Название участка дороги
          Val.Add(HeaderStrToString(PathSectionName));
        end;

        if UsedItems[uiRailPathNumber] = 1  then
        begin
          Par.Add(LangTable.Caption['InfoBar:RailPathNumber']); // Номер ж/д пути
          Val.Add(Format('%s', [GetRailPathNumber_in_String]));
        end;

        if UsedItems[uiMoveDir] = 1 then // Направление движения
        begin
          Par.Add(LangTable.Caption['InfoBar:MoveDir']);
          if MoveDir > 0 then Val.Add(LangTable.Caption['InfoBar:MoveDirInc'])
                         else Val.Add(LangTable.Caption['InfoBar:MoveDirDec']);
        end;

        Par.Add(LangTable.Caption['Common:Rail']); // Нить
        if Rail = rLeft then Val.Add(LangTable.Caption['Common:Left'])
                        else Val.Add(LangTable.Caption['Common:Right']);

        Par.Add(LangTable.Caption['InfoBar:OperatorName']); // Дефект
        Val.Add(HeaderStrToString(Header.OperatorName));

        Par.Add(LangTable.Caption['NotebookRec:Defectcode']); // Дефект
        Val.Add(HeaderStrToString(Defect));

        Par.Add(LangTable.Caption['NotebookRec:Content']); // BlokNotText
        Val.Add(Copy(HeaderBigStrToString(BlokNotText), 1, 50));

        Par.Add(LangTable.Caption['NotebookRec:Coordinate']); // coordinate

        Crd_:= TAvk11ViewForm(MyOwner).DatSrc.DisToRealCoord(TAvk11ViewForm(MyOwner).DatSrc.Notebook[J]^.DisCoord);
        Val.Add(RealCrdToStr(Crd_));

        Y:= ReCalcY(40);
        for I := 0 to Par.Count - 1 do
        begin
          s:= Par[I];
          VPDF.CurrentPage.UnicodeTextOut(ReCalcX(15), Y, 0, s); {Set Font & Print Memo}
          Y:= Y + Round(Abs(VPDF.CurrentPage.Canvas.Font.Height) * 1.1);
        end;

        Y:= ReCalcY(40);
        for I := 0 to Val.Count - 1 do
        begin
          VPDF.CurrentPage.UnicodeTextOut(ReCalcX(300), Y, 0, Val[I]); {Set Font & Print Memo}
          Y:= Y + Round(Abs(VPDF.CurrentPage.Canvas.Font.Height) * 1.1);
        end;

        Inc(Num);

        Tmp1:= TAvk11Display.Create(self);
        Tmp1.Parent:= self;
        Tmp1.Visible:= False;
        Tmp1.SetData(Self, ScrollBar1, TAvk11ViewForm(MyOwner).DatSrc);
        Tmp1.DisplayConfig:= ConfigUnit.Config.DisplayConfig;
        Tmp1.Width:= 1024;
        Tmp1.Height:= 1024;
        Tmp1.ApplyDisplayMode;
        Tmp1.ReCalcOutRect;
        Tmp1.FilterMode:= TAvk11ViewForm(MyOwner).Display.FilterMode;
        Tmp1.Filtr:= TAvk11ViewForm(MyOwner).Display.Filtr;
        Tmp1.SetDisplayColors(ConfigUnit.Config.ActivPColors);
        if TAvk11ViewForm(MyOwner).DatSrc.SkipBackMotion then Tmp1.CenterDisCoord:= SysCoord
                                                         else Tmp1.CenterDisCoord:= DisCoord;
        Tmp1.Zoom:= Zoom;
        Tmp1.Reduction:= Reduction;
        Tmp1.SetViewMode(TBScanViewMode(ViewMode), TRail(DrawRail), ViewLine);
        Tmp1.ShowChGate:= ShowChGate_;
        Tmp1.ShowChSettings:= ShowChSettings_;
        Tmp1.ShowChInfo:= ShowChInfo_;

        Tmp1.AmplTh:= AmplTh;
        Tmp1.AmplDon:= Boolean(AmplDon);
        Tmp1.FullRefresh;

        Tmp1.GetBitMap(TempBMP);
        VPDF.CurrentPage.ShowImage( VPDF.AddImage(TempBMP, icJpeg), ReCalcX(15), Y + ReCalcY(15) {ReCalcY(160)}, ReCalcX(560), ReCalcY(625), 0 ); // Show image and rotate
        VPDF.CurrentPage.Rectangle(ReCalcX(15), Y + ReCalcY(15), ReCalcX(560), ReCalcY(625));

        TempBMP.Free;
        TempBMP:= nil;
        Tmp1.Free;

        if J <> NotebookCount - 1 then VPDF.AddPage; {Add New Page}
      end;
    end;

  {$ENDIF}

  {$IFDEF ESTONIA}

  Num:= 0;
  with TAvk11ViewForm(MyOwner).DatSrc , Header do
    for J:= 0 to NotebookCount - 1 do
    begin
      GPExportForm.ProgressBar1.Position:=Round(((J + 1) / NotebookCount) * ViewPer);
      with Notebook[J]^ do

      if (ID = 2) and (ConfigUnit.Config.ProgramMode <> pmRADIOAVIONICA_MAV) or
         (ID = 3) and (ConfigUnit.Config.ProgramMode = pmRADIOAVIONICA_MAV) then

//      if ID in [2, 3] then // Тип: 1 - Ведомость контроля / 2 - Запись блокнота
      begin
        Par.Clear;
        Val.Clear;

        VPDF.CurrentPage.Canvas.MoveTo(ReCalcX(12), ReCalcY(45)); {Draw Line}
        VPDF.CurrentPage.Canvas.LineTo(ReCalcX(600), ReCalcY(45)); {Draw Line}
        VPDF.CurrentPage.SetFont('Arial', [], 21);  // Show page header
        VPDF.CurrentPage.UnicodeTextOut(ReCalcX(15), ReCalcY(2), 0, 'KONTROLL ARUANNE' + ': ' + ExtractFileName(Filename));
        VPDF.CurrentPage.SetFont('Arial', [], 12);  // Show page header
        Par.Add('Defektoskoop'); // Дефектоскоп
        Val.Add('Авикон-31');
//        Val.Add(LangTable.Caption['InfoBar:' + GetDeviceNameTextId(DeviceID) + ConfigUnit.Config.GetProgramModeId]);
        Par.Add('Kuupäev/Kellaaeg');
//        Par.Add(LangTable.Caption['InfoBar:Date'] + '/' + LangTable.Caption['InfoBar:Time']);
        Val.Add(DateTimeToStr(DT));
        if UsedItems[uiPathSectionName] = 1 then
        begin
          Par.Add('Jaamayahe'); // Название участка дороги
//          Par.Add(LangTable.Caption['InfoBar:PathSectionName']); // Название участка дороги
          Val.Add(HeaderStrToString(PathSectionName));
        end;

        if UsedItems[uiRailPathNumber] = 1  then
        begin
          Par.Add(LangTable.Caption['InfoBar:RailPathNumber']); // Номер ж/д пути
          Val.Add(Format('%s', [GetRailPathNumber_in_String]));
        end;

        if UsedItems[uiMoveDir] = 1 then // Направление движения
        begin
          Par.Add('Suund');
        //Par.Add(LangTable.Caption['InfoBar:MoveDir']);
          if MoveDir > 0 then Val.Add('Kilomeetri kasvu suunas')
                         else Val.Add('Kilomeetri vähenema suunas');
  //        if MoveDir > 0 then Val.Add(LangTable.Caption['InfoBar:MoveDirInc'])
  //                       else Val.Add(LangTable.Caption['InfoBar:MoveDirDec']);
        end;

        Par.Add('Rööpaniit'); // Нить
      //Par.Add(LangTable.Caption['Common:Rail']); // Нить

        if Rail = rLeft then Val.Add('Vasak') else Val.Add('Parem');
//        if Rail = rLeft then Val.Add(LangTable.Caption['Common:Left'])
//                        else Val.Add(LangTable.Caption['Common:Right']);

        Par.Add(LangTable.Caption['Operaatori nimi']);
      //Par.Add(LangTable.Caption['InfoBar:OperatorName']);

        Val.Add(HeaderStrToString(Header.OperatorName));

        Par.Add('Defekti kood'); // Дефект
      //Par.Add(LangTable.Caption['NotebookRec:Defectcode']); // Дефект

        Val.Add(HeaderStrToString(Defect));

        Par.Add(LangTable.Caption['Seisukord']); // BlokNotText
      //Par.Add(LangTable.Caption['NotebookRec:Content']); // BlokNotText

        Val.Add(Copy(HeaderBigStrToString(BlokNotText), 1, 50));

        Par.Add('Raudtee koordinaat'); // coordinate
      //Par.Add(LangTable.Caption['NotebookRec:Coordinate']); // coordinate

        Crd_:= TAvk11ViewForm(MyOwner).DatSrc.DisToRealCoord(TAvk11ViewForm(MyOwner).DatSrc.Notebook[J]^.DisCoord);
        case ConfigUnit.Config.EstonisCoordSysytem of
          0: tmp:= RealCrdToStr(Crd_, 5);
          1: tmp:= RealCrdToStr(Crd_, 4);
          2: tmp:= RealCrdToStr(Crd_, 6);
          else tmp:= RealCrdToStr(Crd_, 2);
        end;

        if Pos('Пк', tmp) <> 0 then tmp[Pos('Пк', tmp)]:= 'P';
        Val.Add(tmp);

        Y:= ReCalcY(40);
        for I := 0 to Par.Count - 1 do
        begin
          s:= Par[I];
          VPDF.CurrentPage.UnicodeTextOut(ReCalcX(15), Y, 0, s); {Set Font & Print Memo}
          Y:= Y + Round(Abs(VPDF.CurrentPage.Canvas.Font.Height) * 1.1);
        end;

        Y:= ReCalcY(40);
        for I := 0 to Val.Count - 1 do
        begin
          VPDF.CurrentPage.UnicodeTextOut(ReCalcX(300), Y, 0, Val[I]); {Set Font & Print Memo}
          Y:= Y + Round(Abs(VPDF.CurrentPage.Canvas.Font.Height) * 1.1);
        end;

        Inc(Num);

        Tmp1:= TAvk11Display.Create(self);
        Tmp1.Parent:= self;
        Tmp1.Visible:= False;
        Tmp1.SetData(Self, ScrollBar1, TAvk11ViewForm(MyOwner).DatSrc);
        Tmp1.DisplayConfig:= ConfigUnit.Config.DisplayConfig;
        Tmp1.Width:= 1024;
        Tmp1.Height:= 1024;
        Tmp1.ApplyDisplayMode;
        Tmp1.ReCalcOutRect;
        Tmp1.FilterMode:= TAvk11ViewForm(MyOwner).Display.FilterMode;
        Tmp1.Filtr:= TAvk11ViewForm(MyOwner).Display.Filtr;
        Tmp1.SetDisplayColors(ConfigUnit.Config.ActivPColors);
        if TAvk11ViewForm(MyOwner).DatSrc.SkipBackMotion then Tmp1.CenterDisCoord:= SysCoord
                                                         else Tmp1.CenterDisCoord:= DisCoord;
        Tmp1.Zoom:= Zoom;
        Tmp1.Reduction:= Reduction;
        Tmp1.SetViewMode(TBScanViewMode(ViewMode), TRail(DrawRail), ViewLine);
        Tmp1.ShowChGate:= ShowChGate_;
        Tmp1.ShowChSettings:= ShowChSettings_;
        Tmp1.ShowChInfo:= ShowChInfo_;

        Tmp1.AmplTh:= AmplTh;
        Tmp1.AmplDon:= Boolean(AmplDon);
        Tmp1.FullRefresh;

        Tmp1.GetBitMap(TempBMP);
        VPDF.CurrentPage.ShowImage( VPDF.AddImage(TempBMP, icJpeg), ReCalcX(15), Y + ReCalcY(15) {ReCalcY(160)}, ReCalcX(560), ReCalcY(625), 0 ); // Show image and rotate
        VPDF.CurrentPage.Rectangle(ReCalcX(15), Y + ReCalcY(15), ReCalcX(560), ReCalcY(625));

        TempBMP.Free;
        TempBMP:= nil;
        Tmp1.Free;

        if J <> NotebookCount - 1 then VPDF.AddPage; {Add New Page}
      end;
    end;

  {$ENDIF}


  VPDF.EndDoc;                                            // Close PDF file
  VPDF.Free;
  Par.Free;
  Val.Free;
  GPExportForm.Free;
  Result:= True;

end;

procedure TNoteBookForm.OnChangeLanguage(Sender: TObject);
begin

  Self.Font.Name              := LangTable.Caption['General:FontName'];

  Self.Caption                := LanguageUnit.LangTable.Caption['Notebook:NotebookforFile:'];
  SpeedButton1.Caption        := LanguageUnit.LangTable.Caption['Notebook:Delete'];
  SpeedButton41.Caption       := LanguageUnit.LangTable.Caption['Notebook:PrintPreview'];
  TBItem2.Caption             := LanguageUnit.LangTable.Caption['Notebook:Save'];
  TBItem1.Caption             := LanguageUnit.LangTable.Caption['Notebook:Summary'];
  TBItem3.Caption             := LanguageUnit.LangTable.Caption['Notebook:Print'];

//  Label16.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:Notebook'];
//  Label27.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:InspectionReport'];
//  TabSheet2.Caption           := LanguageUnit.LangTable.Caption['NotebookRec:InspectionReport'];
  Label4.Caption              := LanguageUnit.LangTable.Caption['NotebookRec:Coordinate'];
  Label6.Caption              := LanguageUnit.LangTable.Caption['NotebookRec:Defectcode'];
//  Label5.Caption              := LanguageUnit.LangTable.Caption['NotebookRec:Defectoscope................................'];
//  Label2.Caption              := LanguageUnit.LangTable.Caption['NotebookRec:Coordinate...................................'];
//  Label1.Caption              := LanguageUnit.LangTable.Caption['NotebookRec:Decodingdate..............'];
//  Label12.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:Tracksection.....'];
//  Label3.Caption              := LanguageUnit.LangTable.Caption['NotebookRec:Tracknumber..................'];
//  Label14.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:Rail.....................'];
//  RadioButton1.Caption        := LanguageUnit.LangTable.Caption['NotebookRec:Section.................'];
  //RadioButton2.Caption        := LanguageUnit.LangTable.Caption['NotebookRec:Section'];
//  Label7.Caption              := LanguageUnit.LangTable.Caption['NotebookRec:Binding........................'];
  //Label8.Caption              := LanguageUnit.LangTable.Caption['NotebookRec:Inspectas..................'];
  //Label9.Caption              := LanguageUnit.LangTable.Caption['NotebookRec:RailScheme......................'];
  //Label10.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:Note..................................'];
  //Label11.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:Decoder..........................'];
  Label17.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:Content'];
  TBItem4.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:ExporttoMSWord'];
  Label1.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:List'];

  //ComboBox2.Clear;
  //ComboBox2.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:Left']);
  //ComboBox2.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:Right']);
  {
  ComboBox1.Clear;
  ComboBox1.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:Boltjoint']);
  ComboBox1.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:Railbody']);
  ComboBox1.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:Weldedjoint']);

  Label13.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:Found......................'];
  Label15.Caption             := LanguageUnit.LangTable.Caption['NotebookRec:Wayinspection.......'];

  ComboBox5.Items.Clear;
  ComboBox5.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:immediate']);
  ComboBox5.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:urgent']);
  ComboBox5.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:duringtheday']);
  ComboBox5.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:3-5days']);
  ComboBox5.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:planned']);

  ComboBox4.Items.Clear;
  ComboBox4.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:duringanalysis']);
  ComboBox4.Items.Add(LanguageUnit.LangTable.Caption['NotebookRec:ontheway']);
           }
  ListView1.Column[0].Caption:= LanguageUnit.LangTable.Caption['NotebookRec:Number'];
  ListView1.Column[1].Caption:= LanguageUnit.LangTable.Caption['NotebookRec:Coordinate'];
  ListView1.Column[2].Caption:= LanguageUnit.LangTable.Caption['NotebookRec:Defectcode'];


  Label2.Caption    := LangTable.Caption['Config:Defect']; // Дефект
  Label3.Caption    := LangTable.Caption['NotebookRec:Start'];     // Начало
  Label5.Caption    := LangTable.Caption['NotebookRec:End'];       // Конец
  Label12.Caption   := LangTable.Caption['NotebookRec:Lat'];       // Широта
  Label15.Caption   := LangTable.Caption['NotebookRec:Lat'];       // Широта
  Label13.Caption   := LangTable.Caption['NotebookRec:Lon'];       // Долгота
  Label14.Caption   := LangTable.Caption['NotebookRec:Lon'];       // Долгота
  Label8.Caption    := LangTable.Caption['NotebookRec:IsFailure'];// Ограничение скорости
  Label9.Caption    := LangTable.Caption['NotebookRec:Size1'];      // Размер №1 (мм)
  Label10.Caption    := LangTable.Caption['NotebookRec:Size2'];      // Размер №2 (мм)
  Label11.Caption   := LangTable.Caption['NotebookRec:Size3'];      // Размер №3 (мм)
  Label7.Caption    := LangTable.Caption['NotebookRec:Comment'];   // Комментарий

  StatusLabel.Caption:= LangTable.Caption['NotebookRec:Status'];   // Status;
  SourceLabel.Caption:= LangTable.Caption['NotebookRec:Source'];   // Source;


  LineLabel.Caption    := LangTable.Caption['NotebookRec:Line'];
  PathLabel.Caption    := LangTable.Caption['NotebookRec:Track'];

  ComboBox1.Clear;
  ComboBox1.Items.Add(LangTable.Caption['Common:No_']);
  ComboBox1.Items.Add(LangTable.Caption['Common:Yes_']);


end;


procedure TNoteBookForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if Msg.CharCode = 27 then Self.Close;
end;

procedure TNoteBookForm.ListView1DblClick(Sender: TObject);
begin
  if GetIndex <> - 1 then
    TAvk11ViewForm(MyOwner).Display.CenterDisCoord:= TAvk11ViewForm(MyOwner).DatSrc.Notebook[GetIndex]^.DisCoord;
end;

procedure TNoteBookForm.TBItem4Click(Sender: TObject);
{var
  WordApp: TWordApplication;
  GPExportForm: TGPExportForm;
  Temp: OleVariant;
  OleFalse,
  OleTrue: OleVariant;
  I: Integer;
 }
begin
  {OleFalse:= False;
  OleTrue:= True;
  Screen.Cursor:= crHourGlass;

  WordApp:= TWordApplication.Create(nil);
  WordApp.AutoQuit:= False;

  try
    WordApp.Connect;
  except
    ShowMessage('Не удается запустить MS Word, создание отчета невозможно!');
    Exit;
  end;

  Temp:= wdPageBreak;
  WordApp.Documents.Add(EmptyParam, EmptyParam);
  GPExportForm:= TGPExportForm.Create(nil);
  GPExportForm.BringToFront;
  GPExportForm.Caption:= LanguageUnit.LangTable.Caption['USB:Pleasewait...'];
  GPExportForm.ProgressBar1.Position:= 0;
  if GenerateReport(50) then
  begin
    for I:= 0 to Rpt.DestRep.Pages.Count - 1 Do
    begin
      ExportToJpgOnePage(Rpt.DestRep, I, '', 2, 50, True);
      GPExportForm.ProgressBar1.Position:= 50 + Round(((I + 1) / Rpt.DestRep.Pages.Count) * 50);
      WordApp.Selection.Paste;
      if I <> Rpt.DestRep.Pages.Count - 1 then WordApp.Selection.InsertBreak(Temp);
    end;
    FreeReport;
  end;
  GPExportForm.Free;

  WordApp.Visible:=true;
  Screen.Cursor:= crDefault; }
end;

end.



