object ViewFileErrorForm: TViewFileErrorForm
  Left = 349
  Top = 273
  BorderStyle = bsSizeToolWin
  Caption = #1054#1096#1080#1073#1082#1080' '#1092#1072#1081#1083#1072
  ClientHeight = 438
  ClientWidth = 766
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShortCut = FormShortCut
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 419
    Width = 766
    Height = 19
    Panels = <>
    ExplicitTop = 411
  end
  object ListView2: TListView
    Left = 0
    Top = 0
    Width = 766
    Height = 419
    Align = alClient
    Columns = <
      item
        Caption = 'ID'
        Width = 120
      end
      item
        Caption = 'DisCrd'
        Width = 120
      end
      item
        Caption = 'Error Offset'
        Width = 120
      end
      item
        Caption = 'Error Length'
        Width = 120
      end
      item
        Caption = 'Last ID'
        Width = 120
      end
      item
        Width = 120
      end>
    ReadOnly = True
    RowSelect = True
    TabOrder = 1
    ViewStyle = vsReport
    OnDblClick = ListView2DblClick
    ExplicitHeight = 411
  end
end
