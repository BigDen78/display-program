﻿//{$DEFINE MYDEBUG}
//{$DEFINE OPENDEBUGMENU}

// 07.12.2017

// Если в ручнике нет сигналов то длительность записи 0 сек

// 03.12.2017
// T Неверный порядок с списке сообщений всплывающих при выборе иконки на центре экрана / из за неправильной организации приоритеты вывода событий

//  02.11.2017
// + скрытие зон движения назад
// + скачки координаты после ручника
// + русский язык в VMT US
// - расчет НПКУч
// - Зоны АК если начинаются левее края экрана или заканчиваются правее то не отрисовываются
// - Долго ставитьтся отметка о дефекте (в блокнот)
// - Проверка правильности/соотвествия данных в окне "Хронология работы"

//  27.03.2017 - //NOTRANSLATE - нет перевода
// + Messages:Attention!
// + Нозвание для новой палитры
// + Блокнот: название столбцов
// + Блокнот: Messages:Attention!
// + Луппа: Перевод
// ? Зоны вывода значений измерений - подправить границы
// ? Тормозит файл с колесами
// + Луппа + В виде рельса = Жопа
// + Блокнот: Удаление - выбор строки
// + Лупа прилипает
// Палитры всетлая и темная
// Сведение
// Контроль версии
// Перепутаны прямые каналы колес
// Редко пишет по пути
// Перепутаны прямые каналы в колесах


// 04.04.12 Исправленно - имена каналов в Режиме Ручной

// ------------------------------------------------------------
// 15.08.2015 USK-004R
// ------------------------------------------------------------

// [+] - Флаг версии - MAV
// [+] - Разделение 58-х каналов на разные дорожки
// [+] - Информация о файле - нить + нить при просмотре В-развертки
// [+] - Версия
// [Л] - Перепутаны наезжающие / отъезжающие каналы
// [+] - Перевод
// [+] - Проверить режим в Виде Рельса
// [+] - Блокировать выбор нить в меню и панели инструментов + Виде Рельса
// [+] - Неверно определяется глубина при включенном режиме В Виде Рельса
// [+] - Цвета 50 град - рабочая / не рабочая
// [+] - Проверить работу со старыми файлами USK-004 и как пишут 50 град
// [+] - Проверить меню Вид - для файлов EGO USW
// [+] - Проверить события EGO USW
// [+] - Кривая схема прозвучивания в окне луппы
// [+] - Проверить геометрию искательной системы - Константы сведения
// [+] - Экспорт в csv формат
// [+] - Наличие GPS / Состояние приемника GPS
// [+] - Испанский язык! убрать выбор из настроек?
// [+] - Перевод: SHORT_+50_ECHO_NOWORK
// [+] - Перевод: Подменю варианта внесения отметки
// [+] - Добавление записей в главном меню
// [X] - Две линии для Norrdico
// [+] - Имя файла ++ для Norrdico
// [+] - Посмотреть БД
// [+] - Проверить версию не MAV
// [+] - Выбоор языка init.ini, код для испанского ? - (Pos(UpperCase('language=102'), UpperCase(Ini[1])) <> 0) then
// [+] - Инсталлятор
// [+] - Работа со флэшкой
// [ ] - Функция создания скриншотов (сразу на 2-х языках)
// [ ] - Help
// [?] - Перевод пунктов меню для EGO USW

// [ ] - Отрицательная координата 000+000
// [X] - Убрать лишние расширения в отрытие файла
// [X] - Отображение GPS трека на карте
{$I DEF.INC}
unit MainUnit; { Language 1 }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, Avk11ViewUnit, StdCtrls, ComCtrls, TB2Item, TB2Dock, TB2Toolbar,
  ImgList, TB2MDI, ExtCtrls, TB2ToolWindow, DisplayUnit,
  Grids, Spin, ConfigUnit, TB2ExtItems, TeeShape, TeEngine, MyTypes,
  TeeProcs, StdActns, ActnList, FileBaseUnit, AppEvnts, GotoPos, HandScan,
  ValEdit, DataBase, InfoBarUnit, InfoBarUnit2, Buttons, { FR_Class, } LanguageUnit, XPMan,
  ShellAPI, Series, Chart, FileAnUnit, Math, Themes, PGBarExport, MapView, HeadScan,
  KuGraphUnit, AviconTypes, AviconDataSource, DataFileConfig, JPeg, CreateBaseProg, AutodecUnit;

type
  TMainForm = class(TForm)
    OpenDialog: TOpenDialog;
    ImageList1: TImageList;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    miFileMan: TTBSubmenuItem;
    miOpenFile: TTBItem;
    miClose: TTBItem;
    Sep1: TTBSeparatorItem;
    miFileDownload: TTBItem;
    miSettings: TTBItem;
    miExit: TTBItem;
    miViewMan: TTBSubmenuItem;
    N6: TTBSeparatorItem;
    miBothRail: TTBItem;
    miRightRail: TTBItem;
    miLeftRail: TTBItem;
    miAllChannels: TTBItem;
    miOnlyNaezdChannels: TTBItem;
    miOnlyOtezdChannels: TTBItem;
    miGotoMan: TTBSubmenuItem;
    miGoto: TTBItem;
    miWinMan: TTBSubmenuItem;
    TBMDIWindowItem1: TTBMDIWindowItem;
    TBMDIHandler1: TTBMDIHandler;
    miZoomList: TTBSubmenuItem;
    Sep5: TTBSeparatorItem;
    Sep4: TTBSeparatorItem;
    TBSeparatorItem9: TTBSeparatorItem;
    TBSeparatorItem10: TTBSeparatorItem;
    tbAmplThList: TTBSubmenuItem;
    AmplTh0: TTBItem;
    AmplTh1: TTBItem;
    AmplTh2: TTBItem;
    AmplTh3: TTBItem;
    AmplTh4: TTBItem;
    AmplTh5: TTBItem;
    AmplTh6: TTBItem;
    AmplTh7: TTBItem;
    AmplTh8: TTBItem;
    AmplTh9: TTBItem;
    AmplTh10: TTBItem;
    AmplTh11: TTBItem;
    AmplTh12: TTBItem;
    AmplTh13: TTBItem;
    AmplTh14: TTBItem;
    AmplTh15: TTBItem;
    miGotoLabel: TTBSubmenuItem;
    TBSeparatorItem13: TTBSeparatorItem;
    miDonAmpl: TTBItem;
    ActionList1: TActionList;
    WindowCascade1: TWindowCascade;
    WindowTileHorizontal1: TWindowTileHorizontal;
    WindowTileVertical1: TWindowTileVertical;
    WindowMinimizeAll1: TWindowMinimizeAll;
    miWinTileVertical: TTBItem;
    miWinTileHorizontal: TTBItem;
    miWinMinimizeAll: TTBItem;
    miWinCascade: TTBItem;
    TBSeparatorItem14: TTBSeparatorItem;
    miGotoRecord: TTBSubmenuItem;
    miHandScan: TTBSubmenuItem;
    miBUIWork: TTBItem;
    miDebug: TTBSubmenuItem;
    TBItem16: TTBItem;
    TBItem17: TTBItem;
    TBItem18: TTBItem;
    TBItem20: TTBItem;
    miFilePar: TTBItem;
    miIntegrityChecking: TTBItem;
    miTape: TTBItem;
    miTapeReduce1: TTBItem;
    miTapeReduce2: TTBItem;
    miRail: TTBItem;
    miFileBase: TTBItem;
    Sep6: TTBSeparatorItem;
    Sep3: TTBSeparatorItem;
    TBBackground1: TTBBackground;
    XPManifest1: TXPManifest;
    TBSubmenuItem1: TTBSubmenuItem;
    tbAbout: TTBItem;
    tbHelp: TTBItem;
    TBItem1: TTBItem;
    miRecent: TTBSubmenuItem;
    tbMailTo: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    tbIE: TTBItem;
    miEchoSizeByAmpl: TTBItem;
    TBSeparatorItem5: TTBSeparatorItem;
    miNotNewRec: TTBItem;
    miNotebook: TTBItem;
    TBItem5: TTBItem;
    miFileDownloadList: TTBSubmenuItem;
    TBSeparatorItem8: TTBSeparatorItem;
    miClearLastDwdFiles: TTBItem;
    TBSeparatorItem15: TTBSeparatorItem;
    Timer1: TTimer;
    TBItem2: TTBItem;
    miSameCoordFiles: TTBSubmenuItem;
    TBSeparatorItem6: TTBSeparatorItem;
    miLincWin: TTBItem;
    TBItem32: TTBItem;
    TBSeparatorItem7: TTBSeparatorItem;
    TBItem33: TTBItem;
    tbFTP: TTBItem;
    TBItem4: TTBItem;
    TBItem6: TTBItem;
    TBItem7: TTBItem;
    TBSeparatorItem16: TTBSeparatorItem;
    miEchoColorByAmpl: TTBItem;
    MainToolbar: TTBToolbar;
    tbOpen: TTBSubmenuItem;
    tbBase: TTBSubmenuItem;
    TBSeparatorItem2: TTBSeparatorItem;
    tbBScan_: TTBItem;
    tbViewCh_: TTBItem;
    tbDrawRail_: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    tbZoomList: TTBSubmenuItem;
    TBSeparatorItem4: TTBSeparatorItem;
    tbGoto: TTBSubmenuItem;
    TBSeparatorItem11: TTBSeparatorItem;
    TBControlItem1: TTBControlItem;
    tbViewTh: TTBSubmenuItem;
    tbVideoSep: TTBSeparatorItem;
    tbNewFileRec: TTBItem;
    tbNotebook: TTBSubmenuItem;
    TBSeparatorItem17: TTBSeparatorItem;
    Panel1: TPanel;
    AmplThTrackBar: TTrackBar;
    Panel2: TPanel;
    TBItem8: TTBItem;
    TBSeparatorItem26: TTBSeparatorItem;
    miBackMotion: TTBItem;
    PopupMenu1: TPopupMenu;
    Action1: TAction;
    miSavePiece: TTBItem;
    Timer2: TTimer;
    miNextWin: TTBItem;
    TBSeparatorItem19: TTBSeparatorItem;
    TBItem3: TTBItem;
    miSensGraph: TTBItem;
    TBSeparatorItem20: TTBSeparatorItem;
    Panel3: TPanel;
    Panel4: TPanel;
    miFiltrMenu: TTBSubmenuItem;
    Filtr_Calc: TTBItem;
    TBSeparatorItem22: TTBSeparatorItem;
    Filtr_Hide: TTBItem;
    Filtr_Mark: TTBItem;
    Filtr_Off: TTBItem;
    TBSeparatorItem23: TTBSeparatorItem;
    TBItem9: TTBItem;
    SummOpenDialog: TOpenDialog;
    Action2: TAction;
    SaveDialog1: TSaveDialog;
    miFileDownload2: TTBItem;
    Timer3: TTimer;
    TBSeparatorItem24: TTBSeparatorItem;
    miChInfo_: TTBItem;
    miChGate: TTBItem;
    miChSettings: TTBItem;
    Panel5: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    TBToolbar2: TTBToolbar;
    Panel6: TPanel;
    TBControlItem3: TTBControlItem;
    TBDock3: TTBDock;
    TBToolWindow4: TTBToolWindow;
    ScanStepBtm: TButton;
    Edit1: TEdit;
    SpeedButton8: TSpeedButton;
    Button134: TButton;
    SpeedButton9: TSpeedButton;
    SelRailImageList: TImageList;
    ChDirImageList: TImageList;
    ViewModeImageList: TImageList;
    tbBScan: TTBSubmenuItem;
    TBItem10: TTBItem;
    TBItem11: TTBItem;
    TBItem12: TTBItem;
    TBItem13: TTBItem;
    tbViewCh: TTBSubmenuItem;
    TBItem14: TTBItem;
    TBItem15: TTBItem;
    TBItem19: TTBItem;
    TBItem21: TTBItem;
    TBItem22: TTBItem;
    TestButton: TButton;
    tbDrawRail: TTBSubmenuItem;
    TBItem23: TTBItem;
    TBItem24: TTBItem;
    TBItem25: TTBItem;
    Button14444: TButton;
    Button1563: TButton;
    TBItem26: TTBItem;
    ImageList2: TImageList;
    WindowTileHorizontalTimer: TTimer;
    TBSeparatorItem18: TTBSeparatorItem;
    ScanStepItem: TTBItem;
    TBSeparatorItem21: TTBSeparatorItem;
    TBSeparatorItem25: TTBSeparatorItem;
    TBItem27: TTBItem;
    TBItem28: TTBItem;
    miMetallSensor: TTBSubmenuItem;
    miMetallSensorValue0: TTBItem;
    miMetallSensorValue1: TTBItem;
    miMetallSensorZone: TTBItem;
    miUnstableBottomSignal: TTBItem;
    miZerroProbeMode: TTBItem;
    miPaintSystemState: TTBItem;
    miViewPaintSystemState: TTBItem;
    tbNewCVSRec: TTBSubmenuItem;
    AddRecPopupMenu: TPopupMenu;
    miAddNewRecMenu: TTBSubmenuItem;
    miInFile: TTBItem;
    miInNordico: TTBItem;
    TBItem29: TTBItem;
    miExportToNORDCO: TTBItem;
    ApplicationEvents1: TApplicationEvents;
    TBItem30: TTBItem;
    TBSeparatorItem27: TTBSeparatorItem;
    miViewTestRecord: TTBItem;
    Info2Toolbar: TTBToolbar;
    TBControlItem2: TTBControlItem;
    InfoPanel2: TPanel;
    miViewTestRecordSep: TTBSeparatorItem;
    TBItem31: TTBItem;
    miViewAC: TTBItem;
    miViewAutomaticSearchRes: TTBItem;
    DebugPanel: TPanel;
    TBItem34: TTBItem;
    miPointSize: TTBSubmenuItem;
    miSmallPoint: TTBItem;
    miMediumPoint: TTBItem;
    miLargePoint: TTBItem;
    biForceUGOUSW: TTBItem;
    tbHeaderFlags: TTBItem;
    miAutoDecodingItem: TTBSubmenuItem;
    TBSeparatorItem28: TTBSeparatorItem;
    TopographTBItem: TTBSubmenuItem;
    miViewMap: TTBItem;
    miUncontrolZones: TTBItem;
    TBSeparatorItem32: TTBSeparatorItem;
    miOperatorLog: TTBItem;
    miMarkOnBScan: TTBItem;
    TBSeparatorItem29: TTBSeparatorItem;
    TBItemTemperatureChart: TTBItem;
    tbAllEventsOnLine: TTBItem;
    tbGPSTrack: TTBItem;
    GPS_SaveDialog: TSaveDialog;
    TBItem35: TTBItem;
    TBSeparatorItem30: TTBSeparatorItem;
    TBItem36: TTBItem;
    SpeedButton10: TSpeedButton;
    SHOW_REDUCEPOS: TTBItem;
    TBItem38: TTBItem;
    TBSeparatorItem31: TTBSeparatorItem;
    tbVideo: TTBItem;
    procedure tbOpenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure miExitClick(Sender: TObject);
    procedure miFileDownloadClick(Sender: TObject);
    procedure AmplThTrackBarChange(Sender: TObject);
    procedure miSettingsClick(Sender: TObject);
    procedure miCloseClick(Sender: TObject);
    procedure MenuItemClick(Sender: TObject);
    procedure tbDrawRailClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure tbReduceClick(Sender: TObject);
    procedure GotoLabelClick(Sender: TObject);
    procedure RightEchoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure tbViewChClick(Sender: TObject);
    procedure tbBScan_Click(Sender: TObject);
    procedure tbBaseClick(Sender: TObject);
    procedure tbNewFileRecClick(Sender: TObject);
    procedure tbNotebook_Click(Sender: TObject);
    procedure miGotoClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tbSoundClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    function ActiveViewForm: TAvk11ViewForm;
    function ActiveViewForm_: TAvk11ViewForm;
    function ActiveHandViewForm: THandScanForm;
    function ActiveHeadViewForm: THeadScanForm;
    procedure TBSubmenuItem4Click(Sender: TObject);
    procedure miBUIWorkClick(Sender: TObject);
    procedure TBItem16Click(Sender: TObject);
    procedure TBItem17Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure miFileParClick(Sender: TObject);
    procedure ChangeADDtoNoteBook(Sender: TObject);
    procedure ChangeSoundFlag(Sender: TObject);
    procedure miIntegrityCheckingClick(Sender: TObject);
    procedure OnChangeLanguage(Sender: TObject);
    procedure miFileManClick(Sender: TObject);
    procedure OpenRecent(Sender: TObject);
    procedure OpenDownLoad(Sender: TObject);
    procedure tbHelpClick(Sender: TObject);
    procedure tbAboutClick(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure OnChangeLanguage_(Sender: TObject);
    procedure TBItem1Click(Sender: TObject);
    procedure tbMailToClick(Sender: TObject);
    procedure tbIEClick(Sender: TObject);
    procedure miEchoSizeByAmplClick(Sender: TObject);
    procedure TBItem2Click(Sender: TObject);
    procedure TBItem20Click(Sender: TObject);
    procedure tbNoSwedClick(Sender: TObject);
    procedure tbSwed1Click(Sender: TObject);
    procedure tbSwed2Click(Sender: TObject);
    procedure tbAsRailClick(Sender: TObject);
    procedure tbOpenPopup(Sender: TTBCustomItem; FromLink: Boolean);
    procedure TBItem4Click(Sender: TObject);
    procedure TBItem5Click(Sender: TObject);
    procedure miClearLastDwdFilesClick(Sender: TObject);
    procedure TBItem18Click(Sender: TObject);
    procedure miSameCoordFilesPopup(Sender: TTBCustomItem; FromLink: Boolean);
    procedure miLincWinClick(Sender: TObject);
    procedure SameCoordClick(Sender: TObject);
    procedure miWinManPopup(Sender: TTBCustomItem; FromLink: Boolean);
    procedure OpenNotebook(Index: Integer);
    procedure TBItem32Click(Sender: TObject);
    procedure TBItem33Click(Sender: TObject);
    procedure tbFTPClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TBItem6Click(Sender: TObject);
    procedure TBItem7Click(Sender: TObject);
    procedure miEchoColorByAmplClick(Sender: TObject);
    procedure TBInvertClick(Sender: TObject);
    procedure TBItem8Click(Sender: TObject);
    procedure miBackMotionClick(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure miSavePieceClick(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure TBItem3Click(Sender: TObject);
    procedure miNextWinClick(Sender: TObject);
    procedure miSensGraphClick(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure SummFiles(Sender: TObject);
    procedure tbFilterClick(Sender: TObject);
    procedure Filtr_HideClick(Sender: TObject);
    procedure Filtr_MarkClick(Sender: TObject);
    procedure Filtr_OffClick(Sender: TObject);
    procedure Filtr_CalcClick(Sender: TObject);
    procedure TBItem9Click(Sender: TObject);
    procedure Button231Click(Sender: TObject);
    procedure miFileDownload2Click(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
    procedure cbWorkLangChange(Sender: TObject);
    procedure miChInfoClick(Sender: TObject);
    procedure miChGateClick(Sender: TObject);
    procedure miChSettingsClick(Sender: TObject);
    procedure TestBtnClick(Sender: TObject);
    procedure miViewManPopup(Sender: TTBCustomItem; FromLink: Boolean);
    procedure ScanStepBtmClick(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TestButtonClick(Sender: TObject);
    procedure Button14444Click(Sender: TObject);
    procedure TBItem26Click(Sender: TObject);
    procedure WindowTileHorizontalTimerTimer(Sender: TObject);
    procedure miDebugClick(Sender: TObject);
    procedure miMetallSensorValue0Click(Sender: TObject);
    procedure miMetallSensorValue1Click(Sender: TObject);
    procedure TBItem27Click(Sender: TObject);
    procedure TBItem28Click(Sender: TObject);
    procedure miMetallSensorZoneClick(Sender: TObject);
    procedure miUnstableBottomSignalClick(Sender: TObject);
    procedure miZerroProbeModeClick(Sender: TObject);
    procedure miPaintSystemStateClick(Sender: TObject);
    procedure miUpdate();
    procedure miViewPaintSystemStateClick(Sender: TObject);
    procedure tbNewCVSRecClick(Sender: TObject);
    procedure TBItem29Click(Sender: TObject);
    procedure miInFileClick(Sender: TObject);
    procedure miInNordicoClick(Sender: TObject);
    procedure miAddNewRecMenuPopup(Sender: TTBCustomItem; FromLink: Boolean);
    procedure miExportToNORDCOClick(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure ApplicationEvents1ShortCut(var Msg: TWMKey; var Handled: Boolean);
//    procedure SaveAsA11ButtonClick(Sender: TObject);
    function LoadDataEvent(StartDisCoord: Integer): Boolean;
    function LoadDataEvent2(StartDisCoord: Integer): Boolean;
    procedure miViewTestRecordClick(Sender: TObject);
    procedure TBItem31Click(Sender: TObject);
    procedure miViewACClick(Sender: TObject);
    procedure miViewAutomaticSearchResClick(Sender: TObject);
    procedure tbGPSTrackClick_(Sender: TObject);
    procedure miSmallPointClick(Sender: TObject);
    procedure miMediumPointClick(Sender: TObject);
    procedure miLargePointClick(Sender: TObject);
    procedure biForceUGOUSWClick(Sender: TObject);
    procedure tbHeaderFlagsClick(Sender: TObject);
    procedure miViewMapClick(Sender: TObject);
    procedure miUncontrolZonesClick(Sender: TObject);
    procedure miOperatorLogClick(Sender: TObject);
    procedure miMarkOnBScanClick(Sender: TObject);
    procedure TBItemTemperatureChartClick(Sender: TObject);
    procedure tbAllEventsOnLineClick(Sender: TObject);
    procedure tbGPSTrackClick(Sender: TObject);
    procedure TBItem35Click(Sender: TObject);
    procedure TBItem36Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure tbVideoClick(Sender: TObject);
    procedure TopographTBItemSelect(Sender: TTBCustomItem; Viewer: TTBItemViewer; Selecting: Boolean);
  protected

  private
    ENDFLAF: Boolean;
    SkipFlag: Boolean;
    FOpenFileFlag: Boolean;
    FClientInstance: TFarProc;
    FPrevClientProc: TFarProc;
    LastAForm: TAvk11ViewForm;
    ALastCoord: Integer;
    LastSender: TObject;
    ROSTOV_Exists: Boolean;
    FPaintSystemDebugMode: Boolean;

    procedure CMChildKey(var Message: TCMChildKey);
    message CM_CHILDKEY;
  public
{$IFDEF REC}
    RecInt: TRecInterface;
{$ENDIF REC}
    ForceUGOUSW: Boolean;
    SaveState: Boolean;
    // CrdList1: array of Integer;
    // CrdList2: array of Integer;

    LastSetROSTOVActiveViewForm: TAvk11ViewForm;
    ListViewFlag: Boolean;
    FileBaseForm: TFileBaseForm;
    InfoBarForm: TInfoBarForm;
    InfoBarForm2: TInfoBarForm2;
    MainFormClose: Boolean;
    TopographTBRect: TRect;

    function OpenFile(FN: string; OpenMininized: Boolean = False; SkipHistory: Boolean = False; DeleteViewedFile: Boolean = False): TAvk11ViewForm;

    procedure NewCrd(Sender: TObject);
    procedure DisplayModeToControl(Sender: TObject);
    procedure ResreshControl(Sender: TObject);
    procedure ControlToDisplayMode;
    procedure ExcludeAssign(V: TAvk11ViewForm);
    procedure CntrToDisplayMode(Sender: TObject);
    procedure KillSelection(StringGrid: TStringGrid);
    procedure SetButtonText(OnFlag: Boolean);
    procedure SetViewButtons(CycleSel: Boolean);
    procedure RepaintChildren;

    property PaintSystemDebugMode: Boolean read FPaintSystemDebugMode;
  end;

var
  MainForm: TMainForm;
  ROSTOV: TAutodec;

  DataBase_: TDataBase;
  ChangeBMView: Boolean;
  ShiftState: TShiftState;
{$IFDEF USEKEY}
  QueryKey: function(SendBuffer: Pointer; SendQuantity: Word; ReceiveBuffer: Pointer; ReceiveQuantity: Word): Integer; cdecl;
  InitLib : function: Integer; cdecl;
  FinishLib : procedure; cdecl;
  KeyExist : Boolean;
  procedure SendCommand(NumFunc: Byte; Data: DWord; var Res_A, Res_B: DWord);
{$ENDIF USEKEY}

implementation

uses
  FlahCardUnit2, CoordGraphUnit, NoteBook, ViewEventUnit, {$IFDEF VIEWBYKM} DBConnecUnit, {$ENDIF}
  ViewFileErrorUnit, FileInfoUnit, AssignCtrl, SaveAs, FiltrParamUnit, AbountUnit_, CommonUnit, NordcoCSV, UCSUnit, OperatorLog, TemperatureChartUnit;

{$R *.DFM}
// ---[ Нажатие кнопок на панели инструментов ]---------------------------------

procedure TMainForm.tbOpenClick(Sender: TObject);
begin
  OpenDialog.DefaultExt := LangTable.Caption['Common:FileExt' + Config.GetProgramModeId];
  OpenDialog.FileName := LangTable.Caption['Common:FileExt' + Config.GetProgramModeId];
  if OpenDialog.Execute then
  begin
    // if not Assigned(
    OpenFile(OpenDialog.FileName);
    // ) then ShowMessage(LangTable.Caption['Common:BadFile']);
  end;
  DisplayModeToControl(Sender);
end;

procedure TMainForm.tbBaseClick(Sender: TObject);
begin
  if not Assigned(FileBaseForm) then
    FileBaseForm := TFileBaseForm.Create(nil)
  else
    FileBaseForm.BringToFront;
end;

procedure TMainForm.tbBScan_Click(Sender: TObject);
begin
  if SkipFlag then Exit;

  if ActiveViewForm <> nil then
    with ActiveViewForm, Display do
    begin
      if tbBScan = Sender then
      begin
        tbBScan.Tag := tbBScan.Tag + 1;
        if tbBScan.Tag > 3 then
          tbBScan.Tag := 0;
      end
      else
      begin
        tbBScan.Tag := TTBItem(Sender).Tag;
      end;

      tbBScan.Tag := TTBItem(Sender).Tag; // tbBScan.Tag + 1;
      tbBScan.Checked := False;
      case tbBScan.Tag of
        0:
          begin
            RailView := False;
            Reduction := 0;
            TBItem13.Checked := True;
          end;
        1:
          begin
            RailView := False;
            Reduction := 1;
            // tbSound.Checked:= False;
            // tbSoundClick(Sender);
            TBItem12.Checked := True;
          end;
        2:
          begin
            RailView := False;
            Reduction := 2;
            // tbSound.Checked:= False;
            // tbSoundClick(Sender);
            TBItem11.Checked := True;
          end;
        3:
          begin
            Reduction := 2;
            RailView := True;
            // tbSound.Checked:= False;
            // tbSoundClick(Sender);
            TBItem10.Checked := True;
          end;
      end;
      // Refresh;
      DisplayModeToControl(Sender);
      FullRefresh;
    end;
end;

procedure TMainForm.tbNoSwedClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    with ActiveViewForm, Display do
    begin
      // tbSound.Checked:= False;
      // tbSoundClick(Sender);

      // tbNoSwed.Checked:= True;
      Reduction := 0;
      RailView := False;
      tbBScan.Tag := 0;
      Refresh;
      DisplayModeToControl(Sender);
    end;
end;

procedure TMainForm.tbSwed1Click(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    with ActiveViewForm, Display do
    begin
      // tbSound.Checked:= False;
      // tbSoundClick(Sender);

      // tbSwed1.Checked:= True;
      Reduction := 1;
      RailView := False;
      tbBScan.Tag := 1;
      Refresh;
      DisplayModeToControl(Sender);
    end;
end;

procedure TMainForm.tbSwed2Click(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    with ActiveViewForm, Display do
    begin
      // tbSound.Checked:= False;
      // tbSoundClick(Sender);

      // tbSwed2.Checked:= True;
      Reduction := 2;
      RailView := False;
      tbBScan.Tag := 2;
      Refresh;
      DisplayModeToControl(Sender);
    end;
end;

procedure TMainForm.tbAsRailClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    with ActiveViewForm, Display do
    begin
      // tbSound.Checked:= False;
      /// tbSoundClick(Sender);

      // tbAsRail.Checked:= True;
      Reduction := 2;
      RailView := True;
      tbBScan.Tag := 3;
      Refresh;
      DisplayModeToControl(Sender);
    end;
end;

procedure TMainForm.tbViewChClick(Sender: TObject);
begin
  if SkipFlag then
    Exit;
  SkipFlag := True;

  if ActiveViewForm <> nil then
    with ActiveViewForm.Display do
    begin
      if Sender = tbViewCh then
      begin
        tbViewCh.Tag := tbViewCh.Tag + 1;
        if tbViewCh.Tag > 2 then
          tbViewCh.Tag := 0;
      end
      else
      begin
        tbViewCh.Tag := TTBItem(Sender).Tag;
      end;
      tbViewCh.Checked := False;

      case tbViewCh.Tag of
        0:
          begin
            ViewChannel := vcAll;
            TBItem14.Checked := True;
            TBItem15.Checked := False;
            TBItem19.Checked := False;
          end;
        1:
          begin
            ViewChannel := vcNaezd;
            TBItem14.Checked := False;
            TBItem15.Checked := True;
            TBItem19.Checked := False;
          end;
        2:
          begin
            ViewChannel := vcOtezd;
            TBItem14.Checked := False;
            TBItem15.Checked := False;
            TBItem19.Checked := True;
          end;
      end;

      Refresh;
      DisplayModeToControl(Sender);
    end;

  SkipFlag := False;
end;

procedure TMainForm.tbDrawRailClick(Sender: TObject);
begin

  if ActiveViewForm <> nil then
  begin
    SkipFlag := True;
    with ActiveViewForm.Display do
    begin
      if Sender = tbDrawRail then
      begin
        tbDrawRail.Tag := tbDrawRail.Tag + 1;
        if tbDrawRail.Tag = 3 then
          tbDrawRail.Tag := 0;
      end
      else
      begin
        tbDrawRail.Tag := TTBItem(Sender).Tag;
      end;

      case tbDrawRail.Tag of
        0:
          begin
            SetViewMode(vmAllLines);
            // WidthPanel3.Width:= 2;
          end;
        1:
          begin
            SetViewMode(vmOneRailLines, rLeft);
            // WidthPanel3.Width:= 2;
          end;
        2:
          begin
            SetViewMode(vmOneRailLines, rRight);
            // WidthPanel3.Width:= 0;
          end;
      end;
      // MainToolbar.Refresh;
    end;
    SkipFlag := False;
    ActiveViewForm.Display.FullRefresh;
    DisplayModeToControl(Sender);
    // Refresh;
  end;
end;

procedure TMainForm.AmplThTrackBarChange(Sender: TObject);
var
  I: Integer;
  s1: string;
  s11: string;
  s2: string;
  a: Integer;
  cfg: TDataFileConfig;
  DatSrc: TAvk11DatSrc;
  avk: TAvk11ViewForm;

begin
  if SkipFlag then Exit;
  if ActiveViewForm <> nil then
  begin

//    Config.ViewThresholdValue:= AmplThTrackBar.Position;

    with ActiveViewForm.Display do
    begin
      AmplTh := AmplThTrackBar.Position;
      Refresh;
    end;

    if Assigned(InfoBarForm) then InfoBarForm.AmplThTrackBarPosition := AmplThTrackBar.Position;
    Panel2.Caption := LangTable.Caption['MenuView:Threshold2'] + ' ' + IntToStr(ActiveViewForm.DatSrc.Config.AmplValue[AmplThTrackBar.Position]) + ' ' + LangTable.Caption['Common:dB'];

    SkipFlag := True;
    for I := 0 to ComponentCount - 1 do
      if (Copy(Components[I].Name, 1, 6) = 'AmplTh') and (Components[I] is TTBItem) and (AmplThTrackBar.Position = Components[I].Tag) then
      begin
        TTBItem(Components[I]).Checked := True;
        Break;
      end;
    SkipFlag := False;
    ActiveViewForm.SetFocus_;
  end
  else if ActiveHandViewForm <> nil then
  begin
    ActiveHandViewForm.AmplThTrackBarPosition := AmplThTrackBar.Position;
    ActiveHandViewForm.CreateImage;
    ActiveHandViewForm.Refresh;
    if Assigned(InfoBarForm) then
      InfoBarForm.AmplThTrackBarPosition := AmplThTrackBar.Position;
    if Assigned(ActiveHandViewForm.DatSrc) then
      Panel2.Caption := LangTable.Caption['MenuView:Threshold2'] + ' ' + IntToStr(ActiveHandViewForm.DatSrc.Config.AmplValue[AmplThTrackBar.Position]) + ' ' + LangTable.Caption['Common:dB'];

    SkipFlag := True;
    for I := 0 to ComponentCount - 1 do
      if (Copy(Components[I].Name, 1, 6) = 'AmplTh') and (Components[I] is TTBItem) and (AmplThTrackBar.Position = Components[I].Tag) then
      begin
        TTBItem(Components[I]).Checked := True;
        Break;
      end;
    SkipFlag := False;
    ActiveHandViewForm.SetFocus_;
  end

end;

var
  AtlFlag: Boolean;

  DC: HDC;
  Frm: TForm;
  I: Integer;
  Bmp: TBitmap;

procedure TMainForm.ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
begin
  (*
    //  Memo1.Lines.Add(Format('%x %x', [Msg.wParam, Msg.lParam]));

    if (Msg.wParam = $12) and ((Msg.lParam shr 24) = $60) then AtlFlag:= True;
    if (Msg.wParam = $12) and ((Msg.lParam shr 24) = $C0) then AtlFlag:= False;
    //  if AtlFlag {and (Msg.wParam = $50) and ((Msg.lParam shr 24) = $20) }then
    begin

    Bmp := TBitmap.Create;
    //    try
    //      for I := Pred(Screen.FormCount) to 0 do
    //      begin
    //        Application.ActiveFormHandle
    //        Frm := Screen.Forms[I];
    //        if not IsWindow(Frm.Handle) or not Frm.Visible then Continue;

    Bmp.Width := 1000; // Frm.Width;
    Bmp.Height := 1000; // Frm.Height;
    DC := GetWindowDC(Application.ActiveFormHandle);
    try
    BitBlt(Bmp.Canvas.Handle, 0, 0, Frm.Width, Frm.Height, DC, 0, 0, SrcCopy);
    finally
    ReleaseDC(Frm.Handle, DC);
    end;
    Beep();
    Bmp.SaveToFile('C:\screen.bmp');
    Bmp.Free;

    end;

    //  MainForm.Caption:= IntToStr(Ord(AtlFlag));
  *)
end;

procedure TMainForm.ApplicationEvents1ShortCut(var Msg: TWMKey; var Handled: Boolean);
const
  SM_CYCAPTION = 4;
  SM_CYFRAME = 33;
  SM_CXFRAME = 32;

var
  Rt: TRect;
  I, J: Integer;
  JpegIm: TJpegImage;
  bm: TBitmap;
  FN: string;
  Curr: TForm;
  Lang: Integer;

begin

  (*
    if Msg.CharCode = 192 then
    //    for Lang := 1 to 2 do
    begin

    //      if Lang = 1 then LangTable.CurrentGroup:= 'Russian'
    //                  else LangTable.CurrentGroup:= 'English';
    //      OnChangeLanguage(nil);
    //      FormShow(nil);

    //      for I := 0 to Application.ComponentCount - 1 do
    //        if Application.Components[I] is TWinControl then TWinControl(Application.Components[I]).Repaint;
    //      for I := 0 to Self.MDIChildCount - 1 do
    //      begin
    //        if Self.MDIChildren[I] is TAvk11ViewForm then TAvk11ViewForm(Self.MDIChildren[I]).OnChangeLanguage(nil);
    //        Self.MDIChildren[I].Repaint;
    //      end;
    //      Sleep(3000);


    //  for I := 0 to Application.ComponentCount - 1 do
    //    for I := 0 to Self.MDIChildCount - 1 do
    begin
    Curr:= Self.ActiveMDIChild;
    //      Curr:= Self.MDIChildren[I];
    //      if Application.Components[I] is TForm then
    //        if Curr.se Focused then

    //      if Application.Components[I] is TForm then
    //        if TForm(Application.Components[I]).Focused then
    begin
    //          Rt.TopLeft:= TForm(Application.Components[I]).ClientToScreen(Point( - GetSystemMetrics(SM_CXFRAME), - GetSystemMetrics(SM_CYCAPTION) - GetSystemMetrics(SM_CYFRAME)));
    //          Rt.BottomRight:= Point(Rt.Left + TForm(Application.Components[I]).Width, Rt.Top + TForm(Application.Components[I]).Height);
    Rt.TopLeft:= Curr.ClientToScreen(Point( - GetSystemMetrics(SM_CXFRAME), - GetSystemMetrics(SM_CYCAPTION) - GetSystemMetrics(SM_CYFRAME)));
    Rt.BottomRight:= Point(Rt.Left + Curr.Width, Rt.Top + Curr.Height);

    bm := TBitMap.Create;
    bm.SetSize(Rt.Right - Rt.Left, Rt.Bottom - Rt.Top);
    BitBlt(bm.Canvas.Handle, 0, 0, bm.Width, bm.Height, GetDC(0), Rt.Left, Rt.Top, SRCCOPY);
    JpegIm := TJpegImage.Create;
    JpegIm.Assign(bm);
    JpegIm.CompressionQuality := 100;
    JpegIm.Compress;

    Beep();
    J:= 0;
    FN:= Curr.Name + '.jpg';
    while True do
    begin
    if not FileExists(FN) then
    begin
    JpegIm.SaveToFile(FN);
    Break;
    end;
    Inc(J);
    FN:= Curr.Name + '(' + IntToStr(J) + ').jpg';
    end;

    bm.Destroy;
    JpegIm.Destroy;

    //   Break;
    end;
    end;
    end;

    if Msg.CharCode = 49 then
    begin
    Rt.TopLeft:= Self.ClientToScreen(Point( - GetSystemMetrics(SM_CXFRAME), - GetSystemMetrics(SM_CYCAPTION) - GetSystemMetrics(SM_CYFRAME)));
    Rt.BottomRight:= Point(Rt.Left + Self.Width, Rt.Top + Self.Height);

    bm := TBitMap.Create;
    bm.SetSize(Rt.Right - Rt.Left, Rt.Bottom - Rt.Top);
    BitBlt(bm.Canvas.Handle, 0, 0, bm.Width, bm.Height, GetDC(0), Rt.Left, Rt.Top, SRCCOPY);
    JpegIm := TJpegImage.Create;
    JpegIm.Assign(bm);
    JpegIm.CompressionQuality := 100;
    JpegIm.Compress;

    Beep();

    FN:= Self.Name + '.jpg';
    while True do
    begin
    if not FileExists(FN) then
    begin
    JpegIm.SaveToFile(FN);
    Break;
    end;
    Inc(J);
    FN:= Self.Name + '(' + IntToStr(J) + ').jpg';
    end;

    bm.Destroy;
    JpegIm.Destroy;
    end;
    *)
end;

procedure TMainForm.biForceUGOUSWClick(Sender: TObject);
begin
  ForceUGOUSW:= biForceUGOUSW.checked;
end;

// ---[ Нажатие в меню ]--------------------------------------------------------

procedure TMainForm.miExitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TMainForm.miFileParClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    ShowFileInfo(ActiveViewForm.DatSrc);
end;

procedure TMainForm.miInFileClick(Sender: TObject);
begin
  // tbNewCVSRec.Click;
end;

procedure TMainForm.miInNordicoClick(Sender: TObject);
begin
  // tbNewCVSRec.Click;
end;

procedure TMainForm.miIntegrityCheckingClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    if ActiveViewForm.DatSrc.TestCheckSumm then
      MessageBox(Handle, PChar(LangTable.Caption['Messages:FileOK']), PChar(LangTable.Caption['Common:Attention']), 64)
    else
      MessageBox(Handle, PChar(LangTable.Caption['Messages:FileCorrupt']), PChar(LangTable.Caption['Common:Attention']), 16);
  end;
end;

procedure TMainForm.miFileDownloadClick(Sender: TObject);
//var
//  FlashCardForm: TFlashCardForm;

begin
{  FlashCardForm := TFlashCardForm.Create(nil);
  FlashCardForm.SetMode(1);
  FlashCardForm.Showmodal;
  FlashCardForm.Free;}
end;

procedure TMainForm.miBUIWorkClick(Sender: TObject);
//var
//  FlashCardForm: TFlashCardForm;

begin {
  FlashCardForm := TFlashCardForm.Create(nil);
  FlashCardForm.SetMode(2);
  FlashCardForm.Showmodal;
  FlashCardForm.Free; }
end;

procedure TMainForm.miSettingsClick(Sender: TObject);
var
  I: Integer;
  CF: TConfigForm;

begin
  CF := TConfigForm.Create(nil);
  CF.OnChangeLanguage_ := OnChangeLanguage_;
  CF.Showmodal;
  CF.Free;

  for I := 0 to MDIChildCount - 1 do
  begin
    if (MDIChildren[I] is TAvk11ViewForm) then
    begin
      TAvk11ViewForm(MDIChildren[I]).Display.SetDisplayColors(Config.ActivDColors);
      TAvk11ViewForm(MDIChildren[I]).Display.DisplayConfig := Config.DisplayConfig;

      TAvk11ViewForm(MDIChildren[I]).DatSrc.Config.SetZeroProbeChannelsViewMode(T_DFC_ZeroProbeChannelsViewMode(Config.ZeroProbeChannelsViewMode));
      TAvk11ViewForm(MDIChildren[I]).Display.ApplyDisplayMode;
      TAvk11ViewForm(MDIChildren[I]).Display.Refresh;

      TAvk11ViewForm(MDIChildren[I]).Display.Refresh;
      TAvk11ViewForm(MDIChildren[I]).Repaint;
    end;
    if (MDIChildren[I] is THandScanForm) then
    begin
      THandScanForm(MDIChildren[I]).Repaint;
    end;
  end;

  SetButtonText(Config.ButtonText);
  SetViewButtons(Config.ViewSelMetod);

  if Config.CoordType then
  begin
    InfoBarForm.vCoord1 := True;
    InfoBarForm.Bevel12.Visible := True;
  end
  else
  begin
    InfoBarForm.vCoord1 := False;
    InfoBarForm.Bevel12.Visible := False;
  end;
end;

procedure TMainForm.miViewManPopup(Sender: TTBCustomItem; FromLink: Boolean);
begin
  if ActiveViewForm <> nil then
  begin

    miChSettings.Enabled := not ActiveViewForm.Display.HeightToSmall;
    miChInfo_.Enabled := not ActiveViewForm.Display.HeightToSmall;
    miChGate.Checked := ActiveViewForm.Display.ShowChGate;
    miChInfo_.Checked := ActiveViewForm.Display.ShowChInfo;
    miChSettings.Checked := ActiveViewForm.Display.ShowChSettings;

  end;
end;

procedure TMainForm.OnChangeLanguage_(Sender: TObject);
var
  I: Integer;

begin
  OnChangeLanguage(nil);
  if Assigned(InfoBarForm) then InfoBarForm.OnChangeLanguage(nil);
  if Assigned(InfoBarForm2) then InfoBarForm2.OnChangeLanguage(nil);
  for I := 0 to MDIChildCount - 1 do
  begin
    if MDIChildren[I] is THandScanForm then THandScanForm(MDIChildren[I]).OnChangeLanguage(Self);
    if MDIChildren[I] is TNoteBookForm then TNoteBookForm(MDIChildren[I]).OnChangeLanguage(Self);
    if MDIChildren[I] is TFlashCardForm2 then TFlashCardForm2(MDIChildren[I]).OnChangeLanguage(Self);
    if MDIChildren[I] is TFileBaseForm then TFileBaseForm(MDIChildren[I]).OnChangeLanguage(Self);
  end;
end;

procedure TMainForm.miCloseClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    ActiveViewForm.Close;
end;

procedure TMainForm.miDebugClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    ScanStepItem.Caption := 'Scan Step: ' + IntToStr(ActiveViewForm.DatSrc.Header.ScanStep);

end;

procedure TMainForm.miGotoClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    with TGotoForm.Create(nil) do
    begin
      SetData(ActiveViewForm);
      if Showmodal = mrOk then
      begin
        ActiveViewForm.Display.CenterDisCoord := ResDisCoord;
        ActiveViewForm.Refresh;
      end;
      Free;
    end;
end;

// ---[ Групповая обработка ]---------------------------------------------------

procedure TMainForm.MenuItemClick(Sender: TObject);
begin
  ControlToDisplayMode;
  with ActiveViewForm, Display do FullRefresh;
end;

procedure TMainForm.CntrToDisplayMode(Sender: TObject);
begin
  ControlToDisplayMode;
end;

procedure TMainForm.ControlToDisplayMode;
var
  I, J: Integer;

begin
  if SkipFlag then Exit;

  if ActiveViewForm <> nil then
    with ActiveViewForm.Display do
    begin
      for J := 0 to miZoomList.Count - 1 do
        if miZoomList.Items[J].Checked then
        begin
          Zoom := miZoomList.Items[J].Tag;
          Break;
        end;

      for I := 0 to tbAmplThList.Count - 1 do
        if tbAmplThList.Items[I].Checked then
        begin
          AmplTh := tbAmplThList.Items[I].Tag;
          AmplThTrackBar.Position := tbAmplThList.Items[I].Tag;
          Break;
        end;

      AmplDon := miDonAmpl.Checked;

      if miRail.Checked then
      begin
        RailView := True;
        Reduction := 2;
      end
      else RailView := False;
      if miTape.Checked then Reduction := 0;
      if miTapeReduce1.Checked then Reduction := 1;
      if miTapeReduce2.Checked then Reduction := 2;

      { if ViewMode in [ddBothRail, ddLeftRail, ddRightRail] then
        begin
        if miBothRail.Checked then ViewMode:= ddBothRail;
        if miLeftRail.Checked then ViewMode:= ddLeftRail;
        if miRightRail.Checked then ViewMode:= ddRightRail;
        end; }

      if not ActiveViewForm.Display.DatSrc.isSingleRailDevice then
      begin
        if miBothRail.Checked then SetViewMode(vmAllLines);
        if miLeftRail.Checked then SetViewMode(vmOneRailLines, rLeft);
        if miRightRail.Checked then SetViewMode(vmOneRailLines, rRight);
      end;

      if miOnlyOtezdChannels.Checked then ViewChannel := vcOtezd;
      if miOnlyNaezdChannels.Checked then ViewChannel := vcNaezd;
      if miAllChannels.Checked then ViewChannel := vcAll;
      SkipBackMotion := not miBackMotion.Checked;
      ViewAC := miViewAC.Checked;

      EchoSizeByAmpl := miEchoSizeByAmpl.Checked;
      EchoColorByAmpl := miEchoColorByAmpl.Checked;

      if Reduction <> 0 then
      begin
        // tbSound.Checked:= False;
        // tbSoundClick(nil);
      end;

      ApplyDisplayMode();
      DisplayModeToControl(Self);
      FullRefresh;
    end;
end;

procedure TMainForm.ResreshControl(Sender: TObject);
begin
  LastSender := nil;
  DisplayModeToControl(Sender);
end;

procedure TMainForm.DisplayModeToControl(Sender: TObject);
type
  TIt = record
    Crd: Integer;
    Name: string;
    Idx: Integer;
    Type_: Integer;
  end;

var
  ID: Byte;
  I, J: Integer;
  New: TTBItem;
  Sep: TTBSeparatorItem;
  pData: PEventData;
  CrdParams: TCrdParams;
  Tmp: Byte;

  New_: TListItem;
  List: array of TIt;

  aa: Integer;
  CalcCount: Integer;

  TimeExists_: Boolean;
  SpeedExists_: Boolean;
  TemperatureExists_: Boolean;
  VF: TAvk11ViewForm;
  Disp: TAvk11Display;
  DatSrc_: TAvk11DatSrc;


begin
  if (ActiveViewForm <> nil) and (Sender <> nil) then
  begin
 //   if Config.ShowUSBLog then
      if ROSTOV_Exists then
      begin
    //    if ActiveViewForm <> LastSetROSTOVActiveViewForm then
        begin
          LastSetROSTOVActiveViewForm:= ActiveViewForm;
          ROSTOV.SetActiveFormData(ActiveViewForm.Display, ActiveViewForm.DatSrc);
        end;
      end;

    SkipFlag := True;

    miBothRail.Enabled := Config.ProgramMode <> pmRADIOAVIONICA_MAV;
    miLeftRail.Enabled := Config.ProgramMode <> pmRADIOAVIONICA_MAV;
    miRightRail.Enabled := Config.ProgramMode <> pmRADIOAVIONICA_MAV;
    miRail.Enabled := True;
    miMarkOnBScan.Enabled := True;
    miTape.Enabled := True;
    miTapeReduce1.Enabled := True;
    miTapeReduce2.Enabled := True;
    miGoto.Enabled := True;
    miClose.Enabled := True;
    miDonAmpl.Enabled := True;
    miZoomList.Enabled := True;
    miGotoLabel.Enabled := True;
    miGotoRecord.Enabled := True;
    miHandScan.Enabled := True;
    miOnlyOtezdChannels.Enabled := True;
    miOnlyNaezdChannels.Enabled := True;
    miAllChannels.Enabled := True;
    miBackMotion.Enabled := True;
    miFilePar.Enabled := True;
    miOperatorLog.Enabled := ActiveViewForm.Display.DatSrc.isA31;
    miUncontrolZones.Enabled := ActiveViewForm.Display.DatSrc.isA31;
    miViewTestRecord.Enabled := ActiveViewForm.Display.DatSrc.TestRecordFileExists;
    miSameCoordFiles.Enabled := True;
    miViewMap.Enabled := ActiveViewForm.Display.DatSrc.isA31 or ActiveViewForm.Display.DatSrc.isUSK;
    miSensGraph.Enabled := True;
    TBItem3.Enabled := True;
    miSavePiece.Enabled := True;
    miIntegrityChecking.Enabled := True;
    miEchoSizeByAmpl.Enabled := True;
    miPointSize.Enabled := True;
    miViewAutomaticSearchRes.Enabled := ActiveViewForm.Display.DatSrc.isA31;
    miViewAC.Enabled := ActiveViewForm.Display.DatSrc.isA31;
    miEchoColorByAmpl.Enabled := True;

//    miMagnetView.Enabled:= ROSTOV_Exists;

    tbAmplThList.Enabled := True;
    tbViewCh.Enabled := True;

    tbDrawRail.Enabled := not ActiveViewForm.Display.DatSrc.isSingleRailDevice;
    miBothRail.Enabled := not ActiveViewForm.Display.DatSrc.isSingleRailDevice;
    miLeftRail.Enabled := not ActiveViewForm.Display.DatSrc.isSingleRailDevice;
    miRightRail.Enabled := not ActiveViewForm.Display.DatSrc.isSingleRailDevice;

    tbBScan.Enabled := True;
    miAutoDecodingItem.Enabled := ActiveViewForm.Display.DatSrc.isA31;
    TopographTBItem.Enabled := ActiveViewForm.Display.DatSrc.isA31;
    tbZoomList.Enabled := True;
    tbGoto.Enabled := True;

    // tbNoSwed.Enabled:= True;
    // tbSwed1.Enabled:= True;
    // tbSwed2.Enabled:= True;
    // tbAsRail.Enabled:= True;

    tbNewFileRec.Enabled := True;
    tbVideo.Enabled := True;
    tbNewCVSRec.Enabled := True;
    tbNotebook.Enabled := True;
    miNotebook.Enabled := True;
    miNotNewRec.Enabled := True;
    miExportToNORDCO.Enabled := True;

    miFiltrMenu.Enabled:= (Config.ProgramMode = pmGEISMAR) and
                          (ActiveViewForm.Display.DatSrc.isVMTUS_Scheme_1_or_2 or
                           ActiveViewForm.Display.DatSrc.isEGOUS or
                           ActiveViewForm.Display.DatSrc.isVMTUS_Scheme_4 or
                           ActiveViewForm.Display.DatSrc.isFilus_X17DW);
    miFiltrMenu.Visible:= Config.ProgramMode = pmGEISMAR;

    miChSettings.Checked := ActiveViewForm.Display.ShowChSettings;
    miChGate.Checked := ActiveViewForm.Display.ShowChGate;
    miChInfo_.Checked := ActiveViewForm.Display.ShowChInfo;

    miOperatorLog.Visible:= (Config.ProgramMode = pmRADIOAVIONICA) or (Config.ProgramMode = pmRADIOAVIONICA_KZ);
//    miViewAC.Visible := ActiveViewForm.Display.DatSrc.Header.UsedItems[uiAcousticContact] = 1;
//    TBSeparatorItem23.Visible := miViewAC.Visible;

//    InfoBarForm2.TempPanel.Visible := ActiveViewForm.Display.DatSrc.TemperatureExists;
    InfoBarForm2.SpeedPanel.Visible := ActiveViewForm.Display.DatSrc.SpeedExists;
    InfoBarForm2.TimePanel.Visible := ActiveViewForm.Display.DatSrc.TimeExists;

    TimeExists_:= ActiveViewForm.Display.DatSrc.TimeExists;
    SpeedExists_:= ActiveViewForm.Display.DatSrc.SpeedExists;
    TemperatureExists_:= ActiveViewForm.Display.DatSrc.TemperatureExists;

    if TimeExists_ and SpeedExists_ and TemperatureExists_ then
    begin
      InfoBarForm2.Width := 158;
      Info2Toolbar.Visible := True;
    end
    else if TimeExists_ and SpeedExists_ and (not TemperatureExists_) then
    begin
      InfoBarForm2.Width := 158 - 50;
      Info2Toolbar.Visible := True;
    end
    else if TimeExists_ and (not SpeedExists_) and (not TemperatureExists_) then
    begin
      InfoBarForm2.Width := 158 - 100;
      Info2Toolbar.Visible := True;
    end
    else if (not TimeExists_) and SpeedExists_ and (not TemperatureExists_) then
    begin
      InfoBarForm2.Width := 158 - 100;
      Info2Toolbar.Visible := True;
    end
    else if (not TimeExists_) and (not SpeedExists_) and (not TemperatureExists_) then
    begin
      Info2Toolbar.Visible := False;
    end;
    InfoPanel2.Width := InfoBarForm2.Width;

    VF:= ActiveViewForm;
    if Assigned(VF) then
    begin
      Disp:= VF.Display;
      if Assigned(Disp) then
      begin

        DatSrc_:= Disp.DatSrc;
        if Assigned(DatSrc_) then
        begin
          miViewPaintSystemState.Visible := ActiveViewForm.Display.DatSrc.isEGOUSW or
                                            ActiveViewForm.Display.DatSrc.isEGOUS or
                                            ActiveViewForm.Display.DatSrc.isVMT_US_BigWP or
                                            ForceUGOUSW;
          miMetallSensor.Visible := ActiveViewForm.Display.DatSrc.isEGOUSW or
                                    ActiveViewForm.Display.DatSrc.isVMT_US_BigWP or
                                    ActiveViewForm.Display.DatSrc.isEGOUS or
                                    ForceUGOUSW;
          TBSeparatorItem21.Visible := ActiveViewForm.Display.DatSrc.isEGOUSW or
                                       ActiveViewForm.Display.DatSrc.isVMT_US_BigWP or
                                       ActiveViewForm.Display.DatSrc.isEGOUS or
                                       ForceUGOUSW;
          miUnstableBottomSignal.Visible := ActiveViewForm.Display.DatSrc.isEGOUSW or
                                            ActiveViewForm.Display.DatSrc.isVMT_US_BigWP or
                                            ActiveViewForm.Display.DatSrc.isEGOUS or
                                            ForceUGOUSW;
        end;
      end;
    end;
    // miZerroProbeMode.Visible:= ActiveViewForm.Display.DatSrc.isEGOUSW or ActiveViewForm.Display.DatSrc.isEGOUS;
    // miPaintSystemState.Visible:= ActiveViewForm.Display.DatSrc.isEGOUSW or ActiveViewForm.Display.DatSrc.isEGOUS;
{$IFDEF ECHOFILTER}
    case ActiveViewForm.Display.FilterMode of
      moNoCalc:
        begin
          Filtr_Calc.Enabled := True;
          Filtr_Hide.Enabled := False;
          Filtr_Mark.Enabled := False;
          Filtr_Off.Enabled := False;
        end;
      moOff, moMark, moHide:
        begin
          Filtr_Calc.Enabled := False;
          Filtr_Hide.Enabled := True;
          Filtr_Mark.Enabled := True;
          Filtr_Off.Enabled := True;
        end;
      { moMark: begin
        Filtr_Calc.Enabled:= False;
        Filtr_Hide.Enabled:= True;
        Filtr_Mark.Enabled:= True;
        Filtr_Off.Enabled:= True;
        end;
        moHide: begin
        Filtr_Calc.Enabled:= False;
        Filtr_Hide.Enabled:= True;
        Filtr_Mark.Enabled:= True;
        Filtr_Off.Enabled:= True;
        end;
        } end;

    Filtr_Hide.Checked := ActiveViewForm.Display.FilterMode = moHide;
    Filtr_Mark.Checked := ActiveViewForm.Display.FilterMode = moMark;
    Filtr_Off.Checked := ActiveViewForm.Display.FilterMode = moOff;
{$ENDIF}
    // tbSound.Enabled:= True;
    AmplThTrackBar.Enabled := True;
    tbViewTh.Enabled := True;
    Panel2.Font.Color := clBlack;

    InfoBarForm.FileInfoPanel.Visible := True;
    // InfoBarForm.HandScanPanel.Visible:= False;

    with ActiveViewForm.Display do
    begin
      if RailView then
      begin
        tbBScan.Tag := 3;
        // tbBScan.Caption:= LangTable.Caption['MenuView:RailForm'];
        tbBScan.ImageIndex := 2;
        miRail.Checked := True;

        TBItem13.Checked := False;
        TBItem12.Checked := False;
        TBItem11.Checked := False;
        TBItem10.Checked := True;

        // tbAsRail.Checked:= True;
      end
      else
        case Reduction of
          0:
            begin
              tbBScan.Tag := 0;
              // tbBScan.Caption:= LangTable.Caption['MenuView:BScan'];
              tbBScan.ImageIndex := 46;
              miTape.Checked := True;
              // tbNoSwed.Checked:= True;

              TBItem13.Checked := True;
              TBItem12.Checked := False;
              TBItem11.Checked := False;
              TBItem10.Checked := False;

            end;
          1:
            begin
              tbBScan.Tag := 1;
              // tbBScan.Caption:= LangTable.Caption['MenuView:Convergence1'];
              tbBScan.ImageIndex := 47;
              miTapeReduce1.Checked := True;
              // tbSwed1.Checked:= True;

              TBItem13.Checked := False;
              TBItem12.Checked := True;
              TBItem11.Checked := False;
              TBItem10.Checked := False;

            end;
          2:
            begin
              tbBScan.Tag := 2;
              // tbBScan.Caption:= LangTable.Caption['MenuView:Convergence2'];
              tbBScan.ImageIndex := 48;
              miTapeReduce2.Checked := True;
              // tbSwed2.Checked:= True;

              TBItem13.Checked := False;
              TBItem12.Checked := False;
              TBItem11.Checked := True;
              TBItem10.Checked := False;

            end;
        end;

      miEchoSizeByAmpl.Checked := EchoSizeByAmpl;

      miDonAmpl.Checked := AmplDon;

      Tmp := 3; // ActiveViewForm.DatSrc.Header.AmplTH;
      AmplTh0.Visible := Tmp = 0;
      AmplTh1.Visible := Tmp <= 1;
      AmplTh2.Visible := Tmp <= 2;
      AmplTh3.Visible := Tmp <= 3;
      AmplTh4.Visible := Tmp <= 4;
      AmplTh5.Visible := Tmp <= 5;
      AmplTh6.Visible := Tmp <= 6;
      case AmplTh of
        0:
          AmplTh0.Checked := True;
        1:
          AmplTh1.Checked := True;
        2:
          AmplTh2.Checked := True;
        3:
          AmplTh3.Checked := True;
        4:
          AmplTh4.Checked := True;
        5:
          AmplTh5.Checked := True;
        6:
          AmplTh6.Checked := True;
        7:
          AmplTh7.Checked := True;
        8:
          AmplTh8.Checked := True;
        9:
          AmplTh9.Checked := True;
        10:
          AmplTh10.Checked := True;
        11:
          AmplTh11.Checked := True;
        12:
          AmplTh12.Checked := True;
        13:
          AmplTh13.Checked := True;
        14:
          AmplTh14.Checked := True;
        15:
          AmplTh15.Checked := True;
      end;

      if (ActiveViewForm.DatSrc <> nil) and (ActiveViewForm.DatSrc.Config <> nil) then
      begin
//        AmplThTrackBar.Min := ActiveViewForm.DatSrc.Config.MinAmplCode;
        AmplThTrackBar.Min := ActiveViewForm.DatSrc.MinAmplCode;
        AmplThTrackBar.Max := ActiveViewForm.DatSrc.Config.MaxAmplCode;
        AmplThTrackBar.Position := AmplTh; // AmplThTrackBar.Min;
        Panel2.Caption := LangTable.Caption['MenuView:Threshold2'] + ' ' + IntToStr(ActiveViewForm.DatSrc.Config.AmplValue[AmplThTrackBar.Position]) + ' ' + LangTable.Caption['Common:dB'];
        // AmplThTrackBarChange(nil);
      end;
      // Panel2.Caption:= LangTable.Caption['MenuView:Threshold2'] + ' ' + DBName[AmplThTrackBar.Position] + ' ' + LangTable.Caption['Common:dB'];

      if (ViewMode = vmAllLines) then
      begin
        miBothRail.Checked := True;
        TBItem25.Checked := True;
        TBItem24.Checked := False;
        TBItem23.Checked := False;
        // tbDrawRail.Caption:= LangTable.Caption['MenuView:BothRail'];
        tbDrawRail.ImageIndex := 19;
        tbDrawRail.Tag := 1;
      end;
      if (ViewMode = vmOneRailLines) and (ViewRail = rLeft) then
      begin
        miLeftRail.Checked := True;
        TBItem25.Checked := False;
        TBItem24.Checked := True;
        TBItem23.Checked := False;
        // tbDrawRail.Caption:= LangTable.Caption['MenuView:LeftRail'];
        tbDrawRail.ImageIndex := 21;
        tbDrawRail.Tag := 2;

      end;
      if (ViewMode = vmOneRailLines) and (ViewRail = rRight) then
      begin
        // tbDrawRail.Caption:= LangTable.Caption['MenuView:RightRail'];
        miRightRail.Checked := True;
        TBItem25.Checked := False;
        TBItem24.Checked := False;
        TBItem23.Checked := True;
        tbDrawRail.ImageIndex := 20;
        tbDrawRail.Tag := 0;
      end;

      for I := 0 to Self.ComponentCount - 1 do
        if (Copy(Self.Components[I].Name, 1, 4) = 'Zoom') and (Self.Components[I] is TTBItem) then (Self.Components[I] as TTBItem)
          .Checked := Self.Components[I].Tag = ActiveViewForm.Display.Zoom;

      case ViewChannel of
        vcAll:
          begin
            // tbViewCh.Caption:= LangTable.Caption['MenuView:AllChannel'];
            tbViewCh.ImageIndex := 34;
            miAllChannels.Checked := True;

            TBItem14.Checked := True;
            TBItem15.Checked := False;
            TBItem19.Checked := False;
          end;
        vcNaezd:
          begin
            // tbViewCh.Caption:= LangTable.Caption['MenuView:Zoomin'];
            tbViewCh.ImageIndex := 35;
            miOnlyNaezdChannels.Checked := True;

            TBItem14.Checked := False;
            TBItem15.Checked := True;
            TBItem19.Checked := False;
          end;
        vcOtezd:
          begin
            // tbViewCh.Caption:= LangTable.Caption['MenuView:Zoomout'];
            tbViewCh.ImageIndex := 36;
            miOnlyOtezdChannels.Checked := True;

            TBItem14.Checked := False;
            TBItem15.Checked := False;
            TBItem19.Checked := True;
          end;
      end;

      miBackMotion.Checked := not SkipBackMotion;
      miViewAC.Checked := ActiveViewForm.Display.ViewAC;
    end;

    J := 0;
    miGotoLabel.Clear;

    for I := 0 to High(ActiveViewForm.DatSrc.LabelList) do
    begin
      New := TTBItem.Create(Self);
      New.Images := InfoBarForm.ImageList;
      New.ImageIndex := -1;

      ActiveViewForm.DatSrc.GetEventData(ActiveViewForm.DatSrc.LabelList[I], ID, pData);

      if not(ID in [EID_Stolb, EID_StolbChainage, EID_Switch, EID_DefLabel, EID_TextLabel]) then
        Continue;

      case ID of
        EID_Stolb:
          with pCoordPost(pData)^ do
            if Km[0] <> Km[1] then
            begin
              New.Caption := Format(LangTable.Caption['Common:Km'] + ' %d/%d (' + LangTable.Caption['Common:pk'] + ' %d/%d)', [Km[0], Km[1], Pk[0], Pk[1]]);
              New.ImageIndex := 6;
            end
            else
            begin
              New.Caption := Format('    ' + LangTable.Caption['Common:PK'] + ' %d/%d (' + LangTable.Caption['Common:km'] + ' %d)', [Pk[0], Pk[1], Km[0]]);
              New.ImageIndex := 7;
            end;

        EID_StolbChainage:
          with pCoordPostChainage(pData)^ do
          begin
            New.Caption := CaCrdToStr(ActiveViewForm.DatSrc.CoordSys, Val);
            // Format(LangTable.Caption['Common:Chainage'] + ' %d', [Val]);
            New.ImageIndex := 6;
          end;

        EID_Switch:
          with pedSwitch(pData)^ do
            New.Caption := LangTable.Caption['Common:Pt'] + ': ' + GetString(Len, Text);
        EID_DefLabel:
          with pedDefLabel(pData)^ do
            if Rail = rLeft then
              New.Caption := LangTable.Caption['Common:Def.code'] + ' ' + GetString(Len, Text) + ' (' + LangTable.Caption['Common:Rail'] + '): ' + LangTable.Caption['Common:Left']
            else
              New.Caption := LangTable.Caption['Common:Def.code'] + ' ' + GetString(Len, Text) + ' (' + LangTable.Caption['Common:Rail'] + '): ' + LangTable.Caption['Common:Right'];

        EID_TextLabel:
          with pedTextLabel(pData)^ do
            New.Caption := GetString(Len, Text);
      end;

      New.Tag := ActiveViewForm.DatSrc.Event[ActiveViewForm.DatSrc.LabelList[I]].DisCoord;
      New.OnClick := MainForm.GotoLabelClick;
      New.Name := 'FileLabel_1' + IntToStr(ActiveViewForm.DatSrc.LabelList[I]);
      miGotoLabel.Add(New);
      Inc(J);
    end;

    if J = 0 then
    begin
      New := TTBItem.Create(Self);
      New.Caption := LangTable.Caption['Common:Empty'];
      New.Tag := -1;
      New.OnClick := nil;
      New.Name := 'NoneFileLabel';
      miGotoLabel.Add(New);
    end;

    CalcCount := 0;
    miGotoRecord.Clear;
    for I := 0 to ActiveViewForm.DatSrc.NotebookCount - 1 do

      if (ActiveViewForm.DatSrc.NoteBook[I].ID = 2) and (ConfigUnit.Config.ProgramMode <> pmRADIOAVIONICA_MAV) or (ActiveViewForm.DatSrc.NoteBook[I].ID = 3) and (ConfigUnit.Config.ProgramMode = pmRADIOAVIONICA_MAV) then
      begin
        New := TTBItem.Create(Self);

        { if ActiveViewForm.DatSrc.SkipBackMotion then ActiveViewForm.DatSrc.GetCoord(ActiveViewForm.DatSrc.Notebook[I].SysCoord, CoordParams)
          else }
        ActiveViewForm.DatSrc.DisToCoordParams(ActiveViewForm.DatSrc.NoteBook[I].DisCoord, CrdParams);

        New.Caption := LangTable.Caption['Common:Coordinate'] + ': ' + RealCrdToStr(CrdParamsToRealCrd(CrdParams, ActiveViewForm.DatSrc.Header.MoveDir), 1) + '; ' + LangTable.Caption[ { 'Common:Code:' } 'Common:Def.code'] + ' ' + HeaderStrToString(ActiveViewForm.DatSrc.NoteBook[I].Defect);
        New.ImageIndex := -1;
        New.Tag := I;
        New.OnClick := MainForm.GotoLabelClick;
        New.Name := 'FileLabel_2' + IntToStr(I);
        miGotoRecord.Add(New);
        Inc(J);
        Inc(CalcCount);
      end;

    if (ActiveViewForm.DatSrc.NotebookCount = 0) or (CalcCount = 0) then
    begin
      New := TTBItem.Create(Self);
      New.Caption := LangTable.Caption['Common:Empty'];
      New.Tag := -1;
      New.OnClick := nil;
      New.Name := 'NoneFileLabel2';
      miGotoRecord.Add(New);
    end;

    SetLength(List, 0);
    for I := 0 to High(ActiveViewForm.DatSrc.HSList) do
    begin
      if (Length(List) > 0) and (List[ High(List)].Crd = ActiveViewForm.DatSrc.HSList[I].DisCoord) then
      begin
        // case ActiveViewForm.DatSrc.HSList[I].HSMode of
        // 1: List[High(List)].Name:= List[High(List)].Name + ' (' + LangTable.Caption['Common:Record'] + ')';
        // 2: List[High(List)].Name:= List[High(List)].Name + ', ' + IntToStr(ActiveViewForm.DatSrc.HSList[I].HSCh);
        // end;
      end
      else
      with ActiveViewForm.DatSrc do
      begin
        SetLength(List, Length(List) + 1);
        List[ High(List)].Idx := (I + 1);
        List[ High(List)].Type_:= 1;
        List[ High(List)].Crd := HSList[I].DisCoord;
                                        //NO_LANGUAGE
        List[High(List)].Name:= Format(' - ПЭП %d° %s', [Config.HandChannelByIdx[HSList[I].Header.HandChNum].Angle, LangTable.Caption['Common:' + GetInspMethodTextId(Config.HandChannelByIdx[HSList[I].Header.HandChNum].Method, False)]]);
        // case ActiveViewForm.DatSrc.HSList[I].HSMode of
        // 1: List[High(List)].Name:= ' - (' + LangTable.Caption['Common:Record'] + ')';
        // 2: List[High(List)].Name:= ' - Канал: ' + IntToStr(ActiveViewForm.DatSrc.HSList[I].HSCh);
        // end;
      end;
    end;

    for I := 0 to High(ActiveViewForm.DatSrc.HeadScanList) do
    begin
      SetLength(List, Length(List) + 1);
      List[ High(List)].Idx := - (I + 1);
      List[ High(List)].Type_:= 2;
      List[ High(List)].Crd := ActiveViewForm.DatSrc.HeadScanList[I].DisCrd;
                                      //NO_LANGUAGE
      List[High(List)].Name:= {Format(}' - Сканер головки рельса'{, [Config.HandChannelByIdx[HSList[I].Header.HandChNum].Angle, LangTable.Caption['Common:' + GetInspMethodTextId(Config.HandChannelByIdx[HSList[I].Header.HandChNum].Method, False)]])};
    end;

    miHandScan.Clear;

    for I := 0 to High(List) do
    begin
      ActiveViewForm.DatSrc.DisToCoordParams(List[I].Crd, CrdParams);

      New := TTBItem.Create(Self);
      New.ImageIndex := -1;
      New.Caption := RealCrdToStr(CrdParamsToRealCrd(CrdParams, ActiveViewForm.DatSrc.Header.MoveDir), 1) + List[I].Name;

      New.Tag := List[I].Idx;
      New.OnClick := MainForm.TBSubmenuItem4Click;
      New.Name := 'FileLabel__' + IntToStr(I);
      miHandScan.Add(New);
      Inc(J);
    end;
    SetLength(List, 0);

    if (Length(ActiveViewForm.DatSrc.HSList) = 0) and (Length(ActiveViewForm.DatSrc.HeadScanList) = 0) then
    begin
      New := TTBItem.Create(Self);
      New.Caption := LangTable.Caption['Common:Empty'];
      New.Tag := -1;
      New.OnClick := nil;
      New.Name := 'NoneFileLabel1';
      miHandScan.Add(New);
    end;

    SkipFlag := False;
{$IFDEF REC}
    RecInt.Enabled := True;
    RecInt.ChangeWin(ActiveViewForm);
{$ENDIF REC}
  end
  else if Sender is THandScanForm then
  begin

    if ROSTOV_Exists then ROSTOV.SetActiveFormData(nil, nil);

    miGotoRecord.Enabled := False;
    miHandScan.Enabled := False;
    miMarkOnBScan.Enabled := False;
    miBothRail.Enabled := False;
    miLeftRail.Enabled := False;
    miRightRail.Enabled := False;
    miRail.Enabled := False;
    miTape.Enabled := False;
    miTapeReduce1.Enabled := False;
    miTapeReduce2.Enabled := False;
    miDonAmpl.Enabled := False;
    miGoto.Enabled := False;
    miGotoLabel.Enabled := False;
    miFilePar.Enabled := False;
    miOperatorLog.Enabled := False;
    miUncontrolZones.Enabled := False;
    miViewTestRecord.Enabled := False;
    miSameCoordFiles.Enabled := False;
    miViewMap.Enabled := False;
    miSensGraph.Enabled := False;
    TBItem3.Enabled := False;
    miSavePiece.Enabled := False;
    miIntegrityChecking.Enabled := False;
    miOnlyOtezdChannels.Enabled := False;
    miOnlyNaezdChannels.Enabled := False;
    miAllChannels.Enabled := False;
    miBackMotion.Enabled := False;
    miZoomList.Enabled := False;
    miClose.Enabled := False;
    tbAmplThList.Enabled := False;
    tbViewCh.Enabled := False;
    tbDrawRail.Enabled := False;
    tbZoomList.Enabled := False;
    tbBScan.Enabled := False;
    miAutoDecodingItem.Enabled := False;
    TopographTBItem.Enabled := False;

    tbGoto.Enabled := False;
    miEchoSizeByAmpl.Enabled := False;
    miPointSize.Enabled := False;
    miViewAutomaticSearchRes.Enabled := False;
    miViewAC.Enabled := False;
    miEchoColorByAmpl.Enabled := False;
{$IFDEF REC}
    RecInt.Enabled := False;
    RecInt.ChangeWin(nil);
{$ENDIF REC}
    tbNewFileRec.Enabled := False;
    tbVideo.Enabled := False;
    tbNewCVSRec.Enabled := False;
    tbNotebook.Enabled := False;
    miNotebook.Enabled := False;
    miNotNewRec.Enabled := False;
    miExportToNORDCO.Enabled := False;
{$IFDEF ECHOFILTER}
//    tbFilter.Enabled := False;
    miFiltrMenu.Enabled := False;
    Filtr_Calc.Enabled := True;
    Filtr_Hide.Enabled := False;
    Filtr_Mark.Enabled := False;
    Filtr_Off.Enabled := False;
{$ENDIF}
    // tbSound.Enabled:= False;

    // tbNoSwed.Enabled:= False;
    // tbSwed1.Enabled:= False;
    // tbSwed2.Enabled:= False;
    // tbAsRail.Enabled:= False;

    AmplThTrackBar.Enabled := True;
    tbViewTh.Enabled := True;
    Panel2.Font.Color := clBlack;

    InfoBarForm.FileInfoPanel.Visible := False;
    // InfoBarForm.HandScanPanel.Visible:= True;

    AmplThTrackBar.Position := THandScanForm(Sender).AmplThTrackBarPosition;


    // g

    if Assigned(THandScanForm(Sender).DatSrc) then
    begin
      aa := THandScanForm(Sender).DatSrc.Config.AmplValue[AmplThTrackBar.Position];
      Panel2.Caption := LangTable.Caption['MenuView:Threshold2'] + ' ' + IntToStr(aa) + ' ' + LangTable.Caption['Common:dB'];
    end;

    // Panel2.Caption:= LangTable.Caption['MenuView:Threshold2'] + ' ' + IntToStr(THandScanForm(Sender).DatSrc.Config.AmplValue[AmplThTrackBar.Position]) + ' ' + LangTable.Caption['Common:dB'];
  end
  else
  begin
    if ROSTOV_Exists then
    begin
      ROSTOV.SetActiveFormData(nil, nil);
    end;

    miGotoRecord.Enabled := False;
    miHandScan.Enabled := False;
    miMarkOnBScan.Enabled := False;
    miBothRail.Enabled := False;
    miLeftRail.Enabled := False;
    miRightRail.Enabled := False;
    miRail.Enabled := False;
    miTape.Enabled := False;
    miTapeReduce1.Enabled := False;
    miTapeReduce2.Enabled := False;
    miDonAmpl.Enabled := False;
    miGoto.Enabled := False;
    miGotoLabel.Enabled := False;
    miFilePar.Enabled := False;
    miOperatorLog.Enabled := False;
    miUncontrolZones.Enabled := False;
    miViewTestRecord.Enabled := False;
    miSameCoordFiles.Enabled := False;
    miViewMap.Enabled := False;
    miSensGraph.Enabled := False;
    TBItem3.Enabled := False;
    miSavePiece.Enabled := False;
    miIntegrityChecking.Enabled := False;
    miOnlyOtezdChannels.Enabled := False;
    miOnlyNaezdChannels.Enabled := False;
    miAllChannels.Enabled := False;
    miBackMotion.Enabled := False;
    miZoomList.Enabled := False;
    miClose.Enabled := False;
    tbAmplThList.Enabled := False;
    tbViewCh.Enabled := False;
    tbDrawRail.Enabled := False;
    tbZoomList.Enabled := False;
    tbBScan.Enabled := False;
    miAutoDecodingItem.Enabled := False;
    TopographTBItem.Enabled := False;

    tbGoto.Enabled := False;
    miPointSize.Enabled := False;
    miViewAutomaticSearchRes.Enabled := False;
    miViewAC.Enabled := False;
    miEchoSizeByAmpl.Enabled := False;
    miEchoColorByAmpl.Enabled := False;
    // tbNoSwed.Enabled:= False;
    // tbSwed1.Enabled:= False;
    // tbSwed2.Enabled:= False;
    // tbAsRail.Enabled:= False;
{$IFDEF REC}
    RecInt.Enabled := False;
    RecInt.ChangeWin(nil);
{$ENDIF REC}
    tbNewFileRec.Enabled := False;
    tbVideo.Enabled := False;
    tbNewCVSRec.Enabled := False;
    tbNotebook.Enabled := False;
    miNotebook.Enabled := False;
    miNotNewRec.Enabled := False;
    miExportToNORDCO.Enabled := False;
    // tbSound.Enabled:= False;
{$IFDEF ECHOFILTER}
//    tbFilter.Enabled := False;
    miFiltrMenu.Enabled := False;
    Filtr_Calc.Enabled := True;
    Filtr_Hide.Enabled := False;
    Filtr_Mark.Enabled := False;
    Filtr_Off.Enabled := False;
{$ENDIF}
    AmplThTrackBar.Enabled := False;
    tbViewTh.Enabled := False;
    Panel2.Font.Color := clSilver;
    TBDock1.Refresh;
  end;
  LastSender := Sender;
end;

// ------------------------------------------------------------------------------

function TMainForm.OpenFile(FN: string; OpenMininized: Boolean = False; SkipHistory: Boolean = False; DeleteViewedFile: Boolean = False): TAvk11ViewForm;
var
  New: TAvk11ViewForm;

begin

  New := TAvk11ViewForm.Create(nil);
  New.SetInfoBarForm(InfoBarForm);
  New.SetInfoBarForm2(InfoBarForm2);
  New.OnChangeDisplayMode := DisplayModeToControl;
  New.OnChangeADDtoNoteBook := ChangeADDtoNoteBook;
  New.OnChangeSoundFlag := ChangeSoundFlag;
  New.DeleteViewedFile := DeleteViewedFile;
  Result := New;

  if not New.LoadFile(FN) then
  begin
    if eIncorrectFileVersion in New.DatSrc.Error then ShowMessage(LangTable.Caption['Messages:NeedNewVer']);
    if eFileNotFound in New.DatSrc.Error then ShowMessage(LangTable.Caption['Messages:FileNotFound']);
    if eIncorrectFile in New.DatSrc.Error then ShowMessage(LangTable.Caption['Messages:BadFile']);
    if eUnknownError in New.DatSrc.Error then ShowMessage(LangTable.Caption['Messages:UnknownError']);
    New.Free;
    Result := nil;
    Exit;
  end;

  New.Display.Show0CoordEvent := TBItem4.Checked;
  New.Display.Show0EchoCountEvent := TBItem6.Checked;
  New.Display.SetViewMode(vmAllLines);
  New.Display.DatSrc.Config.SetZeroProbeChannelsViewMode(T_DFC_ZeroProbeChannelsViewMode(Config.ZeroProbeChannelsViewMode));

  if CompareMem(@New.DatSrc.Header.DeviceID, @MIGView, 7) then
  begin
    tbNotebook.Visible := False;
    tbNewFileRec.Enabled := False;
    tbVideo.Enabled := False;
    tbNewCVSRec.Enabled := False;
    tbGoto.Visible := False;
    tbOpen.Visible := False;
    tbBase.Visible := False;
    tbBScan.Visible := False;
    tbViewCh.Visible := False;
    tbDrawRail.Visible := False;
    tbZoomList.Visible := False;
    TBToolbar1.Visible := False;
  end;

  DisplayModeToControl(New);
  if not SkipHistory then Config.AddToOpenFilesHistory(FN);

  if OpenMininized then New.WindowState := wsMinimized
                   else if Config.OpenMaximazed then New.WindowState := wsMaximized;

  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.MetallSensorValue0 := Config.MetalSensorValue0State;
    ActiveViewForm.Display.MetallSensorValue1 := Config.MetalSensorValue1State;

    ActiveViewForm.Display.MetallSensorZoneState := Config.MetallSensorZoneState;
    ActiveViewForm.Display.UnstableBottomSignalState := Config.UnstableBottomSignalState;
    ActiveViewForm.Display.ZerroProbeModeState := Config.ZerroProbeModeState;
    ActiveViewForm.Display.PaintSystemStateState := Config.PaintSystemStateState;
    ActiveViewForm.Display.PaintSystemViewResState := Config.PaintSystemViewResState;
    ActiveViewForm.Display.PaintSystemViewResDebug := Config.PaintSystemViewResDebug;
  //  ActiveViewForm.Display.AutomaticSearchResState := Config.PaintSystemViewResState;

    ActiveViewForm.Display.DebugView2 := TBItem27.Checked;
    ActiveViewForm.Display.DebugView1 := TBItem27.Checked;

    if (Config.ProgramMode = pmRADIOAVIONICA_MAV) then
    begin
      ActiveViewForm.Display.RailView := False;
      ActiveViewForm.Display.Reduction := 2;
      tbBScan.ImageIndex := 48;
      TBItem11.Checked := True;
    end;

    ActiveViewForm.Display.DrawSameCoordMode:= TBItem36.Checked;
  end;

  {$IFDEF MYDEBUG}
    miViewAC.Checked:= False;
    miViewAC.OnClick(nil);
    ActiveViewForm.Display.ViewAC := True;
    ActiveViewForm.Display.DrawSameCoordMode:= True;
  {$ENDIF}
end;

procedure TMainForm.KillSelection(StringGrid: TStringGrid);
var
  GridRect: TGridRect;

begin
  GridRect := StringGrid.Selection;
  GridRect.Top := 100;
  GridRect.Bottom := 100;
  GridRect.Left := 100;
  GridRect.Right := 100;
  StringGrid.Selection := GridRect;
end;

(*
  {$IFDEF USEKEY}
  procedure LoadDllUSB;
  var
  Handle: DWord;

  begin
  Handle:= LoadLibrary(PChar(ExtractFilePath(Application.ExeName) + 'usbkey.dll'));
  @InitLib:= GetProcAddress(Handle, PChar('initlib'));
  @FinishLib:= GetProcAddress(Handle, PChar('finishlib'));
  @QueryKey:= GetProcAddress(Handle, PChar('query'));
  end;
  {$ENDIF USEKEY}
*)

procedure TMainForm.FormCreate(Sender: TObject);
var
  New: TTBItem;
  I: Integer;
  // ProgDir: string;
  ErrMsg: string;
  Ini: TStringList;
  {
    function GetBitMap(Rt: TRect): TBitMap;
    begin
    Result:= TBitMap.Create;
    //  SelRailImageList.GetBitmap(2, Result);
    //  Result.Assign(Image1.Picture.Bitmap);
    Result.Transparent:= True; // True; //  TransparentColor:= Image1.
    Result.TransparentColor:= RGB(255, 0, 255);
    //  Result.
    Result.SetSize(Rt.Right - Rt.Left + 1, Rt.Bottom - Rt.Top + 1);
    //  Image1.Picture.Bitmap.Canvas.CopyRect(Rect(0, 0, Result.Width - 1, Result.Height - 1), Result.Canvas, Rt);

    Result.Canvas.CopyRect(Rect(0, 0, Result.Width - 1, Result.Height - 1), Image1.Picture.Bitmap.Canvas, Rt);

    end;
  }
begin
  LastSetROSTOVActiveViewForm:= nil;
  MainFormClose:= False;
  ForceUGOUSW:= False;

//    ShowMessage(IntToStr(SizeOf(TFileHeader)));

//    ShowMessage(IntToStr(SizeOf(THeaderBigStr)));
//    ShowMessage(IntToStr(SizeOf(edNORDCO_Rec)));
//    ShowMessage(IntToStr(SizeOf(edAirBrush)));
//    ShowMessage(IntToStr(SizeOf(ShortInt)));
//    ShowMessage(IntToStr(SizeOf(ShortInt)));
  FPaintSystemDebugMode := False;
  {
    SelRailImageList.Clear;
    SelRailImageList.Width:= 68 - 4 - 4;
    //  SelRailImageList.Masked:= True;
    SelRailImageList.DrawingStyle:= dsTransparent;

    SelRailImageList.Add(GetBitMap(Rect(04, 0, 68 - 4, 15)), nil);
    SelRailImageList.Add(GetBitMap(Rect(04, 16, 68 - 4, 16 + 15)), nil);
    SelRailImageList.Add(GetBitMap(Rect(04, 2 * 16, 68 - 4, 2 * 16 + 15)), nil);

    //  SelRailImageList.
    //  SelRailImageList.GetBitmap(0, GetBitMap(Rect(5, 2, 5+68, 2+15)));
    SelRailImageList.Insert(0, GetBitMap(Rect(0, 0, 68, 15)), nil);

    }
{$IFDEF ROSTOV}
ROSTOV:= TAutodec.Create;
{$ENDIF}

{$IFDEF NOCONVERTTOAVIKON11}
  TBSeparatorItem27.Visible := False;
  TBItem30.Visible := False;
  TBItem31.Visible := False;
{$ENDIF}
  SaveDialog1.InitialDir := Config.SavePath;
  SaveState := False;


{$IFDEF AVIKON16}
  tbIE.Visible := False;
  tbFTP.Visible := False;
  tbMailTo.Visible := False;
  tbHelp.Visible := False;
  // tbAbout.Visible:= False;

  Sep4.Visible := False;
  miFileDownload.Visible := False;
  miFileDownload2.Visible := False;
  miFileDownloadList.Visible := False;
  miBUIWork.Visible := False;
  miIntegrityChecking.Visible := False;
  miSensGraph.Visible := False;
{$ENDIF}
{$IFDEF FILUSX17}
  tbIE.Visible := False;
  tbFTP.Visible := False;
  tbMailTo.Visible := False;
  tbHelp.Visible := False;
  miFileDownloadList.Visible := False;
  miIntegrityChecking.Visible := False;
  miSensGraph.Visible := False;
  miFiltrMenu.Visible := True;
//  TBSeparatorItem23.Visible := False;
  tbDrawRail.Visible := False;
  TBSeparatorItem9.Visible := False;
  miRightRail.Visible := False;
  miLeftRail.Visible := False;
  miBothRail.Visible := False;
  miMarkOnBScan.Enabled := False;
{$ENDIF}
{$IFDEF USEKEY}
  KeyExist := False;
{$ENDIF USEKEY}

{$IFNDEF VIEWBYKM}
  TBItem3.Visible := False;
{$ENDIF USEKEY}
{$IFDEF AVIKON12}
  tbDrawRail.Visible := False;
  TBSeparatorItem9.Visible := False;
  miRightRail.Visible := False;
  miLeftRail.Visible := False;
  miBothRail.Visible := False;
  miMarkOnBScan.Enabled := False;
{$ENDIF}
{$IFDEF ECHOFILTER}
//  tbFilter.Visible := True;
{$ENDIF}

  LastAForm := nil;

  FOpenFileFlag := True;
  if not(ThemeServices.ThemesAvailable and ThemeServices.ThemesEnabled) then
    TBBackground1.Free;


  // Language.WorkLang:= Config.WorkLang;
  miEchoSizeByAmpl.Checked := Config.EchoSizeByAmpl;
  miEchoColorByAmpl.Checked := Config.EchoColorByAmpl;

  ErrMsg := '';

  // ProgDir:= ExtractFilePath(Application.ExeName);
  // if not FileExists(ProgDir + 'Language.Dat') then ErrMsg:= ErrMsg + 'Language.Dat; ';
  // if not FileExists(ProgDir + 'av11.dll') then ErrMsg:= ErrMsg + 'av11.dll; ';
  (* if not FileExists(ProgDir + 'Language.Dat') then ErrMsg:= ErrMsg + 'Language.Dat; ';

    {$IFDEF AVIKON16}
    if not FileExists(ProgDir + 'EGO US.ini') then ErrMsg:= ErrMsg + 'EGO US.ini; ';
    {$ENDIF}
    {$IFDEF ALLAVIKON}
    if not FileExists(ProgDir + 'AVIKON11.ini') then ErrMsg:= ErrMsg + 'AVIKON11.ini; ';
    {$ENDIF}
    {$IFDEF AVIKON11}
    if not FileExists(ProgDir + 'AVIKON11.ini') then ErrMsg:= ErrMsg + 'AVIKON11.ini; ';
    {$ENDIF}
    {$IFDEF AVIKON12}
    if not FileExists(ProgDir + 'AVIKON12.ini') then ErrMsg:= ErrMsg + 'AVIKON12.ini; ';
    {$ENDIF} *)

  if not FileExists(DataFileDir + 'map.ini') then
    ErrMsg := ErrMsg + 'map.ini; ';


  if ErrMsg <> '' then
  begin
    Delete(ErrMsg, Length(ErrMsg) - 1, 2);
    ShowMessage(LangTable.Caption['MainMenu:Programself-diagnosticfault.Filesnotfound'] + ': ' + ErrMsg + '.');
  end;


  MainForm.Color := clBtnShadow;
  { if Screen.PixelsPerInch <> 96 then ShowMessage(LangTable.Caption['Messages:']);

    if (Screen.DesktopWidth < 1024) or
    (Screen.DesktopHeight < 768) or
    (GetDeviceCaps(Canvas.Handle, BITSPIXEL) <= 8) then
    begin
    ShowMessage(LangTable.Caption['MainMenu:?']);
    Timer1.Enabled:= True;
    end;
  }
  InfoBarForm := TInfoBarForm.Create(nil);

  // InfoBarForm.Parent:= TBToolWindow4;
  InfoBarForm.Parent := Panel6;
  // .  TBToolWindow4.Visible:= true;

  InfoBarForm.FileInfoPanel.Parent := Panel6;
  // InfoBarForm.HandScanPanel.Parent:= TBToolWindow4;

  InfoBarForm.BringToFront;
  InfoBarForm.Visible := True;
  InfoBarForm.OnInfoClick := miFileParClick;
  InfoBarForm.vFileInfo := False;
  InfoBarForm.vNotebook := False;

  InfoBarForm.vRail := True;

  if Config.CoordType then
  begin
    InfoBarForm.vCoord1 := True;
    InfoBarForm.Bevel12.Visible := True;
  end
  else
  begin
    InfoBarForm.vCoord1 := False;
    InfoBarForm.Bevel12.Visible := False;
  end;

  InfoBarForm.vCoord2 := True;
  InfoBarForm.vTime := True;
  InfoBarForm.CoordMode := False;

  /// //////////////////////////////////////

  InfoBarForm2 := TInfoBarForm2.Create(nil);
  InfoBarForm2.Parent := InfoPanel2;
  InfoBarForm2.FileInfoPanel.Parent := InfoPanel2;
  InfoBarForm2.BringToFront;
  InfoBarForm2.Visible := True;
  // InfoBarForm2.OnInfoClick:= miFileParClick;
  // InfoBarForm2.vFileInfo:= False;
  // InfoBarForm2.vNotebook:= False;
  // InfoBarForm2.vRail:= True;

  /// /////////////////////////////////////


  DataBase_:= TDataBase.Create( { ExtractFilePath(Application.ExeName) } DataFileDir + 'files.dat');
  DataBase_.Load;

  for I := 0 to High(ZoomList) do
  begin
    New := TTBItem.Create(Self);
    New.Caption := ''; // FloatToStr(ZoomList[I] / 100) + ' ' + LangTable.Caption['Common:mm/p'];
    New.Tag := ZoomList[I];
    New.OnClick := CntrToDisplayMode;
    New.AutoCheck := True;
    New.GroupIndex := 100;
    New.Name := 'Zoom' + IntToStr(I);

    // if ZoomList[I] = 500 then New.ShortCut:= ShortCut(Word('0'), [ssShift]);
    // if ZoomList[I] = 10000 then New.ShortCut:= ShortCut(Word('1'), [ssShift]);
    // if ZoomList[I] = 20000 then New.ShortCut:= ShortCut(Word('2'), [ssShift]);
    // if ZoomList[I] = 30000 then New.ShortCut:= ShortCut(Word('3'), [ssShift]);
    // if ZoomList[I] = 40000 then New.ShortCut:= ShortCut(Word('4'), [ssShift]);

    miZoomList.Add(New);
  end;
  Self.KeyPreview := True;

  SkipFlag := False;

  tbOpen.Visible := True;
  miDebug.Visible := False;
  {$IFDEF OPENDEBUGMENU}
  miDebug.Visible := True;
  {$ENDIF}

  // miDebug.Visible:= True;

  SetButtonText(Config.ButtonText);
  SetViewButtons(Config.ViewSelMetod);

  (*
    {$IFDEF REC}
    RecInt:= TRecInterface.Create;
    RecInt.Insert(miFileMan, 11, Self);
    {$ENDIF REC}

    {$IFDEF USEKEY}
    LoadDllUSB;
    {$ENDIF USEKEY}
  *)
  // InfoBarForm.DebugInfo:= True;
  {
    cbWorkLang.Items.Clear;
    for I := 0 to LangTable.GroupsCount - 1 do
    cbWorkLang.Items.Add(LangTable.GroupName[I]);
    cbWorkLang.ItemIndex:= 1;
    }

  LangTable.CurrentGroup := Config.WorkLang;

  // Установка языка выбранного при инсталляции

  // ShowMessage(DataFileDir + 'init.ini');


  if FileExists(ExtractFilePath(Application.ExeName) + 'init.ini') then
  // if FileExists(DataFileDir + 'init.ini') then

  begin

    Ini := TStringList.Create;
    Ini.LoadFromFile(ExtractFilePath(Application.ExeName) + 'init.ini');

    if Ini.Count >= 0 then
    begin
      if (Pos(UpperCase('English'), UpperCase(Ini[1])) <> 0) or (Pos(UpperCase('language=1'), UpperCase(Ini[1])) <> 0) then
      begin
        LangTable.CurrentGroup := 'English';
        Config.WorkLang := 'English';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
        // ShowMessage('E');
      end;

      if (Pos(UpperCase('French'), UpperCase(Ini[1])) <> 0) or (Pos(UpperCase('language=5'), UpperCase(Ini[1])) <> 0) then
      begin
        LangTable.CurrentGroup := 'French';
        Config.WorkLang := 'French';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
        // ShowMessage('F');
      end;

      if (Pos(UpperCase('Russian'), UpperCase(Ini[1])) <> 0) or (Pos(UpperCase('language=16'), UpperCase(Ini[1])) <> 0) then
      begin
        LangTable.CurrentGroup := 'Russian';
        Config.WorkLang := 'Russian';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
        // ShowMessage('R');
      end;

      if (Pos(UpperCase('Turkish'), UpperCase(Ini[1])) <> 0) or (Pos(UpperCase('language=102'), UpperCase(Ini[1])) <> 0) then
      begin
        LangTable.CurrentGroup := 'Turkish';
        Config.WorkLang := 'Turkish';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
        // ShowMessage('T');
      end;

      if (Pos(UpperCase('Spanish'), UpperCase(Ini[1])) <> 0) or (Pos(UpperCase('language=3'), UpperCase(Ini[1])) <> 0) then
      begin
        LangTable.CurrentGroup := 'Spanish';
        Config.WorkLang := 'Spanish';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
        // ShowMessage('S');
      end;

      if (Pos(UpperCase('German'), UpperCase(Ini[1])) <> 0) or (Pos(UpperCase('language=3'), UpperCase(Ini[1])) <> 0) then
      begin
        LangTable.CurrentGroup := 'German';
        Config.WorkLang := 'German';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
        // ShowMessage('G');
      end;

      if (Pos(UpperCase('Hungarian'), UpperCase(Ini[1])) <> 0) or (Pos(UpperCase('language=3'), UpperCase(Ini[1])) <> 0) then
      begin
        LangTable.CurrentGroup := 'Hungarian';
        Config.WorkLang := 'Hungarian';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
        // ShowMessage('H');
      end;


    end;

    if Ini.Count >= 3 then
    begin
      if Pos('GEISMAR', UpperCase(Ini[2])) <> 0 then Config.ProgramMode := pmGEISMAR;
      if Pos('RADIOAVIONICA', UpperCase(Ini[2])) <> 0 then Config.ProgramMode := pmRADIOAVIONICA;
      if Pos('RADIOAVIONICA_MAV', UpperCase(Ini[2])) <> 0 then Config.ProgramMode := pmRADIOAVIONICA_MAV;
      if Pos('RADIOAVIONICA_KZ', UpperCase(Ini[2])) <> 0 then Config.ProgramMode := pmRADIOAVIONICA_KZ;
    end;

    if Ini.Count > 3 then
    begin
      Config.BasePath := Ini[3];
    end;

    Ini.Free;

    DeleteFile(ExtractFilePath(Application.ExeName) + 'init.ini');
    Config.Save;
  end;
{
  Config.ProgramMode := pmGEISMAR;
  LangTable.CurrentGroup := 'English';
  Config.WorkLang := 'English';
  OnChangeLanguage(nil);
  OnChangeLanguage_(Self);
}
  OnChangeLanguage(nil);
  // TBToolbar2.Left:= MainToolbar.Width;
  TBDock1.ArrangeToolbars;

  miMetallSensorValue0.Visible := Config.ShowSensor1DataFlag;
  miMetallSensorValue1.Visible := Config.ShowSensor1DataFlag;
  TBSeparatorItem21.Visible := Config.ShowSensor1DataFlag;
  miMarkOnBScan.Checked:= Config.MarkOnBScan;

  miMetallSensorZone.Checked := Config.MetallSensorZoneState;
  miUnstableBottomSignal.Checked := Config.UnstableBottomSignalState;
  miZerroProbeMode.Checked := Config.ZerroProbeModeState;
  miPaintSystemState.Checked := Config.PaintSystemStateState;
  miMetallSensorValue0.Checked := Config.MetalSensorValue0State;
  miMetallSensorValue1.Checked := Config.MetalSensorValue1State;
  miViewPaintSystemState.Checked := Config.PaintSystemViewResState;
  miViewAutomaticSearchRes.Checked := Config.AutomaticSearchResState;

  miMetallSensorValue0.Checked := Config.MetalSensorValue0State;
  miMetallSensorValue1.Checked := Config.MetalSensorValue1State;

  miViewPaintSystemState.Visible := False;
  miMetallSensor.Visible := False;
  TBSeparatorItem21.Visible := False;
  miUnstableBottomSignal.Visible := False;
  miZerroProbeMode.Visible := False;
  miPaintSystemState.Visible := False;

  if (Config.ProgramMode = pmRADIOAVIONICA) or (Config.ProgramMode = pmRADIOAVIONICA_KZ) then
  begin
    miViewAutomaticSearchRes.Visible:= True;
    miViewAC.Visible:= True;
    miViewMap.Visible:= True;
    miUncontrolZones.Visible:= True;
    TBSeparatorItem23.Visible:= True;
    miFiltrMenu.Visible:= False;
    miViewTestRecord.Visible := True;
//    miViewTestRecordSep.Visible := True;
  end
  else
  if Config.ProgramMode = pmRADIOAVIONICA_MAV then
  begin
    miViewAutomaticSearchRes.Visible:= False;
    miViewMap.Visible:= False;
    miViewAC.Visible:= False;
    miUncontrolZones.Visible:= False;
    TBSeparatorItem23.Visible:= False;
    miFiltrMenu.Visible:= False;
    miViewTestRecord.Visible := False;
//    miViewTestRecordSep.Visible := False;
  end
  else
  if Config.ProgramMode = pmGEISMAR then
  begin
    miViewAutomaticSearchRes.Visible:= False;
    miViewMap.Visible:= False;
    miUncontrolZones.Visible:= False;
    miViewAC.Visible:= False;
    TBSeparatorItem23.Visible:= False;
    miFiltrMenu.Visible:= True;
    miViewTestRecord.Visible := False;
//    miViewTestRecordSep.Visible := False;
  end;

  if (Config.ProgramMode = pmRADIOAVIONICA_MAV) then
  begin
    miInNordico.Checked := True;
    miExportToNORDCO.Visible := True;
  end
  else
  begin
    tbNewFileRec.Visible := True;
    {$IFDEF ESTONIA}
    tbVideo.Visible := True;
    tbVideoSep.Visible := True;
    {$ENDIF}
    miExportToNORDCO.Visible := False;
  end;

  if Config.CDUVer then
  begin
    miOpenFile.Enabled := False;
    tbOpen.Enabled := False;
  end;

  case Config.EchoSize of
    -1: miSmallPoint.Checked:= true;
     0: miMediumPoint.Checked:= true;
     1: miLargePoint.Checked:= true;
  end;

//  AmplThTrackBar.Position:= Config.ViewThresholdValue;

  {$IFDEF ROSTOV}
  ROSTOV_Exists:= ROSTOV.Exists;
  {$ENDIF}
  {$IFNDEF ROSTOV}
  ROSTOV_Exists:= False;
  miAutoDecodingItem.Visible:= False;
//  miMagnetView.Visible:= False;
  {$ENDIF}

  if ROSTOV_Exists then
  begin
    miAutoDecodingItem.LinkSubitems:= ROSTOV.MenuItem;
    miAutoDecodingItem.Enabled:= True;
    TopographTBItem.Visible:= True;
    TopographTBItem.LinkSubitems:= ROSTOV.MenuItem;
  end
  else
  begin
    //miTopographItem.Enabled:= False;
    TopographTBItem.Visible:= False;
  end;

  GPS_SaveDialog.Title:= 'Сохранение ГНСС трека. Задайте имя файла и каталог'; // NoTranslate

//  miMagnetView.Checked:= Config.MagnetView;

end;

procedure TMainForm.FormActivate(Sender: TObject);
begin
  DisplayModeToControl(Self.ActiveMDIChild { ActiveViewForm } );
end;

procedure TMainForm.tbReduceClick(Sender: TObject);
begin
  ControlToDisplayMode;
end;

procedure TMainForm.GotoLabelClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    with ActiveViewForm.Display do
    begin
      if (Sender is TTBItem) and (Copy(TTBItem(Sender).Name, 1, 11) = 'FileLabel_1') then
      begin
        CenterDisCoord := TTBItem(Sender).Tag;
        Refresh;
      end;

      if (Sender is TTBItem) and (Copy(TTBItem(Sender).Name, 1, 11) = 'FileLabel_2') then
      begin
        if ActiveViewForm.DatSrc.SkipBackMotion then
          CenterDisCoord := ActiveViewForm.DatSrc.NoteBook[TTBItem(Sender).Tag].SysCoord
        else
          CenterDisCoord := ActiveViewForm.DatSrc.NoteBook[TTBItem(Sender).Tag].DisCoord;
        Refresh;
      end;
    end;
end;

procedure TMainForm.RightEchoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  s1, s2: string;

begin
  with (Sender as TStringGrid), Canvas do
  begin
    Brush.Style := bsSolid;
    FillRect(Rect);
    Brush.Style := bsClear;
    if TextWidth(Cells[ACol, ARow]) < (Rect.Right - Rect.Left) then
      TextOut((Rect.Left + Rect.Right - TextWidth(Cells[ACol, ARow])) div 2, (Rect.Top + Rect.Bottom - TextHeight(Cells[ACol, ARow])) div 2, Cells[ACol, ARow])
    else
    begin
      s1 := Copy(Cells[ACol, ARow], 1, Pos(' ', Cells[ACol, ARow]) - 1);
      s2 := Copy(Cells[ACol, ARow], Pos(' ', Cells[ACol, ARow]) + 1, MaxInt);
      TextOut((Rect.Left + Rect.Right - TextWidth(s1)) div 2, Rect.Top + (Rect.Bottom - Rect.Top) div 4 - TextHeight(s1) div 2 - 1, s1);
      TextOut((Rect.Left + Rect.Right - TextWidth(s2)) div 2, Rect.Top + 3 * (Rect.Bottom - Rect.Top) div 4 - TextHeight(s2) div 2 - 3, s2);
    end;

  end;
end;

procedure TMainForm.tbNewFileRecClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    tbNewFileRec.Checked := not tbNewFileRec.Checked;
    if tbNewFileRec.Checked and tbVideo.Checked then tbVideo.OnClick(nil);


    if (Config.ProgramMode <> pmRADIOAVIONICA_MAV) then
      ActiveViewForm.ADDtoNoteBook := tbNewFileRec.Checked
    else
      ActiveViewForm.ADDtoNordCo := tbNewFileRec.Checked;
  end;
  {
    if ActiveViewForm <> nil then
    begin
    if miInFile.Checked then
    begin
    tbNewCVSRec.Checked:= not tbNewCVSRec.Checked;
    ActiveViewForm.ADDtoNoteBook:= tbNewCVSRec.Checked;
    end
    else
    begin
    tbNewCVSRec.Checked:= not tbNewCVSRec.Checked;
    ActiveViewForm.ADDtoNordCo:= tbNewCVSRec.Checked;
    end;
    end;
  }
end;

procedure TMainForm.tbNewCVSRecClick(Sender: TObject);
begin
  {
    if ActiveViewForm <> nil then
    begin
    if miInFile.Checked then
    begin
    tbNewCVSRec.Checked:= not tbNewCVSRec.Checked;
    ActiveViewForm.ADDtoNoteBook:= tbNewCVSRec.Checked;
    end
    else
    begin
    tbNewCVSRec.Checked:= not tbNewCVSRec.Checked;
    ActiveViewForm.ADDtoNordCo:= tbNewCVSRec.Checked;
    end;
    end;
    }
end;

procedure TMainForm.tbNotebook_Click(Sender: TObject);
begin
  OpenNotebook(0);
end;

procedure TMainForm.OpenNotebook(Index: Integer);
var
  NBForm: TNoteBookForm;

begin
  if ActiveViewForm <> nil then
  begin
    NBForm := TNoteBookForm.Create(nil);
    NBForm.MyOwner := ActiveViewForm;
    NBForm.FormShow(nil);
    NBForm.Caption := LangTable.Caption['Notebook:NotebookforFile:'];
    if Index <> -1 then
      NBForm.Select(Index);
    NBForm.Showmodal;
    NBForm.Free;
    DisplayModeToControl(ActiveViewForm);
  end;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  I: Integer;

begin
  MainFormClose:= True;

  for I := 0 to Self.MDIChildCount - 1 do
    if Self.MDIChildren[I] is TAvk11ViewForm then
      TAvk11ViewForm(Self.MDIChildren[I]).Close;

  if Assigned(FileBaseForm) then
  begin
    FileBaseForm.Close;
    FileBaseForm := nil;
  end;
end;

procedure TMainForm.tbSoundClick(Sender: TObject);
var
  I: Integer;

begin
  // if ActiveViewForm <> nil then ActiveViewForm.SoundFlag:= tbSound.Checked;
  // if not tbSound.Checked then
  // begin
  // for I:= 0 to Self.MDIChildCount - 1 do
  // if Self.MDIChildren[I] is TAvk11ViewForm then TAvk11ViewForm(Self.MDIChildren[I]).Display.SoundFlag:= False;
  //
  // StopTone;
  // end;
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  ShiftState := Shift;
end;

procedure TMainForm.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  ShiftState := Shift;
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
  if Self.Width < 1200 - 34 then
  begin
    TBToolbar2.DockPos := 0;
    TBToolbar2.DockRow := 2;
  end
  else
  begin
    TBToolbar2.DockPos := 0;
    TBToolbar2.DockRow := 1;
  end;
end;

function TMainForm.ActiveViewForm: TAvk11ViewForm;
begin
  if Assigned(ActiveMDIChild) and (ActiveMDIChild is TAvk11ViewForm) then
  begin
    if not (TAvk11ViewForm(ActiveMDIChild).FormMode = fmSingleChannel) // Если в форма файла открыта форма ручника то это форма ручника
      then Result := TAvk11ViewForm(ActiveMDIChild)
      else Result := nil;
  end else Result := nil;
end;

function TMainForm.ActiveViewForm_: TAvk11ViewForm;
begin
  if Assigned(ActiveMDIChild) and (ActiveMDIChild is TAvk11ViewForm) then Result := TAvk11ViewForm(ActiveMDIChild)
                                                                     else Result := nil;
end;

function TMainForm.ActiveHandViewForm: THandScanForm;
begin
  Result := nil;
  if Assigned(ActiveMDIChild) and (ActiveMDIChild is THandScanForm)
    then Result := THandScanForm(ActiveMDIChild)
    else begin
           // Если в форма файла открыта форма ручника то это форма ручника
           if Assigned(ActiveMDIChild) and (ActiveMDIChild is TAvk11ViewForm) and (TAvk11ViewForm(ActiveMDIChild).FormMode = fmSingleChannel) then
           begin
             Result := THandScanForm(TAvk11ViewForm(ActiveMDIChild).HandSForm_);
           end;
         end;
end;

function TMainForm.ActiveHeadViewForm: THeadScanForm;
begin
  Result := nil;
  if Assigned(ActiveMDIChild) and (ActiveMDIChild is THandScanForm)
    then Result := THeadScanForm(ActiveMDIChild)
    else begin
           // Если в форма файла открыта форма ручника то это форма ручника
           if Assigned(ActiveMDIChild) and (ActiveMDIChild is TAvk11ViewForm) and (TAvk11ViewForm(ActiveMDIChild).FormMode = fmSingleChannel) then
           begin
             Result := THeadScanForm(TAvk11ViewForm(ActiveMDIChild).HeadSForm_);
           end;
         end;
end;

procedure TMainForm.TBSubmenuItem4Click(Sender: TObject);
var
  Idx: Integer;
begin
  if ActiveViewForm <> nil then
    with ActiveViewForm.Display do
    begin
      Idx:= TTBItem(Sender).Tag;
      if Idx > 0 then
      begin
        Idx:= Idx - 1;
        CenterDisCoord := ActiveViewForm.DatSrc.HSList[Idx].DisCoord;
        Refresh;
      end
      else
      begin
        Idx:= Abs(Idx) - 1;
        CenterDisCoord := ActiveViewForm.DatSrc.HeadScanList[Idx].DisCrd;
        Refresh;
      end
    end;
end;

procedure ExecuteFile(FExeName: string; FExeParams: array of string; FWait: Boolean = False);
var
  buffer: array [0 .. 511] of Char;
  TmpStr: string;
  I: Integer;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
  FChangeDir: Boolean;
  FMode: Word;

begin
  FChangeDir := False;
  FMode := SW_SHOWNORMAL;
  TmpStr := ExtractFileName(FExeName);
  for I := 0 to High(FExeParams) do
    TmpStr := TmpStr + ' ' + FExeParams[I];
  StrPCopy(buffer, TmpStr);
  if FChangeDir and (ExtractFilePath(FExeName) <> '') then
  begin
    try
      ChDir(ExtractFilePath(FExeName));
    except
      // On E:Exception do raise EAppExecChDir.Create(E.Message);
    end;
  end;

  { Execution }
  FillChar(StartupInfo, Sizeof(StartupInfo), #0);
  StartupInfo.cb := Sizeof(StartupInfo);
  StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
  StartupInfo.wShowWindow := FMode;
  if not CreateProcess(nil, buffer, { pointer to command line string }
    nil, { pointer to process security attributes }
    nil, { pointer to thread security attributes }
    False, { handle inheritance flag }
    CREATE_NEW_CONSOLE or { creation flags }
    NORMAL_PRIORITY_CLASS, nil, { pointer to new environment block }
    nil, { pointer to current directory name }
    StartupInfo, { pointer to STARTUPINFO }
    ProcessInfo) then
  begin { pointer to PROCESS_INF }
  end
  else if FWait then
  begin
    WaitforSingleObject(ProcessInfo.hProcess, INFINITE);
  end;
end;

procedure TMainForm.TBItem16Click(Sender: TObject);
var
  ViewEventForm: TViewEventForm;
  I: Integer;
  Name: string;

begin
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.DatSrc.AnalyzeFileBody_();

    ViewEventForm := TViewEventForm.Create(nil);
    ViewEventForm.Memo1.Clear;

    for I := 0 to 255 do
    begin
      Name:= '';
      case I of
        0: Name:= 'Сигналы В-развертки';
        EID_HandScan              : Name:= 'EID_HandScan, 0x82 - Ручник';
        EID_Ku                    : Name:= 'EID_Ku, 0x90 - Изменение условной чувствительности';
        EID_Att                   : Name:= 'EID_Att, 0x91 - Изменение аттенюатора';
        EID_TVG                   : Name:= 'EID_TVG, 0x92 - Изменение ВРЧ';
        EID_StStr                 : Name:= 'EID_StStr, 0x93 - Изменение положения начала строба';
        EID_EndStr                : Name:= 'EID_EndStr, 0x94 - Изменение положения конца строба';
        EID_HeadPh                : Name:= 'EID_HeadPh, 0x95 - Список включенных наушников';
        EID_Mode                  : Name:= 'EID_Mode, 0x96 - Изменение режима';
        EID_SetRailType           : Name:= 'EID_SetRailType, 0x9B - Настройка на тип рельса';
        EID_PrismDelay            : Name:= 'EID_PrismDelay, 0x9C - Изменение 2Тп (word)';
        EID_Stolb                 : Name:= 'EID_Stolb, 0xA0 - Отметка координаты';
        EID_Switch                : Name:= 'EID_Switch, 0xA1 - Номер стрелочного перевода';
        EID_DefLabel              : Name:= 'EID_DefLabel, 0xA2 - Отметка дефекта';
        EID_TextLabel             : Name:= 'EID_TextLabel, 0xA3 - Текстовая отметка';
        EID_StBoltStyk            : Name:= 'EID_StBoltStyk, 0xA4 - Нажатие кнопки БС';
        EID_EndBoltStyk           : Name:= 'EID_EndBoltStyk, 0xA5 - Отпускание кнопки БС';
        EID_Time                  : Name:= 'EID_Time, 0xA6 - Отметка времени';
        EID_StolbChainage         : Name:= 'EID_StolbChainage, 0xA7 - Отметка координаты Chainage';
        EID_ZerroProbMode         : Name:= 'EID_ZerroProbMode, 0xA8 - Режим работы датчиков 0 град';
        EID_LongLabel             : Name:= 'EID_LongLabel, 0xA9 - Протяженная отметка';
        EID_SpeedState            : Name:= 'EID_SpeedState, 0xAA - Скорость и Превышение скорости контроля';
        EID_ChangeOperatorName    : Name:= 'EID_ChangeOperatorName, 0xAB - Смена оператора (ФИО полностью)';
        EID_AutomaticSearchRes    : Name:= 'EID_AutomaticSearchRes, 0xAC - «Значимые» участки рельсов, получаемые при автоматическом поиске';
        EID_TestRecordFile        : Name:= 'EID_TestRecordFile, 0xAD - Файл записи контрольного тупика';
        EID_OperatorRemindLabel   : Name:= 'EID_OperatorRemindLabel, 0xAE - Отметки пути, заранее записанные в прибор для напоминания оператору (дефектные рельсы и другие)';
        EID_QualityCalibrationRec : Name:= 'EID_QualityCalibrationRec, 0xAF - Качество настройки каналов контроля';
        EID_Sensor1               : Name:= 'EID_Sensor1, 0xB0 - Сигнал датчика БС и Стрелки';
        EID_AirBrush              : Name:= 'EID_AirBrush, 0xB1 - Краскоотметчик';
        EID_PaintMarkRes          : Name:= 'EID_PaintMarkRes, 0xB2 - Сообщение о выполнении задания на краско-отметку';
        EID_AirBrushTempOff       : Name:= 'EID_AirBrushTempOff, 0xB3 - Временное отключение Краскоотметчика';
        EID_AirBrush2             : Name:= 'EID_AirBrush2, 0xB4 - Краскоотметчик с отладочной информацией';
        EID_AlarmTempOff          : Name:= 'EID_AlarmTempOff, 0xB5 - Временное отключение АСД по всем каналам';
        EID_StartSwitchShunter    : Name:= 'EID_StartSwitchShunter, 0xB6 - Начало зоны стрелочного перевода';
        EID_EndSwitchShunter      : Name:= 'EID_EndSwitchShunter, 0xB7 - Конец зоны стрелочного перевода';
        EID_Temperature           : Name:= 'EID_Temperature, 0xB8 - Температура';
        EID_DebugData             : Name:= 'EID_DebugData, 0xB9 - Отладочная информация';
        EID_PaintSystemParams     : Name:= 'EID_PaintSystemParams, 0xBA - Параметры работы алгоритма краскопульта';
        EID_UMUPaintJob           : Name:= 'EID_UMUPaintJob, 0xBB - Задание БУМ на краскоотметку';
        EID_ACData                : Name:= 'EID_ACData, 0xBC - Данные акустического контакта';
        EID_SmallDate             : Name:= 'EID_SmallDate, 0xBD - Малое универсальное событие';
        EID_RailHeadScaner        : Name:= 'EID_RailHeadScaner, 0xBE - Данные уточняющего сканера головки рельса';
        EID_SensAllowedRanges     : Name:= 'EID_SensAllowedRanges, 0xBF - Таблица разрешенных диапазонов Ку';
        EID_GPSCoord              : Name:= 'EID_GPSCoord, 0xC0 - Географическая координата';
        EID_GPSState              : Name:= 'EID_GPSState, 0xC1 - Состояние приемника GPS';
        EID_Media                 : Name:= 'EID_Media, 0xC2 - Медиаданные';
        EID_GPSCoord2             : Name:= 'EID_GPSCoord2, 0xC3 - Географическая координата со скоростью';
        EID_NORDCO_Rec            : Name:= 'EID_NORDCO_Rec, 0xC4 - Запись NORDCO';
        EID_SensFail              : Name:= 'EID_SensFail, 0xC5 - Отклонение условной чувствительности от нормативного значения';
        EID_AScanFrame            : Name:= 'EID_AScanFrame, 0xC6 - Кадр А-развертки';
        EID_MediumDate            : Name:= 'EID_MediumDate, 0xC7 - Среднее универсальное событие';
        EID_BigDate               : Name:= 'EID_BigDate, 0xC8 - Большое универсальное событие';
        EID_CheckSum              : Name:= 'EID_CheckSum, 0xF0 - Контрольная сумма';
        EID_SysCrd_SS             : Name:= 'EID_SysCrd_SS, 0xF1 - Системная координата короткая';
        EID_SysCrd_SF             : Name:= 'EID_SysCrd_SF, 0xF4 - Полная системная координата с короткой ссылкой';
        EID_SysCrd_FS             : Name:= 'EID_SysCrd_FS, 0xF9 - Системная координата короткая';
        EID_SysCrd_FF             : Name:= 'EID_SysCrd_FF, 0xFC - Полная системная координата с полной ссылкой';
        EID_SysCrd_NS             : Name:= 'EID_SysCrd_NS, 0xF2 - Системная координата новая, короткая';
        EID_SysCrd_NF             : Name:= 'EID_SysCrd_NF, 0xF3 - Системная координата новая, полная';
        EID_SysCrd_UMUA_Left_NF   : Name:= 'EID_SysCrd_UMUA_Left_NF, 0xF5 - Полная системная координата без ссылки БУМ A, левая сторона';
        EID_SysCrd_UMUB_Left_NF   : Name:= 'EID_SysCrd_UMUB_Left_NF, 0xF6 - Полная системная координата без ссылки БУМ В, левая сторона';
        EID_SysCrd_UMUA_Right_NF  : Name:= 'EID_SysCrd_UMUA_Right_NF, 0xF7 - Полная системная координата без ссылки БУМ A, правая сторона';
        EID_SysCrd_UMUB_Right_NF  : Name:= 'EID_SysCrd_UMUB_Right_NF, 0xF8 - Полная системная координата без ссылки БУМ В, правая сторона';
        EID_EndFile               : Name:= 'EID_EndFile, 0xFF - Конец файла';
      end;
      if Name <> '' then
      begin
        if ActiveViewForm.DatSrc.EventsCount[I] <> 0 then ViewEventForm.Memo1.Lines.Add(Format('0x%x - %8d ', [I, ActiveViewForm.DatSrc.EventsCount[I]]) + Name)
                                                     else ViewEventForm.Memo1.Lines.Add(Format('0x%x - нет', [I]));
      end;
    end;

    ViewEventForm.CreateList(ActiveViewForm);
    ViewEventForm.Showmodal;
    ViewEventForm.Release;
  end;
end;

procedure TMainForm.TBItem17Click(Sender: TObject);
var
  ViewFileErrorForm: TViewFileErrorForm;

begin (*
    {$IFNDEF FILUSX17}
    if ActiveViewForm <> nil then
    begin
    ViewFileErrorForm:= TViewFileErrorForm.Create(nil);
    ViewFileErrorForm.CreateList(ActiveViewForm);
    ViewFileErrorForm.ShowModal;
    ViewFileErrorForm.Release;
    end;
    {$ENDIF} *)
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  if Assigned(ROSTOV) then ROSTOV.Free;

  Config.SavePath := SaveDialog1.InitialDir;
{$IFDEF USEKEY}
  if KeyExist then
    FinishLib;
{$ENDIF USEKEY}
  InfoBarForm.Visible := False;
  InfoBarForm.FileInfoPanel.Parent := nil;
  // InfoBarForm.HandScanPanel.Parent:= nil;
  InfoBarForm.Parent := nil;
  InfoBarForm.Release;
end;

procedure TMainForm.ChangeADDtoNoteBook(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    tbNewFileRec.Checked := ActiveViewForm.ADDtoNoteBook;
    tbNewCVSRec.Checked := ActiveViewForm.ADDtoNordCo;
    tbVideo.Checked := ActiveViewForm.SelectVideo;
    // if Assigned(InfoBarForm) then InfoBarForm.AddButton.Down:= tbNewRec.Checked;
  end;
end;

procedure TMainForm.ChangeSoundFlag(Sender: TObject);
begin
  // if ActiveViewForm <> nil then tbSound.Checked:= ActiveViewForm.SoundFlag;
end;

procedure TMainForm.OnChangeLanguage(Sender: TObject);
var
  I: Integer;
  S: string;

begin

  Self.Caption := LangTable.Caption['General:AppCaption' + Config.GetProgramModeId];
  Application.Title := LangTable.Caption['General:AppTitle' + Config.GetProgramModeId];

  Self.Font.Name := LangTable.Caption['General:FontName'];
  TBToolbar1.Font.Name := LangTable.Caption['General:FontName'];
  MainToolbar.Font.Name := LangTable.Caption['General:FontName'];

  miFileMan.Caption := LangTable.Caption['MainMenu:File'];
  miViewMan.Caption := LangTable.Caption['MainMenu:View'];
  miGotoMan.Caption := LangTable.Caption['MainMenu:Goto'];
  miWinMan.Caption := LangTable.Caption['MainMenu:Window'];

  miSavePiece.Caption := LangTable.Caption['MenuFile:Savefilepart'];
  miSensGraph.Caption := LangTable.Caption['MenuFile:Sensitivitydiagram'];

  miOpenFile.Caption := LangTable.Caption['MenuFile:Open'];
  miRecent.Caption := LangTable.Caption['MenuFile:Recentfiles'];
  miClose.Caption := LangTable.Caption['MenuFile:Close'];
  miNotebook.Caption := LangTable.Caption['MenuFile:Notebook'];

  if (Config.ProgramMode = pmRADIOAVIONICA_MAV) then
    miNotNewRec.Caption := LangTable.Caption['MenuFile:Addnewrecord'] + ' (' + LangTable.Caption['ToolBar:InFile'] + ')'
  else
    miNotNewRec.Caption := LangTable.Caption['MenuFile:Addnewrecord'];
  miExportToNORDCO.Caption := { LangTable.Caption['MenuFile:Addnewrecord'] + ' (' + } LangTable.Caption['ToolBar:InNORDCON'] { + ')' } ;
  miFilePar.Caption := LangTable.Caption['MenuFile:Fileinformation'];
  miIntegrityChecking.Caption := LangTable.Caption['MenuFile:Fileintegritycheck'];
  miFileBase.Caption := LangTable.Caption['MenuFile:Database'];
  miSameCoordFiles.Caption := LangTable.Caption['MenuFile:Fileswiththesamecoord'];
  miFileDownload2.Caption := LangTable.Caption['USBFlash:FormTitle'];



  // Filtr_Calc.Caption          := LangTable.Caption['MenuFile:Calculate'];
  // Filtr_Off.Caption           := LangTable.Caption['MenuFile:Off'];
  // Filtr_Mark.Caption          := LangTable.Caption['MenuFile:Mark'];
  // Filtr_Hide.Caption          := LangTable.Caption['MenuFile:Hide'];

  TBItem13.Caption := LangTable.Caption['MenuView:BScan'];
  TBItem12.Caption := LangTable.Caption['MenuView:Convergence1'];
  TBItem11.Caption := LangTable.Caption['MenuView:Convergence2'];
  TBItem10.Caption := LangTable.Caption['MenuView:RailForm'];

//  miFiltrMenu.Caption := LangTable.Caption['MenuFile:NoiseFiltration'];
  miSettings.Caption := LangTable.Caption['MenuFile:Settings'];
  miExit.Caption := LangTable.Caption['MenuFile:Exit'];
  miClearLastDwdFiles.Caption := LangTable.Caption['MenuFile:ClearList'];
  miFileDownloadList.Caption := LangTable.Caption['MenuFile:Lastdownloadedfiles'];

  miPointSize.Caption := LangTable.Caption['MenuView:PointSize'];
  miSmallPoint.Caption := LangTable.Caption['MenuView:SmallPoint'];
  miMediumPoint.Caption := LangTable.Caption['MenuView:MediumPoint'];
  miLargePoint.Caption := LangTable.Caption['MenuView:LargePoint'];

  miMarkOnBScan.Caption := LangTable.Caption['MenuView:MarkOnBScan'];
  miTape.Caption := LangTable.Caption['MenuView:BscanNoConv'];
  miTapeReduce1.Caption := LangTable.Caption['MenuView:BscanConv1'];
  miTapeReduce2.Caption := LangTable.Caption['MenuView:BscanConv2'];
  miRail.Caption := LangTable.Caption['MenuView:Intherailform'];
  miBothRail.Caption := LangTable.Caption['MenuView:BothRail'];
  miRightRail.Caption := LangTable.Caption['MenuView:RightRail'];
  miLeftRail.Caption := LangTable.Caption['MenuView:LeftRail'];
  miAllChannels.Caption := LangTable.Caption['MenuView:AllChannel'];

  if Config.ProgramMode = pmRADIOAVIONICA_MAV then
  begin
    miOnlyNaezdChannels.Caption := LangTable.Caption['MenuView:Zoomin_MAV'];
    miOnlyOtezdChannels.Caption := LangTable.Caption['MenuView:Zoomout_MAV'];
  end
  else
  begin
    miOnlyNaezdChannels.Caption := LangTable.Caption['MenuView:Zoomin'];
    miOnlyOtezdChannels.Caption := LangTable.Caption['MenuView:Zoomout'];
  end;

  miBackMotion.Caption := LangTable.Caption['MenuView:Backmotionzones'];
  miEchoSizeByAmpl.Caption := LangTable.Caption['MenuView:PointsizebyAmplitude'];
  miEchoColorByAmpl.Caption := LangTable.Caption['MenuView:PointcolorbyAmplitude'];

  miChSettings.Caption := LangTable.Caption['MenuView:ChSettings'];
  miChGate.Caption := LangTable.Caption['MenuView:ChGate'];
  miChInfo_.Caption := LangTable.Caption['MenuView:ChInfo'];
  miMetallSensorValue0.Caption := LangTable.Caption['MenuView:MetallSensorValue0'];
  miMetallSensorValue1.Caption := LangTable.Caption['MenuView:MetallSensorValue1'];

  miZoomList.Caption := LangTable.Caption['MenuView:Scale'];
  tbAmplThList.Caption := LangTable.Caption['MenuView:Threshold2'];
  miDonAmpl.Caption := LangTable.Caption['MenuView:BSEnvelope'];

  miGoto.Caption := LangTable.Caption['MenuGoto:ToCoordinate'];
  miGotoLabel.Caption := LangTable.Caption['MenuGoto:ToOperatorMark'];
  miGotoRecord.Caption := LangTable.Caption['MenuGoto:ToNotebookRecord'];
  miHandScan.Caption := LangTable.Caption['MenuGoto:ToManualScan'];

  miWinCascade.Caption := LangTable.Caption['MenuWindows:Cascade'];
  miWinTileHorizontal.Caption := LangTable.Caption['MenuWindows:TileHorizontal'];
  miWinTileVertical.Caption := LangTable.Caption['MenuWindows:TileVertical'];
  miWinMinimizeAll.Caption := LangTable.Caption['MenuWindows:MinimizeAll'];
  // miLincWin.Visible:= False;
  miLincWin.Caption := LangTable.Caption['MenuWindows:Linkwindows'];
  miNextWin.Caption := LangTable.Caption['MenuWindows:NextWindow'];

  tbHelp.Visible := False;
  tbMailTo.Visible := False;
  tbIE.Visible := False;
  tbFTP.Visible := False;

  // tbHelp.Caption              := LangTable.Caption['MenuHelp:Help'];
  tbAbout.Caption := LangTable.Caption['MenuHelp:About'];
  // tbMailTo.Caption            := LangTable.Caption['MenuHelp:MailtoAuthor'];
  // tbIE.Caption                := LangTable.Caption['MenuHelp:"Radioavionika"HomePage'];
  // tbFTP.Caption               := LangTable.Caption['MenuHelp:'];

  TBToolbar1.Caption := 'MainMenu';
  TBToolbar2.Caption := 'ToolBar';
  MainToolbar.Caption := 'Measurement';

  // TBToolbar1.Caption          := LangTable.Caption['ToolBar:MainMenu'];
  // TBToolbar2.Caption          := LangTable.Caption['ToolBar:ToolBar'];
  // MainToolbar.Caption         := LangTable.Caption['ToolBar:Measurement'];

  tbOpen.Caption := LangTable.Caption['ToolBar:Open'];
  tbOpen.Hint := LangTable.Caption['ToolBar:Open'];
  tbBase.Caption := LangTable.Caption['ToolBar:Database'];
  tbBase.Hint := LangTable.Caption['ToolBar:Database'];
  tbBScan.Caption := LangTable.Caption['ToolBar:BScanViewMode'];
  tbBScan.Hint := LangTable.Caption['ToolBar:BScanViewMode'];
  tbViewCh.Caption := LangTable.Caption['ToolBar:ViewChannelMode'];
  tbViewCh.Hint := LangTable.Caption['ToolBar:ViewChannelMode'];

  TBItem14.Caption := LangTable.Caption['MenuView:AllChannel'];

  if Config.ProgramMode = pmRADIOAVIONICA_MAV then
  begin
    TBItem15.Caption := LangTable.Caption['MenuView:Zoomin_MAV'];
    TBItem19.Caption := LangTable.Caption['MenuView:Zoomout_MAV'];
  end
  else
  begin
    TBItem15.Caption := LangTable.Caption['MenuView:Zoomin'];
    TBItem19.Caption := LangTable.Caption['MenuView:Zoomout'];
  end;

  tbZoomList.Caption := LangTable.Caption['ToolBar:Scale'];
  tbZoomList.Hint := LangTable.Caption['ToolBar:Scale'];
  tbGoto.Caption := LangTable.Caption['ToolBar:Goto'];
  tbGoto.Hint := LangTable.Caption['ToolBar:Goto'];
  // tbFilter.Caption            := LangTable.Caption['MenuFile:Filtration'];
  tbNewFileRec.Caption := LangTable.Caption['ToolBar:NewRec'];
  tbNewFileRec.Hint := LangTable.Caption['ToolBar:NewRec'];
  tbVideo.Caption := LangTable.Caption['ToolBar:EVideo'];
  tbVideo.Hint := LangTable.Caption['ToolBar:EVideo'];
  tbNewCVSRec.Caption := LangTable.Caption['ToolBar:NewRec'];
  tbNewCVSRec.Hint := LangTable.Caption['ToolBar:NewRec'];
  tbNotebook.Caption := LangTable.Caption['ToolBar:Notebook'];
  tbNotebook.Hint := LangTable.Caption['ToolBar:Notebook'];
  // tbSound.Caption             := LangTable.Caption['MenuView:Sound'];
  // tbSound.Hint                := LangTable.Caption['MenuView:Sound'];
  Panel2.Caption := LangTable.Caption['ToolBar:Threshold'] + ' -6 ' + LangTable.Caption['Common:dB'];
  OpenDialog.Filter := LangTable.Caption['Common:FileFilter' + Config.GetProgramModeId];
  tbDrawRail.Caption := LangTable.Caption['ToolBar:ViewRailMode'];
  TBItem25.Caption := LangTable.Caption['MenuView:BothRail'];
  TBItem24.Caption := LangTable.Caption['MenuView:LeftRail'];
  TBItem23.Caption := LangTable.Caption['MenuView:RightRail'];

  miInNordico.Caption := LangTable.Caption['ToolBar:InNORDCON'];
  miInFile.Caption := LangTable.Caption['ToolBar:InFile'];
  tbDrawRail.ImageIndex := 19;

  for I := 0 to tbAmplThList.Count - 1 do
  begin
    S := tbAmplThList.Items[I].Caption;
    Delete(S, Length(S) - 1, 2);
    S := S + LangTable.Caption['Common:dB'];
    tbAmplThList.Items[I].Caption := S;
  end;

  for I := 0 to High(ZoomList) do
    miZoomList.Items[I].Caption := FloatToStr(ZoomList[I] / 100) + ' ' + LangTable.Caption['Common:mm/p'];

  SetCrdToStrCaptions(LangTable.Caption['Common:KM'], LangTable.Caption['Common:Pk'], LangTable.Caption['Common:M'], LangTable.Caption['Common:Centimeter'], LangTable.Caption['Common:Millimeter'], LangTable.Caption['Common:Imperial'], LangTable.Caption['Common:MetricA'], LangTable.Caption['Common:MetricB']);

  miViewTestRecord.Caption := LangTable.Caption['MenuFile:ViewTestRec'];
  miViewPaintSystemState.Caption := LangTable.Caption['MenuView:PaintSystemResults'];
  miMetallSensor.Caption := LangTable.Caption['MenuView:MetallSensor'];
  miMetallSensorZone.Caption := LangTable.Caption['MenuView:Zone'];
  miUnstableBottomSignal.Caption := LangTable.Caption['MenuView:UnstableBottomSignal'];
//  tbiViewMap //NO_LANGUAGE
end;

procedure TMainForm.miFileManClick(Sender: TObject);
var
  I: Integer;
  AItem: TTBCustomItem;

begin
  for I := miFileMan.Count - 1 downto 0 do
    if miFileMan.Items[I].Tag < 0 then
      miFileMan.Delete(I);

  miRecent.Clear;
  for I := Config.OpenFilesHistory.Count - 1 downto 0 do
  begin
    AItem := TTBCustomItem.Create(nil);
    AItem.Name := 'RecentFile' + IntToStr(I);
    AItem.Tag := -(I + 1);
    AItem.Caption := Config.OpenFilesHistory.Strings[I];
    AItem.OnClick := OpenRecent;
    miRecent.Add(AItem);
  end;

  for I := miFileDownloadList.Count - 1 downto 0 do
    if Copy(miFileDownloadList.Items[I].Name, 1, 12) = 'DownLoadFile' then
      miFileDownloadList.Delete(I);

  for I := Config.DownLoadFilesHistory.Count - 1 downto 0 do
  begin
    AItem := TTBCustomItem.Create(nil);
    AItem.Name := 'DownLoadFile' + IntToStr(I);
    AItem.Tag := I;
    AItem.Caption := Config.DownLoadFilesHistory.Strings[I];
    AItem.OnClick := OpenDownLoad;
    miFileDownloadList.Add(AItem);
  end;

  if Config.DownLoadFilesHistory.Count = 0 then
  begin
    AItem := TTBCustomItem.Create(nil);
    AItem.Name := 'DownLoadFile';
    AItem.Tag := 0;
    AItem.Caption := LangTable.Caption['Common:Empty'];
    AItem.OnClick := nil;
    miFileDownloadList.Add(AItem);
    TBSeparatorItem8.Visible := False;
    miClearLastDwdFiles.Visible := False;
    TBSeparatorItem15.Visible := False;
  end
  else
  begin
    TBSeparatorItem8.Visible := True;
    miClearLastDwdFiles.Visible := True;
    TBSeparatorItem15.Visible := True;
  end;
end;

procedure TMainForm.OpenRecent(Sender: TObject);
begin
  if FileExists((Sender as TTBCustomItem).Caption) then OpenFile((Sender as TTBCustomItem).Caption)
                                                   else MessageBox(Handle, PChar(LangTable.Caption['Messages:Filenotfound']), PChar(LangTable.Caption['Common:Attention']), 16);
end;

procedure TMainForm.miOperatorLogClick(Sender: TObject);
var
  I, J, L: Integer;
  ID_: Byte;
  pData: PEventData;
  EventText: string;
  ChannelText: string;
  TimeText: string;

  CrdParams: TCrdParams;
  str1, str2: string;

//  ModeText: string;
  RailText_: string;
  ChName: string;
  TimeName: string;

  NewItem: TListItem;
  LastModeIdx: Integer;

//  InModeTime: array of TPoint;

  CDUFlag: Boolean;

begin
  if (ActiveViewForm.DatSrc <> nil) and (ActiveViewForm.DatSrc.Config <> nil) then
  begin
    LastModeIdx:= -1;

    OperatorLogForm.AVF:= ActiveViewForm;
    OperatorLogForm.BringToFront;
    OperatorLogForm.ListView1.Items.Clear;

    with ActiveViewForm.DatSrc do
    begin
(*      for I := 0 to EventCount - 1 do
      begin
        GetEventData(I, ID_, pData);
        if ID_ = EID_Mode then
        begin
          SetLength(InModeTime, Length(InModeTime) + 1);
          InModeTime[High(InModeTime)].X:= pedMode(pData)^.Time;
          InModeTime[High(InModeTime)].Y:= I;
        end;
      end;
  *)
      for I := -1 to EventCount - 1 do
      begin
        EventText:= '';
        if I <> - 1 then GetEventData(I, ID_, pData)
                    else EventText:= 'Оператор - ' + HeaderStrToString(Header.OperatorName);

        // ---- Вывод текстового сообщения / отметки, вертикальная линия + текст -----

        CDUFlag:= False;
        ChannelText:= '-';
        TimeText:= '-';

        if I <> - 1 then
        case ID_ of
          EID_Stolb: with pCoordPost(pData)^ do
                     begin
                       EventText := 'Отметка - Столб: ';
                       if Km[0] = Km[1] then EventText := EventText + Format(LangTable.Caption['Common:pk'] + ' %d/%d (' + LangTable.Caption['Common:km'] + ' %d)', [Pk[0], Pk[1], Km[0]])
                                        else EventText := EventText + Format(LangTable.Caption['Common:km'] + ' %d/%d ', [Km[0], Km[1]]);
                     end;
  EID_StolbChainage: with pedCoordPostChainage(pData)^ do
                     begin
                       EventText := 'Отметка - Столб: ' + CaCrdToStr(CoordSys, Val);
                     end;
        EID_Switch:  with pedSwitch(pData)^ do
                     begin
                       EventText := 'Отметка - ' + LangTable.Caption['Common:Pt'] + ': ' + GetString(Len, Text);
                     end;
      EID_DefLabel:  with pedDefLabel(pData)^ do
                     begin
                       EventText := 'Отметка - Дефект ';
                       if Rail = rLeft then
                       begin
                         EventText := EventText + LangTable.Caption['Common:Def.code'] + ' (' + LangTable.Caption['Common:Left'] + '): ';
                       end
                       else
                       begin
                         EventText := EventText + LangTable.Caption['Common:Def.code'] + ' (' + LangTable.Caption['Common:Right'] + '): ';
                       end;
                       EventText := EventText + GetString(Len, Text);
                     end;
     EID_TextLabel:  with pedTextLabel(pData)^ do
                     begin
                       EventText := 'Отметка - ' + GetString(Len, Text);
                     end;
  EID_ChangeOperatorName:
                     with pedTextLabel(pData)^ do
                     begin
                       EventText := 'Смена оператора: ' + GetString(Len, Text);
                     end;
       EID_EndFile: EventText := 'Выключение регистрации';
   EID_SetRailType: EventText := 'Настройка на тип рельса';
    EID_StBoltStyk: EventText := 'Режим "Контроль БС" - включён';
   EID_EndBoltStyk: EventText := 'Режим "Контроль БС" - выключен';
  //              EID_GPSCoord:    Text := Format(LangTable.Caption['EventsName:GPSCoord'], [GPSSingleToText(pedGPSCoord(pData)^.Lat, True), GPSSingleToText(pedGPSCoord(pData)^.Lon, False)]);
  //              EID_GPSState:    Text := LangTable.Caption['EventsName:GPSState'];
          EID_Mode: with pedMode(pData)^ do
//                      if LastModeIdx <> ModeIdx then
                      begin
                        LastModeIdx:= ModeIdx;
//                        S:= 'Выбран режим работы дефектоскопа - ';
                        if ModeIdx in [4 .. 7]
                          then begin
                                 EventText:= LangTable.Caption['EventsName:Mode' + IntToStr(ModeIdx)];
                                 TimeText:= Format('%d', [Params[I].Time]);
                               end
                          else if ModeIdx in [0, 2] then //  Каналы сплошного контроля
                               begin
                                 EventText:= LangTable.Caption['EventsName:Mode' + IntToStr(ModeIdx)];
                                 RailText_ := RailToStr(RailToPathRail(Rail), 0);
                                 ChName := LangTable.Caption[Config.HandChannelByNum[EvalChNum].ShortName];
                                 TimeText:= Format('%d', [Params[I].Time]);
                                 if Channel then ChannelText:= Format({LangTable.Caption['Common:Rail'] + }'%s, ' + {LangTable.Caption['Common:Channel'] + } '%s', [RailText_, ChName]);
                               end
                               else
                               if ModeIdx in [1, 3] then // Каналы ручного контроля
                               begin
                                 //RailText_ := RailToStr(RailToPathRail(Rail), 0);
                                 EventText:= LangTable.Caption['EventsName:Mode' + IntToStr(ModeIdx)];
                                 ChName := LangTable.Caption[Config.HandChannelByNum[EvalChNum].ShortName];
                                 TimeText:= Format('%d', [Params[I].Time]);
                                 if Channel then ChannelText:= ChName;
                               end;
                      end;
        EID_HeadPh: with pedEvalChByteParam(pData)^ do
                    begin
                      EventText := LangTable.Caption['EventsName:ChangeHeadphones'] + ': ' + LangTable.Caption['Common:State' + IntToStr(pedEvalChByteParam(pData)^.Value)] + ' ' + Format(' (' + LangTable.Caption['Common:Rail'] + ': %s, ' + LangTable.Caption['Common:Channel'] + ': %s)', [RailToStr(RailToPathRail(Rail), 0), LangTable.Caption[Config.EvalChannelByNum[EvalCh].ShortName]]);
                    end;
//    EID_StSwarStyk: S := LangTable.Caption['EventsName:StartWeldedjoint'];
//    EID_EndSwarStyk: S := LangTable.Caption['EventsName:EndWeldedjoint'];

    EID_HandScan: begin

                    for J := 0 to Length(HSList) - 1 do
                      if HSList[J].EventIdx = I then
                      begin
                        RailText_:= RailToStr(RailToPathRail(HSList[J].Header.Rail), 0);
                        ChName := LangTable.Caption[Config.HandChannelByNum[HSList[J].Header.HandChNum].ShortName];
                        //ChName := IntToStr(HSList[J].Header.HandChNum);
                        TimeText := Format('%d', [Round(Length(HSList[J].Samples) / 50)]);
                        ChannelText:= Format('%s, %s', [RailText_, ChName]);
                        EventText := 'Ручной контроль (запись)';
                        Break;
                      end;
                  end;


    EID_RailHeadScaner: EventText := 'Сканер головки';
    EID_Media: with pedMediaData(pData)^ do
                 case DataType of
                    1: EventText:= 'Запись аудио комментария';
                    2: EventText:= 'Фото';
                    3: EventText:= 'Запись видео';
                  end;



  {              EID_AirBrushTempOff:  begin
                                        Text := LangTable.Caption['EventsName:PaintSystemState'] + ' ';
                                        if pedAirBrushTempOff(pData)^.State = 1
                                         then Text := Text + LangTable.Caption['Common:State0'] // Выкл
                                         else Text := Text + LangTable.Caption['Common:State1']; // Вкл
                                      end;
                EID_ZerroProbMode:    begin
                                        case TZerroProbMode(pedZerroProbMode(pData)^.Mode) of
                                          zpmBoth:  Text := LangTable.Caption['EventsName:WorkWPZerroProbe_Both']; // Работа 0° - Оба;
                                          zpmOnlyA: Text := LangTable.Caption['EventsName:WorkWPZerroProbe_OnlyA']; // Работа 0° - КПА;
                                          zpmOnlyB: Text := LangTable.Caption['EventsName:WorkWPZerroProbe_OnlyB']; // Работа 0° - КПВ;
                                        end;
                                      end; }

    EID_OperatorRemindLabel:
            with pedOperatorRemindLabel(pData)^ do
            begin

              case LabelType of // Тип отметки:
                0: EventText:= '';
                1: EventText:= 'Дефект: ';
                2: EventText:= 'ОДР: ';
                3: EventText:= 'Подозрение на дефект: ';
              end;
              EventText:= EventText + GetString(Len, Text) + Format(' L%d мм, H%d мм ', [Length, Depth]);
              if Rail = rLeft then
              begin
                EventText := EventText + ' (' + LangTable.Caption['Common:Left'] + ')';
              end
              else
              begin
                EventText := EventText + ' (' + LangTable.Caption['Common:Right'] + ')';
              end;
              EventText:= 'Напоминание оператору ' + EventText;
              CDUFlag:= True;
            end;

       EID_SmallDate:
            with pedSmallDate(pData)^ do

            if DataType = 1 then
            begin
              EventText:= 'Необходимо перенастроить каналы контроля!';
              CDUFlag:= True;
            end;

//       EID_SpeedState:

       EID_QualityCalibrationRec:
            with pedQualityCalibrationRec(pData)^ do
              if RecType in [1, 2] then
              begin
                EventText:= 'Качество настройки каналов контроля - ';
                case RecType of
                  1: EventText := EventText + 'Неуд. по БС ';
                  2: EventText := EventText + 'Неуд. по АТС ';
                end;
                EventText := EventText + RailToStr(RailToPathRail(pedMode(pData)^.Rail), 0) + ' ';
                for L := 0 to Len - 1 do
                  EventText := EventText + LangTable.Caption[Config.EvalChannelByNum[Data[L]].ShortName] (*EvalChNumToName(Data[L])*) (*CID_GateIndex_To_Name(Data[L].CID, Data[L].GateIndex)*) + ', ';
                Delete(EventText, Length(EventText) - 1, 2);
                CDUFlag:= True;
              end;

    EID_SensFail: with pedSensFail(pData)^ do
                  if DeltaKu <> 0 then
                  begin
                      EventText:= 'Отклонение условной чувствительности от нормативного значения - ';
                      EventText:= EventText + Format(' (' + LangTable.Caption['Common:Rail'] + ': %s, ' + LangTable.Caption['Common:Channel'] + ': %s)',
                                                    [RailToStr(RailToPathRail(pedEvalChByteParam(pData)^.Rail), 0), LangTable.Caption[Config.EvalChannelByNum[pedEvalChByteParam(pData)^.EvalCh].ShortName]]);
                      EventText:= EventText + Format(' на %d дБ', [DeltaKu]);
                      CDUFlag:= True;
                  end;
        end;

        if EventText <> '' then
        begin
          ActiveViewForm.DatSrc.DisToCoordParams(Event[I].DisCoord, CrdParams);
          str1:= RealCrdToStr(CrdParamsToRealCrd(CrdParams, ActiveViewForm.DatSrc.Header.MoveDir), 1);
    //      ActiveViewForm.DatSrc.DisToCoordParams(ActiveViewForm.DatSrc.AKStateData__[I].Y, CrdParams);
    //      str2:= RealCrdToStr(CrdParamsToRealCrd(CrdParams, ActiveViewForm.DatSrc.Header.MoveDir), 1);

//          OperatorLogForm.ListBox1.Items.AddObject(Format('%s - %s: %s', [GetTime(Event[I].DisCoord), str1, s]), Pointer(I));
          NewItem:= OperatorLogForm.ListView1.Items.Add;
          NewItem.Data:= Pointer(Event[I].DisCoord);
          NewItem.Caption:= GetTime(Event[I].DisCoord);
          NewItem.SubItems.Add(str1);
          if CDUFlag then NewItem.SubItems.Add('Авикон-31')
                     else NewItem.SubItems.Add('Оператор');
          NewItem.SubItems.Add(EventText);
          NewItem.SubItems.Add(ChannelText);
          NewItem.SubItems.Add(TimeText);
        end;
      end;
    end;
    OperatorLogForm.Visible:= True;
  end;

{
  LastModeIdx:= 0;
  for I:= 0 to High(InModeTime) do
    LastModeIdx:= LastModeIdx + InModeTime[I].X;
  ShowMessage(IntToStr(Round((LastModeIdx / 1000) / 60 )));
 }

       (*


  // --------- Отрисовка протяженных отметок -------------------------------
  // --------- Отрисовка отметок алгоритма краскопульта --------------------------------
  // --------- Отрисовка значимых мест найденных на БУИ --------------------------------
  // --------- Отрисовка АК --------------------------------
  // --------- Отрисовка HC --------------------------------
  // --------- Отрисовка меток Записей Блокнота ----------------------------------
  *)
end;

procedure TMainForm.OpenDownLoad(Sender: TObject);
var
  Idx: Integer;
  FN: string;

begin
  Idx := TTBCustomItem(Sender).Tag;
  FN := Config.DownLoadFilesHistory.Strings[Idx];
  Config.DownLoadFilesHistory.Delete(Idx);
  if FileExists(FN) then
    OpenFile(FN)
  else
    MessageBox(Handle, PChar(LangTable.Caption['Messages:Filenotfound']), PChar(LangTable.Caption['Common:Attention']), 16);
end;

procedure TMainForm.tbHeaderFlagsClick(Sender: TObject);
var
  S: string;
  RL: string;

begin
  if ActiveViewForm <> nil then
    with ActiveViewForm.DatSrc.Header do
    begin
      RL:= Chr(13) + Chr(10);
      if UsedItems[uiUnitsCount] = 1 then S:= S + 'Количество блоков прибора' + RL;
      if UsedItems[uiRailRoadName] = 1 then S:= S + 'Название железной дороги' + RL;
      if UsedItems[uiOrganization] = 1 then S:= S + 'Название организации осуществляющей контроль' + RL;
      if UsedItems[uiDirectionCode] = 1 then S:= S + 'Код направления' + RL;
      if UsedItems[uiPathSectionName] = 1 then S:= S + 'Название участка дороги' + RL;
      if UsedItems[uiRailPathNumber] = 1 then S:= S + 'Номер ж/д пути' + RL;
      if UsedItems[uiRailPathTextNumber] = 1 then S:= S + 'Номер ж/д пути (текст)' + RL;
      if UsedItems[uiRailSection] = 1 then S:= S + 'Звено рельса' + RL;
      if UsedItems[uiDateTime] = 1 then S:= S + 'Дата время контроля' + RL;
      if UsedItems[uiOperatorName] = 1 then S:= S + 'Имя оператора' + RL;
      if UsedItems[uiCheckSumm] = 1 then S:= S + 'Контрольная сумма' + RL;
      if UsedItems[uiStartMetric] = 1 then S:= S + 'Начальная координата - Метрическая' + RL;
      if UsedItems[uiMoveDir] = 1 then S:= S + 'Направление движения' + RL;
      if UsedItems[uiPathCoordSystem] = 1 then S:= S + 'Система путейской координат' + RL;
      if UsedItems[uiWorkRailTypeA] = 1 then S:= S + 'Рабочая нить (для однониточных приборов)' + RL;
      if UsedItems[uiStartChainage] = 1 then S:= S + 'Начальная координата в XXX.YYY' + RL;
      if UsedItems[uiWorkRailTypeB] = 1 then S:= S + 'Рабочая нить вариант №2' + RL;
      if UsedItems[uiTrackDirection] = 1 then S:= S + 'Код направления' + RL;
      if UsedItems[uiTrackID] = 1 then S:= S + 'TrackID' + RL;
      if UsedItems[uiCorrSides] = 1 then S:= S + 'Соответствие стороны тележки нитям пути' + RL;
      if UsedItems[uiControlDir] = 1 then S:= S + 'Направление контроля' + RL;
      if UsedItems[uiGaugeSideLeftSide] = 1 then S:= S + 'Соответствие стороны тележки рабочен / не рабочей грани головки рельса' + RL;
      if UsedItems[uiGaugeSideRightSide] = 1 then S:= S + 'Соответствие стороны тележки рабочен / не рабочей грани головки рельса' + RL;
      if UsedItems[uiGPSTrackinDegrees] = 1 then S:= S + 'Запись GPS трека (значения в градусах)' + RL;
      if UsedItems[uiSetMaintenanceServicesDate] = 1 then S:= S + 'Дата технического обслуживания (запись тупика)' + RL;
      if UsedItems[uiSetCalibrationDate] = 1 then S:= S + 'Дата калибровки' + RL;
      if UsedItems[uiSetCalibrationName] = 1 then S:= S + 'Название настройки' + RL;
      if UsedItems[uiAcousticContact] = 1 then S:= S + 'Запись акустического контакта' + RL;
      if UsedItems[uiBScanTreshold_minus_6dB] = 1 then S:= S + 'Порог В-развертки - 6 Дб' + RL;
      if UsedItems[uiBScanTreshold_minus_12dB] = 1 then S:= S + 'Порог В-развертки - 12 Дб' + RL;
      if UsedItems[uiHandScanFile] = 1 then S:= S + 'Файл ручного контроля' + RL;
      if UsedItems[uiTemperature] = 1 then S:= S + 'Температура окружающей стреды' + RL;
      if UsedItems[uiSpeed] = 1 then S:= S + 'Скорость' + RL;
      if UsedItems[uiRailPathTextNumber] = 1 then S:= S + 'Номер ж/д пути в формате текст' + RL;
    end;
  ShowMessage(S);
end;

procedure TMainForm.tbHelpClick(Sender: TObject);
var
  FN: string;

begin
  { if Config.WorkLang = 0 then FN:= ExtractFilePath(Application.ExeName) + 'Ru_help.chm'
    else FN:= ExtractFilePath(Application.ExeName) + 'En_help.chm';

    if FileExists(FN) then ExecuteFile('hh.exe', [FN])
    else ShowMessage(LangTable.Caption['Messages:Helpfilenotfound'] + ':' + #$0D + #$0A + FN);
    }
end;

procedure TMainForm.tbAboutClick(Sender: TObject);
begin
  with TAboutForm_.Create(nil) do
  begin
    {$IFDEF ROSTOV}
    Label2.Caption := Label2.Caption + AnsiString(#13#10)
        + 'Модуль АР версия: ' + Rostov.getLibraryVersion();
    {$ENDIF}
    Showmodal;
    Free;
  end;
end;

procedure TMainForm.tbAllEventsOnLineClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    with ActiveViewForm, Display do
      FullRefresh;
end;

procedure TMainForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
const
  KeyCode: array [1 .. 6] of Char = ('S', 'P', '3', 'G', 'W', '0');
  KeyCode2: array [1 .. 5] of Char = ('D', 'E', 'B', 'U', 'G');
  Idx: Integer = 1;
  Idx1: Integer = 1;
  Time: Cardinal = 0;
  Time1: Cardinal = 0;

begin
  if GetTickCount - Time > 1000 then
    Idx := 1;
  if (Idx in [1 .. 6]) and (Chr(Msg.CharCode) = KeyCode[Idx]) then
  begin
    Inc(Idx);
    Time := GetTickCount;
    if Idx = 7 then
    begin
      Idx := 1;
      miDebug.Visible := not miDebug.Visible;
      if miDebug.Visible then
      begin
        TBItem36.Checked:= False;
        TBItem36.Click;
      end;

    end;
  end;

  if GetTickCount - Time1 > 1000 then
    Idx1 := 1;
  if (Idx1 in [1 .. 5]) and (Chr(Msg.CharCode) = KeyCode2[Idx1]) then
  begin
    Inc(Idx1);
    Time1 := GetTickCount;
    if Idx1 = 6 then
    begin
      Idx1 := 1;
      FPaintSystemDebugMode := not FPaintSystemDebugMode;
      if ActiveViewForm <> nil then ActiveViewForm.Display.FullRefresh;
    end;
  end;
{$IFDEF REC}
  if Assigned(RecInt.FCurRE) and (Length(RecInt.FCurRE.Res) <> 0) then
  begin
    if Msg.CharCode = 219 then
      RecInt.FResForm.tbBackward_Click(nil);
    if Msg.CharCode = 221 then
      RecInt.FResForm.tbForward_Click(nil);
  end;
{$ENDIF}
{$IFDEF FILTERPARAM}
  if (Msg.CharCode = 80) and (ActiveViewForm <> nil) and Assigned(ActiveViewForm.Display.Filtr) then
    ShowFiltrParamForm(ActiveViewForm.Display, ActiveViewForm.Display.Filtr);
{$ENDIF}
end;

procedure TMainForm.NewCrd(Sender: TObject);
begin
  { if not SaveState then
    begin
    SetLength(CrdList1, Length(CrdList1) + 1);
    CrdList1[High(CrdList1)]:= Integer(Sender);
    SaveState:= not SaveState;
    end
    else
    begin
    SetLength(CrdList2, Length(CrdList2) + 1);
    CrdList2[High(CrdList2)]:= Integer(Sender);
    SaveState:= not SaveState;
    end; }
end;

procedure TMainForm.TBItem1Click(Sender: TObject);
var
  I: Integer;
  Value: string;

begin
  if ActiveViewForm <> nil then
  begin
//    if InputQuery('', 'Enter Password', Value) then
 //     if Value = 'ddeenn' then
      begin
        // CrdLine.Clear;
        // ActiveViewForm.DatSrc.FAnalyzeNewCrd:= NewCrd;
        try
          ActiveViewForm.DatSrc.ReAnalyze;
        except
        end;
        ActiveViewForm.Display.Refresh;
        // for I:= 0 to High(CrdList1) do CrdLine.AddXY(CrdList2[I], CrdList1[I]);
        // SetLength(CrdList1, 0);
        // SetLength(CrdList2, 0);
      end;
  end;
end;


procedure TMainForm.miViewMapClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    GPS_SaveDialog.FileName:= ChangeFileExt(ActiveViewForm.DatSrc.FileName, '.gps');
    if GPS_SaveDialog.Execute then
      MakeGPXfile(ActiveViewForm.DatSrc, GPS_SaveDialog.FileName);
  end;
end;

procedure TMainForm.tbMailToClick(Sender: TObject);
begin
  ShellExecute(Application.Handle, nil, 'mailto:DenisF@Avionika-NDT.com;Radavion_SNK@spb.orw.rzd?subject=Avikon-11', nil, nil, SW_SHOWNORMAL);
end;

procedure TMainForm.tbIEClick(Sender: TObject);
begin
  ShellExecute(Application.Handle, nil, 'explorer', 'http://www.avionika-ndt.com/', nil, SW_SHOWNORMAL);
end;

procedure TMainForm.miMarkOnBScanClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.ViewLabels := not ActiveViewForm.Display.ViewLabels;
    Config.MarkOnBScan := ActiveViewForm.Display.ViewLabels;
    ActiveViewForm.Display.FullRefresh;
  end;
end;

procedure TMainForm.tbFTPClick(Sender: TObject);
begin
  ShellExecute(Application.Handle, nil, 'explorer', 'ftp://10.248.0.125/Radioavionika', nil, SW_SHOWNORMAL);
end;

procedure TMainForm.miEchoSizeByAmplClick(Sender: TObject);
begin
  MenuItemClick(Sender);
  Config.EchoSizeByAmpl := miEchoSizeByAmpl.Checked;
end;

procedure TMainForm.TBItem2Click(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    with TFileAnForm.Create(nil) do
    begin
      CreateGraph(ActiveViewForm.DatSrc);
      Showmodal;
    end;
end;

procedure TMainForm.TBItem20Click(Sender: TObject);
begin
  // if (ActiveViewForm <> nil) and Assigned(ActiveViewForm.DatSrc.Log) then ShowMessage(ActiveViewForm.DatSrc.Log.Text);
end;

procedure TMainForm.TBItem26Click(Sender: TObject);
begin

  if ActiveViewForm <> nil then
    with ActiveViewForm, Display do
    begin
      ShowSensor1DataFlag := TBItem26.Checked;
      FullRefresh;
    end;
end;

procedure TMainForm.TBItem27Click(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.DebugView1 := TBItem27.Checked;
    ActiveViewForm.Display.FullRefresh;
  end;
end;

procedure TMainForm.TBItem28Click(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.DebugView2 := TBItem27.Checked;
    ActiveViewForm.Display.FullRefresh;
  end;
end;

procedure TMainForm.TBItem29Click(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.ShowGPSPath;
  end;
end;

procedure TMainForm.SetButtonText(OnFlag: Boolean);
begin
  if OnFlag then
  begin
    MainToolbar.Options := MainToolbar.Options + [tboImageAboveCaption];
    MainToolbar.ShowHint := False;
    Panel1.Visible := True;
    tbViewTh.Visible := False;
  end
  else
  begin
    MainToolbar.Options := MainToolbar.Options - [tboImageAboveCaption];
    MainToolbar.ShowHint := True;
    Panel1.Visible := False;
    tbViewTh.Visible := True;
  end;
end;

procedure TMainForm.SetViewButtons(CycleSel: Boolean);
begin
  if CycleSel then
  begin
    // tbNoSwed.Visible:= False;
    // tbSwed1.Visible:= False;
    // tbSwed2.Visible:= False;
    // tbAsRail.Visible:= False;
    // tbBScan.Visible:= True;
  end
  else
  begin
    // tbNoSwed.Visible:= True;
    // tbSwed1.Visible:= True;
    // tbSwed2.Visible:= True;
    // tbAsRail.Visible:= True;
    // tbBScan.Visible:= False;
  end;
end;

(*
  procedure TMainForm.SpeedButton10Click(Sender: TObject);
  var
  I, X, X0, Y0, Y1, S: Integer;
  Rt: TRect;
  Text: string;


  begin
  X0:= 170;
  Y1:= 20;
  Y0:= 120;
  S:= 20;
  with PaintBox1.Canvas do
  if ActiveViewForm <> nil then
  with ActiveViewForm.Display.DatSrc.Config do
  begin
  Brush.Color:= clWhite;
  FillRect(Rect(0, 0, PaintBox1.Width, PaintBox1.Height));
  Pen.Color:= clBlack;

  for I := 0 to ScanChannelCount - 1 do
  begin


  X:= X0 + ScanChannelByIdx[I].Position;
  Rt:= Rect(X, Y0, X + S, Y0 + S);
  Rectangle(Rt);
  Text:= IntToStr(ScanChannelByIdx[I].EnterAngle);
  TextOut(Rt.Left + (Rt.Right - Rt.Left - TextWidth(Text)) div 2, Rt.Top + (Rt.Bottom - Rt.Top - TextHeight(Text)) div 2, Text);

  MoveTo((Rt.Right + Rt.Left) div 2, Rt.Bottom);
  LineTo(Round((Rt.Right + Rt.Left) / 2 + 100 * cos((ScanChannelByIdx[I].EnterAngle-90) * pi / 180)),
  Round(Rt.Bottom                 - 100 * sin((ScanChannelByIdx[I].EnterAngle-90) * pi / 180)));
  {

  Ellipse(X, Y1, X + S, Y1 + S);

  MoveTo(X + S div 2, Y1 + S div 2);
  LineTo(Round((Rt.Right + Rt.Left) / 2 + 100 * cos((ScanChannelByIdx[I].TurnAngle-90) * pi / 180)),
  Round(Rt.Bottom                 - 100 * sin((ScanChannelByIdx[I].TurnAngle-90) * pi / 180)));
  }
  end;
  end;
  end;
  *)
procedure TMainForm.SpeedButton10Click(Sender: TObject);
var
  I, J, K: Integer;
  S, S1, S2, S3: string;
  str: array [1..10] of string;

  RailRoadNames: TStringList;
//  Organizations: TStringList;
//  PathSectionNames: TStringList;
//  Paths: TStringList;

  RailRoadNames_out: TStringList;
  Organizations_out: TStringList;
  PathSectionNames_out: TStringList;
  Paths_out: TStringList;


  Marks: TStringList;
  Data: TAviconDataSource;
  crd: TMRFCrd;

  RRidx: Integer;
  Orgidx: Integer;
  PSectionidx: Integer;
  Pathidx: Integer;

  RRidx_: Integer;
  Orgidx_: Integer;
  PSectionidx_: Integer;
  Pathidx_: Integer;


  Rec: TFileNotebook;
  DefCnt: Integer;

begin
(*
//  PathSectionNames:= TStringList.Create;
  RailRoadNames:= TStringList.Create;
//  Organizations:= TStringList.Create;
  Marks:= TStringList.Create;
//  Paths:= TStringList.Create;

  RailRoadNames_out:= TStringList.Create;
  Organizations_out:= TStringList.Create;
  PathSectionNames_out:= TStringList.Create;
  Paths_out:= TStringList.Create;

  for I := 0 to DataBase_.Count - 1 do
  begin
    S:= HeaderStrToString(DataBase_.Item[I].RailRoadName);
    if RailRoadNames.IndexOf(S) = -1 then RailRoadNames.Add(S);
{
    S:= HeaderStrToString(DataBase_.Item[I].Organization);
    if Organizations.IndexOf(S) = -1 then Organizations.Add(S);

    S:= HeaderStrToString(DataBase_.Item[I].PathSectionName);
    if PathSectionNames.IndexOf(S) = -1 then PathSectionNames.Add(S);

    S:= HeaderStrToString(DataBase_.Item[I].RailPathNumber);
    if Paths.IndexOf(S) = -1 then Paths.Add(S); }
  end;

  // Дороги
  for I := 0 to RailRoadNames.Count - 1 do RailRoadNames_out.AddObject(IntToStr(I + 1) + '%' + RailRoadNames[I], Pointer(I + 1));

  // ПЧ
  for I := 0 to DataBase_.Count - 1 do
  begin
    S1:= HeaderStrToString(DataBase_.Item[I].RailRoadName);
    if RailRoadNames.IndexOf(S1) = - 1 then ShowMessage('Ошибка - ПЧ');
    S2:= IntToStr(RailRoadNames.IndexOf(S1) + 1) + '%' + HeaderStrToString(DataBase_.Item[I].Organization);
    if Organizations_out.IndexOf(S2) = - 1 then Organizations_out.AddObject(S2, RailRoadNames.IndexOf(S1));
  end;

  for I := 0 to Organizations_out.Count - 1 do Organizations_out[I]:= IntToStr(I + 1) + '%' + Organizations_out[I];

  // Перегоны
  for I := 0 to DataBase_.Count - 1 do
  begin
    S1:= HeaderStrToString(DataBase_.Item[I].RailRoadName);
    S1:= HeaderStrToString(DataBase_.Item[I].Organization);
    if Organizations.IndexOf(S1) = - 1 then ShowMessage('Ошибка - Перегоны');
    S2:= IntToStr(Organizations.IndexOf(S1) + 1) + '%' + HeaderStrToString(DataBase_.Item[I].PathSectionName);
    if PathSectionNames_out.IndexOf(S2) = - 1 then PathSectionNames_out.Add(S2);
  end;
  for I := 0 to PathSectionNames_out.Count - 1 do PathSectionNames_out[I]:= IntToStr(I + 1) + '%' + PathSectionNames_out[I];

  // Пути
  for I := 0 to DataBase_.Count - 1 do
  begin
    S1:= HeaderStrToString(DataBase_.Item[I].PathSectionName);
    if PathSectionNames.IndexOf(S1) = - 1 then ShowMessage('Ошибка - Пути');
    S2:= IntToStr(PathSectionNames.IndexOf(S1) + 1) + '%' + HeaderStrToString(DataBase_.Item[I].RailPathNumber);
    if Paths_out.IndexOf(S2) = - 1 then Paths_out.Add(S2);
  end;
  for I := 0 to Paths_out.Count - 1 do Paths_out[I]:= IntToStr(I + 1) + '%' + Paths_out[I];

  // Точки
  DefCnt:= 0;
  for I := 0 to DataBase_.Count - 1 do
  begin
    {$Q+}

    RRidx:= RailRoadNames_out.IndexOf(HeaderStrToString(DataBase_.Item[I].RailRoadName));
    RRidx_:= RailRoadNames_out.Objects[RRidx];

    for K := 0 to Organizations_out.Count - 1 do
      if (Organizations_out[K]Self = HeaderStrToString(DataBase_.Item[I].Organization)) then



    Orgidx:= Organizations.IndexOf(HeaderStrToString(DataBase_.Item[I].Organization));
    PSectionidx:= PathSectionNames.IndexOf(HeaderStrToString(DataBase_.Item[I].PathSectionName));
    Pathidx_:= Paths.IndexOf(HeaderStrToString(DataBase_.Item[I].RailPathNumber));

    Data:= TAviconDataSource.Create;


    if Data.LoadFromFile(DataBase_.Item[I].FileName) then
    begin

      Rec.ID:= 1; // Тип: 1 - Ведомость контроля / 2 - Запись блокнота / 3 - NORDCO
      Rec.Rail:= rLeft; // Нить: 0 - левая / 1 - правая
      Rec.DisCoord:= 100; // Дисплейная координата
      Rec.Defect:= StringToHeaderStr('Test #' + IntToStr(DefCnt)); // NORDCO::Type - Код дефекта по UIC 712
      Inc(DefCnt);
      Rec.BlokNotText:= StringToHeaderBigStr('None'); // Текст блокнота // NORDCO::Comment - Примечание
      Rec.DT:= now;
      Rec.SysCoord:= 100; // Звено2
      Data.NotebookAdd(Addr(Rec));

      for J:= 0 to Data.NotebookCount - 1 do
      begin
        try

          crd:= Data.DisToMRFCrd(Data.Notebook[J].DisCoord);

//          Marks.Add(Format('id:%d|RR:%d|ORG:%d|PATH:%d|TRACK:%s|KM:%d|PK:%d|M:%d|RAIL:%d|DEF:%s|TEXT:%s', [Marks.Count,
          Marks.Add(Format('%d|%d|%d|%d|%d|%d|%d|%d|%d|%s|%s', [Marks.Count, // 0
                                                                RRidx,       // 1
                                                                Orgidx,      // 2
                                                                PSectionidx,     // 3
                                                                Pathidx_, // HeaderStrToString(Data.Notebook[J].Track), // 4
                                                                crd.Km,                                    // 5
                                                                crd.Pk,                                    // 6
                                                                Round(crd.mm / 1000),                      // 7
                                                                Ord(Data.Notebook[J].Rail),                // 8
                                                                HeaderStrToString(Data.Notebook[J].Defect),    // 9
                                                                HeaderBigStrToString(Data.Notebook[J].BlokNotText)])); // 10

          //Marks.Add(Format('км %d ПК %d м %d; Нить: %s; Описание: %s %s', [s1, s2, s3, crd.Km, crd.Pk, Round(crd.mm / 1000), RailToStr(Data.Notebook[J].Rail, 0), HeaderStrToString(Data.Notebook[J].Defect), HeaderBigStrToString(Data.Notebook[J].BlokNotText)]));
        finally

        end;
      end;
    end;
    Data.Free;
    {$Q-}

  end;

  RailRoadNames_out.SaveToFile('D:\!_BASE\RailRoadNames.txt');

//  for I := 0 to PathSectionNames.Count - 1 do RailRoadNames[I]:= IntToStr(I) + '-' + PathSectionNames[I];
  PathSectionNames_out.SaveToFile('D:\!_BASE\PathSectionNames.txt');

  Organizations_out.SaveToFile('D:\!_BASE\Organizations.txt');
  Paths_out.SaveToFile('D:\!_BASE\Paths.txt');
  Marks.SaveToFile('D:\!_BASE\Marks.txt');

  PathSectionNames_out.Free;
  RailRoadNames_out.Free;
  Organizations_out.Free;
  Paths_out.Free;

  PathSectionNames.Free;
  RailRoadNames.Free;
  Organizations.Free;
  Marks.Free;
  Paths.Free;

  *)

  {$Q+}
  Marks:= TStringList.Create;
  for I := 0 to DataBase_.Count - 1 do
  begin
    Data:= TAviconDataSource.Create;
    if Data.LoadFromFile(DataBase_.Item[I].FileName) then
    begin

      Rec.ID:= 1; // Тип: 1 - Ведомость контроля / 2 - Запись блокнота / 3 - NORDCO
      Rec.Rail:= rLeft; // Нить: 0 - левая / 1 - правая
      Rec.DisCoord:= 100; // Дисплейная координата
      Rec.Defect:= StringToHeaderStr('Test #' + IntToStr(DefCnt)); // NORDCO::Type - Код дефекта по UIC 712
      Inc(DefCnt);
      Rec.BlokNotText:= StringToHeaderBigStr('None'); // Текст блокнота // NORDCO::Comment - Примечание
      Rec.DT:= now;
      Rec.SysCoord:= 100; // Звено2
      Data.NotebookAdd(Addr(Rec));

      str[1]:= HeaderStrToString(DataBase_.Item[I].RailRoadName);
      str[2]:= HeaderStrToString(DataBase_.Item[I].Organization);
      str[3]:= HeaderStrToString(DataBase_.Item[I].PathSectionName);
      str[4]:= HeaderStrToString(DataBase_.Item[I].RailPathNumber);


      for J:= 0 to Data.NotebookCount - 1 do
      begin
        try

          crd:= Data.DisToMRFCrd(Data.Notebook[J].DisCoord);

  //                       RR ORG:%d|PATH:%d|TRACK:%s|KM:%d|PK:%d|M:%d|RAIL:%d|DEF:%s|TEXT:%s', [Marks.Count,
          Marks.Add(Format('%s|%s|%s|%s|%d|%d|%d|%d|%s|%s', [str[1],
                                                                str[2],
                                                                str[3],
                                                                str[4],
                                                                crd.Km,                                    // 5
                                                                crd.Pk,                                    // 6
                                                                Round(crd.mm / 1000),                      // 7
                                                                Ord(Data.Notebook[J].Rail),                // 8
                                                                HeaderStrToString(Data.Notebook[J].Defect),    // 9
                                                                HeaderBigStrToString(Data.Notebook[J].BlokNotText)])); // 10

          //Marks.Add(Format('км %d ПК %d м %d; Нить: %s; Описание: %s %s', [s1, s2, s3, crd.Km, crd.Pk, Round(crd.mm / 1000), RailToStr(Data.Notebook[J].Rail, 0), HeaderStrToString(Data.Notebook[J].Defect), HeaderBigStrToString(Data.Notebook[J].BlokNotText)]));
        finally

        end;
      end;
    end;
    Data.Free;
  end;
  {$Q-}
  Marks.SaveToFile('D:\!_BASE\Marks.txt');

end;

procedure TMainForm.SpeedButton8Click(Sender: TObject);
begin
  ActiveViewForm.Display.Refresh;
end;

procedure TMainForm.tbOpenPopup(Sender: TTBCustomItem; FromLink: Boolean);
begin
  miFileManClick(Self);
end;

procedure TMainForm.TBItem4Click(Sender: TObject);
var
  I: Integer;

begin
  for I := 0 to MainForm.MDIChildCount - 1 do
    if MainForm.MDIChildren[I] is TAvk11ViewForm then
    begin
      TAvk11ViewForm(MainForm.MDIChildren[I]).Display.Show0CoordEvent := TBItem4.Checked;
      TAvk11ViewForm(MainForm.MDIChildren[I]).Display.Refresh;
    end;
end;

procedure TMainForm.TBItem6Click(Sender: TObject);
var
  I: Integer;

begin
  for I := 0 to MainForm.MDIChildCount - 1 do
    if MainForm.MDIChildren[I] is TAvk11ViewForm then
    begin
      TAvk11ViewForm(MainForm.MDIChildren[I]).Display.Show0EchoCountEvent := TBItem6.Checked;
      TAvk11ViewForm(MainForm.MDIChildren[I]).Display.Refresh;
    end;
end;

procedure TMainForm.TBItem5Click(Sender: TObject);
var
  Value: string;

begin
  if ActiveViewForm <> nil then
    if InputQuery('', 'Enter Password', Value) then
      if Value = 'ddeenn' then
        ActiveViewForm.DatSrc.CutExHeader;
end;

procedure TMainForm.miClearLastDwdFilesClick(Sender: TObject);
begin
  Config.DownLoadFilesHistory.Clear;
end;

procedure TMainForm.TBItem18Click(Sender: TObject);
var
  CoordGraphForm: TCoordGraphForm;
  A11View: TAvk11ViewForm;

begin
  A11View := ActiveViewForm;
  if A11View <> nil then
  begin
    CoordGraphForm := TCoordGraphForm.Create(nil);
    CoordGraphForm.CalcData(A11View.DatSrc);
  end;
end;

procedure TMainForm.miSameCoordFilesPopup(Sender: TTBCustomItem; FromLink: Boolean);
var
  I: Integer;
  New: TTBItem;
  Image: TBitmap;
  StartFile: Integer;
  EndFile: Integer;
  StartBase: Integer;
  EndBase: Integer;
  Sep: TTBSeparatorItem;
  CurX: Integer;
  GrapRt: TRect;
  BorRt: TRect;
  DrawRt: TRect;
  DrawWidth: Integer;
  GrapWidth: Integer;

  DatSrc_: TAvk11DatSrc;

begin

  GrapRt := Rect(0, 0, 119, 15);
  GrapWidth := GrapRt.Right - GrapRt.Left + 1;
  BorRt := Rect(1, 1, 118, 14);
  DrawRt := Rect(2, 2, 117, 13);
  DrawWidth := DrawRt.Right - DrawRt.Left + 1;

  miSameCoordFiles.Clear;
  ImageList2.Clear;
  ImageList2.Width := GrapWidth;

  if ActiveViewForm <> nil then
  begin
    DatSrc_ := ActiveViewForm.DatSrc;
    with DatSrc_ do
    begin
      StartFile := Min(RealCoordToReal_(StartRealCoord), RealCoordToReal_(EndRealCoord));
      EndFile := Max(RealCoordToReal_(StartRealCoord), RealCoordToReal_(EndRealCoord));
    end;

    if ActiveViewForm.DatSrc.MaxDisCoord <> 0 then
      if DatSrc_.Header.MoveDir > 0 then
        CurX := DrawRt.Left + Round(DrawWidth * ActiveViewForm.Display.CenterDisCoord / ActiveViewForm.DatSrc.MaxDisCoord)
      else
        CurX := DrawRt.Left + Round(DrawWidth * (ActiveViewForm.DatSrc.MaxDisCoord - ActiveViewForm.Display.CenterDisCoord) / ActiveViewForm.DatSrc.MaxDisCoord);

    with ActiveViewForm.DatSrc do
      for I := 0 to DataBase_.Count - 1 do
        if (HeaderStrToString(DatSrc_.Header.PathSectionName) = HeaderStrToString(DataBase_.Item[I].PathSectionName)) and
           (DatSrc_.GetRailPathNumber_in_HeadStr = DataBase_.Item[I].RailPathNumber) and
           (FileName <> DataBase_.Item[I].FileName) then
        begin

          with DataBase_.Item[I] do
          begin
            StartBase := Min(RealCoordToReal_(DataBase_.ItemStartRealCoord[I]), RealCoordToReal_(DataBase_.ItemEndRealCoord[I]));
            EndBase := Max(RealCoordToReal_(DataBase_.ItemStartRealCoord[I]), RealCoordToReal_(DataBase_.ItemEndRealCoord[I]));
          end;

          if (StartFile <= EndBase) and (EndFile >= StartBase) then
          begin
            Image := TBitmap.Create;
            with Image, Canvas do
            begin
              Width := GrapWidth;
              Height := 16;

              Brush.Color := clWindow;
              FillRect(GrapRt);
              Pen.Color := clBlack;
              Rectangle(BorRt);
              Brush.Color := ConfigUnit.Config.GetMoveDirColor(DataBase_.Item[I].MoveDir);

              if EndFile <> StartFile then

                FillRect(Rect(Max(DrawRt.Left, DrawRt.Left + Round((StartBase - StartFile) / (EndFile - StartFile) * DrawWidth)), DrawRt.Top, Min(DrawRt.Right, DrawRt.Left + Round((EndBase - StartFile) / (EndFile - StartFile) * DrawWidth) + 1), DrawRt.Bottom));

              Brush.Color := clWindow;
              Rectangle(CurX - 1, GrapRt.Top, CurX + 2, GrapRt.Bottom);
            end;

            New := TTBItem.Create(Self);
            New.Caption := LangTable.Caption['InfoBar:Date'] + ': ' + DateToStr(DataBase_.ItemDate[I]) + ' - ' + LangTable.Caption['MainMenu:File'] + ': ' + ExtractFileName(DataBase_.Item[I].FileName);

            // New.Caption:= DataBase_.Item[I].PathSectionName + ' - ' + IntToStr(ActiveViewForm.DatSrc.Header.RailPathNumber);

            // if (ActiveViewForm.DatSrc.Header.RailPathNumber = DataBase_.Item[I].RailPathNumber) and
            // (ActiveViewForm.DatSrc.Header.PathSectionName = DataBase_.Item[I].PathSectionName) then New.Caption:= New.Caption + '[+]';

            New.ImageIndex := ImageList2.Add(Image, nil);
            New.Images := ImageList2;
            New.Tag := I;
            New.Name := 'SameCoordFile_' + IntToStr(I);
            New.OnClick := SameCoordClick;
            miSameCoordFiles.Add(New);
          end;
        end;
    if miSameCoordFiles.Count <> 0 then
    begin
      Sep := TTBSeparatorItem.Create(Self);
      Sep.Name := 'File_Sep';
      miSameCoordFiles.Insert(0, Sep);

      Image := TBitmap.Create;
      with Image, Canvas do
      begin
        Width := GrapWidth;
        Height := 16;
        Brush.Color := clWindow;
        FillRect(GrapRt);
        Pen.Color := clBlack;
        Rectangle(BorRt);
        { case ActiveViewForm.DatSrc.Header.MoveDir of
          -1: Brush.Color:= clGreen;
          1: Brush.Color:= clRed;
          end; }
        Brush.Color := ConfigUnit.Config.GetMoveDirColor(ActiveViewForm.DatSrc.Header.MoveDir);

        FillRect(DrawRt);

        Brush.Color := clWindow;
        Rectangle(CurX - 1, GrapRt.Top, CurX + 2, GrapRt.Bottom);
      end;

      New := TTBItem.Create(Self);
      try
        with ActiveViewForm do
          New.Caption := LangTable.Caption['InfoBar:Date'] + ': ' + DateToStr(EncodeDate(DatSrc.Header.Year, DatSrc.Header.Month, DatSrc.Header.Day)) + ' - ' + LangTable.Caption['MainMenu:File'] + ': ' + ExtractFileName(ActiveViewForm.DatSrc.FileName);
      except
        with ActiveViewForm do
          New.Caption := 'Error';
      end;
      // New.Caption:= ActiveViewForm.DatSrc.Header.PathSectionName + ' - ' + IntToStr(ActiveViewForm.DatSrc.Header.RailPathNumber);
      New.ImageIndex := ImageList2.Add(Image, nil);
      New.Images := ImageList2;
      New.Name := 'SameCoordFile_A';
      miSameCoordFiles.Insert(0, New);

      Exit;
    end;
  end;

  New := TTBItem.Create(Self);
  New.Caption := LangTable.Caption['Common:Empty'];
  New.Name := 'No_SameCoordFile';
  miSameCoordFiles.Add(New);

end;

procedure TMainForm.miLincWinClick(Sender: TObject);
begin
  if ActiveViewForm = nil then
    Exit;
  with TAssignCtrlForm.Create(nil) do
    Execute(ActiveViewForm);
end;

procedure TMainForm.miMetallSensorValue0Click(Sender: TObject);
begin
  // miMetallSensorZone.Checked:= False;
  miUnstableBottomSignal.Checked := False;
  miZerroProbeMode.Checked := False;
  miPaintSystemState.Checked := False;

  miMetallSensorValue0.Checked := not miMetallSensorValue0.Checked;
  miUpdate();
end;

procedure TMainForm.miMetallSensorValue1Click(Sender: TObject);
begin
  // miMetallSensorZone.Checked:= False;
  miUnstableBottomSignal.Checked := False;
  miZerroProbeMode.Checked := False;
  miPaintSystemState.Checked := False;

  miMetallSensorValue1.Checked := not miMetallSensorValue1.Checked;
  miUpdate();
end;

procedure TMainForm.miMetallSensorZoneClick(Sender: TObject);
begin
  if not miMetallSensorZone.Checked then
  begin
    miMetallSensorZone.Checked := True;
    { miMetallSensorValue0.Checked:= False;
      miMetallSensorValue1.Checked:= False;
      miUnstableBottomSignal.Checked:= False;
      miZerroProbeMode.Checked:= False;
      miPaintSystemState.Checked:= False; }

  end
  else
    miMetallSensorZone.Checked := False;
  miUpdate();
end;

procedure TMainForm.miPaintSystemStateClick(Sender: TObject);
begin
  if not miPaintSystemState.Checked then
  begin
    miPaintSystemState.Checked := True;
    // miMetallSensorZone.Checked:= False;
    miMetallSensorValue0.Checked := False;
    miMetallSensorValue1.Checked := False;
    miUnstableBottomSignal.Checked := False;
    miZerroProbeMode.Checked := False;
  end
  else
    miPaintSystemState.Checked := False;
  miUpdate();
end;

procedure TMainForm.miZerroProbeModeClick(Sender: TObject);
begin
  if not miZerroProbeMode.Checked then
  begin
    miZerroProbeMode.Checked := True;
    // miMetallSensorZone.Checked:= False;
    miMetallSensorValue0.Checked := False;
    miMetallSensorValue1.Checked := False;
    miUnstableBottomSignal.Checked := False;
    miPaintSystemState.Checked := False;
  end
  else
    miZerroProbeMode.Checked := False;
  miUpdate();
end;

procedure TMainForm.miUnstableBottomSignalClick(Sender: TObject);
begin
  if not miUnstableBottomSignal.Checked then
  begin
    miUnstableBottomSignal.Checked := True;
    // miMetallSensorZone.Checked:= False;
    miMetallSensorValue0.Checked := False;
    miMetallSensorValue1.Checked := False;
    miZerroProbeMode.Checked := False;
    miPaintSystemState.Checked := False;
  end
  else
    miUnstableBottomSignal.Checked := False;
  miUpdate();
end;

procedure TMainForm.miViewPaintSystemStateClick(Sender: TObject);
begin
  miViewPaintSystemState.Checked := not miViewPaintSystemState.Checked;
  miUpdate();
end;

procedure TMainForm.miUpdate();
begin
  Config.MetallSensorZoneState := miMetallSensorZone.Checked;
  Config.UnstableBottomSignalState := miUnstableBottomSignal.Checked;
  Config.ZerroProbeModeState := miZerroProbeMode.Checked;
  Config.PaintSystemStateState := miPaintSystemState.Checked;
  Config.MetalSensorValue0State := miMetallSensorValue0.Checked;
  Config.MetalSensorValue1State := miMetallSensorValue1.Checked;
  Config.PaintSystemViewResState := miViewPaintSystemState.Checked;

  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.MetallSensorValue0 := miMetallSensorValue0.Checked;
    ActiveViewForm.Display.MetallSensorValue1 := miMetallSensorValue1.Checked;

    ActiveViewForm.Display.MetallSensorZoneState := Config.MetallSensorZoneState;
    ActiveViewForm.Display.UnstableBottomSignalState := Config.UnstableBottomSignalState;
    ActiveViewForm.Display.ZerroProbeModeState := Config.ZerroProbeModeState;
    ActiveViewForm.Display.PaintSystemStateState := Config.PaintSystemStateState;
    ActiveViewForm.Display.PaintSystemViewResState := Config.PaintSystemViewResState;
    ActiveViewForm.Display.PaintSystemViewResDebug := Config.PaintSystemViewResDebug;
//    ActiveViewForm.Display.AutomaticSearchResState := Config.AutomaticSearchResState;

    ActiveViewForm.Display.FullRefresh;
  end;
end;

procedure TMainForm.SameCoordClick(Sender: TObject);
var
  Save: TAvk11ViewForm;

begin
  if ActiveViewForm = nil then
    Exit;
  Save := ActiveViewForm;
  OpenFile(DataBase_.Item[TTBItem(Sender).Tag].FileName);
  if ActiveViewForm = nil then
    Exit;
  Save.Display.AssignTo(ActiveViewForm.Display);
  ActiveViewForm.Display.AssignTo(Save.Display);

  if Assigned(FileBaseForm) then
    FileBaseForm.WindowState := wsMinimized;
  // WindowTileHorizontal1.Execute;
  // WindowTileHorizontal1.Execute;
  Save.SetFocus_;
  Save.SetFocus;
  // WindowTileHorizontal1.Execute;
  Self.Refresh;
  WindowTileHorizontalTimer.Enabled := True;
end;

var
  First, Stop: Boolean;
//  OutDat: TAvk11DatDst;
  OutDat2: TMemoryStream;
//  LogLog: TStringList;
  EIndex: Integer;
  ds: TAvk11DatSrc;
  CreateBaseProgForm: TCreateBaseProgForm;

function TMainForm.LoadDataEvent(StartDisCoord: Integer): Boolean;
{var
  R: TRail;
  Ch, I: Integer;
  R_: Byte;
  ID: Byte;
  pData: PEventData;
}
begin
(*
  OutDat.AddSysCoord(ActiveViewForm.DatSrc.CurSysCoord);
  for R := rLeft to rRight do
    for Ch := 1 to 11 do
      if (Ch <> 8) and (Ch <> 9) then
        with ActiveViewForm.DatSrc.CurEcho[R, Ch] do
          if Count <> 0 then
            OutDat.AddEcho(Ord(R = rLeft), Ch, Count, Delay[1], Ampl[1], Delay[2], Ampl[2], Delay[3], Ampl[3], Delay[4], Ampl[4], Delay[5], Ampl[5], Delay[6], Ampl[6], Delay[7], Ampl[7], Delay[8], Ampl[8]);
  if First then
  begin
    for R := rLeft to rRight do
      for Ch := 0 to 9 do
      begin
        R_ := Ord(R = rLeft);
        OutDat.AddSens(R_, Ch, ActiveViewForm.DatSrc.CurParams.Par[R][Ch].Ku);
        OutDat.AddAtt(R_, Ch, ActiveViewForm.DatSrc.CurParams.Par[R][Ch].Att);
        OutDat.AddVRU(R_, Ch, ActiveViewForm.DatSrc.CurParams.Par[R][Ch].TVG);
        OutDat.AddStStr(R_, Ch, ActiveViewForm.DatSrc.CurParams.Par[R][Ch].StStr);
        OutDat.AddEndStr(R_, Ch, ActiveViewForm.DatSrc.CurParams.Par[R][Ch].EndStr);
        OutDat.Add2Tp(R_, Ch, ActiveViewForm.DatSrc.CurParams.Par[R][Ch].PrismDelay);
      end;
    First := False;
  end;
  {
    if (ds.EventCount <> 0) and (Length(ds.LabelList) <> 0) then
    if not Stop then
    begin
    while ds.Event[ds.LabelList[EIndex]].DisCoord < ds.CurDisCoord do
    if EIndex < High(ds.LabelList) then Inc(EIndex) else Stop:= True;

    if not Stop then
    while ds.Event[ds.LabelList[EIndex]].DisCoord = ds.CurDisCoord do
    begin
    if ds.Event[ds.LabelList[EIndex]].ID = EID_Stolb then
    begin
    ds.GetEventData(ds.LabelList[EIndex], ID, pData);
    OutDat.AddStolb(pCoordPost(pData)^.Km[0], pCoordPost(pData)^.Km[1], pCoordPost(pData)^.Pk[0], pCoordPost(pData)^.Pk[1]);
    end;
    if ds.Event[ds.LabelList[EIndex]].ID = EID_StBoltStyk then
    begin
    OutDat.AddStBoltStyk()
    end;
    if ds.Event[ds.LabelList[EIndex]].ID = EID_EndBoltStyk then
    begin
    OutDat.AddEdBoltStyk;
    end;
    if EIndex < High(ds.LabelList) then Inc(EIndex) else begin Stop:= True; Break; end;
    end;
    end;
  }

  if (ds.EventCount <> 0) and (Length(ds.LabelList) <> 0) then
    if not Stop then
    begin
      while ds.Event[EIndex].DisCoord < ds.CurDisCoord do
        if EIndex < ds.EventCount - 1 then
          Inc(EIndex)
        else
          Stop := True;

      if not Stop then
        while ds.Event[EIndex].DisCoord = ds.CurDisCoord do
        begin
          if ds.Event[EIndex].ID = EID_Stolb then
          begin
            ds.GetEventData(EIndex, ID, pData);
            OutDat.AddStolb(pCoordPost(pData)^.Km[0], pCoordPost(pData)^.Km[1], pCoordPost(pData)^.Pk[0], pCoordPost(pData)^.Pk[1]);
          end;
          if ds.Event[EIndex].ID = EID_StBoltStyk then
          begin
            OutDat.AddStBoltStyk()
          end;
          if ds.Event[EIndex].ID = EID_EndBoltStyk then
          begin
            OutDat.AddEdBoltStyk;
          end;
          if EIndex < ds.EventCount then
            Inc(EIndex)
          else
          begin
            Stop := True;
            Break;
          end;
        end;
    end;

  I := 100 * ActiveViewForm.DatSrc.CurDisCoord div ActiveViewForm.DatSrc.MaxDisCoord;
  CreateBaseProgForm.Gauge1.Position := I;
  Result := True;
  *)
end;
(*
procedure TMainForm.SaveAsA11ButtonClick(Sender: TObject);
var
  DW: DWord;

begin
  if ActiveViewForm <> nil then
    if SaveDialog1.Execute then
    begin

      CreateBaseProgForm := TCreateBaseProgForm.Create(nil);
      CreateBaseProgForm.Visible := True;
      CreateBaseProgForm.BringToFront;
      CreateBaseProgForm.SetFocus;
      CreateBaseProgForm.Caption := 'Преобразование в файл формата Авикон-11';
      CreateBaseProgForm.Panel2.Visible := False;
      CreateBaseProgForm.Panel3.Visible := False;
      CreateBaseProgForm.Height := 65;
      CreateBaseProgForm.Refresh;
      Self.Refresh;
      DW := GetTickCount();
      while (GetTickCount() - DW < 1000) do
        Sleep(0);
      EIndex := 0;
      Stop := False;

      OutDat := TAvk11DatDst.Create;
      OutDat.CreateFile(SaveDialog1.FileName, 0);
      OutDat.FillHeader(now, 'test', 'test', 'test');
      OutDat.Header.StartKM := ActiveViewForm.DatSrc.Header.StartKM;
      OutDat.Header.StartPk := ActiveViewForm.DatSrc.Header.StartPk;
      OutDat.Header.StartMetre := ActiveViewForm.DatSrc.Header.StartMetre;
      ds := ActiveViewForm.DatSrc;

      OutDat.AddHeader;

      First := True;
      ActiveViewForm.DatSrc.LoadData(0, ActiveViewForm.DatSrc.MaxDisCoord { div 4 } , 0, LoadDataEvent);
      OutDat.CloseFile;

      CreateBaseProgForm.Release;
    end;
end;
*)
procedure TMainForm.WindowTileHorizontalTimerTimer(Sender: TObject);
begin
  WindowTileHorizontalTimer.Enabled := False;
  WindowTileHorizontal1.Execute;
end;

procedure TMainForm.ScanStepBtmClick(Sender: TObject);
var
  a: TFileHeader;

begin
  if ActiveViewForm <> nil then
  begin
    a := ActiveViewForm.DatSrc.Header;
    a.ScanStep := StrToInt(Edit1.Text);
    ActiveViewForm.DatSrc.Header := a;
  end;
end;

procedure TMainForm.ExcludeAssign(V: TAvk11ViewForm);
var
  I: Integer;

begin
  for I := 0 to MDIChildCount - 1 do
    if (MDIChildren[I] is TAvk11ViewForm) then
      TAvk11ViewForm(MDIChildren[I]).Display.DeleteAssign(V.Display);
end;

procedure TMainForm.miWinManPopup(Sender: TTBCustomItem; FromLink: Boolean);
var
  I, J: Integer;

begin
  J := 0;
  for I := 0 to MDIChildCount - 1 do
    if (MDIChildren[I] is TAvk11ViewForm) then
      Inc(J);
  miLincWin.Enabled := J >= 2;

  miNextWin.Enabled := Self.MDIChildCount > 1;
end;

procedure TMainForm.miViewTestRecordClick(Sender: TObject);
var
  FN: string;

begin
  if ActiveViewForm <> nil then
  begin
    FN := DataFileDir + 'Файл тупика ' + IntToStr(GetTickCount());
    ActiveViewForm.DatSrc.ExtractTestRecordFile(FN);
    OpenFile(FN, False, True, True);
  end;
end;

var
 AVF: TAvk11ViewForm;

function TMainForm.LoadDataEvent2(StartDisCoord: Integer): Boolean;
var
  R: TRail;
  Ch, I, J, K: Integer;
  R_: Byte;
  ID: Byte;
  pData: PEventData;
  Cnt: Integer;
  RIndex: array [0..16] of TRail;
  ChIndex: array [0..16] of Integer;
  CIDIndex: array [0..16] of Integer;

begin

{

XSysCrd: Integer; // координата
ChannelCount: Byte; // количествo каналов вкоторых есть сигналы на данной координате


далее ChannelCount записей в следующем формате:


	Side: Byte; // Сторона
	Channel: Byte; // Канал CID
	SignalCount: Byte; // Количество сигналов

	далее SignalCount записей в следующем формате:

		Delay: Byte;
		Ampl: Byte;
}


  Cnt:= 0;
  for R := rLeft to rRight do
    for Ch := 1 to 11 do
      if AVF.DatSrc.CurEcho[R, Ch].Count <> 0 then
      begin
        RIndex[Cnt]:= R;
        ChIndex[Cnt]:= Ch;
        CIDIndex[Cnt]:= AVF.DatSrc.Header.ChIdxtoCID[Ch];
        Cnt:= Cnt + 1;
      end;

//  if Cnt <> 0 then
  begin

    OutDat2.WriteBuffer(AVF.DatSrc.CurSysCoord, 4);
//    LogLog.Add(Format('SysCrd: %d', [AVF.DatSrc.CurSysCoord]));

    OutDat2.WriteBuffer(Cnt, 1);
//    LogLog.Add(Format('ChCnt: %d', [Cnt]));
    for J := 0 to Cnt - 1 do
    begin
      OutDat2.WriteBuffer(RIndex[J], 1);
      OutDat2.WriteBuffer(CIDIndex[J], 1);
      OutDat2.WriteBuffer(AVF.DatSrc.CurEcho[RIndex[J], ChIndex[J]].Count, 1);
  {
      LogLog.Add(Format(' Rail: %d', [Ord(RIndex[J])]));
      LogLog.Add(Format(' CID: %d', [CIDIndex[J]]));
      LogLog.Add(Format(' Echo Cnt: %d', [AVF.DatSrc.CurEcho[RIndex[J], ChIndex[J]].Count]));
       }
      for K := 1 to AVF.DatSrc.CurEcho[RIndex[J], ChIndex[J]].Count do
      begin
        OutDat2.WriteBuffer(AVF.DatSrc.CurEcho[RIndex[J], ChIndex[J]].Delay[K], 1);
        OutDat2.WriteBuffer(AVF.DatSrc.CurEcho[RIndex[J], ChIndex[J]].Ampl[K], 1);
      {
        LogLog.Add(Format('  %d. Delay %d', [K, AVF.DatSrc.CurEcho[RIndex[J], ChIndex[J]].Delay[K]]));
        LogLog.Add(Format('  %d. Ampl  %d',  [K, AVF.DatSrc.CurEcho[RIndex[J], ChIndex[J]].Ampl[K]]));
        }
      end;
    end;

    I := 100 * AVF.DatSrc.CurDisCoord div AVF.DatSrc.MaxDisCoord;
    CreateBaseProgForm.Gauge1.Position := I;
  end;
  Result := True;
end;

procedure TMainForm.TBItem31Click(Sender: TObject);
var
  DW: DWord;

begin
  if ActiveViewForm <> nil then
    if SaveDialog1.Execute then
    begin
      AVF:= ActiveViewForm;

      CreateBaseProgForm := TCreateBaseProgForm.Create(nil);
      CreateBaseProgForm.Visible := True;
      CreateBaseProgForm.BringToFront;
      CreateBaseProgForm.SetFocus;
      CreateBaseProgForm.Caption := 'Преобразование в файл формата БУМ';
      CreateBaseProgForm.Panel2.Visible := False;
      CreateBaseProgForm.Panel3.Visible := False;
      CreateBaseProgForm.Height := 65;
      CreateBaseProgForm.Refresh;
      Self.Refresh;
      DW := GetTickCount();
      while (GetTickCount() - DW < 1000) do Sleep(0);
      EIndex := 0;
      Stop := False;

      OutDat2 := TMemoryStream.Create();
//      LogLog:= TStringList.Create;

      ds := AVF.DatSrc;

      First := True;
      AVF.DatSrc.LoadData(AVF.Display.StartDisCoord, AVF.Display.EndDisCoord, 0, LoadDataEvent2);
      OutDat2.SaveToFile(SaveDialog1.FileName);
      OutDat2.Free;

//      LogLog.SaveToFile(SaveDialog1.FileName + '_log.txt');
//      LogLog.Free;

      CreateBaseProgForm.Release;
    end;
end;

procedure TMainForm.TBItem32Click(Sender: TObject);
begin
  Config.ShowEventIndex := TBItem32.Checked;
end;

procedure TMainForm.TBItem33Click(Sender: TObject);
begin
 // Config.ShowUSBLog := TBItem33.Checked;
end;

procedure TMainForm.TBItem35Click(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    ActiveViewForm.DatSrc.FilterACData(60);
end;

procedure TMainForm.TBItem36Click(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    ActiveViewForm.Display.DrawSameCoordMode:= TBItem36.Checked;
end;

procedure TMainForm.tbVideoClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    tbVideo.Checked := not tbVideo.Checked;
    if tbVideo.Checked and tbNewFileRec.Checked then tbNewFileRec.OnClick(nil);
    ActiveViewForm.SelectVideo:= tbVideo.Checked;

//    if (Config.ProgramMode <> pmRADIOAVIONICA_MAV) then
//      ActiveViewForm.ADDtoNoteBook := tbNewFileRec.Checked
//    else
//      ActiveViewForm.ADDtoNordCo := tbNewFileRec.Checked;
  end;
end;

procedure TMainForm.tbGPSTrackClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    showPathOnMap(ActiveViewForm.DatSrc);
end;

procedure TMainForm.tbGPSTrackClick_(Sender: TObject);
begin
  MenuItemClick(Sender);
  Config.EchoSizeByAmpl := miEchoSizeByAmpl.Checked;
end;

procedure TMainForm.miSmallPointClick(Sender: TObject);
begin
  Config.EchoSize := -1;
  MenuItemClick(Sender);
end;

procedure TMainForm.miMediumPointClick(Sender: TObject);
begin
  Config.EchoSize := 0;
  MenuItemClick(Sender);
end;

procedure TMainForm.miLargePointClick(Sender: TObject);
begin
  Config.EchoSize := 1;
  MenuItemClick(Sender);
end;

procedure TMainForm.FormShow(Sender: TObject);
var
  I: Integer;

begin
  if FOpenFileFlag then
  begin
    for I := 1 to ParamCount do
      if FileExists(ParamStr(I)) then
        OpenFile(ParamStr(I));
    FOpenFileFlag := False;
  end;
end;

procedure TMainForm.TBItem7Click(Sender: TObject);
var
  Value: string;
  New, Code: Integer;

begin
  if ActiveViewForm <> nil then
  begin
    Value := IntToStr(ActiveViewForm.DatSrc.Header.ScanStep);
    if InputQuery('Scan Step Edit', 'Enter new scan step value', Value) then
    begin
      Val(Value, New, Code);
      if Code = 0 then
        ActiveViewForm.DatSrc.SetScanStep(New);
    end;
  end;
end;

procedure TMainForm.miEchoColorByAmplClick(Sender: TObject);
begin
  MenuItemClick(Sender);
  Config.EchoColorByAmpl := miEchoColorByAmpl.Checked;
end;

procedure TMainForm.RepaintChildren;
var
  I: Integer;

begin
  for I := 0 to MDIChildCount - 1 do
    if (MDIChildren[I] is TAvk11ViewForm) then
      with TAvk11ViewForm(MDIChildren[I]) do
      begin
        Display.Refresh;
        Repaint;
      end;
end;

procedure TMainForm.TBInvertClick(Sender: TObject);
begin
  RepaintChildren;
end;

procedure TMainForm.TBItem8Click(Sender: TObject);
begin
  if Assigned(InfoBarForm) then
  begin
    InfoBarForm.DebugInfo := MainForm.TBItem8.Checked;
    MainForm.DebugPanel.Visible := MainForm.TBItem8.Checked;
  end;
end;

procedure TMainForm.CMChildKey(var Message: TCMChildKey);
var
  Msg: TWMKey;
  Handled: Boolean;

begin
  (*
    if ((Message.Sender = ListView1) or
    (Message.Sender = ListView2)) and

    (Message.CharCode in [VK_RIGHT,
    VK_LEFT,
    VK_UP,
    VK_DOWN,
    VK_END,
    VK_HOME,
    VK_PRIOR,
    VK_NEXT,
    VK_RETURN]) then
    begin
    if ActiveViewForm <> nil then
    begin
    ActiveViewForm.SetFocus_;
    Msg.CharCode:= Message.CharCode;
    ActiveViewForm.FormShortCut(Msg, Handled);
    end;
    Message.Result:= 1;
    end else inherited;
    *)
end;

procedure TMainForm.miAddNewRecMenuPopup(Sender: TTBCustomItem; FromLink: Boolean);
begin
  if tbNewCVSRec.Checked then
  begin
    miInFile.Enabled := False;
    miInNordico.Enabled := False;
  end
  else
  begin
    miInFile.Enabled := True;
    miInNordico.Enabled := True;
  end;
end;

procedure TMainForm.miBackMotionClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.SkipBackMotion := not miBackMotion.Checked;
    // DisplayModeToControl(ActiveViewForm);
  end;
end;

procedure TMainForm.miViewACClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin

    if ActiveViewForm.DatSrc.Header.UsedItems[uiAcousticContact] <> 1 then
    begin
      ShowMessage('Нет данных акустического контакта' (*LangTable.Caption['Messages:NeedNewVer']*)); //NOTRANSLATE
      Exit;
    end;

    ActiveViewForm.DatSrc.AnalyzeAC(miDebug.Visible);

    ActiveViewForm.Display.ViewAC := miViewAC.Checked;
    ActiveViewForm.Display.FullRefresh;
    // DisplayModeToControl(ActiveViewForm);
  end;
end;

procedure TMainForm.miViewAutomaticSearchResClick(Sender: TObject);
begin
  miViewAutomaticSearchRes.Checked := not miViewAutomaticSearchRes.Checked;
  Config.AutomaticSearchResState := miViewAutomaticSearchRes.Checked;
  if ActiveViewForm <> nil then
    ActiveViewForm.Display.FullRefresh;
end;

procedure TMainForm.miChSettingsClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.ShowChSettings := not ActiveViewForm.Display.ShowChSettings;
    Config.ShowChSettings := ActiveViewForm.Display.ShowChSettings;
    ActiveViewForm.Display.FullRefresh;
  end;
end;

procedure TMainForm.miChGateClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.ShowChGate := not ActiveViewForm.Display.ShowChGate;
    Config.ShowChGate := ActiveViewForm.Display.ShowChGate;
    ActiveViewForm.Display.FullRefresh;
  end;
end;

procedure TMainForm.miChInfoClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.ShowChInfo := not ActiveViewForm.Display.ShowChInfo;
    Config.ShowChInfo := ActiveViewForm.Display.ShowChInfo;
    ActiveViewForm.Display.FullRefresh;
  end;
end;

procedure TMainForm.Action1Execute(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    ShellExecute(Application.Handle, nil, 'explorer', PChar(ExtractFilePath(ActiveViewForm.DatSrc.FileName)), nil, SW_SHOWNORMAL);
  end;
end;

procedure TMainForm.miSavePieceClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    with TSaveAsForm.Create(nil) do
    begin
      SetData(ActiveViewForm);
      Showmodal;
      Free;
    end;
end;
{$IFDEF USEKEY}

function SearchKey: Boolean;
begin
  if (InitLib and $01) <> 0 then
  begin
    Result := True;
    Exit;
  end;
  Result := False;
  FinishLib;
end;

procedure SendCommand(NumFunc: Byte; Data: DWord; var Res_A, Res_B: DWord);
var
  ResByte: array [1 .. 8] of Byte;
  SrcByte: array [1 .. 11] of Byte;

begin
  SrcByte[7] := NumFunc;
  Move(Data, SrcByte[8], 4);
  QueryKey(@SrcByte, 11, @ResByte, 10);
  Move(ResByte[1], Res_A, 4);
  Move(ResByte[5], Res_B, 4);
end;
{$ENDIF USEKEY}

procedure TMainForm.TestBtnClick(Sender: TObject);
{
  type
  TDBFileItem = packed record
  FileName: WideString[255];       // Имя файла
  Length_: Integer;            // Длина файла в милиметрах
  end;

  var
  F: file;
  A: TDBFileItem;
  B: TDBFileItem;
  }

begin
  {
    AssignFile(F, 'X:\TEST');
    ReWrite(F, 1);
    A.FileName:= 'İleri';
    BlockWrite(F, A, SizeOf(A));
    CloseFile(F);

    AssignFile(F, 'X:\TEST');
    Reset(F, 1);
    BlockRead(F, B, SizeOf(A));
    CloseFile(F);
  }

  case TButton(Sender).Tag of
    0:
      if Assigned(ActiveMDIChild) and (ActiveMDIChild is TAvk11ViewForm) then
      begin
        // TAvk11ViewForm(ActiveMDIChild).Display.FullRefresh;
        // TAvk11ViewForm(ActiveMDIChild).Display.resize;
        TAvk11ViewForm(ActiveMDIChild).Display.Refresh;
      end;
    1:
      OpenFile('X:\WORK DATA\АВИКОН-16\avicon16scheme1.a16');
    2:
      OpenFile('X:\WORK DATA\АВИКОН-16\avicon16scheme2.a16');
    3:
      OpenFile('X:\WORK DATA\АВИКОН-16\Avicon16Wheel.a16');

    4:
      begin
        LangTable.CurrentGroup := 'Russian';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
      end;
    5:
      begin
        LangTable.CurrentGroup := 'English';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
      end;
    6:
      begin
        LangTable.CurrentGroup := 'Turkish';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
      end;
    7:
      begin
        LangTable.CurrentGroup := 'French';
        OnChangeLanguage(nil);
        OnChangeLanguage_(Self);
      end;
  end;
  DisplayModeToControl(Sender);
end;

procedure TMainForm.TestButtonClick(Sender: TObject);
var
  T: DWord;

begin
  //
  ActiveViewForm.Display.Zoom := 10000;
  ActiveViewForm.Display.CenterDisCoord := 35000;

  T := GetTickCount();
  ActiveViewForm.Display.Refresh;
  TestButton.Caption := Format('%d', [GetTickCount() - T]);
end;

procedure TMainForm.Timer2Timer(Sender: TObject);
var
  a, B: DWord;
  Key: DWord;

begin
{$IFDEF USEKEY}
{$IFDEF REC}
  if SearchKey then
  begin
    KeyExist := True;
    if Assigned(RecInt) then
      RecInt.Visible := True;
    // TBItem13.Visible:= True;
    // miSuspLoc.Visible:= True;
    Key := 100 xor $5CA117DE;
    SendCommand(1, Key, a, B);
  end
  else
  begin
    KeyExist := False;
    if Assigned(RecInt) then
      RecInt.Visible := False;
    // TBItem13.Visible:= False;
    // miSuspLoc.Visible:= False;
  end;
{$ENDIF REC}
{$ENDIF USEKEY}
end;

procedure TMainForm.Timer3Timer(Sender: TObject);
begin
  Timer3.Enabled := False;
  if Config.CDUVer then
  begin
    tbBaseClick(nil);
    FileBaseForm.TBItem2.Click;
  end;

  // OpenFile('testfile.a11');
  // OpenFile('X:\testfile.a16');

end;

procedure TMainForm.TopographTBItemSelect(Sender: TTBCustomItem; Viewer: TTBItemViewer; Selecting: Boolean);
begin
  TopographTBRect:= Viewer.BoundsRect;
end;

procedure TMainForm.miUncontrolZonesClick(Sender: TObject);
begin
  if (ActiveViewForm.DatSrc <> nil) and (ActiveViewForm.DatSrc.Config <> nil) then
  begin
    UCSForm.FormUpdate(ActiveViewForm, miDebug.Visible);
    UCSForm.BringToFront;
    UCSForm.Visible:= True;
  end;
end;

procedure TMainForm.TBItem3Click(Sender: TObject);
{$IFDEF VIEWBYKM}
var
  DBCont: TDBConnectForm;
  AVF: TAvk11ViewForm;
  Rec: TA11RecObj;
{$ENDIF}
begin
{$IFDEF VIEWBYKM}
{$IFDEF REC}
  if (ActiveViewForm <> nil) and (RecInt <> nil) then
  begin
    RecInt.Calc;
    AVF := ActiveViewForm;
    Rec := RecInt.CurRE;
    DBCont := TDBConnectForm.Create(nil);
    DBCont.Repaint;
    DBCont.ConnectToDB;
    DBCont.SetDate(AVF, AVF.DatSrc, Rec);
    DisplayModeToControl(DBCont);
  end;
{$ENDIF REC}
{$ENDIF}
end;

procedure TMainForm.miNextWinClick(Sender: TObject);
begin
  MainForm.Next;
end;

procedure TMainForm.miExportToNORDCOClick(Sender: TObject);
var
  I: Integer;
  FN: string;
  FNordcoCSVFile: TNordcoCSVFile;
  NewLine: TNordcoCSVRecordLine;

begin
  if ActiveViewForm <> nil then
    if ActiveViewForm.DatSrc.NotebookCount <> 0 then
    begin
      FNordcoCSVFile := TNordcoCSVFile.Create;
      for I := 0 to ActiveViewForm.DatSrc.NotebookCount - 1 do
        if ActiveViewForm.DatSrc.NoteBook[I].ID = 3 then
          with ActiveViewForm.DatSrc.NoteBook[I]^ do
          begin
            // BlokNotText:= StringToHeaderBigStr(Memo.Text); // Привязка / Текст блокнота (кодировка win)
            // Defect:= StringToHeaderStr(NameEdit.Text);
            // NewLine.Defect             := StringToHeaderStr(CodeDefectEdit.Text); // NORDCO::Type - Код дефекта по UIC 712
            // BlokNotText        := StringToHeaderBigStr(CommentEdit.Text); // NORDCO::Comment - Примечание
            NewLine.DataEntryDate := DT; // Дата составления списка дефектов
            NewLine.SurveyDate := DT; // Дата обнаружения дефекта
            NewLine.Line := HeaderStrToString(Line); // Название или идентификатор контролируемой линии
            NewLine.Track := HeaderStrToString(Track); // Идентификатор пути, в котором обнаружен дефект
            NewLine.LocationFrom := LocationFrom; // Начало обнаруженного дефекта
            NewLine.LocationTo := LocationTo; // Конец обнаруженного дефекта. В случае точечного дефекта то же самое, как поле  "Location From"
            NewLine.LatitudeFrom := LatitudeFrom; // GPS-широта для поля "Location From"
            NewLine.LatitudeTo := LatitudeTo; // GPS-широта для поля "Location To"
            NewLine.LongitudeFrom := LongitudeFrom; // GPS-долгота для поля "Location From"
            NewLine.LongitudeTo := LongitudeTo; // GPS-долгота для поля "Location To"
            // Тип верхнего строения пути - из списка: Left Rail, Right Rail
            NewLine.AssetType := HeaderStrToString(AssetType);
            NewLine.Source := HeaderStrToString(Source); // Метод обнаружения дефекта - из списка: Ultrasonic Trolley, Visual Inspection
            NewLine.Status := HeaderStrToString(Status); // Это поле указывает, закрыт ли дефект (т.е. удален из пути) - из списка: Open, Closed
            NewLine.IsFailure := IsFailure; // Если конкретный дефект вызвало отказ, то это поле должно иметь значение TRUE. Отказом можно считать любой дефект, который вызывает ограничение скорости
            NewLine.Size := Size; // Размер дефекта для  неточечного дефекта. Например, если дефект это трещина, то ее длина.
            NewLine.Size2 := Size2; // Размер дефекта для  неточечного дефекта (если более чем одномерный)
            NewLine.Size3 := Size3; // Размер дефекта для  неточечного дефекта (если более чем одномерный)
            NewLine.UnitofMeasurement := HeaderStrToString(UnitofMeasurement); // Ед. измерения поля "Size"
            NewLine.UnitofMeasurement2 := HeaderStrToString(UnitofMeasurement); // Ед. измерения поля "Size2"
            NewLine.UnitofMeasurement3 := HeaderStrToString(UnitofMeasurement); // Ед. измерения поля "Size3"
            NewLine.Operator_ := HeaderStrToString(Operator_); // Оператор// Оператор
            NewLine.Device := HeaderStrToString(Device); // Прибор

            FNordcoCSVFile.AddLine(NewLine);
          end;

      FN := ActiveViewForm.DatSrc.FileName + '.csv';
      FNordcoCSVFile.SaveToFile(FN);

      ShowMessage(LangTable.Caption['Messages:NORDCONExportOK'] + ' ' + FN);

    end
    else
      ShowMessage(LangTable.Caption['Messages:NORDCONExportError']);
end;

procedure TMainForm.miSensGraphClick(Sender: TObject);
begin
  if ActiveViewForm <> nil then
    with TKuGraphForm.Create(nil) do
    begin
      // SetDate(ActiveViewForm.DatSrc);
      Showmodal;
    end;
end;

procedure TMainForm.ListBox1DblClick(Sender: TObject);
begin
  // if ListBox1.ItemIndex <> - 1 then
  // OpenFile(ListBox1.Items[ListBox1.ItemIndex]);
end;

procedure TMainForm.SummFiles(Sender: TObject);
// if ActiveViewForm <> nil then ActiveViewForm.DatSrc.Test:= 1;
{
  var
  I: Integer;
  Src: TAvk11DatSrc;
  Dst: TMemoryStream;
  Header: TAvk11Header2;
  SysCrd: Integer;
}
begin
  {
    if SummOpenDialog.Execute then
    begin
    Dst:= TMemoryStream.Create;
    Dst.Position:= 0;
    SysCrd:= 0;
    for I:= 0 to SummOpenDialog.Files.Count - 1 do
    begin
    Src:= TAvk11DatSrc.Create;
    Src.LoadFromFile(SummOpenDialog.Files.Strings[I]);
    //      Src.SavePiece2(0, Src.MaxDisCoord, True, (I = 0), (I = SummOpenDialog.Files.Count - 1), Dst, SysCrd);
    end;
    if SaveDialog1.Execute then Dst.SaveToFile(SaveDialog1.FileName);
    Dst.Free;
    end;
    }
end;

procedure TMainForm.tbFilterClick(Sender: TObject);
begin
{$IFDEF ECHOFILTER}
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.FiltrCalc;
    DisplayModeToControl(ActiveViewForm);
  end;
{$ENDIF}
end;

procedure TMainForm.Filtr_HideClick(Sender: TObject);
begin
{$IFDEF ECHOFILTER}
  Config.FilterMode:= 3;
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.FilterMode := moHide;
    ActiveViewForm.Display.Refresh;
  end;
{$ENDIF}
end;

procedure TMainForm.Filtr_MarkClick(Sender: TObject);
begin
{$IFDEF ECHOFILTER}
  Config.FilterMode:= 2;
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.FilterMode := moMark;
    ActiveViewForm.Display.Refresh;
  end;
{$ENDIF}
end;

procedure TMainForm.Filtr_OffClick(Sender: TObject);
begin
{$IFDEF ECHOFILTER}
  Config.FilterMode:= 1;
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.FilterMode := moOff;
    ActiveViewForm.Display.Refresh;
  end;
{$ENDIF}
end;

procedure TMainForm.Filtr_CalcClick(Sender: TObject);
begin
{$IFDEF ECHOFILTER}
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.FiltrCalc;

    case Config.FilterMode of
      1: ActiveViewForm.Display.FilterMode := moOff;
      2: ActiveViewForm.Display.FilterMode := moMark;
      3: ActiveViewForm.Display.FilterMode := moHide;
    end;

    DisplayModeToControl(ActiveViewForm);
    ActiveViewForm.Display.Refresh;
  end;
{$ENDIF}
end;

procedure TMainForm.TBItem9Click(Sender: TObject);
begin
  if ActiveViewForm <> nil then
  begin
    ActiveViewForm.Display.DatSrc.UnpackBottom := TBItem9.Checked;
    ActiveViewForm.Display.Refresh;
  end;
end;

procedure TMainForm.TBItemTemperatureChartClick(Sender: TObject);
var
  I: Integer;
  X: Extended;
  ID_: Byte;
  pData: PEventData;

begin
  if (ActiveViewForm = nil) then Exit;

  TemperatureChart.Series1.Clear;
  with ActiveViewForm.DatSrc do
    for I := 0 to EventCount - 1 do
    begin
      GetEventData(I, ID_, pData);
      if ID_ = EID_Temperature then

//        X:= StrToFloat(IntToStr(DisToMRFCrd(Event[I].DisCoord).Km) {+ '.' + IntToStr(DisToMRFCrd(Event[I].DisCoord).Pk)});

        TemperatureChart.Series1.AddXY( {100 *} DisToMRFCrd(Event[I].DisCoord).Km * 1000 + DisToMRFCrd(Event[I].DisCoord).Pk * 100 + DisToMRFCrd(Event[I].DisCoord).mm / 1000 {Event[I].OffSet} {div FileBody.Size}, pedTemperature(@(pData^[2]))^.Value);
    end;


  TemperatureChart.Visible:= True;
end;

procedure TMainForm.Button14444Click(Sender: TObject);
begin

//  Self.Caption := IntToStr(ActiveViewForm.Display.PixelCount_);

end;

procedure TMainForm.Button231Click(Sender: TObject);
var
  I, C: Integer;

begin
  for I := 0 to ActiveViewForm.DatSrc.NotebookCount - 1 do
  begin
    if not Odd(I) then
      C := ActiveViewForm.DatSrc.NoteBook[I].DisCoord;
    if Odd(I) then
    begin
      // ActiveViewForm.DatSrc.SavePiece(C, ActiveViewForm.DatSrc.Notebook[I].DisCoord, False, 'X:\pice ' + IntToStr(I + 1) + '.a11');
    end;
  end;
end;

procedure TMainForm.cbWorkLangChange(Sender: TObject);
begin
  { LangTable.CurrentGroup:= cbWorkLang.Text;
    OnChangeLanguage(nil);
    OnChangeLanguage_(self); }
end;

procedure TMainForm.miFileDownload2Click(Sender: TObject);
var
  FlashCardForm2: TFlashCardForm2;

begin
  FlashCardForm2 := TFlashCardForm2.Create(nil);
  FlashCardForm2.Showmodal;
  FlashCardForm2.Free;
end;

end.






miSameCoordFiles
