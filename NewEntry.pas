﻿{$I DEF.INC}
unit NewEntry; {Language 2}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, Grids, Mask, Avk11ViewUnit, MyTypes,
  LanguageUnit, AviconTypes;

type
  TNewEntryForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    InFilePanel: TPanel;
    CVSPanel: TPanel;
    CancelButton: TButton;
    OkButton: TButton;
    Label17: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    PosEdit: TEdit;
    NameEdit: TEdit;
    Memo: TMemo;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Edit1_: TEdit;
    GPSStLatEdit: TEdit;
    GPSStLonEdit: TEdit;
    GPSEdLonEdit: TEdit;
    CommentEdit: TEdit;
    ComboBox1: TComboBox;
    SizeEdit: TEdit;
    Size2Edit: TEdit;
    Size3Edit: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Bevel1: TBevel;
    CodeDefectEdit: TComboBox;
    LineLabel: TLabel;
    LineEdit: TEdit;
    PathEdit: TEdit;
    PathLabel: TLabel;
    Label14: TLabel;
    GPSEdLatEdit: TEdit;
    procedure Panel3Resize(Sender: TObject);
    procedure PosEditKeyPress(Sender: TObject; var Key: Char);
    procedure NameEditKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    function Prepare(ViewForm: TAvk11ViewForm; CenterCoord: Integer; Rail: TRail{; Path: string}): Boolean;
    function PrepareNordCo(ViewForm: TAvk11ViewForm; Coord1, Coord2: Integer; Rail: TRail{; Path: string}): Boolean;
    procedure NameEditChange(Sender: TObject);
    procedure MemoChange(Sender: TObject);
    procedure VedChange(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure DateEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure OnChangeLanguage(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
//    CaCrdParams: TCaCrdParams;
//    MRFCrdParams: TMRFCrdParams;
    FBuffer: TBitMap;
    FViewForm: TAvk11ViewForm;
    CenterDisCoordTemp: Integer;
    CenterSysCoordTemp: Integer;
    RailTemp: TRail;
//    InFile: Boolean;

    StCrd: Integer;
    EdCrd: Integer;

  public
  end;

var
  NewEntryForm: TNewEntryForm;

implementation

uses
  MainUnit, AviconDataSource, ConfigUnit;

{$R *.DFM}

procedure TNewEntryForm.Panel3Resize(Sender: TObject);
begin
//  Panel5.Left:= (Panel3.Width - Panel5.Width) div 2;
end;

procedure TNewEntryForm.PosEditKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then NameEdit.SetFocus;
end;

procedure TNewEntryForm.NameEditKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then Memo.SetFocus;
end;

procedure TNewEntryForm.FormCreate(Sender: TObject);
begin
  FBuffer:= TBitMap.Create;
//  ComboBox1.Ctl3D:= False;
  OnChangeLanguage(nil);
//  PageControl1.ActivePageIndex:= 0;
end;

procedure TNewEntryForm.FormShow(Sender: TObject);
begin
//  PageControl1.ActivePage:= TabSheet1;

//  CheckBox1.Checked:= False;
//  CheckBox2.Checked:= False;

//  RadioButton1.Checked:= True;
  //Edit2.Enabled:= True;
//  Edit6.Text:= '';
//  Edit7.Text:= '';
//  Edit8.Text:= '';
//  Edit2.Text:= '0';
//  ComboBox1.ItemIndex:= 0;

end;

procedure TNewEntryForm.OkButtonClick(Sender: TObject);
var
  I, Idx: Integer;
  Device_tmp: string;
  EndDisplayCoord: Integer;
//  CodeDefect: string;
  Size_, Size2_, Size3_: Single;
  Path: Integer;

begin
//  if CheckBox1.Checked then ID_:= 1 else
//  if CheckBox2.Checked then ID_:= 2 else Exit;

  if (Config.ProgramMode <> pmRADIOAVIONICA_MAV) then
  begin

    Idx:= FViewForm.DatSrc.NotebookAdd;
    FViewForm.DatSrc.Notebook[Idx]^.DisCoord:= 0;
    FViewForm.DatSrc.ModifyExData:= True;

    with FViewForm.DatSrc.Notebook[Idx]^ do
    begin
      ID:= 2; // Тип: 1 - Ведомость контроля / 2 - Запись блокнота.
      Rail:= RailTemp;

      DisCoord:= CenterDisCoordTemp;
      SysCoord:= CenterSysCoordTemp;
      DT:= Now;
      BlokNotText:= StringToHeaderBigStr(Memo.Text); // Привязка / Текст блокнота (кодировка win)
      Defect:= StringToHeaderStr(NameEdit.Text);
      Zoom:= FViewForm.Display.Zoom;
      Reduction:= FViewForm.Display.Reduction;
      ViewMode:= Ord(FViewForm.Display.ViewMode);
      DrawRail:= Ord(FViewForm.Display.ViewRail);
      ViewLine:= FViewForm.Display.ViewLine;
      AmplTh:= FViewForm.Display.AmplTh;
      AmplDon:= Ord(FViewForm.Display.AmplDon);

      ShowChGate_:= FViewForm.Display.ShowChGate;
      ShowChSettings_:= FViewForm.Display.ShowChSettings;
      ShowChInfo_:= FViewForm.Display.ShowChInfo;

    end;
    FViewForm.OnActivate(nil);
    FViewForm.DatSrc.SaveNB;
  end
  else
  begin
    Idx:= FViewForm.DatSrc.NotebookAdd;
    FViewForm.DatSrc.Notebook[Idx]^.DisCoord:= 0;
    FViewForm.DatSrc.ModifyExData:= True;

    with FViewForm.DatSrc.Notebook[Idx]^ do
    begin
      ID:= 3; // Тип: 3 - NORDCO
      Rail:= RailTemp;

//      DisCoord:= CenterDisCoordTemp;
//      SysCoord:= CenterSysCoordTemp;

      DisCoord:= (StCrd + EdCrd) div 2;
      SysCoord:= FViewForm.DatSrc.DisToSysCoord(DisCoord);

      DT:= Now;
      BlokNotText:= StringToHeaderBigStr(Memo.Text); // Привязка / Текст блокнота (кодировка win)
      Defect:= StringToHeaderStr(NameEdit.Text);
      Zoom:= FViewForm.Display.Zoom;
      Reduction:= FViewForm.Display.Reduction;
      ViewMode:= Ord(FViewForm.Display.ViewMode);
      DrawRail:= Ord(FViewForm.Display.ViewRail);
      ViewLine:= FViewForm.Display.ViewLine;
      AmplTh:= FViewForm.Display.AmplTh;
      AmplDon:= Ord(FViewForm.Display.AmplDon);
      ShowChGate_:= FViewForm.Display.ShowChGate;
      ShowChSettings_:= FViewForm.Display.ShowChSettings;
      ShowChInfo_:= FViewForm.Display.ShowChInfo;

      Defect             := StringToHeaderStr(CodeDefectEdit.Text); // NORDCO::Type - Код дефекта по UIC 712
      BlokNotText        := StringToHeaderBigStr(CommentEdit.Text); // NORDCO::Comment - Примечание
      DT                 := Trunc(Now); // NORDCO::SurveyDate - Дата обнаружения дефекта; NORDCO::DataEntryDate - Дата составления списка дефектов
      Line               := StringToHeaderStr(LineEdit.Text); // Название или идентификатор контролируемой линии
      Track              := StringToHeaderStr(PathEdit.Text); // Идентификатор пути, в котором обнаружен дефект
      LocationFrom       := CaCrdToVal(csMetric1kmTurkish, FViewForm.DatSrc.DisToCaCrd(StCrd));  // Начало обнаруженного дефекта
      LocationTo         := CaCrdToVal(csMetric1kmTurkish, FViewForm.DatSrc.DisToCaCrd(EdCrd));    // Конец обнаруженного дефекта. В случае точечного дефекта то же самое, как поле  "Location From"
      LatitudeFrom       := FViewForm.DatSrc.GetGPSCoord(StCrd).Lat;  // GPS-широта для поля "Location From"
      LatitudeTo         := FViewForm.DatSrc.GetGPSCoord(EdCrd).Lat;  // GPS-широта для поля "Location To"
      LongitudeFrom      := FViewForm.DatSrc.GetGPSCoord(StCrd).Lon;  // GPS-долгота для поля "Location From"
      LongitudeTo        := FViewForm.DatSrc.GetGPSCoord(EdCrd).Lon;  // GPS-долгота для поля "Location To"
                        // Тип верхнего строения пути - из списка: Left Rail, Right Rail
      if FViewForm.DatSrc.Header.WorkRailTypeA = 0 then AssetType:= StringToHeaderStr('Left Rail')
                                                   else AssetType:= StringToHeaderStr('Right Rail'); // Тип верхнего строения пути - из списка: Left Rail, Right Rail
      Source             := StringToHeaderStr('Ultrasonic Trolley'); // Метод обнаружения дефекта - из списка: Ultrasonic Trolley, Visual Inspection
      Status             := StringToHeaderStr('Open'); // Это поле указывает, закрыт ли дефект (т.е. удален из пути) - из списка: Open, Closed
      IsFailure          := Boolean(ComboBox1.ItemIndex); // Если конкретный дефект вызвало отказ, то это поле должно иметь значение TRUE. Отказом можно считать любой дефект, который вызывает ограничение скорости

      if (SizeEdit.Text <> '') and (not TryStrToFloat(SizeEdit.Text, Size_)) then begin SizeEdit.SetFocus; Exit; end;
      if (Size2Edit.Text <> '') and (not TryStrToFloat(Size2Edit.Text, Size2_)) then begin Size2Edit.SetFocus; Exit; end;
      if (Size3Edit.Text <> '') and (not TryStrToFloat(Size3Edit.Text, Size3_)) then begin Size3Edit.SetFocus; Exit; end;

      Size               := Size_; // Размер дефекта для  неточечного дефекта. Например, если дефект это трещина, то ее длина.
      Size2              := Size2_; // Размер дефекта для  неточечного дефекта (если более чем одномерный)
      Size3              := Size3_; // Размер дефекта для  неточечного дефекта (если более чем одномерный)
      UnitofMeasurement  := StringToHeaderStr('mm'); // Ед. измерения поля "Size"
      UnitofMeasurement2 := StringToHeaderStr('mm'); // Ед. измерения поля "Size2"
      UnitofMeasurement3 := StringToHeaderStr('mm'); // Ед. измерения поля "Size3"
      Operator_          := FViewForm.DatSrc.Header.OperatorName; // Оператор// Оператор
      Device_tmp:= 'USK-004R';
      for I := 0 to FViewForm.DatSrc.Header.UnitsCount - 1 do
        if FViewForm.DatSrc.Header.UnitsInfo[I].UnitType = dutUpUnit then
        begin
          Device_tmp:= Device_tmp + ' s/n ' + HeaderStrToString(FViewForm.DatSrc.Header.UnitsInfo[I].WorksNumber);
          Break;
        end;
      Device             := StringToHeaderStr(Device_tmp); // Прибор
    end;

(*
  Source:= ;
  Status:= ; //Это поле указывает, закрыт ли дефект (т.е. удален из пути) - из списка: Open, Closed
  item.IsFailure:= IsFailure;
  item.DataEntryDate:= Trunc(Now);  // Дата составления списка дефектов
  if UseSize then
  begin
    item.Size:= Size;              // Размер дефекта для  неточечного дефекта. Например, если дефект это трещина, то ее длина.
    item.UnitofMeasurement:= 'mm'; //Ед. измерения поля
  end;
  if UseSize2 then
  begin
    item.Size2:= Size2;             // Размер дефекта для  неточечного дефекта (если более чем одномерный)
    item.UnitofMeasurement2:= 'mm'; // Ед. измерения поля
  end;
  if UseSize3 then
  begin
    item.Size3:= Size3;             // Размер дефекта для  неточечного дефекта (если более чем одномерный)
    item.UnitofMeasurement3:= 'mm'; // Ед. измерения поля
  end;



  SetLength(FList, Length(FList) + 1);
  FList[Length(FList) - 1]:= item;











  //    if not TryStrToInt(Edit1.Text, CodeDefect) then begin Edit1.SetFocus; Exit; end;
      if (SizeEdit.Text <> '') and (not TryStrToFloat(SizeEdit.Text, Size)) then begin SizeEdit.SetFocus; Exit; end;
      if (Size2Edit.Text <> '') and (not TryStrToFloat(Size2Edit.Text, Size2)) then begin Size2Edit.SetFocus; Exit; end;
      if (Size3Edit.Text <> '') and (not TryStrToFloat(Size3Edit.Text, Size3)) then begin Size3Edit.SetFocus; Exit; end;
      if (PathEdit.Text <> '') and (not TryStrToInt(PathEdit.Text, Path)) then begin PathEdit.SetFocus; Exit; end;

      FViewForm.NordcoCSVFile.AddLine(FViewForm.DatSrc,
                                      LineEdit.Text,
                                      Path,
                                      StCrd, EdCrd,
                                      CodeDefectEdit.Text,
                                      Boolean(ComboBox1.ItemIndex),
                                      Size,
                                      Size2,
                                      Size3,
                                      SizeEdit.Text <> '',
                                      Size2Edit.Text <> '',
                                      Size3Edit.Text <> '',
                                      );
*)
    FViewForm.OnActivate(nil);
    FViewForm.DatSrc.SaveNB;
  end;
  ModalResult:= mrOk;
end;

function TNewEntryForm.Prepare(ViewForm: TAvk11ViewForm; CenterCoord: Integer; Rail: TRail{; Path: string}): Boolean;
var
  S: string;
  I: Integer;
//  Temp: TRealCoord;

  Params: TMiasParams;
  Time: string;
  RC: TRealCoord;

begin

  //MaxFileNameLength

  CVSPanel.Visible:= False;
  InFilePanel.Visible:= True;

//    InFile:= True;
  RailTemp:= Rail;
  ViewForm.DatSrc.GetParamFirst(CenterCoord, Params{, , Time});
//  ViewForm.DatSrc.DisToCoordParams(CenterCoord, CoordParams);

//  Edit9.Text:= Format(Language.GetCaption(0020027) + ' 0%d/ ' + Language.GetCaption(0020028) + ' 0%d', [{ViewForm.DatSrc.Header.UpDeviceID, ViewForm.DatSrc.Header.DwnDeviceID}]);

//  Edit4.Text:= RealCoordToStr(CoordParamsToRealCoord(CoordParams, ViewForm.DatSrc.Header.MoveDir), 1);

  RC:= CrdParamsToRealCrd(ViewForm.Display.MousePosDat.CrdParams, ViewForm.Display.DatSrc.Header.MoveDir);
  PosEdit.Text:= RealCrdToStr(RC);

//  PosEdit.Text:= RealCoordToStr(CoordParamsToRealCoord(CoordParams, ViewForm.DatSrc.Header.MoveDir, TCoordSys(ViewForm.DatSrc.Header.PathCoordSystem)), 1);

//  Edit1.Text:= Header GetText CodeToText(ViewForm.DatSrc.Header.PathSectionName SecName, ViewForm.DatSrc.Header.CharSet);

  if not ViewForm.Display.SkipBackMotion then
  begin
    CenterDisCoordTemp:= CenterCoord;
    CenterSysCoordTemp:= ViewForm.DatSrc.DisToSysCoord(CenterCoord);
  end
  else
  begin
    CenterDisCoordTemp:= - MaxInt;
    CenterSysCoordTemp:= CenterCoord;
  end;

//  Edit10.DateTime:= Now;
//  Edit5.Text:= IntToStr(ViewForm.DatSrc.Header.Path);

//  ComboBox3.ItemIndex:= 4;
//  ComboBox4.ItemIndex:= 0;

//  if RailTemp = rLeft then ComboBox2.ItemIndex:= 0
//                      else ComboBox2.ItemIndex:= 1;

  Memo.Clear;
  NameEdit.Text:= '';
  FViewForm:= ViewForm;
end;

function GPSSingleToText( A: Single; Lat: Boolean; Mode: Integer = 1 ): string; //  Широта = Latitude
var
  D, M, S: LongInt;
  SW: string;

begin
  try
    if Lat then //  Широта = Latitude
    begin

      if (A >= -90) and (A <= 90) then
      begin
        if A >= 0 then SW:= 'N' else SW:= 'S';
        if Mode = 1 then
        begin
          D:= Trunc( A );
          M:= Trunc( ( A - D ) * 60 );
          S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
          Result:= Format( '%s:%d°%d.%d', [SW, D, M, S ] );
        end
        else Result:= Format( '%s%2.10f', [SW, A] );
      end else Result:= '';
    end
    else
    begin
      if (A >= -180) and (A <= 180) then
      begin
        if A >= 0 then SW:= 'E' else SW:= 'W';
        if Mode = 1 then
        begin
          D:= Trunc( A );
          M:= Trunc( ( A - D ) * 60 );
          S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
          Result:= Format( '%s:%d°%d.%d', [SW, D, M, S ] );
        end
        else Result:= Format( '%s%2.10f', [SW, A] );
      end else Result:= '';
    end;

  except
    Result:= '';
  end;
end;


function TNewEntryForm.PrepareNordCo(ViewForm: TAvk11ViewForm; Coord1, Coord2: Integer; Rail: TRail{; Path: string}): Boolean;
var
  S: string;
  I: Integer;
  Params: TMiasParams;
  Time: string;
  RC: TRealCoord;

begin
  CVSPanel.Visible:= True;
  InFilePanel.Visible:= False;

//  InFile:= False;
  RailTemp:= Rail;
  StCrd:= Coord1;
  EdCrd:= Coord2;

  GPSStLatEdit.Text:= GPSSingleToText(ViewForm.DatSrc.GetGPSCoord(Coord1).Lat, True);
  GPSStLonEdit.Text:= GPSSingleToText(ViewForm.DatSrc.GetGPSCoord(Coord1).Lon, False);
  GPSEdLatEdit.Text:= GPSSingleToText(ViewForm.DatSrc.GetGPSCoord(Coord2).Lat, True);
  GPSEdLonEdit.Text:= GPSSingleToText(ViewForm.DatSrc.GetGPSCoord(Coord2).Lon, False);
  LineEdit.Text:= HeaderStrToString(ViewForm.DatSrc.Header.PathSectionName);
  PathEdit.Text:= ViewForm.DatSrc.GetRailPathNumber_in_String;

  SizeEdit.Text:= IntToStr((Coord2 - Coord1) * ViewForm.DatSrc.Header.ScanStep div 100);
  FViewForm:= ViewForm;
end;

procedure TNewEntryForm.NameEditChange(Sender: TObject);
begin
//  CheckBox2.Checked:= True;
end;

procedure TNewEntryForm.MemoChange(Sender: TObject);
begin
//  CheckBox2.Checked:= True;
end;

procedure TNewEntryForm.VedChange(Sender: TObject);
var
  Code, I, J: Integer;

begin
//  CheckBox1.Checked:= True;

//  J:= Pos('.', Edit7.Text);
{  if J <> 0 then
  begin
    J:= J + 1;
    I:= J;
    while (J <= Length(Edit7.Text)) and (Edit7.Text[J] in ['0'..'9']) do Inc(J);
    if J <> I then
    begin
      Val(Copy(Edit7.Text, I, J - I), I, Code);
      if (Code = 0) and ((I = 1) or (I = 2) or (I = 3)) then ComboBox1.ItemIndex:= I - 1;
    end;
  end; }
end;

procedure TNewEntryForm.RadioButton2Click(Sender: TObject);
begin
//  Edit2.Enabled:= not RadioButton2.Checked;
end;

procedure TNewEntryForm.RadioButton1Click(Sender: TObject);
begin
//  Edit2.Enabled:= RadioButton1.Checked;
end;

procedure TNewEntryForm.DateEdit1KeyPress(Sender: TObject; var Key: Char);
var
  I: Integer;

begin
  if Key = #13 then
    for I:= 0 to Self.ComponentCount - 1 do
      if Self.Components[I] is TWinControl then
        if ((Self.Components[I] as TWinControl).Tag = (Sender as TWinControl).Tag + 1) and (Self.Components[I] as TWinControl).Enabled then
          (Self.Components[I] as TWinControl).SetFocus;
end;

procedure TNewEntryForm.OnChangeLanguage(Sender: TObject);
begin
  Self.Caption                := LangTable.Caption['NotebookRec:NewRecord'];
  Self.Font.Name              := LangTable.Caption['General:FontName'];
//  TabSheet1.Caption           := LangTable.Caption['NotebookRec:Notebook'];
//  TabSheet2.Caption           := LangTable.Caption['NotebookRec:InspectionReport'];
  Label4.Caption              := LangTable.Caption['NotebookRec:Coordinate'];
  Label6.Caption              := LangTable.Caption['NotebookRec:Defectcode'];
//  CheckBox2.Caption           := LangTable.Caption['NotebookRec:AddtoNotebook'];
//  CheckBox1.Caption           := LangTable.Caption['NotebookRec:AddInspectionReport'];
  CancelButton.Caption        := LangTable.Caption['Common:Cancel'];
//  Label5.Caption              := LangTable.Caption['NotebookRec:Defectoscope................................'];
//  Label2.Caption              := LangTable.Caption['NotebookRec:Coordinate...................................'];
//  Label1.Caption              := LangTable.Caption['NotebookRec:Decodingdate..............'];
//  Label12.Caption             := LangTable.Caption['NotebookRec:Tracksection.....'];
//  Label3.Caption              := LangTable.Caption['NotebookRec:Tracknumber..................'];
//  Label14.Caption             := LangTable.Caption['NotebookRec:Rail.....................'];
//  RadioButton1.Caption        := LangTable.Caption['NotebookRec:Section.................'];
//  RadioButton2.Caption        := LangTable.Caption['NotebookRec:Section'];
//  Label7.Caption              := LangTable.Caption['NotebookRec:Binding........................'];
//  Label8.Caption              := LangTable.Caption['NotebookRec:Inspectas..................'];
//  Label9.Caption              := LangTable.Caption['NotebookRec:RailScheme......................'];
//  Label10.Caption             := LangTable.Caption['NotebookRec:Note..................................'];
//  Label11.Caption             := LangTable.Caption['NotebookRec:Decoder..........................'];
  Label17.Caption             := LangTable.Caption['NotebookRec:Content'];

//  Label13.Caption             := LangTable.Caption['NotebookRec:Found......................'];
//  Label15.Caption             := LangTable.Caption['NotebookRec:Wayinspection.......'];
   {
  ComboBox3.Items.Clear;
  ComboBox3.Items.Add(LangTable.Caption['NotebookRec:immediate']);
  ComboBox3.Items.Add(LangTable.Caption['NotebookRec:urgent']);
  ComboBox3.Items.Add(LangTable.Caption['NotebookRec:duringtheday']);
  ComboBox3.Items.Add(LangTable.Caption['NotebookRec:3-5days']);
  ComboBox3.Items.Add(LangTable.Caption['NotebookRec:planned']);

  ComboBox4.Items.Clear;
  ComboBox4.Items.Add(LangTable.Caption['NotebookRec:duringanalysis']);
  ComboBox4.Items.Add(LangTable.Caption['NotebookRec:ontheway']);

  ComboBox2.Clear;
  ComboBox2.Items.Add(LangTable.Caption['NotebookRec:Left']);
  ComboBox2.Items.Add(LangTable.Caption['NotebookRec:Right']);

  ComboBox1.Items.Add(LangTable.Caption['NotebookRec:Weldedjoint']);
  }

  Label1.Caption    := LangTable.Caption['Config:Defect']; // Дефект
  Label2.Caption    := LangTable.Caption['NotebookRec:Start'];     // Начало
  Label3.Caption    := LangTable.Caption['NotebookRec:End'];       // Конец
  Label11.Caption   := LangTable.Caption['NotebookRec:Lat'];       // Широта
  Label14.Caption   := LangTable.Caption['NotebookRec:Lat'];       // Широта
  Label12.Caption   := LangTable.Caption['NotebookRec:Lon'];       // Долгота
  Label13.Caption   := LangTable.Caption['NotebookRec:Lon'];       // Долгота
  Label7.Caption    := LangTable.Caption['NotebookRec:IsFailure'];// Ограничение скорости
  Label8.Caption    := LangTable.Caption['NotebookRec:Size1'];      // Размер №1 (мм)
  Label9.Caption    := LangTable.Caption['NotebookRec:Size2'];      // Размер №2 (мм)
  Label10.Caption   := LangTable.Caption['NotebookRec:Size3'];      // Размер №3 (мм)
  Label5.Caption    := LangTable.Caption['NotebookRec:Comment'];   // Комментарий

  LineLabel.Caption    := LangTable.Caption['NotebookRec:Line'];
  PathLabel.Caption    := LangTable.Caption['NotebookRec:Track'];

  ComboBox1.Clear;
  ComboBox1.Items.Add(LangTable.Caption['Common:No_']);
  ComboBox1.Items.Add(LangTable.Caption['Common:Yes_']);
  ComboBox1.ItemIndex:= 1;
end;

procedure TNewEntryForm.CheckBox2Click(Sender: TObject);
begin
//  if CheckBox2.Checked then PageControl1.ActivePageIndex:= 0;
end;

procedure TNewEntryForm.CheckBox1Click(Sender: TObject);
begin
//  if CheckBox1.Checked then PageControl1.ActivePageIndex:= 1;
end;

procedure TNewEntryForm.PageControl1Change(Sender: TObject);
begin                               {
  case PageControl1.ActivePageIndex of
    0: CheckBox2.Checked:= True;
    1: CheckBox1.Checked:= True;
  end;                               }
end;

end.

