﻿{$I DEF.INC}
unit InfoBarUnit; {Language 12}

interface

uses
  Windows, Types, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ExtCtrls, StdCtrls, TeeProcs, TeEngine, Chart, Grids, Math,
  Buttons, ValEdit, ComCtrls, MyTypes, ConfigUnit, LanguageUnit,
  Series, TeeShape, DisplayUnit, AviconDataSource, AviconTypes;

type
  TInfoBarForm = class(TForm)
    FileInfoPanel: TPanel;
    ImageList: TImageList;
    TimePanel: TPanel;
    Label43: TLabel;
    Label26: TLabel;
    SimplCoordPanel: TPanel;
    Bevel12: TBevel;
    Label2: TLabel;
    Label3: TLabel;
    Bevel3: TBevel;
    Panel12: TPanel;
    LabelDL: TLabel;
    LabelH1: TLabel;
    LabelDH: TLabel;
    LabelH2: TLabel;
    Bevel1: TBevel;
    Image7: TImage;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    LabelH1MM: TLabel;
    LabelDHMM: TLabel;
    LabelH2MM: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Bevel7: TBevel;
    Bevel8: TBevel;
    Bevel10: TBevel;
    Bevel13: TBevel;
    Bevel14: TBevel;
    Bevel15: TBevel;
    Panel1: TPanel;
    Bevel6: TBevel;
    Bevel5: TBevel;
    LDhV: TLabel;
    LDhN: TLabel;
    Bevel4: TBevel;
    LDyN: TLabel;
    LDyV: TLabel;
    RailPanel: TPanel;
    Bevel11: TBevel;
    Label40: TLabel;
    Label40V: TLabel;
    Bevel2: TBevel;
    Panel2: TPanel;
    Label134: TLabel;
    Panel3: TPanel;
    Panel5: TPanel;
    Label4: TLabel;
    Panel6: TPanel;
    Label6: TLabel;
    Label17: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label1_: TPanel;
    Label1: TLabel;
    Panel4: TPanel;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure Panel2Click(Sender: TObject);
    procedure AddButtonClick(Sender: TObject);
    procedure DelButtonClick(Sender: TObject);
    procedure OnItemDblClick(Sender: TObject);
    procedure ChangeMouseMode(Sender: TObject);

  private
    AScan1: array of TChartShape;
    AScan2: array of TChartShape;
    HSAScan: array of TChartShape;
    BlackItem1: array of TPoint;
    BlackItem2: array of TPoint;
    FOnInfoClick: TNotifyEvent;
    FOnNewRecordClick: TNotifyEvent;
    FCoordMode: Boolean;

    FvFileInfo: Boolean;
    FvNotebook: Boolean;
    FvRail: Boolean;
    FvCoord1: Boolean;
    FvCoord2: Boolean;
    FvTime: Boolean;

    FBuffer: TBitMap;
    FDatSrc: TAvk11DatSrc;
    FDisplay: TAvk11Display;
    FSavePageIdx: Integer;

    function GetDblRecordClick: TNotifyEvent;
    procedure SetDblRecordClick(New: TNotifyEvent);

    procedure SetViewFileInfo(NewState: Boolean);
    procedure SetViewNotebook(NewState: Boolean);
    procedure SetViewRail(NewState: Boolean);
    procedure SetViewCoord1(NewState: Boolean);
    procedure SetViewCoord2(NewState: Boolean);
    procedure SetViewTime(NewState: Boolean);

    procedure CreateCoordImage(Buffer: TBitMap);

    procedure SetAScanDelayZone(Min1, Max1, Min2, Max2: Integer);
    procedure ClearAScan1;
    procedure AddAScan1(Delay: Extended; Ampl: Integer; Color: TColor);
    procedure ClearAScan2;
    procedure AddAScan2(Delay: Extended; Ampl: Integer; Color: TColor);
    procedure EndAScan;
    procedure CalcHeight;

  public
    DebugInfo: Boolean;
//    MyList: TMyListView;
    AmplThTrackBarPosition: Integer;

    procedure SetHSAScanDelayZone(Min, Max: Integer);
    procedure ClearHSAScan;
    procedure AddHSAScan(Delay: Extended; Ampl: Integer; Color: TColor);
    procedure EndHSAScan;

    procedure RefreshDat(X, Y: Integer);
    procedure SetControl(DatSrc: TAvk11DatSrc; Display: TAvk11Display);
    procedure CreateNBList;
    procedure OnChangeLanguage(Sender: TObject);

    property vFileInfo: Boolean read FvFileInfo write SetViewFileInfo;
    property vNotebook: Boolean read FvNotebook write SetViewNotebook;
    property vRail: Boolean read FvRail write SetViewRail;
    property vCoord1: Boolean read FvCoord1 write SetViewCoord1;
    property vCoord2: Boolean read FvCoord2 write SetViewCoord2;
    property vTime: Boolean read FvTime write SetViewTime;

    property CoordMode: Boolean read FCoordMode write FCoordMode;

    property OnInfoClick: TNotifyEvent read FOnInfoClick write FOnInfoClick;
    property OnNewRecordClick: TNotifyEvent read FOnNewRecordClick write FOnNewRecordClick;
  end;

implementation

uses MainUnit, DebugFormUnit;

{$R *.dfm}

procedure KillSelection(StringGrid: TStringGrid);
var
  GridRect: TGridRect;

begin
  GridRect:= StringGrid.Selection;
  GridRect.Top:= 100;
  GridRect.Bottom:= 100;
  GridRect.Left:= 100;
  GridRect.Right:= 100;
  StringGrid.Selection:= GridRect;
end;

procedure TInfoBarForm.FormCreate(Sender: TObject);
begin
  Panel12.Top:= 0;
  {$IFDEF AVIKON12}
  RailPanel.Visible:= False;
  {$ENDIF}

  {$IFDEF FILUSX17}
  RailPanel.Visible:= False;
  {$ENDIF}

  {$IFDEF AVIKON16}
  TabSheet1.TabVisible:= False; // Таблица эхо-сигналов
  {$ENDIF}

  Panel12.SendToBack;
  Panel12.Refresh;
  FileInfoPanel.Refresh;

//  HSParam.RowCount:= 6;
{
  Sensitivity1.ColCount:= 5;
  Sensitivity1.ColWidths[0]:= 52;
  Sensitivity1.ColWidths[1]:= 22;
  Sensitivity1.ColWidths[2]:= 22;
  Sensitivity1.ColWidths[3]:= 22;
  Sensitivity1.ColWidths[4]:= 22;

  Sensitivity1.RowCount:= 6; }
//  StringGrid1.RowHeights[0]:= 0;
//  StringGrid1.ColWidths[0]:= 87;
//  StringGrid1.ColWidths[1]:= 57;

//  StringGrid2.ColCount:= 3;
//  StringGrid2.ColWidths[0]:= 87;
//  StringGrid2.ColWidths[1]:= 28;
//  StringGrid2.ColWidths[2]:= 28;
//  KillSelection(StringGrid2);
             {
  Sensitivity2.ColCount:= 3;
  Sensitivity2.ColWidths[0]:= 52;
  Sensitivity2.ColWidths[1]:= 45;
  Sensitivity2.ColWidths[2]:= 45;

  Sensitivity2.RowCount:= 6;

  KillSelection(StringGrid1);
  KillSelection(Sensitivity1);
  KillSelection(Sensitivity2);
 }
//  KillSelection(HSParam);
//  KillSelection(HSEcho);

//  KillSelection(RightEcho);
//  KillSelection(LeftEcho);

  FOnInfoClick:= nil;
  FOnNewRecordClick:= nil;
  FBuffer:= nil;

  FDatSrc:= nil;
  FDisplay:= nil;

  FCoordMode:= False;

  ClearAScan1;
  ClearAScan2;

//  HandScanPanel.Align:= alClient;

//  KillSelection(sgHeadPh);

  OnChangeLanguage(nil);
end;

procedure TInfoBarForm.ClearAScan1;
var
  I: Integer;

begin
  for I:= 0 to High(AScan1) do
    if Assigned(AScan1[I]) then
    begin
      AScan1[I].Free;
      AScan1[I]:= nil;
    end;
{
  SetLength(AScan1, 2);
  AScan1[0]:= TChartShape.Create(Chart1);
  AScan1[0].ParentChart:= Chart1;
  AScan1[0].Style:= chasTriangle;
  AScan1[0].Brush.Color:= clBlack;
  AScan1[0].X0:= 0;
  AScan1[0].X1:= 0;
  AScan1[0].Y0:= 0;
  AScan1[0].Y1:= 0;

  AScan1[1]:= TChartShape.Create(Chart1);
  AScan1[1].ParentChart:= Chart1;
  AScan1[1].Style:= chasHorizLine;
  AScan1[1].Pen.Color:= clBlack;
  AScan1[1].X0:= 0;
  AScan1[1].X1:= 1000;
  AScan1[1].Y0:= 0;
  AScan1[1].Y1:= 0;

  SetLength(BlackItem1, 0);}
end;

procedure TInfoBarForm.ClearHSAScan;
var
  I: Integer;

begin
  for I:= 0 to High(HSAScan) do
    if Assigned(HSAScan[I]) then
    begin
      HSAScan[I].Free;
      HSAScan[I]:= nil;
    end;

  SetLength(HSAScan, 2);
{  HSAScan[0]:= TChartShape.Create(Chart3);
  HSAScan[0].ParentChart:= Chart3;
  HSAScan[0].Style:= chasTriangle;
  HSAScan[0].Brush.Color:= clBlack;
  HSAScan[0].X0:= 0;
  HSAScan[0].X1:= 0;
  HSAScan[0].Y0:= 0;
  HSAScan[0].Y1:= 0;

  HSAScan[1]:= TChartShape.Create(Chart3);
  HSAScan[1].ParentChart:= Chart3;
  HSAScan[1].Style:= chasHorizLine;
  HSAScan[1].Pen.Color:= clBlack;
  HSAScan[1].X0:= 0;
  HSAScan[1].X1:= 1000;
  HSAScan[1].Y0:= 0;
  HSAScan[1].Y1:= 0;
 }
  SetLength(BlackItem1, 0);
end;

procedure TInfoBarForm.AddAScan1(Delay: Extended; Ampl: Integer; Color: TColor);
begin
{  SetLength(AScan1, Length(AScan1) + 1);
  AScan1[High(AScan1)]:= TChartShape.Create(Chart1);
  AScan1[High(AScan1)].ParentChart:= Chart1;
  AScan1[High(AScan1)].Style:= chasTriangle;
  AScan1[High(AScan1)].Brush.Color:= Color;
  AScan1[High(AScan1)].Pen.Color:= Color;
  AScan1[High(AScan1)].X0:= Round(Delay - Ampl / 10);
  AScan1[High(AScan1)].X1:= Round(Delay + Ampl / 10);
  AScan1[High(AScan1)].Y0:= - 12; }
 // AScan1[High(AScan1)].Y1:= {DBValue[}Ampl{]};
end;

procedure TInfoBarForm.AddHSAScan(Delay: Extended; Ampl: Integer; Color: TColor);
begin
  SetLength(HSAScan, Length(HSAScan) + 1);
{  HSAScan[High(HSAScan)]:= TChartShape.Create(Chart3);
  HSAScan[High(HSAScan)].ParentChart:= Chart3;
  HSAScan[High(HSAScan)].Style:= chasTriangle;
  HSAScan[High(HSAScan)].Brush.Color:= Color;
  HSAScan[High(HSAScan)].Pen.Color:= Color;
  HSAScan[High(HSAScan)].X0:= Round(Delay - Ampl / 10);
  HSAScan[High(HSAScan)].X1:= Round(Delay + Ampl / 10);
  HSAScan[High(HSAScan)].Y0:= - 12;              }
//  HSAScan[High(HSAScan)].Y1:= {DBValue[}Ampl{]};
end;

procedure TInfoBarForm.ClearAScan2;
var
  I: Integer;
begin
{
  for I:= 0 to High(AScan2) do
    if Assigned(AScan2[I]) then
    begin
      AScan2[I].Free;
      AScan2[I]:= nil;
    end;

  SetLength(AScan2, 2);
  AScan2[0]:= TChartShape.Create(Chart2);
  AScan2[0].ParentChart:= Chart2;
  AScan2[0].Style:= chasTriangle;
  AScan2[0].Brush.Color:= clBlack;
  AScan2[0].Pen.Color:= Color;
  AScan2[0].X0:= 0;
  AScan2[0].X1:= 0;
  AScan2[0].Y0:= 0;
  AScan2[0].Y1:= 0;

  AScan2[1]:= TChartShape.Create(Chart2);
  AScan2[1].ParentChart:= Chart2;
  AScan2[1].Style:= chasHorizLine;
  AScan2[1].Pen.Color:= clBlack;
  AScan2[1].X0:= 0;
  AScan2[1].X1:= 1000;
  AScan2[1].Y0:= 0;
  AScan2[1].Y1:= 0;

  SetLength(BlackItem2, 0); }
end;

procedure TInfoBarForm.AddAScan2(Delay: Extended; Ampl: Integer; Color: TColor);
begin                        {
  SetLength(AScan2, Length(AScan2) + 1);
  AScan2[High(AScan2)]:= TChartShape.Create(Chart2);
  AScan2[High(AScan2)].ParentChart:= Chart2;
  AScan2[High(AScan2)].Style:= chasTriangle;
  AScan2[High(AScan2)].Brush.Color:= Color;
  AScan2[High(AScan2)].Pen.Color:= Color;
  AScan2[High(AScan2)].X0:= Delay - 2 * Ampl div 15;
  AScan2[High(AScan2)].X1:= Delay + 2 * Ampl div 15;
  AScan2[High(AScan2)].Y0:= - 12; }
//  AScan2[High(AScan2)].Y1:= DBValue[Ampl];
end;

procedure TInfoBarForm.SetAScanDelayZone(Min1, Max1, Min2, Max2: Integer);
begin
//  Chart1.BottomAxis.SetMinMax(Min1, Max1);
//  Chart2.BottomAxis.SetMinMax(Min2, Max2);
end;

procedure TInfoBarForm.SetHSAScanDelayZone(Min, Max: Integer);
begin
  //Chart3.BottomAxis.SetMinMax(Min, Max);
end;

procedure TInfoBarForm.EndAScan;
var
  I: Integer;

begin
{  SetLength(AScan1, Length(AScan1) + 1);
  AScan1[High(AScan1)]:= TChartShape.Create(Chart1);
  AScan1[High(AScan1)].ParentChart:= Chart1;
  AScan1[High(AScan1)].Style:= chasHorizLine;
  AScan1[High(AScan1)].Brush.Color:= clLime;
  AScan1[High(AScan1)].Pen.Color:= clLime;
  AScan1[High(AScan1)].Pen.Width:= 3;
  AScan1[High(AScan1)].X0:= Chart1.BottomAxis.Minimum;
  AScan1[High(AScan1)].X1:= Chart1.BottomAxis.Maximum;
//  AScan1[High(AScan1)].Y0:= DBValue[AmplThTrackBarPosition];
//  AScan1[High(AScan1)].Y1:= DBValue[AmplThTrackBarPosition];

  SetLength(AScan2, Length(AScan2) + 1);
  AScan2[High(AScan2)]:= TChartShape.Create(Chart2);
  AScan2[High(AScan2)].ParentChart:= Chart2;
  AScan2[High(AScan2)].Style:= chasHorizLine;
  AScan2[High(AScan2)].Brush.Color:= clLime;
  AScan2[High(AScan2)].Pen.Color:= clLime;
  AScan2[High(AScan2)].Pen.Width:= 3;
  AScan2[High(AScan2)].X0:= Chart2.BottomAxis.Minimum;
  AScan2[High(AScan2)].X1:= Chart2.BottomAxis.Maximum;
//  AScan2[High(AScan2)].Y0:= DBValue[AmplThTrackBarPosition];
//  AScan2[High(AScan2)].Y1:= DBValue[AmplThTrackBarPosition]; }
end;

procedure TInfoBarForm.EndHSAScan;
begin
{  SetLength(HSAScan, Length(HSAScan) + 1);
  HSAScan[High(HSAScan)]:= TChartShape.Create(Chart3);
  HSAScan[High(HSAScan)].ParentChart:= Chart3;
  HSAScan[High(HSAScan)].Style:= chasHorizLine;
  HSAScan[High(HSAScan)].Brush.Color:= clLime;
  HSAScan[High(HSAScan)].Pen.Color:= clLime;
  HSAScan[High(HSAScan)].Pen.Width:= 3;
  HSAScan[High(HSAScan)].X0:= Chart3.BottomAxis.Minimum;
  HSAScan[High(HSAScan)].X1:= Chart3.BottomAxis.Maximum; }
//  HSAScan[High(HSAScan)].Y0:= DBValue[AmplThTrackBarPosition];
//  HSAScan[High(HSAScan)].Y1:= DBValue[AmplThTrackBarPosition];
end;

procedure TInfoBarForm.DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  S1, S2: string;

begin
{  with (Sender as TStringGrid), Canvas do
  begin
    Brush.Style:= bsSolid;
    FillRect(Rect);
    Brush.Style:= bsClear;
    if TextWidth(Cells[ACol, ARow]) < (Rect.Right - Rect.Left) then
      TextOut((Rect.Left + Rect.Right - TextWidth(Cells[ACol, ARow])) div 2,
              (Rect.Top + Rect.Bottom - TextHeight(Cells[ACol, ARow])) div 2,
              Cells[ACol, ARow]) else
    begin
      S1:= Copy(Cells[ACol, ARow], 1, Pos(' ', Cells[ACol, ARow]) - 1);
      S2:= Copy(Cells[ACol, ARow], Pos(' ', Cells[ACol, ARow]) + 1, MaxInt);
      TextOut((Rect.Left + Rect.Right - TextWidth(S1)) div 2,
              Rect.Top + (Rect.Bottom - Rect.Top) div 4 - TextHeight(S1) div 2 - 1, S1);
      TextOut((Rect.Left + Rect.Right - TextWidth(S2)) div 2,
              Rect.Top + 3*(Rect.Bottom - Rect.Top) div 4 - TextHeight(S2) div 2 - 3, S2);
    end;
  end;    }
end;

procedure TInfoBarForm.Panel2Click(Sender: TObject);
begin
  if Assigned(FOnInfoClick) then FOnInfoClick(Self);
end;

procedure TInfoBarForm.AddButtonClick(Sender: TObject);
begin
  if Assigned(FOnNewRecordClick) then FOnNewRecordClick(Self);
end;

procedure TInfoBarForm.DelButtonClick(Sender: TObject);
begin
  {
  if (not Assigned(FDatSrc)) or (not Assigned(FDisplay)) then Exit;

  if Application.MessageBox(PChar(LangTable.Caption['Notebook:SureDel']), PChar(LangTable.Caption['Common:Attention']), MB_OKCANCEL + MB_DEFBUTTON1) <> IDOK then Exit;

  if MyList.ItemIndex <> - 1 then
  if Integer(MyList.Items.Objects[MyList.ItemIndex]) <> - 1 then
  begin
    FDatSrc.NotebookDelete(Integer(MyList.Items.Objects[MyList.ItemIndex]));
    CreateNBList;
    FDisplay.Refresh;
  end;
  }
end;

function TInfoBarForm.GetDblRecordClick: TNotifyEvent;
begin
//  Result:= MyList.OnItemDblClick;
end;

procedure TInfoBarForm.SetDblRecordClick(New: TNotifyEvent);
begin
//  MyList.OnItemDblClick:= New;
end;

procedure TInfoBarForm.OnItemDblClick(Sender: TObject);
begin
{ if (not Assigned(FDatSrc)) or (not Assigned(FDisplay)) then Exit;

  if (MyList.ItemIndex <> - 1) then
  begin
    FDisplay.CenterDisCoord:= FDatSrc.Nobebook[Integer(MyList.Items.Objects[MyList.ItemIndex])].DisCoord;
    FDisplay.Refresh;
  end; }
end;

procedure TInfoBarForm.SetViewFileInfo(NewState: Boolean);
begin
//  FvFileInfo:= NewState;
//  FileInfo.Visible:= NewState;
//  TabSheet6.Show;
//  TabSheet6.Visible:= NewState;
//  TabSheet6.TabVisible:= NewState;
end;

procedure TInfoBarForm.SetViewNotebook(NewState: Boolean);
begin
//  FvNotebook:= NewState;
//  TabSheet7.Show;
//  TabSheet7.Visible:= NewState;
//  TabSheet7.TabVisible:= NewState;
end;

procedure TInfoBarForm.SetViewRail(NewState: Boolean);
begin
  {$IFDEF AVIKON12}
//  Exit;
  {$ENDIF}

  FvRail:= NewState;
  RailPanel.Visible:= NewState;
  CalcHeight;
end;

procedure TInfoBarForm.SetViewCoord1(NewState: Boolean);
begin
  FvCoord1:= NewState;
//  CoordPanel.Visible:= NewState;
  CalcHeight;
end;

procedure TInfoBarForm.SetViewCoord2(NewState: Boolean);
begin
  FvCoord2:= NewState;
  SimplCoordPanel.Visible:= NewState;
  CalcHeight;
end;

procedure TInfoBarForm.SetViewTime(NewState: Boolean);
begin
  FvTime:= NewState;
  TimePanel.Visible:= NewState;
  CalcHeight;
end;

procedure TInfoBarForm.CalcHeight;
var
  NewHeight: Integer;

begin
//  NewHeight:= ChPanel.Height + MousePanel.Height;

//  if RailPanel.Visible then Inc(NewHeight, RailPanel.Height);
//  if CoordPanel.Visible then Inc(NewHeight, CoordPanel.Height);
//  if SimplCoordPanel.Visible then Inc(NewHeight, SimplCoordPanel.Height);
//  if TimePanel.Visible then Inc(NewHeight, TimePanel.Height);

//  Panel4.Height:= NewHeight + 2;
end;

procedure TInfoBarForm.CreateCoordImage(Buffer: TBitMap);
begin
end;

procedure TInfoBarForm.SetControl(DatSrc: TAvk11DatSrc; Display: TAvk11Display);
var
  I: Integer;

begin
  FDatSrc:= DatSrc;
  FDisplay:= Display;

  if (not Assigned(DatSrc)) or (not Assigned(Display)) then Exit;

  FDisplay.OnChangeMouseMode:= ChangeMouseMode;
{
  FileInfo.Strings.Clear;
  FileInfo.InsertRow('Подразделение',    CodeToText(FDatSrc.Header.Name, FDatSrc.Header.CharSet), True);
  FileInfo.InsertRow('Номер прибора',    Format('0%d/0%d', [FDatSrc.Header.UpDeviceID, FDatSrc.Header.DwnDeviceID]), True);
  FileInfo.InsertRow('Дата',             CodeToDate(FDatSrc.Header.Day, FDatSrc.Header.Month, FDatSrc.Header.Year), True);
  FileInfo.InsertRow('Время',            CodeToTime(FDatSrc.Header.Hour, FDatSrc.Header.Minute), True);
  FileInfo.InsertRow('ФИО оператора',    CodeToText(FDatSrc.Header.Operator, FDatSrc.Header.CharSet), True);
  FileInfo.InsertRow('Код направления',  IntToStr(FDatSrc.Header.DirCode), True);
  FileInfo.InsertRow('Название участка', CodeToText(FDatSrc.Header.SecName, FDatSrc.Header.CharSet), True);
  FileInfo.InsertRow('Путь',             IntToStr(FDatSrc.Header.Path), True);
  if FDatSrc.Header.MoveDir = 0 then FileInfo.InsertRow('Направление движения', 'В сторону уменьшения путейской координаты', True)
                                else FileInfo.InsertRow('Направление движения', 'В сторону увеличения путейской координаты', True);

  for I:= 0 to FDatSrc.RSPInfoCount - 1 do
    FileInfo.InsertRow(RSPParName[FDatSrc.RSPInfoByIdx[I].ID], FDatSrc.RSPInfoByIdx[I].Text, True);
 }
  CreateNBList;
end;

procedure TInfoBarForm.CreateNBList;
var
  I: Integer;

begin
{
  if (not Assigned(FDatSrc)) or (not Assigned(FDisplay)) then Exit;

  MyList.Clear;
  MyList.BeginUpdate;

  for I:= 0 to FDatSrc.NobebookCount - 1 do
    if FCoordMode then MyList.Items.AddObject(Format('%s=%s', [LineCoordToStr(FDatSrc.DisToSysCoord(FDatSrc.Nobebook[I].DisCoord) * FDatSrc.Header.ScanStep / 100 / 1000  + FDatSrc.Header.StartKM * 1000 + FDatSrc.Header.StartPk * 100 + FDatSrc.Header.StartMetre, 0), FDatSrc.Nobebook[I].Defect]), Pointer(I))
                  else MyList.Items.AddObject(RealCoordToStr(FDatSrc.DisToRealCoord(FDatSrc.Nobebook[I].DisCoord), 0) + '=' + FDatSrc.Nobebook[I].Defect, Pointer(I));

  MyList.EndUpdate;
  }
end;

procedure TInfoBarForm.RefreshDat(X, Y: Integer);
var
  R, RRR: TRail;
  Ch: Integer;
  I, J, I1, I2: Integer;
  S: string;
  DisCoord1, DisCoord2, Ch1Delay1, Ch1Delay2, Ch2Delay1, Ch2Delay2, Ch1, Ch2: Integer;
  SameRect: Boolean;
  Flg: Boolean;
  FHName1: string;
  FHName2: string;
  ResDisCoord: Int64;
  ResSysCoord, ResOffset: Integer;
  Col, Row, ChNum: Integer;
  RC: TRealCoord;
  VVV: Integer;
  ResDtD: TIntegerDynArray;
  Index: array of Integer;
  SaveEventID: Integer;
  SkipCount: Integer;
  SaveEventIdx: Integer;
  Flag: Boolean;

begin
  if (not Assigned(FDatSrc)) or (not Assigned(FDisplay)) then Exit;

//  Label40.Caption:= format('%d:%d', [X, Y]);

//  Label74.Caption:= Format('Mode: %d; Sensor Left: %d; Sensor Right: %d', [FDisplay.MousePosDat.Params.Mode, Ord(FDisplay.MousePosDat.Params.Sensor1[rLeft]), Ord(FDisplay.MousePosDat.Params.Sensor1[rRight])]);

  case FDisplay.MouseMode of

          mmIdle,
  mmSelRailPoint: begin

                    if not MainForm.tbAllEventsOnLine.Checked then // При наличии событий с одинаковыми идентификаторами они выводятся не все а только первое и последнее
                    begin

                      SetLength(Index, 0);
                      SkipCount:= 0;
                      SaveEventIdx:= 0;
                      for I:= 0 to High(FDisplay.InfoRect) do
                        if PtInRect(FDisplay.InfoRect[I].Rt, Point(X, Y)) then
                        begin
                          if (Length(Index) = 0) or (SaveEventID <> FDisplay.InfoRect[I].EventID) or not FDisplay.InfoRect[I].CanGroup then
                          begin
                            if SkipCount <> 0 then
                            begin
                              SetLength(Index, Length(Index) + 1);
                              Index[High(Index)]:= SaveEventIdx;
                              SkipCount:= 0;
                            end;
                            SetLength(Index, Length(Index) + 1);
                            Index[High(Index)]:= I;
                            SaveEventID:= FDisplay.InfoRect[I].EventID;
                          end
                          else
                          if SaveEventID = FDisplay.InfoRect[I].EventID then
                          begin
                            SkipCount:= SkipCount + 1;
                            SaveEventIdx:= I;
                          end;
                        end;

                      if SkipCount <> 0 then
                      begin
                        SetLength(Index, Length(Index) + 1);
                        Index[High(Index)]:= SaveEventIdx;
                      end;

                      if Length(Index) > 1 then
                      repeat
                        Flag:= True;
                        for J:= 0 to High(Index) - 1 do // Сотрировка по номеру события
                          if FDisplay.InfoRect[Index[J]].EventIdx > FDisplay.InfoRect[Index[J + 1]].EventIdx then
                          begin
                            I:= Index[J];
                            Index[J]:= Index[J + 1];
                            Index[J + 1]:= I;
                            Flag:= False;
                          end;
                      until Flag;


                      FDisplay.ShowHint:= False;
                      FDisplay.Hint:= '';
                      for J:= 0 to High(Index) do
                      begin
                        I:= Index[J];
                        if Config.ShowEventIndex then FDisplay.Hint:= FDisplay.Hint + Format('[%d / 0x%x] - ', [FDisplay.InfoRect[I].EventIdx, FDisplay.InfoRect[I].EventID]) + ' ';
                        case FDisplay.InfoRect[I].Typ of
                             rtInfo: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                         rtHandScan: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                   rtRailHeadScaner: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                         rtNoteBook: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                              rtRec: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                         rtAirBrush: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                            rtAlarm: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                            rtMedia: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                        end;
                      end;
                    end
                    else
                    begin
                      FDisplay.ShowHint:= False;
                      FDisplay.Hint:= '';
                      for I:= 0 to High(FDisplay.InfoRect) do
                        if PtInRect(FDisplay.InfoRect[I].Rt, Point(X, Y)) then
                        begin

//                          if Config.ShowEventIndex then FDisplay.Hint:= FDisplay.Hint + Format('[%d / 0x%x] - ', [FDisplay.InfoRect[I].EventIdx, FDisplay.InfoRect[I].EventID]) + FDisplay.InfoRect[I].Text + #10

                          case FDisplay.InfoRect[I].Typ of
                               rtInfo: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                           rtHandScan: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                     rtRailHeadScaner: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                           rtNoteBook: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                                rtRec: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                           rtAirBrush: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                              rtAlarm: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                              rtMedia: FDisplay.Hint:= FDisplay.Hint + FDisplay.InfoRect[I].Text + #10;
                          end;
                        end;
                    end;

                                        // Формирование полной расшифровки без сокращения списка см. выше

                    SetLength(Index, Length(FDisplay.InfoRect));
                    for I:= 0 to High(FDisplay.InfoRect) do Index[I]:= I;
                    if Length(Index) > 1 then
                    repeat
                      Flag:= True;
                      for J:= 0 to High(Index) - 1 do // Сотрировка по номеру события
                        if FDisplay.InfoRect[Index[J]].EventIdx > FDisplay.InfoRect[Index[J + 1]].EventIdx then
                        begin
                          I:= Index[J];
                          Index[J]:= Index[J + 1];
                          Index[J + 1]:= I;
                          Flag:= False;
                        end;
                    until Flag;


                    FDisplay.FullHint:= '';
                    for J:= 0 to High(FDisplay.InfoRect) do
                    begin
                      I:= Index[J];
                      if PtInRect(FDisplay.InfoRect[I].Rt, Point(X, Y)) then
                      begin

                        //EventID

                        //need sort

                        FDisplay.FullHintDisCoord:= FDisplay.DatSrc.Event[FDisplay.InfoRect[I].EventIdx].DisCoord;
                        if Config.ShowEventIndex then FDisplay.Hint:= FDisplay.Hint + Format('[%d / 0x%x] - ', [FDisplay.InfoRect[I].EventIdx, FDisplay.InfoRect[I].EventID]) + ' ';
                        case FDisplay.InfoRect[I].Typ of
                             rtInfo: FDisplay.FullHint:= FDisplay.FullHint + FDisplay.InfoRect[I].Text + #10;
                         rtHandScan: FDisplay.FullHint:= FDisplay.FullHint + FDisplay.InfoRect[I].Text + #10;
                   rtRailHeadScaner: FDisplay.FullHint:= FDisplay.FullHint + FDisplay.InfoRect[I].Text + #10;
                         rtNoteBook: FDisplay.FullHint:= FDisplay.FullHint + FDisplay.InfoRect[I].Text + #10;
                              rtRec: FDisplay.FullHint:= FDisplay.FullHint + FDisplay.InfoRect[I].Text + #10;
                         rtAirBrush: FDisplay.FullHint:= FDisplay.FullHint + FDisplay.InfoRect[I].Text + #10;
                            rtAlarm: FDisplay.FullHint:= FDisplay.FullHint + FDisplay.InfoRect[I].Text + #10;
                            rtMedia: FDisplay.FullHint:= FDisplay.FullHint + FDisplay.InfoRect[I].Text + #10;
                        end;
                       { if I > 20 then
                        begin
                          FDisplay.Hint:= FDisplay.Hint + '...' + #10;
                          Break;
                        end; }
                      end;
                    end;

                    if FDisplay.Hint <> '' then
                    begin
                      S:= FDisplay.Hint;
                      S[Length(S)]:= ' ';
                      FDisplay.Hint:= S;
                      FDisplay.ShowHint:= True;
                      Application.ActivateHint(Mouse.CursorPos);
                    end;

                    if not FDisplay.GetMouseData(X, Y) then
                    begin
                      //Panel17.Caption:= 'X';
                      //Panel18.Caption:= 'X';
                      //Panel19.Caption:= 'X';
                      //Panel20.Caption:= 'X';
                      //Panel21.Caption:= 'X';
                      //Panel22.Caption:= 'X';
                      //Panel24.Caption:= 'X';
                      //Panel25.Caption:= 'X';
                      //Panel26.Caption:= 'X';
                      //Panel27.Caption:= 'X';
                      //Panel28.Caption:= 'X';
                      //Label46.Caption:= 'X';
                      //Label49.Caption:= 'X';
                      Label43.Caption:= LangTable.Caption['Common:Time'] + ':-';
                      Label40.Caption:= LangTable.Caption['Common:Rail'];
                      Label40V.Caption:= ' -';
                      Label2.Caption:= ' -';
                      LDyV.Caption:= '-';
                      LDhV.Caption:= '-';
//                      StringGrid1.Cells[1, 0]:= 'X';
//                      StringGrid1.Cells[1, 1]:= 'X';
//                      StringGrid1.Cells[1, 2]:= 'X';
//                      Label39.Caption:= '';

(*                        if DebugInfo then
                        begin
                          //DebugLabel.AutoSize:= True;
                          //DebugLabel.WordWrap:= True;

                          FDisplay.DatSrc.DisToFileOffset(FDisplay.MousePosDat.MouseDisCoord, ResDisCoord, ResSysCoord, ResOffset);
//                          DebugLabel.Caption:= Format('SysCoord: %d'  + #$d + #$a + 'DisCoord: %d (%d)' + #$d + #$a + 'Offset: %x (%d)' + #$d + #$a, [ResSysCoord, ResDisCoord, FDisplay.MousePosDat.MouseDisCoord, ResOffset, ResOffset]);
                          MainForm.DebugPanel.Caption:= Format('SysCoord: %d'  + #$d + #$a + 'DisCoord: %d (%d)' + #$d + #$a + 'Offset: %x (%d)' + #$d + #$a, [ResSysCoord, ResDisCoord, FDisplay.MousePosDat.MouseDisCoord, ResOffset, ResOffset]);

                        end
                        else
                        begin
                          //DebugLabel.AutoSize:= False;
                          //DebugLabel.Height:= 0;
                        end;
  *)
                    end
                    else
                    begin

                      if DebugInfo then
                      begin
                        //DebugLabel.AutoSize:= True;
                        //DebugLabel.WordWrap:= True;

                        FDisplay.DatSrc.DisToFileOffset(FDisplay.MousePosDat.MouseDisCoord, ResDisCoord, ResSysCoord, ResOffset);
//                          DebugLabel.Caption:= Format('SysCoord: %d'  + #$d + #$a + 'DisCoord: %d (%d)' + #$d + #$a + 'Offset: %x (%d)' + #$d + #$a, [ResSysCoord, ResDisCoord, FDisplay.MousePosDat.MouseDisCoord, ResOffset, ResOffset]);
                        MainForm.DebugPanel.Caption:= Format('SysCoord: %d; DisCoord: %d (%d); Offset: %x (%d)', [ResSysCoord, ResDisCoord, FDisplay.MousePosDat.MouseDisCoord, ResOffset, ResOffset]);

                      end;

                      with FDisplay.MousePosDat.CrdParams do
                      begin
                        //Panel17.Caption:= IntToStr(LeftStolb[ssLeft].Km);
                        //Panel18.Caption:= IntToStr(LeftStolb[ssRight].Km);
                        //Panel19.Caption:= IntToStr(LeftStolb[ssLeft].Pk);
                        //Panel20.Caption:= IntToStr(LeftStolb[ssRight].Pk);

//                        if FCoordMode then Label2.Caption:= Format(LangTable.Caption['Common:Coordinate'] + ': %s', [LineCoordToStr(FDatSrc.DisToSysCoord(FDisplay.MousePosDat.MouseDisCoord) * FDatSrc.Header.ScanStep / 100 / 1000  + FDatSrc.Header.StartKM * 1000 + FDatSrc.Header.StartPk * 100 + FDatSrc.Header.StartMetre, 0)]) else
                        begin
                          RC:= CrdParamsToRealCrd(FDisplay.MousePosDat.CrdParams, FDatSrc.Header.MoveDir);
                          Label2.Caption:= RealCrdToStr(RC);
                        end;

                        if DebugInfo then
                        begin
                          //DebugLabel.AutoSize:= True;
                          //DebugLabel.WordWrap:= True;

                          FDisplay.DatSrc.DisToFileOffset(FDisplay.MousePosDat.MouseDisCoord, ResDisCoord, ResSysCoord, ResOffset);
//                          DebugLabel.Caption:= Format('SysCoord: %d'  + #$d + #$a + 'DisCoord: %d (%d)' + #$d + #$a + 'Offset: %x (%d)' + #$d + #$a, [ResSysCoord, ResDisCoord, FDisplay.MousePosDat.MouseDisCoord, ResOffset, ResOffset]);
                          MainForm.Panel5.Caption:= Format('SysCoord: %d'  + #$d + #$a + 'DisCoord: %d (%d)' + #$d + #$a + 'Offset: %x (%d)' + #$d + #$a, [ResSysCoord, ResDisCoord, FDisplay.MousePosDat.MouseDisCoord, ResOffset, ResOffset]);

                        end
                        else
                        begin
                          //DebugLabel.AutoSize:= False;
                          //DebugLabel.Height:= 0;
                        end;

{                        if R.LeftStolb[ssLeft].Km = R.LeftStolb[ssRight].Km then
                        begin
                          //Panel17.Visible:= False;
                          //Panel18.Visible:= False;
                          //Panel26.Visible:= True;
                        end
                        else
                        begin
                          //Panel17.Visible:= True;
                          //Panel18.Visible:= True;
                          //Panel26.Visible:= False;
                        end;

                        if R.RightStolb[ssLeft].Km = R.RightStolb[ssRight].Km then
                        begin
                          //Panel21.Visible:= False;
                          //Panel22.Visible:= False;
                          //Panel27.Visible:= True;
                        end
                        else
                        begin
                          //Panel21.Visible:= True;
                          //Panel22.Visible:= True;
                          //Panel27.Visible:= False;
                        end;
 }                           {
                        if LeftStolb[ssLeft].Pk = LeftStolb[ssRight].Pk then
                        begin
                          Panel19.Visible:= False;
                          Panel20.Visible:= False;
                          Panel28.Visible:= True;
                        end
                        else
                        begin
                          Panel19.Visible:= True;
                          Panel20.Visible:= True;
                          Panel28.Visible:= False;
                        end;

                        Panel26.Caption:= IntToStr(LeftStolb[ssLeft].Km);
                        Panel27.Caption:= IntToStr(RightStolb[ssLeft].Km);
                        Panel28.Caption:= IntToStr(LeftStolb[ssLeft].Pk);

                        Label46.Caption:= Format('%3.2f ' + LangTable.Caption['Common:m'], [ToLeftStolbMM / 1000]);
                        Label49.Caption:= Format('%3.2f ' + LangTable.Caption['Common:m'], [ToRightStolbMM / 1000]);

                        Panel21.Caption:= IntToStr(RightStolb[ssLeft].Km);
                        Panel22.Caption:= IntToStr(RightStolb[ssRight].Km);
                        Panel24.Caption:= IntToStr(RightStolb[ssLeft].Pk);
                        Panel25.Caption:= IntToStr(RightStolb[ssRight].Pk);
                        }
                      end;

                      Label43.Caption:= LangTable.Caption['Common:Time'] + ':' + FDisplay.MousePosDat.Time;
                      Label26.Caption:= LangTable.Caption['Common:Speed'] + ':' + FDisplay.MousePosDat.Speed + ' ' + LangTable.Caption['Common:km/h'];

                      with FDisplay.MousePosDat do
                        if Ok {(Ch1_ <> - 1) and (Ch2_ <> - 1)} then
                        begin
                             {
                          case Ch1_ of
                           0, 1: FHName1:= Format('%3.1f', [2.95 * D1]);
                           2, 3,
                              5: case Round(D1) of
                                      0..40: FHName1:= Format('%3.1f', [     35 * (D1 -   0) / 40]);
                                     41..75: FHName1:= Format('%3.1f', [36 - 33 * (D1 -  40) / 35]);
                                    76..100: FHName1:= Format('%3.1f', [ 3 + 12 * (D1 -  75) / 25]);
                                   101..135: FHName1:= Format('%3.1f', [15 + 10 * (D1 - 100) / 35]);
                                 end;
                              4: FHName1:= Format('%3.1f', [D1 * 3.26 / 2 * Cos(70 * Pi / 180)]);
                           6, 7,
                           8, 9: FHName1:= Format('%3.1f', [D1 * 3.26 / 2 * Cos(42 * Pi / 180)]);
                          end;

                          case Ch2_ of
                           0, 1: FHName2:= Format('%3.1f', [2.95 * D2]);
                           2, 3,
                              5: case Round(D2) of
                                      0..40: FHName2:= Format('%3.1f', [     35 * (D2 -   0) / 40]);
                                     41..75: FHName2:= Format('%3.1f', [36 - 33 * (D2 -  40) / 35]);
                                    76..100: FHName2:= Format('%3.1f', [ 3 + 12 * (D2 -  75) / 25]);
                                   101..135: FHName2:= Format('%3.1f', [15 + 10 * (D2 - 100) / 35]);
                                 end;
                              4: FHName2:= Format('%3.1f', [D2 * 3.26 / 2 * Cos(70 * Pi / 180)]);
                           6, 7,
                           8, 9: FHName2:= Format('%3.1f', [D2 * 3.26 / 2 * Cos(42 * Pi / 180)]);
                          end;
                                                               }
                          if not FDisplay.RailView then
                          begin
//                            StringGrid1.Color:= clWindow;
//                            StringGrid2.Color:= clWindow;

                          //  if D1 = D2 then
                            begin
//                              StringGrid2.Visible:= False;
//                              StringGrid1.Visible:= True;
//                              StringGrid1.ColCount:= 2;
//                              StringGrid1.ColWidths[0]:= 87;
//                              StringGrid1.ColWidths[1]:= 57;
//                              StringGrid1.Cells[1, 0]:= Format('%d, %d', [Ch1_, Ch2_]);
//                              StringGrid1.Cells[1, 1]:= ;
//                              StringGrid1.Cells[1, 2]:= ;

                              DebugForm.DebugMemo1.Clear;
                              DebugForm.DebugMemo1.Lines.Add(Format('DisCoord = %d', [FDisplay.MousePosDat.MouseDisCoord]));
                              DebugForm.DebugMemo1.Lines.Add(Format('SysCoord = %d', [FDisplay.DatSrc.DisToSysCoord(FDisplay.MousePosDat.MouseDisCoord)]));
//                              FDisplay.DatSrc.te DisToDisCoords(FDisplay.MousePosDat.MouseDisCoord, ResDtD);
//                              DebugForm.DebugMemo.Lines.Add(Format('%d', [Length(ResDtD)]));

                              //DebugForm.DebugMemo.Lines.Add('Back Motion Zone - IN');
                              //DebugForm.DebugMemo.Lines.Add('Back Motion Zone - OUT');



                              LDyV.Caption:= Format('%3.1f %s', [Delay[0], LangTable.Caption['Common:us']]);
                              case Config.MeasurementMode of
                                mmMillimeter: LDhV.Caption:= Format('%3.0f %s', [Depth[0], LangTable.Caption['Common:mm']]);
                                    mmInches: LDhV.Caption:= Format('%3.2f"', [Depth[0] * 0.03937]);
                              end;



//                              KillSelection(StringGrid1);
                            end ;
                            {else
                            begin
                              StringGrid2.Visible:= True;
                              StringGrid1.Visible:= False;
                              StringGrid2.Cells[1, 0]:= IntToStr(Ch1_);
                              StringGrid2.Cells[1, 1]:= IntToStr(D1);
                              StringGrid2.Cells[1, 2]:= FHName1;
                              StringGrid2.Cells[2, 0]:= IntToStr(Ch2_);
                              StringGrid2.Cells[2, 1]:= IntToStr(D2);
                              StringGrid2.Cells[2, 2]:= FHName2;
                            end; }

//                            Sensitivity1.Color:= clWindow;
//                            Sensitivity2.Color:= clWindow;
//                            sgHeadPh.Color:= clWindow;
                            {
                            for R:= rLeft to rRight do
                              for Ch:= 0 to 9 do
                              begin
                                if Params.HeadPh[R, Ch] then
                                begin
                                  if Sound[R, Ch] xor (Ch = 0) then sgHeadPh.Cells[1 + Ord(R), 1 + Ch]:= LangTable.Caption['InfoBar_:Signal']
                                                               else sgHeadPh.Cells[1 + Ord(R), 1 + Ch]:= LangTable.Caption['InfoBar_:On'];
                                end else sgHeadPh.Cells[1 + Ord(R), 1 + Ch]:= LangTable.Caption['InfoBar_:Off'];

                              end; }

                            (*
                            case Ch1_ of
                              {$IFDEF ALLAVIKON}
                              2, 3: Label39.Caption:= LangTable.Caption['InfoBar_:Echochannelofrailheadinspection58°(2,3)'];
                              4, 5: Label39.Caption:= LangTable.Caption['InfoBar_:Echo70°(4)andmirror58°(5)channelofrailheadinspection'];
                              {$ENDIF}
                              {$IFDEF AVIKON11}
                              2, 3: Label39.Caption:= LangTable.Caption['InfoBar_:Echochannelofrailheadinspection58°(2,3)'];
                              4, 5: Label39.Caption:= LangTable.Caption['InfoBar_:Echo70°(4)andmirror58°(5)channelofrailheadinspection'];
                              {$ENDIF}
                              {$IFDEF AVIKON12}
                              2, 3: Label39.Caption:= LangTable.Caption['InfoBar_:Echochannelofrailheadinspection58°(2,3)'];
                              4, 5: Label39.Caption:= LangTable.Caption['InfoBar_:Echo70°(4)andmirror58°(5)channelofrailheadinspection'];
                              {$ENDIF}
                              {$IFDEF FILUSX17}
                              2, 3: Label39.Caption:= LangTable.Caption['InfoBar_:Echochannelofrailheadinspection58°(2,3)'];
                              4, 5: Label39.Caption:= LangTable.Caption['InfoBar_:Echo70°(4)andmirror58°(5)channelofrailheadinspection'];
                              {$ENDIF}
                              {$IFDEF AVIKON16}
                              2, 3: if FDisplay.DatSrc.Header.Reserved <> 2 then Label39.Caption:= LangTable.Caption['InfoBar_:Echochannelofrailheadinspection58°(2,3)']
                                                                            else Label39.Caption:= LangTable.Caption['InfoBar_:Mirror58°(2)andEcho58°(3)channelofrailheadinspection'];
                              4, 5: Label39.Caption:= LangTable.Caption['InfoBar_:Echochannelofrailheadinspection70°(4,5)'];
                              {$ENDIF}
                              6, 7: Label39.Caption:= LangTable.Caption['InfoBar_:Echochannelrailweb42°(6,7)andrailfoot42°(8,9)inspection'];
                              0, 1: Label39.Caption:= LangTable.Caption['InfoBar_:Mirror/shadow0°(0)andecho0°(1)channelsrailheadandwebi.'];
                            end; *)
                          end
                          else
                          begin
                              LDyV.Caption:= '-';
//                              LDhV.Caption:= Format('%3.1f', [Depth[0]]);
                              case Config.MeasurementMode of
                                mmMillimeter: LDhV.Caption:= Format('%3.0f %s', [Depth[0], LangTable.Caption['Common:mm']]);
                                    mmInches: LDhV.Caption:= Format('%3.2f"', [Depth[0] * 0.03937]);
                              end;

//                            for Row:= 0 to 2 do StringGrid1.Cells[1, Row]:= '';
//                            for Col:= 1 to 2 do
//                              for Row:= 0 to 2 do StringGrid2.Cells[Col, Row]:= '';

//                            StringGrid1.Color:= clBtnFace;
//                            StringGrid2.Color:= clBtnFace;

//                            Sensitivity1.Color:= clBtnFace;
//                            Sensitivity2.Color:= clBtnFace;

//                            sgHeadPh.Color:= clBtnFace;

  //                          for Col:= 1 to 2 do
//                              for Row:= 0 to 5 do Sensitivity2.Cells[Col, Row]:= '';

//                            for Col:= 1 to 4 do
  //                            for Row:= 0 to 5 do Sensitivity1.Cells[Col, Row]:= '';

//                            for R:= rLeft to rRight do
  //                            for Ch:= 0 to 9 do
    //                            sgHeadPh.Cells[1 + Ord(R), 1 + Ch]:= '';

                            //Label39.Caption:= '';
                          end;

                          if FDatSrc.Header.UsedItems[uiWorkRailTypeA] = 1 then// Контролируемая нить - для однониточных приборов
                          begin
                            if FDatSrc.Header.WorkRailTypeA = 0
                              then Label40V.Caption:= LangTable.Caption['Common:Left']
                              else Label40V.Caption:= LangTable.Caption['Common:Right'];
                          end
                          else Label40V.Caption:= RailToStr( FDatSrc.RailToPathRail(Rail), 0);

                          (*
                          {$IFNDEF FILUSX17}
                          case Rail of
                            rLeft: Label40V.Caption:= LangTable.Caption['Common:Left'];
                           rRight: Label40V.Caption:= LangTable.Caption['Common:Right'];
                          end;
                          {$ENDIF} *)

                       (*   {$IFDEF FILUSX17}

                          if FDatSrc.Header.MoveDir = 0 then
                          begin
                            if FDatSrc.Header.RailPos = 0 then Label40.Caption:= LangTable.Caption['Common:Rail'] + ' ' + LangTable.Caption['Common:Right']
                                                          else Label40.Caption:= LangTable.Caption['Common:Rail'] + ' ' + LangTable.Caption['Common:Left'];
                          end
                          else
                          begin
                            if FDatSrc.Header.RailPos = 0 then Label40.Caption:= LangTable.Caption['Common:Rail'] + ' ' + LangTable.Caption['Common:Left']
                                                          else Label40.Caption:= LangTable.Caption['Common:Rail'] + ' ' + LangTable.Caption['Common:Right'];
                          end;
                          {$ENDIF}      *)
                        end;


//                          for I:= 1 to 8 do
//                          begin
//                            RightEcho.Cells[0, I]:= '';
  //                          RightEcho.Cells[1, I]:= '';
    //                        LeftEcho.Cells[0, I]:= '';
      //                      LeftEcho.Cells[1, I]:= '';
//                          end;

                          (*
                          if FDisplay.Reduction = 0 then
                          begin
                            RightEcho.Color:= clWindow;

                            for I:= 1 to CurEcho[Rail, Ch1_].Count do
                            begin
                       //       RightEcho.Cells[0, I]:=  DBName[CurEcho[Rail, Ch1_].Ampl[I]];
                              if Ch1_ in [0, 1] then RightEcho.Cells[1, I]:= Format('%3.1f', [CurEcho[Rail, Ch1_].Delay[I] / 3])
                                                else RightEcho.Cells[1, I]:= IntToStr(CurEcho[Rail, Ch1_].Delay[I]);
                            end;

                            LeftEcho.Color:= clWindow;
                            I1:= 1;
                            I2:= 1;
                            for I:= 1 to CurEcho[Rail, Ch2_].Count do
                            begin
                              if Ch2_ = 1 then
                              begin
                              {  if (CurEcho[Rail, Ch2_].Delay[I] / FDatSrc.DelayKoeff >= FDatSrc.CurParams.StStr[Rail, 0]) and
                                   (CurEcho[Rail, Ch2_].Delay[I] / FDatSrc.DelayKoeff <= FDatSrc.CurParams.EdStr[Rail, 0]) then
                                begin
                                  RightEcho.Cells[0, I1]:=  DBName[CurEcho[Rail, Ch2_].Ampl[I]];
                                  RightEcho.Cells[1, I1]:= Format('%3.1f', [CurEcho[Rail, Ch2_].Delay[I] / 3]);
                                  Inc(I1);
                                end
                                else
                                begin
                                  LeftEcho.Cells[0, I2]:= DBName[CurEcho[Rail, Ch2_].Ampl[I]];
                                  LeftEcho.Cells[1, I2]:= Format('%3.1f', [CurEcho[Rail, Ch2_].Delay[I] / 3]);
                                  Inc(I2);
                                end; }
                              end
                              else
                              begin
//                                LeftEcho.Cells[0, I]:= DBName[CurEcho[Rail, Ch2_].Ampl[I]];
//                                LeftEcho.Cells[1, I]:= IntToStr(CurEcho[Rail, Ch2_].Delay[I]);
                                Inc(I2);
                              end;
                            end;
                          end
                          else
                          begin
                            RightEcho.Color:= clBtnFace;
                            LeftEcho.Color:= clBtnFace;
                            sgHeadPh.Color:= clBtnFace;
                          end;
                          *)

//                          ClearAScan1;
//                          ClearAScan2;

//                          Color:= clBlack;

//                          if FDisplay.Reduction = 0 then
//                          begin
//                            Chart1.Color:= clWindow;
  //                          Chart1.BackColor:= clWindow;
    //                        Chart2.BackColor:= clWindow;

 {                            if ComboBox1.ItemIndex <= High(ScanChNumList) then
                            begin
                              ChNum:= ScanChNumList[ComboBox1.ItemIndex];

                              for J:= 0 to High(CurEchoList) do
                              begin
                                SetAScanDelayZone(FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin,
                                                  FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax,

                                                  FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMin,
                                                  FDatSrc.Config.ScanChannelByNum[ChNum].BScanGateMax);

                                with CurEchoList[J].Echo[FDisplay.MousePosDat.Rail, ChNum] do
                                  for I:= 1 to Count do
                                    AddAScan1(Delay[I], FDatSrc.Config.AmplValue[Ampl[I]], Color);
                              end;
                            end;   }

//                            for I:= 0 to High(AScanArr[2]) do AddAScan2(AScanArr[2, I].Delay, AScanArr[2, I].Ampl, AScanArr[2, I].Color);
//                            EndAScan;
//                          end
//                          else
//                          begin
//                           Chart1.BackColor:= clBtnFace;
//                            Chart2.BackColor:= clBtnFace;
//                          end;
                    end;
                  end;
//  mmSelRailPoint: ;
       mmSetRect: begin
                    FDisplay.GetMouseRect(FDisplay.SelRect, DisCoord1, DisCoord2, Ch1Delay1, Ch1Delay2, Ch2Delay1, Ch2Delay2, Ch1, Ch2, SameRect);
                    if (DisCoord1 <> - MaxInt) and (DisCoord2 <> - MaxInt) then
                    begin
             {         Memo1.Clear;
                      Memo1.Lines.Add(IntToStr(FDisplay.SelRect.Left) + '-' + IntToStr(FDisplay.SelRect.Right));
                      Memo1.Lines.Add(IntToStr(DisCoord1));
                      Memo1.Lines.Add(IntToStr(DisCoord2));
                      Memo1.Lines.Add(IntToStr(DisCoord2 - DisCoord1));
                      Memo1.Lines.Add(IntToStr(FDatSrc.Header.ScanStep));
              }

                      VVV:= Abs(Trunc((DisCoord1 - DisCoord2) * FDatSrc.Header.ScanStep / 100));

                      if VVV < 1000 then
                      begin

                        case Config.MeasurementMode of
                          mmMillimeter: begin
                                          S:= Format('%d', [VVV]);
                                          Label134.Caption:= 'ΔL [' + LangTable.Caption['Common:mm'] + ']';
                                        end;
                              mmInches: begin
                                          S:= Format('%3.2f"', [VVV * 0.03937]);
                                          Label134.Caption:= 'ΔL';
                                        end;
                        end;

                      end
                      else
                      begin

                        case Config.MeasurementMode of
                          mmMillimeter: begin
                                          if VVV < 5000 then S:= Format('%1.2f', [VVV / 1000])
                                                        else S:= Format('%6.1f', [VVV / 1000]);
                                          Label134.Caption:= 'ΔL [' + LangTable.Caption['Common:m'] + ']';
                                        end;
                              mmInches: begin
                                          S:= Format('%3.2f', [3.281 * VVV / 1000]);
                                          Label134.Caption:= 'ΔL [ft]';
                                        end;
                        end;

                      end;


                      //if Length(S) > 3 then Insert(' ', S, Length(S) - 2);
                      LabelDL.Caption:= S; //  + ' ' + LangTable.Caption['Common:mm'];

//                      LabelDL.Caption:= IntToStr(Abs(Trunc((DisCoord1 - DisCoord2) * FDatSrc.Header.ScanStep / 100))) + ' ' + LangTable.Caption['Common:mm'];
                      if not FDisplay.RailView and SameRect then
                      begin
                        if (Ch1Delay1 <> Ch2Delay1) or (Ch1Delay2 <> Ch2Delay2) then
                        begin
//                          CalcPanel1.Visible:= False;
//                          CalcPanel2.Visible:= True;
//                          Label32.Caption:= LangTable.Caption['Common:Channel'] + Format(': %d', [Ch1]);
//                          Label50.Caption:= LangTable.Caption['Common:Channel'] + Format(': %d', [Ch2]);

//                          LabelH1_1.Caption:= Format('%5d', [Math.Min(Ch1Delay1, Ch1Delay2)]) + ' ' + LangTable.Caption['InfoBar_:us'];
//                          LabelH2_1.Caption:= Format('%5d', [Math.Max(Ch1Delay1, Ch1Delay2)]) + ' ' + LangTable.Caption['InfoBar_:us'];
//                          LabelDH_1.Caption:= Format('%5d', [Math.Max(Ch1Delay1, Ch1Delay2) - Math.Min(Ch1Delay1, Ch1Delay2)]) + ' ' + LangTable.Caption['InfoBar_:us'] + #10;

//                          LabelH1_2.Caption:= Format('%5d', [Math.Min(Ch2Delay1, Ch2Delay2)]) + ' ' + LangTable.Caption['InfoBar_:us'];
//                          LabelH2_2.Caption:= Format('%5d', [Math.Max(Ch2Delay1, Ch2Delay2)]) + ' ' + LangTable.Caption['InfoBar_:us'];
//                          LabelDH_2.Caption:= Format('%5d', [Math.Max(Ch2Delay1, Ch2Delay2) - Math.Min(Ch2Delay1, Ch2Delay2)]) + ' ' + LangTable.Caption['InfoBar_:us'] + #10;
                        end
                        else
                        begin
                          LabelH1.Visible:= True;
                          LabelH2.Visible:= True;
                          LabelDH.Visible:= True;
                          LabelH1.Caption:= Format('%d', [Math.Min(Ch2Delay1, Ch2Delay2)]) {+ ' ' + LangTable.Caption['InfoBar_:us']};
                          LabelH2.Caption:= Format('%d', [Math.Max(Ch2Delay1, Ch2Delay2)]) {+ ' ' + LangTable.Caption['InfoBar_:us']};
                          LabelDH.Caption:= Format('%d', [Math.Max(Ch2Delay1, Ch2Delay2) - Math.Min(Ch2Delay1, Ch2Delay2)]) {  + ' ' + LangTable.Caption['InfoBar_:us'] + #10};

            //            LabelH1MM.Caption:= FHName1; // Format('%5d', [Math.Min(Ch1Delay1, Ch1Delay2)]) + ' ' + LangTable.Caption['InfoBar_:us'];
            //            LabelH2MM.Caption:= FHName2; // Format('%5d', [Math.Max(Ch1Delay1, Ch1Delay2)]) + ' ' + LangTable.Caption['InfoBar_:us'];
            //            LabelDHMM.Caption:= ''; //Format('%5d', [Math.Max(Ch1Delay1, Ch1Delay2) - Math.Min(Ch1Delay1, Ch1Delay2)]) + ' ' + LangTable.Caption['InfoBar_:us'] + #10;
                        end;
                      end
                      else
                      begin
                          LabelH1.Visible:= False;
                          LabelH2.Visible:= False;
                          LabelDH.Visible:= False;
//                        CalcPanel1.Visible:=
//                        CalcPanel2.Visible:= False;
                      end;
                    end;
                  end;
  end;
end;

procedure TInfoBarForm.ChangeMouseMode(Sender: TObject);
begin
  if (not Assigned(FDatSrc)) or (not Assigned(FDisplay)) then Exit;

  if FDisplay.MouseMode = mmSetRect then
  begin
    Panel12.BringToFront;

//    FSavePageIdx:= InfoPageControl.ActivePageIndex;
//    InfoPageControl.ActivePage:= TabSheet5;
  end
  else     Panel12.SendToBack

//  if (FDisplay.MouseMode <> mmSetRect) and (InfoPageControl.ActivePage = TabSheet5) then
//  begin
//    InfoPageControl.ActivePageIndex:= FSavePageIdx;
//  end;
end;

procedure TInfoBarForm.OnChangeLanguage(Sender: TObject);
begin
  Self.Font.Name              := LangTable.Caption['General:FontName'];
  Label40V.Font.Name          := LangTable.Caption['General:FontName'];
  Label2.Font.Name            := LangTable.Caption['General:FontName'];

  LabelDH.Font.Name           := LangTable.Caption['General:FontName'];
  LabelDL.Font.Name           := LangTable.Caption['General:FontName'];
  LabelH1.Font.Name           := LangTable.Caption['General:FontName'];
  LabelH2.Font.Name           := LangTable.Caption['General:FontName'];

  LDyV.Font.Name              := LangTable.Caption['General:FontName'];
  LDhV.Font.Name              := LangTable.Caption['General:FontName'];

  Label134.Caption            := 'ΔL [' + LangTable.Caption['Common:mm'] + ']';
  Label134.Font.Name          := LangTable.Caption['General:FontName'];

  Label4.Font.Name            := LangTable.Caption['General:FontName'];
  Label7.Font.Name            := LangTable.Caption['General:FontName'];
  Label1.Font.Name            := LangTable.Caption['General:FontName'];
  Label7.Caption              := '[' + LangTable.Caption['Common:us'] + ']';

  Label6.Font.Name            := LangTable.Caption['General:FontName'];
  Label8.Font.Name            := LangTable.Caption['General:FontName'];
  Label5.Font.Name            := LangTable.Caption['General:FontName'];
  Label8.Caption              := '[' + LangTable.Caption['Common:us'] + ']';

  Label17.Font.Name           := LangTable.Caption['General:FontName'];
  Label17.Caption             := 'ΔH [' + LangTable.Caption['Common:us'] + ']';

  Label3.Caption              := LangTable.Caption['Common:Coordinate'];
//  Label41.Caption             := LangTable.Caption['Common:Nearestmarks'] + ':';  // Координата:
//  Label42.Caption             := LangTable.Caption['Common:Km'];  // Км
//  Label45.Caption             := LangTable.Caption['Common:PK'];  // ПК
//  Label4.Caption              := LangTable.Caption['InfoBar_:Fileinformation'];  // Информация о файле
//  FileInfo.TitleCaptions.Clear;
//  FileInfo.TitleCaptions.Add(    LangTable.Caption['InfoBar_:Property']); // Название
//  FileInfo.TitleCaptions.Add(    LangTable.Caption['InfoBar_:Value']); // Значение
//  Label5.Caption              := LangTable.Caption['InfoBar_:Params'];  // Параметры
//  Label6.Caption              := LangTable.Caption['InfoBar_:Echosignaltables'];  // Таблицы эхо-сигналов
//  Label13.Caption             := LangTable.Caption['InfoBar_:Echosignaltables'];  // Таблицы эхо-сигналов
//  Label14.Caption             := LangTable.Caption['InfoBar_:Params'];

//  StaticText1.Caption         := LangTable.Caption['InfoBar_:Zoomin:'];  // Наезжающий:
//  StaticText2.Caption         := LangTable.Caption['InfoBar_:Zoomout:'];  // Отъезжающий:
//  Label7.Caption              := LangTable.Caption['InfoBar_:A-scan'];  // А-развертка
//  Label16.Caption             := LangTable.Caption['InfoBar_:A-scan'];  // А-развертка
//  StaticText4.Caption         := LangTable.Caption['InfoBar_:Zoomin:'];  // Наезжающий:
//  StaticText5.Caption         := LangTable.Caption['InfoBar_:Zoomout:'];  // Отъезжающий:
//  Panel31.Caption             := LangTable.Caption['InfoBar_:dB'];  // дБ
//  Panel32.Caption             := LangTable.Caption['InfoBar_:dB'];  // дБ
//  Panel29.Caption             := LangTable.Caption['InfoBar_:us'];  // мкс
//  Panel30.Caption             := LangTable.Caption['InfoBar_:us'];  // мкс
//  Label8.Caption              := LangTable.Caption['InfoBar_:Notebook'];  // Блокнот
//  AddButton.Caption           := LangTable.Caption['InfoBar_:New'];  // Новая
//  DelButton.Caption           := LangTable.Caption['InfoBar_:Delete'];  // Удалить
//  Label9.Caption              := LangTable.Caption['InfoBar_:Measuring'];  // Измерения
//  Label19.Caption             := LangTable.Caption['InfoBar_:Measuring'];  // Измерения
  Label40.Caption             := LangTable.Caption['Common:Rail'];
//  Label39.Caption             := LangTable.Caption['Common:Channel'];
  Label43.Caption             := LangTable.Caption['Common:Time'];
  Label26.Caption             := LangTable.Caption['Common:Speed'];

//  Panel44.Caption             := LangTable.Caption['InfoBar_:dB'];  // дБ
//  Panel42.Caption             := LangTable.Caption['InfoBar_:us'];  // мкс

//  HSParam.Cells[0, 0]:= LangTable.Caption['Common:Rail']; // 'Нить';
//  HSParam.Cells[0, 1]:= LangTable.Caption['Common:Channel']; // 'Канал';
//  HSParam.Cells[0, 2]:= LangTable.Caption['HandScan:Surface']; // 'Поверхность';
//  HSParam.Cells[0, 3]:= LangTable.Caption['HandScan:Attenuator']; // 'Положение аттенюатора';
//  HSParam.Cells[0, 4]:= LangTable.Caption['HandScan:Sensitivity']; // 'Чувств-сть';
//  HSParam.Cells[0, 5]:= LangTable.Caption['InfoBar_:TVG']; // 'ВРЧ';

//  Label15.Caption := LangTable.Caption['HandScan:Probing'] + ' ' + LangTable.Caption['HandScan:Cycle:'];
//  Label10.Caption := LangTable.Caption['HandScan:Delay:'] + ' ' + LangTable.Caption['HandScan:Cycle:'];

//  HSEcho.Cells[0, 0]:= LangTable.Caption['Common:dB']; // 'дБ';
//  HSEcho.Cells[1, 0]:= LangTable.Caption['Common:us']; // 'мкс';

//  Sensitivity1.Cells[0, 0]:= LangTable.Caption['Common:Channel'];
//  Sensitivity1.Cells[0, 1]:= LangTable.Caption['InfoBar_:Sens.'];
//  Sensitivity1.Cells[0, 2]:= LangTable.Caption['InfoBar_:Att-r'];
//  Sensitivity1.Cells[0, 3]:= LangTable.Caption['InfoBar_:TVG'];
//  Sensitivity1.Cells[0, 4]:= LangTable.Caption['InfoBar_:Probedelay'];
//  Sensitivity1.Cells[0, 5]:= LangTable.Caption['InfoBar_:Bang'];

//  StringGrid1.Cells[0, 0]:= LangTable.Caption['Common:Channel'];
//  StringGrid1.Cells[0, 1]:= LangTable.Caption['InfoBar_:Delay[us]'];
//  StringGrid1.Cells[0, 2]:= LangTable.Caption['InfoBar_:Depth[mm]'];

  LDyN.Caption:= LangTable.Caption['Common:Delay[us]'];
  LDhN.Caption:= LangTable.Caption['Common:Depth[mm]'];



//  StringGrid2.Cells[0, 0]:= LangTable.Caption['Common:Channel'];
//  StringGrid2.Cells[0, 1]:= LangTable.Caption['InfoBar_:Delay[us]'];
//  StringGrid2.Cells[0, 2]:= LangTable.Caption['InfoBar_:Depth[mm]'];
{
  Sensitivity2.Cells[0, 0]:= LangTable.Caption['Common:Channel'];
  Sensitivity2.Cells[0, 1]:= LangTable.Caption['InfoBar_:Sens.'];
  Sensitivity2.Cells[0, 2]:= LangTable.Caption['InfoBar_:Att-r'];
  Sensitivity2.Cells[0, 3]:= LangTable.Caption['InfoBar_:TVG'];
  Sensitivity2.Cells[0, 4]:= LangTable.Caption['InfoBar_:Probedelay'];
  Sensitivity2.Cells[0, 5]:= LangTable.Caption['InfoBar_:Bang'];

  RightEcho.Cells[0, 0]:= LangTable.Caption['Common:dB'];
  RightEcho.Cells[1, 0]:= LangTable.Caption['InfoBar_:us'];
  LeftEcho.Cells[0, 0]:= LangTable.Caption['Common:dB'];
  LeftEcho.Cells[1, 0]:= LangTable.Caption['InfoBar_:us'];

  Label27.Caption:= LangTable.Caption['InfoBar_:ADA'];
}

//  MyList.NameCaption:= LangTable.Caption['InfoBar_:Coordinate'];
//  MyList.ValueCaption:= LangTable.Caption['Common:Def.code'];
{
  sgHeadPh.ColCount:= 3;
  sgHeadPh.RowCount:= 11;
  sgHeadPh.RowHeights[0]:= 30;
  sgHeadPh.ColWidths[0]:= 47;
  sgHeadPh.ColWidths[1]:= 50;
  sgHeadPh.ColWidths[2]:= 50;

  sgHeadPh.Cells[0,  0]:= LangTable.Caption['InfoBar_:Channel'];
  sgHeadPh.Cells[1,  0]:= LangTable.Caption['InfoBar_:LeftRail'];
  sgHeadPh.Cells[2,  0]:= LangTable.Caption['InfoBar_:RightRail'];

  sgHeadPh.Cells[0,  1]:= '0';
  sgHeadPh.Cells[0,  2]:= '1';
  sgHeadPh.Cells[0,  3]:= '2';
  sgHeadPh.Cells[0,  4]:= '3';
  sgHeadPh.Cells[0,  5]:= '4';
  sgHeadPh.Cells[0,  6]:= '5';
  sgHeadPh.Cells[0,  7]:= '6';
  sgHeadPh.Cells[0,  8]:= '7';
  sgHeadPh.Cells[0,  9]:= '8';
  sgHeadPh.Cells[0, 10]:= '9';

  KillSelection(sgHeadPh);
}
end;

end.
