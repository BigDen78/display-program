object LabelsBaseForm: TLabelsBaseForm
  Left = 0
  Top = 0
  Caption = 'LabelsBaseForm'
  ClientHeight = 467
  ClientWidth = 605
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 336
    Top = 104
    Width = 113
    Height = 25
    Caption = 'Open and Connect'
    TabOrder = 0
    OnClick = Button1Click
  end
  object DBGrid1: TDBGrid
    Left = 49
    Top = 304
    Width = 320
    Height = 120
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object SQLConnection1: TSQLConnection
    DriverName = 'ASA'
    GetDriverFunc = 'getSQLDriverASA'
    LibraryName = 'dbxasa.dll'
    VendorLib = 'dbodbc9.dll'
    Left = 64
    Top = 48
  end
  object s: TSQLQuery
    Params = <>
    Left = 152
    Top = 48
  end
  object SQLTable1: TSQLTable
    MaxBlobSize = -1
    SQLConnection = SQLConnection1
    Left = 224
    Top = 56
  end
  object OpenDialog: TOpenDialog
    Left = 360
    Top = 160
  end
  object SQLMonitor1: TSQLMonitor
    Left = 120
    Top = 144
  end
  object DataSource1: TDataSource
    Left = 296
    Top = 240
  end
end
