@echo off

set paramsScript=%~dp0\copyArParams.bat
set params=avikon31PrjPath libArBuildPath mingwXtoolsPath

rem ---------- get path parameters ---------
setLocal enableDelayedExpansion
if exist "%paramsScript%" (
    call %paramsScript%
) else (
	echo @echo off > %paramsScript%
	for %%x in (%params%) do (
		echo set %%x=>> %paramsScript%
	)
    echo "You should specify parameters in created %paramsScript%"
	exit
)
for %%x in (%params%) do (
	IF "!%%x!" == "" (
		echo You should specify %%x in %paramsScript%
		exit
	)
)

set target="%Avikon31PrjPath%\_EXE_\ar"
mkdir %target%
copy "%libArBuildPath%\libAr.dll" %target%\ar.dll
copy "%mingwXtoolsPath%\i686-w64-mingw32\sysroot\mingw\bin\libwinpthread-1.dll" %target%
copy "%mingwXtoolsPath%\i686-w64-mingw32\sysroot\lib\libstdc++-6.dll" %target%
copy "%mingwXtoolsPath%\i686-w64-mingw32\sysroot\lib\libgcc_s_dw2-1.dll" %target%
set mingwBoostLibPath="%mingwXtoolsPath%\i686-w64-mingw32\sysroot\usr\local\lib"
copy "%mingwBoostLibPath%\libboost_filesystem.dll" %target%
copy "%mingwBoostLibPath%\libboost_locale.dll" %target%
copy "%mingwBoostLibPath%\libboost_system.dll" %target%
copy "%mingwBoostLibPath%\libboost_iostreams.dll" %target%
copy "%mingwBoostLibPath%\..\bin\libzlib.dll" %target%



