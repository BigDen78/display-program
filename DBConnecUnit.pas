unit DBConnecUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Avk11Engine, LanguageUnit;

type
  TDBConnectForm = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Label6: TLabel;
    Edit6: TEdit;
    Label7: TLabel;
    Edit7: TEdit;
    procedure Button1Click(Sender: TObject);
  private
    FDatSrc: TAvk11DatSrc;
  public
    procedure SetDate(DatSrc: TAvk11DatSrc);
  end;

var
  DBConnectForm: TDBConnectForm;

implementation

{$R *.dfm}

procedure TDBConnectForm.Button1Click(Sender: TObject);
begin

  Edit1.Text:= CodeToText(FDatSrc.Header.Name, FDatSrc.Header.CharSet);
  Edit2.Text:= IntToStr(FDatSrc.Header.DirCode);
  Edit3.Text:= CodeToText(FDatSrc.Header.SecName, FDatSrc.Header.CharSet);
  Edit4.Text:= Format('%d ' + LangTable.Caption['Misc:km'] +
                                                            ' %d ' + LangTable.Caption['Misc:pk'] +
                                                            ' %d '  + LangTable.Caption['Misc:m'], [FDatSrc.Header.StartKM, FDatSrc.Header.StartPk, FDatSrc.Header.StartMetre]); // Начальная координата
  Edit5.Text:= Format('%d ' + LangTable.Caption['Misc:km'] +
                                                           ' %d ' + LangTable.Caption['Misc:pk'] +
                                                           ' %d '  + LangTable.Caption['Misc:m'], [FDatSrc.ExHeader.EndKM, FDatSrc.ExHeader.EndPk, FDatSrc.ExHeader.EndMM div 1000]); // Конечная координата
  Edit6.Text:= CodeToDate(FDatSrc.Header.Day, FDatSrc.Header.Month, FDatSrc.Header.Year);
  Edit7.Text:= IntToStr(FDatSrc.Header.Path);
(*


    Memo1.Clear;

//    Memo1.Lines.Add(LangTable.Caption['FileInfo:Organization'] + ' ' + CodeToText(FDatSrc.Header.Name, FDatSrc.Header.CharSet));
//    Memo1.Lines.Add(LangTable.Caption['FileInfo:Defectoscopenumber'] + ' ' +  Format(LangTable.Caption['NotebookRec:CDUNo'] + ' 0%d/ ' + LangTable.Caption['NotebookRec:UMUNo'] + ' 0%d', [FDatSrc.Header.UpDeviceID, FDatSrc.Header.DwnDeviceID]));

    Memo1.Lines.Add(LangTable.Caption['FileBase:StartCoordinate'] + ' ' + Format('%d ' + LangTable.Caption['Misc:km'] +
                                                            ' %d ' + LangTable.Caption['Misc:pk'] +
                                                            ' %d '  + LangTable.Caption['Misc:m'], [FDatSrc.Header.StartKM, FDatSrc.Header.StartPk, FDatSrc.Header.StartMetre])); // Начальная координата
    Memo1.Lines.Add(LangTable.Caption['FileBase:EndCoordinate'] + ' ' + Format('%d ' + LangTable.Caption['Misc:km'] +
                                                           ' %d ' + LangTable.Caption['Misc:pk'] +
                                                           ' %d '  + LangTable.Caption['Misc:m'], [FDatSrc.ExHeader.EndKM, FDatSrc.ExHeader.EndPk, FDatSrc.ExHeader.EndMM div 1000])); // Конечная координата

    Memo1.Lines.Add(LangTable.Caption['FileInfo:Date'] + ' ' + CodeToDate(FDatSrc.Header.Day, FDatSrc.Header.Month, FDatSrc.Header.Year));
    Memo1.Lines.Add(LangTable.Caption['FileInfo:Time'] + ' ' + CodeToTime(FDatSrc.Header.Hour, FDatSrc.Header.Minute));
    Memo1.Lines.Add(LangTable.Caption['FileInfo:EndTime'] + ' ' + FDatSrc.GetTime(FDatSrc.MaxDisCoord));
    Memo1.Lines.Add(LangTable.Caption['FileInfo:Operator'] + ' ' + CodeToText(FDatSrc.Header.Operator, FDatSrc.Header.CharSet));
    Memo1.Lines.Add(LangTable.Caption['FileInfo:DirectionCode'] + ' ' + IntToStr(FDatSrc.Header.DirCode));
    Memo1.Lines.Add(LangTable.Caption['FileInfo:Tracksection'] + ' ' + CodeToText(FDatSrc.Header.SecName, FDatSrc.Header.CharSet));
    Memo1.Lines.Add(LangTable.Caption['FileInfo:Tracknumber'] + ' ' + IntToStr(FDatSrc.Header.Path));
    if FDatSrc.Header.MoveDir = 0 then Memo1.Lines.Add(LangTable.Caption['FileInfo:MovingDirection'] + ' ' + LangTable.Caption['FileInfo:Downward'])
                             else Memo1.Lines.Add(LangTable.Caption['FileInfo:MovingDirection'] + ' ' + LangTable.Caption['FileInfo:UpWard']);
(*
    {$IFNDEF AVIKON12}
    if FDatSrc.Header.RailPos = 0 then Memo1.Lines.Add(LangTable.Caption['FileInfo:Railposition'], LangTable.Caption['FileInfo:Positive'], True)
                             else Memo1.Lines.Add(LangTable.Caption['FileInfo:Railposition'], LangTable.Caption['FileInfo:Negate'], True);
    {$ENDIF}
    {$IFDEF AVIKON12}
    if FDatSrc.Header.RailPos = 0 then Memo1.Lines.Add(LangTable.Caption['Misc:Rail'], LangTable.Caption['Misc:Left'], True)
                             else Memo1.Lines.Add(LangTable.Caption['Misc:Rail'], LangTable.Caption['Misc:Right'], True);
    {$ENDIF}
  *)
end;

procedure TDBConnectForm.SetDate(DatSrc: TAvk11DatSrc);
begin
  FDatSrc:= DatSrc;
end;

end.
