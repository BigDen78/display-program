unit AssignCtrl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, ExtCtrls, Avk11ViewUnit, ComCtrls, LanguageUnit;

type
  TAssignCtrlForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    CheckListBox1: TListView;
    procedure Button1Click(Sender: TObject);
  protected
    FAvk11View: TAvk11ViewForm;
  public
    procedure Execute(Avk11View: TAvk11ViewForm);
  end;

implementation

uses
  MainUnit, DisplayUnit;

{$R *.DFM}

procedure TAssignCtrlForm.Execute(Avk11View: TAvk11ViewForm);
var
  I, J: Integer;
  S: string;
  Cur: TAvk11ViewForm;
  New: TListItem;

begin
  FAvk11View:= Avk11View;
  Caption:= LangTable.Caption['MenuWindows:Linkwindows'];
  Label1.Caption:= LangTable.Caption['LinkWindows:Linkwindow'] + Avk11View.Caption + LangTable.Caption['LinkWindows:withwindow'];
  CheckListBox1.Column[1].Caption:= LangTable.Caption['InfoBar:Date'];
  CheckListBox1.Column[2].Caption:= LangTable.Caption['FileBase:FileName'];

  CheckListBox1.Clear;
  J:= 0;
  for I:= 0 to MainForm.MDIChildCount - 1 do
    if (MainForm.MDIChildren[I] is TAvk11ViewForm) and
       (MainForm.MDIChildren[I] <> Avk11View) then
      begin
        Cur:= MainForm.MDIChildren[I] as TAvk11ViewForm;
        New:= CheckListBox1.Items.Add;
        Inc(J);
        New.Caption:= IntToStr(J);
        try
          New.SubItems.Add(DateToStr(EncodeDate(Cur.DatSrc.Header.Year, Cur.DatSrc.Header.Month, Cur.DatSrc.Header.Day)));
        finally

        end;
        New.SubItems.Add(ExtractFileName(Cur.DatSrc.FileName));
        New.Checked:= Avk11View.Display.TestAssign(Cur.Display);
        New.Data:= Cur.Display;
      end;

  ShowModal;
end;

procedure TAssignCtrlForm.Button1Click(Sender: TObject);
var
  I: Integer;

begin
  for I:= 0 to CheckListBox1.Items.Count - 1 do
    if CheckListBox1.Items[I].Checked
      then FAvk11View.Display.AssignTo(TAvk11Display(CheckListBox1.Items[I].Data))
      else FAvk11View.Display.DeleteAssign(TAvk11Display(CheckListBox1.Items[I].Data))
end;

end.
