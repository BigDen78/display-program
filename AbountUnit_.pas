unit AbountUnit_;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TAboutForm_ = class(TForm)
    Button1: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutForm_: TAboutForm_;

implementation

{$R *.dfm}

uses
  LanguageUnit, ConfigUnit;

procedure TAboutForm_.FormCreate(Sender: TObject);
begin
  Self.Caption:= LangTable.Caption['MenuHelp:About'];
  Label1.Caption:= LangTable.Caption['General:AppCaption' + Config.GetProgramModeId];
  Label2.Caption:= LangTable.Caption['General:AppVersion'] + ' 4.41';
  if Config.ProgramMode = pmRADIOAVIONICA_KZ then Label2.Caption:= Label2.Caption + 'a';
  Label3.Caption:= LangTable.Caption['General:Developer' + Config.GetProgramModeId];
end;

end.
