object FileBaseForm: TFileBaseForm
  Left = 293
  Top = 185
  Caption = #1041#1072#1079#1072' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1086#1074' '#1082#1086#1085#1090#1088#1086#1083#1103
  ClientHeight = 604
  ClientWidth = 865
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 242
    Top = 45
    Width = 5
    Height = 540
    ResizeStyle = rsUpdate
  end
  object TBDock1: TTBDock
    Left = 0
    Top = 0
    Width = 865
    Height = 45
    AllowDrag = False
    Background = MainForm.TBBackground1
    object TBToolbar2: TTBToolbar
      Left = 0
      Top = 0
      Caption = 'TBToolbar2'
      DockPos = 0
      DockRow = 1
      FullSize = True
      TabOrder = 0
      object TBControlItem1: TTBControlItem
        Control = Panel1
      end
      object TBItem1: TTBItem
        Caption = #1050#1072#1090#1072#1083#1086#1075
        ImageIndex = 0
        Images = MainForm.ImageList1
        Options = [tboImageAboveCaption]
        OnClick = TBItem1Click
      end
      object TBSeparatorItem1: TTBSeparatorItem
      end
      object TBItem2: TTBItem
        Caption = #1054#1073#1085#1086#1074#1080#1090#1100
        ImageIndex = 4
        Images = MainForm.ImageList1
        Options = [tboImageAboveCaption]
        OnClick = TBItem2Click
      end
      object TBSeparatorItem2: TTBSeparatorItem
      end
      object TBItem4: TTBItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
        ImageIndex = 24
        Images = MainForm.ImageList1
        Options = [tboImageAboveCaption]
        OnClick = TBItem4Click
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 264
        Height = 41
        BevelOuter = bvNone
        TabOrder = 0
        object TBEditItem1: TEdit
          Left = 5
          Top = 10
          Width = 254
          Height = 21
          ReadOnly = True
          TabOrder = 0
          Text = 'TBEditItem1'
        end
      end
    end
  end
  object ListBox1: TListBox
    Left = 536
    Top = 577
    Width = 321
    Height = 49
    ItemHeight = 13
    TabOrder = 1
    Visible = False
  end
  object ListView1: TListView
    Left = 247
    Top = 45
    Width = 618
    Height = 540
    Align = alClient
    Columns = <>
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    FullDrag = True
    MultiSelect = True
    OwnerData = True
    ReadOnly = True
    RowSelect = True
    ParentFont = False
    TabOrder = 3
    ViewStyle = vsReport
    OnChanging = ListView1Changing
    OnClick = ListView1Click
    OnColumnClick = ListView1ColumnClick
    OnCustomDrawItem = ListView1CustomDrawItem
    OnData = ListView1Data
    OnDataHint = ListView1DataHint
    OnDblClick = ListView1DblClick
  end
  object TreeView1: TTreeView
    Left = 0
    Top = 45
    Width = 242
    Height = 540
    Align = alLeft
    HideSelection = False
    Indent = 19
    ReadOnly = True
    TabOrder = 2
    OnClick = TreeView1Click
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 585
    Width = 865
    Height = 19
    Panels = <
      item
        Width = 500
      end>
  end
end
