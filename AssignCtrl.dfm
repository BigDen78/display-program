object AssignCtrlForm: TAssignCtrlForm
  Left = 340
  Top = 144
  BorderStyle = bsDialog
  Caption = #1057#1074#1103#1079#1072#1090#1100' '#1086#1082#1085#1072
  ClientHeight = 257
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 306
    Top = 224
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 394
    Top = 224
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 8
    Top = 9
    Width = 464
    Height = 40
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 0
      Top = 0
      Width = 32
      Height = 13
      Align = alTop
      Caption = 'Label1'
      WordWrap = True
    end
    object Panel2: TPanel
      Left = 0
      Top = 13
      Width = 464
      Height = 8
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object CheckListBox1: TListView
    Left = 6
    Top = 55
    Width = 465
    Height = 161
    Checkboxes = True
    Columns = <
      item
        Caption = #8470
      end
      item
        Caption = #1044#1072#1090#1072
        Width = 80
      end
      item
        Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072
        Width = 300
      end>
    ColumnClick = False
    TabOrder = 3
    ViewStyle = vsReport
  end
end
