unit FileAnUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, TeEngine, Series,
  TeeProcs, Chart, TeeShape, Grids, ValEdit, XPMan, MyTypes, AviconTypes, AviconDataSource;

type
  TFileAnForm = class(TForm)
    Chart3: TChart;
    Series6: THorizBarSeries;
    Chart1: TChart;
    Series1: TLineSeries;
    Splitter1: TSplitter;
    Panel1: TPanel;
    CheckBox1: TCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBox1Click(Sender: TObject);
  public
    Bags: array of TChartShape;
    procedure CreateGraph(DatSrc: TAvk11DatSrc);
  end;

implementation

{$R *.dfm}

procedure TFileAnForm.CreateGraph(DatSrc: TAvk11DatSrc);
var
  R: RRail;
  LoadID: Byte;
  I, J, SkipBytes, Ch: Integer;
  LastSysCoord: LongInt;
  LastDisCoord: LongInt;
  SaveLastSysCoord: LongInt;
  LoadByte: array [1..22] of Byte;
  LoadLongInt: array [1..5] of LongInt absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
//  CurEcho_: TCurEcho;

  DisCoord: Int64;
  SysCoord: Integer;
  Offset: Integer;

  Echo_Summ: Integer;
  Coord_Summ: Integer;
  Other_Summ: Integer;
  Echo_Ch0_Summ: Integer;
  Ch0_Event_Summ: Integer;

  FSrcDat: array [0..7] of Byte;
  DA: array [0..255] of Integer;

  CurSysCoord: Integer;

begin
{  CurSysCoord:= 0;
  DatSrc.DisToFileOffset(0, DisCoord, SysCoord, Offset);
  DatSrc.FileBody.Position:= Offset;
  FillChar(DA[0], 256 * 4, 0);

  Echo_Summ:= 0;
  Coord_Summ:= 0;
  Other_Summ:= 0;
  Echo_Ch0_Summ:= 0;
  Ch0_Event_Summ:= 0;

  Series1.Clear;

  if not DatSrc.TestOffsetFirst then Exit;

  repeat
    SkipBytes:= - 1;

    if not DatSrc.TestOffsetNext then Exit;

    DatSrc.FileBody.ReadBuffer(LoadID, 1);
    if LoadID and 128 = 0 then
    begin
      DatSrc.FileBody.ReadBuffer(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
      Ch:= LoadID shr 3 and 7;

      if Ch = 1 then
      begin
        Inc(Echo_Ch0_Summ, Avk11EchoLen[LoadID and $07] + 1);
        Inc(Ch0_Event_Summ);
      end else Inc(Echo_Summ, Avk11EchoLen[LoadID and $07] + 1);

      if Ch <> 1 then
        case LoadID and $07 of
          1: begin
               Inc(DA[FSrcDat[1]]);
             end;
          2: begin
               Inc(DA[FSrcDat[1]]);
               Inc(DA[FSrcDat[2]]);
             end;
          3: begin
               Inc(DA[FSrcDat[1]]);
               Inc(DA[FSrcDat[2]]);
               Inc(DA[FSrcDat[3]]);
             end;
          4: begin
               Inc(DA[FSrcDat[1]]);
               Inc(DA[FSrcDat[2]]);
               Inc(DA[FSrcDat[3]]);
               Inc(DA[FSrcDat[4]]);
             end;
        end;
    end
    else
    begin
      case LoadID of
     EID_HandScan: begin           // ������
                     DatSrc.FileBody.ReadBuffer(HSHead, 6);
                     Inc(Other_Summ, 6);
                     repeat
                       DatSrc.FileBody.ReadBuffer(HSItem, 3);
                       Inc(Other_Summ, 3);
                       if HSItem.Count < 4 then
                       begin
                         DatSrc.FileBody.ReadBuffer(HSItem.Data[0], Avk11EchoLen[HSItem.Count]);
                         Inc(Other_Summ, Avk11EchoLen[HSItem.Count]);
                       end;
                     until HSItem.Sample = $FFFF;
                   end;

                   EID_Sens        ,  EID_Att         , EID_VRU         ,
                   EID_StStr       ,  EID_EndStr     ,  EID_HeadPh      , EID_Mode        ,
                   EID_2Tp         ,  EID_ZondImp    ,  EID_Stolb       , EID_Strelka     , EID_2Tp_Word     ,  
                   EID_DefLabel    ,  EID_TextLabel  ,  EID_StBoltStyk  , EID_EndBoltStyk ,
                   EID_Time        ,  EID_StLineLabel,  EID_EndLineLabel, EID_StSwarStyk  ,
                   EID_EndSwarStyk ,  EID_OpChange   ,  EID_PathChange  , EID_MovDirChange,
                   EID_SezdNo:
                         begin
                            SkipBytes:= Avk11EventLen[LoadID];
                            Inc(Other_Summ, Avk11EventLen[LoadID] + 1);
                         end;

             EID_ACLeft: begin
                           DatSrc.FileBody.ReadBuffer(LoadByte[1], 1);
                           Inc(Other_Summ, Avk11EventLen[LoadID] + 1);
                         end;
            EID_ACRight: begin
                           DatSrc.FileBody.ReadBuffer(LoadByte[1], 1);
                           Inc(Other_Summ, Avk11EventLen[LoadID] + 1);
                         end;
          EID_SysCrd_SS: begin // ��������� ���������� �������� ������, �������� ����������
                           DatSrc.FileBody.ReadBuffer(LoadByte[1], 2);
                           Inc(Coord_Summ, 1 + 2);
                           CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[2]);

                           Series1.AddXY(DatSrc.FileBody.Position, CurSysCoord);
                         end;
          EID_SysCrd_SF: begin // ��������� ���������� �������� ������, ������ ����������
                           DatSrc.FileBody.ReadBuffer(LoadByte[1], 1);
                           DatSrc.FileBody.ReadBuffer(LoadLongInt, 4);
                           Inc(Coord_Summ, 1 + 5);
                           CurSysCoord:= LoadLongInt[1];

                           Series1.AddXY(DatSrc.FileBody.Position, CurSysCoord);
                         end;
          EID_SysCrd_FS: begin // ��������� ���������� ������ ������, �������� ����������
                           DatSrc.FileBody.ReadBuffer(LoadByte[1], 5);
                           Inc(Coord_Summ, 1 + 5);
                           CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[5]);

                           Series1.AddXY(DatSrc.FileBody.Position, CurSysCoord);
                         end;
          EID_SysCrd_FF: begin // ��������� ���������� ������ ������, ������ ����������
                           DatSrc.FileBody.ReadBuffer(LoadByte[1], 4);
                           DatSrc.FileBody.ReadBuffer(LoadLongInt, 4);
                           Inc(Coord_Summ, 1 + 8);
                           CurSysCoord:= LoadLongInt[1];

                           Series1.AddXY(DatSrc.FileBody.Position, CurSysCoord);
                         end;
            EID_EndFile: begin // ����� �����
                           Break;
                         end;
      end;
    end;
    if SkipBytes <> - 1 then DatSrc.FileBody.Position:= DatSrc.FileBody.Position + SkipBytes;
  until False;

  Series6.Clear;
  Series6.AddBar(100 * Echo_Ch0_Summ / DatSrc.FileBody.Size + 100 * Echo_Summ / DatSrc.FileBody.Size + 100 * Coord_Summ / DatSrc.FileBody.Size + 100 * Other_Summ / DatSrc.FileBody.Size, '���', clBlue);
  Series6.AddBar(100 * Echo_Summ / DatSrc.FileBody.Size, '��� ������� (��� ������ ����� 1)', clRed);
  Series6.AddBar(100 * Echo_Ch0_Summ / DatSrc.FileBody.Size, '��� ������� (1 �����)', clRed);
  Series6.AddBar(100 * Coord_Summ / DatSrc.FileBody.Size, '����������', clYellow);
  Series6.AddBar(100 * Other_Summ / DatSrc.FileBody.Size, '������', clGreen);

  I:= Round(DatSrc.MaxDisCoord * (DatSrc.Header.ScanStep / 100) / 1000);

  for I:= 0 to High(Bags) do Bags[I].Free;

  for I:= 0 to DatSrc.ErrorCount - 1 do
  begin
    SetLength(Bags, Length(Bags) + 1);
    Bags[High(Bags)]:= TChartShape.Create(nil);
    with Bags[High(Bags)] do
    begin
      ParentChart:= Chart1;
      Style:= chasRectangle;
      X0:= DatSrc.Error[I].Offset;
      X1:= DatSrc.Error[I].Offset;
      Y0:= Series1.MinYValue;
      Y1:= Series1.MaxYValue;
      Brush.Color:= clRed;
    end;
  end;
 }

end;

procedure TFileAnForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  I: Integer;

begin
  for I:= 0 to High(Bags) do Bags[I].Free;
end;

procedure TFileAnForm.CheckBox1Click(Sender: TObject);
begin
  Series1.Pointer.Visible:= CheckBox1.Checked;
end;

end.
