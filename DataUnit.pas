unit DataUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TB2Item, TB2Dock, TB2Toolbar, ExtCtrls, ImgList;

type
  TForm1 = class(TForm)
    ImageList1: TImageList;
    Panel7: TPanel;
    Button122: TButton;
    Button152: TButton;
    CLearButton: TButton;
    Button123: TButton;
    ScrollBox1: TScrollBox;
    Panel2: TPanel;
    Edit1: TEdit;
    Edit2: TEdit;
    Panel3: TPanel;
    ListBox1: TListBox;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem2: TTBItem;
    TBItem1: TTBItem;
    TBItem4: TTBItem;
    TBItem3: TTBItem;
    Panel6: TPanel;
    ListBox2: TListBox;
    TBDock2: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    TBItem7: TTBItem;
    TBItem8: TTBItem;
    Panel16: TPanel;
    ListBox3: TListBox;
    TBDock3: TTBDock;
    TBToolbar3: TTBToolbar;
    TBItem9: TTBItem;
    TBItem10: TTBItem;
    TBItem11: TTBItem;
    TBItem12: TTBItem;
    Panel17: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    procedure Button123Click(Sender: TObject);
    procedure Button122Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TStringData = packed record
    SaveShift: array [1..8] of Byte;
    Name: array [0..37] of Byte;
    OpperList: array [0..9] of array [0..19] of Byte;
    PeregList: array [0..99] of array [0..19] of Byte;
    SpLabList: array [0..29] of array [0..19] of Byte;
    Unuse: array [1..5000] of Byte;
  end;



var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button123Click(Sender: TObject);
begin
  ListBox1.Items.Add('�������-������');
  ListBox1.Items.Add('�������-�����');
  ListBox1.Items.Add('�����-���������');
  ListBox1.Items.Add('���������-���������');
  ListBox1.Items.Add('����������-��������');
  ListBox1.Items.Add('��������-���������');
  ListBox1.Items.Add('���������-����');
  ListBox1.Items.Add('�������.����-������');
  ListBox1.Items.Add('�������.����-������');
  ListBox1.Items.Add('����-��������');
  ListBox1.Items.Add('����-����������');
  ListBox1.Items.Add('�������-������.����');
  ListBox1.Items.Add('����������-������');
  ListBox1.Items.Add('�������-������');
  ListBox1.Items.Add('�������-�����');
  ListBox1.Items.Add('�����-���������');
  ListBox1.Items.Add('���������-���������');
  ListBox1.Items.Add('����������-��������');
  ListBox1.Items.Add('��������-���������');
  ListBox1.Items.Add('���������-����');
  ListBox1.Items.Add('�������.����-������');
  ListBox1.Items.Add('�������.����-������');
  ListBox1.Items.Add('����-��������');
  ListBox1.Items.Add('����-����������');
  ListBox1.Items.Add('�������-������.����');
  ListBox1.Items.Add('����������-������');
  ListBox1.Items.Add('�������-������');
  ListBox1.Items.Add('�������-�����');
  ListBox1.Items.Add('�����-���������');
  ListBox1.Items.Add('���������-���������');
  ListBox1.Items.Add('�������-���������');
  ListBox1.Items.Add('�������-������');
  ListBox1.Items.Add('�����������-������');
  ListBox1.Items.Add('�����������-�����');
  ListBox1.Items.Add('���-�����-���');
  ListBox1.Items.Add('�����-�����������');
  ListBox1.Items.Add('�����-���-�����');
  ListBox1.Items.Add('�.����-�����');
  ListBox1.Items.Add('��������-�.�����');
  ListBox1.Items.Add('�.�����-��������');
  ListBox1.Items.Add('��������-��������');
  ListBox1.Items.Add('��������-����������');
  ListBox1.Items.Add('����������-��������');
  ListBox1.Items.Add('��������-���������');
  ListBox1.Items.Add('���������-������');
  ListBox1.Items.Add('������-�����������');
  ListBox1.Items.Add('�����������-�.����');
  ListBox1.Items.Add('��������-����������');
  ListBox1.Items.Add('������� ����-��������');
  ListBox1.Items.Add('�����-��������');
  ListBox1.Items.Add('�������-�������');
  ListBox1.Items.Add('�������-���.����');
  ListBox1.Items.Add('�������-���������');
  ListBox1.Items.Add('�������-���������');
  ListBox1.Items.Add('���-������');
  ListBox1.Items.Add('���-������');
  ListBox1.Items.Add('������-�����������');
  ListBox1.Items.Add('������-�����');
  ListBox1.Items.Add('���������-�������');
  ListBox1.Items.Add('������-���');


  ListBox2.Items.Add('������ �.�');
  ListBox2.Items.Add('������ �.�.');
  ListBox2.Items.Add('������� �.�.');
  ListBox2.Items.Add('������� �.�.');
  ListBox2.Items.Add('�������� �.�.');
  ListBox2.Items.Add('������� �.�.');
  ListBox2.Items.Add('�������� �.�.');
  ListBox2.Items.Add('�������� �.�.');
  ListBox2.Items.Add('�������� �.�.');
  ListBox2.Items.Add('������ �.�.');
  ListBox2.Items.Add('������ �.�');
  ListBox2.Items.Add('������ �.�.');
  ListBox2.Items.Add('�������� �.�.');
  ListBox2.Items.Add('��������� �.�.');
  ListBox2.Items.Add('������� �.�.');
  ListBox2.Items.Add('������ �.�.');
  ListBox2.Items.Add('����� �.�.');
  ListBox2.Items.Add('����� �.�.');
  ListBox2.Items.Add('������ �.�.');
  ListBox2.Items.Add('���� �.�.');

  ListBox3.Items.Add('������������� ���');
  ListBox3.Items.Add('�����������');
  ListBox3.Items.Add('������� ����');
  ListBox3.Items.Add('�������');
  ListBox3.Items.Add('���������');
  ListBox3.Items.Add('����');
  ListBox3.Items.Add('���');
  ListBox3.Items.Add('���������� �����');
  ListBox3.Items.Add('����� �����������');
  ListBox3.Items.Add('������');
end;

procedure TForm1.Button122Click(Sender: TObject);
var
  Buff: TStringData;
  Code: Integer;
  PCh_: Integer;
  Res: Integer;
  I: Integer;

begin
(*
  if power_on <> 1 then Exit;
  Sleep(500);
  ChipCount:= chipcntr;
  if not CheckBox3.Checked then RichEdit1.Lines.Insert(0, Format('ChipCntr Res = %d', [ChipCount]));

  TextToCode(Edit1.Text, 20, Buff.RailRoad);

  Val(Edit2.Text, PCh_, Code);
  Buff.PCh:= PCh_;

  for I:= 0 to 29 do
    if I < ListBox1.Items.Count then TextToCode(ListBox1.Items.Strings[I], 20, Buff.PeregList[I])
                                else TextToCode('', 20, Buff.PeregList[I]);

  for I:= 0 to 9 do
    if I < ListBox2.Items.Count then TextToCode(ListBox2.Items.Strings[I], 20, Buff.OpperList[I])
                                else TextToCode('', 20, Buff.OpperList[I]);

  for I:= 0 to 9 do
    if I < ListBox3.Items.Count then TextToCode(ListBox3.Items.Strings[I], 20, Buff.SpLabList[I])
                                else TextToCode('', 20, Buff.SpLabList[I]);

//  Sleep(1500);
  Res:= Writepages(@Buff, 0, 0, 1);
  Sleep(100);
//  if not CheckBox3.Checked then RichEdit1.Lines.Insert(0, Format('Page: %d; Chip: %d; Write Bytes: %d', [0, 0, Res]));
  power_off;
  */*)
end;

end.

