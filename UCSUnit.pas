unit UCSUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Avk11ViewUnit, TeEngine, Series, TeeProcs, Chart, ComCtrls;

type
  TUCSForm = class(TForm)
    ListBox1: TListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Panel3: TPanel;
    Chart1: TChart;
    Series1: TLineSeries;
    Memo1: TMemo;
    Panel4__: TPanel;
    ListView1: TListView;
    Panel4: TPanel;
    Panel6: TPanel;
    procedure Button1Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    AVF: TAvk11ViewForm;

  public

    procedure FormUpdate(AVF_: TAvk11ViewForm; Debug: boolean);
  end;

var
  UCSForm: TUCSForm;

implementation

uses
  AviconTypes, MyTypes, MainUnit;

{$R *.dfm}

procedure TUCSForm.Button1Click(Sender: TObject);
begin
  Self.Visible:= False;
//  AVF:= nil;
end;

procedure TUCSForm.FormCreate(Sender: TObject);
begin
//  AVF:= nil;
end;

procedure TUCSForm.FormHide(Sender: TObject);
begin
  if not MainForm.miDebug.Visible then
    if self.Tag <> Ord(MainForm.miViewAC.Checked) then
    begin
      MainForm.miViewAC.Checked:= Boolean(self.Tag);
      MainForm.miViewAC.onClick(nil);
    end;
end;

procedure TUCSForm.FormShow(Sender: TObject);
begin
  self.Tag:= Ord(MainForm.miViewAC.Checked);
  if not MainForm.miViewAC.Checked then MainForm.miViewAC.Click;
end;

procedure TUCSForm.ListBox1Click(Sender: TObject);
begin
  if ListBox1.ItemIndex <> - 1 then
  begin
    AVF.ScrollBar1.Position:= (AVF.DatSrc.UCSData[Integer(ListBox1.Items.Objects[ListBox1.ItemIndex])].StDisCoord + AVF.DatSrc.UCSData[Integer(ListBox1.Items.Objects[ListBox1.ItemIndex])].EdDisCoord) div 2;
  end;
end;

procedure TUCSForm.ListView1SelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin

 // if ListBox1.ItemIndex <> - 1 then
  begin
    AVF.ScrollBar1.Position:= (AVF.DatSrc.UCSData[ListView1.Items.IndexOf(Item)].StDisCoord + AVF.DatSrc.UCSData[ListView1.Items.IndexOf(Item)].EdDisCoord) div 2;
  end;

end;

procedure TUCSForm.FormUpdate(AVF_: TAvk11ViewForm; Debug: boolean);
var
  I: Integer;
  CrdParams: TCrdParams;
  str1, str2: string;
  R: TRail;
  J, Summ: Integer;
  newitem: TListItem;

begin
  Panel3.Visible:= Debug;
  if not Debug then Self.Width:= 575
               else Self.Width:= 1305;

  AVF:= AVF_;
  AVF.DatSrc.AnalyzeAC(Debug);

  J:= High(AVF.DatSrc.AKGis);
  for I := High(AVF.DatSrc.AKGis) to 0 do
   if AVF.DatSrc.AKGis[I] <> 0 then
   begin
     J:= I;
     Break;
   end;

  Series1.Clear;
  for I := 0 to J do Series1.AddXY(I * AVF.DatSrc.Header.ScanStep div 100, AVF.DatSrc.AKGis[I]);

//  Memo1.Lines.Add(Format('��� �� � ��� �������� - %d ��.', [AVF.DatSrc.GOOD_AC_algorithm_situation]));
//  Memo1.Lines.Add(Format('��� �� � ���� ������� - %d ��.', [AVF.DatSrc.BAD_AC_algorithm_situation]));
//  Memo1.Lines.Add(Format('��� �� � ���� ������� - %d %%', [Round(100 * AVF.DatSrc.BAD_AC_algorithm_situation / (AVF.DatSrc.GOOD_AC_algorithm_situation + AVF.DatSrc.BAD_AC_algorithm_situation))]));
//  Memo1.Lines.Add(Format('����� ������� - %d ��.', [AVF.DatSrc.GOOD_AC_algorithm_situation + AVF.DatSrc.BAD_AC_algorithm_situation]));
//  Memo1.Lines.Add('-----');
  Memo1.Lines.Add(Format('���� - %s', [AVF.DatSrc.FileName]));
  Memo1.Lines.Add(Format('����� - %d �', [Round(AVF.DatSrc.MaxDisCoord / 100000 * AVF.DatSrc.Header.ScanStep)]));

  Summ:= 0;
  for R:= rLeft to rRight do
     for I := 0 to 16 do
     begin
       Summ:= Summ + AVF.DatSrc.AKCount[R, I];
       Memo1.Lines.Add(Format('%d %d - %d', [Ord(R), I, AVF.DatSrc.AKCount[R, I]]));
     end;

  Memo1.Lines.Add(Format('����� ������� �� - %d ��.', [Summ]));

  Summ:= 0;
  for I := 0 to High(AVF.DatSrc.UCSData) do
   Summ:= Summ + AVF.DatSrc.UCSData[I].EdDisCoord - AVF.DatSrc.UCSData[I].StDisCoord;

  for R:= rLeft to rRight do
     for I := 0 to 99 do
     if AVF.DatSrc.AKExists[R, I] then
     begin

      if AVF.DatSrc.AKCount[R, I] <> 0 then

       Memo1.Lines.Add(Format('%s %s - %d ��; ���: %d ��, ����: %d ��, ����: %d ��, ���� �����: %3.2f%%', [RailToStr(R, 0),
                                                                             AVF.Display.EvalChNumToName(I),
                                                                             AVF.DatSrc.AKCount[R, I],
                                                                             Round(AVF.DatSrc.AKMinLen[R, I] / 100 * AVF.DatSrc.Header.ScanStep),
                                                                             Round(AVF.DatSrc.AKMaxLen[R, I] / 100 * AVF.DatSrc.Header.ScanStep),
                                                                             Round(AVF.DatSrc.AKMediumLen[R, I] / 100 * AVF.DatSrc.Header.ScanStep),
                                                                             100 * AVF.DatSrc.AKSummLen[R, I] / AVF.DatSrc.MaxDisCoord
                                                                             ]));
     end;

  if AVF.DatSrc.MaxDisCoord <> 0 then
    Memo1.Lines.Add(Format('���������� �� - %3.2f %%', [100 * Summ / AVF.DatSrc.MaxDisCoord]));


  ListView1.Clear;


  UCSForm.ListBox1.Items.Clear;
  UCSForm.ListBox1.Items.BeginUpdate;
  for I := 0 to High(AVF.DatSrc.UCSData) do
  begin
    AVF.DatSrc.DisToCoordParams(AVF.DatSrc.UCSData[I].StDisCoord, CrdParams);
    str1:= RealCrdToStr(CrdParamsToRealCrd(CrdParams, AVF.DatSrc.Header.MoveDir), 1);
    AVF.DatSrc.DisToCoordParams(AVF.DatSrc.UCSData[I].EdDisCoord, CrdParams);
    str2:= RealCrdToStr(CrdParamsToRealCrd(CrdParams, AVF.DatSrc.Header.MoveDir), 1);
    UCSForm.ListBox1.Items.AddObject(Format('%d. %s - %s [%3.3f �]', [I, str1, str2, (AVF.DatSrc.UCSData[I].EdDisCoord - AVF.DatSrc.UCSData[I].StDisCoord) * AVF.DatSrc.Header.ScanStep / 100000]), Pointer(I));

    newitem:= ListView1.Items.Add;
    newitem.Caption:= IntToStr(I);
    newitem.SubItems.Add(str1);
    newitem.SubItems.Add(str2);
    newitem.SubItems.Add(Format('%3.3f �', [(AVF.DatSrc.UCSData[I].EdDisCoord - AVF.DatSrc.UCSData[I].StDisCoord) * AVF.DatSrc.Header.ScanStep / 100000]));
  end;
  UCSForm.ListBox1.Items.EndUpdate;

  Panel4.Caption:= Format('��������� ������������� ��������������������� �������� - %3.1f ������ (%3.1f%%)', [AVF.DatSrc.UCSLength, AVF.DatSrc.UCSProcent]); // NoTranslate
  if Panel3.Visible then Panel4.Caption:= Panel4.Caption + ' ' + IntToStr(AVF.DatSrc.ACEventCount);
end;

end.
