{$I DEF.INC}

unit RecData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls, MyTypes, Math, // Avk11Engine,
  CheckLst, Series, TeEngine, TeeProcs, Chart, Types, RecEngine, RecTypes, {AvDataNet,}
  Grids, ArrowCha, Spin, TeeShape, RecDebugUnit, FiltrUnit, RecDataUnit, AviconDataSource, AviconTypes;

type
  TFindHoleParam = record
    Ch: Integer;
    BtmSglMin: Integer;
    BtmSglMax: Integer;
    DZoneMin: Integer;
    DZoneMax: Integer;
    AngleMin: Single;
    AngleMax: Single;
  end;

const

//  AlgNo_BHSearch   = 99001;
//  AlgNo_PBDeadZone = 99002;
//  AlgNameList: array [AlgNo_BHSearch..AlgNo_PBDeadZone] of string =
//
//                                                         ('����� ��',
//                                                          '�������� ������ ��� ����� ���. 1, 6, 7');

  FindHoleParam: array [0..5] of TFindHoleParam =   // ���� (�� ��������) ����� ����� (��� ���������) �� �������� ��

  ((Ch: 1; BtmSglMin: 57 * 3; BtmSglMax: 63 * 3; DZoneMin: 56; DZoneMax: 111; AngleMin:   - 1; AngleMax:     1),     // �-65
   (Ch: 6; BtmSglMin: 57 * 3; BtmSglMax: 63 * 3; DZoneMin: 43; DZoneMax: 102; AngleMin: - 1.5; AngleMax: - 0.3),     // �-65
   (Ch: 7; BtmSglMin: 57 * 3; BtmSglMax: 63 * 3; DZoneMin: 43; DZoneMax: 102; AngleMin:   0.3; AngleMax:   1.5),     // �-65
   (Ch: 1; BtmSglMin: 46 * 3; BtmSglMax: 49 * 3; DZoneMin: 57; DZoneMax:  84; AngleMin:   - 1; AngleMax:     1),     // ������
   (Ch: 6; BtmSglMin: 46 * 3; BtmSglMax: 49 * 3; DZoneMin: 46; DZoneMax:  74{67}; AngleMin: - 1.5; AngleMax: - 0.3),     // ������
   (Ch: 7; BtmSglMin: 46 * 3; BtmSglMax: 49 * 3; DZoneMin: 46; DZoneMax:  74{67}; AngleMin:   0.3; AngleMax:   1.5));    // ������

                   // ���� ����� ��
  TBSGisMin = 105; // 35 ���
  TBSGisMax = 255; // 85 ���

type
  {$IFDEF RECDEBUG}

  TOutInfoItem = record
    Rail: RRail_;
    Ch: Integer;
    Sgl_Rt: TRect;
    Dbg_Rt: TRect;
  end;

  TOutInfo = array of TOutInfoItem;

  TOutLink = record
    Rt: TRect;
    Visible: Boolean;
  end;

  TOnScrPBItem = record
    PtList: array of TPoint;
    PBIdx: Integer;
  end;
  {$ENDIF}


// ----[ TMask ]----------------------------------------------------------------

  TMaskItem = record
    Coord: Integer;
    Delay: Integer;
    Ampl: Integer;
    Ok: Boolean;
  end;

  TMaskItemList = array of TMaskItem;

//  TMask = class;
//  TOnResEvent = procedure (Sender: TMask; var Res: TMaskItemList; BMState: Boolean) of object;
(*
  TSearchMask = record
    Delay: array [0..255] of Integer;
    Items: TMaskItemList;
    Del: Boolean;
  end;

  TResIdx = array of Integer;

  TMask = class
  private
    FMask: array of TSearchMask;
    FWorkCoord: Integer;
    FLastBMState: Boolean;

  protected
    function AddMask(Delay, Ampl, Coord: Integer; MaxSample: Boolean): Integer;
    procedure Summ(ResIdx: TResIdx);
    procedure DelMask(Idx: TResIdx);
    procedure Put(Index, Delay, Ampl, Coord: Integer; MaxSample: Boolean);
    function TestAll(Delay, Coord: Integer; var ResIdx: TResIdx): Boolean; // ����� � ������ ����������� ����� � ������� ����� ���������� ����� ������
    function Update(Index, Delay, Ampl, Coord: Integer; MaxSample: Boolean): Boolean;
    procedure TestEnd(Coord: Integer; Stop: Boolean; var ResIdx: TResIdx);

  public
    R: RRail;
    Ch: Integer;

    MaxCoordSpace: Integer;
    DelaySearchZone: Integer;
    FOnRes: TOnResEvent;

    constructor Create;
    destructor Destroy;
    procedure NewCoord(Coord: Integer; BMState: Boolean);
    procedure NewEcho(Delay, Ampl: Integer; MaxSample: Boolean);
    procedure Stop;
    property OnRes: TOnResEvent read FOnRes write FOnRes;
  end;
  *)
// ----[ ����� ����� �2 ]-------------------------------------------------------

  TPBTypItems = (pbtiInBMZ, pbtiConstruct, pbtiClear, pbtiNotLast, pbtiReCheck, pbtiUCS, pbtiDefect, pbtiDel);
  TPBTyp = set of TPBTypItems;
(*
  TBHTypItems = (bhtBwd, bhtReCheck);

  TBHTyp = set of TBHTypItems;

  TPBTyp = (pbtNotSet, // �� ������
             pbtClear, // ����� ������� ������
         pbtConstruct, // ����� ��
               pbtUCS,  // �����
            pbtForDef   // ������

               {,
            pbtForHole, pbtForHoleBolt, pbtForHoleDef,
            pbtHoleMain, pbtHoleBolt, pbtHoleDef,
            pbtEnd, pbtForUCS, pbtUnknown, pbtForDef} {, pbtNoise , pbtDel});
  *)
  TBScanSetSubItem = record
    Crd: Integer;
    Delay: Integer;
//    StDelay: Integer;
//    EdDelay: Integer;
    Ampl: Integer;
  end;
(*
  {$IFDEF RECDEBUG}
  TTypListItem = record
    Typ: TPBTyp;
    AlgNo: Integer;
  end;
  {$ENDIF}
*)
  TPBItem = record
    Crd: TRange;
    Del: TRange;
    CCrd: Integer;
    CDel: Integer;
//    A: Byte;         // "���������" �����
    Typ: TPBTyp;
//    BHTyp: TBHTyp;   // ��� ������� ��������
    EchoCount: Word; // ���������� ��������
    SummAmpl_: Byte;

//    KFAngle: Single; // ����������� ������� �����
    Dat: array [3..7] of Word;

//    E: Single;

    {$IFDEF RECDEBUG}
    Items: array of TBScanSetSubItem;
    {$ENDIF}
(*
    KfDelay: Single;
    KfAmpl: Single;
    KfFill: Single;
    Count: Integer;
    Th: Integer;
    KOut: Integer;
    KIn: Integer;  *)
//    TypList: array of TTypListItem;
  end;

  TPBList = array [rLeft..rRight, 0..7] of array of TPBItem;
  PPBList = ^TPBList;

  TMaskItemRec = record
    Del: Boolean;
//    Dir: array [1..6] of Integer;
    Item: TMaskItemList;
  end;

  TBSignal = class;

  TRecData = class;

  TMaskShow = class
  private
    FDatSrc: TAviconDataSource;
    PBCap: array [rLeft..rRight, 1..7] of Integer;
    PBIdx: array [rLeft..rRight, 1..7] of Integer;
    FIdx: array [rLeft..rRight] of Integer;
    FMask: array [rLeft..rRight, 1..7] of array of TMaskItemRec;
    PBList: PPBList;

  protected

{    function AddMask(Delay, Ampl, Coord: Integer; MaxSample: Boolean): Integer;
    procedure Summ(ResIdx: TResIdx);
    procedure DelMask(Idx: TResIdx);
    procedure Put(Index, Delay, Ampl, Coord: Integer; MaxSample: Boolean);
    function TestAll(Delay, Coord: Integer; var ResIdx: TResIdx): Boolean; // ����� � ������ ����������� ����� � ������� ����� ���������� ����� ������
    function Update(Index, Delay, Ampl, Coord: Integer; MaxSample: Boolean): Boolean;
}

    function BlockOK(StartDisCoord: Integer): Boolean;
    procedure TestEnd(CurDisCoord: Integer; Drop: Boolean);

  public

//    GE_: Integer;
//    R: RRail;
//    Ch: Integer;
//    PBList: array [rLeft..rRight, 0..7] of array of TPBItem;                 // �����
//    FOnRes: TOnResEvent;

    MaxCoordSpace: Integer;
    DelaySearchZone: Integer;
    FBSignal: TBSignal;
    FFiltr: TFiltr;
    FRecData: TRecData;

    constructor Create(DatSrc: TAvk11DatSrc; RecData_: TRecData; PBList_: PPBList; BSignal: TBSignal; Filtr: TFiltr);
    destructor Destroy;
    procedure Calc(StCrd, EdCrd: Integer);

//    procedure NewCoord(Coord: Integer; BMState: Boolean);
//    procedure NewEcho(Delay, Ampl: Integer; MaxSample: Boolean);
//    procedure Stop;
//    property OnRes: TOnResEvent read FOnRes write FOnRes;

  end;
  
// -----[ TRecData ]------------------------------------------------------------

  TBMDataItem = record
    DisRange: TRange;
    SysRange: TRange;
    DisItems: array of TRange;
    LastPass: array of TRange;
  end;

  TBMDataList = array of TBMDataItem;  // ���� �������� �����

  TBSPos = record
    MaxDelay: Integer;
    MinDelay: Integer;
    Crd: Integer;
  end;

  TBSPosList = array [rLeft..rRight] of array of TBSPos;

  TSingleObjTyp = (so_BHOk, so_BHUnCtrl, so_BHDef, so_End, so_BSHole, so_Blade);

  TBodyZonesItem = record
    Range: TRange;
    ObjList: array of Integer;
  end;

  TRailObj = record
    F1, F2: Boolean;
    HeadZones: array of TRange;
    BodyZones: array of TBodyZonesItem;
  end;

  TBPListItem = record
    Ch: Integer;
    CCrd: Integer;
    Idx: Integer;
    Main: Boolean;
  end;

  TSingleObj = record
    Typ: TSingleObjTyp;
    Use: Boolean;
    DisRange: TRange;
    SysRange: TRange;
    PBList: array of TBPListItem;
//    Name: string;
  end;

  TDelayRegItem = record      // ������� ���� �� ��������
    StDelay: Integer;
    EdDelay: Integer;
    MaxValue: Integer;
    EMedium: Single;
    ETh: Single;
  end;

  TDefPBListItem = record
    Ch: Integer;
    Crd: TRange;
    Dly: TRange;
    Idx: Integer;
  end;

  TDefTyp = (dtHead, dtBody, dtBottom, dtUnknown, dtLostBtm, pbtiUnChkCon);

  TDefListItem = record
    Typ: TDefTyp;
    Rgn: TRange;
//    PBCount: Integer;
    SummLen: Single;
    PBList: array of TDefPBListItem;
    {$IFDEF RECDEBUG}
    Text: string;
    {$ENDIF}
  end;

(*
  TPBItem = record
    Crd: TRange;
    Del: TRange;
    CCrd: Integer;
    CDel: Integer;
    A: Byte;         // "���������" �����
    Typ: TPBTyp;
    BHTyp: TBHTyp;   // ��� ������� ��������
    EchoCount: Word; // ���������� ��������

//    KFAngle: Single; // ����������� ������� �����
//    Dat: array [3..7] of Word;  // Dat[3] - ����� ����� 0 - � ���� �������� �����
//                                // Dat[3] - ����� ����� 0 - � ���� �������� �����
//    E: Single;

    {$IFDEF RECDEBUG}
    KfDelay: Single;
    KfAmpl: Single;
    KfFill: Single;
    Count: Integer;
    Th: Integer;
    KOut: Integer;
    KIn: Integer;
    Items: array of TBScanSetSubItem;
    TypList: array of TTypListItem;
    {$ENDIF}
  end;
*)
  {$IFDEF RECDEBUG}
  TDebugGis = record
    Gis: array [0..255] of Integer;
    MaxPos: array [1..2] of Integer;
    Range: TRange;
  end;
  {$ENDIF}

  TPointTyp = (ptCh0, ptCh1, ptCh6, ptCh7, ptButton, ptOtherRail);

  TMaskPoint = record
    Crd: Integer;
    Typ: TPointTyp;
    State: Boolean;
    Ch: Integer;
    PBIdx: Integer;
    Crd2: Integer;
    Debug: Boolean;
    Depth: Integer;
  end;

  TMaskPoint2 = record
    Ch: Integer;
    Crd: TRange;
    Depth: TRange;
    PBIdx: Integer;
//    SetList: TMySet;
    HoleIdx: Integer;
//    Used: Boolean;
    UsedIdx: Integer;
  end;

  TRailSingleObj = array [rLeft..rRight] of array of TRailObj;
  PRailSingleObj = ^TRailSingleObj;

  TPointItem = record
    Typ: Integer;
    Crd: Integer;
  end;

  TPtnItem = record
    Typ: Integer;
    Idx1: Integer;
    Idx2: Integer;
    Crd: Integer;
    State: Boolean;
  end;


  TWorkGis = record
    AmplGis: array [0..255] of Integer;
    DelayReg: array of TDelayRegItem;
//    Len: Integer;
//    SummE: Integer;
//    MedE: Integer;
  end;

  TWorkItem = record
    Idx: Integer;                // ������ �����         // ���� (���� �� ����������) ������
    Items: array of TRange;      // ���� �� ���������� (�������)
    Len: Integer;                // ��������� ����� - ���� ������

    Gis: array [1..7] of TWorkGis;

//    Th: Integer;
//    MedE: Integer;
//    AmplGis: array [1..7, 0..255] of Integer;
//    DelayReg: array [1..7] of array of TDelayRegItem;
  end;


  TTechnoErrorTyp = (teDoublePass, teBJButton, teSens, teSpeed);

  TTechnoErrorItem = record
    R: RRail_;
    Ch: Integer;
    Range: TRange;
    Value: Integer;
    Typ: TTechnoErrorTyp;
  end;

  TTechnoErrorList = array of TTechnoErrorItem;

  TConstructTyp = ( ct_OneHole       ,   // ��������� ���������
                    ct_Plug          ,   // ��������
                    ct_WeldedJoint   ,   // ������� ����
                    ct_BoltJoint     ,   // �������� ����
                    ct_FlangeRail    ,   // ����� �����
                    ct_Blade         ,   // ������ ����������� ��������
                    ct_Frog          ,   // ����������
                    ct_SupportingRail);  // ������ �����
              //    ct_Switch        );  // �������


  TIdItem = record
    Mask: array [TSingleObjTyp] of TMinMax;
    ResTyp: TConstructTyp;
//    Skip: Boolean;
  end;

  TObjTypeCnt = array [TSingleObjTyp] of Integer;

  TConstructItem = record
    Typ: TConstructTyp;
    Head: TRange;
    Body: TRange;
    Del: Boolean;
    {$IFDEF RECDEBUG}
    TypCnt: TObjTypeCnt;
    Idx: array of Integer;
    {$ENDIF}
  end;

  TConstructList = array [rLeft..rRight] of array of TConstructItem;

  TDefList = array [rLeft..rRight] of array of TDefListItem;

  TEnd2Item = record
                Crd: Integer;
                Range: TRange;
                Typ: Integer; // 0 - �������� ���������; 1 - ����� � ��; 2 - ����� �� �������� 3 - ������� ����� �� ��������
//                MaxVal: Integer;
//                Debug: string;
              end;

  TEnd3Item = record
                Crd: Integer;
                Value: Integer;
                Ok: Boolean;
//                WidthOk: Boolean;
//                Width: TRange;
              end;

  TUnCtrlZList = array [rLeft..rRight] of array of
  record
    Rng: TRange;                    // ��������������������� �������
    IdxList: TIntegerDynArray;
  end;

  TBtmPt = record
    Crd: Integer;
    Delay: Integer;
  end;

// ----------- TBSignal --------------------------------------------------------

  TBSPice = record
    R: TRange;
    BM: Boolean;
  end;

  TBSGis = array [rLeft..rRight, TBSGisMin..TBSGisMax] of Integer;                       // ����� ������� �������

  TBSignal = class
  private
    FDatSrc: TAvk11DatSrc;
    FStCrd: Integer;
    FEdCrd: Integer;
    FBSGis: array of TBSGis;                                                    // ����� ������� �������
    FBSGis2: TBSGis;                                                            // ��������� ��������� ������� �������
    FBSFlag: array [rLeft..rRight] of Integer;
    FBSFlag2: Boolean;                                                          // ������� ��������� �������� �����
    FScanStep: Single;

  protected
    procedure FindBottomSignalZone;
    function FindBSZoneBlockOK1(StartDisCoord: Integer): Boolean; // --- ���������� ���������� ��� ������� ������ --------------------------------
    function FindBSZoneBlockOK2(StartDisCoord: Integer): Boolean; // --- ���������� ���������� ��� ������� ������ --------------------------------

  public
    {$IFDEF RECDEBUG}
    FDebugGis: array [rLeft..rRight] of array of TDebugGis;
    {$ENDIF}

    OnProgress: TNotifyEvent;
    Ch: Integer;
    FBSPos: TBSPosList;                                                         // ����� ������� �������
    FGisStep: Integer;

    constructor Create;
    destructor Destroy;
    procedure Calc(DatSrc: TAvk11DatSrc; StCrd, EdCrd: Integer);
  end;

  TBMList = class // --- ���������� ��� �������� ����� ----------------------------------
    FBMData: TBMDataList;                        // ���� �������� �����
    FMaxBMLen: Integer;
//    LastZones: array of TRange;
//    WorkZones: array of TRange;
    constructor Create;
    destructor Destroy;
    procedure Calc(DatSrc: TAvk11DatSrc);
  end;
{
  TBJPtListItem = record
    Range: TRange;
    Typ: Integer;
    Idx1: Integer;
    Idx2: Integer;
  end;

  TBJPtList = array of TBJPtListItem;
 }
  TRecData = class
  private
    FDebug: Boolean;
    FRail: RRail_;
    FParam: array [1..10] of Integer;
    FDatSrc: TAvk11DatSrc;
    FMaskShow: TMaskShow;

    FRE: TRecEngine;
    RecDataForm: TRecDataForm;
    FMaxShift: Integer;

    FIdx: array [rLeft..rRight, 1..7] of Integer;
    FCrd: array [rLeft..rRight, 1..3] of Integer;

    FBSFlag: array [rLeft..rRight] of Integer;
    FBSFlag2: Boolean;
//    FPoints: array [rLeft..rRight] of array of TMaskPoint;                    // ������ ��������� - �� ����������
//    FEndTh: array [rLeft..rRight] of Integer;
//    FEnd3: array [rLeft..rRight] of array of TEnd3Item;

    FTime: Integer;

//    procedure SetPBTyp(var PB: TPBItem; AlgNo: Integer; NewType: TPBTyp);
    function BackMotion(Crd: Integer): Boolean; // �� ��������� � ���� �������� �����
    function TestRange(Range: TRange): Boolean; // �� ��������� � ���� �������� �����
    procedure TestRange2(Range: TRange; var ResCrd: TIntegerDynArray); // ��������� ���� �� ������ Range ����� ���� ��
    procedure MoveFrom(var Crd: Integer; Dir: Integer);
    function GetRedCentr(Ch: Integer; var PB: TPBItem): Integer;
    function GetRed(Crd, Del: TRange; Ch: Integer): TRange;

  protected
    FProgressMess: TProgressMess;

//    procedure CreateBMDataList;       // --- ���������� ��� �������� ����� ----------------------------------
//    procedure FindBottomSignalZone;   // --- ������������ ���� ������� ������� ------------------------------
//    function FindBSZoneBlockOK1(StartDisCoord: Integer): Boolean;
//    function FindBSZoneBlockOK2(StartDisCoord: Integer): Boolean;
    procedure FindBottomSignalHoles;  // --- ����� ����� � ������ ������� -----------------------------------
    function FindBottomSignalHolesBlockOK(StartDisCoord: Integer): Boolean;
    procedure BJButton;               // --- ������������ ���� �� �� ������� ������ �� ----------------------
    procedure PBSearch;               // --- ����� ����� � ���������� ����� 1 ������ ------------------------
//    function PBSearchBlockOK(StartDisCoord: Integer): Boolean;
//    procedure PBSearchOnRes(Mask: TMask; var Res: TMaskItemList; BMState: Boolean);
    procedure SortPBSearch;           // --- ���������� ����� � ����� ---------------------------------------
    procedure MarkPB;                 // --- ������� ����� ������������ �� ���� ������ ----------------------
//    procedure MakeSigns;              // --- ������������ ��������� ��� ������� 0, 1, 6 � 7 -----------------
//    procedure PBDeadZone;             // --- �������� ������ ��� ����� -------------------------------------
//    procedure SearchBoltHoles;        // --- ����� ��������� - ����������� ��������� ������ ������� ---------
//    procedure EndSearchStep1;         // --- ����� ������ 1 �� �������� -------------------------------------
//    procedure EndSearchStep2;         // --- ����� ������ 2 �� ���������� � ����� � �� ----------------------
//    function EndSearchBlockOK(StartDisCoord: Integer): Boolean;
    procedure UCSSearch;              // --- ����� ��������������������� �������� 1 -------------------------
//    procedure KillUCS;                // --- ����� ��������������������� �������� 2 -------------------------
//    procedure BigHole_to_PBList;      // --- �������������� ������� ����� � ����������� ������ --------------
//    procedure BladeSearch;            // --- ����� ������� --------------------------------------------------
//    procedure SortSingleObj;          // --- C��������� �������� �� ������������ ���������� -----------------
//    procedure MakeHeadRailObj;        // --- ������������ ��� �� ��� ������� ������� ------------------------
//    procedure MarkEndPB;              // --- ������� ����� ������������ �� ����� ----------------------------
//    procedure MarkEnd2PB;             // --- ������� ����� ������������ �� ����� 2 --------------------------
//    procedure MarkUnknownPB;          // --- ��������� ����� ������������� ����� � ����� �������� �����  ----
//    procedure MakeBodyRailObj;        // --- ������������ ��� �� �� �����  ----------------------------------
//    procedure HeadSearchZone;         // --- ���������� ��� ������ ��� �� �� ������� ------------------------
//    procedure BodySearchZone;         // --- ���������� ��� ������ ��� �� �� ����� --------------------------
//    procedure MakeBodyNoisePath;      // --- ����� ������� ������� - �� ������� ����� -----------------------
//    function BodyNoiseBlockOK(StartDisCoord: Integer): Boolean;
//    procedure MakeHeadNoisePath;      // --- ����� ������� ������� - �� ������� ������� ---------------------
//    function HeadNoiseBlockOK(StartDisCoord: Integer): Boolean;
//    procedure TestBPtoNoise;          // --- �������� ����� �� ��������� � ������� ������� ------------------
    procedure MakeConstruct;          // --- ������������ �������������� ��������� --------------------------
    procedure SearchTechnoError;      // --- ����� ��� ��������� ���������� �������� -----------------------
    procedure MakeDefectPBList;       // --- ���������� ����� � �������� ������ ��� ������ �������� ---------
    procedure SearchDefect;           // --- ����� �������� -------------------------------------------------

//    function GetBtmDelay(R: RRail; Crd: Integer): Integer;
    function TestBtmSgnCrd(R: RRail_; Crd: Integer): Boolean;
    function GetRedCrd(Ch, CCrd, CDel: Integer): Integer;
    procedure PrgMes(S: string);

  public
    FBSignal: TBSignal;
    FBMList: TBMList;
    FFiltr: TFiltr;

//    FBMList: Pointer;
    FStCrd: Integer;
    FEdCrd: Integer;
    FScanStep: Single;
//    FPtn1: array [rLeft..rRight] of array of TPtnItem;
//    FPtn2: array [rLeft..rRight] of array of TPtnItem;
//    FPBList: array [rLeft..rRight, 0..7] of array of TPBItem;
//    FEnd2: array [rLeft..rRight] of array of TEnd2Item;
//    FSingleObj: array [rLeft..rRight] of array of TSingleObj;                 // ��������� ������� - ��������� � ��.
//    FBMData: TBMDataList;                                                     // ���� �������� �����
    FBSPice: array [rLeft..rRight] of array of TBSPice;                       // ������� � ������ ������� - �� ��� ������������ ����� � ������ �������
//    FChMask: array [rLeft..rRight, 1..7] of TMask;                            // ����� �����
    FPBList: TPBList;                                                           // �����
    FPBListCap: array [rLeft..rRight, 0..7] of Integer;                       // ������ ������ ����� - ��� ������������� ���������
    FPBListIdx: array [rLeft..rRight, 0..7] of Integer;                       // ������ ������ ����� - ��� ������������� ���������
    FPBMaxLen: array [rLeft..rRight, 0..7] of Integer;                        // ������������ ����� �����
//    FPoints2: array [rLeft..rRight] of array of TMaskPoint2;                  // ������ ��������� - �� ����������
//    FPoints2MaxLen: array [rLeft..rRight] of Integer;
//    FBlades: array [rLeft..rRight] of array of TRange;                        // ���� ��������

    FBJButton: array of TRange;                                                 // ���� ������� ������ �� - ���� ������� - ���� ����
    FBJSearch: array of TRange;                                                 // ���� ��� ������ ��� ��
    FBJView: array of TRange;                                                   // ���� - ��� ����������� ��

//    FRailObj: TRailSingleObj;                                                   // �������� ����� ������� � ��.
//    FHeadLen: Integer;                                                          // ����� ������
//    FBJHeadBlock: Integer;                                                      // ��� ������ ������
//    FTHead: array [rLeft..rRight] of array [1..7] of array of Integer;        // ������ ��� ������ ������ 1 - ��������� ������; 2..7 - ������� �� �������
//    FFilter1: array [rLeft..rRight, 0..7, 0..255] of Integer;                 // ������ 1 ��� ������ ������
//    FFilter2: array [rLeft..rRight, 0..7, 0..255] of Integer;                 // ������ 2 ��� ������ ������

    FTechnoError: TTechnoErrorList;
    FUnCtrlZ: TUnCtrlZList;                                                     // ��������������������� �������
//    FHZone: array [rLeft..rRight] of array of TWorkItem;
//    FBZone: array [rLeft..rRight] of array of TWorkItem;
    FConstruct: TConstructList;
    FDefPBList: array [rLeft..rRight] of array of TBPListItem;
    FDefList: TDefList;                                                         // �������

    {$IFDEF RECDEBUG}
    FAmplGis: array [rLeft..rRight, 1..7, 0..255] of Integer;
    FDebugGraph: array [0..1, 0..1] of array of Single;
    FPointList: array [rLeft..rRight] of array of TPointItem;
    {$ENDIF}

    constructor Create;
    destructor Destroy; override;
    function GetPBAngleKF(var PB: TPBItem): Integer;
    procedure GetCoordState(Crd: Integer; var Idx1, Idx2: Integer; var BM: Boolean);
    procedure CalcCoeff(Ch: Integer; BMState: Boolean; var Res_: TMaskItemList; var KfDelay, KfAmpl: Single);
    procedure Work(RE: TRecEngine; DatSrc: TAvk11DatSrc; Range: TRange; Debug: Boolean = False; DebugDisCrd: Integer = - 1);
    property Filtr: TFiltr read FFiltr;
    property TechnoError: TTechnoErrorList read FTechnoError;
    property Construct: TConstructList read FConstruct;
    property Defect: TDefList read FDefList;
    property UnCtrlZ: TUnCtrlZList read FUnCtrlZ;                               // ��������������������� �������
//    property BMData: TBMDataList read FBMData;
    property ProgressMess: TProgressMess read FProgressMess write FProgressMess;
  end;

  TChannel = record
    DelayMin: Integer;                // ����������� �������� ����������� ����� ��������
    DelayMax: Integer;                // ������������ �������� ����������� ����� ��������
    DelayStep: Integer;               // ��� ������������� �� �������� [��� / 100]
    Shift: array [1..3] of Integer;   // ��������� �������� ������������ ������ ����������� �������
    RedPar: array [1..2] of Single;   // ���������� ��� �������� � ������� �������
    OutLine: Integer;                 // ����� ������ �����������
  end;

const
{
  ChList: array [0..11] of TChannel = ((DelayMin: 3;  DelayMax: 68;  DelayStep:  33; Shift: ( -28,    0, -28); RedPar: (0, 0); OutLine: 3),       // 0� ���
                                       (DelayMin: 3;  DelayMax: 68;  DelayStep:  33; Shift: ( -28,    0, -28); RedPar: (0, 0); OutLine: 3),       // 0� ���
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: ( -70+70+25,  -50-25, -70+70); RedPar: (0,  1.172); OutLine: 2),  // FORWARD_UWORK_ECHO_58;
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: ( -187+70+20+70, -150+75, -187+70+20); RedPar: (0, -1.172); OutLine: 2),  // BACKWARD_UWORK_ECHO_58;
                                       (DelayMin: 3;  DelayMax: 91;  DelayStep: 100; Shift: (20,  -25-50, 70); RedPar: (0,  1.532); OutLine: 0),  // FORWARD_ECHO_70;
                                       (DelayMin: 3;  DelayMax: 91;  DelayStep: 100; Shift: (5,  -90, 25); RedPar: (0, -1.172); OutLine: 0),  // BACKWARD_ECHO_70;
                                       (DelayMin: 10; DelayMax: 184; DelayStep: 100; Shift: (-25+40 - 15, -125    - 10, -25+40  ); RedPar: (0,  1.091); OutLine: 3),  // FORWARD_ECHO_42_WEB;
                                       (DelayMin: 10; DelayMax: 184; DelayStep: 100; Shift: (-75-40 + 85, -125-25 + 140, -75-40); RedPar: (0, -1.091); OutLine: 3),  // BACKWARD_ECHO_42_WEB;
                                       (DelayMin: 10; DelayMax: 184; DelayStep: 100; Shift: (-25+40 - 15, -125    - 10, -25+40); RedPar: (0,  1.091); OutLine: 3),  // FORWARD_ECHO_42_BASE;
                                       (DelayMin: 10; DelayMax: 184; DelayStep: 100; Shift: (-75-40 + 85, -125-25 + 140, -75-40); RedPar: (0, -1.091); OutLine: 3),  // BACKWARD_ECHO_42_BASE;
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: ( -70+70+25,  -50-25,  -70+70); RedPar: (0,  1.172); OutLine: 1),  //  FORWARD_WORK_ECHO_58;
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: (-187+70+20+70, -150+75, -187+70+20); RedPar: (0, -1.172); OutLine: 1)); //  BACKWARD_WORK_ECHO_58;
}
  ChList: array [0..15] of TChannel = ((DelayMin: 3;  DelayMax: 68;  DelayStep:  33; Shift: ( -25,  -75,   50); RedPar: (0,      0); OutLine: 4),  //  0 - 0� ���
                                       (DelayMin: 3;  DelayMax: 68;  DelayStep:  33; Shift: ( -25,  -75,   50); RedPar: (0,      0); OutLine: 4),  //  1 - 0� ���
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: (  25,  -75,    0); RedPar: (0,  1.172); OutLine: 2),  //  2 - FORWARD_UWORK_ECHO_58;
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: ( -25,  -75, -100); RedPar: (0, -1.172); OutLine: 2),  //  3 - BACKWARD_UWORK_ECHO_58;
                                       (DelayMin: 3;  DelayMax: 91;  DelayStep: 100; Shift: (  20,  -75,   70); RedPar: (0,  1.532); OutLine: 0),  //  4 - FORWARD_ECHO_70;
                                       (DelayMin: 3;  DelayMax: 91;  DelayStep: 100; Shift: (   5,  -90,   25); RedPar: (0, -1.172); OutLine: 0),  //  5 - BACKWARD_ECHO_70;
                                       (DelayMin: 10; DelayMax: 184; DelayStep: 100; Shift: (   0, -135,   15); RedPar: (0,  1.091); OutLine: 3),  //  6 - FORWARD_ECHO_42_WEB;
                                       (DelayMin: 10; DelayMax: 184; DelayStep: 100; Shift: ( -30,  -10, -115); RedPar: (0, -1.091); OutLine: 3),  //  7 - BACKWARD_ECHO_42_WEB;
                                       (DelayMin: 10; DelayMax: 184; DelayStep: 100; Shift: (   0, -135,   15); RedPar: (0,  1.091); OutLine: 3),  //  8 - FORWARD_ECHO_42_BASE;
                                       (DelayMin: 10; DelayMax: 184; DelayStep: 100; Shift: ( -30,  -10, -115); RedPar: (0, -1.091); OutLine: 3),  //  9 - BACKWARD_ECHO_42_BASE;
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: (  25,  -75,    0); RedPar: (0,  1.172); OutLine: 1),  // 10 - FORWARD_WORK_ECHO_58;
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: ( -25,  -75, -100); RedPar: (0, -1.172); OutLine: 1),  // 11 - BACKWARD_WORK_ECHO_58;
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: (  25,  -75,    0); RedPar: (0,  1.172); OutLine: 2),  // 12 - FORWARD_UWORK_MIRR_58;
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: ( -25,  -75, -100); RedPar: (0, -1.172); OutLine: 2),  // 13 - BACKWARD_UWORK_MIRR_58;
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: (  25,  -75,    0); RedPar: (0,  1.172); OutLine: 2),  // 14 - FORWARD_WORK_MIRR_58;
                                       (DelayMin: 17; DelayMax: 135; DelayStep: 100; Shift: ( -25,  -75, -100); RedPar: (0, -1.172); OutLine: 2)); // 15 - BACKWARD_WORK_MIRR_58;

implementation

uses
  RecConfigUnit;

// --------[ TSearchMaskList ]--------------------------------------------------
(*
constructor TMask.Create;
begin
  FOnRes:= nil;
end;

destructor TMask.Destroy;
var
  I: Integer;

begin
  for I:= 0 to High(FMask) do
    SetLength(FMask[I].Items, 0);
  SetLength(FMask, 0);
end;

procedure TMask.Stop;
var
  I: Integer;
  List: TResIdx;

begin
  TestEnd(FWorkCoord, True, List);
  if Assigned(FOnRes) then
    for I:= 0 to High(List) do
      FOnRes(Self, FMask[List[I]].Items, FLastBMState);
  DelMask(List);
  SetLength(List, 0);
end;

procedure TMask.NewCoord(Coord: Integer; BMState: Boolean);
var
  I: Integer;
  List: TResIdx;

begin
  FWorkCoord:= Coord;
  TestEnd(Coord, FLastBMState <> BMState, List);
  if Assigned(FOnRes) then
    for I:= 0 to High(List) do
      FOnRes(Self, FMask[List[I]].Items, FLastBMState);
  DelMask(List);
  FLastBMState:= BMState;
  SetLength(List, 0);
end;

procedure TMask.NewEcho(Delay, Ampl: Integer; MaxSample: Boolean);
var
  I: Integer;
  List: TResIdx;

begin
  if TestAll(Delay, FWorkCoord, List) then
  begin
    Update(List[0], Delay, Ampl, FWorkCoord, MaxSample);
 //   if Length(List) > 1 then Summ(List);
  end
  else AddMask(Delay, Ampl, FWorkCoord, MaxSample);
  SetLength(List, 0);
end;

function TMask.AddMask(Delay, Ampl, Coord: Integer; MaxSample: Boolean): Integer;
var
  I: Integer;
  Flg: Boolean;

begin
  Flg:= False;
  for I:= 0 to High(FMask) do
    if FMask[I].Del then
    begin
      Flg:= True;
      FMask[I].Del:= False;
      Result:= I;
      Break;
    end;

  if not Flg then
  begin
    SetLength(FMask, Length(FMask) + 1);
    Result:= High(FMask);
  end;

  for I:= 0 to 255 do FMask[Result].Delay[I]:= - MaxInt;
  Put(Result, Delay, Ampl, Coord, MaxSample);
end;

procedure TMask.DelMask(Idx: TResIdx);
var
  I, J: Integer;

begin
  if Length(Idx) <> 0 then
  begin
    for I:= 0 to High(Idx) do
    begin
      SetLength(FMask[Idx[I]].Items, 0);
      FMask[Idx[I]].Del:= True;
    end;
  end;
end;

procedure TMask.Put(Index, Delay, Ampl, Coord: Integer; MaxSample: Boolean);
const
  DelayShift: array [0..16] of Integer = (0, - 1, + 1, - 2, + 2, - 3, + 3, - 4, + 4, - 5, + 5, - 6, + 6, - 7,  + 7, - 8, + 8);

var
  I: Integer;

begin
  for I:= 0 to DelaySearchZone do
    if (Delay + DelayShift[I] >= 0) and (Delay + DelayShift[I] <= 255) then
      if (Ch = 1) or
         (    Odd(Ch) and (DelayShift[I] >= 0)) or
         (not Odd(Ch) and (DelayShift[I] <= 0)) then
      FMask[Index].Delay[Delay + DelayShift[I]]:= Coord;

  SetLength(FMask[Index].Items, Length(FMask[Index].Items) + 1);
  FMask[Index].Items[High(FMask[Index].Items)].Coord:= Coord;
  FMask[Index].Items[High(FMask[Index].Items)].Delay:= Delay;
  FMask[Index].Items[High(FMask[Index].Items)].Ampl:= Ampl;
  FMask[Index].Items[High(FMask[Index].Items)].Ok:= MaxSample;
end;

function TMask.TestAll(Delay, Coord: Integer; var ResIdx: TResIdx): Boolean;
var
  I: Integer;

begin
  SetLength(ResIdx, 0);
  Result:= False;
  for I:= 0 to High(FMask) do
    if (not FMask[I].Del) and (FMask[I].Delay[Delay] <> - MaxInt) and (Coord - FMask[I].Delay[Delay] <= MaxCoordSpace) then
    begin
      SetLength(ResIdx, Length(ResIdx) + 1);
      ResIdx[High(ResIdx)]:= I;
    end;
  Result:= Length(ResIdx) <> 0;
end;

function TMask.Update(Index, Delay, Ampl, Coord: Integer; MaxSample: Boolean): Boolean;
begin
  Put(Index, Delay, Ampl, Coord, MaxSample);
end;

procedure TMask.Summ(ResIdx: TResIdx);
var
  I, J, K, L, M, N: Integer;
  Flag: Boolean;
  Tmp: TMaskItem;

begin
  for I:= 1 to High(ResIdx) do
  begin
    for J:= 0 to 255 do
      if FMask[ResIdx[0]].Delay[J] < FMask[ResIdx[I]].Delay[J] then FMask[ResIdx[0]].Delay[J]:= FMask[ResIdx[I]].Delay[J];

    K:= Length(FMask[ResIdx[0]].Items);
    SetLength(FMask[ResIdx[0]].Items, K + Length(FMask[ResIdx[I]].Items));

    for J:= 0 to High(FMask[ResIdx[I]].Items) do
      FMask[ResIdx[0]].Items[K + J]:= FMask[ResIdx[I]].Items[J];
  end;

  with FMask[ResIdx[0]] do
    for I:= 0 to High(Items) - 1 do
    begin
      K:= I;
      for J:= I + 1 to High(Items) do
        if Items[K].Coord > Items[J].Coord then K:= J;
      if K <> I then
      begin
        Tmp:= Items[K];
        Items[K]:= Items[I];
        Items[I]:= Tmp;
      end;
    end;

  Move(ResIdx[1], ResIdx[0], 4 * High(ResIdx));
  SetLength(ResIdx, High(ResIdx));
  DelMask(ResIdx);
end;

procedure TMask.TestEnd(Coord: Integer; Stop: Boolean; var ResIdx: TResIdx);
var
  I, J: Integer;
  Flag: Boolean;

begin
  SetLength(ResIdx, 0);
  for I:= 0 to High(FMask) do
    if not FMask[I].Del then
    begin
      Flag:= True;
      if (not Stop) or (Length(FMask[I].Items) > 100) then
        for J:= 0 to 255 do
          if (FMask[I].Delay[J] <> - MaxInt) and (Coord - FMask[I].Delay[J] <= MaxCoordSpace) then
          begin
            Flag:= False;
            Break;
          end;
      if Flag then
      begin
        SetLength(ResIdx, Length(ResIdx) + 1);
        ResIdx[High(ResIdx)]:= I;
  //      Break;
      end;
    end;
end;
  *)
// ----[ ����� ����� �2 ]-------------------------------------------------------

function TMaskShow.BlockOK(StartDisCoord: Integer): Boolean;
type
  TEchoDataItem = record
    MaskIdx: Integer;
    Value: Integer;
    Ok: Boolean;
  end;

const
  Ma: array [1..3, 1..10] of TPoint = (((X: 1; Y:   0),   // ������
                                        (X: 1; Y:   1),
                                        (X: 1; Y: - 1),
                                        (X: 1; Y:   2),
                                        (X: 1; Y: - 2),
                                        (X: 2; Y:   0),
                                        (X: 2; Y:   1),
                                        (X: 2; Y: - 1),
                                        (X: 2; Y:   2),
                                        (X: 2; Y: - 2)),

                                       ((X: 1; Y: - 1),   // ����������
                                        (X: 1; Y:   0),
                                        (X: 0; Y: - 1),
                                        (X: 2; Y: - 2),
                                        (X: 2; Y: - 1),
                                        (X: 1; Y: - 2),
                                        (X: 0; Y:   0),
                                        (X: 0; Y:   0),
                                        (X: 0; Y:   0),
                                        (X: 0; Y:   0)),

                                       ((X: 1; Y:   1),   // �����������
                                        (X: 1; Y:   0),
                                        (X: 0; Y:   1),
                                        (X: 2; Y:   2),
                                        (X: 2; Y:   1),
                                        (X: 1; Y:   2),
                                        (X: 0; Y:   0),
                                        (X: 0; Y:   0),
                                        (X: 0; Y:   0),
                                        (X: 0; Y:   0)));

var
  I, J, K, L, Q: Integer;
  IL: Integer;
  ML: Integer;
  EchoData: array [1..8] of TEchoDataItem;

  FR: RRail_;
  FCh: Integer;
  FChMa: Integer;

begin

// -----------------------------------------------------  ����� �����

  if not FDatSrc.BackMotion then
    for FR:= rLeft to rRight do
      for FCh:= 1 to 7 do
      begin
        if FCh = 1 then
          while (FDatSrc.CurDisCoord > FBSignal.FBSPos[FR, FIdx[FR]].Crd) and // ����������� ������ ��
                (FIdx[FR] < High(FBSignal.FBSPos[FR])) do Inc(FIdx[FR]);

        if FDatSrc.CurEcho[FR, FCh].Count <> 0 then
        begin

          case FCh of                                     // ���������� ������ ������� ������
            1: FChMa:= 1;
            2: FChMa:= 2;
            3: FChMa:= 3;
            4: FChMa:= 2;
            5: FChMa:= 3;
            6: FChMa:= 2;
            7: FChMa:= 3;
          end;

          with FDatSrc.CurEcho[FR, FCh] do   // ������ �������� ������� ����������
          begin
                                                    // ������������ ������ - ������ ������ � ������ ����������� ������
            for I:= 1 to Min(8, Count) do                   // ����������� ������� ������� ����������
            begin

              EchoData[I].Ok:= (not Assigned(FFiltr)) or
                               (Assigned(FFiltr) and FFiltr.TestEcho(FDatSrc.CurDisCoord, FR, FCh, Ampl[I], Delay[I]));

              if FCh = 1 then
                EchoData[I].Ok:= EchoData[I].Ok and
                  ((FDatSrc.CurEcho[FR, FCh].Delay[I] < FBSignal.FBSPos[FR, FIdx[FR]].MinDelay) and
                   (FDatSrc.CurEcho[FR, FCh].Delay[I] >= ChList[FCh].DelayMin * 3) and
                   (FDatSrc.CurEcho[FR, FCh].Delay[I] <= ChList[FCh].DelayMax * 3));

              if not EchoData[I].Ok then Continue;

              EchoData[I].Value:= MaxInt;           // ���������� ������
              for J:= 0 to High(FMask[FR, FCh]) do   // ������������� ����������� �����
                if not FMask[FR, FCh][J].Del then
                begin
                  K:= High(FMask[FR, FCh][J].Item);  // ����� ������ ���������� ������� ��������� � �����
                  for L:= 1 to 10 do                // ���� ������������ DX, DY � ������� ������
                    if (Ma[FChMa, L].X = (FDatSrc.CurDisCoord - FMask[FR, FCh][J].Item[K].Coord)) and
                       (Ma[FChMa, L].Y = (Delay[I] - FMask[FR, FCh][J].Item[K].Delay)) and
                       (L < EchoData[I].Value) { and
                       ((L = 1) or ((L <> 1) and (Mask[FR, FCh][J].Dir[L] < 2))) } then
                    begin
                  {    case L of
                        1: begin
                             if Mask[FR, FCh][J].Dir[2] > 0 then Dec(Mask[FR, FCh][J].Dir[2]);
                             if Mask[FR, FCh][J].Dir[3] > 0 then Dec(Mask[FR, FCh][J].Dir[3]);
                             if Mask[FR, FCh][J].Dir[4] > 0 then Dec(Mask[FR, FCh][J].Dir[4]);
                             if Mask[FR, FCh][J].Dir[5] > 0 then Dec(Mask[FR, FCh][J].Dir[5]);
                             if Mask[FR, FCh][J].Dir[6] > 0 then Dec(Mask[FR, FCh][J].Dir[6]);
                           end;
                      end; }
                      EchoData[I].MaskIdx:= J;
                      EchoData[I].Value:= L;
                      Break;
                    end;
                end;
            end;

            for I:= 1 to Min(8, Count) do
            begin
              if not EchoData[I].Ok then Continue;
              if EchoData[I].Value <> MaxInt then                         // ������� ������ �� ������ � ���� �� ����������� �����
              begin                                                       // ������ - ��������� ������� ������ � �����
                J:= EchoData[I].MaskIdx;

                K:= Length(FMask[FR, FCh][J].Item);
                SetLength(FMask[FR, FCh][J].Item, K + 1);
                FMask[FR, FCh][J].Item[High(FMask[FR, FCh][J].Item)].Coord:= FDatSrc.CurDisCoord;
                FMask[FR, FCh][J].Item[High(FMask[FR, FCh][J].Item)].Delay:= Delay[I];
                FMask[FR, FCh][J].Item[High(FMask[FR, FCh][J].Item)].Ampl:= Ampl[I];
  //              Inc(Mask[FR, FCh][J].Dir[EchoData[I].Value]);
              end
              else
              begin                                                       // �� ������ - ������� ����� �����
                ML:= - 1;
                for K:= 0 to High(FMask[FR, FCh]) do                       // ���� ��������� ����� � ������� ����������� �����
                  if FMask[FR, FCh][K].Del then
                  begin
                    ML:= K;
                    Break;
                  end;

                if ML = - 1 then
                begin
                  ML:= Length(FMask[FR, FCh]);
                  SetLength(FMask[FR, FCh], ML + 1);
                end;
                                                                          // ������ ����� ������ � ������� - ML
                FMask[FR, FCh][ML].Del:= False;
  //              for J:= 1 to 6 do Mask[FR, FCh][ML].Dir[J]:= 0;
                SetLength(FMask[FR, FCh][ML].Item, 1);
                FMask[FR, FCh][ML].Item[0].Coord:= FDatSrc.CurDisCoord;
                FMask[FR, FCh][ML].Item[0].Delay:= Delay[I];
                FMask[FR, FCh][ML].Item[0].Ampl:= Ampl[I];
              end;
            end;
          end;
        end;
      end;
  TestEnd(FDatSrc.CurDisCoord, FDatSrc.BackMotion);
  Result:= True;
end;

procedure TMaskShow.TestEnd(CurDisCoord: Integer; Drop: Boolean);
var
  I, J, K, M, J_: Integer;
  FR: RRail_;
  FCh: Integer;
  New: TPBItem;
  Flg: Boolean;
  SummAmpl: Integer;
  Crd_: TRange;
  Del_: TRange;

begin
  for FR:= rLeft to rRight do
    for FCh:= 1 to 7 do
      for J:= 0 to High(FMask[FR, FCh]) do
        if not FMask[FR, FCh][J].Del then
          if (CurDisCoord - FMask[FR, FCh][J].Item[High(FMask[FR, FCh][J].Item)].Coord > 3) or Drop then
          begin
                                                         // �������� ����� - ������� / ������ ?

            with FMask[FR, FCh][J] do
            begin
              SummAmpl:= 0;
              for J_:= 0 to High(Item) do SummAmpl:= SummAmpl + Item[J_].Ampl; // ��������� ��������� �����
            end;

            if (FCh <> 1) then
            begin
              Flg:= False;
              with FMask[FR, FCh][J] do
              begin
//                SummAmpl:= 0;
//                for J_:= 0 to High(Item) do SummAmpl:= SummAmpl + Item[J_].Ampl; // ��������� ��������� �����
                if (Length(Item) > 1) and
                   (((Length(Item) <= 4) and (SummAmpl >= RecConfig.PBCountAmpl[Length(Item)])) or
                    (Length(Item) > 4)) then Flg:= True;

                if Flg and (FCh <> 1) then // �������� ���� �������
                begin
                  Crd_.StCrd:= Item[0].Coord;
                  Crd_.EdCrd:= Item[High(Item)].Coord;
                  Del_.StCrd:= Item[0].Delay;
                  Del_.EdCrd:= Item[High(Item)].Delay;
                  if Abs(Round(100 * (GetLen(Del_) - 1) / (GetLen(Crd_) - 1))) < 60 then Flg:= False;
                end;
              end;
            end else Flg:= True; // 1 - ����� ����� ��� �����

            if Flg then
            begin
              Inc(PBIdx[FR, FCh]);                         // ������������ ��������� �������
              if PBIdx[FR, FCh] > PBCap[FR, FCh] - 1 then
              begin
                PBCap[FR, FCh]:= 2 * PBCap[FR, FCh];
                SetLength(PBList[FR, FCh], PBCap[FR, FCh]);
              end;
  {
              SetLength(PBList[FR, FCh, PBIdx[FR, FCh]].Item, Length(FMask[FR, FCh][J].Item));
              for I:= 0 to High(FMask[FR, FCh][J].Item) do
                PBList[FR, FCh, PBIdx[FR, FCh]].Item[I]:= FMask[FR, FCh][J].Item[I];
  }

              with FMask[FR, FCh][J] do
              begin
                New.Crd.StCrd:= Item[0].Coord;
                New.Crd.EdCrd:= Item[High(Item)].Coord;
                New.Del.StCrd:= Item[0].Delay;
                New.Del.EdCrd:= Item[High(Item)].Delay;
                New.CCrd:= (Item[0].Coord + Item[High(Item)].Coord) div 2;
                New.CDel:= (Item[0].Delay + Item[High(Item)].Delay) div 2;
//                New.A:= 0;
//                for I:= 0 to High(Item) do New.A:= Max(New.A, Item[I].Ampl); // ��������� �����
                New.EchoCount:= Length(Item);
                New.SummAmpl_:= Min(255, SummAmpl);
              end;

              New.Typ:= [];
                                                // ������� ���������� �������� � ���������� 3, 4, 5, 6 � 7
              if FCh = 1 then
                for K:= 3 to 7 do
                begin
                  New.Dat[K]:= 0;
                  with FMask[FR, FCh][J] do
                    for M:= 0 to High(Item) do
                      if Item[M].Ampl < K then Inc(New.Dat[K]);
                end;

              PBList[FR, FCh, PBIdx[FR, FCh]]:= New;

              with FMask[FR, FCh][J] do
              begin
                {$IFDEF RECDEBUG}
                K:= - 1;
                for J_:= 0 to High(Item) do
                begin
               {   if (K <> - 1) and (J_ > 0) and (Item[J_].Coord = Item[J_ - 1].Coord) then
                  begin
                    New.Items[K].StDelay:= Min(Item[J_].Delay - 2, Item[J_ - 1].Delay - 2);
                    New.Items[K].EdDelay:= Max(Item[J_].Delay + 2, Item[J_ - 1].Delay + 2);
                  end
                  else
                  begin }
                    K:= Length(PBList[FR, FCh, PBIdx[FR, FCh]].Items);
                    SetLength(PBList[FR, FCh, PBIdx[FR, FCh]].Items, K + 1);
                    PBList[FR, FCh, PBIdx[FR, FCh]].Items[K].Crd:= Item[J_].Coord;
                    PBList[FR, FCh, PBIdx[FR, FCh]].Items[K].Delay:= Item[J_].Delay {- 2};
  //                  PBList[FR, FCh, PBIdx[FR, FCh]].Items[K].StDelay:= Item[J_].Delay {- 2};
  //                  PBList[FR, FCh, PBIdx[FR, FCh]].Items[K].EdDelay:= Item[J_].Delay {+ 2};
                    PBList[FR, FCh, PBIdx[FR, FCh]].Items[K].Ampl:= Item[J_].Ampl;
  //                end;
                end;
                {$ENDIF}
              end;
            end;

//          for J:= 0 to High(Res) do New.E:= New.E + Sqr(Res[J].Ampl); // ������� �����
//          New.E:= New.E / Length(Res);
//          PBList[FR, FCh, PBIdx[FR, FCh]]:= New;

            SetLength(FMask[FR, FCh][J].Item, 0); // ������� ������ ������ ��� ������ ������
            FMask[FR, FCh][J].Del:= True;
          end;
{  except
    showmessage('ERROR!!!');
  end;}
end;

procedure TMaskShow.Calc(StCrd, EdCrd: Integer);
var
  I, J: Integer;
  L, Len: Integer;

  A: Integer;
  D: Integer;
  C: Integer;

  R: RRail_;
  Ch: Integer;

begin
  for R:= rLeft to rRight do      // ���������� ��������
  begin
    FIdx[R]:= 0;
    for Ch:= 1 to 7 do
    begin
      PBCap[R, Ch]:= 1000;
      PBIdx[R, Ch]:= - 1;
      SetLength(PBList[R, Ch], PBCap[R, Ch]);
    end;
  end;

  if Assigned(FFiltr) then FFiltr.StartFilter(StCrd);
  FDatSrc.LoadData(StCrd, EdCrd, 0, BlockOK); // ����� �����
  TestEnd(MaxInt, True);

  for R:= rLeft to rRight do                //
    for Ch:= 1 to 7 do
      SetLength(PBList[R, Ch], PBIdx[R, Ch] + 1);
end;

constructor TMaskShow.Create(DatSrc: TAvk11DatSrc; RecData_: TRecData; PBList_: PPBList; BSignal: TBSignal; Filtr: TFiltr);
begin
  FDatSrc:= DatSrc;
  FBSignal:= BSignal;
  PBList:= PBList_;
  FFiltr:= Filtr;
  FRecData:= RecData_;
end;

destructor TMaskShow.Destroy;
var
  R: RRail_;
  I, Ch: Integer;

begin
  for R:= rLeft to rRight do      // �������� ��������
    for Ch:= 1 to 7 do
    begin
      SetLength(PBList[R, Ch], 0);
      {$IFDEF RECDEBUG}
      for I:= 0 to High(PBList[R, Ch]) do SetLength(PBList[R, Ch, I].Items, 0);
      {$ENDIF}
      for I:= 0 to High(FMask[R, Ch]) do SetLength(FMask[R, Ch, I].Item, 0);
      SetLength(FMask[R, Ch], 0);
    end;
end;

// --------[ TRecData ]--------------------------------------------------------

constructor TRecData.Create;
begin
  FProgressMess:= nil;
  FDebug:= False;
  FStCrd:= 0;
  FEdCrd:= 0;
  inherited Create;
end;

destructor TRecData.Destroy;
var
  R: RRail_;
  I, Ch: Integer;

begin
  SetLength(FTechnoError, 0);
  for R:= rLeft to rRight do
  begin
    SetLength(FConstruct[R], 0);

    for I:= 0 to High(FDefList[R]) do
      SetLength(FDefList[R, I].PBList, 0);

    SetLength(FDefList[R], 0);
    for I:= 0 to High(FDefList[R]) do
      SetLength(FDefList[R, I].PBList, 0);

    SetLength(FUnCtrlZ[R], 0);
//    SetLength(FEnd2[R], 0);
//    for I:= 0 to High(FSingleObj[R]) do
//      SetLength(FSingleObj[R, I].PBList, 0);
//    SetLength(FSingleObj[R], 0);
    SetLength(FDefPBList[R], 0);
  end;

  for R:= rLeft to rRight do
  begin
    for Ch:= 1 to 7 do
    begin

    {$IFDEF RECDEBUG}
{      for I:= 0 to High(FPBList[R, Ch]) do
      begin
        SetLength(FPBList[R, Ch, I].Items, 0);
        SetLength(FPBList[R, Ch, I].TypList, 0);
      end; }
    {$ENDIF}
      SetLength(FPBList[R, Ch], 0);
    end;
  end;


//  for I:= 0 to High(FBMData) do
//    SetLength(FBMData[I].DisItems, 0);
//  SetLength(FBMData, 0);

  SetLength(FBJButton, 0);
  SetLength(FBJSearch, 0);
  SetLength(FBJView, 0);


  inherited Destroy;
end;

function TRecData.GetPBAngleKF(var PB: TPBItem): Integer;
begin
  if GetLen(PB.Crd) - 1 = 0
    then Result:= 0
    else Result:= Round(100 * (GetLen(PB.Del) - 1) / (GetLen(PB.Crd) - 1));
end;

function TRecData.GetRedCentr(Ch: Integer; var PB: TPBItem): Integer;
begin
  Result:= Round(PB.CCrd + (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * PB.CDel) / FScanStep);
end;

function TRecData.GetRed(Crd, Del: TRange; Ch: Integer): TRange;
begin
  Result.StCrd:= Round(Crd.StCrd + (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * Del.StCrd) / FScanStep);
  Result.EdCrd:= Round(Crd.EdCrd + (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * Del.EdCrd) / FScanStep);
end;
(*
procedure TRecData.SetPBTyp(var PB: TPBItem; AlgNo: Integer; NewType: TPBTyp);
begin
  PB.Typ:= NewType;
  {$IFDEF RECDEBUG}
  SetLength(PB.TypList, Length(PB.TypList) + 1);
  PB.TypList[High(PB.TypList)].Typ:= NewType;
  PB.TypList[High(PB.TypList)].AlgNo:= AlgNo;
  {$ENDIF}
end;
*)
procedure TRecData.CalcCoeff(Ch: Integer; BMState: Boolean; var Res_: TMaskItemList; var KfDelay, KfAmpl: Single);
type
  TSgn = (iDelay, iAmpl);
  TTyp = (iSrc, iComp);

var
  I, J, K: Integer;
  Typ: TTyp;
  Sgn: TSgn;
  DL: Integer;
  Min_: array [TSgn] of Single;
  Max_: array [TSgn] of Single;
  Cen: array [TSgn, TTyp] of Single;
  Graph: array [TSgn, TTyp] of array of Single;
  Kf: array [TSgn] of Single;
  S1, S2, S3: Single;

  Res: TMaskItemList;

begin
  if Length(Res_) < 2 then Exit;

  SetLength(Res, 1);            // ���� � ����� ���� �������� �� �� �� ���������
  Res[0]:= Res_[0];
  for I:= 1 to High(Res_) do
    if Res_[I - 1].Coord <> Res_[I].Coord then
      for K:= Res_[I - 1].Coord + 1 to Res_[I].Coord do
      begin
        J:= Length(Res);
        SetLength(Res, J + 1);
        Res[J].Coord:= K;
        Res[J].Delay:= (Res_[I].Delay + Res_[I - 1].Delay) div 2;
        Res[J].Ampl:= (Res_[I].Ampl + Res_[I - 1].Ampl) div 2;
      end
      else
      begin
        J:= Length(Res);
        SetLength(Res, J + 1);
        Res[J]:= Res_[I];
      end;

  // -------- ��������� ����� (����� � ������������� ���������) � ��������� ---------

  DL:= High(Res);           // ���������� �������� ��������� � ��������
  for Typ:= iSrc to iComp do
    for Sgn:= iDelay to iAmpl do SetLength(Graph[Sgn, Typ], DL + 1);

  for I:= 0 to DL do
  begin
    Graph[iDelay, iSrc, I]:= Res[I].Delay;
    Graph[iAmpl,  iSrc, I]:= Res[I].Ampl;
  end;
                            // ����������� ��������� � ���������� ��������
  for Sgn:= iDelay to iAmpl do
  begin
    Min_[Sgn]:= MaxInt;
    Max_[Sgn]:= - MaxInt;
    for I:= 0 to DL do
    begin
      if Min_[Sgn] > Graph[Sgn, iSrc, I] then Min_[Sgn]:= Graph[Sgn, iSrc, I];
      if Max_[Sgn] < Graph[Sgn, iSrc, I] then Max_[Sgn]:= Graph[Sgn, iSrc, I];
    end;
  end;

                            // ������������ ��������
  for Sgn:= iDelay to iAmpl do
    if Max_[Sgn] - Min_[Sgn] <> 0 then
      for I:= 0 to DL do
        Graph[Sgn, iSrc, I]:= 2 * (Graph[Sgn, iSrc, I] - Min_[Sgn]) / (Max_[Sgn] - Min_[Sgn]) - 1;

                            // ���������� ��������� �������� ��������� � ��������

  if Ch = 1 then for I:= 0 to DL do Graph[iDelay, iComp, I]:= 1 - 2 * Sin(Pi * I / DL);
  if ((not BMState) and (Ch = 6)) or
           (BMState and (Ch = 7)) then for I:= 0 to DL do Graph[iDelay, iComp, I]:= 1 - 2 * I / DL;
  if ((not BMState) and (Ch = 7)) or
           (BMState and (Ch = 6)) then for I:= 0 to DL do Graph[iDelay, iComp, I]:= 2 * I / DL - 1;

  for I:= 0 to DL do Graph[iAmpl, iComp, I]:= 2 * Sin(Pi * I / DL) - 1;

                            // ������ �������������� ������������
                              // ���������� �������� ��� ������� �������

  for Typ:= iSrc to iComp do
    for Sgn:= iDelay to iAmpl do
    begin
      Cen[Sgn, Typ]:= 0;
      for K:= 0 to DL do
        Cen[Sgn, Typ]:= Cen[Sgn, Typ] + Graph[Sgn, Typ, K];
      Cen[Sgn, Typ]:= Cen[Sgn, Typ] / (DL + 1);
    end;

                              // ��� ������ �������������� ������������
  for Sgn:= iDelay to iAmpl do
  begin
    S1:= 0;
    S2:= 0;
    S3:= 0;
    for K:= 0 to DL do
    begin
      S1:= S1 + (Graph[Sgn, iSrc, K] - Cen[Sgn, iSrc]) * (Graph[Sgn, iComp, K] - Cen[Sgn, iComp]);
      S2:= S2 + Sqr(Graph[Sgn, iSrc, K] - Cen[Sgn, iSrc]);
      S3:= S3 + Sqr(Graph[Sgn, iComp, K] - Cen[Sgn, iComp]);
    end;
    if (S2 <> 0) and (S3 <> 0) then Kf[Sgn]:= S1 / (Sqrt(S2) * Sqrt(S3))
                               else Kf[Sgn]:= 0;
  end;

  {$IFDEF RECDEBUG}
  for Typ:= iSrc to iComp do
    for Sgn:= iDelay to iAmpl do
    begin
      SetLength(FDebugGraph[Ord(Sgn), Ord(Typ)], DL + 1);
      Move(Graph[Sgn, Typ, 0], FDebugGraph[Ord(Sgn), Ord(Typ), 0], (DL + 1) * SizeOf(Single));
    end;
  {$ENDIF}

                            // �������� ��������� ��������
  for Typ:= iSrc to iComp do
    for Sgn:= iDelay to iAmpl do SetLength(Graph[Sgn, Typ], 0);

  KfDelay:= Kf[iDelay];
  KfAmpl:= Kf[iAmpl];

  SetLength(Res, 0);
end;

// -----------------------------------------------------------------------------
// ------------ ���������� ��� �������� ����� ----------------------------------
// -----------------------------------------------------------------------------
(*
procedure TRecData.CreateBMDataList;
var
  I: Integer;
  J: Integer;
  K: Integer;
  SC: Integer;
  EC: Integer;
  Flg: Boolean;
  DisCoord: Integer;
  StDisCrd: Integer;
  EdDisCrd: Integer;
  LastEdDisCrd: Integer;

begin
  I:= 0;
  LastEdDisCrd:= - 1;
  with FDatSrc do
    repeat
      Flg:= False;
      StDisCrd:= Event[FCDList[I]].Event.DisCoord;
      EdDisCrd:= Event[FCDList[I]].Event.DisCoord;
      repeat
        for J:= 0 to High(FCDList) - 1 do
          if (I <> J) and (I <> J + 1) then
            if (Event[FCDList[I]].Event.SysCoord >= Min(Event[FCDList[J]].Event.SysCoord, Event[FCDList[J + 1]].Event.SysCoord)) and
               (Event[FCDList[I]].Event.SysCoord <= Max(Event[FCDList[J]].Event.SysCoord, Event[FCDList[J + 1]].Event.SysCoord)) and
               (Event[FCDList[J]].Event.SysCoord <> Event[FCDList[J + 1]].Event.SysCoord) then
            begin
              DisCoord:= Event[FCDList[J]].Event.DisCoord +
                          Round( Abs(Event[FCDList[I]].Event.SysCoord - Event[FCDList[J]].Event.SysCoord) /
                                 Abs(Event[FCDList[J]].Event.SysCoord - Event[FCDList[J + 1]].Event.SysCoord) *
                                (Event[FCDList[J + 1]].Event.DisCoord - Event[FCDList[J]].Event.DisCoord));

              StDisCrd:= Min(StDisCrd, DisCoord);
              EdDisCrd:= Max(EdDisCrd, DisCoord);
              Flg:= True;
            end;

        if (I + 1 > High(FCDList)) or
           (Event[FCDList[I + 1]].Event.DisCoord > EdDisCrd) then Break;
        Inc(I);
      until False;

      if Flg and ((LastEdDisCrd = - 1) or (StDisCrd > LastEdDisCrd)) then
      begin
        SetLength(FBMData, Length(FBMData) + 1);
        FBMData[High(FBMData)].DisRange.StCrd:= StDisCrd;
        FBMData[High(FBMData)].DisRange.EdCrd:= EdDisCrd;
        LastEdDisCrd:= EdDisCrd;
      end;
      Inc(I);
    until I >= Length(FCDList);

  with FDatSrc do
    for I:= 0 to High(FBMData) do
    begin
      for J:= 0 to High(FCDList) do
        if (Event[FCDList[J]].Event.DisCoord > FBMData[I].DisRange.StCrd) and
           (Event[FCDList[J]].Event.DisCoord < FBMData[I].DisRange.EdCrd) then
        begin
          SetLength(FBMData[I].DisItems, Length(FBMData[I].DisItems) + 1);
          if Length(FBMData[I].DisItems) = 1
            then FBMData[I].DisItems[High(FBMData[I].DisItems)].StCrd:= FBMData[I].DisRange.StCrd
            else FBMData[I].DisItems[High(FBMData[I].DisItems)].StCrd:= FBMData[I].DisItems[High(FBMData[I].DisItems) - 1].EdCrd;
          FBMData[I].DisItems[High(FBMData[I].DisItems)].EdCrd:= Event[FCDList[J]].Event.DisCoord;
        end;

      if Length(FBMData[I].DisItems) <> 0 then
      begin
        SetLength(FBMData[I].DisItems, Length(FBMData[I].DisItems) + 1);
        FBMData[I].DisItems[High(FBMData[I].DisItems)].StCrd:= FBMData[I].DisItems[High(FBMData[I].DisItems) - 1].EdCrd;
        FBMData[I].DisItems[High(FBMData[I].DisItems)].EdCrd:= FBMData[I].DisRange.EdCrd;
      end;
    end;

  with FDatSrc do
    for I:= 0 to High(FBMData) do
    begin
      FBMData[I].SysRange.StCrd:= DisToSysCoord(FBMData[I].DisRange.StCrd);
      FBMData[I].SysRange.EdCrd:= DisToSysCoord(FBMData[I].DisRange.EdCrd);
    end;
end;
 *)
function TRecData.TestRange(Range: TRange): Boolean; // ���������
var
  IdxA1, IdxA2: Integer;
  IdxB1, IdxB2: Integer;
  BM1, BM2: Boolean;

begin
  GetCoordState(Range.StCrd, IdxA1, IdxA2, BM1);
  GetCoordState(Range.EdCrd, IdxB1, IdxB2, BM2);

  Result:= ((IdxA1 = IdxB1) and (not BM1) and (not BM2)) or
           (Abs(IdxA1 - IdxB1) = 1) and (not BM1) and (not BM2);
end;

procedure TRecData.TestRange2(Range: TRange; var ResCrd: TIntegerDynArray); // ��������� ���� �� ������ Range ����� ���� ��
var
  I, J: Integer;

begin
  SetLength(ResCrd, 0);
  for I:= 0 to High(FBMList.FBMData) do                        // ��������� �� ��������� � ���� �������� �����
    for J:= 0 to High(FBMList.FBMData[I].DisItems) do
      if Odd(J) and
         (Range.StCrd <= FBMList.FBMData[I].DisItems[J].StCrd) and
         (Range.EdCrd >= FBMList.FBMData[I].DisItems[J].EdCrd) then
      begin
        SetLength(ResCrd, Length(ResCrd) + 1);
        ResCrd[High(ResCrd)]:= FBMList.FBMData[I].DisItems[J].StCrd;
        SetLength(ResCrd, Length(ResCrd) + 1);
        ResCrd[High(ResCrd)]:= FBMList.FBMData[I].DisItems[J].EdCrd;
      end;
end;

function TRecData.BackMotion(Crd: Integer): Boolean; // �� ��������� � ���� �������� �����
var
  I, J: Integer;

begin
  Result:= False;
  for I:= 0 to High(FBMList.FBMData) do                        // ��������� �� ��������� � ���� �������� �����
    if (Crd >= FBMList.FBMData[I].DisRange.StCrd) and
       (Crd <= FBMList.FBMData[I].DisRange.EdCrd) then
      for J:= 0 to High(FBMList.FBMData[I].DisItems) do
        if (Odd(J)) and
           (Crd >= FBMList.FBMData[I].DisItems[J].StCrd) and
           (Crd <= FBMList.FBMData[I].DisItems[J].EdCrd) then
        begin
          Result:= True;
          Exit;
        end;
end;

procedure TRecData.GetCoordState(Crd: Integer; var Idx1, Idx2: Integer; var BM: Boolean);
var
  I, J: Integer;

begin
  BM:= False;
  Idx1:= 0;
  Idx2:= 0;
  for I:= 0 to High(FBMList.FBMData) do
    if (Crd >= FBMList.FBMData[I].DisRange.StCrd) and              // ��������� �� ��������� � ����
       (Crd <= FBMList.FBMData[I].DisRange.EdCrd) then
      for J:= 0 to High(FBMList.FBMData[I].DisItems) do
      begin
        if (Crd >= FBMList.FBMData[I].DisItems[J].StCrd) and
           (Crd <= FBMList.FBMData[I].DisItems[J].EdCrd) then
        begin
          BM:= FDatSrc.DisToSysCoord(FBMList.FBMData[I].DisItems[J].StCrd) >
               FDatSrc.DisToSysCoord(FBMList.FBMData[I].DisItems[J].EdCrd);

          Idx1:= I;
          Idx2:= J;
          Exit;
        end;
      end;

  J:= 0;
  for I:= 0 to High(FBMList.FBMData) do                        // ��������� �� ��������� ���� ���� �������� �����
  begin
    if (Crd >= J) and
       (Crd <= FBMList.FBMData[I].DisRange.StCrd) then
      begin
        Idx1:= I;
        Idx2:= - 1;
        Exit;
      end;
    J:= FBMList.FBMData[I].DisRange.EdCrd;
  end;

  if Length(FBMList.FBMData) <> 0 then
    if (Crd >= FBMList.FBMData[High(FBMList.FBMData)].DisRange.EdCrd) and
       (Crd <= FDatSrc.MaxDisCoord) then
      begin
        Idx1:= High(FBMList.FBMData);
        Idx2:= - 1;
      end;
end;

procedure TRecData.MoveFrom(var Crd: Integer; Dir: Integer); // ������� ���������� � ������ / ����� ���� �������� �����
var
  I, J: Integer;

begin
  for I:= 0 to High(FBMList.FBMData) do
    if (Crd >= FBMList.FBMData[I].DisRange.StCrd) and              // ��������� �� ��������� � ����
       (Crd <= FBMList.FBMData[I].DisRange.EdCrd) then
      for J:= 0 to High(FBMList.FBMData[I].DisItems) do            // ������������� �������� ����
        if (Odd(J)) and                                    // ��������� �� ��������� � ���� �������� �����
           (Crd >= FBMList.FBMData[I].DisItems[J].StCrd) and
           (Crd <= FBMList.FBMData[I].DisItems[J].EdCrd) then
        begin
          if Dir > 0 then Crd:= FBMList.FBMData[I].DisItems[J].EdCrd + 1    // ��� �������  
                     else Crd:= FBMList.FBMData[I].DisItems[J].StCrd - 1;
          Exit;
        end;
end;

// -----------------------------------------------------------------------------
// ------------ ����� ����� � ���������� ����� 1 ������ ------------------------
// -----------------------------------------------------------------------------
(*
function TRecData.PBSearchBlockOK(StartDisCoord: Integer): Boolean;
var
  R: RRail;
  I: Integer;
  Ch: Integer;
  MaxA: Integer;
  MaxI: Integer;

begin
  for R:= rLeft to rRight do
  begin
                                                          // ��� 1 ������
    Ch:= 1;
    while (FDatSrc.CurDisCoord > FBSignal.FBSPos[R, FIdx[R, 1]].Crd) and // ����������� ������ �� 
          (FIdx[R, 1] < High(FBSignal.FBSPos[R])) do Inc(FIdx[R, 1]);

    if Assigned(FChMask[R, Ch]) then
    begin
      MaxA:= - 1;
      with FDatSrc.CurEcho[R, Ch] do
        for I:= 1 to Count do
          if MaxA < Ampl[I] then
          begin
            MaxA:= Ampl[I];
            MaxI:= I;
          end;

      FChMask[R, Ch].NewCoord(FDatSrc.CurDisCoord, FDatSrc.BackMotion);
      with FDatSrc.CurEcho[R, Ch] do
        for I:= 1 to Count do
        begin
          if Assigned(FFiltr) then
            if not FFiltr.TestEcho(FDatSrc.CurDisCoord, R, Ch, Ampl[I], Delay[I]) then Continue;

          if (FDatSrc.CurEcho[R, Ch].Delay[I] < FBSignal.FBSPos[R, FIdx[R, 1]].MinDelay) and
             (FDatSrc.CurEcho[R, Ch].Delay[I] > ChList[Ch].DelayMin * 3) and
             (FDatSrc.CurEcho[R, Ch].Delay[I] < ChList[Ch].DelayMax * 3) then FChMask[R, Ch].NewEcho(Delay[I], Ampl[I] - 2, MaxI = I);

//          if (FDatSrc.CurEcho[R, Ch].Delay[I] < FDatSrc.CurParams.StStr[R, 0] * FDatSrc.DelayKoeff) and
//             (FDatSrc.CurEcho[R, Ch].Delay[I] > ChList[Ch].DelayMin * 3) and
//             (FDatSrc.CurEcho[R, Ch].Delay[I] < ChList[Ch].DelayMax * 3) then FChMask[R, Ch].NewEcho(Delay[I], Ampl[I] - 2, MaxI = I);

//             (FDatSrc.CurEcho[R, Ch].Delay[I] / FDatSrc.DelayKoeff >= ) and
//             (FDatSrc.CurEcho[R, Ch].Delay[I] / FDatSrc.DelayKoeff <= FDatSrc.CurParams.EdStr[R, 0])

        end;
    end;
                                                            // ��� ���. �������
    for Ch:= 2 to 7 do
      if Assigned(FChMask[R, Ch]) then
      begin
        FChMask[R, Ch].NewCoord(FDatSrc.CurDisCoord, FDatSrc.BackMotion);
        with FDatSrc.CurEcho[R, Ch] do
          for I:= 1 to Count do
          begin
            if Assigned(FFiltr) then
              if not FFiltr.TestEcho(FDatSrc.CurDisCoord, R, Ch, Ampl[I], Delay[I]) then Continue;

            if (FDatSrc.CurEcho[R, Ch].Delay[I] > ChList[Ch].DelayMin) and
               (FDatSrc.CurEcho[R, Ch].Delay[I] < ChList[Ch].DelayMax) then FChMask[R, Ch].NewEcho(Delay[I], Ampl[I] - 2, False);
          end;
      end;
  end;
  Result:= True;
end;

procedure TRecData.PBSearchOnRes(Mask: TMask; var Res: TMaskItemList; BMState: Boolean);
var
  I, J, K, M, Th, MaxCrd: Integer;
  KfD, KfA, KfF: Single;
  Good, F1, F2, F3, F4: Boolean;
  Res2: TMaskItemList;
  KfD_, KfA_: array [1..5] of Single;

  Crd, D: Integer;
  New: TPBItem;
//  Angle: Single;
  DelayTh: Integer;
  SummAmpl: Integer;

begin
( *
      {
  if Mask.Ch = 1 then
  begin
    F1:= False;
    for I:= 0 to High(Res) do F1:= F1 or Res[I].Ok;
    if not F1 then Exit;
  end;
            }
  if BMState then Exit;

  SummAmpl: Integer;
  SummAmpl:= 0;
  for J:= 0 to High(Res) do SummAmpl:= SummAmpl + Res[J].Ampl; // ��������� ��������� �����
//  if  (Length(Res) > 1)    then
//    or (SummAmpl >= 25)}
//  if ((Mask.Ch in [0, 2, 3, 4, 5, 6, 7]) and (Length(Res) > 2)) or
//     ((Mask.Ch = 1) and (Length(Res) > 0)) then
  begin
  ( *
  // -------------- ��� ������ ����� ����� 1 ������ - ��������� ��������� �� ����� -----------------

    if (Mask.Ch = 1) and
       (Res[High(Res)].Coord - Res[0].Coord  + 1 > 85 / FScanStep) then // ��� ������ ����� ����� 1 ������ - ��������� ������
    begin
      F1:= False;
      SetLength(Res2, 0);

      Th:= - 1;
      for I:= 0 to High(Res) do Th:= Max(Th, Res[I].Ampl);
      Th:= 3 + Round(2/3 * (Th - 3));

      for I:= 0 to High(Res) do
      begin
        if Res[I].Ampl >= Th then F1:= True;
        if F1 and (Res[I].Ampl < Th) then
        begin
          F1:= False;
          PBSearchOnRes(Mask, Res2, BMState);
          SetLength(Res2, 0);
        end;
        if F1 then
        begin
          SetLength(Res2, Length(Res2) + 1);
          Res2[High(Res2)]:= Res[I];
        end;
      end;
      Exit;
    end;
    * )
  // ---------------------- ���������� ����� ----------------------------------

    New.Crd.StCrd:= Res[0].Coord;
    New.Crd.EdCrd:= Res[High(Res)].Coord;
    New.Del.StCrd:= Res[0].Delay;
    New.Del.EdCrd:= Res[High(Res)].Delay;

//    New.CCrd:= Res[Length(Res) div 2].Coord;
//    New.CDel:= Res[Length(Res) div 2].Delay;
    New.CCrd:= (Res[0].Coord + Res[High(Res)].Coord) div 2;
    New.CDel:= (Res[0].Delay + Res[High(Res)].Delay) div 2;

    New.A:= 0;
    for J:= 0 to High(Res) do New.A:= Max(New.A, Res[J].Ampl); // ��������� �����

//    for J:= 0 to High(Res) do New.E:= New.E + Sqr(Res[J].Ampl); // ������� �����
//    New.E:= New.E / Length(Res);

    SetPBTyp(New, 1, pbtNotSet);

    New.EchoCount:= Length(Res);
//    Angle:= GetLen(New.Del) / (Res[High(Res)].Coord - Res[0].Coord + 1);
//    Angle:= (New.Del.EdCrd - New.Del.StCrd)  / (Res[High(Res)].Coord - Res[0].Coord + 1);

    {$IFDEF RECDEBUG}
//    KfF:= Length(Res) / Abs(Res[High(Res)].Coord - Res[0].Coord + 1); // ---------------------- ����������� ���������� -----------------------------
//    CalcCoeff(Mask.Ch, False, Res, KfD, KfA); // --------------- ������������ ����� � ������������� ��������� --------------
//    New.KfDelay:= KfD;
//    New.KfAmpl:= KfA;
//    New.KfFill:= KfF;
//    New.KFAngle:= Angle;
    {$ENDIF}

    if Mask.Ch in [1, 6, 7] then
    begin
//      Crd:= (New.Crd.StCrd + New.Crd.EdCrd) div 2;  // ���������� ������� ������ ���� ������� �������
      Crd:= Round((New.Crd.StCrd + (ChList[Mask.Ch].Shift[2] + ChList[Mask.Ch].RedPar[2] * New.Del.StCrd) / FScanStep +
                   New.Crd.EdCrd + (ChList[Mask.Ch].Shift[2] + ChList[Mask.Ch].RedPar[2] * New.Del.EdCrd) / FScanStep) / 2);  // ���������� ������� ������ ���� ������� �������

      D:= (FBSignal.FBSPos[Mask.R, 0].MaxDelay + FBSignal.FBSPos[Mask.R, 0].MinDelay) div 2; // ���������� �������� ������� �������
      for J:= 0 to High(FBSignal.FBSPos[Mask.R]) - 1 do
        if ((Crd >= FBSignal.FBSPos[Mask.R, J].Crd) and
            (Crd <  FBSignal.FBSPos[Mask.R, J + 1].Crd)) or
            (J = High(FBSignal.FBSPos[Mask.R])) then
        begin
          K:= J;
          D:= (FBSignal.FBSPos[Mask.R, K].MaxDelay + FBSignal.FBSPos[Mask.R, K].MinDelay) div 2; // ���������� �������� ������� �������
          Break;
        end;

      if Mask.Ch in [6, 7] then
      begin
        DelayTh:= Round(115 + 31 * (D / 3 - 49) / 12);
        if Min(New.Del.StCrd, New.Del.EdCrd) > DelayTh then SetPBTyp(New, 0, pbtConstruct);
      end;

( *
      for I:= 0 to 5 do                                            // ��������� �� �������������� � ����������
        if (Mask.Ch = FindHoleParam[I].Ch) and
           (D >= FindHoleParam[I].BtmSglMin) and
           (D <= FindHoleParam[I].BtmSglMax) and
           (FindHoleParam[I].DZoneMin <= Res[0].Delay) and
           (FindHoleParam[I].DZoneMax >= Res[0].Delay) and
           (FindHoleParam[I].DZoneMin <= Res[High(Res)].Delay) and
           (FindHoleParam[I].DZoneMax >= Res[High(Res)].Delay) and
           (Angle >= FindHoleParam[I].AngleMin) and
           (Angle <= FindHoleParam[I].AngleMax) then
        begin
          for J:= 3 to 7 do                 // ������� ���������� �������� � ���������� 3, 4, 5, 6 � 7
          begin
            New.Dat[J]:= 0;
            for M:= 0 to High(Res) do
              if Res[M].Ampl + 2 < J then Inc(New.Dat[J]);
          end;
          if (GetLen(New.Crd) < 8) and (KfF < 0.7) then Continue; // ��� ����� ����� ������ ����� ������ ��������� ����������� ����������

          SetPBTyp(New, 2, pbtForHole);
          Break;
        end;

      {$IFDEF RECDEBUG}
      if New.Typ <> pbtForHole then
      begin
        F1:= False;
        for I:= 0 to 5 do                                            // ��������� �� �������������� � ����������
          if (Mask.Ch = FindHoleParam[I].Ch) and
             (D >= FindHoleParam[I].BtmSglMin) and
             (D <= FindHoleParam[I].BtmSglMax) then
          begin

            if not (FindHoleParam[I].DZoneMin <= Res[0].Delay)         then SetPBTyp(New, 1002, pbtNotSet);
            if not (FindHoleParam[I].DZoneMax >= Res[0].Delay)         then SetPBTyp(New, 1003, pbtNotSet);
            if not (FindHoleParam[I].DZoneMin <= Res[High(Res)].Delay) then SetPBTyp(New, 1004, pbtNotSet);
            if not (FindHoleParam[I].DZoneMax >= Res[High(Res)].Delay) then SetPBTyp(New, 1005, pbtNotSet);
            if not (Angle >= FindHoleParam[I].AngleMin)                then SetPBTyp(New, 1006, pbtNotSet);
            if not (Angle <= FindHoleParam[I].AngleMax)                then SetPBTyp(New, 1007, pbtNotSet);

            F1:= True;
            Break;
          end;

        if not F1 then SetPBTyp(New, 1001, pbtNotSet) else // �� ��� ��
      end;
      {$ENDIF}
      * )

//      SetPBTyp(New, 0, pbtNotSet);
    end;

    {$IFDEF RECDEBUG}
    K:= - 1;
    for J:= 0 to High(Res) do
    begin
      if (K <> - 1) and (J > 0) and (Res[J].Coord = Res[J - 1].Coord) then
      begin
        New.Items[K].StDelay:= Min(Res[J].Delay - 2, Res[J - 1].Delay - 2);
        New.Items[K].EdDelay:= Max(Res[J].Delay + 2, Res[J - 1].Delay + 2);
      end
      else
      begin
        K:= Length(New.Items);
        SetLength(New.Items, K + 1);
        New.Items[K].Crd:= Res[J].Coord;
        New.Items[K].StDelay:= Res[J].Delay - 2;
        New.Items[K].EdDelay:= Res[J].Delay + 2;
        New.Items[K].Ampl:= Res[J].Ampl;
      end;
    end;
   {$ENDIF}
{
    J:= Length(FPBList[Mask.R, Mask.Ch]);
    SetLength(FPBList[Mask.R, Mask.Ch], J + 1);
    FPBList[Mask.R, Mask.Ch, J]:= New;
}

   Inc(FPBListIdx[Mask.R, Mask.Ch]);
   if FPBListIdx[Mask.R, Mask.Ch] >= FPBListCap[Mask.R, Mask.Ch] then
   begin
     FPBListCap[Mask.R, Mask.Ch]:= FPBListCap[Mask.R, Mask.Ch] * 2;
     SetLength(FPBList[Mask.R, Mask.Ch], FPBListCap[Mask.R, Mask.Ch]);
   end;
   FPBList[Mask.R, Mask.Ch, FPBListIdx[Mask.R, Mask.Ch]]:= New;
  end;  * )
end;
 *)
procedure TRecData.PBSearch;
var
  R: RRail_;
  I, J, K: Integer;
  KOut, KIn: Integer;
  Ch: Integer;
  Flg: Boolean;

begin

  FMaskShow:= TMaskShow.Create(FDatSrc, Self, @FPBList, FBSignal, FFiltr);
  FMaskShow.Calc(FStCrd, FEdCrd);

// ----------------------------------------------------------------------------------------------------
// --- ���������� ����� 1 ������                                                                    ---
// --- ���������� �� ���������� ����� �� 1 ���� ���� ��� ���������� ������������� ����� ����������� ---
// ----------------------------------------------------------------------------------------------------

  for R:= rLeft to rRight do
    for I:= 0 to High(FPBList[R, 1]) do
    begin
      K:= 0;
      J:= I - 1;
      while J >= 0 do // ������������ ���������� ����� ����� ����� I
      begin
        if Abs(GetCentr(FPBList[R, 1, I].Crd) - GetCentr(FPBList[R, 1, J].Crd)) > 500 / FScanStep then Break;
        Inc(K);
        Dec(J);
      end;
      J:= I + 1;      // ������������ ���������� ����� ������ ����� I
      while J <= High(FPBList[R, 1]) do
      begin
        if Abs(GetCentr(FPBList[R, 1, I].Crd) - GetCentr(FPBList[R, 1, J].Crd)) > 500 / FScanStep then Break;
        Inc(K);
        Inc(J);
      end;
                                         // �� ���������� ����� ������������� ����� (������ ����) �������� ����� ����������
      J:= 7;
      if K in [ 0..  4] then J:= 3 else
      if K in [ 5.. 12] then J:= 4 else
      if K in [13.. 25] then J:= 5 else
      if K in [24.. 51] then J:= 6 else
      if K in [52..255] then J:= 7;
      KOut:= FPBList[R, 1, I].Dat[J];                        // ������� ���������� �������� ���������� �� �����
      KIn:= FPBList[R, 1, I].EchoCount - FPBList[R, 1, I].Dat[J];  // ������� ���������� �������� ���������� � �����

      with FPBList[R, 1, I] do
      begin
        Flg:= ((EchoCount >= 2) and (EchoCount <= 4) and (SummAmpl_ >= RecConfig.PBCountAmpl[EchoCount])) or
              (EchoCount > 4);
        if (not Flg) or ((KIn / EchoCount) < 0.5) then Typ:= Typ + [pbtiDel];
      end;
    end;
end;

// -----------------------------------------------------------------------------
// --- ����� ����� � ������ ������� --------------------------------------------
// -----------------------------------------------------------------------------

function TRecData.FindBottomSignalHolesBlockOK(StartDisCoord: Integer): Boolean;
var
  R: RRail_;
  I: Integer;
  F: Integer;
  Flg: Boolean;
  IdxA1, IdxA2: Integer;
  IdxB1, IdxB2: Integer;
  BM: Boolean;

begin
  for R:= rLeft to rRight do
  begin
    if FBSFlag2 <> FDatSrc.BackMotion then    // ���������� ����������� �������� ?
    begin
      if FBSFlag[R] = 1 then                  // ���� �� - �� ��������� ������� ����� ������� �����������
      begin
        if (FDatSrc.CurDisCoord - 1 - FCrd[R, 1] > 1) then
        begin
          F:= Length(FBSPice[R]);
          SetLength(FBSPice[R], F + 1);
          FBSPice[R, F].R.StCrd:= FCrd[R, 1];
          FBSPice[R, F].R.EdCrd:= FDatSrc.CurDisCoord;
          GetCoordState((FDatSrc.CurDisCoord - 1 + FCrd[R, 1]) div 2, IdxA1, IdxA2, BM);
          FBSPice[R, F].BM:= BM;
        end;
        FCrd[R, 1]:= FDatSrc.CurDisCoord;
      end;
    end
    else
    begin
      for I:= FIdx[R, 1] to High(FBSignal.FBSPos[R]) - 1 do // ���������� ������� ������ ���� ������� �������
      begin
        if (FDatSrc.CurDisCoord >= FBSignal.FBSPos[R, I].Crd) and
           (FDatSrc.CurDisCoord <= FBSignal.FBSPos[R, I + 1].Crd) then
        begin
          FIdx[R, 1]:= I;
          Break;
        end;
      end;
                                                // ���������� ������� ������� ������� �� ������ ����������
      Flg:= False;
      with FDatSrc.CurEcho[R, 1] do
        for I:= 1 to Count do
        begin
          Flg:= (Delay[I] >= FBSignal.FBSPos[R, FIdx[R, 1]].MinDelay) and
                (Delay[I] <= FBSignal.FBSPos[R, FIdx[R, 1]].MaxDelay) and
                (Ampl[I] >= 6);       // ����� �� ������� ������� �� 0 ��
          if Flg then Break;
        end;

      case FBSFlag[R] of
        0: if not Flg then  // ���� ����� ������� ��
           begin
             FBSFlag[R]:= 1;
             FCrd[R, 1]:= FDatSrc.CurDisCoord; // ��������� ���������� �����
           end;
        1: if Flg then      // ���� ����� ������ ��������
           begin
             if (FDatSrc.CurDisCoord - 1       // �������� ���������� �����
                  - FCrd[R, 1] + 1 >= RecConfig.BSPiceMinSize) then
             begin
               F:= Length(FBSPice[R]);
               SetLength(FBSPice[R], F + 1);
               FBSPice[R, F].R.StCrd:= FCrd[R, 1];
               FBSPice[R, F].R.EdCrd:= FDatSrc.CurDisCoord - 1;
               GetCoordState((FDatSrc.CurDisCoord - 1 + FCrd[R, 1]) div 2, IdxA1, IdxA2, BM);
               FBSPice[R, F].BM:= BM;
             end;
             FBSFlag[R]:= 0;
           end;
      end;
    end;
  end;
  FBSFlag2:= FDatSrc.BackMotion;
  Result:= True;
end;

function TRecData.TestBtmSgnCrd(R: RRail_; Crd: Integer): Boolean;
var
  I: Integer;

begin
  Result:= False;
  for I:= 0 to High(FPBList[R, 0]) do
    if MyTypes.PtInRange(Crd, FPBList[R, 0, I].Crd) then Exit;
  Result:= True;
end;

function TRecData.GetRedCrd(Ch, CCrd, CDel: Integer): Integer;
begin
  Result:= Round(CCrd + 1 * (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * CDel) / FScanStep);
end;

procedure TRecData.FindBottomSignalHoles;
var
  R: RRail_;
  I, J, K, L, C1, C2, F: Integer;
  St1, St2, Len1, Len2: Integer;
  Flg: Boolean;
  Flg1: Boolean;
  Flg2: Boolean;
  FPt1: TIntegerDynArray;
  FPt2: TIntegerDynArray;
  Crd: Integer;
  PtList: TIntegerDynArray;
  StIdx, Count: Integer;

begin
                   // --- ������ 1 --------------------------------------------
                                           // ���� ����� - ��� ��� ���� ��� ����������
  for R:= rLeft to rRight do
  begin
    FBSFlag[R]:= 0;
    FIdx[R, 1]:= 0;
  end;

  FBSFlag2:= False;
  FDatSrc.LoadData(FStCrd, FEdCrd, 0, FindBottomSignalHolesBlockOK);

  for R:= rLeft to rRight do           // ���������� ������
    if (FBSFlag[R] > 0) then
    begin
      L:= Length(FBSPice[R]);
      SetLength(FBSPice[R], L + 1);
      FBSPice[R, L].R.StCrd:= FCrd[FRail, 1];
      FBSPice[R, L].R.EdCrd:= FEdCrd - 1;
    end;

             // --- ������ 2 --------------------------------------------

  for R:= rLeft to rRight do           // ���������� ����� ��������� ����� - ������������ ����������
  begin
    L:= 0;
    C1:= 0;
    C2:= 0;
    while L <= High(FBSPice[R]) do
    begin
      Inc(C1, FBSPice[R, L].R.EdCrd - FBSPice[R, L].R.StCrd + 1);      // ����� ������ �����
      if L <> High(FBSPice[R]) then
                                                                       // ������� ���������� �����
        if (FBSPice[R, L + 1].R.StCrd -
            FBSPice[R, L].R.EdCrd > RecConfig.BSHoleTh) or             // 1. �������� ������ ������ > ������  (�� ����� ��������� �� �� ����� ��� �� X ��)
           (FBSPice[R, L + 1].BM <> FBSPice[R, L].BM) then             // 2. ���������� ����������� ��������
        begin
          if C1 >= RecConfig.BSHoleMinSize then                        // ����� ������ ���� ������ ������
          begin
            if FBSPice[R, C2].R.EdCrd > RecConfig.BSHoleSkip then      // ���������� ����� � ������ �����
            begin
              F:= Length(FPBList[R, 0]);
              SetLength(FPBList[R, 0], F + 1);                         // ��������� ����� - ��� ��������� �����
              FPBList[R, 0, F].Crd.StCrd:= FBSPice[R, C2].R.StCrd;
              FPBList[R, 0, F].Crd.EdCrd:= FBSPice[R, L].R.EdCrd;
              FPBList[R, 0, F].CCrd:= GetCentr(FPBList[R, 0, F].Crd);
              FPBList[R, 0, F].EchoCount:= GetLen(FPBList[R, 0, F].Crd);
              FPBList[R, 0, F].Typ:= [];
              if FBSPice[R, C2].BM then
                FPBList[R, 0, F].Typ:= FPBList[R, 0, F].Typ + [pbtiInBMZ];
              {$IFDEF RECDEBUG}
              SetLength(FPBList[R, 0, F].Items, 2);
              FPBList[R, 0, F].Items[0].Crd:= FBSPice[R, C2].R.StCrd;
              FPBList[R, 0, F].Items[1].Crd:= FBSPice[R, L].R.EdCrd;
              {$ENDIF}
            end;
            Inc(L);
          end
          else L:= C2 + 1;
          C2:= L;
          C1:= 0;
          Continue;
        end;
      Inc(L);
    end;

    if  C1 >= RecConfig.BSHoleMinSize then                          // ����� ������ ���� ������ ������
      if FBSPice[R, C2].R.EdCrd > RecConfig.BSHoleSkip then         // ���������� ����� � ������ �����
      begin
        F:= Length(FPBList[R, 0]);                                  // ��������� ����� - ��� ��������� �����
        SetLength(FPBList[R, 0], F + 1);
        FPBList[R, 0, F].Crd.StCrd:= FBSPice[R, C2].R.StCrd;
        FPBList[R, 0, F].Crd.EdCrd:= FBSPice[R, High(FBSPice[R])].R.EdCrd;
        FPBList[R, 0, F].EchoCount:= GetLen(FPBList[R, 0, F].Crd);
        FPBList[R, 0, F].CCrd:= GetCentr(FPBList[R, 0, F].Crd);
//        FPBList[R, 0, F].Dat[3]:= Ord(FBSPice[R, C2].BM);
        FPBList[R, 0, F].Typ:= [];
        if FBSPice[R, C2].BM then FPBList[R, 0, F].Typ:= [pbtiInBMZ];
// ASD       SetPBTyp(FPBList[R, 0, F], 5, pbtForHole);
//        SetPBTyp(FPBList[R, 0, F], 5, pbtNotSet);

        {$IFDEF RECDEBUG}
        SetLength(FPBList[R, 0, F].Items, 2);
        FPBList[R, 0, F].Items[0].Crd:= FBSPice[R, C2].R.StCrd;
        FPBList[R, 0, F].Items[1].Crd:= FBSPice[R, High(FBSPice[R])].R.EdCrd;
        {$ENDIF}
      end;
  end;

  for R:= rLeft to rRight do       // ��������� ����� - ��� ��������� �����
  begin
    K:= 0;
    for I:= 0 to High(FPBList[R, 0]) do
    begin
      FPBList[R, 0, I].Del.StCrd:= FBSignal.FBSPos[R, K].MinDelay;
      FPBList[R, 0, I].Del.EdCrd:= FBSignal.FBSPos[R, K].MaxDelay;
      {$IFDEF RECDEBUG}
      for J:= K to High(FBSignal.FBSPos[R]) - 1 do // ���������� ������� ������ ���� ������� �������
        if (FPBList[R, 0, I].Items[0].Crd >= FBSignal.FBSPos[R, J].Crd) and
           (FPBList[R, 0, I].Items[0].Crd <  FBSignal.FBSPos[R, J + 1].Crd) then
        begin
          K:= J;
          Break;
        end;
//      FPBList[R, 0, I].Items[0].StDelay:= FBSignal.FBSPos[R, K].MinDelay;
//      FPBList[R, 0, I].Items[0].EdDelay:= FBSignal.FBSPos[R, K].MaxDelay;
//      FPBList[R, 0, I].Items[1].StDelay:= FBSignal.FBSPos[R, K].MinDelay;
//      FPBList[R, 0, I].Items[1].EdDelay:= FBSignal.FBSPos[R, K].MaxDelay;
      FPBList[R, 0, I].Items[0].Delay:= (FBSignal.FBSPos[R, K].MinDelay + FBSignal.FBSPos[R, K].MaxDelay) div 2;
      FPBList[R, 0, I].Items[1].Delay:= (FBSignal.FBSPos[R, K].MinDelay + FBSignal.FBSPos[R, K].MaxDelay) div 2;

      {$ENDIF}
    end;
  end;

         // �������� ������� ��������� ������ ��� ���������� ��

  SetLength(PtList, 1000);
  StIdx:= 0;
  for R:= rLeft to rRight do
  begin
    C1:= 0;
    for I:= 0 to High(FPBList[R, 0]) do
//      if FPBList[R, 0, I].Dat[3] <> 1 then      // ����� ����� �� � ���� �������� �����
      if not (pbtiInBMZ in FPBList[R, 0, I].Typ) then      // ����� ����� �� � ���� �������� �����
      begin
        Flg:= False;
        for J:= C1 to High(FBMList.FBMData) do                         // ��������� �������� �� ����� � ������� ���� ��
          if MyTypes.InRange(FPBList[R, 0, I].Crd, FBMList.FBMData[J].DisRange) then
          begin
            K:= J;
            Flg:= True;
//            C1:= J;
            Break;
          end;

        if Flg then         // ���� �������� ����� ��������� ���� �� ��� (����������) �������������
        begin
          Flg1:= True;
//          for Crd:= FPBList[R, 0, I].Crd.StCrd to FPBList[R, 0, I].Crd.EdCrd do
          Crd:= FPBList[R, 0, I].Crd.StCrd;
          while Crd <= FPBList[R, 0, I].Crd.EdCrd do
          begin
//            SetLength(PtList, 0);
//            FDatSrc.DisToDisCoords(Crd, PtList);
            Count:= 0;
            FDatSrc.DisToDisCoords_(Crd, PtList, StIdx, Count);

            Flg2:= False;
            for L:= 0 to Count - 1  {High(PtList)} do
              if TestBtmSgnCrd(R, PtList[L] {, I}) then
              begin
                Flg2:= True;
                Break;
              end;

            if not Flg2 then
            begin
              Flg1:= False;
              Break;
            end;
            Inc(Crd, 3);
          end;
//          FPBList[R, 0, I].Dat[4]:= Ord(Flg1);
          if Flg1 then FPBList[R, 0, I].Typ:= FPBList[R, 0, I].Typ + [pbtiReCheck];
        end;
      end; // Dat[4]:= 0;
  end;

  SetLength(PtList, 0);
end;

// -----------------------------------------------------------------------------
// --- ���������� ����� � ����� ------------------------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.SortPBSearch;
var
  R_: RRail_;
  I, J, K, Ch, L, L1, L2, L3, L4, MinLen: Integer;
  SortTmp2: TPBItem;
  Angle1, Angle2: Single;
  NewCrd, NewDel: TRange;
  CS: TChartShape;

function Part(L, R: integer):integer;
var
  V, I, J, B: integer;

begin
  V:= FPBList[R_, Ch, r].Crd.EdCrd;
  I:= L - 1;
  J:= R;
  repeat
    repeat
      Dec(J)
    until (FPBList[R_, Ch, J].Crd.EdCrd <= V) or (J = I + 1);
    repeat
      Inc(I)
    until (FPBList[R_, Ch, I].Crd.EdCrd >= V) or (I = J - 1);

    SortTmp2:= FPBList[R_, Ch, I];
    FPBList[R_, Ch, I]:= FPBList[R_, Ch, J];
    FPBList[R_, Ch, J]:= SortTmp2;

  until I >= J;

  FPBList[R_, Ch, J]:= FPBList[R_, Ch, I];
  FPBList[R_, Ch, I]:= FPBList[R_, Ch, R];
  FPBList[R_, Ch, R]:= SortTmp2;

  Part:= I;
end;

procedure QuickSort(L, T: integer);
var
  I: Integer;

begin
  if L < T then
  begin
    I:= Part(L, T);
    QuickSort(L, I - 1);
    QuickSort(I + 1, T);
  end;
end;

begin
  for R_:= rLeft to rRight do
    for Ch:= 0 to 7 do
    begin
      FPBMaxLen[R_, Ch]:= 0;
      for I:= 0 to High(FPBList[R_, Ch]) do
        FPBMaxLen[R_, Ch]:= Max(FPBMaxLen[R_, Ch], GetLen(FPBList[R_, Ch, I].Crd));
      QuickSort(1, High(FPBList[R_, Ch]));
    end;
end;

// -----------------------------------------------------------------------------
// --- ������������ ���� �� �� ������� ������ �� -------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.BJButton;
var
  R: RRail_;
  Flg: Boolean;
  Flg1: Boolean;
  Flg2: Boolean;
  StIdx, EdIdx, A, B, C, D, I, J, K, L, C1, C2, Count, Step : Integer;

  TestIdx1: Integer;
  TestIdx2: Integer;
  RR: TRange;

  FFullBJB_: array of TFlgRange;
  CurR, R1, R2: TRange;
  FBJPtList: TIntegerDynArray;

  HighFFullBJB_: Integer;
  Cap, StIdx2: Integer;

begin

 // ---------------------------------------------------------- ����� �� �������� ������� ������ ��

  Flg:= False;
  for I:= 0 to FDatSrc.EventCount - 1 do
  begin
    if (not Flg) and (FDatSrc.Event[I].ID = EID_StBoltStyk) then
    begin
      J:= I;
      Flg:= True;
    end;
    if Flg and (FDatSrc.Event[I].ID = EID_EndBoltStyk) then
    begin
      C1:= FDatSrc.Event[J].DisCoord;
      C2:= FDatSrc.Event[I].DisCoord;
      if (C2 >= FStCrd) and (C1 <= FEdCrd) then
      begin
        if C1 < FStCrd then C1:= FStCrd;
        if C2 > FEdCrd then C2:= FEdCrd;
        SetLength(FBJButton, Length(FBJButton) + 1);         // ���� ������� ������ ��
        FBJButton[High(FBJButton)].StCrd:= C1;
        FBJButton[High(FBJButton)].EdCrd:= C2;
      end;
      Flg:= False;
    end;
  end;

  if Flg then                                                // ����������
  begin
    C1:= FDatSrc.Event[J].DisCoord;
    C2:= FDatSrc.Event[FDatSrc.EventCount - 1].DisCoord;
    SetLength(FBJButton, Length(FBJButton) + 1);         // ���� ������� ������ ��
    FBJButton[High(FBJButton)].StCrd:= C1;
    FBJButton[High(FBJButton)].EdCrd:= C2;
  end;

 // ---------------------------------------------------------- ������� ����� �� ��� - ����������� ���
(*
//  SetLength(FBJPtList, 1000);
  for I:= 0 to High(FBJButton) do
  begin
    for C:= FBJButton[I].StCrd to FBJButton[I].EdCrd do
    begin

//      FDatSrc.DisToDisCoords_(C, FBJPtList, Count);           // ��������� �����
//      for J:= 0 to Count - 1{High(FBJPtList)} do

      SetLength(FBJPtList, 0);
      FDatSrc.DisToDisCoords(C, FBJPtList);           // ��������� �����
      for J:= 0 to High(FBJPtList) do
      begin
        Flg:= False;
        for K:= 0 to High(FFullBJB_) do
        begin
          if (FFullBJB_[K].Dir = + 1) and
             (FBJPtList[J] - FFullBJB_[K].R.EdCrd = 1) then
          begin
            FFullBJB_[K].R.EdCrd:= FBJPtList[J];
            Flg:= True;
            Break;
          end;
          if (FFullBJB_[K].Dir = - 1) and
             (FFullBJB_[K].R.StCrd - FBJPtList[J] = 1) then
          begin
            FFullBJB_[K].R.StCrd:= FBJPtList[J];
            Flg:= True;
            Break;
          end;
        end;

        if not Flg then
          for K:= 0 to High(FFullBJB_) do
          begin
            if (FFullBJB_[K].Dir = 0) and
               (FBJPtList[J] - FFullBJB_[K].R.EdCrd = 1) then
            begin
              FFullBJB_[K].R.EdCrd:= FBJPtList[J];
              FFullBJB_[K].Dir:= + 1;
              Flg:= True;
            end;
            if (FFullBJB_[K].Dir = 0) and
               (FFullBJB_[K].R.StCrd - FBJPtList[J] = 1) then
            begin
              FFullBJB_[K].R.StCrd:= FBJPtList[J];
              FFullBJB_[K].Dir:= - 1;
              Flg:= True;
            end;
          end;

        if not Flg then
        begin
          SetLength(FFullBJB_, Length(FFullBJB_) + 1);
          FFullBJB_[High(FFullBJB_)].R.StCrd:= FBJPtList[J];
          FFullBJB_[High(FFullBJB_)].R.EdCrd:= FBJPtList[J];
          FFullBJB_[High(FFullBJB_)].Dir:= 0;
          FFullBJB_[High(FFullBJB_)].Flg:= True;
        end;
      end;
    end;
  end;
  SetLength(FBJPtList, 0);
*)

  HighFFullBJB_:= - 1;
  Cap:= 1000;
  SetLength(FFullBJB_, Cap);
  Step:= 1;
  StIdx:= 0;
  StIdx2:= 0;

  SetLength(FBJPtList, 1000);
  for I:= 0 to High(FBJButton) do
  begin
    C:= FBJButton[I].StCrd;

    for K:= 0 to HighFFullBJB_ do
      if FFullBJB_[K].R.EdCrd < C - FBMList.FMaxBMLen then StIdx2:= K
                                                      else Break;
    repeat
      Count:= 0;
      FDatSrc.DisToDisCoords_(C, FBJPtList, StIdx, Count);           // ��������� �����
      StIdx:= Max(StIdx - 10, 0);
      for J:= 0 to Count - 1 do
      begin
        Flg:= False;
        for K:= StIdx2 to HighFFullBJB_ do
        begin
          if (FFullBJB_[K].Dir = + 1) and
             (FBJPtList[J] - FFullBJB_[K].R.EdCrd = Step) then
          begin
    //        FFullBJB_[K].R.EdCrd:= FBJPtList[J];

            FFullBJB_[K].R.StCrd:= Min(FBJPtList[J], FFullBJB_[K].R.StCrd);
            FFullBJB_[K].R.EdCrd:= Max(FBJPtList[J], FFullBJB_[K].R.EdCrd);

            Flg:= True;
            Break;
          end;
          if (FFullBJB_[K].Dir = - 1) and
             (FFullBJB_[K].R.StCrd - FBJPtList[J] = Step) then
          begin

//            FFullBJB_[K].R.StCrd:= FBJPtList[J];

            FFullBJB_[K].R.StCrd:= Min(FBJPtList[J], FFullBJB_[K].R.StCrd);
            FFullBJB_[K].R.EdCrd:= Max(FBJPtList[J], FFullBJB_[K].R.EdCrd);

            Flg:= True;
            Break;
          end;
        end;

        if not Flg then
          for K:= StIdx2 to HighFFullBJB_ do
          begin
            if (FFullBJB_[K].Dir = 0) and
               (FBJPtList[J] - FFullBJB_[K].R.EdCrd = Step) then
            begin

//              FFullBJB_[K].R.EdCrd:= FBJPtList[J];

              FFullBJB_[K].R.StCrd:= Min(FBJPtList[J], FFullBJB_[K].R.StCrd);
              FFullBJB_[K].R.EdCrd:= Max(FBJPtList[J], FFullBJB_[K].R.EdCrd);

              FFullBJB_[K].Dir:= + 1;
              Flg:= True;
            end;
            if (FFullBJB_[K].Dir = 0) and
               (FFullBJB_[K].R.StCrd - FBJPtList[J] = Step) then
            begin
//              FFullBJB_[K].R.StCrd:= FBJPtList[J];

              FFullBJB_[K].R.StCrd:= Min(FBJPtList[J], FFullBJB_[K].R.StCrd);
              FFullBJB_[K].R.EdCrd:= Max(FBJPtList[J], FFullBJB_[K].R.EdCrd);

              FFullBJB_[K].Dir:= - 1;
              Flg:= True;
            end;
          end;

        if not Flg then
        begin
          Inc(HighFFullBJB_);
          if HighFFullBJB_ = Cap then
          begin
            Cap:= 2 * Cap;
            SetLength(FFullBJB_, Cap);
          end;

          FFullBJB_[HighFFullBJB_].R.StCrd:= FBJPtList[J];
          FFullBJB_[HighFFullBJB_].R.EdCrd:= FBJPtList[J];
          FFullBJB_[HighFFullBJB_].Dir:= 0;
          FFullBJB_[HighFFullBJB_].Flg:= True;
        end;
      end;
      Inc(C, Step);
      if C > FBJButton[I].EdCrd then Break;
    until False;
  end;
  SetLength(FBJPtList, 0);
  SetLength(FFullBJB_, HighFFullBJB_ + 1);

 // ---------------------------------------------------------- ����������� ��� - ��� ��������� ��� ������ ��� ��

  I:= 0;
  while I <= High(FFullBJB_) do
  begin
    if FFullBJB_[I].Flg then
    begin
      CurR:= FFullBJB_[I].R;
      repeat
        Flg:= True;
        J:= I + 1;
//        J:= 0;
        while J <= High(FFullBJB_) do
        begin
//          if FFullBJB_[I].Flg then
            if FFullBJB_[J].Flg and TestRangeIntersec(CurR, FFullBJB_[J].R) then
            begin
              CurR:= OutRange(CurR, FFullBJB_[J].R);
              FFullBJB_[J].Flg:= False;
              Flg:= False;
            end;
          Inc(J);
        end;
      until Flg;
      SetLength(FBJSearch, Length(FBJSearch) + 1);
      FBJSearch[High(FBJSearch)]:= CurR;
    end;
    Inc(I);
  end;
  SetLength(FFullBJB_, 0);

   // ---------------------------------------------------------- ����������

  for I:= 0 to High(FBJSearch) - 1 do
  begin
    K:= I;
    for J:= I + 1 to High(FBJSearch) do
      if FBJSearch[K].StCrd > FBJSearch[J].StCrd then K:= J;
    if K <> I then
    begin
      RR:= FBJSearch[I];
      FBJSearch[I]:= FBJSearch[K];
      FBJSearch[K]:= RR;
    end;
  end;

   // ---------------------------------------------------------- ����������� ��� - ��� ��������� ��� ����������� ��

  if Length(FBJSearch) = 1 then
  begin
    SetLength(FBJView, 1);
    FBJView[0]:= FBJSearch[0];
  end
  else
  if Length(FBJSearch) > 1 then
  begin
    CurR:= FBJSearch[0];

    StIdx:= 0;
    repeat
      TestIdx1:= StIdx;                         // ����� ������� � ���������
      TestIdx2:= StIdx + 1;
      if TestIdx2 > High(FBJSearch) then
      begin
        SetLength(FBJView, Length(FBJView) + 1);
        FBJView[High(FBJView)]:= FBJSearch[TestIdx1];
        Break;  // ���� ���������� ��� - �� �����
      end;

      Flg:= True;
      repeat
                                                // ���������� �� �����

        Flg2:= (Abs(FBJSearch[TestIdx1].StCrd - FBJSearch[TestIdx2].StCrd) < 250) or
               (Abs(FBJSearch[TestIdx1].StCrd - FBJSearch[TestIdx2].EdCrd) < 250) or
               (Abs(FBJSearch[TestIdx1].EdCrd - FBJSearch[TestIdx2].StCrd) < 250) or
               (Abs(FBJSearch[TestIdx1].EdCrd - FBJSearch[TestIdx2].EdCrd) < 250);

        if not Flg2 then                // ���������� �� ������� - ������� ������ � ������������� �� � SYSCRD
        begin
          R1:= Range(FDatSrc.DisToSysCoord(FBJSearch[TestIdx1].StCrd), FDatSrc.DisToSysCoord(FBJSearch[TestIdx1].EdCrd));
          R2:= Range(FDatSrc.DisToSysCoord(FBJSearch[TestIdx2].StCrd), FDatSrc.DisToSysCoord(FBJSearch[TestIdx2].EdCrd));
          C1:= GetCentr(R1);
          C2:= GetCentr(R2);
          Flg2:= Abs(C1 - C2) < 1000 / FScanStep;
        end;

        if not Flg2 then Break; // ���� ��� �� ����� - ����� �����

//        if not TestRangeIntersec(R1, R2) then Break; // ���� ��� �� ����� - ����� �����

        Inc(TestIdx1);
        Inc(TestIdx2);
        if TestIdx2 > High(FBJSearch) then Break;
      until False;

      SetLength(FBJView, Length(FBJView) + 1);
      FBJView[High(FBJView)].StCrd:= MaxInt;
      FBJView[High(FBJView)].EdCrd:= - MaxInt;
      for K:= StIdx to TestIdx2 - 1 do
      begin
        FBJView[High(FBJView)].StCrd:= Min(FBJView[High(FBJView)].StCrd, FBJSearch[K].StCrd);
        FBJView[High(FBJView)].EdCrd:= Max(FBJView[High(FBJView)].EdCrd, FBJSearch[K].EdCrd);
      end;

      if TestIdx2 > High(FBJSearch) then Break;
      StIdx:= TestIdx2;
      if StIdx > High(FBJSearch) then Break;
    until False;
  end;
  SetLength(FFullBJB_, 0);
end;

// -----------------------------------------------------------------------------
// ------------- �������� ����� � ����� ��������� ------------------------------
// -----------------------------------------------------------------------------
(*
procedure TRecData.MakeSigns;
var
  R: RRail;
  Ch: Integer;
  I, J, K, L, C: Integer;
  TmpR: TRange;
  SortTmp2: TMaskPoint2;

begin
(* ASD
  for R:= rLeft to rRight do
    for Ch:= 0 to 7 do
      if Ch in [0, 1, 6, 7] then
        for J:= 0 to High(FPBList[R, Ch]) do
          if (FPBList[R, Ch, J].Typ in [pbtForHole, pbtForHoleDef, pbtForHoleBolt]) and
             (((Ch = 0) and (FPBList[R, Ch, J].Dat[3] <> 1)) or (Ch <> 0)) then   // ��� ������ ���������� ����� ...
          begin
            TmpR.StCrd:= Round(FPBList[R, Ch, J].Crd.StCrd + (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * FPBList[R, Ch, J].Del.StCrd) / FScanStep);
            TmpR.EdCrd:= Round(FPBList[R, Ch, J].Crd.EdCrd + (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * FPBList[R, Ch, J].Del.EdCrd) / FScanStep);
            L:= Length(FPoints2[R]);
            SetLength(FPoints2[R], L + 1);
            FPoints2[R, L].Ch:= Ch;
            FPoints2[R, L].Crd:= TmpR;
            case Ch of
              0: begin
                   if GetLen(FPoints2[R, L].Crd) > 200 / FScanStep then // ��������� ������������� ����� 0 ������
                   begin
                     FPoints2[R, L].Ch:= Ch;
                     FPoints2[R, L].Crd.StCrd:= TmpR.StCrd;
                     FPoints2[R, L].Crd.EdCrd:= Round(TmpR.StCrd + 40 / FScanStep);
                     FPoints2[R, L].Depth.StCrd:= 0;
                     FPoints2[R, L].Depth.EdCrd:= 180;
                     FPoints2[R, L].PBIdx:= J;
                     FPoints2[R, L].UsedIdx:= - 1;

                     L:= Length(FPoints2[R]);
                     SetLength(FPoints2[R], L + 1);
                     FPoints2[R, L].Ch:= Ch;
                     FPoints2[R, L].Crd.StCrd:= Round(TmpR.EdCrd - 40 / FScanStep);
                     FPoints2[R, L].Crd.EdCrd:= TmpR.EdCrd;
                   end;

                   FPoints2[R, L].Depth.StCrd:= 0;
                   FPoints2[R, L].Depth.EdCrd:= 180;
                 end;
              1: begin
                   FPoints2[R, L].Depth.StCrd:= Round(2.95 * Min(FPBList[R, Ch, J].Del.StCrd, FPBList[R, Ch, J].Del.EdCrd) / 3) - 15;
                   FPoints2[R, L].Depth.EdCrd:= Round(2.95 * Max(FPBList[R, Ch, J].Del.StCrd, FPBList[R, Ch, J].Del.EdCrd) / 3) + 15;
                   FPoints2[R, L].Crd.StCrd:= FPoints2[R, L].Crd.StCrd - Round(5 / FScanStep);
                   FPoints2[R, L].Crd.EdCrd:= FPoints2[R, L].Crd.EdCrd + Round(5 / FScanStep);
                 end;
              6: begin
                   FPoints2[R, L].Depth.StCrd:= Round(1.21 * Min(FPBList[R, Ch, J].Del.StCrd, FPBList[R, Ch, J].Del.EdCrd));
                   FPoints2[R, L].Depth.EdCrd:= Round(1.21 * Max(FPBList[R, Ch, J].Del.StCrd, FPBList[R, Ch, J].Del.EdCrd));
                   FPoints2[R, L].Crd.EdCrd:= FPoints2[R, L].Crd.EdCrd + Round(30 / FScanStep);
                 end;
              7: begin
                   FPoints2[R, L].Depth.StCrd:= Round(1.21 * Min(FPBList[R, Ch, J].Del.StCrd, FPBList[R, Ch, J].Del.EdCrd));
                   FPoints2[R, L].Depth.EdCrd:= Round(1.21 * Max(FPBList[R, Ch, J].Del.StCrd, FPBList[R, Ch, J].Del.EdCrd));
                   FPoints2[R, L].Crd.StCrd:= FPoints2[R, L].Crd.StCrd - Round(30 / FScanStep);
                 end;
            end;
            FPoints2[R, L].PBIdx:= J;
            FPoints2[R, L].UsedIdx:= - 1;
          end;

// -------------------------- C��������� ��������� --------------------------------------------------------------

  for R:= rLeft to rRight do
  begin
    FPoints2MaxLen[R]:= 0;
    for I:= 0 to High(FPoints2[R]) - 1 do
    begin
      FPoints2MaxLen[R]:= Max(FPoints2MaxLen[R], GetLen(FPoints2[R, I].Crd));
      K:= FPoints2[R, I].Crd.StCrd;
      C:= - 1;
      for J:= I + 1 to High(FPoints2[R]) do
      begin
        if K > FPoints2[R, J].Crd.StCrd then
        begin
          K:= FPoints2[R, J].Crd.StCrd;
          C:= J;
        end;
      end;
      if C <> - 1 then
      begin
        SortTmp2:= FPoints2[R, I];
        FPoints2[R, I]:= FPoints2[R, C];
        FPoints2[R, C]:= SortTmp2;
      end;
    end;
  end;
   * )
end;

// -----------------------------------------------------------------------------
// --- ����� ��������� - ����������� ��������� ������ ������� ------------------
// -----------------------------------------------------------------------------

procedure TRecData.SearchBoltHoles;
var
  R: RRail;
  I, J, K, L, Idx, Squar: Integer;
  TmpR: TRange;
  Def, Flg, Flg1, Flg2: Boolean;
  ChCount: array [0..7] of Integer;
  PBItem: TPBItem;
  TmpSet: TMySet;
  SetList: array [rLeft..rRight] of TMySetList;
  DelSet: array of Integer;

begin
// --------------------------- ����������� ������� ����������� ��������� ------------------------
(* ASD
//  RecDataForm.RichEdit1.Clear;
//  Log:= TStringList.Create;

  for R:= rLeft to rRight do
  begin
    SetList[R]:= TMySetList.Create;

//    Log.Add(Format('����: %d', [Ord(R)]));

    for I:= 0 to High(FPoints2[R]) - 1 do               // ������������� �������� - ������ � ������
    begin
      if FPoints2[R, I].UsedIdx <> - 1 then               // ��������� �� ������ �� ������� ������� � ���� �� ��� ��������� �������� - ���� ������ �� ��������� � ���������� ��������
      begin
        Flg2:= True;                                      // ������ ���� - ��� TmpSet ����� ���������
        Idx:= FPoints2[R, I].UsedIdx;                     //  ����� ��������� ���� �� ���������
        TmpSet:= SetList[R][Idx];
      end
      else
      begin
        Flg2:= False;                                     // ������ ���� - ��� TmpSet ����� ���������
        TmpSet:= TMySet.Create;
        Idx:= MaxInt;                                     //  ����� ��������� ���� �� ���������
        FPoints2[R, I].UsedIdx:= Idx;
      end;
      TmpSet.AddVal(I);

//      Log.Add(Format(' ------ ������: %d  ------ ', [I]));

      for J:= I + 1 to High(FPoints2[R]) do             // ������������� �������� - ������ � ������ I x J
      begin

        Squar:= IntersecSquare(FPoints2[R, I].Crd, FPoints2[R, I].Depth, FPoints2[R, J].Crd, FPoints2[R, J].Depth);

        if (FPoints2[R, J].Ch = 0) or (FPoints2[R, I].Ch = 0)
          then Flg1:= Squar > 2  // ���� ��� ������������ ����� ��� �� 30%  (��� 0-�� ������) �� ...
          else Flg1:= Squar > 2; // ���� ��� � ������� ������������ ����� ��� �� 50% (��� ���� ������� ����� 0-��) �� ...

//        if Squar <> 0 then Log.Add(Format(' � ��������: %d �������: %d', [J, Squar]));

        if Flg1 then                                     // ��������� �������
        begin
          if FPoints2[R, I].UsedIdx <> FPoints2[R, J].UsedIdx then // ���� �� ��� ������ � ���� ��������� � ������� ��������� ������� I �� ������ ������ �� ����
          begin
            if FPoints2[R, J].UsedIdx <> - 1 then          // ���� �� ��� ������ � ����� �� ���������
            begin
              SetList[R][FPoints2[R, J].UsedIdx].AddSet(TmpSet);   // ��������� ���������
              if Flg2 then
              begin
                SetLength(DelSet, Length(DelSet) + 1);
                DelSet[High(DelSet)]:= Idx;
              end else TmpSet.Free;                        // ��������� ��������� ���������

              Idx:= FPoints2[R, J].UsedIdx;
              TmpSet:= SetList[R][Idx];                          // ������ ��������� �� ��������� ����� �������� � ���
              Flg2:= True;                                       // ������ ���� - ��� TmpSet ������ �� ��� ��������� ���������
              for K:= 0 to TmpSet.Count - 1 do FPoints2[R, TmpSet[K]].UsedIdx:= Idx; // ��������� �������
//              Log.Add(Format(' ������� � �������� : %d �� �� � ��������� %d �������� � ���', [J, Idx]));
            end;
//            else Log.Add(Format(' ������� � �������� : %d', [J]));
          end; // else Log.Add(Format(' ������� � �������� : %d �� �� ��� ������ � ��������� %d', [J, FPoints2[R, I].UsedIdx]));
//          Log.SaveToFile('X:\Log.txt');
          TmpSet.AddVal(J);
          FPoints2[R, J].UsedIdx:= Idx;
        end;
      end;

      if not Flg2 then
      begin
        L:= SetList[R].AddSet(TmpSet);
        for K:= 0 to SetList[R][L].Count - 1 do FPoints2[R, SetList[R][L][K]].UsedIdx:= L; // ��������� �������
//        Log.Add(Format(' ������� ��������� : %d', [L]));
//        Log.SaveToFile('X:\Log.txt');
      end;
    end;

    for I:= High(DelSet) downto 0 do SetList[R].Delete(DelSet[I]);
    SetLength(DelSet, 0);

//    Log.SaveToFile('X:\Log.txt');

// -------------------------- Log - ������ -------------------------------------------------
   {
    RecDataForm.RichEdit1.Lines.Add('Rail ' + IntToStr(Integer(R)));
    for I:= 0 to SetList[R].Count - 1 do
    begin
      RecDataForm.RichEdit1.Lines.Add('');
      RecDataForm.RichEdit1.Lines.Add(Format('Set �%d', [I]));
      S:= '';
      for J:= 0 to SetList[R][I].Count - 1 do
        S:= S + Format(' %d ', [SetList[R][I][J]]);
      RecDataForm.RichEdit1.Lines.Add(S);
    end;
    }
// -------------------------- ������� ��������� -------------------------------------------------


    for I:= 0 to SetList[R].Count - 1 do
      if SetList[R][I].Count > 1 then
      begin
        TmpR.StCrd:= MaxInt;
        TmpR.EdCrd:= - MaxInt;
        Def:= False;
        for K:= 0 to 7 do ChCount[K]:= 0;
        for J:= 0 to SetList[R][I].Count - 1 do
        begin
          PBItem:= FPBList[R, FPoints2[R, SetList[R][I][J]].Ch, FPoints2[R, SetList[R][I][J]].PBIdx];
//          PBItem.Typ
          FPoints2[R, SetList[R][I][J]].HoleIdx:= Length(FSingleObj[R]);
          if (FPoints2[R, SetList[R][I][J]].Ch <> 0) or
             ((FPoints2[R, SetList[R][I][J]].Ch = 0) and (GetLen(PBItem.Crd) < 65 / FScanStep)) then
          begin
            TmpR.StCrd:= Min(TmpR.StCrd, GetRed(PBItem.Crd, PBItem.Del, FPoints2[R, SetList[R][I][J]].Ch).StCrd);
            TmpR.EdCrd:= Max(TmpR.EdCrd, GetRed(PBItem.Crd, PBItem.Del, FPoints2[R, SetList[R][I][J]].Ch).EdCrd);
          end;
          if PBItem.Typ <> pbtForHoleBolt then Inc(ChCount[FPoints2[R, SetList[R][I][J]].Ch]);
          if PBItem.Typ = pbtForHoleDef then Def:= True;

          case PBItem.Typ of
               pbtForHole: SetPBTyp(PBItem, AlgNo_BHSearch, pbtHoleMain);
           pbtForHoleBolt: SetPBTyp(PBItem, AlgNo_BHSearch, pbtHoleBolt);
            pbtForHoleDef: SetPBTyp(PBItem, AlgNo_BHSearch, pbtHoleDef);
          end;

          FPBList[R, FPoints2[R, SetList[R][I][J]].Ch, FPoints2[R, SetList[R][I][J]].PBIdx]:= PBItem;
        end;

    { // ------------------------------------------ // ����� 1, ����� 6, ����� 7 - 2 �� 3 (����������)
        if (FShSt[ptCh1].Ok and FShSt[ptCh6].Ok) or
           (FShSt[ptCh1].Ok and FShSt[ptCh7].Ok) or
           (FShSt[ptCh6].Ok and FShSt[ptCh7].Ok) then Flg:= True else
      // ------------------------------------------ // ����� 6 ��� ����� 7 � ������� 0
        if (FShSt[ptCh0].Ok and FShSt[ptCh6].Ok) or
           (FShSt[ptCh0].Ok and FShSt[ptCh7].Ok) then Flg:= True else
      // ------------------------------------------ // ����� 0 � ����� 1
        if FShSt[ptCh1].Ok and FShSt[ptCh0].Ok then Flg:= True; }

        if ((ChCount[1] <> 0) and (ChCount[6] <> 0)) or    // ���������� ��������� �� ���
           ((ChCount[1] <> 0) and (ChCount[7] <> 0)) or
           ((ChCount[6] <> 0) and (ChCount[7] <> 0)) or
           ((ChCount[0] <> 0) and (ChCount[6] <> 0)) or
           ((ChCount[0] <> 0) and (ChCount[7] <> 0)) then
        begin
          SetLength(FSingleObj[R], Length(FSingleObj[R]) + 1); // ��������� ���������

//          FSingleObj[R][High(FSingleObj[R])].Name:= '';
          if (ChCount[0] in [1..3]) and
             (ChCount[1] = 1) and
             (ChCount[6] = 1) and
             (ChCount[7] = 1) then FSingleObj[R][High(FSingleObj[R])].Typ:= so_BHOk;

          if (not (ChCount[0] in [1..3])) or
             (not (ChCount[1] in [1..3])) or
             (not (ChCount[6] in [1..3])) or
             (not (ChCount[7] in [1..3])) then FSingleObj[R][High(FSingleObj[R])].Typ:= so_BHUnCtrl;

          if Def then FSingleObj[R][High(FSingleObj[R])].Typ:= so_BHDef;

//          FSingleObj[R][High(FSingleObj[R])].Typ:= so_BoltHole;

          FSingleObj[R][High(FSingleObj[R])].DisRange:= TmpR;
          FSingleObj[R, High(FSingleObj[R])].SysRange.StCrd:= FDatSrc.DisToSysCoord(TmpR.StCrd);
          FSingleObj[R, High(FSingleObj[R])].SysRange.EdCrd:= FDatSrc.DisToSysCoord(TmpR.EdCrd);
        end;
      end;
  end;

  for R:= rLeft to rRight do SetList[R].Free; * )
end;

// -----------------------------------------------------------------------------
// --- �������� ������ ��� ����� ----------------------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.PBDeadZone;
var
  R: RRail;
  Ch: Integer;
  F1, F2: Boolean;
  I, J, L1, L2: Integer;
  BSIdx: Integer;
  CompIdx: Integer;

begin
  exit;

  for R:= rLeft to rRight do                       // �������� ������� ��� �����
    for Ch:= 1 to 7 do
    begin
      BSIdx:= 0;
      if Ch in [1, 6, 7] then                        // ������� ����� 1 6 7 �������
        for I:= 0 to High(FPBList[R, Ch]) do
     //     if FPBList[R, Ch, I].Typ = pbtForHole then  // �� ��� ��� ���������
          begin
            if GetLen(FPBList[R, Ch, I].Crd) < 3 then Continue;
        {    if Ch <> 1 then
            begin
              for J:= BSIdx to High(FBSignal.FBSPos[R]) - 1 do // ���������� ������� ������ ���� ������� �������
                if (FPBList[R, Ch, I].Crd.StCrd >= FBSignal.FBSPos[R, J].Crd) and
                   (FPBList[R, Ch, I].Crd.StCrd <= FBSignal.FBSPos[R, J + 1].Crd) then
                begin
                  BSIdx:= J;
                  Break;
                end;
//              FBSignal.FBSPos[R, BSIdx].MaxDelay
//              FBSignal.FBSPos[R, BSIdx].MinDelay
            end;  }

//            RecDataForm.ListBox5.Items.AddObject(Format('�������� � R: %d; Ch: %d; %d', [Ord(R), Ch, I]), Pointer(0));

            for J:= I - 1 downto 0 do          // �� ������� ����� �����
              begin
                if FPBList[R, Ch, J].Crd.EdCrd < FPBList[R, Ch, I].Crd.StCrd then Break;   // ����� ?
                if GetLen(FPBList[R, Ch, J].Crd) < 3 then Continue;
//                if (Ch in [6, 7]) and (Max(FPBList[R, Ch, J].Del.StCrd, FPBList[R, Ch, J].Del.EdCrd) > 128) then Continue; // 6 7 ������ ����������� �� �������

                L1:= PointToLine(Point(FPBList[R, Ch, J].Crd.StCrd, FPBList[R, Ch, J].Del.StCrd),  // ������ �� ������ ����� J �� ����� I
                                 Point(FPBList[R, Ch, I].Crd.StCrd, FPBList[R, Ch, I].Del.StCrd),
                                 Point(FPBList[R, Ch, I].Crd.EdCrd, FPBList[R, Ch, I].Del.EdCrd));

                L2:= PointToLine(Point(FPBList[R, Ch, J].Crd.EdCrd, FPBList[R, Ch, J].Del.EdCrd),  // ������ �� ����� ����� J �� ����� I
                                 Point(FPBList[R, Ch, I].Crd.StCrd, FPBList[R, Ch, I].Del.StCrd),
                                 Point(FPBList[R, Ch, I].Crd.EdCrd, FPBList[R, Ch, I].Del.EdCrd));

//                F1:= (L1 > 0) and (L2 > 0);           // ����� J ��� ������ I
//                Flg:= Flg and (GetLen(FPBList[R, Ch, J].Crd) < GetLen(FPBList[R, Ch, I].Crd));     // ����� J �� ������ ������ ����� I
(*                                                                                            // ��������������� �������
                if {(L1 > 0) and (L2 > 0) and                                                // ����� J ��� ������ I
                   }(GetLen(FPBList[R, Ch, J].Crd) < GetLen(FPBList[R, Ch, I].Crd)) then     // ����� J �� ������ ������ ����� I
                begin
                  if MyTypes.InRange(FPBList[R, Ch, J].Crd, FPBList[R, Ch, I].Crd) then     // J ��������� ������ � I - ��������� ����
                  begin
                    SetPBTyp(FPBList[R, Ch, J], AlgNo_PBDeadZone, pbtForHoleBolt);
                  end
                  else
                  if TestRangeIntersec(FPBList[R, Ch, J].Crd, FPBList[R, Ch, I].Crd) and
                     (L1 > 2) and (L2 > 2) and (L1 < 14) and (L2 < 14) then
                  begin
                    SetPBTyp(FPBList[R, Ch, J], AlgNo_PBDeadZone, pbtForHoleDef);
                  end;
                end;   * )

                F1:= MyTypes.InRange(FPBList[R, Ch, J].Crd, FPBList[R, Ch, I].Crd) and
                     (Max(FPBList[R, Ch, J].Del.StCrd, FPBList[R, Ch, J].Del.EdCrd) < 130) and
                     (Max(FPBList[R, Ch, I].Del.StCrd, FPBList[R, Ch, I].Del.EdCrd) < 130);
                F2:= StickOut(FPBList[R, Ch, J].Crd, FPBList[R, Ch, I].Crd) and (Abs(L1) < 14) and (Abs(L2) < 14);
                CompIdx:= (Ch * 10000 + (I + 1)) * (1 - 2 * Ord(R = rLeft));
//                if F1 or F2 then RecDataForm.ListBox5.Items.AddObject(Format('| R: %d; Ch: %d | %d - %d | H1: %d; H2: %d | ������: %d; �������: %d', [Ord(R), Ch, I, J, L1, L2, Ord(F1), Ord(F2)]), Pointer(CompIdx));
              end;

            for J:= I + 1 to High(FPBList[R, Ch]) do  // �� ������� ����� ������
              begin
                if FPBList[R, Ch, J].Crd.EdCrd - FPBMaxLen[R, Ch] > FPBList[R, Ch, I].Crd.EdCrd then Break;  // ����� ?
                if GetLen(FPBList[R, Ch, J].Crd) < 3 then Continue;
//                if (Ch in [6, 7]) and (Max(FPBList[R, Ch, J].Del.StCrd, FPBList[R, Ch, J].Del.EdCrd) > 128) then Continue; // 6 7 ������ ����������� �� �������

                L1:= PointToLine(Point(FPBList[R, Ch, J].Crd.StCrd, FPBList[R, Ch, J].Del.StCrd),   // ������ �� ������ ����� J �� ����� I
                                 Point(FPBList[R, Ch, I].Crd.StCrd, FPBList[R, Ch, I].Del.StCrd),
                                 Point(FPBList[R, Ch, I].Crd.EdCrd, FPBList[R, Ch, I].Del.EdCrd));

                L2:= PointToLine(Point(FPBList[R, Ch, J].Crd.EdCrd, FPBList[R, Ch, J].Del.EdCrd),    // ������ �� ����� ����� J �� ����� I
                                 Point(FPBList[R, Ch, I].Crd.StCrd, FPBList[R, Ch, I].Del.StCrd),
                                 Point(FPBList[R, Ch, I].Crd.EdCrd, FPBList[R, Ch, I].Del.EdCrd));

(*                                                                                            // ��������������� �������
                if {(L1 > 0) and (L2 > 0) and                                                // ����� J ��� ������ I
                   }(GetLen(FPBList[R, Ch, J].Crd) < GetLen(FPBList[R, Ch, I].Crd)) then     // ����� J �� ������ ������ ����� I
                begin
                  if MyTypes.InRange(FPBList[R, Ch, J].Crd, FPBList[R, Ch, I].Crd) then     // J ��������� ������ � I - ��������� ����
                  begin
                    SetPBTyp(FPBList[R, Ch, J], 8001, pbtForHoleBolt);
                  end
                  else
                  if TestRangeIntersec(FPBList[R, Ch, J].Crd, FPBList[R, Ch, I].Crd) and
                     (L1 > 2) and (L2 > 2) and (L1 < 14) and (L2 < 14) then
                  begin
                    SetPBTyp(FPBList[R, Ch, J], 8002, pbtForHoleDef);
                  end;
                end; * )

                                 // ����� J ��� ������ I
                F1:= MyTypes.InRange(FPBList[R, Ch, J].Crd, FPBList[R, Ch, I].Crd) and
                     (Max(FPBList[R, Ch, J].Del.StCrd, FPBList[R, Ch, J].Del.EdCrd) < 130) and
                     (Max(FPBList[R, Ch, I].Del.StCrd, FPBList[R, Ch, I].Del.EdCrd) < 130);
                F2:= StickOut(FPBList[R, Ch, J].Crd, FPBList[R, Ch, I].Crd) and (Abs(L1) < 14) and (Abs(L2) < 14);
                CompIdx:= (Ch * 10000 + (I + 1)) * (1 - 2 * Ord(R = rLeft));
//                if F1 or F2 then RecDataForm.ListBox5.Items.AddObject(Format('| R: %d; Ch: %d | %d - %d | H1: %d; H2: %d | ������: %d; �������: %d', [Ord(R), Ch, I, J, L1, L2, Ord(F1), Ord(F2)]), Pointer(CompIdx));
//                if F1 then RecDataForm.ListBox5.Items.AddObject(Format('R: %d; Ch: %d; %d - %d; H1: %d; H2: %d; ������: %d', [Ord(R), Ch, I, J, L1, L2, Ord(F1)]), Pointer(CompIdx));
              end;
          end;
    end;
(*


        begin
          for J:= Max(0, I - 20) to Min(High(FPBList[R, Ch]), I + 20) do
            if (I <> J) and
               (FPBList[R, Ch, J].Typ <> pbtHole) and
               (FPBList[R, Ch, I].Crd.StCrd <= FPBList[R, Ch, J].Crd.StCrd + 2) and
               (FPBList[R, Ch, I].Crd.EdCrd >= FPBList[R, Ch, J].Crd.EdCrd - 1) and
               (110 > FPBList[R, Ch, J].Del.StCrd) { and
               (FPBList[R, Ch, I].Del.StCrd < FPBList[R, Ch, J].Del.StCrd) and
               (FPBList[R, Ch, I].Del.EdCrd < FPBList[R, Ch, J].Del.EdCrd) } then
            begin
           //   if (J < 0) or (J > High(FPBList[R, Ch])) then ShowMessage('Fuck!');
           //   FPBList[R, Ch, J].Typ:= pbtHole;
              SetPBTyp(FPBList[R, Ch, J], 99, pbtHole);
            end;
        end;

      end;
  *)
  (*
  for R:= rLeft to rRight do                       // ����������� ����� 1 � ���������
    for I:= 0 to High(FSingleObj[R]) do
      if FSingleObj[R, I].Typ = so_BoltHole then
        for J:= 0 to High(FPBList[R, 1]) do
          if MyTypes.InRange(FPBList[R, 1, J].Crd, FSingleObj[R, I].DisRange) and
            (FPBList[R, 1, J].Del.StCrd <= FindHoleParam[0].DZoneMax) and
            (FPBList[R, 1, J].Del.EdCrd >= FindHoleParam[0].DZoneMin) then SetPBTyp(FPBList[R, 1, J], 199, pbtHole);
* )
end;

// -----------------------------------------------------------------------------
// --- ������������ ����� � ��������� ------------------------------------------
// -----------------------------------------------------------------------------
(*
procedure TRecData.HolePBMark;
var
  R: RRail;
  II, JJ: Integer;
  I, J: Integer;
  Ch: Integer;
  Angle: Single;

begin
  for R:= rLeft to rRight do
    for II:= 0 to High(FSingleObj[R]) do
      with FSingleObj[R, II] do
        if Typ = so_BoltHole then
        begin
          for JJ:= 0 to High(PBList) do
          begin
            Ch:= PBList[JJ].Ch;
            I:= PBList[JJ].Idx;

            if FPBList[R, Ch, I].Typ = pbtHole then
            begin
              for J:= Max(0, I - 20) to Min(High(FPBList[R, Ch]), I + 20) do
              begin
                Angle:= (FPBList[R, Ch, J].Del.EdCrd - FPBList[R, Ch, J].Del.StCrd + 1) / (FPBList[R, Ch, J].Crd.EdCrd - FPBList[R, Ch, J].Crd.StCrd + 1);
                if (I <> J) and
                   (FPBList[R, Ch, J].Typ <> pbtHole) and
                   (((Ch = 6) and (Angle > - 1.3) and (Angle < - 0.3)) or         // ������
                    ((Ch = 7) and (Angle <   1.3) and (Angle >   0.3))) and
                   (FPBList[R, Ch, I].Crd.StCrd <= FPBList[R, Ch, J].Crd.StCrd + 2) and
                   (FPBList[R, Ch, I].Crd.EdCrd >= FPBList[R, Ch, J].Crd.EdCrd - 1) and
                   (130 > FPBList[R, Ch, J].Del.StCrd) { and
                   (FPBList[R, Ch, I].Del.StCrd < FPBList[R, Ch, J].Del.StCrd) and
                   (FPBList[R, Ch, I].Del.EdCrd < FPBList[R, Ch, J].Del.EdCrd) } then
                begin
                  SetPBTyp(FPBList[R, Ch, J], 99, pbtHole);
                  with FSingleObj[R, II] do
                  begin
                    SetLength(PBList, Length(PBList) + 1);
                    PBList[High(PBList)].Ch:= Ch;
                    PBList[High(PBList)].Idx:= J;
                    PBList[High(PBList)].Main:= False;
                  end;
                end;
              end;
            end;

            for J:= 0 to High(FPBList[R, 1]) do
              if (FPBList[R, 1, J].Typ <> pbtHole) and
                  MyTypes.InRange(FPBList[R, 1, J].Crd, FSingleObj[R, II].DisRange) and
                 (FPBList[R, 1, J].Del.StCrd >= FindHoleParam[0].DZoneMin) and
                 (FPBList[R, 1, J].Del.EdCrd >= FindHoleParam[0].DZoneMin) then

              begin
                SetPBTyp(FPBList[R, 1, J], 199, pbtHole);
                with FSingleObj[R, II] do
                begin
                  SetLength(PBList, Length(PBList) + 1);
                  PBList[High(PBList)].Ch:= 1;
                  PBList[High(PBList)].Idx:= J;
                  PBList[High(PBList)].Main:= False;
                end;
              end;
          end;
        end;

(*
  for R:= rLeft to rRight do                       // ����������� ����� 1 ������ ������� ���� ���������
    for Ch:= 1 to 7 do
      if Ch in [1, 6, 7] then
      for I:= 0 to High(FPBList[R, Ch]) do
        if FPBList[R, Ch, I].Typ = pbtHole then
        begin
          for J:= Max(0, I - 20) to Min(High(FPBList[R, Ch]), I + 20) do
            if (I <> J) and
               (FPBList[R, Ch, J].Typ <> pbtHole) and
               (FPBList[R, Ch, I].Crd.StCrd <= FPBList[R, Ch, J].Crd.StCrd + 2) and
               (FPBList[R, Ch, I].Crd.EdCrd >= FPBList[R, Ch, J].Crd.EdCrd - 1) and
               (110 > FPBList[R, Ch, J].Del.StCrd) { and
               (FPBList[R, Ch, I].Del.StCrd < FPBList[R, Ch, J].Del.StCrd) and
               (FPBList[R, Ch, I].Del.EdCrd < FPBList[R, Ch, J].Del.EdCrd) } then
            begin
           //   if (J < 0) or (J > High(FPBList[R, Ch])) then ShowMessage('Fuck!');
           //   FPBList[R, Ch, J].Typ:= pbtHole;
              SetPBTyp(FPBList[R, Ch, J], 99, pbtHole);
            end;
        end;

  for R:= rLeft to rRight do                       // ����������� ����� 1 � ���������
    for I:= 0 to High(FSingleObj[R]) do
      if FSingleObj[R, I].Typ = so_BoltHole then
        for J:= 0 to High(FPBList[R, 1]) do
          if MyTypes.InRange(FPBList[R, 1, J].Crd, FSingleObj[R, I].DisRange) and
            (FPBList[R, 1, J].Del.StCrd <= FindHoleParam[0].DZoneMax) and
            (FPBList[R, 1, J].Del.EdCrd >= FindHoleParam[0].DZoneMin) then SetPBTyp(FPBList[R, 1, J], 199, pbtHole);
* )
end;
  *)
// -----------------------------------------------------------------------------
// --- ����� ������ ------------------------------------------------------------
// -----------------------------------------------------------------------------
(*
function TRecData.EndSearchBlockOK(StartDisCoord: Integer): Boolean;
var
  R: RRail;
  I, Delta: Integer;
  Ch, Crd: Integer;
  Flg: Boolean;

begin
  for R:= rLeft to rRight do
    for Ch:= 2 to 7 do
      for I:= 1 to FDatSrc.CurEcho[R, Ch].Count do
      begin
        Crd:= Round((FDatSrc.CurDisCoord - FStCrd + (1 - 2 * Ord(FDatSrc.BackMotion)) * (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * FDatSrc.CurEcho[R, Ch].Delay[I]) / FScanStep) / FBJHeadBlock );
        if Crd < 0 then Crd:= 0;
        if Crd > FHeadLen - 1 then Crd:= FHeadLen - 1;

        if Odd(Ch) then Delta:= - 1 else Delta:= 1;
        Flg:= FDatSrc.CurDisCoord - FFilter1[R, Ch, FDatSrc.CurEcho[R, Ch].Delay[I] div 2 + Delta] < 4;
        FFilter1[R, Ch, FDatSrc.CurEcho[R, Ch].Delay[I] div 2]:= FDatSrc.CurDisCoord;
        if not Flg then Continue;

        Flg:= Abs(FDatSrc.CurDisCoord - FFilter2[R, Ch, FDatSrc.CurEcho[R, Ch].Delay[I]]) < 600;
        FFilter2[R, Ch, FDatSrc.CurEcho[R, Ch].Delay[I]]:= FDatSrc.CurDisCoord;
        if Flg then Continue;

        if (Ch in [2..5]) or ((Ch in [6, 7]) and (FDatSrc.CurEcho[R, Ch].Delay[I] in [120..165])) then
          Inc(FTHead[R, Ch, Crd], Sqr(FDatSrc.CurEcho[R, Ch].Ampl[I]));
      end;
  Result:= True;
end;

procedure TRecData.EndSearchStep1;
const
  Th2 = 0.25;

var
  R: RRail;
  I, J, K1, K, C, L, MaxVal, MaxIdx: Integer;
  Summ1: Integer;
  Summ2: Integer;
  ColCount: Integer;
  Flg: Boolean;
  Flg2: Boolean;
  TmpR: TRange;

begin
                                                        // ������
  FillChar(FFilter1[rLeft, 0, 0], 2 * 8 * 256 * 4, 0);
  FillChar(FFilter2[rLeft, 0, 0], 2 * 8 * 256 * 4, 0);
                                                        // ���������� �������� (����������)
  FBJHeadBlock:= Round(40 / FScanStep);
  FHeadLen:= 1 + (FEdCrd - FStCrd) div FBJHeadBlock;
  for R:= rLeft to rRight do
    for I:= 1 to 7 do SetLength(FTHead[R, I], FHeadLen);

  FDatSrc.LoadData(FStCrd, FEdCrd, 0, EndSearchBlockOK);

  for R:= rLeft to rRight do                          // ������������ �������
    for I:= 2 to 7 do
    begin
      K:= 0;
      for J:= 0 to FHeadLen - 2 do
      begin
        L:= FTHead[R, I, J];
        FTHead[R, I, J]:= K + FTHead[R, I, J] + FTHead[R, I, J + 1];
        K:= L;
      end;
    end;
                                                        // ����������� ����������� �������� � ����
  for R:= rLeft to rRight do
    for I:= 0 to FHeadLen - 1 do
    begin
      ColCount:= 0;
      MaxVal:= 0;
      for C:= 2 to 7 do
      begin
        if FTHead[R, C, I] > 60 then Inc(ColCount);              // ������������ ���������� ������� ��� ���� "������"
        MaxVal:= MaxVal + FTHead[R, C, I];
      end;
      FTHead[R, 1, I]:= MaxVal * ColCount * ColCount;
    end;
                                                                 // ��� ����� + ����������

  for R:= rLeft to rRight do
  begin
    K1:= 2900;                                                   // �����
    Flg:= False;
    MaxVal:= 0;
    MaxIdx:= 0;
    for I:= 0 to High(FTHead[R, 1]) do
    begin
      if (not Flg) and (FTHead[R, 1, I] > K1) then               // ����� ������
      begin
        J:= I;
        Flg:= True;
        MaxIdx:= I;
        MaxVal:= FTHead[R, 1, I];
      end;
      if Flg and (FTHead[R, 1, I] < K1) then                     // ����� �����
      begin
        for K:= MaxIdx downto 1 do                               // ����������� ����� ������ ���������� ����������� ������� � ������ ������� �������� �� ��������
          if (MaxVal * Th2 <= Max(FTHead[R, 1, K], FTHead[R, 1, K - 1])) and
             (MaxVal * Th2 >= Min(FTHead[R, 1, K], FTHead[R, 1, K - 1])) then
          begin
            if Abs(FTHead[R, 1, K] - FTHead[R, 1, K - 1]) = 0
              then TmpR.StCrd:= Round((K - 0.5) * FBJHeadBlock)
              else TmpR.StCrd:= FStCrd + Round(K * FBJHeadBlock - (FTHead[R, 1, K] - MaxVal * Th2) / Abs(FTHead[R, 1, K] - FTHead[R, 1, K - 1]) * FBJHeadBlock);
            Break;
          end;
                                                                 // ����������� ������ ������ ���������� ����������� ������� � ������ ������� �������� �� ��������
        for K:= MaxIdx to High(FTHead[R, 1]) - 1 do
          if (MaxVal * Th2 <= Max(FTHead[R, 1, K], FTHead[R, 1, K + 1])) and
             (MaxVal * Th2 >= Min(FTHead[R, 1, K], FTHead[R, 1, K + 1])) then
          begin
            if Abs(FTHead[R, 1, K] - FTHead[R, 1, K + 1]) = 0
              then TmpR.StCrd:= Round((K + 0.5) * FBJHeadBlock)
              else TmpR.EdCrd:= FStCrd + Round(K * FBJHeadBlock + (FTHead[R, 1, K] - MaxVal * Th2) / Abs(FTHead[R, 1, K] - FTHead[R, 1, K + 1]) * FBJHeadBlock);
            Break;
          end;

        if TestRange(TmpR) then
        begin
          Flg2:= True;                                           // �������� �� �������� �� ����� �� ���� �������
          for L:= 0 to High(FBlades[R]) do
            if (TmpR.StCrd >= FBlades[R, L].StCrd) and
               (TmpR.EdCrd <= FBlades[R, L].EdCrd) then
            begin
              Flg2:= False;
              Break;
            end;

          if Flg2 then        // ���������
          begin
            if (Length(FEnd2[R]) <> 0) and (TestRangeIntersec(FEnd2[R, High(FEnd2[R])].Range, TmpR)) then
            begin
              FEnd2[R, High(FEnd2[R])].Range:= OutRange(FEnd2[R, High(FEnd2[R])].Range, TmpR);
            end
            else
            begin
              SetLength(FEnd2[R], Length(FEnd2[R]) + 1);
              FEnd2[R, High(FEnd2[R])].Crd:= GetCentr(TmpR);
              FEnd2[R, High(FEnd2[R])].Range:= TmpR;
              FEnd2[R, High(FEnd2[R])].Typ:= 2;
//              FEnd2[R, High(FEnd2[R])].MaxVal:= MaxVal;
            end;
          end;
        end;
        Flg:= False;
        MaxVal:= 0;
        MaxIdx:= 0;
      end
      else
      if MaxVal < FTHead[R, 1, I] then
      begin
        MaxVal:= FTHead[R, 1, I];
        MaxIdx:= I;
      end;
    end;
  end;
end;

procedure TRecData.EndSearchStep2;
type
  TMask = record
    Crd: TRange;
    Typ: Integer;
  end;

  TRes = record
    Idx: Integer;
    Ok: Boolean
  end;

var
  R: RRail;
  I, J, K, L, M, N, Crd, StIdx: Integer;
  Tmp: TEnd2Item;
  M1: array of TMask;
  M2: array of TMask;
  Res: array of TRes;
  StEnd: Integer;
  MaxVol: Integer;
  Flg: Boolean;
  FFF: Boolean;
  Width: TRange;

begin
(* ASD
  for R:= rLeft to rRight do                    // ���������� ������� ��������� 1 - ��  2 - ���������� ��
  begin
    for I:= 0 to High(FSingleObj[R]) do                     // ���������� ������� ��������� - ��
      if FSingleObj[R, I].Typ in [so_BHOk, so_BHUnCtrl, so_BHDef] then
      begin
        SetLength(FEnd2[R], Length(FEnd2[R]) + 1);
        FEnd2[R, High(FEnd2[R])].Crd:= GetCentr(FSingleObj[R, I].DisRange);
        FEnd2[R, High(FEnd2[R])].Range:= FSingleObj[R, I].DisRange;
        FEnd2[R, High(FEnd2[R])].Typ:= 0;
      end;

    for I:= 0 to High(FPBList[R, 0]) do                     // ���������� ������� ��������� - ���������� ��
//      if (FPBList[R, 0, I].Typ in [pbtNotSet, pbtForHole, pbtUCS, pbtUnknown, pbtForDef]) and
      if not (FPBList[R, 0, I].Typ in [pbtHoleMain, pbtHoleBolt, pbtHoleDef]) and
             (FPBList[R, 0, I].Dat[3] <> 1) then
      begin
        SetLength(FEnd2[R], Length(FEnd2[R]) + 1);
        FEnd2[R, High(FEnd2[R])].Crd:= GetCentr(FPBList[R, 0, I].Crd);
        FEnd2[R, High(FEnd2[R])].Range:= FPBList[R, 0, I].Crd;
        FEnd2[R, High(FEnd2[R])].Typ:= 1;
      end;
  end;

  for R:= rLeft to rRight do                    // ����������
  begin
    for I:= 0 to High(FEnd2[R]) - 1 do
    begin
      K:= I;
      for J:= I + 1 to High(FEnd2[R]) do
        if FEnd2[R, K].Crd > FEnd2[R, J].Crd then K:= J;
      if K <> I then
      begin
        Tmp:= FEnd2[R, I];
        FEnd2[R, I]:= FEnd2[R, K];
        FEnd2[R, K]:= Tmp;
      end;
    end;
  end;

  SetLength(M1, 8);                               // ����� ������
  M1[0].Crd:= Range( - 480, - 440 );
  M1[0].Typ:= 0;                                  // ���������
  M1[1].Crd:= Range( - 340, - 300);               // ����
  M1[1].Typ:= 0;                                  // ��������� (��� ���� - ��������� ��� ����� ��)
  M1[2].Crd:= Range( - 110,  - 90 );
  M1[2].Typ:= 0;                                  // ���������
  M1[3].Crd:= Range(  - 20,    20 );
  M1[3].Typ:= 1;                                  // ����� � ��
  M1[4].Crd:= Range(  - 20,    20 );
  M1[4].Typ:= 2;                                  // ����� �� �������� �������
  M1[5].Crd:= Range(    90,   110 );
  M1[5].Typ:= 0;                                  // ���������
  M1[6].Crd:= Range(   300,   340 );
  M1[6].Typ:= 0;                                  // ���������
  M1[7].Crd:= Range(   440,   480 );
  M1[7].Typ:= 0;                                  // ���������

  for I:= 0 to 7 do                               // ��������� � DisCoord
  begin
    M1[I].Crd.StCrd:= Round(M1[I].Crd.StCrd / FScanStep);
    M1[I].Crd.EdCrd:= Round(M1[I].Crd.EdCrd / FScanStep);
  end;

  SetLength(M2, 8);
  SetLength(Res, 8);

  for R:= rLeft to rRight do
  begin
    StIdx:= 0;
    Flg:= False;

    for Crd:= FStCrd to FEdCrd do                // ������� ����� �� ���� ����������� � ����� 1
    begin

      for J:= StIdx to High(FEnd2[R]) do         // ����������� �������� - � �������� ������� �����������
        if Crd - Round(480 / FScanStep) > FEnd2[R, J].Crd then Inc(StIdx) else Break;

      for I:= 0 to 7 do
      begin
        M2[I].Crd.StCrd:= Crd + M1[I].Crd.StCrd;  // ����������� ����� ��� ������� ���������� - M2[]
        M2[I].Crd.EdCrd:= Crd + M1[I].Crd.EdCrd;
        M2[I].Typ:= M1[I].Typ;
        Res[I].Ok:= False;                        // ����� �����������
        Res[I].Idx:= - 1;
      end;

      for J:= StIdx to High(FEnd2[R]) do         // ��������� �������� �� ��������� � ����� - Res[]
      begin
        if Crd + Round(480 / FScanStep) < FEnd2[R, J].Crd then Break; // ����� ������
        for I:= 0 to 7 do
        begin
          FFF:= MyTypes.PtInRange(FEnd2[R, J].Crd, M2[I].Crd) and (FEnd2[R, J].Typ = M2[I].Typ);
          if FFF then
          begin
            Res[I].Idx:= J;
            Res[I].Ok:= True;
          end;
        end;
      end;

      K:= 0;                                    // ������������ ���������� ��������� ����� ����� - �� ������ ���� �����������
      for I:= 0 to 7 do
        if (I <> 4) and Res[I].Ok then Inc(K);

      if (not Flg) and (K >= 3) and Res[4].Ok then             // ��������� ����� - ������ �����
      begin
        MaxVol:= K + 1;
        StEnd:= Crd;
        Flg:= True;
        Width:= FEnd2[R, Res[4].Idx].Range;  // � ������
      end else
      if Flg and (not ((K >= 3) and Res[4].Ok)) then             // ����� �����
      begin
        Flg:= False;
      ( *
        SetLength(FEnd3[R], Length(FEnd3[R]) + 1);
        FEnd3[R, High(FEnd3[R])].Crd:= GetCentr(Width);
        FEnd3[R, High(FEnd3[R])].Value:= Max(MaxVol, K);
        FEnd3[R, High(FEnd3[R])].Ok:= True;
      * )
        SetLength(FSingleObj[R], Length(FSingleObj[R]) + 1);
        FSingleObj[R, High(FSingleObj[R])].Typ:= so_End;
        FSingleObj[R, High(FSingleObj[R])].DisRange:= Width;
        FSingleObj[R, High(FSingleObj[R])].SysRange.StCrd:= FDatSrc.DisToSysCoord(FSingleObj[R, High(FSingleObj[R])].DisRange.StCrd);
        FSingleObj[R, High(FSingleObj[R])].SysRange.EdCrd:= FDatSrc.DisToSysCoord(FSingleObj[R, High(FSingleObj[R])].DisRange.EdCrd);
        FSingleObj[R, High(FSingleObj[R])].Use:= False;
      end;
    end;
  end;

  SetLength(M1, 0);
  SetLength(M2, 0);
  SetLength(Res, 0); * )
end;

// -----------------------------------------------------------------------------
// --- �������� ����� �� ��������� � ������� ������� ---------------------------
// -----------------------------------------------------------------------------

procedure TRecData.TestBPtoNoise;
var
  R: RRail;
  Ch: Integer;
  I, J, K, L: Integer;
  Flg: Boolean;
  List: array of array of Integer;
  SummE: Single;

begin
(*
  for R:= rLeft to rRight do
    for Ch:= 1 to 7 do
    begin
      if Ch in [2, 3, 4, 5] then                           // �� ������� �������
        for I:= 0 to High(FHZone[R]) do                    // ����� ����
        begin

    //     if (R = rLeft) and (I = 1) and (Ch = 2) then ShowMessage('!');

          SetLength(List, Length(FHZone[R, I].Gis[Ch].DelayReg));  // ������ ����� ���������� � ������� ����
          for K:= 0 to High(FPBList[R, Ch]) do             // ������������� ��� �����
            if FPBList[R, Ch, K].Typ in [pbtNotSet, pbtForHole, pbtUnknown] then // ����� ����� ���� ������ ��� �����
            begin
              Flg:= False; // �������� �� ����� � ���� �� ���������� I
              for J:= 0 to High(FHZone[R, I].Items) do

                if PtInRange(GetRedCrd(Ch, FPBList[R, Ch, K].CCrd, FPBList[R, Ch, K].CDel), FHZone[R, I].Items[J]) then
                begin
                  Flg:= True; // ��������
                  Break;
                end;

              if Flg then
                for L:= 0 to High(FHZone[R, I].Gis[Ch].DelayReg) do // �������� �� ����� � ���� �� �������� L
                  if (Min(FPBList[R, Ch, K].Del.StCrd, FPBList[R, Ch, K].Del.EdCrd) >= FHZone[R, I].Gis[Ch].DelayReg[L].StDelay - 2) and // ����� ������ � ����
                     (Max(FPBList[R, Ch, K].Del.StCrd, FPBList[R, Ch, K].Del.EdCrd) <= FHZone[R, I].Gis[Ch].DelayReg[L].EdDelay + 2) then
                  begin
                    SetLength(List[L], Length(List[L]) + 1);
                    List[L, High(List[L])]:= K;
                  end;
            end;

          with FHZone[R, I].Gis[Ch] do
            for J:= 0 to High(List) do // ������ ����� ������������ �� ���� DelayReg[J]
            begin
              DelayReg[J].EMedium:= 0;
              DelayReg[J].ETh:= 0;
              if Length(List[J]) <> 0 then
              begin
                SummE:= 0;
                for K:= 0 to High(List[J]) do // ������������� ��� ����� � ����������� ������� � �����
                  SummE:= SummE + FPBList[R, Ch, List[J, K]].E;
                DelayReg[J].EMedium:= SummE / Length(List[J]);
                DelayReg[J].ETh:= Max(DelayReg[J].EMedium * ((DelayReg[J].EdDelay - DelayReg[J].StDelay) / 4),
                                      DelayReg[J].EMedium * 4);


                for K:= 0 to High(List[J]) do // ������������� ��� ����� � ��������� �������
                begin
                  if (FPBList[R, Ch, List[J, K]].E >= DelayReg[J].ETh) then SetPBTyp(FPBList[R, Ch, List[J, K]], 6, pbtForDef)
                                                                       else SetPBTyp(FPBList[R, Ch, List[J, K]], 655, pbtNoise);
                end;
              end;
            end;

          for J:= 0 to High(List) do SetLength(List[J], 0);
          SetLength(List, 0);
        end;
    end;

  for R:= rLeft to rRight do
    for Ch:= 1 to 7 do
    begin
      if Ch in [1, 6, 7] then                              // �� ������� �����
        for I:= 0 to High(FBZone[R]) do                    // ����� ����
        begin
          SetLength(List, Length(FBZone[R, I].Gis[Ch].DelayReg));  // ������ ����� ���������� � ������� ����
          for K:= 0 to High(FPBList[R, Ch]) do             // ������������� ��� �����
            if FPBList[R, Ch, K].Typ in [pbtNotSet, pbtForHole, pbtUnknown] then // ����� ����� ���� ������ ��� �����
            begin
              Flg:= False; // �������� �� ����� � ���� �� ���������� I
              for J:= 0 to High(FBZone[R, I].Items) do
                if PtInRange(GetRedCrd(Ch, FPBList[R, Ch, K].CCrd, FPBList[R, Ch, K].CDel), FBZone[R, I].Items[J]) then
                begin
                  Flg:= True; // ��������
                  Break;
                end;

              if Flg then
                for L:= 0 to High(FBZone[R, I].Gis[Ch].DelayReg) do // �������� �� ����� � ���� �� �������� L
                  if (Min(FPBList[R, Ch, K].Del.StCrd, FPBList[R, Ch, K].Del.EdCrd) >= FBZone[R, I].Gis[Ch].DelayReg[L].StDelay - 2) and // ����� ������ � ����
                     (Max(FPBList[R, Ch, K].Del.StCrd, FPBList[R, Ch, K].Del.EdCrd) <= FBZone[R, I].Gis[Ch].DelayReg[L].EdDelay + 2) then
                  begin
                    SetLength(List[L], Length(List[L]) + 1);
                    List[L, High(List[L])]:= K;
                  end;
            end;

          with FBZone[R, I].Gis[Ch] do
            for J:= 0 to High(List) do // ������ ����� ������������ �� ���� DelayReg[J]
            begin
              DelayReg[J].EMedium:= 0;
              DelayReg[J].ETh:= 0;
              if Length(List[J]) <> 0 then
              begin
                SummE:= 0;
                for K:= 0 to High(List[J]) do // �����
                  SummE:= SummE + FPBList[R, Ch, List[J, K]].E;
                DelayReg[J].EMedium:= SummE / Length(List[J]);
                DelayReg[J].ETh:= Max(DelayReg[J].EMedium * ((DelayReg[J].EdDelay - DelayReg[J].StDelay) / 4),
                                      DelayReg[J].EMedium * 4);

                for K:= 0 to High(List[J]) do // �����
                  if (FPBList[R, Ch, List[J, K]].E >= DelayReg[J].ETh) and (Ch <> 1) then SetPBTyp(FPBList[R, Ch, List[J, K]], 6, pbtForDef)
                                                                                     else SetPBTyp(FPBList[R, Ch, List[J, K]], 655, pbtNoise);
              end;
            end;

          for J:= 0 to High(List) do SetLength(List[J], 0);
          SetLength(List, 0);
        end;
    end;
    * )
( *   not use
    MinVal_: Single;
    MaxVal_: Single;
    Th: Single;
      if (FPBList[R, Ch, K].Crd.StCrd >= FHZone[R, I].Items[J].StCrd {- 100} / FScanStep) and // �������� ?
         (FPBList[R, Ch, K].Crd.EdCrd <= FHZone[R, I].Items[J].EdCrd {+ 100} / FScanStep) then
        SetPBTyp(FPBList[R, Ch, K], 1234567, pbtNoise);

    for K:= 0 to High(List[J]) do // �����
    begin
      DelayReg[J].ESigma:= DelayReg[J].ESigma + Sqr(FPBList[R, Ch, List[J, K]].E - DelayReg[J].EMedium);
        DelayReg[J].ESigma:= DelayReg[J].ESigma + Sqr(- FPBList[R, Ch, List[J, K]].E - DelayReg[J].EMedium);
    end;
    DelayReg[J].ESigma:= Sqrt(DelayReg[J].ESigma / Length(List[J]) / 2);
    MinVal_:= DelayReg[J].EMedium - 2 * DelayReg[J].ESigma;
    MaxVal_:= DelayReg[J].EMedium + 2 * DelayReg[J].ESigma;

        FPBList[R, Ch, List[J, K]].Text:= Format('%3.1f/%3.1f', [MinVal_, MaxVal_]);
      if (FPBList[R, Ch, K].Crd.StCrd >= FBZone[R, I].Items[J].StCrd - 100 / FScanStep) and // �������� ?
         (FPBList[R, Ch, K].Crd.EdCrd <= FBZone[R, I].Items[J].EdCrd + 100 / FScanStep) then
    DelayReg[J].ESigma:= 0;
    for K:= 0 to High(List[J]) do // �����
      DelayReg[J].ESigma:= DelayReg[J].ESigma + Sqr(FPBList[R, Ch, List[J, K]].E - DelayReg[J].EMedium);
    DelayReg[J].ESigma:= Sqrt(DelayReg[J].ESigma / Length(List[J]));
    MinVal_:= DelayReg[J].EMedium - 3 * DelayReg[J].ESigma;
    MaxVal_:= DelayReg[J].EMedium + 3 * DelayReg[J].ESigma;
* )
end;
  *)
// -----------------------------------------------------------------------------
// --- ����� ��������������������� �������� ------------------------------------
// -----------------------------------------------------------------------------

                                                  //  IdxList - ����� ������ ������:
                                                  //  [0]     - ��������� ����� �����
                                                  //  [1]     - ��������� ����� �� ������� ����
                                                  //  [2...N] - ������� �����/�����

procedure TRecData.UCSSearch;
var
  R: RRail_;
  I, J, K, C1, C2: Integer;
  IdxList_: TIntegerDynArray;
  NPLen: Single;
  Flg: Boolean;

begin

  for R:= rLeft to rRight do
  begin
    SetLength(IdxList_, 0);
    I:= 0;
    while I <= High(FPBList[R, 0]) do
    begin
(*      Flg:= False;
      for J:= 0 to High(FConstruct[R]) do
        if (FConstruct[R, J].Typ in [{ct_Frog, } ct_BoltJoint {, ct_Blade}]) and
           (MyTypes.InRange(FPBList[R, 0, I].Crd, FConstruct[R, J].Body)) then
        begin
          Flg:= True;
          Break;
        end;
      if Flg then Continue;
  *)

      if {ASD not (FPBList[R, 0, I].Typ in [pbtHole, pbtEnd]) and}
         (pbtiClear in FPBList[R, 0, I].Typ) and
//         (FPBList[R, 0, I].Dat[4] <> 1) and (FPBList[R, 0, I].Dat[3] <> 1) then
         (not (pbtiReCheck in FPBList[R, 0, I].Typ)) and         // ���������� ��������������� ���������� ��
         (not (pbtiInBMZ in FPBList[R, 0, I].Typ)) and
         (not (pbtiDefect in FPBList[R, 0, I].Typ)) then            // ���������� ���������� �� ����������� � ���
      begin
        if Length(IdxList_) = 0 then                              // ������ ������ - ������ ����� ������ ��� ������ ����� ����� ������ �������������� �������
        begin
          SetLength(IdxList_, 3);
          IdxList_[0]:= GetLen(FPBList[R, 0, I].Crd);
          IdxList_[1]:= FPBList[R, 0, I].Crd.EdCrd;
          IdxList_[2]:= I;
          Inc(I);
        end
        else
        if FPBList[R, 0, I].Crd.StCrd - IdxList_[1] < 6500 / FScanStep then  // ��������� I - ��� �����/�����
        begin                                                                // ���� ��� ������� �� ��������� �� ������ ��� �� 5 � �� ��������� ��
          IdxList_[0]:= IdxList_[0] + GetLen(FPBList[R, 0, I].Crd);
          IdxList_[1]:= FPBList[R, 0, I].Crd.EdCrd;
          SetLength(IdxList_, Length(IdxList_) + 1);
          IdxList_[High(IdxList_)]:= I;
          Inc(I);
        end
        else
        begin                                                                         // ���� ������ �� ����� ������ ����
          NPLen:= FPBList[R, 0, IdxList_[High(IdxList_)]].Crd.EdCrd -
                  FPBList[R, 0, IdxList_[            2]].Crd.StCrd;
          if (NPLen > 1500 / FScanStep) and (100 * IdxList_[0] / NPLen > 3) then           // ��������� ������ � ��������� � %
          begin


            C1:= FPBList[R, 0, IdxList_[            2]].Crd.StCrd;
            C2:= FPBList[R, 0, IdxList_[High(IdxList_)]].Crd.EdCrd;
          (*  begin
              Flg:= False;
              J:= C1;
              repeat
                SetLength(FUnCtrlZ[R], Length(FUnCtrlZ[R]) + 1);

                FUnCtrlZ[R, High(FUnCtrlZ[R])].Rng.StCrd:= J;
                FUnCtrlZ[R, High(FUnCtrlZ[R])].Rng.EdCrd:= J;
                SetLength(FUnCtrlZ[R, High(FUnCtrlZ[R])].IdxList, Length(IdxList_));
                for K:= 0 to High(IdxList_) do FUnCtrlZ[R, High(FUnCtrlZ[R])].IdxList[K]:= IdxList_[K];

                if Flg then Break;
                Inc(J, Round(100 / FScanStep));
                if J > C2 then
                begin
                  J:= C2;
                  Flg:= True;
                end;
              until False;
            end; *)

       //     SetLength(FUnCtrlZ[R], Length(FUnCtrlZ[R]) + 1);
       //     FUnCtrlZ[R, High(FUnCtrlZ[R])].StCrd:= FPBList[R, 0, IdxList[            2]].Crd.StCrd;
       //     FUnCtrlZ[R, High(FUnCtrlZ[R])].EdCrd:= FPBList[R, 0, IdxList[High(IdxList)]].Crd.EdCrd;
            for J:= IdxList_[2] to IdxList_[High(IdxList_)] do
// ASD             if not (FPBList[R, 0, J].Typ in [pbtHole, pbtEnd]) then
              if (pbtiClear in FPBList[R, 0, J].Typ) then
                FPBList[R, 0, J].Typ:= FPBList[R, 0, J].Typ + [pbtiUCS];
//                SetPBTyp(FPBList[R, 0, J], 9, pbtUCS);  // �������� �����

            SetLength(FUnCtrlZ[R], Length(FUnCtrlZ[R]) + 1);
            FUnCtrlZ[R, High(FUnCtrlZ[R])].Rng.StCrd:= FPBList[R, 0, IdxList_[             2]].Crd.StCrd;
            FUnCtrlZ[R, High(FUnCtrlZ[R])].Rng.EdCrd:= FPBList[R, 0, IdxList_[High(IdxList_)]].Crd.EdCrd;
            SetLength(FUnCtrlZ[R, High(FUnCtrlZ[R])].IdxList, Length(IdxList_));
            for K:= 0 to High(IdxList_) do FUnCtrlZ[R, High(FUnCtrlZ[R])].IdxList[K]:= IdxList_[K];
{
            SetLength(FUnCtrlZ[R], Length(FUnCtrlZ[R]) + 1);
            FUnCtrlZ[R, High(FUnCtrlZ[R])].StCrd:= FPBList[R, 0, IdxList[            2]].Crd.StCrd;
            FUnCtrlZ[R, High(FUnCtrlZ[R])].EdCrd:= FPBList[R, 0, IdxList[High(IdxList)]].Crd.EdCrd;
            for J:= IdxList[2] to IdxList[High(IdxList)] do
              if not (FPBList[R, 0, J].Typ in [pbtHole, pbtEnd]) then
                SetPBTyp(FPBList[R, 0, J], 9, pbtUCS);  // �������� �����
}
          end
          else I:= IdxList_[2] + 1;                                                    // ���� ���� ����������� ������������ ����� � ���������� �����
          SetLength(IdxList_, 0);
        end;
      end else Inc(I);
    end;

    if Length(IdxList_) <> 0 then // ����������
    begin
      NPLen:= FPBList[R, 0, IdxList_[High(IdxList_)]].Crd.EdCrd -
              FPBList[R, 0, IdxList_[            2]].Crd.StCrd;

      if (NPLen > 1500 / FScanStep) and (100 * IdxList_[0] / NPLen > 3) then           // ��������� ������ � ��������� � %
      begin
      {
        SetLength(FUnCtrlZPt[R], Length(FUnCtrlZPt[R]) + 1);
        FUnCtrlZPt[R, High(FUnCtrlZPt[R])].StCrd:= FPBList[R, 0, IdxList[            2]].Crd.StCrd;
        FUnCtrlZPt[R, High(FUnCtrlZPt[R])].EdCrd:= FPBList[R, 0, IdxList[High(IdxList)]].Crd.EdCrd;
      }
      (*
        C1:= FPBList[R, 0, IdxList_[            2]].Crd.StCrd;
        C2:= FPBList[R, 0, IdxList_[High(IdxList_)]].Crd.EdCrd;
        begin
          Flg:= False;
          J:= C1;
          repeat
            SetLength(FUnCtrlZ[R], Length(FUnCtrlZ[R]) + 1);

            FUnCtrlZ[R, High(FUnCtrlZ[R])].Rng.StCrd:= J;
            FUnCtrlZ[R, High(FUnCtrlZ[R])].Rng.EdCrd:= J;
            SetLength(FUnCtrlZ[R, High(FUnCtrlZ[R])].IdxList, Length(IdxList_));
            for K:= 0 to High(IdxList_) do FUnCtrlZ[R, High(FUnCtrlZ[R])].IdxList[K]:= IdxList_[K];

            if Flg then Break;
            Inc(J, Round(100 / FScanStep));
            if J > C2 then
            begin
              J:= C2;
              Flg:= True;
            end;
          until False;
        end;
        *)

        SetLength(FUnCtrlZ[R], Length(FUnCtrlZ[R]) + 1);
        FUnCtrlZ[R, High(FUnCtrlZ[R])].Rng.StCrd:= FPBList[R, 0, IdxList_[             2]].Crd.StCrd;
        FUnCtrlZ[R, High(FUnCtrlZ[R])].Rng.EdCrd:= FPBList[R, 0, IdxList_[High(IdxList_)]].Crd.EdCrd;
        SetLength(FUnCtrlZ[R, High(FUnCtrlZ[R])].IdxList, Length(IdxList_));
        for K:= 0 to High(IdxList_) do FUnCtrlZ[R, High(FUnCtrlZ[R])].IdxList[K]:= IdxList_[K];

        for J:= IdxList_[2] to IdxList_[High(IdxList_)] do
// ASD          if not (FPBList[R, 0, J].Typ in [pbtHole, pbtEnd]) then
          if (pbtiClear in FPBList[R, 0, J].Typ) then
            FPBList[R, 0, J].Typ:= FPBList[R, 0, J].Typ + [pbtiUCS];
//            SetPBTyp(FPBList[R, 0, J], 9, pbtUCS);  // �������� �����
      end;
    end;
  end;
  SetLength(IdxList_, 0);
end;

// -----------------------------------------------------------------------------
// ---                                                   -----------------------
// -----------------------------------------------------------------------------
(*
procedure TRecData.KillUCS;
var
  R: RRail;
  I, J: Integer;
  Flg: Boolean;

begin       ( *
  for R:= rLeft to rRight do
    for I:= 0 to High(FConstruct[R]) do
      if FConstruct[R, I].Typ in [ct_Frog, ct_BoltJoint, ct_Blade] then
        for J:= 0 to High(FUnCtrlZPt[R]) do
          if MyTypes.InRange(FUnCtrlZPt[R, J], FConstruct[R, I].Body) then FUnCtrlZPt[R, J].StCrd:= - MaxInt;

  for R:= rLeft to rRight do
  begin
    Flg:= False;
    I:= 0;
    while I <= High(FUnCtrlZPt[R]) do
    begin
      if FUnCtrlZPt[R, I].StCrd <> - MaxInt then
      begin
        if not Flg then
        begin
          if Length(FUnCtrlZ[R]) <> 0 then   // ��������� ������ ��������� ���� ���� ��� ��������� �� ����� ����� ����� � ���
          begin
            if GetLen(FUnCtrlZ[R, High(FUnCtrlZ[R])]) >= Round(90 / FScanStep) then // ��� ������ ����� ����
              SetLength(FUnCtrlZ[R], Length(FUnCtrlZ[R]) + 1);
          end else SetLength(FUnCtrlZ[R], Length(FUnCtrlZ[R]) + 1); //
          FUnCtrlZ[R, High(FUnCtrlZ[R])].StCrd:= FUnCtrlZPt[R, I].StCrd;
          FUnCtrlZ[R, High(FUnCtrlZ[R])].EdCrd:= FUnCtrlZPt[R, I].EdCrd;
          Flg:= True;
          J:= I;
          Inc(I);
        end
        else
        if Flg then
        begin
          if Abs(FUnCtrlZPt[R, I].StCrd - FUnCtrlZPt[R, J].StCrd) <= Round(100 / FScanStep) then
          begin
            FUnCtrlZ[R, High(FUnCtrlZ[R])].EdCrd:= FUnCtrlZPt[R, I].StCrd;
            J:= I;
            Inc(I);
          end
          else Flg:= False;
        end;
      end else Inc(I);
    end;
  end;            * )
end;

// -----------------------------------------------------------------------------
// --- �������������� ������� ����� � ����������� ������ -----------------------
// -----------------------------------------------------------------------------

procedure TRecData.BigHole_to_PBList;
var
  R: RRail;
  Flg: Boolean;
  TmpR: TRange;
  I, J, C1, C2: Integer;

begin
(*
  for R:= rLeft to rRight do
    for I:= 0 to High(FPBList[R, 0]) do
//      if FPBList[R, 0, I].Typ <> pbtUCS then
      begin
        C1:= FPBList[R, 0, I].Crd.StCrd;
        C2:= FPBList[R, 0, I].Crd.EdCrd;
        if Abs(C2 - C1) > 300 / FScanStep then
        begin
          Flg:= False;
          J:= C1;
          repeat
            SetLength(FSingleObj[R], Length(FSingleObj[R]) + 1);

            TmpR.StCrd:= J;
            TmpR.EdCrd:= J;

            if TestRange(TmpR) then
            begin
              FSingleObj[R, High(FSingleObj[R])].DisRange.StCrd:= J;
              FSingleObj[R, High(FSingleObj[R])].DisRange.EdCrd:= J;
              FSingleObj[R, High(FSingleObj[R])].SysRange.StCrd:= FDatSrc.DisToSysCoord(J);
              FSingleObj[R, High(FSingleObj[R])].SysRange.EdCrd:= FDatSrc.DisToSysCoord(J);
              FSingleObj[R, High(FSingleObj[R])].Typ:= so_BSHole;
              FSingleObj[R, High(FSingleObj[R])].Use:= False;
            end;

            if Flg then Break;
            Inc(J, Round(500 / FScanStep));
            if J > C2 then
            begin
              J:= C2;
              Flg:= True;
            end;
          until False;
        end;
      end; * )
end;

// -----------------------------------------------------------------------------
// --- ����� ������� -----------------------------------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.BladeSearch;
var
  R: RRail;
  Flg: Boolean;
  I, J, K: Integer;

begin
  for R:= rLeft to rRight do
  begin
    I:= 0;
    while I <= High(FBSignal.FBSPos[R]) - 1 do // ���������� ������� ������ ���� ������� �������
    begin
      J:= I;
      while (Abs((FBSignal.FBSPos[R, J + 1].MaxDelay + FBSignal.FBSPos[R, J + 1].MinDelay) div 2 -             // ���� ������ ����� �������
                 (FBSignal.FBSPos[R, J    ].MaxDelay + FBSignal.FBSPos[R, J    ].MinDelay) div 2) <= 8) and
            (Abs(FBSignal.FBSPos[R, J].MaxDelay - FBSignal.FBSPos[R, J].MinDelay) <= 12) and
            (J <= High(FBSignal.FBSPos[R]) - 2) do Inc(J);

      if (I <> J) and                                                                        // ���� ��� ������ � ��������
         (FBSignal.FBSPos[R, J].Crd - FBSignal.FBSPos[R, I].Crd >  500 / FScanStep) and                        // 0.5 - 10.5 ������ � ��������
         (FBSignal.FBSPos[R, J].Crd - FBSignal.FBSPos[R, I].Crd < 10500 / FScanStep) and                       // �� ��� �� �������� ��� ��
         ((FBSignal.FBSPos[R, J].MinDelay + FBSignal.FBSPos[R, J].MaxDelay) / 2 <  54 * 3) then
      begin
        SetLength(FBlades[R], Length(FBlades[R]) + 1);                                       // ���� ��������
        FBlades[R, High(FBlades[R])].StCrd:= FBSignal.FBSPos[R, I].Crd - Round(400 / FScanStep);
        FBlades[R, High(FBlades[R])].EdCrd:= FBSignal.FBSPos[R, J].Crd + Round(400 / FScanStep);
     {
        Flg:= False;
        K:= FBSignal.FBSPos[R, I].Crd - Round(400 / FScanStep);
        repeat
          SetLength(FSingleObj[R], Length(FSingleObj[R]) + 1);
          FSingleObj[R, High(FSingleObj[R])].DisRange.StCrd:= K;
          FSingleObj[R, High(FSingleObj[R])].DisRange.EdCrd:= K;
          FSingleObj[R, High(FSingleObj[R])].SysRange.StCrd:= FDatSrc.DisToSysCoord(K);
          FSingleObj[R, High(FSingleObj[R])].SysRange.EdCrd:= FDatSrc.DisToSysCoord(K);
          FSingleObj[R, High(FSingleObj[R])].Typ:= so_Blade;
          FSingleObj[R, High(FSingleObj[R])].Use:= False;
          if Flg then Break;
          Inc(K, Round(500 / FScanStep));
          if K > FBSignal.FBSPos[R, J].Crd + Round(400 / FScanStep) then
          begin
            K:= FBSignal.FBSPos[R, J].Crd + Round(400 / FScanStep);
            Flg:= True;
          end;
        until False; }
      end;
      if I = J then Inc(I)
               else I:= J + 1;
    end;
  end;
end;

// -----------------------------------------------------------------------------
// --- C��������� �������� �� ������������ ���������� --------------------------
// -----------------------------------------------------------------------------

procedure TRecData.SortSingleObj;
var
  R: RRail;
  I, J, K, C: Integer;
  SortTmp3: TSingleObj;

begin
  for R:= rLeft to rRight do
    for I:= 0 to High(FSingleObj[R]) - 1 do
    begin
      K:= GetCentr(FSingleObj[R, I].SysRange);
      C:= - 1;
      for J:= I + 1 to High(FSingleObj[R]) do
      begin
        if K > GetCentr(FSingleObj[R, J].SysRange) then
        begin
          K:= GetCentr(FSingleObj[R, J].SysRange);
          C:= J;
        end;
      end;
      if C <> - 1 then
      begin
        SortTmp3:= FSingleObj[R, I];
        FSingleObj[R, I]:= FSingleObj[R, C];
        FSingleObj[R, C]:= SortTmp3;
      end;
    end;
end;

// -----------------------------------------------------------------------------
// ----------------------- ������������ ��� ��� ������� ������� ----------------
// -----------------------------------------------------------------------------

procedure TRecData.MakeHeadRailObj;
var
  R: RRail;
  A, B, C, D, I, J: Integer;
  Idx: TIntegerDynArray;
  Cur: TRange;
  List: TIntegerDynArray;
  Tmp: TIntegerDynArray;

begin
  for R:= rLeft to rRight do
  begin
    SetLength(Idx, 0);
    for I:= 0 to High(FSingleObj[R]) - 1 do
      if FSingleObj[R, I].Typ = so_End then
      begin
        SetLength(Idx, Length(Idx) + 1);
        Idx[High(Idx)]:= I;
      end;

    I:= 0;
    J:= 0;
    while I <= High(Idx) do
      if ((I <= High(Idx) - 1) and
          (Abs(GetCentr(FSingleObj[R, Idx[I]].SysRange) -
               GetCentr(FSingleObj[R, Idx[I + 1]].SysRange)) > 200 / FScanStep)) or
         (I = High(Idx)) then                                         // ���� ��������� ������� �� ����������
      begin
        Cur.StCrd:= + MaxInt;                                         // ���� ������������ � ����������� ���������� �����
        Cur.EdCrd:= - MaxInt;
        for A:= J to I do
        begin
          Cur.StCrd:= Min(Cur.StCrd, FSingleObj[R, Idx[A]].SysRange.StCrd);
          Cur.EdCrd:= Max(Cur.EdCrd, FSingleObj[R, Idx[A]].SysRange.EdCrd);
        end;
        Cur.StCrd:= FDatSrc.SysToDisCoord(Cur.StCrd);                 // ��������� � Display
        Cur.EdCrd:= FDatSrc.SysToDisCoord(Cur.EdCrd);

        SetLength(List, 0);                                           // �������� ��� ����� � ���� �����
        FDatSrc.DisToDisCoords(Cur.StCrd, List);
        FDatSrc.DisToDisCoords(Cur.EdCrd, List);

        SetLength(Tmp, 0);                                       // ���� ����� �������� ������ �� ����� ����� ����������� �������� ����� �� ������ - ������� ��������� ��
        for A:= 0 to High(List) do
          for C:= 0 to High(FBMList.FBMData) do
            for B:= 0 to High(FBMList.FBMData[C].DisItems) do
              if (List[A] = FBMList.FBMData[C].DisItems[B].EdCrd) then
                 begin
                   SetLength(Tmp, Length(Tmp) + 1);
                   Tmp[High(Tmp)]:= A;
                 end;

        for A:= High(Tmp) downto 0 do
        begin
          SetLength(List, Length(List) + 1);
          Move(List[Tmp[A]], List[Tmp[A] + 1], (Length(List) - Tmp[A] - 1) * SizeOf(Integer));
        end;

        SetLength(Tmp, 0);


        for A:= 0 to High(List) - 1 do                                // ��������� �����
        begin
          C:= A;
          for B:= A + 1 to High(List) do
            if List[C] > List[B] then C:= B;

          D:= List[A];
          List[A]:= List[C];
          List[C]:= D;
        end;

        A:= 0;                                                        // ������� 1 - �������� Range ������� ���������� � ���� �������� �����
        while A <= High(List) - 1 do
        begin
          if (BackMotion(List[A])) and (BackMotion(List[A + 1])) then
          begin
            if A + 1 < High(List) then                                // ���� ���� ����� ��������� ���
              Move(List[A + 2], List[A], (Length(List) - A - 2) * 4);
            SetLength(List, Length(List) - 2);                        // ������� ������
          end else Inc(A);
        end;

   {     if Odd(Length(List)) then
        begin
          SetLength(List, Length(List) + 1);
          List[High(List)]:= List[High(List) - 1];
        end;       }

        SetLength(FRailObj[R], Length(FRailObj[R]) + 1);              // ������� ������� ����
        SetLength(FRailObj[R, High(FRailObj[R])].HeadZones, Length(List) div 2);

        A:= 0;
        B:= 0;
//        Inc(GE);
        while A < Length(List) do
        begin
          FRailObj[R, High(FRailObj[R])].HeadZones[B].StCrd:= List[A];
          FRailObj[R, High(FRailObj[R])].HeadZones[B].EdCrd:= List[A + 1];
          Inc(A, 2);
          Inc(B);
        end;

        J:= I + 1;
        I:= I + 1;

      end else Inc(I);
  end;
  SetLength(Idx, 0);
end;

// -----------------------------------------------------------------------------
// --- ������� ����� ������������ �� ����� -------------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.MarkEndPB;
var
  R: RRail;
  Ch: Integer;
  I, J, K: Integer;
  TmpR: TRange;

begin
(*
  for R:= rLeft to rRight do
    for Ch:= 0 to 7 do
      for I:= 0 to High(FPBList[R, Ch]) do
        if (FPBList[R, Ch, I].Typ <> pbtHoleMain) and
           ((Ch in [0, 2..5]) or
            ((Ch in [6, 7]) and
             (FPBList[R, Ch, I].Del.StCrd in [120..174]) and
             (FPBList[R, Ch, I].Del.EdCrd in [120..174]))) then
        begin
          if Ch in [0..1] then
          begin
            TmpR.StCrd:= Round(FPBList[R, Ch, I].Crd.StCrd + ChList[Ch].Shift[2] / FScanStep);
            TmpR.EdCrd:= Round(FPBList[R, Ch, I].Crd.EdCrd + ChList[Ch].Shift[2] / FScanStep);
          end
          else
          begin
            TmpR.StCrd:= Round(FPBList[R, Ch, I].Crd.StCrd + ((ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * FPBList[R, Ch, I].Del.StCrd) / FScanStep));
            TmpR.EdCrd:= Round(FPBList[R, Ch, I].Crd.EdCrd + ((ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * FPBList[R, Ch, I].Del.EdCrd) / FScanStep));
          end;

          if Ch <> 0 then
          begin
            for J:= 0 to High(FConstruct[R]) do
              if (FConstruct[R, J].Typ = ct_BoltJoint) and
                 (TmpR.StCrd >= FConstruct[R, J].Head.StCrd) and
                 (TmpR.EdCrd <= FConstruct[R, J].Head.EdCrd) then
              begin
                SetPBTyp(FPBList[R, Ch, I], 2002, pbtEnd);
                Break;
              end;
          end
          else
            for J:= 0 to High(FConstruct[R]) do
              if (FConstruct[R, J].Typ = ct_BoltJoint) and
                 (PtInRange(GetCentr(FConstruct[R, J].Head), TmpR) or
                  PtInRange(GetCentr(TmpR), FConstruct[R, J].Head)) then
              begin
                SetPBTyp(FPBList[R, Ch, I], 2001, pbtEnd);
                Break;
              end;

          if Ch <> 0 then
          begin
            for J:= 0 to High(FRailObj[R]) do
              for K:= 0 to High(FRailObj[R, J].HeadZones) do

              if (TmpR.StCrd >= FRailObj[R, J].HeadZones[K].StCrd) and
                 (TmpR.EdCrd <= FRailObj[R, J].HeadZones[K].EdCrd) then
              begin
                SetPBTyp(FPBList[R, Ch, I], 2002, pbtEnd);
                Break;
              end;
          end
          else
            for J:= 0 to High(FRailObj[R]) do
              for K:= 0 to High(FRailObj[R, J].HeadZones) do
                if PtInRange(GetCentr(FRailObj[R, J].HeadZones[K]), TmpR) or
                   PtInRange(GetCentr(TmpR), FRailObj[R, J].HeadZones[K]) then
                begin
                  SetPBTyp(FPBList[R, Ch, I], 2001, pbtEnd);
                  Break;
                end;
        end;
        * )
end;

// -----------------------------------------------------------------------------
// --- ������� ����� ������������ �� ����� 2 -----------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.MarkEnd2PB;
var
  R: RRail;
  Ch: Integer;
  I, J, Len: Integer;
  TmpR: TRange;

begin
{
  Len:= Round(RecConfig.EndWidth / 2 / FScanStep);

  for R:= rLeft to rRight do
    for Ch:= 0 to 7 do
      for I:= 0 to High(FPBList[R, Ch]) do
        if (FPBList[R, Ch, I].Typ <> pbtHole) and
           ((Ch in [0, 2..5]) or
            ((Ch in [6, 7]) and
             (FPBList[R, Ch, I].Del.StCrd in [120..174]) and
             (FPBList[R, Ch, I].Del.EdCrd in [120..174]))) then
        begin
          TmpR.StCrd:= Round(FPBList[R, Ch, I].Crd.StCrd + ((ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * FPBList[R, Ch, I].Del.StCrd) / FScanStep));
          TmpR.EdCrd:= Round(FPBList[R, Ch, I].Crd.EdCrd + ((ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * FPBList[R, Ch, I].Del.EdCrd) / FScanStep));

          if Ch <> 0 then
          begin
            for J:= 0 to High(FEnd3[R]) do
              if FEnd3[R, J].Ok and
                 (TmpR.StCrd >= FEnd3[R, J].Crd - Len) and
                 (TmpR.EdCrd <= FEnd3[R, J].Crd + Len) then
              begin
                SetPBTyp(FPBList[R, Ch, I], 1111, pbtEnd);
                Break;
              end;
          end
          else
            for J:= 0 to High(FEnd3[R]) do
            begin
              if FEnd3[R, J].Ok and
                 (PtInRange(FEnd3[R, J].Crd, TmpR) or
                  PtInRange(GetCentr(TmpR), Range(FEnd3[R, J].Crd - Len, FEnd3[R, J].Crd + Len))) then
              begin
                SetPBTyp(FPBList[R, Ch, I], 1111, pbtEnd);
                Break;
              end;
            end;
        end;
        }
end;

// -----------------------------------------------------------------------------
// ----------- ��������� ����� ������������� ����� � ����� �������� �����  ------------------
// -----------------------------------------------------------------------------

procedure TRecData.MarkUnknownPB;
var
  R: RRail;
  I, J, K: Integer;
  Ch: Integer;

begin                           (*
  for R:= rLeft to rRight do
    for Ch:= 6 to 7 do
      for I:= 0 to High(FPBList[R, Ch]) do
        if FPBList[R, Ch, I].Typ in [pbtForHole] then
          for J:= 0 to High(FBMList.FBMData) do
            for K:= 0 to High(FBMList.FBMData[J].DisItems) do
              if Odd(K) then
              begin
                if (Ch = 7) and
                   (FPBList[R, Ch, I].Crd.StCrd >= FBMList.FBMData[J].DisItems[K].EdCrd) and
                   (FPBList[R, Ch, I].Crd.EdCrd <= FBMList.FBMData[J].DisItems[K].EdCrd + 270 / FScanStep) then SetPBTyp(FPBList[R, Ch, I], 11, pbtUnknown);

                if (Ch = 6) and
                   (FPBList[R, Ch, I].Crd.StCrd >= FBMList.FBMData[J].DisItems[K].EdCrd) and
                   (FPBList[R, Ch, I].Crd.EdCrd <= FBMList.FBMData[J].DisItems[K].EdCrd + 170 / FScanStep) then SetPBTyp(FPBList[R, Ch, I], 11, pbtUnknown);
              end;                * )
end;

// -----------------------------------------------------------------------------
// ----------------------- ������������ ��� �� �����  ---------------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.MakeBodyRailObj;
var
  R: RRail;
  Cur: TRange;
  I, J, A, B, C, D, F: Integer;
  Tmp: TIntegerDynArray;
  Idx: TIntegerDynArray;
  List: TIntegerDynArray;
  ResCrd: TIntegerDynArray;
  TypCount: array [TSingleObjTyp] of Integer;

begin
  for R:= rLeft to rRight do
  begin
    SetLength(Idx, 0);               // ������ ������������ ����� - ������� ������ �� so_Hole, so_BSHole, so_Blade
    for I:= 0 to High(FSingleObj[R]) do
      if (FSingleObj[R, I].Typ in [so_BHOk, so_BHUnCtrl, so_BHDef, so_BSHole, so_Blade]) and
         (FSingleObj[R, I].DisRange.StCrd > FStCrd) and
         (FSingleObj[R, I].DisRange.EdCrd < FEdCrd) then
      begin
        SetLength(Idx, Length(Idx) + 1);
        Idx[High(Idx)]:= I;
      end;

    I:= 0;
    J:= 0;
    while I <= High(Idx) do          // ���� �� ���������
    begin
                                     // ���������� �� ���������� 1,7 ����� (�� ��������� ����������)
      if ((I <= High(Idx) - 1) and                                      // ���� �� ��������� ������� �� ���������� I � I + 1
          (Abs(GetCentr(FSingleObj[R, Idx[I]].SysRange) -
               GetCentr(FSingleObj[R, Idx[I + 1]].SysRange)) > 1700 / FScanStep)) or

         (I = High(Idx)) then                                           // ���� ��������� ������� �� ����������

      begin

        FillChar(TypCount[so_BHOk], SizeOf(TypCount), 0);          // ���������� ���������� ����

        for A:= J to I do
          Inc(TypCount[FSingleObj[R, Idx[A]].Typ]);

        if not ((TypCount[so_BHOk] = 0) and                      // ���������� ��������� ���������� ������ �� ������� ������
                (TypCount[so_BHUnCtrl] = 0) and
                (TypCount[so_BHDef] = 0) and
                (TypCount[so_End] = 0) and
                (TypCount[so_BSHole] <> 0) and
                (TypCount[so_Blade] = 0)) then
        begin
          Cur.StCrd:= + MaxInt;                                      // ���� ������������ � ����������� ����������
          Cur.EdCrd:= - MaxInt;
          for A:= J to I do
          begin
            Cur.StCrd:= Min(Cur.StCrd, FSingleObj[R, Idx[A]].SysRange.StCrd);
            Cur.EdCrd:= Max(Cur.EdCrd, FSingleObj[R, Idx[A]].SysRange.EdCrd);
          end;
          Cur.StCrd:= FDatSrc.SysToDisCoord(Cur.StCrd);            // ��������� � Display
          Cur.EdCrd:= FDatSrc.SysToDisCoord(Cur.EdCrd);

          SetLength(List, 0);                                      // �������� ��� ����� �� ���� �����
          FDatSrc.DisToDisCoords(Cur.StCrd, List);                 // ��� ������
          A:= Length(List) + 1;
          FDatSrc.DisToDisCoords(Cur.EdCrd, List);                 // ��� �����
             
          SetLength(Tmp, 0);                                       // ���� ����� �������� ������ �� ����� ����� ����������� �������� ����� �� ������ - ������� ��������� ��
          for A:= 0 to High(List) do
            for C:= 0 to High(FBMList.FBMData) do
              for B:= 0 to High(FBMList.FBMData[C].DisItems) do
                if (List[A] = FBMList.FBMData[C].DisItems[B].EdCrd) then
                   begin
                     SetLength(Tmp, Length(Tmp) + 1);
                     Tmp[High(Tmp)]:= A;
                   end;

          for A:= High(Tmp) downto 0 do
          begin
            SetLength(List, Length(List) + 1);
            Move(List[Tmp[A]], List[Tmp[A] + 1], (Length(List) - Tmp[A] - 1) * SizeOf(Integer));
          end;

          SetLength(Tmp, 0);

          for A:= 0 to High(List) - 1 do                           // ��������� �����
          begin
            C:= A;
            for B:= A + 1 to High(List) do
              if List[C] > List[B] then C:= B;

            D:= List[A];
            List[A]:= List[C];
            List[C]:= D;
          end;

          {$IFDEF RECDEBUG}
          B:= Length(FPointList[R]);                               // �������
          SetLength(FPointList[R], B + Length(List));
          for A:= 0 to High(List) do
          begin
            FPointList[R, B + A].Crd:= List[A];
            FPointList[R, B + A].Typ:= 1;
          end;
          {$ENDIF}
                                                                   // ��������� ����� ��� ��������� ���
          A:= 0;                                                   // ������� 1 - �������� Range ������� ���������� � ���� �������� �����
          while A <= High(List) - 1 do
          begin
            if (BackMotion(List[A])) and (BackMotion(List[A + 1])) then
            begin
              if A + 1 < High(List) then                                // ���� ���� ����� ��������� ���
                Move(List[A + 2], List[A], (Length(List) - A - 2) * 4);
              SetLength(List, Length(List) - 2);                        // ������� ������
            end else Inc(A);
          end;

          A:= 0;                                                   // ������� 2 - ������� ���������� ����� �������� � ��� - � ������ / � ����� ����
          while A <= High(List) - 1 do
          begin
            if BackMotion(List[A]) then
            begin
               if Odd(A) then MoveFrom(List[A], - 1)               // � ������ / � ����� ���� � ����������� �� ������ �����
                         else MoveFrom(List[A], + 1);
            end;
            Inc(A);
          end;
                                                                   // ������� 3 -  ��� ����� � ���� �������� ������, �� ��������� ��� �� ������ �� ���� ��� �������� �����
          A:= 0;
          C:= High(List) - 1;                                      // ������������� ����� - �� ��� ���� �� ������ ������
          while A <= C do
          begin
            TestRange2(MyTypes.Range(List[A], List[A + 1]), ResCrd); // ������� ���������� ����� ����������� ������������ �� ������� Range
            for B:= 0 to High(ResCrd) do
            begin
              SetLength(List, Length(List) + 1);                     // ���������� ��������� �����
              List[High(List)]:= ResCrd[B];
            end;
            Inc(A, 2);
          end;

          for A:= 0 to High(List) - 1 do                           // ��������� ����� - ��������
          begin
            C:= A;
            for B:= A + 1 to High(List) do
              if List[C] > List[B] then C:= B;
            D:= List[A];
            List[A]:= List[C];
            List[C]:= D;
          end;

          {$IFDEF RECDEBUG}
          B:= Length(FPointList[R]);                                    // �������
          SetLength(FPointList[R], B + Length(List));
          for A:= 0 to High(List) do
          begin
            FPointList[R, B + A].Crd:= List[A];
            FPointList[R, B + A].Typ:= 2;
          end;
          {$ENDIF}

          SetLength(FRailObj[R], Length(FRailObj[R]) + 1);           // ������� ������� ����
          SetLength(FRailObj[R, High(FRailObj[R])].BodyZones, Length(List) div 2);

          A:= 0;
          B:= 0;
          while A < High(List) do                                    // ��������� ���� �� �����
          begin
            FRailObj[R, High(FRailObj[R])].BodyZones[B].Range.StCrd:= List[A];
            FRailObj[R, High(FRailObj[R])].BodyZones[B].Range.EdCrd:= List[A + 1];

            for D:= J to I do                                          // �������� ������ �������� �� ��������� �������� ������� ������ �������
            begin
              if (FSingleObj[R, Idx[D]].DisRange.StCrd <= List[A + 1]) and // ��������� ���� �� ����������
                 (FSingleObj[R, Idx[D]].DisRange.EdCrd >= List[A]) then
              begin
                F:= Length(FRailObj[R, High(FRailObj[R])].BodyZones[B].ObjList);
                SetLength(FRailObj[R, High(FRailObj[R])].BodyZones[B].ObjList, F + 1);
                FRailObj[R, High(FRailObj[R])].BodyZones[B].ObjList[F]:= Idx[D];
              end;
            end;

            Inc(A, 2);
            Inc(B);
          end;
        end;

        J:= I + 1; // ��� ����������� �������������
        I:= I + 1;
      end else Inc(I);
    end;
  end;
  SetLength(Idx, 0);
end;

// -----------------------------------------------------------------------------
// --- ���������� ��� ������ ��� �� �� ������� -----------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.HeadSearchZone;
var
  R: RRail;
  I, J, K, L, M: Integer;
  SavePt: TPtnItem;

begin
  for R:= rLeft to rRight do
  begin
    SetLength(FPtn1[R], 0);                               // ��������� �����

    SetLength(FPtn1[R], Length(FPtn1[R]) + 2);              // ����� ��� ������
    FPtn1[R][High(FPtn1[R]) - 1].Typ:= 1;
    FPtn1[R][High(FPtn1[R]) - 1].Idx1:= 0;
    FPtn1[R][High(FPtn1[R]) - 1].Idx2:= 0;
    FPtn1[R][High(FPtn1[R]) - 1].Crd:= FStCrd;
    FPtn1[R][High(FPtn1[R]) - 1].State:= True;
    FPtn1[R][High(FPtn1[R])].Typ:= 4;                       // � ��� �����
    FPtn1[R][High(FPtn1[R])].Idx1:= 0;
    FPtn1[R][High(FPtn1[R])].Idx2:= 0;
    FPtn1[R][High(FPtn1[R])].Crd:= FEdCrd;
    FPtn1[R][High(FPtn1[R])].State:= True;

    for I:= 0 to High(FRailObj[R]) do                     // ���� ������
      if Length(FRailObj[R, I].HeadZones) <> 0 then
        for J:= 0 to High(FRailObj[R, I].HeadZones) do
        begin
          SetLength(FPtn1[R], Length(FPtn1[R]) + 2);
          FPtn1[R][High(FPtn1[R]) - 1].Typ:= 2;             // ������ �����
          FPtn1[R][High(FPtn1[R]) - 1].Idx1:= I;
          FPtn1[R][High(FPtn1[R]) - 1].Idx2:= J;
          FPtn1[R][High(FPtn1[R]) - 1].Crd:= FRailObj[R, I].HeadZones[J].StCrd;
          FPtn1[R][High(FPtn1[R]) - 1].State:= True;
          FPtn1[R][High(FPtn1[R])].Typ:= 2;                 // ����� �����
          FPtn1[R][High(FPtn1[R])].Idx1:= I;
          FPtn1[R][High(FPtn1[R])].Idx2:= J;
          FPtn1[R][High(FPtn1[R])].Crd:= FRailObj[R, I].HeadZones[J].EdCrd;
          FPtn1[R][High(FPtn1[R])].State:= True;
        end;

    for I:= 0 to High(FBMList.FBMData) do                         // ���� �������� �����
      for J:= 0 to High(FBMList.FBMData[I].DisItems) do
      if Odd(J) and
         (FBMList.FBMData[I].DisItems[J].StCrd <= FEdCrd) and
         (FBMList.FBMData[I].DisItems[J].EdCrd >= FStCrd) then
      begin
        SetLength(FPtn1[R], Length(FPtn1[R]) + 2);
        FPtn1[R][High(FPtn1[R]) - 1].Typ:= 3;               // ������ ����
        FPtn1[R][High(FPtn1[R]) - 1].Idx1:= I;
        FPtn1[R][High(FPtn1[R]) - 1].Idx2:= J;
        FPtn1[R][High(FPtn1[R]) - 1].Crd:= FBMList.FBMData[I].DisItems[J].StCrd;
        FPtn1[R][High(FPtn1[R]) - 1].State:= True;
        FPtn1[R][High(FPtn1[R])].Typ:= 3;                   // ����� ����
        FPtn1[R][High(FPtn1[R])].Idx1:= I;
        FPtn1[R][High(FPtn1[R])].Idx2:= J;
        FPtn1[R][High(FPtn1[R])].Crd:= FBMList.FBMData[I].DisItems[J].EdCrd;
        FPtn1[R][High(FPtn1[R])].State:= True;
      end;

    for I:= 0 to High(FPtn1[R]) - 1 do                     // ���������� ����� �� ����������
    begin
      K:= I;
      for J:= I + 1 to High(FPtn1[R]) do
        if FPtn1[R][K].Crd > FPtn1[R][J].Crd then K:= J;
      if K <> I then
      begin
        SavePt:= FPtn1[R][I];
        FPtn1[R][I]:= FPtn1[R][K];
        FPtn1[R][K]:= SavePt;
      end;
    end;

    L:= Length(FHZone[R]);                             // ������� ����� ���� - �������� L
    SetLength(FHZone[R], L + 1);
    FHZone[R, L].Idx:= - 1;                            // ������ ����� ����������

    for I:= 0 to High(FPtn1[R]) - 1 do                     // ������������� 2 ����� I-� � I+1 �
    begin

      if ((FPtn1[R, I].Typ = 2) or (FPtn1[R, I + 1].Typ = 2)) and  // ���� ��� ����� ����� ...
         (FHZone[R, L].Idx = - 1) then FHZone[R, L].Idx:= FPtn1[R, I].Idx1;  // ... � ������ ����� ��� �� ���������� �� ���������� ������ �����

      if (FPtn1[R, I].Typ = 2) and (FPtn1[R, I + 1].Typ = 2) and (FPtn1[R, I].Idx1 = FPtn1[R, I + 1].Idx1) then // ���� ��� ����� ����� ...
      begin
        if L = High(FHZone[R]) then    // ���� ������ �������� � ��������� ����� - �� ������� ����� ����
        begin
          L:= Length(FHZone[R]);
          SetLength(FHZone[R], L + 1);
          FHZone[R, L].Idx:= - 1;
        end else Inc(L);                  // ���� ������ �������� � ���������� ����� - �� ������������ �� ��������� ����
      end;
                                                              // ���� 2 ����� - ���� �������� ����� - �� ...
      if (FPtn1[R, I].Typ = 3) and (FPtn1[R, I + 1].Typ = 3) and (FPtn1[R, I].Idx1 = FPtn1[R, I + 1].Idx1) and (FPtn1[R, I].Idx2 = FPtn1[R, I + 1].Idx2) then
      begin
        if (FHZone[R, L].Idx <> - 1) and
           (FPtn1[R, I].Crd < FRailObj[R, FHZone[R, L].Idx].HeadZones[High(FRailObj[R, FHZone[R, L].Idx].HeadZones)].EdCrd) then
          Dec(L);                                             // ������������ � ���������� ���������� ����
      end;
                                                                  // � ���� ��������� ������� �������
      if ((FPtn1[R, I].Typ = 1) and (FPtn1[R, I + 1].Typ = 2)) or   // ������ - �����
         ((FPtn1[R, I].Typ = 1) and (FPtn1[R, I + 1].Typ = 3)) or   // ������ - ���
         ((FPtn1[R, I].Typ = 1) and (FPtn1[R, I + 1].Typ = 4)) or   // ������ - �����
         ((FPtn1[R, I].Typ = 2) and (FPtn1[R, I + 1].Typ = 2) and (FPtn1[R, I].Idx1 <> FPtn1[R, I + 1].Idx1)) or // ����� - ����� (������)
         ((FPtn1[R, I].Typ = 2) and (FPtn1[R, I + 1].Typ = 3)) or   // ����� - ���
         ((FPtn1[R, I].Typ = 2) and (FPtn1[R, I + 1].Typ = 4)) or   // ����� - �����
         ((FPtn1[R, I].Typ = 3) and (FPtn1[R, I + 1].Typ = 2)) or   // ��� - �����
         ((FPtn1[R, I].Typ = 3) and (FPtn1[R, I + 1].Typ = 3) and ((FPtn1[R, I].Idx1 <> FPtn1[R, I + 1].Idx1) or (FPtn1[R, I].Idx2 <> FPtn1[R, I + 1].Idx2))) or // ��� - ��� (������)
         ((FPtn1[R, I].Typ = 3) and (FPtn1[R, I + 1].Typ = 4)) then // ��� - �����
      begin
        M:= Length(FHZone[R, L].Items);                        // ����� ������� �� ���� ����� I � I + 1
        SetLength(FHZone[R, L].Items, M + 1);
        FHZone[R, L].Items[M].StCrd:= FPtn1[R, I].Crd;
        FHZone[R, L].Items[M].EdCrd:= FPtn1[R, I + 1].Crd;
      end;
    end;

  end;
end;

// -----------------------------------------------------------------------------
// --- ���������� ��� ������ ��� �� �� ����� -----------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.BodySearchZone;
var
  R: RRail;
  I, J, K, L, M: Integer;
  SavePt: TPtnItem;
  Flg1, Flg2, Flg3: Boolean;

begin
  for R:= rLeft to rRight do
  begin
    SetLength(FPtn2[R], 0);                               // ��������� �����

    SetLength(FPtn2[R], Length(FPtn2[R]) + 2);              // ����� ��� ������
    FPtn2[R][High(FPtn2[R]) - 1].Typ:= 1;
    FPtn2[R][High(FPtn2[R]) - 1].Idx1:= 0;
    FPtn2[R][High(FPtn2[R]) - 1].Idx2:= 0;
    FPtn2[R][High(FPtn2[R]) - 1].Crd:= FStCrd;
    FPtn2[R][High(FPtn2[R]) - 1].State:= True;
    FPtn2[R][High(FPtn2[R])].Typ:= 4;                       // � ��� �����
    FPtn2[R][High(FPtn2[R])].Idx1:= 0;
    FPtn2[R][High(FPtn2[R])].Idx2:= 0;
    FPtn2[R][High(FPtn2[R])].Crd:= FEdCrd;
    FPtn2[R][High(FPtn2[R])].State:= True;

    for I:= 0 to High(FRailObj[R]) do                     // ���� ������
      if Length(FRailObj[R, I].HeadZones) <> 0 then
        for J:= 0 to High(FRailObj[R, I].HeadZones) do
        begin
          SetLength(FPtn2[R], Length(FPtn2[R]) + 2);
          FPtn2[R][High(FPtn2[R]) - 1].Typ:= 2;             // ������ �����
          FPtn2[R][High(FPtn2[R]) - 1].Idx1:= I;
          FPtn2[R][High(FPtn2[R]) - 1].Idx2:= J;
          FPtn2[R][High(FPtn2[R]) - 1].Crd:= GetCentr(FRailObj[R, I].HeadZones[J]) - 1;
          FPtn2[R][High(FPtn2[R]) - 1].State:= True;
          FPtn2[R][High(FPtn2[R])].Typ:= 2;                 // ����� �����
          FPtn2[R][High(FPtn2[R])].Idx1:= I;
          FPtn2[R][High(FPtn2[R])].Idx2:= J;
          FPtn2[R][High(FPtn2[R])].Crd:= GetCentr(FRailObj[R, I].HeadZones[J]) + 1;
          FPtn2[R][High(FPtn2[R])].State:= True;
        end;

    for I:= 0 to High(FBMList.FBMData) do                         // ���� �������� �����
      for J:= 0 to High(FBMList.FBMData[I].DisItems) do
      if Odd(J) and
         (FBMList.FBMData[I].DisItems[J].StCrd <= FEdCrd) and
         (FBMList.FBMData[I].DisItems[J].EdCrd >= FStCrd) then
      begin
        SetLength(FPtn2[R], Length(FPtn2[R]) + 2);
        FPtn2[R][High(FPtn2[R]) - 1].Typ:= 3;               // ������ ����
        FPtn2[R][High(FPtn2[R]) - 1].Idx1:= I;
        FPtn2[R][High(FPtn2[R]) - 1].Idx2:= J;
        FPtn2[R][High(FPtn2[R]) - 1].Crd:= FBMList.FBMData[I].DisItems[J].StCrd;
        FPtn2[R][High(FPtn2[R]) - 1].State:= True;
        FPtn2[R][High(FPtn2[R])].Typ:= 3;                   // ����� ����
        FPtn2[R][High(FPtn2[R])].Idx1:= I;
        FPtn2[R][High(FPtn2[R])].Idx2:= J;
        FPtn2[R][High(FPtn2[R])].Crd:= FBMList.FBMData[I].DisItems[J].EdCrd;
        FPtn2[R][High(FPtn2[R])].State:= True;
      end;

    for I:= 0 to High(FPtn2[R]) - 1 do                     // ���������� ����� �� ����������
    begin
      K:= I;
      for J:= I + 1 to High(FPtn2[R]) do
        if FPtn2[R][K].Crd > FPtn2[R][J].Crd then K:= J;
      if K <> I then
      begin
        SavePt:= FPtn2[R][I];
        FPtn2[R][I]:= FPtn2[R][K];
        FPtn2[R][K]:= SavePt;
      end;
    end;

    L:= Length(FBZone[R]);                             // ������� ����� ���� - �������� L
    SetLength(FBZone[R], L + 1);
    FBZone[R, L].Idx:= - 1;                            // ������ ����� ����������

    for I:= 0 to High(FPtn2[R]) - 1 do                     // ������������� 2 ����� I-� � I+1 �
    begin
      Flg1:= (FPtn2[R, I].Typ = 2);
      Flg2:= FPtn2[R, I + 1].Typ = 2;
      Flg3:= FBZone[R, L].Idx = - 1;

      if (Flg1 or Flg2) and  // ���� ��� ����� ����� ...
         (Flg3) then FBZone[R, L].Idx:= FPtn2[R, I].Idx1;  // ... � ������ ����� ��� �� ���������� �� ���������� ������ �����


//      if ((FPtn2[R, I].Typ = 2) or (FPtn2[R, I + 1].Typ = 2)) and  // ���� ��� ����� ����� ...
//         (FBZone[R, L].Idx = - 1) then FBZone[R, L].Idx:= FPtn2[R, I].Idx1;  // ... � ������ ����� ��� �� ���������� �� ���������� ������ �����

      if (FPtn2[R, I].Typ = 2) and (FPtn2[R, I + 1].Typ = 2) and (FPtn2[R, I].Idx1 = FPtn2[R, I + 1].Idx1) then // ���� ��� ����� ����� ...
      begin
        if L = High(FBZone[R]) then    // ���� ������ �������� � ��������� ����� - �� ������� ����� ����
        begin
          L:= Length(FBZone[R]);
          SetLength(FBZone[R], L + 1);
          FBZone[R, L].Idx:= - 1;
        end else Inc(L);                  // ���� ������ �������� � ���������� ����� - �� ������������ �� ��������� ����
      end;
                                                              // ���� 2 ����� - ���� �������� ����� - �� ...
      if (FPtn2[R, I].Typ = 3) and (FPtn2[R, I + 1].Typ = 3) and (FPtn2[R, I].Idx1 = FPtn2[R, I + 1].Idx1) and (FPtn2[R, I].Idx2 = FPtn2[R, I + 1].Idx2) then
      begin
        if (FBZone[R, L].Idx <> - 1) and
           (FPtn2[R, I].Crd < FRailObj[R, FBZone[R, L].Idx].HeadZones[High(FRailObj[R, FBZone[R, L].Idx].HeadZones)].EdCrd) then
          Dec(L);                                             // ������������ � ���������� ���������� ����
      end;
                                                                  // � ���� ��������� ������� �������
      if ((FPtn2[R, I].Typ = 1) and (FPtn2[R, I + 1].Typ = 2)) or   // ������ - �����
         ((FPtn2[R, I].Typ = 1) and (FPtn2[R, I + 1].Typ = 3)) or   // ������ - ���
         ((FPtn2[R, I].Typ = 1) and (FPtn2[R, I + 1].Typ = 4)) or   // ������ - �����
         ((FPtn2[R, I].Typ = 2) and (FPtn2[R, I + 1].Typ = 2) and (FPtn2[R, I].Idx1 <> FPtn2[R, I + 1].Idx1)) or // ����� - ����� (������)
         ((FPtn2[R, I].Typ = 2) and (FPtn2[R, I + 1].Typ = 3)) or   // ����� - ���
         ((FPtn2[R, I].Typ = 2) and (FPtn2[R, I + 1].Typ = 4)) or   // ����� - �����
         ((FPtn2[R, I].Typ = 3) and (FPtn2[R, I + 1].Typ = 2)) or   // ��� - �����
         ((FPtn2[R, I].Typ = 3) and (FPtn2[R, I + 1].Typ = 3) and ((FPtn2[R, I].Idx1 <> FPtn2[R, I + 1].Idx1) or (FPtn2[R, I].Idx2 <> FPtn2[R, I + 1].Idx2))) or // ��� - ��� (������)
         ((FPtn2[R, I].Typ = 3) and (FPtn2[R, I + 1].Typ = 4)) then // ��� - �����
      begin
        M:= Length(FBZone[R, L].Items);                        // ����� ������� �� ���� ����� I � I + 1
        SetLength(FBZone[R, L].Items, M + 1);
        FBZone[R, L].Items[M].StCrd:= FPtn2[R, I].Crd;
        FBZone[R, L].Items[M].EdCrd:= FPtn2[R, I + 1].Crd;
      end;
    end;
  end;
end;

// -----------------------------------------------------------------------------
// --- ����� ������� ������� - �� ������� ����� --------------------------------
// -----------------------------------------------------------------------------

function TRecData.BodyNoiseBlockOK(StartDisCoord: Integer): Boolean;
var
  R: RRail;
  Ch: Integer;
  I, J, K, F: Integer;
  Summ: Integer;
  ChSumm: Integer;
  MaxPos: Integer;
  MaxVol: Integer;
  Crd: Integer;

  MinIdx: Integer;
  MinLen: Single;
  Flg: Boolean;

  FindPik: Boolean;
  Koeff: Single;
  Vol: Single;
  Delta: Single;
  DeltaCrd: Integer;

  Buff: TDestDat;
  X, D: Integer;
  DL, DH: Integer;

begin           (*
  for Ch:= 1 to 7 do                                // ���������� ����������� ��� �������� ����� � �������
    if Ch in [1, 6, 7] then
    begin
      Buff:= FDatSrc.CurEcho[FRail, Ch];
      if Buff.Count = 0 then Continue;

      if Ch = 1 then
        for I:= FParam[3] to High(FBSignal.FBSPos[FRail]) - 1 do // ���������� ������� ������ ���� ������� �������
        begin
          if (FDatSrc.CurDisCoord >= FBSignal.FBSPos[FRail, I].Crd) and
             (FDatSrc.CurDisCoord <  FBSignal.FBSPos[FRail, I + 1].Crd) then
          begin
            FParam[3]:= I;
            Break;
          end;
        end;

      if Length(FPBList[FRail, Ch]) <> 0 then // ���� ���� ����� �� �������� � ���� - ��������� (���������) ������� ������������ �� �����
      begin
        J:= FIdx[FRail, Ch];                                       // ����������� ����� �� ������� ����������
        while FPBList[FRail, Ch, J].Crd.EdCrd < FDatSrc.CurDisCoord do
          if J < High(FPBList[FRail, Ch]) then Inc(J)
                                           else Break;
        FIdx[FRail, Ch]:= J;                                       // ������������ ����� ���������� ������� ����������
        while FPBList[FRail, Ch, J].Crd.EdCrd - FPBMaxLen[FRail, Ch] <= FDatSrc.CurDisCoord do
        begin
          if (FPBList[FRail, Ch, J].Crd.StCrd <= FDatSrc.CurDisCoord) and
             (FPBList[FRail, Ch, J].Crd.EdCrd >= FDatSrc.CurDisCoord) and
             ((FPBList[FRail, Ch, J].Typ = pbtHole) or
              (FPBList[FRail, Ch, J].Typ = pbtEnd) or
              (FPBList[FRail, Ch, J].Typ = pbtUnknown)) then
          begin
            if FPBList[FRail, Ch, J].EchoCount < 4 then        // ���� ����� �������� - ����� �� �������� �� ������ � �����
            begin
              DL:= FPBList[FRail, Ch, J].Crd.EdCrd - FPBList[FRail, Ch, J].Crd.StCrd + 1;
              DH:= FPBList[FRail, Ch, J].Del.EdCrd - FPBList[FRail, Ch, J].Del.StCrd + 1;
              X:= Abs(FPBList[FRail, Ch, J].Crd.StCrd - FDatSrc.CurDisCoord);
              D:= FPBList[FRail, Ch, J].Del.StCrd + Round(X * DH / DL); // ���������� �������� ��� ������� ����������
            end
            else                                               // ���� ����� ������ - ����� �� �������� �� 3 ������
            begin
              if FDatSrc.CurDisCoord <= FPBList[FRail, Ch, J].CCrd then // �������� � ������ ��������
              begin
                DL:= FPBList[FRail, Ch, J].CCrd - FPBList[FRail, Ch, J].Crd.StCrd + 1;
                DH:= FPBList[FRail, Ch, J].CDel - FPBList[FRail, Ch, J].Del.StCrd + 1;
                X:= Abs(FPBList[FRail, Ch, J].Crd.StCrd - FDatSrc.CurDisCoord);
                D:= FPBList[FRail, Ch, J].Del.StCrd + Round(X * DH / DL); // ���������� �������� ��� ������� ����������
              end
              else                                                            // �������� �� ������ ��������
              begin
                DL:= FPBList[FRail, Ch, J].Crd.EdCrd - FPBList[FRail, Ch, J].CCrd + 1;
                DH:= FPBList[FRail, Ch, J].Del.EdCrd - FPBList[FRail, Ch, J].CDel + 1;
                X:= Abs(FPBList[FRail, Ch, J].CCrd - FDatSrc.CurDisCoord);
                D:= FPBList[FRail, Ch, J].CDel + Round(X * DH / DL); // ���������� �������� ��� ������� ����������
              end;
            end;
            for I:= 1 to Buff.Count do         // �������� ������� ���������� � �����
              if (Buff.Delay[I] >= D - 5) and (Buff.Delay[I] <= D + 5) then Buff.Ampl[I]:= 255;
          end;
          Inc(J);
          if J > High(FPBList[FRail, Ch]) then Break;
        end;
      end;

      for I:= 1 to Buff.Count do         // ������� ���������� � ����� - ���������� - �� ���������� ��� ���������� �����������
        if (Buff.Ampl[I] <> 255) and     // ������ �� ������������
           ((Ch <> 1) or                 // �� ������ �����
           ((Ch = 1) and
            ((Buff.Delay[I] < FBSignal.FBSPos[FRail, FParam[3]].MinDelay) or // ������ ����� ��������� ��� ������ �� ��
             (Buff.Delay[I] > FBSignal.FBSPos[FRail, FParam[3]].MaxDelay)))) then
          Inc(FBZone[FRail, FParam[1]].Gis[Ch].AmplGis[Buff.Delay[I]], Sqr(16 - Buff.Ampl[I]));
    end;             * )
end;

procedure TRecData.MakeBodyNoisePath;
var
  R: RRail;
  Ch: Integer;
  Flg: Boolean;
  I, J, K, L, M, C, Q: Integer;

begin
  for R:= rLeft to rRight do
    for I:= 0 to High(FBZone[R]) do            // ������������� ����
    begin
      for Ch:= 1 to 7 do
        if Ch in [1, 6, 7] then
        begin
          FIdx[R, Ch]:= 0;
          FillChar(FBZone[R, I].Gis[Ch].AmplGis[0], 256 * 4, 0);  // �������������� �����������
        end;

      FBZone[R, I].Len:= 0;
      for J:= 0 to High(FBZone[R, I].Items) do  // ������������� �������� ����
      begin
        FRail:= R;
        FParam[1]:= I;
        FParam[2]:= J;
        FParam[3]:= 0;
                                                                        // ���������� ����������
        FDatSrc.LoadData(FBZone[R, I].Items[J].StCrd, FBZone[R, I].Items[J].EdCrd, 0, BodyNoiseBlockOK);
        Inc(FBZone[R, I].Len, FBZone[R, I].Items[J].EdCrd - FBZone[R, I].Items[J].StCrd);
      end;

                                         // ����������� ���������� - ������� �����
      with FBZone[R, I] do
      begin
        L:= 10000; // 10 + Round(1 * Len * FScanStep / 22000);
        for Ch:= 1 to 7 do
          if Ch in [1, 6, 7] then
          begin
            Flg:= False;
            for Q:= 0 to 255 do
            begin
              if (not Flg) and (Gis[Ch].AmplGis[Q] >= L) then
              begin
                J:= Gis[Ch].AmplGis[Q];
                Flg:= True;
                K:= Q;
              end;
              if Flg and (J < Gis[Ch].AmplGis[Q]) then J:= Gis[Ch].AmplGis[Q];
              if Flg and ((Q = 255) or (Gis[Ch].AmplGis[Q] < L)) then
              begin
                while K >=0 do
                begin
                  if Gis[Ch].AmplGis[K] < L / 2 then Break;
                  Dec(K);
                end;

                M:= Q;
                while M <= 255 do
                begin
                  if Gis[Ch].AmplGis[M] < L / 2 then Break;
                  Inc(M);
                end;

                C:= Length(Gis[Ch].DelayReg);

                if (C <> 0) and ((K - Gis[Ch].DelayReg[C - 1].EdDelay) < 3 ) then
                begin
                  Gis[Ch].DelayReg[C - 1].EdDelay:= M;
                  Flg:= False;
                end
                else
                begin
                  SetLength(Gis[Ch].DelayReg, C + 1);
                  Gis[Ch].DelayReg[C].StDelay:= K;
                  Gis[Ch].DelayReg[C].EdDelay:= M;
                  Gis[Ch].DelayReg[C].MaxValue:= J;
                  Flg:= False;
                end;
              end;
            end;
          end;
      end;
    end;
end;

// -----------------------------------------------------------------------------
// --- ����� ������� ������� - �� ������� ������� ------------------------------
// -----------------------------------------------------------------------------

function TRecData.HeadNoiseBlockOK(StartDisCoord: Integer): Boolean;
var
  R: RRail;
  I: Integer;
  Buff: TDestDat;
  Ch, Crd: Integer;

begin
  for Ch:= 2 to 5 do                   // ���������� ����������� ��� �������� �������� �������
  begin
    Buff:= FDatSrc.CurEcho[FRail, Ch];
    for I:= 1 to Buff.Count do
    begin
      Crd:= Round(FDatSrc.CurDisCoord + (1 - 2 * Ord(FDatSrc.BackMotion)) * (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * Buff.Delay[I]) / FScanStep);
      if (Crd >= FParam[2]) and (Crd <= FParam[3]) then
        Inc(FHZone[FRail, FParam[1]].Gis[Ch].AmplGis[Buff.Delay[I]], Sqr(16 - Buff.Ampl[I]));
    end;
  end;
end;

procedure TRecData.MakeHeadNoisePath;
var
  R: RRail;
  Ch: Integer;
  Flg: Boolean;
  I, J, K, L, M, C, Q: Integer;

begin
  for R:= rLeft to rRight do
    for I:= 0 to High(FHZone[R]) do            // ������������� ����
    begin
      for Ch:= 2 to 5 do
      begin
        FIdx[R, Ch]:= 0;
        FillChar(FHZone[R, I].Gis[Ch].AmplGis[0], 256 * 4, 0);  // �������������� �����������
      end;

      FHZone[R, I].Len:= 0;
      for J:= 0 to High(FHZone[R, I].Items) do  // ������������� �������� ����
      begin
        FRail:= R;
        FParam[1]:= I;
        FParam[2]:= FHZone[R, I].Items[J].StCrd;
        FParam[3]:= FHZone[R, I].Items[J].EdCrd;
                                                                      // ���������� ����������
        FDatSrc.LoadData(FHZone[R, I].Items[J].StCrd - FMaxShift, FHZone[R, I].Items[J].EdCrd + FMaxShift, 0, HeadNoiseBlockOK);
        Inc(FHZone[R, I].Len, FHZone[R, I].Items[J].EdCrd - FHZone[R, I].Items[J].StCrd);
      end;
                                         // ����������� ���������� - ������� �����

      with FHZone[R, I] do
      begin
        L:= 1000;
        for Ch:= 2 to 5 do
        begin
          Flg:= False;
          for Q:= 0 to 255 do
          begin
            if (not Flg) and (Gis[Ch].AmplGis[Q] >= L) then
            begin
              J:= Gis[Ch].AmplGis[Q];
              Flg:= True;
              K:= Q;
            end;
            if Flg and (J < Gis[Ch].AmplGis[Q]) then J:= Gis[Ch].AmplGis[Q];
            if Flg and ((Q = 255) or (Gis[Ch].AmplGis[Q] < L)) then
            begin

              while K >=0 do
              begin
                if Gis[Ch].AmplGis[K] < L / 2 then Break;
                Dec(K);
              end;

              M:= Q;
              while M <= 255 do
              begin
                if Gis[Ch].AmplGis[M] < L / 2 then Break;
                Inc(M);
              end;

              if (Length(Gis[Ch].DelayReg) <> 0) and
                 (Gis[Ch].DelayReg[High(Gis[Ch].DelayReg)].StDelay <= K) and
                 (Gis[Ch].DelayReg[High(Gis[Ch].DelayReg)].EdDelay >= K) then
              begin
                C:= High(Gis[Ch].DelayReg);
                Gis[Ch].DelayReg[C].StDelay:= Min(Gis[Ch].DelayReg[C].StDelay, K);
                Gis[Ch].DelayReg[C].EdDelay:= Max(Gis[Ch].DelayReg[C].EdDelay, M);
                Gis[Ch].DelayReg[C].MaxValue:= Max(J, Gis[Ch].DelayReg[C].MaxValue);
              end
              else
              begin
                C:= Length(Gis[Ch].DelayReg);
                SetLength(Gis[Ch].DelayReg, C + 1);
                Gis[Ch].DelayReg[C].StDelay:= K;
                Gis[Ch].DelayReg[C].EdDelay:= M;
                Gis[Ch].DelayReg[C].MaxValue:= J;
              end;
              Flg:= False;
            end;
          end;
        end;
      end;
    end;
end;
                       *)
// -----------------------------------------------------------------------------
// --- ����� ��� ���������� ���������� �������� --------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.SearchTechnoError;
type
  TTempItem = record
    Flg: Boolean;
    Range: TRange;
  end;

  TState = record
    Flg: Boolean;
    Error: Integer;
    Crd: Integer;
  end;

const
  IdxList: array [0..9] of Integer = ( 1, 2, 3, 3, 4, 4, 5, 5, 6, 6 );

var
  R: RRail_;
  I, J, K, Ch: Integer;
  L: Single;
  Flg, Flg1, Flg2: Boolean;
  Temp: array [rLeft..rRight] of array of TTempItem;
  State: array [rLeft..rRight, 0..9] of TState;
  Params: TMiasParams;
  MinVal: Integer;
  MaxVal: Integer;
  Speed: Single;
  sss: Single;

begin
 (*                            // ----------- ����� ���������� ���������� ��������� --------------------

  if RecConfig.DoublePass then
  begin
    for R:= rLeft to rRight do                     // ���������� ��� ��������� ������ ��� ��� ���������� ��������
      for I:= 0 to High(FRailObj[R]) do
        if Length(FRailObj[R, I].HeadZones) = 1 then // ���� ����� ��� ������� - ���������� ���������� ��������
        begin
          SetLength(Temp[R], Length(Temp[R]) + 1);
          Temp[R, High(Temp[R])].Flg:= True;
          Temp[R, High(Temp[R])].Range.StCrd:= FRailObj[R, I].HeadZones[0].StCrd;
          Temp[R, High(Temp[R])].Range.EdCrd:= FRailObj[R, I].HeadZones[0].EdCrd;
        end;

    for I:= 0 to High(Temp[rLeft]) do              // ��������� ����� � ������ ����
    begin
      K:= - 1;
      for J:= 0 to High(Temp[rRight]) do
        if Abs(GetCentr(Temp[rLeft, I].Range) - GetCentr(Temp[rRight, J].Range)) < 100 / FScanStep then
        begin
          K:= J;
          Temp[rRight, J].Flg:= False;
          Break;
        end;

      if K <> - 1 then                              // ���������
      begin
        SetLength(FTechnoError, Length(FTechnoError) + 1);
        FTechnoError[High(FTechnoError)].Typ:= teDoublePass;
        FTechnoError[High(FTechnoError)].R:= r_Both;
        FTechnoError[High(FTechnoError)].Range.StCrd:= Min(Temp[rLeft, I].Range.StCrd, Temp[rRight, K].Range.StCrd);
        FTechnoError[High(FTechnoError)].Range.EdCrd:= Max(Temp[rLeft, I].Range.EdCrd, Temp[rRight, K].Range.EdCrd);
      end
      else                                          // �� ���������
      begin
        SetLength(FTechnoError, Length(FTechnoError) + 1);
        FTechnoError[High(FTechnoError)].Typ:= teDoublePass;
        FTechnoError[High(FTechnoError)].R:= rLeft;
        FTechnoError[High(FTechnoError)].Range:= Temp[rLeft, I].Range;
      end;
    end;

    for I:= 0 to High(Temp[rRight]) do              // ��������� ������ ����� - ��� ��� �� ���������
      if Temp[rRight, I].Flg then
      begin
        SetLength(FTechnoError, Length(FTechnoError) + 1);
        FTechnoError[High(FTechnoError)].Typ:= teDoublePass;
        FTechnoError[High(FTechnoError)].R:= rRight;
        FTechnoError[High(FTechnoError)].Range:= Temp[rRight, I].Range;
      end;

    for R:= rLeft to rRight do
      SetLength(Temp[R], 0);
  end;

                            // ----------- ����� ���������� ������� ������ �� --------------------

  if RecConfig.BJButton then
  begin
    for R:= rLeft to rRight do
      for I:= 0 to High(FRailObj[R]) do
        if Length(FRailObj[R, I].HeadZones) <> 0 then // ����� �������
        begin
          Flg:= False;
          for J:= 0 to High(FRailObj[R, I].HeadZones) do  // ��� ������� �������
            for K:= 0 to High(FBJButton) do               // ������������� ���� ������� ������ ��
            begin
                                                          // ��������� �������� �� ������� ������ � ����
              Flg1:= (FBJButton[K].StCrd >= FRailObj[R, I].HeadZones[J].StCrd - 2200 / FScanStep) and
                     (FBJButton[K].StCrd <= GetCentr(FRailObj[R, I].HeadZones[J]) - 400 / FScanStep);
                                                          // ��������� �������� �� ���������� ������ � ����
              Flg2:= (FBJButton[K].EdCrd >= GetCentr(FRailObj[R, I].HeadZones[J]) + 400 / FScanStep) and
                     (FBJButton[K].EdCrd <= FRailObj[R, I].HeadZones[J].EdCrd + 2200 / FScanStep);

              if Flg1 or Flg2 then
              begin
                Flg:= True;
                Break;
              end;
            end;
          if not Flg then                                   // ��������� � ��������� ����� ��� ����������� �����������
          begin
            SetLength(Temp[R], Length(Temp[R]) + 1);
            Temp[R, High(Temp[R])].Flg:= True;
            Temp[R, High(Temp[R])].Range.StCrd:= FRailObj[R, I].HeadZones[0].StCrd;
            Temp[R, High(Temp[R])].Range.EdCrd:= FRailObj[R, I].HeadZones[0].EdCrd;
          end
        end;


    for I:= 0 to High(Temp[rLeft]) do              // ��������� ����� � ������ ����
    begin
      K:= - 1;
      for J:= 0 to High(Temp[rRight]) do
        if Abs(GetCentr(Temp[rLeft, I].Range) - GetCentr(Temp[rRight, J].Range)) < 100 / FScanStep then
        begin
          K:= J;
          Temp[rRight, J].Flg:= False;
          Break;
        end;

      if K <> - 1 then                              // ���������
      begin
        SetLength(FTechnoError, Length(FTechnoError) + 1);
        FTechnoError[High(FTechnoError)].Typ:= teBJButton;
        FTechnoError[High(FTechnoError)].R:= r_Both;
        FTechnoError[High(FTechnoError)].Range.StCrd:= Min(Temp[rLeft, I].Range.StCrd, Temp[rRight, K].Range.StCrd);
        FTechnoError[High(FTechnoError)].Range.EdCrd:= Max(Temp[rLeft, I].Range.EdCrd, Temp[rRight, K].Range.EdCrd);
      end
      else                                          // �� ���������
      begin
        SetLength(FTechnoError, Length(FTechnoError) + 1);
        FTechnoError[High(FTechnoError)].Typ:= teBJButton;
        FTechnoError[High(FTechnoError)].R:= rLeft;
        FTechnoError[High(FTechnoError)].Range:= Temp[rLeft, I].Range;
      end;
    end;

    for I:= 0 to High(Temp[rRight]) do              // ��������� ������ ����� - ��� ��� �� ���������
      if Temp[rRight, I].Flg then
      begin
        SetLength(FTechnoError, Length(FTechnoError) + 1);
        FTechnoError[High(FTechnoError)].Typ:= teBJButton;
        FTechnoError[High(FTechnoError)].R:= rRight;
        FTechnoError[High(FTechnoError)].Range:= Temp[rRight, I].Range;
      end;

    for R:= rLeft to rRight do
      SetLength(Temp[R], 0);
  end;
*)
  // ------------------- �������� ���������������� --------------------------------------------------


  if RecConfig.UseKuZones then
  begin

    MinVal:= MaxInt;
    MaxVal:= - MaxInt;

    for R:= rLeft to rRight do
      for Ch:= 0 to 9 do
        State[R, Ch].Flg:= False;

    FDatSrc.GetParamFirst(0, Params);
    for I:= - 1 to FDatSrc.EventCount - 1 do
    begin
      if I <> - 1 then FDatSrc.GetParamNext(FDatSrc.Event[I].DisCoord, Params);
      for R:= rLeft to rRight do
        for Ch:= 0 to 9 do
        begin
          Flg1:= Params.Par[R, Ch].Ku < RecConfig.KuZones[IdxList[Ch]].Min;
          Flg2:= Params.Par[R, Ch].Ku > RecConfig.KuZones[IdxList[Ch]].Max;

          if (not State[R, Ch].Flg) and (Flg1 or Flg2) then
          begin
            if I <> - 1 then State[R, Ch].Crd:= FDatSrc.Event[I].DisCoord
                        else State[R, Ch].Crd:= 0;
            State[R, Ch].Flg:= True;
            if Flg1 then State[R, Ch].Error:= - 1;
            if Flg2 then State[R, Ch].Error:= + 1;
          end;

          if State[R, Ch].Flg and (not Flg1) and (not Flg2) then
          begin
            SetLength(FTechnoError, Length(FTechnoError) + 1);
            FTechnoError[High(FTechnoError)].Typ:= teSens;
            FTechnoError[High(FTechnoError)].R:= R;
            FTechnoError[High(FTechnoError)].Ch:= Ch;
            FTechnoError[High(FTechnoError)].Range.StCrd:= State[R, Ch].Crd;
            FTechnoError[High(FTechnoError)].Range.EdCrd:= FDatSrc.Event[I].DisCoord;
            FTechnoError[High(FTechnoError)].Value:= State[R, Ch].Error;
            State[R, Ch].Flg:= False;
          end;
        end;
    end;

    for R:= rLeft to rRight do
      for Ch:= 0 to 9 do
        if State[R, Ch].Flg then
        begin
          SetLength(FTechnoError, Length(FTechnoError) + 1);
          FTechnoError[High(FTechnoError)].Typ:= teSens;
          FTechnoError[High(FTechnoError)].R:= R;
          FTechnoError[High(FTechnoError)].Ch:= Ch;
          FTechnoError[High(FTechnoError)].Range.StCrd:= State[R, Ch].Crd;
          FTechnoError[High(FTechnoError)].Range.EdCrd:= FDatSrc.MaxDisCoord;
          FTechnoError[High(FTechnoError)].Value:= State[R, Ch].Error;
          State[R, Ch].Flg:= False;
        end;
  end;

  // -------------------[ �������� ]---------------------------------------------------

  if RecConfig.UseMaxSpeed then
    with FDatSrc do
    begin
      Flg:= False;
      for I:= 0 to GetTimeListCount - 1 do
      begin
        K:= GetTimeList(I).DC;
        GetSpeed(K, Speed);

        if (not Flg) and (Speed > RecConfig.MaxSpeed) then
        begin
          Flg:= True;
          L:= Speed;
          J:= K;
        end;

        if Flg and (Speed > L) then L:= Speed;

        if Flg and (Speed < RecConfig.MaxSpeed) then
        begin
          Flg:= False;
          SetLength(FTechnoError, Length(FTechnoError) + 1);
          FTechnoError[High(FTechnoError)].Typ:= teSpeed;
          FTechnoError[High(FTechnoError)].R:= rNotSet;
          FTechnoError[High(FTechnoError)].Range.StCrd:= J;
          FTechnoError[High(FTechnoError)].Range.EdCrd:= GetTimeList(I).DC;
          FTechnoError[High(FTechnoError)].Value:= Round(L * 10);
        end;
      end;

      if Flg then
      begin
        Flg:= False;
        SetLength(FTechnoError, Length(FTechnoError) + 1);
        FTechnoError[High(FTechnoError)].Typ:= teSpeed;
        FTechnoError[High(FTechnoError)].R:= rNotSet;
        FTechnoError[High(FTechnoError)].Range.StCrd:= J;
        FTechnoError[High(FTechnoError)].Range.EdCrd:= FDatSrc.MaxDisCoord;
        FTechnoError[High(FTechnoError)].Value:= Round(L * 10);
      end;
    end;

end;

// -----------------------------------------------------------------------------
// --- ������������ �������������� ��������� -----------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.MakeConstruct;
var
  R: RRail_;
  I: Integer;
(*
  RR: RRail;
  I, II, J, K, L, Hz, Hi, Bz, Bi, Q: Integer;
  Idx1, Idx2: Integer;
  Flg, Flg1, Flg2: Boolean;
  Params: TMiasParams;
  MinVal: Integer;
  MaxVal: Integer;
  Speed: Single;
  Len: Integer;
  TypCount: TObjTypeCnt;
//  Temp: array of Integer;
//  Temp2: array of Integer;
//  IdList: array of TIdItem;
  ResTyp: TConstructTyp;
  *)        (*
function RecTyp(TypCount: TObjTypeCnt; var ResTyp: TConstructTyp; DebugCrd: Integer): Boolean;
var
  I: Integer;
  soT: TSingleObjTyp;

begin

//  Inc(GE);

  Result:= False;
  for I:= 0 to High(IdList) do
  begin
    Flg:= True;
    for soT:= so_BHOk to so_Blade do
      if soT <> so_End then
        Flg:= Flg and (TypCount[soT] >= IdList[I].Mask[soT].Min) and (TypCount[soT] <= IdList[I].Mask[soT].Max);
    if Flg then
    begin
      Result:= True;
      ResTyp:= IdList[I].ResTyp;
      Exit;
    end;
  end;

//  if not ((TypCount[so_BoltHole] = 0) and
//          (TypCount[so_End] = 0) and
//          (TypCount[so_BSHole] = 1) and
//          (TypCount[so_Blade]= 0)) then
{
  if not Result then
    ShowMessage(Format('�� ������������ ���������� ��: %d; ���: %d; ���: %d; ����: %d', [TypCount[so_BoltHole], TypCount[so_End], TypCount[so_BSHole], TypCount[so_Blade]]));
}
  {$IFDEF RECDEBUG}
//  RecDataForm.ListBox3.Items.AddObject(Format('��: %d; ���: %d; ���: %d; ����: %d', [TypCount[so_BoltHole], TypCount[so_End], TypCount[so_BSHole], TypCount[so_Blade]]), Pointer(DebugCrd));
  {$ENDIF}
end;
             *)
begin
            // ���� �� - ����� �� ������ ��

  R:= rLeft;
//  for I:= 0 to High(FBJSearch) do
  for I:= 0 to High(FBJView) do
//    for R:= rLeft to rRight do
    begin
      SetLength(FConstruct[R], Length(FConstruct[R]) + 1);
      FConstruct[R, High(FConstruct[R])].Head:= FBJView[I];
      FConstruct[R, High(FConstruct[R])].Body:= FBJView[I];
      FConstruct[R, High(FConstruct[R])].Typ:= ct_BoltJoint;
      FConstruct[R, High(FConstruct[R])].Del:= False;
    end;

  {$IFDEF RECDEBUG}
//  RecDataForm.ListBox3.Items.Clear;
  {$ENDIF}
      (*
                                                                      // �������� Body �������� �� ����� ���������� �������� � 1
  for R:= rLeft to rRight do
    for Hz:= 0 to High(FRailObj[R]) do
      for Hi:= 0 to High(FRailObj[R, Hz].HeadZones) do                // ����� �����
        for Bz:= 0 to High(FRailObj[R]) do
          for Bi:= 0 to High(FRailObj[R, Bz].BodyZones) do            // ����� Body �������
            if (FRailObj[R, Hz].HeadZones[Hi].StCrd >= FRailObj[R, Bz].BodyZones[Bi].Range.StCrd - 160 / FScanStep) and  // ����� [Hz, Hi] �������� � Body [Bz, Bi] ?
               (FRailObj[R, Hz].HeadZones[Hi].EdCrd <= FRailObj[R, Bz].BodyZones[Bi].Range.EdCrd + 160 / FScanStep) then
            begin
              if Length(FRailObj[R, Bz].BodyZones[Bi].ObjList) = 0 then Continue;
              Idx1:= 0;                                                                 //  ������� �������� �� ��������� ������� ������������� Body ����
              Idx2:= High(FRailObj[R, Bz].BodyZones[Bi].ObjList);

              SetLength(Temp, 0);                                                       // ���������� ������ ��������� �� ��������� ������� ������������ ����
              for II:= Idx1 to Idx2 do
              begin
                I:= FRailObj[R, Bz].BodyZones[Bi].ObjList[II];
                if FSingleObj[R, I].Typ  in [so_BHOk, so_BHUnCtrl, so_BHDef] then
                begin
                  SetLength(Temp, Length(Temp) + 1);
                  Temp[High(Temp)]:= I;
                end;
              end;

              SetLength(FConstruct[R], Length(FConstruct[R]) + 1);                      // ������� ����� �������� ����
              FConstruct[R, High(FConstruct[R])].Head:= FRailObj[R, Hz].HeadZones[Hi];  // ���� � ������� ������������� �� �����

              MinVal:= MaxInt;                                       // ����� ��������� ����������� �� ������ 1,2 ����� �� �����
              MaxVal:= - MaxInt;                                     // � ��������� ������� ����

          ([1]*
              FillChar(TypCount[so_BoltHole], SizeOf(TypCount), 0);          // ������� ����������
              for I:= 0 to High(Temp) do                                     // ������������� ������� �� ��������� ������� ������������� Body ����
                if Abs(GetCentr(FRailObj[R, Hz].HeadZones[Hi]) - GetCentr(FSingleObj[R, Temp[I]].DisRange)) <= 1200 / FScanStep then
                begin
                  FSingleObj[R, Temp[I]].Use:= True;
                  Inc(TypCount[FSingleObj[R, Temp[I]].Typ]);

                  {$IFDEF RECDEBUG}
                  with FConstruct[R, High(FConstruct[R])] do
                  begin
                    SetLength(Idx, Length(Idx) + 1);
                    Idx[High(Idx)]:= Temp[I];
                  end;
                  {$ENDIF}

                  MinVal:= Min(MinVal, FSingleObj[R, Temp[I]].DisRange.StCrd); // ��������� ������� ����
                  MaxVal:= Max(MaxVal, FSingleObj[R, Temp[I]].DisRange.EdCrd);
                end;


              if TypCount[so_BoltHole] + TypCount[so_BSHole] + TypCount[so_Blade] = 0 then
              begin
                SetLength(FConstruct[R], Length(FConstruct[R]) - 1);
              end
              else
              begin
                MinVal:= Min(MinVal, FRailObj[R, Hz].HeadZones[Hi].StCrd);
                MaxVal:= Max(MaxVal, FRailObj[R, Hz].HeadZones[Hi].EdCrd);

                FConstruct[R, High(FConstruct[R])].Body.StCrd:= MinVal;
                FConstruct[R, High(FConstruct[R])].Body.EdCrd:= MaxVal;
                FConstruct[R, High(FConstruct[R])].Typ:= ct_BoltJoint;
                FConstruct[R, High(FConstruct[R])].Del:= False;

                {$IFDEF RECDEBUG}
                FConstruct[R, High(FConstruct[R])].TypCnt:= TypCount;
                {$ENDIF}
              end;
          * [1])
            end;
     *)
  // ---------------------------------------------  �������� Body �������� �� ����� �2 --------------------------------
(*
  for R:= rLeft to rRight do
    for Hi:= 0 to High(FEnd3[R]) do
      if FEnd3[R, Hi].Ok then
        for Bz:= 0 to High(FRailObj[R]) do
          for Bi:= 0 to High(FRailObj[R, Bz].BodyZones) do            // ����� Body �������
            if (FEnd3[R, Hi].Crd >= FRailObj[R, Bz].BodyZones[Bi].Range.StCrd) and  // ����� [Hz, Hi] �������� � Body [Bz, Bi] ?
               (FEnd3[R, Hi].Crd <= FRailObj[R, Bz].BodyZones[Bi].Range.EdCrd) then
            begin
              if Length(FRailObj[R, Bz].BodyZones[Bi].ObjList) = 0 then Continue;
              Idx1:= 0;                                               //  ������� �������� �� ��������� ������� ������������� Body ����
              Idx2:= High(FRailObj[R, Bz].BodyZones[Bi].ObjList);

              SetLength(Temp, 0);                                                                      // ���������� ������ ���� ��������
              for II:= Idx1 to Idx2 do
              begin
                I:= FRailObj[R, Bz].BodyZones[Bi].ObjList[II];
                if (not FSingleObj[R, I].Use) and (FSingleObj[R, I].Typ = so_BoltHole) then
                begin
                  SetLength(Temp, Length(Temp) + 1);
                  Temp[High(Temp)]:= I;
                end;
              end;

              SetLength(FConstruct[R], Length(FConstruct[R]) + 1);
              FConstruct[R, High(FConstruct[R])].Head.StCrd:= FEnd3[R, Hi].Crd - Round(RecConfig.EndWidth / 2 / FScanStep);
              FConstruct[R, High(FConstruct[R])].Head.EdCrd:= FEnd3[R, Hi].Crd + Round(RecConfig.EndWidth / 2 / FScanStep);

              MinVal:= MaxInt;
              MaxVal:= - MaxInt;

              FillChar(TypCount[so_BoltHole], SizeOf(TypCount), 0);          // ������� ����������
              for I:= 0 to High(Temp) do                                                                //  ������������� �������� �� ��������� ������� ������������� Body ����
                if Abs(FEnd3[R, Hi].Crd - GetCentr(FSingleObj[R, Temp[I]].DisRange)) <= 1200 / FScanStep then
                begin
                  FSingleObj[R, Temp[I]].Use:= True;
                  Inc(TypCount[FSingleObj[R, Temp[I]].Typ]);

                  {$IFDEF RECDEBUG}
                  with FConstruct[R, High(FConstruct[R])] do
                  begin
                    SetLength(Idx, Length(Idx) + 1);
                    Idx[High(Idx)]:= Temp[I];
                  end;
                  {$ENDIF}

                  MinVal:= Min(MinVal, FSingleObj[R, Temp[I]].DisRange.StCrd);
                  MaxVal:= Max(MaxVal, FSingleObj[R, Temp[I]].DisRange.EdCrd);
                end;


              if TypCount[so_BoltHole] + TypCount[so_BSHole] + TypCount[so_Blade] = 0 then
              begin
                SetLength(FConstruct[R], Length(FConstruct[R]) - 1);
              end
              else
              begin
                MinVal:= Min(MinVal, FEnd3[R, Hi].Crd - Round(RecConfig.EndWidth / 2 / FScanStep));
                MaxVal:= Max(MaxVal, FEnd3[R, Hi].Crd + Round(RecConfig.EndWidth / 2 / FScanStep));

                FConstruct[R, High(FConstruct[R])].Body.StCrd:= MinVal;
                FConstruct[R, High(FConstruct[R])].Body.EdCrd:= MaxVal;
                FConstruct[R, High(FConstruct[R])].Typ:= ct_BoltJoint;

                {$IFDEF RECDEBUG}
                FConstruct[R, High(FConstruct[R])].TypCnt:= TypCount;
                {$ENDIF}
              end;
            end;
  *)
   (*
  // ------------------  �������� ���������� Body �������� ----------------------------------------

  SetLength(IdList, 6);                 // ������ ��� �������� ������� �� ���� �� �� ��������� ���������
  I:= 0;                                // ��������� ���������
  IdList[I].Mask[so_BHOk].Min:= 1;
  IdList[I].Mask[so_BHOk].Max:= 1;
  IdList[I].Mask[so_BSHole  ].Min:= 0;
  IdList[I].Mask[so_BSHole  ].Max:= 0;
  IdList[I].Mask[so_Blade   ].Min:= 0;
  IdList[I].Mask[so_Blade   ].Max:= 0;
  IdList[I].ResTyp:= ct_OneHole;

  I:= 1;                                // ��������
  IdList[I].Mask[so_BHOk].Min:= 2;
  IdList[I].Mask[so_BHOk].Max:= 2;
  IdList[I].Mask[so_BSHole  ].Min:= 0;
  IdList[I].Mask[so_BSHole  ].Max:= 0;
  IdList[I].Mask[so_Blade   ].Min:= 0;
  IdList[I].Mask[so_Blade   ].Max:= 0;
  IdList[I].ResTyp:= ct_Plug;

  I:= 2;                                // ������� ����
  IdList[I].Mask[so_BHOk].Min:= 3;
  IdList[I].Mask[so_BHOk].Max:= 13;
  IdList[I].Mask[so_BSHole  ].Min:= 0;
  IdList[I].Mask[so_BSHole  ].Max:= 0;
  IdList[I].Mask[so_Blade   ].Min:= 0;
  IdList[I].Mask[so_Blade   ].Max:= 0;
  IdList[I].ResTyp:= ct_WeldedJoint;
//  IdList[I].ResTyp:= ct_BoltJoint;

  I:= 3;                               // ������ ����������� ��������
  IdList[I].Mask[so_BHOk].Min:= 0;
  IdList[I].Mask[so_BHOk].Max:= 22;
  IdList[I].Mask[so_BSHole  ].Min:= 0;
  IdList[I].Mask[so_BSHole  ].Max:= 18;
  IdList[I].Mask[so_Blade   ].Min:= 2;
  IdList[I].Mask[so_Blade   ].Max:= 99;
  IdList[I].ResTyp:= ct_Blade;

  I:= 4;                               // ����������
  IdList[I].Mask[so_BHOk].Min:= 2;
  IdList[I].Mask[so_BHOk].Max:= 35;
  IdList[I].Mask[so_BSHole  ].Min:= 2;
  IdList[I].Mask[so_BSHole  ].Max:= 22;
  IdList[I].Mask[so_Blade   ].Min:= 0;
  IdList[I].Mask[so_Blade   ].Max:= 0;
  IdList[I].ResTyp:= ct_Frog;

  I:= 5;                               // ����� �����
  IdList[I].Mask[so_BHOk].Min:= 14;
  IdList[I].Mask[so_BHOk].Max:= 35;
  IdList[I].Mask[so_BSHole  ].Min:= 0;
  IdList[I].Mask[so_BSHole  ].Max:= 0;
  IdList[I].Mask[so_Blade   ].Min:= 0;
  IdList[I].Mask[so_Blade   ].Max:= 0;
  IdList[I].ResTyp:= ct_FlangeRail;

  for R:= rLeft to rRight do
    for Bz:= 0 to High(FRailObj[R]) do
      for Bi:= 0 to High(FRailObj[R, Bz].BodyZones) do            // ����� Body �������
        if Length(FRailObj[R, Bz].BodyZones[Bi].ObjList) <> 0 then
        begin
                                                                                                     // ������������� �� ��������������� ��� �� �������
          SetLength(Temp, 0);                                                                        // ���������� ������ ���� ��������

          for I:= 0 to High(FRailObj[R, Bz].BodyZones[Bi].ObjList) do
            if (not FSingleObj[R, FRailObj[R, Bz].BodyZones[Bi].ObjList[I]].Use) and
               (FSingleObj[R, FRailObj[R, Bz].BodyZones[Bi].ObjList[I]].Typ <> so_End) then
            begin
              SetLength(Temp, Length(Temp) + 1);
              Temp[High(Temp)]:= FRailObj[R, Bz].BodyZones[Bi].ObjList[I];
            end;

          if Length(Temp) = 0 then Continue;

          if Length(Temp) = 1 then
          begin
            FillChar(TypCount[so_BHOk], SizeOf(TypCount), 0);          // ������� ����������
            Inc(TypCount[FSingleObj[R, Temp[0]].Typ]);
            if not RecTyp(TypCount, ResTyp, GetCentr(FSingleObj[R, Temp[0]].DisRange)) then
            begin
              FillChar(TypCount[so_BHOk], SizeOf(TypCount), 0);          // ������� ����������
              Continue;
            end;
            SetLength(FConstruct[R], Length(FConstruct[R]) + 1);
            FConstruct[R, High(FConstruct[R])].Typ:= ResTyp;
            FConstruct[R, High(FConstruct[R])].Body.StCrd:= FSingleObj[R, Temp[0]].DisRange.StCrd;
            FConstruct[R, High(FConstruct[R])].Body.EdCrd:= FSingleObj[R, Temp[0]].DisRange.EdCrd;
            FConstruct[R, High(FConstruct[R])].Del:= False;
            {$IFDEF RECDEBUG}
            FConstruct[R, High(FConstruct[R])].TypCnt:= TypCount;
            SetLength(FConstruct[R, High(FConstruct[R])].Idx, 1);        // ��������� ������� ���������� �� ������� ��� �������� ��
            FConstruct[R, High(FConstruct[R])].Idx[0]:= Temp[0];
            {$ENDIF}
          end
          else
          begin
            K:= 0;
            FillChar(TypCount[so_BHOk], SizeOf(TypCount), 0);          // ������� ����������
            for I:= 0 to High(Temp) - 1 do
            begin
              Inc(TypCount[FSingleObj[R, Temp[I]].Typ]);

              {$IFDEF RECDEBUG}
              SetLength(Temp2, Length(Temp2) + 1);                        // ��������� ������� ��������� �� ������� ��� �������� ��
              Temp2[High(Temp2)]:= Temp[I];
              {$ENDIF}

              Flg1:= True;                                       // ������ ������� ������ ����� (720) �� ��� ������ ���������
              for J:= 0 {I + 1} to High(Temp) do
                if J <> I then
                if (Abs(GetCentr(FSingleObj[R, Temp[J]].DisRange) -
                        GetCentr(FSingleObj[R, Temp[I]].DisRange)) <= 920 / FScanStep) then
                begin
                  Flg1:= False;
                  Break;
                end;

              if Flg1 then       // ��������� ����� �������������� ������� - ���� ��������� ������ ������������� ������� Flg1 = True
              begin
                if not RecTyp(TypCount, ResTyp, GetCentr(FSingleObj[R, Temp[K]].DisRange)) then
                begin
                  {$IFDEF RECDEBUG}
                  SetLength(Temp2, 0);
                  {$ENDIF}
                  FillChar(TypCount[so_BHOk], SizeOf(TypCount), 0);          // ������� ����������
                  Continue;
                end;

                SetLength(FConstruct[R], Length(FConstruct[R]) + 1);
                FConstruct[R, High(FConstruct[R])].Typ:= ResTyp;
                FConstruct[R, High(FConstruct[R])].Body.StCrd:= FSingleObj[R, Temp[K]].DisRange.StCrd;
                FConstruct[R, High(FConstruct[R])].Body.EdCrd:= FSingleObj[R, Temp[I]].DisRange.EdCrd;
                FConstruct[R, High(FConstruct[R])].Del:= False;
                {$IFDEF RECDEBUG}
                FConstruct[R, High(FConstruct[R])].TypCnt:= TypCount;
                SetLength(FConstruct[R, High(FConstruct[R])].Idx, Length(Temp2));      // ��������� ������� ���������� �� ������� ��� �������� ��
                Move(Temp2[0], FConstruct[R, High(FConstruct[R])].Idx[0], Length(Temp2) * SizeOf(Integer));
                SetLength(Temp2, 0);
                {$ENDIF}

                FillChar(TypCount[so_BHOk], SizeOf(TypCount), 0);          // ������� ����������
                K:= I + 1;
              end;
              if (not Flg1) and (I = High(Temp) - 1) then // ��������� ������� ������ � �����������
              begin
                {$IFDEF RECDEBUG}
                SetLength(Temp2, Length(Temp2) + 1);                        // ��������� ������� ���������� �� ������� ��� �������� ��
                Temp2[High(Temp2)]:= Temp[I + 1];
                {$ENDIF}
                Inc(TypCount[FSingleObj[R, Temp[I + 1]].Typ]);
                if not RecTyp(TypCount, ResTyp, GetCentr(FSingleObj[R, Temp[K]].DisRange)) then
                begin
                  SetLength(Temp2, 0);
                  FillChar(TypCount[so_BHOk], SizeOf(TypCount), 0);          // ������� ����������
                  Continue;
                end;
                SetLength(FConstruct[R], Length(FConstruct[R]) + 1);
                FConstruct[R, High(FConstruct[R])].Typ:= ResTyp;
                FConstruct[R, High(FConstruct[R])].Body.StCrd:= FSingleObj[R, Temp[K]].DisRange.StCrd;
                FConstruct[R, High(FConstruct[R])].Body.EdCrd:= FSingleObj[R, Temp[I + 1]].DisRange.EdCrd;
                FConstruct[R, High(FConstruct[R])].Del:= False;
                {$IFDEF RECDEBUG}
                FConstruct[R, High(FConstruct[R])].TypCnt:= TypCount;
                SetLength(FConstruct[R, High(FConstruct[R])].Idx, Length(Temp2));
                Move(Temp2[0], FConstruct[R, High(FConstruct[R])].Idx[0], Length(Temp2) * SizeOf(Integer));
                SetLength(Temp2, 0);
                {$ENDIF}
                FillChar(TypCount[so_BHOk], SizeOf(TypCount), 0);          // ������� ����������
                K:= I + 1;
              end;
            end;
            if Flg1 then  // ���������� ! - ��������� ��������� ������� - ��������
            begin
              K:= High(Temp);
              FillChar(TypCount[so_BHOk], SizeOf(TypCount), 0);          // ������� ����������
              Inc(TypCount[FSingleObj[R, Temp[I]].Typ]);
              if RecTyp(TypCount, ResTyp, GetCentr(FSingleObj[R, Temp[K]].DisRange)) then
              begin
                SetLength(FConstruct[R], Length(FConstruct[R]) + 1);
                FConstruct[R, High(FConstruct[R])].Typ:= ResTyp;
                FConstruct[R, High(FConstruct[R])].Body.StCrd:= FSingleObj[R, Temp[K]].DisRange.StCrd;
                FConstruct[R, High(FConstruct[R])].Body.EdCrd:= FSingleObj[R, Temp[K]].DisRange.EdCrd;
                FConstruct[R, High(FConstruct[R])].Del:= False;
                {$IFDEF RECDEBUG}
                FConstruct[R, High(FConstruct[R])].TypCnt:= TypCount;
                SetLength(FConstruct[R, High(FConstruct[R])].Idx, 1);        // ��������� ������� ���������� �� ������� ��� �������� ��
                FConstruct[R, High(FConstruct[R])].Idx[0]:= Temp[K];
                {$ENDIF}
              end;
            end;
          end;
        end;

  SetLength(IdList, 0);

  // ------------------  ������������� �������� ������� �������� ������� ----------------------

  for R:= rLeft to rRight do
  begin
    case R of
       rLeft: RR:= rRight;
      rRight: RR:= rLeft;
    end;

    for I:= 0 to High(FConstruct[R]) do              // �������� ������� - ������ �����
      if FConstruct[R, I].Typ = ct_Blade then
        for II:= 0 to High(FConstruct[RR]) do
        begin
          if (FConstruct[RR, II].Typ in [ct_OneHole, ct_Plug, ct_WeldedJoint]) and
             ((FConstruct[RR, II].Body.StCrd >= FConstruct[R, I].Body.StCrd) or
              (FConstruct[R, I].Body.StCrd - FConstruct[RR, II].Body.StCrd < 650 / FScanStep)) and
             ((FConstruct[RR, II].Body.EdCrd <= FConstruct[R, I].Body.EdCrd) or
              (FConstruct[RR, II].Body.EdCrd - FConstruct[R, I].Body.EdCrd < 650 / FScanStep)) then FConstruct[RR, II].Typ:= ct_SupportingRail;
         end;

    for I:= 0 to High(FConstruct[R]) do              // �������� ���������� - ����� �����
      if FConstruct[R, I].Typ = ct_Frog then
        for II:= 0 to High(FConstruct[RR]) do
        begin
          if (FConstruct[RR, II].Typ in [ct_OneHole, ct_Plug, ct_WeldedJoint]) and
             ((FConstruct[RR, II].Body.StCrd >= FConstruct[R, I].Body.StCrd) or
              (FConstruct[R, I].Body.StCrd - FConstruct[RR, II].Body.StCrd < 650 / FScanStep)) and
             ((FConstruct[RR, II].Body.EdCrd <= FConstruct[R, I].Body.EdCrd) or
              (FConstruct[RR, II].Body.EdCrd - FConstruct[R, I].Body.EdCrd < 650 / FScanStep)) then FConstruct[RR, II].Typ:= ct_FlangeRail;;
         end;

  end;

  *)
end;

// -----------------------------------------------------------------------------
// --- ������� ����� ������������ �� ���� ������ -------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.MarkPB;
var
  R: RRail_;
  I, J, K, Ch: Integer;
  Flg1: Boolean;
  Flg2: Boolean;

begin
  for R:= rLeft to rRight do // ��������� ����� �� ��������� � ���� ������� ������ ��
    for Ch:= 0 to 7 do
      for I:= 0 to High(FPBList[R, Ch]) do
//      if FPBList[R, Ch, I].Typ = pbtNotSet then
      begin
{        if (R = rLeft) and
           (Ch = 1) and
           (I = 28) then ShowMessage('!');
 }
        Flg1:= True;
//        for J:= 0 to High(FBJSearch) do                            // �������� �� ��������� � ���� ��
        for J:= 0 to High(FBJView) do                            // �������� �� ��������� � ���� ��
//          if PtInRange(FPBList[R, Ch, I].CCrd, FBJSearch[J]) then
          if PtInRange(FPBList[R, Ch, I].CCrd, FBJView[J]) then
          begin
            Flg1:= False;
            Break;
          end;

        if not Flg1 then
        begin
//          SetPBTyp(FPBList[R, Ch, I], 9903, pbtConstruct);
          FPBList[R, Ch, I].Typ:= FPBList[R, Ch, I].Typ + [pbtiConstruct];
          Continue;
        end;
        for J:= 0 to High(FBMList.FBMData) do                     // �������� �� ��������� � ���
          if PtInRange(FPBList[R, Ch, I].CCrd, FBMList.FBMData[J].DisRange) then
          begin
            Flg2:= False;
            for K:= 0 to High(FBMList.FBMData[J].LastPass) do
              if PtInRange(FPBList[R, Ch, I].CCrd, FBMList.FBMData[J].LastPass[K]) then
              begin
                Flg2:= True;
                Break;
              end;
            if not Flg2 then
            begin
              Flg1:= False;
              Break;
            end;
          end;

        if Flg1 then // SetPBTyp(FPBList[R, Ch, I], 9902, pbtClear)
          FPBList[R, Ch, I].Typ:= FPBList[R, Ch, I].Typ + [pbtiClear] else
          FPBList[R, Ch, I].Typ:= FPBList[R, Ch, I].Typ + [pbtiNotLast];
      end;

{
        for J:= 0 to High(FBJSearch) do                            // �������� �� ��������� � ���� ��
          if PtInRange(FPBList[R, Ch, I].CCrd, FBJSearch[J]) then
          begin
            Flg:= False;
            Break;
          end;


        if Length(FBJButton[R]) <> 0 then
          while FPBList[R, Ch, I].CCrd > FBJButton[BtnIdx].EdCrd do
            if BtnIdx < High(FBJButton) then Inc(BtnIdx) else Break;

        if Length(FBlades[R]) <> 0 then
          while FPBList[R, Ch, I].CCrd > FBlades[R, BldIdx].EdCrd do
            if BldIdx < High(FBlades[R]) then Inc(BldIdx) else Break;

        if (not PtInRange(FPBList[R, Ch, I].CCrd, FBJButton[BtnIdx])) and
           (not PtInRange(FPBList[R, Ch, I].CCrd, FBlades[R, BldIdx])) then


          FPBList[R, Ch, I].Typ:= pbtForDef;


      end;
 }
{    begin
                  // ������� ������ ��
                  // ���
                  // ������
      BtnIdx:= 0;
      BldIdx:= 0;
      for I:= 0 to High(FPBList[R, Ch]) do
      begin
        if Length(FBJButton[R]) <> 0 then
          while FPBList[R, Ch, I].CCrd > FBJButton[BtnIdx].EdCrd do
            if BtnIdx < High(FBJButton) then Inc(BtnIdx) else Break;
        if Length(FBlades[R]) <> 0 then
          while FPBList[R, Ch, I].CCrd > FBlades[R, BldIdx].EdCrd do
            if BldIdx < High(FBlades[R]) then Inc(BldIdx) else Break;
        if (not PtInRange(FPBList[R, Ch, I].CCrd, FBJButton[BtnIdx])) and
           (not PtInRange(FPBList[R, Ch, I].CCrd, FBlades[R, BldIdx])) then
          FPBList[R, Ch, I].Typ:= pbtForDef;
      end;
    end; }
end;

// -----------------------------------------------------------------------------
// --- ���������� ����� � �������� ������ ��� ������ �������� ------------------
// -----------------------------------------------------------------------------

procedure TRecData.MakeDefectPBList;
var
  R: RRail_;
  I, J, K, D, Ch, Crd_: Integer;
  Cap: Integer;
  Idx: Integer;
  Crd1: Integer;
  Crd2: Integer;
  Tmp: TBPListItem;
  Tmp2: TBtmPt;
  Angle: Single;
  Flg1, Flg2: Boolean;

begin

//  ----------------------------  ���������� ����� -------------------------------------
 (*
  for R:= rLeft to rRight do
    for Ch:= 0 to 7 do
      for I:= 0 to High(FPBList[R, Ch]) do
        if (FPBList[R, Ch, I].Typ in [pbtNotSet, pbtForHole]) and
           (GetLen(FPBList[R, Ch, I].Crd) >= 3) then
        begin
//           if (R = rLeft) and (Ch = 2) and (I = 559) then
//           showmessage('!');

           Angle:= (FPBList[R, Ch, I].Del.EdCrd - FPBList[R, Ch, I].Del.StCrd + 1)  / (FPBList[R, Ch, I].Crd.EdCrd - FPBList[R, Ch, I].Crd.StCrd + 1);

       //  if ((Ch = 6) and (Angle > - 1.3) and (Angle < - 0.3) and (Math.Min(FPBList[R, Ch, I].Del.StCrd, FPBList[R, Ch, I].Del.EdCrd) < GetBtmDelay(R, GetCentr(FPBList[R, Ch, I].Crd)))) or
       //     ((Ch = 7) and (Angle <   1.3) and (Angle >   0.3) and (Math.Min(FPBList[R, Ch, I].Del.StCrd, FPBList[R, Ch, I].Del.EdCrd) < GetBtmDelay(R, GetCentr(FPBList[R, Ch, I].Crd)))) or

//           if ((Ch = 6) and (Angle > - 1.3) and (Angle < - 0.3) and (Math.Min(FPBList[R, Ch, I].Del.StCrd, FPBList[R, Ch, I].Del.EdCrd) < 165)) or
//              ((Ch = 7) and (Angle <   1.3) and (Angle >   0.3) and (Math.Min(FPBList[R, Ch, I].Del.StCrd, FPBList[R, Ch, I].Del.EdCrd) < 165)) or

           if ((Ch = 6) and (Angle > - 1.3) and (Angle < - 0.3) and (FPBList[R, Ch, I].Del.EdCrd <= 165)) or
              ((Ch = 7) and (Angle <   1.3) and (Angle >   0.3) and (FPBList[R, Ch, I].Del.EdCrd <= 165)) or
              ((Ch in [2, 4]) and (Angle > - 1.92) and (Angle < - 0.04)) or
              ((Ch in [3, 5]) and (Angle <   1.92) and (Angle >   0.04)) or
               (Ch = 1) or
               ((Ch = 0) and (FPBList[R, 0, I].Dat[3] <> 1)) then
           begin
             Flg1:= True;
             if not RecConfig.SearchInBJ then
               for J:= 0 to High(FConstruct[R]) do
                 if (FConstruct[R, J].Typ in [ct_WeldedJoint, ct_BoltJoint]) and
                    ((Ch in [2, 3, 4, 5]) and (MyTypes.PtInRange( GetRedCentr( Ch, FPBList[R, Ch, I] ), FConstruct[R, J].Head)) or
                     (Ch in [0, 1, 6, 7]) and (MyTypes.PtInRange( GetRedCentr( Ch, FPBList[R, Ch, I] ), FConstruct[R, J].Body))) then
                 begin
                   Flg1:= False;
                   Break;
                 end;


             Flg2:= True;
             if not RecConfig.SearchInSw then
               for J:= 0 to High(FConstruct[R]) do
                 if (FConstruct[R, J].Typ in [ct_FlangeRail, ct_Blade, ct_Frog, ct_SupportingRail]) and
                     MyTypes.InRange(FPBList[R, Ch, I].Crd, FConstruct[R, J].Body) then
                 begin
                   Flg2:= False;
                   Break;
                 end;

             if Flg1 and Flg2 then SetPBTyp(FPBList[R, Ch, I], 88, pbtForDef);
           end;
        end;
*)
                                   // �������� ������ ����� ��� ������ ��������

  for R:= rLeft to rRight do
  begin
    Cap:= 1000;
    Idx:= 0;
    SetLength(FDefPBList[R], Cap);
    for Ch:= 0 to 7 do
    begin
 //     J:= 0;
      for I:= 0 to High(FPBList[R, Ch]) do
        if (pbtiClear in FPBList[R, Ch, I].Typ) and
//           (not (pbtiUCS in FPBList[R, Ch, I].Typ)) and
           (not (pbtiInBMZ in FPBList[R, Ch, I].Typ)) and
           (not (pbtiNotLast in FPBList[R, Ch, I].Typ)) and
           (not (pbtiReCheck in FPBList[R, Ch, I].Typ)) and
           (not (pbtiDel in FPBList[R, Ch, I].Typ)) then

      begin
     {   if J <> - 1 then
        begin
          while FPBList[R, Ch, I].CCrd > FBJButton[J].EdCrd do
            if J < High(FBJButton)
              then Inc(J)
              else begin
                     J:= - 1;
                     Break;
                   end;
          if (J <> - 1) and PtInRange(FPBList[R, Ch, I].CCrd, FBJButton[J]) then Continue;
        end;         }
        if Idx = Cap then
        begin
          Cap:= Cap + 1000;
          SetLength(FDefPBList[R], Cap);
        end;
//        FDefPBList[R, Idx].
        FDefPBList[R, Idx].Ch:= Ch;
        FDefPBList[R, Idx].Idx:= I;
        Inc(Idx);
      end;
    end;
    SetLength(FDefPBList[R], Idx);
  end;

                                 // ����������
  for R:= rLeft to rRight do
    for I:= 0 to High(FDefPBList[R]) - 1 do
    begin
      K:= I;
      for J:= I + 1 to High(FDefPBList[R]) do
      begin
        with FPBList[R, FDefPBList[R, K].Ch, FDefPBList[R, K].Idx] do
          Crd1:= Round(CCrd + (ChList[FDefPBList[R, K].Ch].Shift[2] + ChList[FDefPBList[R, K].Ch].RedPar[2] * CDel) / FScanStep);

        with FPBList[R, FDefPBList[R, J].Ch, FDefPBList[R, J].Idx] do
          Crd2:= Round(CCrd + (ChList[FDefPBList[R, J].Ch].Shift[2] + ChList[FDefPBList[R, J].Ch].RedPar[2] * CDel) / FScanStep);

       if Crd1 > Crd2 then K:= J;
      end;
      if K <> I then
      begin
        Tmp:= FDefPBList[R, K];
        FDefPBList[R, K]:= FDefPBList[R, I];
        FDefPBList[R, I]:= Tmp;
      end;
    end;
end;

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
{
function TRecData.GetBtmDelay(R: RRail; Crd: Integer): Integer;
var
  I: Integer;

begin
  if Length(FBtmPtList[R]) = 0 then
  begin
    Result:= 255;
    Exit;
  end;
  for I:= 0 to High(FBtmPtList[R]) - 1 do
    if (Crd >= FBtmPtList[R, I].Crd) and (Crd <= FBtmPtList[R, I + 1].Crd) then
    begin
      Result:= Round((FBtmPtList[R, I].Delay + FBtmPtList[R, I + 1].Delay) / 2);
      Break;
    end;
end;
}
// -----------------------------------------------------------------------------
// --- ����� �������� ----------------------------------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.SearchDefect;       // --- ����� �������� -------------------------------------------------
type
  TDelayRange = record
    Min: Integer;
    Max: Integer;
  end;

  TItemPosEn = (pHead, pBody, pBottom);

  TItemPos = set of TItemPosEn;

  TItem = record
    Ch: Integer;
    Delay: Integer;
    Len: Integer;
    Pos: TItemPos;
  end;

var
  R: RRail_;
  I: Integer;
  J: Integer;
  M: Integer;
  Ch: Integer;
  ChCount: array [0..7] of Integer;
  Crd1: Integer;
  Crd2: Integer;
  Crd3: Integer;
  Crd4: Integer;
  Flg: Boolean;
  PBIdx: array of Integer;
  Res: array [TItemPosEn, 0..7] of TDelayRange; // 1 - ������� 2 - ����� 3 - �������
  ResInByCh: array [1..3, 0..7] of Integer;
  ResOutByCh: array [1..3, 0..7] of Integer;
  ResIn: array [1..3] of Integer;
  ResOut: array [1..3] of Integer;
  F: array [1..3] of Boolean;
  DefTyp: TDefTyp;
  XRange: TRange;
  Ch_: Integer;
  PBItem: TPBItem;
  S: string;
  SS: string;
  Idx1, Idx2: Integer;

{$IFDEF RECDEBUG}
function PosToStr(Val: TItemPosEn): string;
begin
  case Val of
    pHead: Result:= '���';
    pBody: Result:= '���';
  pBottom: Result:= '���';
  end;
end;

function PosSetToStr(Val: TItemPos): string;
var
  I: TItemPosEn;

begin
  Result:= '';
  for I:= pHead to pBottom do
    if I in Val then Result:= Result + PosToStr(I) + ' ';
end;
{$ENDIF}

procedure GetRes;
var
  J, K, L, Ch_: Integer;
  Typ: TItemPosEn;
  SummMaxLen, SummLen: Integer;
  Items: array of TItem;
  Summ: array [pHead..pBottom] of Integer;
  SummL: array [pHead..pBottom] of Single; // ��������� ����� ����� �� �����
  Count: array [1..3] of Integer;
  AngleFlg: Boolean;
//  New: TDefListItem;
  ChLen: array [0..7] of Integer;
  ChCount: Integer;
  OnlyCh0: Boolean;
  GroupOk: Boolean;
  MaxIdx: Integer;
  NewRgn: TRange;
  NewPBI: TDefPBListItem;
  Flg1, Flg2: Boolean;

begin
  for Ch_:= 0 to 7 do ChLen[Ch_]:= 0;                               // ���������� ��� ������� ������������ ������ ����� �� �������
                                                                    // �������� ������ ����������� �����
  SetLength(Items, Length(PBIdx));
  for J:= 0 to High(PBIdx) do                                       // �������� � ����� ����� ��������� �����
  begin
    PBItem:= FPBList[R, FDefPBList[R, PBIdx[J]].Ch, FDefPBList[R, PBIdx[J]].Idx];
    Items[J].Pos:= [];
    for Typ:= pHead to pBottom do
    begin
      if (PBItem.CDel >= Res[Typ, FDefPBList[R, PBIdx[J]].Ch].Min) and
         (PBItem.CDel <= Res[Typ, FDefPBList[R, PBIdx[J]].Ch].Max) then Items[J].Pos:= Items[J].Pos + [Typ];
    end;
    Items[J].Ch:= FDefPBList[R, PBIdx[J]].Ch;
    Items[J].Delay:= PBItem.CDel;
    Items[J].Len:= GetLen(PBItem.Crd);
    ChLen[FDefPBList[R, PBIdx[J]].Ch]:= Max(ChLen[FDefPBList[R, PBIdx[J]].Ch], Items[J].Len); // ������������ ������ ����� �� �������
  end;

  ChCount:= 0;                                                      // ��������� ����������� �������
  SummMaxLen:= 0;                                                   // ��������� �����
  for Ch_:= 0 to 7 do if ChLen[Ch_] <> 0 then                       // ����������� ...
  begin
    Inc(ChCount);                                                   // ���������� ����������� �������
    Inc(SummMaxLen, ChLen[Ch_]);                                    // � ��������� ������
  end;

                                       // ����������� ������� ������� 1 ������ � ����������� ���������
  L:= - 1;
  for J:= 0 to High(Items) do
    if Items[J].Ch = 1 then
    begin
      if L = - 1 then L:= J
                 else if Items[J].Delay < Items[L].Delay then L:= J;
    end;

  OnlyCh0:= True;                // ������� ��� ����� 0 ������
  SummLen:= 0;                   // ��������� �����
  for Typ:= pHead to pBottom do
  begin
    SummL[Typ]:= 0;
    Summ[Typ]:= 0;
  end;

  S:= '';
  Count[1]:= 0;
  Count[2]:= 0;
  Count[3]:= 0;
  for J:= 0 to High(Items) do
  begin
    OnlyCh0:= OnlyCh0 and (Items[J].Ch = 0);  // ������ 0-� �����
    if ((Items[J].Ch = 1) and (L = J)) // ��� 1 ������ ����� ������ 1 ����� � ����������� ����������
        or (Items[J].Ch <> 1) then
    begin
      for Typ:= pHead to pBottom do
        if Typ in Items[J].Pos then
        begin
          Inc(Summ[Typ]);
          SummL[Typ]:= SummL[Typ] + Items[J].Len;
        end;
    end;
    SummLen:= SummLen + Items[J].Len;
{
    if (Items[J].Ch in [2, 3, 4, 5, 6, 7]) then
    begin
      if Abs(GetPBAngleKF(FPBList[R, FDefPBList[R, PBIdx[J]].Ch, FDefPBList[R, PBIdx[J]].Idx])) < 0.05 then Inc(Count[3]); // ����� �������������� ?
      Inc(Count[2]);
    end
    else Inc(Count[1]);
}  end;
{
  AngleFlg:= (Count[2] = 0) or    // ��� ����� � ��������� �������
             ((Count[2] <> 0) and // ���� ����� � ��������� �������
             ((Count[2] - Count[3]) <> 0)); // �� ��� ����� ��������������
}
  {$IFDEF RECDEBUG}
  S:= Chr(13) + Chr(13) + '�����: ' + Chr(13);
  for J:= 0 to High(Items) do
  begin
    if ((Items[J].Ch = 1) and (L = J)) or (Items[J].Ch <> 1) then SS:= '+' else SS:= '-';
    S:= S + Format('%d - [' + SS + '] - Ch: %d; D: %d; Len: %3.1f ��; Pos: %s' + Chr(13), [J, Items[J].Ch, Items[J].Delay, Items[J].Len * FScanStep, PosSetToStr(Items[J].Pos)]);
  end;
  S:= S + Format(Chr(13) + '��������:' + Chr(13) + '� �������: %d' + Chr(13) + '� �����: %d' + Chr(13) + '� �������: %d' + Chr(13) + Chr(13), [Summ[pHead], Summ[pBody], Summ[pBottom]]);
  {$ENDIF}
  SetLength(Items, 0);

  GroupOk:= ((SummMaxLen >= Floor(RecConfig.LenTh[ChCount] / FScanStep)) and (not OnlyCh0)) or
            ((SummMaxLen >= Floor(RecConfig.Ch0OnePBLen / FScanStep)) and OnlyCh0);

  if GroupOk then
//  if AngleFlg and
//     ((OnlyCh0 and (SummLen >= RecConfig.Ch0PBLen / FScanStep)) or                                         // ������ ������
//      ((not OnlyCh0) and (Length(PBIdx) = 1) and (SummLen >= RecConfig.OnePBLen / FScanStep)) or           // ������ 1 ����� � ��� �� ������
//      ((not OnlyCh0) and (Length(PBIdx) > 1) and (SummLen >= RecConfig.SomePBLen / FScanStep))) then        // ��������� ����� � �� ������ ������
  begin
                                   // ���������� ������������ ���������� � ������� ������
    if ((Summ[pHead] > Summ[pBody]) or ((Summ[pHead] = Summ[pBody]) and (SummL[pHead] > SummL[pBody]))) and
       ((Summ[pHead] > Summ[pBottom]) or ((Summ[pHead] = Summ[pBottom]) and (SummL[pHead] > SummL[pBottom]))) then DefTyp:= dtHead;

    if ((Summ[pBody] > Summ[pBottom]) or ((Summ[pBody] = Summ[pBottom]) and (SummL[pBody] > SummL[pBottom]))) and
       ((Summ[pBody] > Summ[pHead]) or ((Summ[pBody] = Summ[pHead]) and (SummL[pBody] > SummL[pHead]))) then DefTyp:= dtBody;

    if ((Summ[pBottom] > Summ[pBody]) or ((Summ[pBottom] = Summ[pBody]) and (SummL[pBottom] > SummL[pBody]))) and
       ((Summ[pBottom] > Summ[pHead]) or ((Summ[pBottom] = Summ[pHead]) and (SummL[pBottom] > SummL[pHead]))) then DefTyp:= dtBottom;

    if OnlyCh0 then DefTyp:= dtLostBtm;

                                       // ������ ������ ������ �������
    NewRgn.StCrd:= MaxInt;
    NewRgn.EdCrd:= - MaxInt;
    for K:= 0 to High(PBIdx) do
    begin
      PBItem:= FPBList[R, FDefPBList[R, PBIdx[K]].Ch, FDefPBList[R, PBIdx[K]].Idx];
      NewRgn.StCrd:= Min(NewRgn.StCrd, PBItem.Crd.StCrd);
      NewRgn.EdCrd:= Max(NewRgn.EdCrd, PBItem.Crd.EdCrd);
    end;

    J:= Length(FDefList[R]);
                                                             // ���������� ���������� ��
    Flg1:= ((J <> 0) and
            (FDefList[R, J - 1].Typ = dtLostBtm) and
            (DefTyp = dtLostBtm) and
            (NewRgn.StCrd - FDefList[R, J - 1].Rgn.EdCrd < 670 / FScanStep));

                                                            // ���������� ����������� ��������
    Flg2:= (J <> 0) and (Abs(NewRgn.StCrd - FDefList[R, J - 1].Rgn.EdCrd) < 1000 / FScanStep);

    if Flg1 or Flg2 then                     // ����������
    begin
      FDefList[R, J - 1].Rgn.StCrd:= Min(NewRgn.StCrd, FDefList[R, J - 1].Rgn.StCrd);
      FDefList[R, J - 1].Rgn.EdCrd:= Max(NewRgn.EdCrd, FDefList[R, J - 1].Rgn.EdCrd);

      MaxIdx:= Length(FDefList[R, J - 1].PBList);
      SetLength(FDefList[R, J - 1].PBList, MaxIdx + Length(PBIdx));

      for K:= 0 to High(PBIdx) do
      begin
        PBItem:= FPBList[R, FDefPBList[R, PBIdx[K]].Ch, FDefPBList[R, PBIdx[K]].Idx];
        NewPBI.Idx:= FDefPBList[R, PBIdx[K]].Idx;
        NewPBI.Ch:= FDefPBList[R, PBIdx[K]].Ch;
        NewPBI.Crd:= PBItem.Crd;
        if not (FDefPBList[R, PBIdx[K]].Ch in [0, 1])
          then NewPBI.Dly:= PBItem.Del
          else begin
                 NewPBI.Dly.StCrd:= PBItem.Del.StCrd div 3;
                 NewPBI.Dly.EdCrd:= PBItem.Del.EdCrd div 3;
               end;
        FDefList[R, J - 1].PBList[MaxIdx + K]:= NewPBI;
      end;
    end
    else
    begin               // ��������� ������
      MaxIdx:= Length(FDefList[R]);
      SetLength(FDefList[R], MaxIdx + 1);
      FDefList[R, MaxIdx].Typ:= DefTyp;
      FDefList[R, MaxIdx].Rgn:= NewRgn;
      FDefList[R, MaxIdx].SummLen:= SummLen;
      SetLength(FDefList[R, MaxIdx].PBList, Length(PBIdx));
      for K:= 0 to High(PBIdx) do
      begin
        PBItem:= FPBList[R, FDefPBList[R, PBIdx[K]].Ch, FDefPBList[R, PBIdx[K]].Idx];
        NewPBI.Idx:= FDefPBList[R, PBIdx[K]].Idx;
        NewPBI.Ch:= FDefPBList[R, PBIdx[K]].Ch;
        NewPBI.Crd:= PBItem.Crd { Range(Crd3, Crd4)};
        if not (FDefPBList[R, PBIdx[K]].Ch in [0, 1])
          then NewPBI.Dly:= PBItem.Del
          else begin
                 NewPBI.Dly.StCrd:= PBItem.Del.StCrd div 3;
                 NewPBI.Dly.EdCrd:= PBItem.Del.EdCrd div 3;
               end;
        FDefList[R, MaxIdx].PBList[K]:= NewPBI;
      end;
    end;
  end;
end;

(*
    New.Typ:=
    New.Rgn.StCrd:= MaxInt;
    New.Rgn.EdCrd:= - MaxInt;
//    New.PBCount:= 0;
    New.SummLen:= SummLen;
    {$IFDEF RECDEBUG}
    New.Text:= S;
    {$ENDIF}

    for K:= 0 to High(PBIdx) do
    begin
      PBItem:= FPBList[R, FDefPBList[R, PBIdx[K]].Ch, FDefPBList[R, PBIdx[K]].Idx];
//      if New.PBCount < 21 then             // ��������� ����� �������
      begin
        SetLength(New.PBList);
        New.PBList[New.PBCount].Idx:= FDefPBList[R, PBIdx[K]].Idx;
        New.PBList[New.PBCount].Ch:= FDefPBList[R, PBIdx[K]].Ch;
        New.PBList[New.PBCount].Crd:= PBItem.Crd { Range(Crd3, Crd4)};
        if not (FDefPBList[R, PBIdx[K]].Ch in [0, 1])
          then New.PBList[New.PBCount].Dly:= PBItem.Del
          else begin
                 New.PBList[New.PBCount].Dly.StCrd:= PBItem.Del.StCrd div 3;
                 New.PBList[New.PBCount].Dly.EdCrd:= PBItem.Del.EdCrd div 3;
               end;
        Inc(New.PBCount);
      end;
                                                   // ���������� �������
      New.Rgn.StCrd:= Min(New.Rgn.StCrd, PBItem.Crd.StCrd);
      New.Rgn.EdCrd:= Max(New.Rgn.EdCrd, PBItem.Crd.EdCrd);
    end;

    J:= Length(FDefList[R]);                   // ���������� ���������� ��
    if (J <> 0) and
       (FDefList[R, J - 1].Typ = dtLostBtm) and
       (New.Typ = dtLostBtm) and
       (New.Rgn.StCrd - FDefList[R, J - 1].Rgn.EdCrd < 670 / FScanStep) then
    begin
      FDefList[R, J - 1].Rgn.StCrd:= Min(New.Rgn.StCrd, FDefList[R, J - 1].Rgn.StCrd);
      FDefList[R, J - 1].Rgn.EdCrd:= Max(New.Rgn.EdCrd, FDefList[R, J - 1].Rgn.EdCrd);
      for K:= 0 to New.PBCount - 1 do
      begin
        if FDefList[R, J - 1].PBCount > 20 then Break;
        FDefList[R, J - 1].PBList[FDefList[R, J - 1].PBCount]:= New.PBList[K];
        Inc(FDefList[R, J - 1].PBCount);
      end;
    end
    else
    begin

/// ----------- ���������� ����������� �������� --------------------------------

      MaxIdx:= High(FDefList[R]);
      if (Length(FDefList[R]) <> 0) and
         (GetCentr(New.Rgn) - GetCentr(FDefList[R, MaxIdx].Rgn) < 1000 / FScanStep) then
      begin
        FDefList[R, MaxIdx].Rgn.StCrd:= Min(New.Rgn.StCrd, FDefList[R, MaxIdx].Rgn.StCrd);
        FDefList[R, MaxIdx].Rgn.EdCrd:= Max(New.Rgn.EdCrd, FDefList[R, MaxIdx].Rgn.EdCrd);
      end
      else
      begin
        SetLength(FDefList[R], J + 1);
        FDefList[R, J]:= New;
      end
    end; *)

begin
  Res[pHead, 0].Min:= 0;       Res[pHead, 0].Max:= 255;
  Res[pHead, 1].Min:= 0;       Res[pHead, 1].Max:= 11 * 3 - 1;
  Res[pHead, 2].Min:= 0;       Res[pHead, 2].Max:= 255;
  Res[pHead, 3].Min:= 0;       Res[pHead, 3].Max:= 255;
  Res[pHead, 4].Min:= 0;       Res[pHead, 4].Max:= 75;
  Res[pHead, 5].Min:= 0;       Res[pHead, 5].Max:= 255;
  Res[pHead, 6].Min:= 0;       Res[pHead, 6].Max:= 30;
  Res[pHead, 7].Min:= 0;       Res[pHead, 7].Max:= 30;

  Res[pBody, 0].Min:= 0;       Res[pBody, 0].Max:= 255;
  Res[pBody, 1].Min:= 11 * 3;  Res[pBody, 1].Max:= 50 * 3 - 1;
  Res[pBody, 2].Min:= - 1;     Res[pBody, 2].Max:= - 1;
  Res[pBody, 3].Min:= - 1;     Res[pBody, 3].Max:= - 1;
  Res[pBody, 4].Min:= 76;      Res[pBody, 4].Max:= 255;
  Res[pBody, 5].Min:= - 1;     Res[pBody, 5].Max:= - 1;
  Res[pBody, 6].Min:= 31;      Res[pBody, 6].Max:= 130;
  Res[pBody, 7].Min:= 31;      Res[pBody, 7].Max:= 130;

  Res[pBottom, 0].Min:= 0;       Res[pBottom, 0].Max:= 255;
  Res[pBottom, 1].Min:= 50 * 3;  Res[pBottom, 1].Max:= 255;
  Res[pBottom, 2].Min:= - 1;     Res[pBottom, 2].Max:= - 1;
  Res[pBottom, 3].Min:= - 1;     Res[pBottom, 3].Max:= - 1;
  Res[pBottom, 4].Min:= - 1;     Res[pBottom, 4].Max:= - 1;
  Res[pBottom, 5].Min:= - 1;     Res[pBottom, 5].Max:= - 1;
  Res[pBottom, 6].Min:= 131;     Res[pBottom, 6].Max:= 255;
  Res[pBottom, 7].Min:= 131;     Res[pBottom, 7].Max:= 255;

  for R:= rLeft to rRight do
  begin
    Flg:= False;
    SetLength(PBIdx, 0);
    for I:= 0 to High(FDefPBList[R]) do
    begin
      with FPBList[R, FDefPBList[R, I].Ch, FDefPBList[R, I].Idx] do
        Crd1:= Round(CCrd + (ChList[FDefPBList[R, I].Ch].Shift[2] + ChList[FDefPBList[R, I].Ch].RedPar[2] * CDel) / FScanStep);
      if not Flg then
      begin
        Crd2:= Crd1;
        Flg:= True;
        SetLength(PBIdx, 1);
        PBIdx[0]:= I;
      end
      else
//      if Flg or (I = High(FDefPBList[R])) then
      begin
        if Abs(Crd2 - Crd1) < Floor(RecConfig.SummZone / FScanStep) then   //  < ��������� 1 // ���� ����� �� ����������� ����� � ������ PBIdx
        begin
          SetLength(PBIdx, Length(PBIdx) + 1);
          PBIdx[High(PBIdx)]:= I;
   //       if I = High(FDefPBList[R]) then GetRes;
        end
        else
        begin
          GetRes;                                   // �� ����������� ������ ��������� �������
          SetLength(PBIdx, 1);
          Crd2:= Crd1;
          Flg:= True;                               // �������� ����� ����������
          PBIdx[0]:= I;
        end;
      end;
    end;

    if Flg then GetRes; // ����������

  end;
  SetLength(PBIdx, 0);

/// ----------- ���������� ���� ������� �������� --------------------------------
(*
  for R:= rLeft to rRight do
  begin
    I:= 0;
    J:= I;
    repeat
      if J >= High(FDefList[R]) then Break;  // ���������� ������� ��� - ���������� �����
      while (J < High(FDefList[R])) and
            (GetCentr(FDefList[R, J].Rgn) - GetCentr(FDefList[R, J + 1].Rgn) < 1000 / FScanStep) do Inc(J);
      if I <> J then
      begin
        FDefList[R, M].

        for M:= I to J do FDefList[R, M].Typ:= dtDel;
        Inc(J);
        I:= J;
      end
      else
      begin
        Inc(J);
        I:= J;
      end;
    until False;
  end;
*)
// ---------------  ������� ����� ������� ---------------------------------------

  for R:= rLeft to rRight do
    for I:= 0 to High(FDefList[R]) do
    begin
      for J:= 0 to 7 do ChCount[J]:= 0;
      for J:= 0 to High(FDefList[R, I].PBList) do
        Inc(ChCount[FDefList[R, I].PBList[J].Ch]);

      if (Ord(ChCount[0] <> 0) + // ���� ������������ � 4-� ��� ����� ������� � ...
          Ord(ChCount[1] <> 0) +
          Ord(ChCount[2] <> 0) +
          Ord(ChCount[3] <> 0) +
          Ord(ChCount[4] <> 0) +
          Ord(ChCount[5] <> 0) +
          Ord(ChCount[6] <> 0) +
          Ord(ChCount[7] <> 0) >= 4) and

         (ChCount[2] +          // ... � ����� 6 ����� (��� 0 � 1 �������) �� - ������������ ��
          ChCount[3] +
          ChCount[4] +
          ChCount[5] +
          ChCount[6] +
          ChCount[7] >= 6) then FDefList[R, I].Typ:= pbtiUnChkCon;

      for J:= 0 to High(FDefList[R, I].PBList) do
      begin
        Idx1:= FDefList[R, I].PBList[J].Ch;
        Idx2:= FDefList[R, I].PBList[J].Idx;
        FPBList[R, Idx1, Idx2].Typ:= FPBList[R, Idx1, Idx2].Typ + [pbtiDefect];
      end;
    end;

//        with FPBList[R, FDefList[R, I].PBList[J].Ch, FDefList[R, I].PBList[J].Idx] do
//          Typ:= Typ + [pbtiDefect];

end;

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

procedure TRecData.PrgMes(S: string);
begin
  if Assigned(FProgressMess) then
  begin
    FProgressMess(Format('%3.1f ���', [(GetTickCount - FTime) / 1000]));
    FTime:= GetTickCount;
    FProgressMess(S);
  end;
end;

procedure TRecData.Work(RE: TRecEngine; DatSrc: TAvk11DatSrc; Range: TRange; Debug: Boolean = False; DebugDisCrd: Integer = - 1);
var
  I, J, L, Ch: Integer;
  R: RRail_;

begin
  FDatSrc:= DatSrc;
  FDebug:= Debug;
  FStCrd:= Range.StCrd;
  FEdCrd:= Range.EdCrd;
  FScanStep:= DatSrc.Header.ScanStep / 100;

  FMaxShift:= 0;
  for I:= 0 to 7 do
    FMaxShift:= Max(FMaxShift, Abs(Round(ChList[I].Shift[1] + ChList[I].RedPar[1] * 255 / FScanStep)));

  {$IFDEF RECDEBUG}
  RecDataForm:= TRecDataForm.Create(nil);
  {$ENDIF}

  FTime:= GetTickCount;

  // --- ������������ ���� ������� ������� ----------------------------

  PrgMes('������������ ���� ������� �������');
  FBSignal:= TBSignal.Create;
  FBSignal.Calc(FDatSrc, FStCrd, FEdCrd);

  // --- ���������� ��� �������� ����� --------------------------------

  PrgMes('���� �������� �����');
  FBMList:= TBMList.Create;
  TBMList(FBMList).Calc(FDatSrc);

  // --- ���������� ---------------------------------------------------

  PrgMes('���������� ');
  FFiltr:= TFiltr.Create;
  FFiltr.OnProgress:= nil;
//DF  FFiltr.Calc(FDatSrc, FBSignal, FBMList, FStCrd, FEdCrd);

  // --- ����� ����� � ������ ������� ---------------------------------

  PrgMes('����� ����� � ������ �������                              ');
  FindBottomSignalHoles;

  // --- ������������ ���� �� �� ������� ������ �� --------------------

  PrgMes('������������ ���� �� �� ������� ������ ��                 ');
  BJButton;

  // --- ����� ����� � ���������� ����� 1 ������ ----------------------

  PrgMes('����� �����');
  PBSearch;

  // --- ���������� ����� � ����� ------------------------------------- :  6  :

  PrgMes('����� �����');
  SortPBSearch;

// --- �������� ������ ��� ����� ------------------------------------:  9  :
//  PrgMes('�������� ������ ��� �����');
//  PBDeadZone;
// --- ������������ ��������� ��� ������� 0, 1, 6 � 7 --------------- :  7  :
//  PrgMes('������������ ��������� ��� ������� 0, 1, 6, 7');
//  MakeSigns;
// --- ����� ��������� - ����������� ��������� ������ ������� ------- :  8  :
//  PrgMes('����� ��������� - ����������� ��������� ������ ������� ');
//  SearchBoltHoles;
// --- ����� ������� ------------------------------------------------ :  10 :
//  PrgMes('����� �������');
//  BladeSearch;
// --- ������� ����� ������������ �� ���� ������ -------------------------------

  PrgMes('����� �����');
  MarkPB;

//  PrgMes('����� ������ 1                                            '); EndSearchStep1;         // --- ����� ������ ��� 1 - �� �������� ------------------------------:  11 :
//  PrgMes('����� ������ 2                                            '); EndSearchStep2;         // --- ����� ������ ��� 2 - �� ������, ��, ������ ------------------- :  12 :
//  PrgMes('�������������� ������� ����� � ����������� ������         '); BigHole_to_PBList;      // --- �������������� ������� ����� � ����������� ������ ------------ :  13 :
//  PrgMes('C��������� �������� �� ������������ ����������            '); SortSingleObj;          // --- C��������� �������� �� ������������ ���������� --------------- :  14 :
//  PrgMes('������������ ��� ��� ������� �������                      '); MakeHeadRailObj;        // --- ������������ ��� ��� ������� ������� ------------------------- :  15 :
//  PrgMes('������� ����� ������������ �� ����� 1                     '); MarkEndPB;              // --- ������� ����� ������������ �� ����� 1 ------------------------ :  16 :
//  PrgMes('������� ����� ������������ �� ����� 2                     '); MarkEnd2PB;             // --- ������� ����� ������������ �� ����� 2 ------------------------ :  17 :

//  PrgMes('��������� ����� ������������� ����� � ����� �������� �����'); MarkUnknownPB;          // --- ��������� ����� ������������� ����� � ����� �������� �����  -- :  19 :
//  PrgMes('������������ ��� �� �����                                 '); MakeBodyRailObj;        // --- ������������ ��� �� �����  ----------------------------------- :  20 :
//  PrgMes('���������� ��� ������ ��� �� �� �������                   '); HeadSearchZone;         // --- ���������� ��� ������ ��� �� �� ������� ---------------------- :  21 :
//  PrgMes('���������� ��� ������ ��� �� �� �����                     '); BodySearchZone;         // --- ���������� ��� ������ ��� �� �� ����� ------------------------ :  22 :
//  PrgMes('����� ������� ������� - �� ������� �����                  '); MakeBodyNoisePath;      // --- ����� ������� ������� - �� ������� ����� --------------------- :  23 :
//  PrgMes('����� ������� ������� - �� ������� �������                '); MakeHeadNoisePath;      // --- ����� ������� ������� - �� ������� ������� ------------------- :  24 :
//  PrgMes('�������� ����� �� ��������� � ������� �������             '); TestBPtoNoise;          // --- �������� ����� �� ��������� � ������� ������� ---------------- :  25 :

// --- ������������ �������������� ��������� -------------------------

    PrgMes('�������������� ��������');
    MakeConstruct;

// --- ���������� ����� ��� ������ �������� --------------------------

    PrgMes('���������� ����� ��� ������ ��������');
    MakeDefectPBList;

// --- ����� �������� ------------------------------------------------

    PrgMes('����� ��������');
    SearchDefect;

// --- ����� ��������������������� �������� -------------------------

    PrgMes('����� ��������������������� ��������');
    UCSSearch;

// --- ����� ��� ���������� ���������� �������� ----------------------

    PrgMes('����� ��� ���������� ���������� ��������');
    SearchTechnoError;

// --- ����� ��������������������� �������� --------------------------

//    PrgMes('����� ��������������������� �������� 2                    ');
//    KillUCS;

// -----------------------------------------------------------------------------

  {$IFDEF RECDEBUG}
  if Debug then
  begin
    RecDataForm.SetData(DatSrc, Self, DebugDisCrd);
    RecDataForm.ShowModal;
    RecDataForm.Free;
  end;

// ------[ �������� �������� ]--------------------------------------------------

  for R:= rLeft to rRight do
  begin
//    SetLength(FDebugGis[R], 0);
    SetLength(FDebugGraph[0, 0], 0);
    SetLength(FDebugGraph[1, 0], 0);
    SetLength(FDebugGraph[1, 1], 0);
    SetLength(FDebugGraph[0, 1], 0);
    SetLength(FPointList[R], 0);
    for I:= 0 to High(FConstruct[R]) do
      SetLength(FConstruct[R, I].Idx, 0);
{    for Ch:= 1 to 7 do
      for I:= 0 to High(FPBList[R, Ch]) do
      begin
        SetLength(FPBList[R, Ch, I].Items, 0);
        SetLength(FPBList[R, Ch, I].TypList, 0);
      end; }
  end;
  {$ENDIF}

  for R:= rLeft to rRight do
  begin
//    SetLength(FEnd2[R], 0);
//    SetLength(FEnd3[R], 0);
//    SetLength(FPoints2[R], 0);
//    for Ch:= 0 to 7 do
//      SetLength(FPBList[R, Ch], 0);
//    SetLength(FBlades[R], 0);
    SetLength(FBSPice[R], 0);
{
    for I:= 0 to High(FRailObj[R]) do
    begin
      SetLength(FRailObj[R, I].HeadZones, 0);
      SetLength(FRailObj[R, I].BodyZones, 0);
    end;
}
//    SetLength(FRailObj[R], 0);
//    SetLength(FPtn1[R], 0);
//    SetLength(FPtn2[R], 0);
{    for I:= 0 to High(FHZone[R]) do
    begin
      SetLength(FHZone[R, I].Items, 0);
      for Ch:= 1 to 7 do
        SetLength(FHZone[R, I].Gis[Ch].DelayReg, 0);
    end;
    for I:= 0 to High(FBZone[R]) do
    begin
      SetLength(FBZone[R, I].Items, 0);
      for Ch:= 1 to 7 do
        SetLength(FBZone[R, I].Gis[Ch].DelayReg, 0);
    end;
 }
    SetLength(FBSPice[R], 0);
//    SetLength(FBSignal.FBSPos[R], 0);
//    SetLength(FPtn1[R], 0);
//    SetLength(FPtn2[R], 0);
//    SetLength(FRailObj[R], 0);
//    for Ch:= 1 to 7 do
//    begin
//      SetLength(FTHead[R, Ch], 0);
//    end;
  end;

  FBSignal.Free;
  FBMList.Free;
  FFiltr.Free;
end;


// -----------------------------------------------------------------------------
// ---------- ������������ ���� ������� ������� --------------------------------
// -----------------------------------------------------------------------------

constructor TBSignal.Create;
begin
  OnProgress:= nil;
end;

destructor TBSignal.Destroy;
var
  I, L, Ch: Integer;
  R: RRail_;

begin
  for R:= rLeft to rRight do
  begin
    SetLength(FBSPos[R], 0);
    {$IFDEF RECDEBUG}
    SetLength(FDebugGis[R], 0);
    {$ENDIF}
  end;
  SetLength(FBSGis, 0);
end;

procedure TBSignal.Calc(DatSrc: TAvk11DatSrc; StCrd, EdCrd: Integer);
begin
  FStCrd:= Max(0, StCrd);
  FEdCrd:= Min(DatSrc.MaxDisCoord, EdCrd);
  FDatSrc:= DatSrc;
  FScanStep:= FDatSrc.Header.ScanStep / 100;
  FindBottomSignalZone;
end;

function TBSignal.FindBSZoneBlockOK1(StartDisCoord: Integer): Boolean; // --- ���������� ���������� ��� ������� ������ --------------------------------
var
  R: RRail_;
  I: Integer;

begin
  for R:= rLeft to rRight do
    with FDatSrc.CurEcho[R, 1] do
      for I:= 1 to Count do
        if Delay[I] in [TBSGisMin..TBSGisMax] then
          Inc(FBSGis[(FDatSrc.CurDisCoord - StartDisCoord) div FGisStep, R, Delay[I]], Sqr(Ampl[I] - 2));
  Result:= True;
  if Assigned(OnProgress) then OnProgress( Pointer(80 * ((FDatSrc.CurDisCoord - StartDisCoord) div FGisStep) div High(FBSGis)) );
end;

function TBSignal.FindBSZoneBlockOK2(StartDisCoord: Integer): Boolean; // --- ���������� ���������� ��� ������� ������ --------------------------------
var
  R: RRail_;
  I: Integer;

begin
  for R:= rLeft to rRight do
    with FDatSrc.CurEcho[R, 1] do
      for I:= 1 to Count do
        if Delay[I] in [TBSGisMin..TBSGisMax] then
          Inc(FBSGis2[R, Delay[I]], Sqr(Ampl[I] - 2));
  Result:= True;
end;


procedure TBSignal.FindBottomSignalZone;
var
  R: RRail_;
  I, J, L, K, Crd: Integer;
  FGisWidth: Integer;
  MaxPos: array [1..2] of Integer;
  MaxVol: array [1..2] of Integer;
  NewPoint: TBSPos;
  Sig1: Integer;
  Sig2: Integer;
  Gis: TBSGis;

begin
  FGisWidth:= Round(1000 / FScanStep);   // ������ ����
              // ����                 [   ������ ����   ]
  Sig1:= 5;   // ���������� ��������� [1] [2] [3] [4] [5]
  Sig2:= 2;   // ������ �� �������� [-2] [-1] [X] [+1] [+2] - ����� ���� Sig1
  FGisStep:= FGisWidth div Sig1;         // ��� ����
  SetLength(FBSGis, Ceil((FEdCrd - FStCrd) / FGisStep));

  {$IFDEF RECDEBUG}
//  for R:= rLeft to rRight do         // ����� ������� - ������������� ������
//    SetLength(FDebugGis[R], Ceil((FEdCrd - FStCrd) / FGisStep));
  {$ENDIF}

  for R:= rLeft to rRight do
  begin
    J:= Length(FBSPos[R]);               // ������ ����� � ������ ������� ������� - ������ �������� �����
    SetLength(FBSPos[R], J + 1);
  end;

  FDatSrc.LoadData(FStCrd, FEdCrd, 0, FindBSZoneBlockOK1); // ������ ����������

//  I:= FStCrd;                     // �������� � ������ �����
//  while I < FEdCrd do             // ���� �� ����� �����
  for I:= 0 to High(FBSGis) do
  begin
//    for R:= rLeft to rRight do
//      FillChar(FTestGis[R, 0], 1024, 0);
//    for FSigIdx:= 1 to Sig do
//      FDatSrc.LoadData(I - FGisWidth div 2, I + FGisWidth div 2, 0, FindBSZoneBlockOK);
//    Inc(I, FGisStep);
//    I * FGisStep

    Crd:= FStCrd + I * FGisStep + FGisStep div 2;

    for R:= rLeft to rRight do
//      for L:= 0 to 255 do
      for L:= 35 * 3 to 85 * 3 do
      begin
        Gis[R, L]:= 0;                      // �������� �����������
        for K:= Max(0, I - Sig2) to Min(High(FBSGis), I + Sig2) do
          Gis[R, L]:= Gis[R, L] + FBSGis[K, R, L];
      end;

    for R:= rLeft to rRight do
    begin
      MaxVol[1]:= 0;                       // ���� �������� �1
        for L:= 35 * 3 to 85 * 3 do
//          if FTestGis[K, R, L] > MaxVol[1] then
          if Gis[R, L] > MaxVol[1] then
          begin
      //      MaxVol[1]:= FTestGis[R, L];
            MaxVol[1]:= Gis[R, L];
            MaxPos[1]:= L;
          end;

      MaxVol[2]:= 0;                       // ���� �������� �2
      for L:= 35 * 3 to 85 * 3 do
       if not ((L >= MaxPos[1] - 15) and (L <= MaxPos[1] + 15)) then
//        if FTestGis[0, R, L] > MaxVol[2] then
        if Gis[R, L] > MaxVol[2] then
        begin
//          MaxVol[2]:= FTestGis[0, R, L];
          MaxVol[2]:= Gis[R, L];
          MaxPos[2]:= L;
        end;

      if MaxVol[1] / FGisWidth > 0.1 then  // ���� ���� ������ �����
      begin
        if MaxVol[1] / 6 <= MaxVol[2] then // ���� ������ �������� ���������� ����� ...
        begin
          NewPoint.MinDelay:= Min(MaxPos[1], MaxPos[2]) - 6; // ... ��������� ���� ������� �������
          NewPoint.MaxDelay:= Max(MaxPos[1], MaxPos[2]) + 6;
        end
        else
        begin                              // ����� �����������
          NewPoint.MinDelay:= MaxPos[1] - 6;
          NewPoint.MaxDelay:= MaxPos[1] + 6;
        end;
        NewPoint.Crd:= Crd;
        J:= Length(FBSPos[R]);             // ��������� ���������� �� ��������� �����
        if not CompareMem(@FBSPos[R, J - 1].MaxDelay, @NewPoint.MaxDelay, 8) then
        begin                  // ����� ����� � ������ ������� �������
          SetLength(FBSPos[R], J + 2);
          FBSPos[R, J]:= FBSPos[R, J - 1]; // ��������� ���������� ����� ...
          FBSPos[R, J].Crd:= Crd;          // �� ������� ����������
          FBSPos[R, J + 1]:= NewPoint;     // ����� �����
        end;

        if (NewPoint.MaxDelay - NewPoint.MinDelay > 15) then // � ������ �������� ��������� �������������� ����� ��� ���������� ���������
        begin
          J:= Length(FBSPos[R]);
          SetLength(FBSPos[R], J + 6);               // + 2 �����
          FBSPos[R, J]:= FBSPos[R, J - 1];
          FBSPos[R, J].Crd:= Crd;                    // �� ������� ����������
          FBSPos[R, J + 1]:= FBSPos[R, J - 1];
          FBSPos[R, J + 1].Crd:= Crd;                // �� ������� ����������

          FBSPos[R, J + 2]:= FBSPos[R, J - 1];
          FBSPos[R, J + 2].Crd:= Crd + FGisStep div 3; // �� ������� ���������� + 1 / 3 ����
          FBSPos[R, J + 3]:= FBSPos[R, J - 1];
          FBSPos[R, J + 3].Crd:= Crd + FGisStep div 3; // �� ������� ���������� + 1 / 3 ����

          FBSPos[R, J + 4]:= FBSPos[R, J - 1];
          FBSPos[R, J + 4].Crd:= Crd + 2 * FGisStep div 3; // �� ������� ���������� + 2 / 3 ����
          FBSPos[R, J + 5]:= FBSPos[R, J - 1];
          FBSPos[R, J + 5].Crd:= Crd + 2 * FGisStep div 3; // �� ������� ���������� + 2 / 3 ����
        end;
      end;

      {$IFDEF RECDEBUG}                      // ��������� ���������� ����������
//      Move(Gis[R, TBSGisMin], FDebugGis[R, I].Gis[TBSGisMin], (TBSGisMax - TBSGisMin + 1) * 4); // �����������
//      FDebugGis[R, I].MaxPos[1]:= MaxPos[1];                 // ������� ����������
//      FDebugGis[R, I].MaxPos[2]:= MaxPos[2];
//      FDebugGis[R, I].Range.StCrd:= Crd - FGisWidth div 2;     // ���������� ���� ���������� �����������
//      FDebugGis[R, I].Range.EdCrd:= Crd + FGisWidth div 2;
      {$ENDIF}
    end;
    if Assigned(OnProgress) then OnProgress( Pointer(80 + 20 * I div High(FBSGis)) );
  end;

  for R:= rLeft to rRight do            // �������� ������ ������ ����� � ������ ������� ������� ...
  begin
    if Length(FBSPos[R]) >= 3 then        // ... ���� ���� ���
    begin
      FBSPos[R, 0]:= FBSPos[R, 2];        // � ������ ����� ��� ��� ��� ���������� ����� ����� ������������ I - 1 ���� � �� ���.
      FBSPos[R, 0].Crd:= FStCrd;
      FBSPos[R, 1].MinDelay:= FBSPos[R, 2].MinDelay;
      FBSPos[R, 1].MaxDelay:= FBSPos[R, 2].MaxDelay;
    end;

    J:= Length(FBSPos[R]);             // ��������� ����� � ������ ������� �������
    SetLength(FBSPos[R], J + 1);
    FBSPos[R, J]:= FBSPos[R, J - 1];
    FBSPos[R, J].Crd:= FEdCrd;
  end;

  //----------- �������������� ��������� � ������ �������� ������� ������� ---------------

  for R:= rLeft to rRight do
  begin
    I:= 0;
    while I <= High(FBSPos[R]) do
    begin
      if FBSPos[R, I].MaxDelay - FBSPos[R, I].MinDelay > 15 then
      begin
//        FillChar(FTestGis[0, R, 0], 1024, 0); // �������� �����������

        FillChar(FBSGis2[R, TBSGisMin], (TBSGisMax - TBSGisMin + 1) * 4, 0); // �������� �����������
        FDatSrc.LoadData(FBSPos[R, I].Crd, FBSPos[R, I + 1].Crd, 0, FindBSZoneBlockOK2);
        MaxVol[1]:= 0;                       // ���� ��������

//        Inc(GE);

        for L:= FBSPos[R, I].MinDelay to FBSPos[R, I].MaxDelay do
          if (L in [TBSGisMin..TBSGisMax]) and
             (FBSGis2[R, L] > MaxVol[1]) then
          begin
            MaxVol[1]:= FBSGis2[R, L];
            MaxPos[1]:= L;
          end;
        FBSPos[R, I].MaxDelay:= MaxPos[1] + 15;
        FBSPos[R, I].MinDelay:= MaxPos[1] - 15;
        FBSPos[R, I + 1].MaxDelay:= MaxPos[1] + 15;
        FBSPos[R, I + 1].MinDelay:= MaxPos[1] - 15;
        Inc(I, 2);
      end else Inc(I, 2);
    end;
  end;
end;

// --- ���������� ��� �������� ����� ----------------------------------

constructor TBMList.Create;
begin
  //
end;

destructor TBMList.Destroy;
var
  I: Integer;

begin
  for I:= 0 to High(FBMData) do
  begin
    SetLength(FBMData[I].DisItems, 0);
    SetLength(FBMData[I].LastPass, 0);
  end;
  SetLength(FBMData, 0);
//  SetLength(LastZones, 0);
end;

procedure TBMList.Calc(DatSrc: TAvk11DatSrc);
var
  I: Integer;
  J: Integer;
  K: Integer;
  SC: Integer;
  EC: Integer;
  Flg: Boolean;
  DisCoord: Integer;
  StDisCrd: Integer;
  EdDisCrd: Integer;
  LastEdDisCrd: Integer;

  FullLen: Integer;
  SummLen: Integer;
  CurLen: Integer;
  SysRange: TMinMax;
  MinSys: Integer;

  StCrd: Integer;

begin
  I:= 0;
  LastEdDisCrd:= - 1;
  with DatSrc do
    repeat
      Flg:= False;
      StDisCrd:= Event[FCDList[I]].DisCoord;
      EdDisCrd:= Event[FCDList[I]].DisCoord;
      repeat
        for J:= 0 to High(FCDList) - 1 do
          if (I <> J) and (I <> J + 1) then
            if (Event[FCDList[I]].SysCoord >= Min(Event[FCDList[J]].SysCoord, Event[FCDList[J + 1]].SysCoord)) and
               (Event[FCDList[I]].SysCoord <= Max(Event[FCDList[J]].SysCoord, Event[FCDList[J + 1]].SysCoord)) and
               (Event[FCDList[J]].SysCoord <> Event[FCDList[J + 1]].SysCoord) then
            begin
              DisCoord:= Event[FCDList[J]].DisCoord +
                          Round( Abs(Event[FCDList[I]].SysCoord - Event[FCDList[J]].SysCoord) /
                                 Abs(Event[FCDList[J]].SysCoord - Event[FCDList[J + 1]].SysCoord) *
                                (Event[FCDList[J + 1]].DisCoord - Event[FCDList[J]].DisCoord));

              StDisCrd:= Min(StDisCrd, DisCoord);
              EdDisCrd:= Max(EdDisCrd, DisCoord);
              Flg:= True;
            end;

        if (I + 1 > High(FCDList)) or
           (Event[FCDList[I + 1]].DisCoord > EdDisCrd) then Break;
        Inc(I);
      until False;

      if Flg and ((LastEdDisCrd = - 1) or (StDisCrd > LastEdDisCrd)) then
      begin
        SetLength(FBMData, Length(FBMData) + 1);
        FBMData[High(FBMData)].DisRange.StCrd:= StDisCrd;
        FBMData[High(FBMData)].DisRange.EdCrd:= EdDisCrd;
        LastEdDisCrd:= EdDisCrd;
      end;
      Inc(I);
    until I >= Length(FCDList);

  with DatSrc do
    for I:= 0 to High(FBMData) do
    begin
      for J:= 0 to High(FCDList) do
        if (Event[FCDList[J]].DisCoord > FBMData[I].DisRange.StCrd) and
           (Event[FCDList[J]].DisCoord < FBMData[I].DisRange.EdCrd) then
        begin
          SetLength(FBMData[I].DisItems, Length(FBMData[I].DisItems) + 1);
          if Length(FBMData[I].DisItems) = 1
            then FBMData[I].DisItems[High(FBMData[I].DisItems)].StCrd:= FBMData[I].DisRange.StCrd
            else FBMData[I].DisItems[High(FBMData[I].DisItems)].StCrd:= FBMData[I].DisItems[High(FBMData[I].DisItems) - 1].EdCrd;
          FBMData[I].DisItems[High(FBMData[I].DisItems)].EdCrd:= Event[FCDList[J]].DisCoord;
        end;

      if Length(FBMData[I].DisItems) <> 0 then
      begin
        SetLength(FBMData[I].DisItems, Length(FBMData[I].DisItems) + 1);
        FBMData[I].DisItems[High(FBMData[I].DisItems)].StCrd:= FBMData[I].DisItems[High(FBMData[I].DisItems) - 1].EdCrd;
        FBMData[I].DisItems[High(FBMData[I].DisItems)].EdCrd:= FBMData[I].DisRange.EdCrd;
      end;
    end;

  with DatSrc do
    for I:= 0 to High(FBMData) do
    begin
      FBMData[I].SysRange.StCrd:= DisToSysCoord(FBMData[I].DisRange.StCrd);
      FBMData[I].SysRange.EdCrd:= DisToSysCoord(FBMData[I].DisRange.EdCrd);
    end;

// -------- ����� ��������� ������ �������� - LastZones ------------------------

  for I:= 0 to High(FBMData) do
  begin
    SummLen:= 0;
    FullLen:= GetLen(FBMData[I].SysRange);
    for J:= High(FBMData[I].DisItems) downto 0 do
      if not Odd(J) then
      begin
        SysRange.Min:= DatSrc.DisToSysCoord(FBMData[I].DisItems[J].StCrd);
        SysRange.Max:= DatSrc.DisToSysCoord(FBMData[I].DisItems[J].EdCrd);
        if SummLen = 0 then                            // ������ ������� ...
        begin
          MinSys:= SysRange.Min;
          Inc(SummLen, GetLen(FBMData[I].DisItems[J]));

//          SetLength(LastZones, Length(LastZones) + 1);          // ... ����� �������
//          LastZones[High(LastZones)]:= FBMData[I].DisItems[J];

          SetLength(FBMData[I].LastPass, Length(FBMData[I].LastPass) + 1);          // ... ����� �������
          FBMData[I].LastPass[High(FBMData[I].LastPass)]:= FBMData[I].DisItems[J];
        end
        else
        if SysRange.Min < MinSys then // ����������� ������� ����� �� ���� ����� - ������ ���� �� ���� ������ ����������� �����
        begin

          SetLength(FBMData[I].LastPass, Length(FBMData[I].LastPass) + 1);
          FBMData[I].LastPass[High(FBMData[I].LastPass)].StCrd:= FBMData[I].DisItems[J].StCrd;
          if  SysRange.Max - SysRange.Min <> 0
            then FBMData[I].LastPass[High(FBMData[I].LastPass)].EdCrd:= FBMData[I].DisItems[J].StCrd + Round((MinSys - SysRange.Min) * (FBMData[I].DisItems[J].EdCrd - FBMData[I].DisItems[J].StCrd) / (SysRange.Max - SysRange.Min))
            else FBMData[I].LastPass[High(FBMData[I].LastPass)].EdCrd:= FBMData[I].DisItems[J].StCrd + Round((MinSys - SysRange.Min));


//          SetLength(LastZones, Length(LastZones) + 1);
//          LastZones[High(LastZones)].StCrd:= FBMData[I].DisItems[J].StCrd;
//          LastZones[High(LastZones)].EdCrd:= FBMData[I].DisItems[J].StCrd + Round((MinSys - SysRange.Min) * (FBMData[I].DisItems[J].EdCrd - FBMData[I].DisItems[J].StCrd) / (SysRange.Max - SysRange.Min));
          Inc(SummLen, MinSys - SysRange.Min);  // ����� ����� ����������� �����
          MinSys:= SysRange.Min;
        end;
        if SummLen >= FullLen then Break;
      end;
  end;

// -------- ����������� ������������ ������������� ��� -------------------

  FMaxBMLen:= 0;
  for I:= 0 to High(FBMData) do
    FMaxBMLen:= Max(FMaxBMLen, GetLen(FBMData[I].DisRange));

// -------- ���������� ������� ��� - ��� ������ ��� �� � ��� -------------------
(*
  StCrd:= 0;
  for I:= 0 to High(FBMData) do
  begin
    SetLength(WorkZones, Length(WorkZones) + 1);          // ... ����� �������
    WorkZones[High(WorkZones)].StCrd:= StCrd;
    WorkZones[High(WorkZones)].EdCrd:= FBMData[I].DisRange.StCrd;

    SummLen:= 0;
    FullLen:= GetLen(FBMData[I].SysRange);
    for J:= High(FBMData[I].DisItems) downto 0 do
      if not Odd(J) then
      begin
        SysRange.Min:= DatSrc.DisToSysCoord(FBMData[I].DisItems[J].StCrd);
        SysRange.Max:= DatSrc.DisToSysCoord(FBMData[I].DisItems[J].EdCrd);
        if SummLen = 0 then                            // ������ ������� ...
        begin
          MinSys:= SysRange.Min;
          Inc(SummLen, GetLen(FBMData[I].DisItems[J]));

          SetLength(WorkZones, Length(WorkZones) + 1);          // ... ����� �������
          WorkZones[High(WorkZones)]:= FBMData[I].DisItems[J];
        end
        else
        if SysRange.Min < MinSys then // ����������� ������� ����� �� ���� ����� - ������ ���� �� ���� ������ ����������� �����
        begin
          SetLength(WorkZones, Length(WorkZones) + 1);
          WorkZones[High(WorkZones)].StCrd:= FBMData[I].DisItems[J].StCrd;
          WorkZones[High(WorkZones)].EdCrd:= FBMData[I].DisItems[J].StCrd + Round((MinSys - SysRange.Min) * (FBMData[I].DisItems[J].EdCrd - FBMData[I].DisItems[J].StCrd) / (SysRange.Max - SysRange.Min));
          Inc(SummLen, MinSys - SysRange.Min);  // ����� ����� ����������� �����
          MinSys:= SysRange.Min;
        end;
        if SummLen >= FullLen then Break;
      end;

    StCrd:= FBMData[I].DisRange.EdCrd;
  end;
  SetLength(WorkZones, Length(WorkZones) + 1);          // ����������
  WorkZones[High(WorkZones)].StCrd:= StCrd;
  WorkZones[High(WorkZones)].EdCrd:= DatSrc.MaxDisCoord;
  *)
end;

end.
