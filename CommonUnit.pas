unit CommonUnit;

interface

uses
  Forms, Classes;


//type
//  TShowPathOnMap = function(form : TForm; list: TList) : integer; stdcall;

var
//  ShowPathOnMap: TShowPathOnMap;
  DataFileDir: string;
  procedure TransferFilesToCommonSystemDir(Override_: Boolean = false);


implementation

uses
  Windows, SHFolder, ConfigUnit, CfgTablesUnit_2010, LanguageUnit, SysUtils, Dialogs;

const
  Files: array [1..3] of string = ({'init.ini', }'map.ini', 'files.dat', 'map.xml');

var
  I: Integer;
  DCP: PChar;
  Dir: string;

procedure TransferFilesToCommonSystemDir(Override_: Boolean = false);
begin

  DCP:= AllocMem(MAX_PATH);
  SHGetFolderPath( Application.Handle, {CSIDL_COMMON_APPDATA} CSIDL_LOCAL_APPDATA, 0, SHGFP_TYPE_DEFAULT, DCP);
  DataFileDir:= DCP + '\A14Data\';
  FreeMem(DCP);

  if not DirectoryExists(DataFileDir) then CreateDir(DataFileDir);

  Dir:= ExtractFilePath(Application.ExeName);
  for I:= 1 to 3 do
    if Override_ or (not FileExists(DataFileDir + Files[I])) then
      CopyFile(PChar(Dir + Files[I]), PChar(DataFileDir + Files[I]), False);

end;

var
//  LibName: string;
//  dllHandle : Cardinal;
  iSize: integer;
  pw: PWideChar;

function StringToPWide(sStr: string; var iNewSize: integer): PWideChar;
var
  pw: PWideChar;
  iSize: integer;
begin
  iSize := Length(sStr) + 1;
  iNewSize := iSize * 2;

  pw := AllocMem(iNewSize);

  MultiByteToWideChar(CP_ACP, 0, PAnsiChar(sStr), iSize, pw, iNewSize);

  Result := pw;
end;

initialization

  TransferFilesToCommonSystemDir(FileExists(ExtractFilePath(Application.ExeName) + 'init.ini'));

  Config:= TConfig.Create;
  Config.Load;

//  Language:= TProgLanguage.Create;
  LangTable:= TLanguageTable.Create(nil);

  LangTable.LoadFromFile( ExtractFilePath(Application.ExeName) + 'map.xml');
  LangTable.CurrentGroup:= Config.WorkLang;

//  ShowPathOnMap:= nil;
//  LibName:= ExtractFilePath(Application.ExeName) + 'mapView.dll';
//  pw:= StringToPWide(LibName, iSize);

//  dllHandle := LoadLibrary(PWideChar(LibName));
//  if dllHandle <> 0 then @ShowPathOnMap:= GetProcAddress(dllHandle, 'showPathOnMap');

finalization

  Config.Save;
  Config.Free;

//  Language.Free;
//  Language:= nil;

  LangTable.Free;

//  FreeLibrary(dllHandle);

end.




{
type
  PTList = ^TList;
  TShowPathOnMap = function(form : TForm; list: TList) : integer; stdcall;

 // list      : TList;}
  {
    if Assigned(func) then
      func(Form1, list)
    else
      ShowMessage('Function not found');

      }


111
112
113
121
122
123
124
125
127
1321
1322
133
134
135
139
153
154
211
212
213
2201
2202
2203
2204
221
2221
2222
2223
223
224
2251
2252
227
2321
2322
233
234
235
236
239
253
254
301
302
303
411
412
421
422
431
432
471
472
481
