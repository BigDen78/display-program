unit PGBarExport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls;

type
  TGPExportForm = class(TForm)
    ProgressBar1: TProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure ChgProgress(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GPExportForm: TGPExportForm;

implementation

{$R *.dfm}

procedure TGPExportForm.FormCreate(Sender: TObject);
begin
{  GroupBox1.Visible:=true;}
  ProgressBar1.Position:=0;
end;

procedure TGPExportForm.ChgProgress(Sender: TObject);
begin
  ProgressBar1.Position:= Integer(Sender);
  Self.Refresh;
end;

end.
