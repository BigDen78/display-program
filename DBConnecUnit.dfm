object DBConnectForm: TDBConnectForm
  Left = 192
  Top = 110
  Width = 870
  Height = 640
  Caption = 'DBConnectForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 31
    Top = 11
    Width = 67
    Height = 13
    Caption = #1055#1088#1077#1076#1087#1088#1080#1103#1090#1080#1077
  end
  object Label2: TLabel
    Left = 31
    Top = 35
    Width = 88
    Height = 13
    Caption = #1050#1086#1076' '#1085#1072#1087#1088#1072#1074#1083#1077#1085#1080#1103
  end
  object Label3: TLabel
    Left = 31
    Top = 59
    Width = 43
    Height = 13
    Caption = #1055#1077#1088#1077#1075#1086#1085
  end
  object Label4: TLabel
    Left = 31
    Top = 108
    Width = 117
    Height = 13
    Caption = #1053#1072#1095#1072#1083#1100#1085#1072#1103' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1072
  end
  object Label5: TLabel
    Left = 31
    Top = 132
    Width = 110
    Height = 13
    Caption = #1050#1086#1085#1077#1095#1085#1072#1103' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1072
  end
  object Label6: TLabel
    Left = 31
    Top = 157
    Width = 26
    Height = 13
    Caption = #1044#1072#1090#1072
  end
  object Label7: TLabel
    Left = 31
    Top = 84
    Width = 24
    Height = 13
    Caption = #1055#1091#1090#1100
  end
  object Button1: TButton
    Left = 304
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 152
    Top = 8
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 1
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 152
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Edit2'
  end
  object Edit3: TEdit
    Left = 152
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'Edit3'
  end
  object Edit4: TEdit
    Left = 152
    Top = 104
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 4
    Text = 'Edit4'
  end
  object Edit5: TEdit
    Left = 152
    Top = 128
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 5
    Text = 'Edit5'
  end
  object Edit6: TEdit
    Left = 152
    Top = 152
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 6
    Text = 'Edit6'
  end
  object Edit7: TEdit
    Left = 152
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 7
    Text = 'Edit7'
  end
end
