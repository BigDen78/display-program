{$I DEF.INC}

unit RecConfigUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, MyTypes, IniFiles;

type
  TRecConfigForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    ScrollBox1: TScrollBox;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label11: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Bevel4: TBevel;
    Bevel7: TBevel;
    Bevel8: TBevel;
    Label1: TLabel;
    Bevel11: TBevel;
    Bevel12: TBevel;
    Label2: TLabel;
    Bevel1: TBevel;
    Label4: TLabel;
    Label5: TLabel;
    SpeedEdit: TEdit;
    Ch0MinEdit: TEdit;
    Ch0MaxEdit: TEdit;
    Ch1MinEdit: TEdit;
    Ch1MaxEdit: TEdit;
    Ch2MinEdit: TEdit;
    Ch4MinEdit: TEdit;
    Ch2MaxEdit: TEdit;
    Ch4MaxEdit: TEdit;
    Ch6MinEdit: TEdit;
    Ch6MaxEdit: TEdit;
    Ch8MinEdit: TEdit;
    Ch8MaxEdit: TEdit;
    UseMaxSpeed: TCheckBox;
    UseKuZones: TCheckBox;
    CheckBox1: TCheckBox;
    CheckBox2_: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox2: TCheckBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label6: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBoxClick(Sender: TObject);
    procedure EditChange(Sender: TObject);
  private
    SkipFlg: Boolean;

    procedure ControlToConfig(Sender: TObject);
    procedure ConfigToControl(Sender: TObject);
  public
    { Public declarations }
  end;

  TRecConfig = class
  protected
    FConfig: TINIFile;
    FConfigFileName: string;
  private
  public
    UseMaxSpeed: Boolean;
    MaxSpeed: Single;
    UseKuZones: Boolean;
    KuZones: array [1..6] of TMinMax;
//    SearchInBJ: Boolean;
//    SearchInSw: Boolean;
//    DoublePass: Boolean;
//    BJButton: Boolean;
//    EndWidth: Integer;              // ������ ���� ����� ��������� �� ���������� � ����� � ��
//    OnePBLen: Integer;
//    SomePBLen: Integer;
//    Ch0PBLen: Integer;

    SummZone: Integer;              // ���� ���������� ����� (��)
    Ch0OnePBLen: Integer;           // ����� �� ������ ������ 0 ������ (���� ��� ����)
    LenTh: array [1..8] of Integer; // ����� �� ��������� ������ ����� � ����������� �� ���������� ����������� ������� [��]
                                    // ���� � ����� ������ ��������� ����� �� ������� ����� ������������ ������

    BSPiceMinSize: Integer;         // ����������� ������ ���������� ����� � �� (� �������� ��)
    BSHoleTh: Integer;              // ������������ ����� ��������� �� � ���� ���������� �� (� ��������) ��� ������ ����� � ��
    BSHoleSkip: Integer;            // ���������� ����� � ������ ����� (� ��������)

    BSHoleMinSize: Integer;         // ����������� ������ ���� ���������� �� (� ��������)
    PBCountAmpl: array [2..4] of Integer;

    constructor Create(ConfigFileName: string = '');
    destructor Destroy; override;
    procedure Load;
    procedure Save;
  end;


var
  RecConfigForm: TRecConfigForm;
  RecConfig: TRecConfig;

implementation

{$R *.dfm}

procedure TRecConfigForm.Button1Click(Sender: TObject);
begin
  ModalResult:= mrOk;
end;

procedure TRecConfigForm.ControlToConfig(Sender: TObject);
var
  I: Integer;
  Code: Integer;
  Res: Single;

function MyStrToInt(Str: string): Integer;
begin
  Val(Str, Result, Code);
end;

begin
  if SkipFlg then Exit;
  SkipFlg:= True;

  RecConfig.KuZones[1].Min:= MyStrToInt(Ch0MinEdit.Text);
  RecConfig.KuZones[1].Max:= MyStrToInt(Ch0MaxEdit.Text);
  RecConfig.KuZones[2].Min:= MyStrToInt(Ch1MinEdit.Text);
  RecConfig.KuZones[2].Max:= MyStrToInt(Ch1MaxEdit.Text);
  RecConfig.KuZones[3].Min:= MyStrToInt(Ch2MinEdit.Text);
  RecConfig.KuZones[3].Max:= MyStrToInt(Ch2MaxEdit.Text);
  RecConfig.KuZones[4].Min:= MyStrToInt(Ch4MinEdit.Text);
  RecConfig.KuZones[4].Max:= MyStrToInt(Ch4MaxEdit.Text);
  RecConfig.KuZones[5].Min:= MyStrToInt(Ch6MinEdit.Text);
  RecConfig.KuZones[5].Max:= MyStrToInt(Ch6MaxEdit.Text);
  RecConfig.KuZones[6].Min:= MyStrToInt(Ch8MinEdit.Text);
  RecConfig.KuZones[6].Max:= MyStrToInt(Ch8MaxEdit.Text);

//  RecConfig.OnePBLen:= MyStrToInt(Edit1.Text);
//  RecConfig.SomePBLen:= MyStrToInt(Edit2.Text);
//  RecConfig.Ch0PBLen:= MyStrToInt(Edit3.Text);

  Val(SpeedEdit.Text, Res, Code);
  if Code = 0 then RecConfig.MaxSpeed:= Res;

  RecConfig.UseMaxSpeed:= UseMaxSpeed.Checked;
  RecConfig.UseKuZones:= UseKuZones.Checked;

//  RecConfig.SearchInBJ:= CheckBox3.Checked;
//  RecConfig.SearchInSw:= CheckBox4.Checked;
//  RecConfig.DoublePass:= CheckBox1.Checked;
//  RecConfig.BJButton:= CheckBox2.Checked;

  SkipFlg:= False;
end;

procedure TRecConfigForm.ConfigToControl(Sender: TObject);
begin
  if SkipFlg then Exit;
  SkipFlg:= True;

  UseMaxSpeed.Checked:= RecConfig.UseMaxSpeed;
  UseKuZones.Checked:= RecConfig.UseKuZones;

//  CheckBox3.Checked:= RecConfig.SearchInBJ;
//  CheckBox4.Checked:= RecConfig.SearchInSw;
//  CheckBox1.Checked:= RecConfig.DoublePass;
//  CheckBox2.Checked:= RecConfig.BJButton;

  Ch0MinEdit.Text:= IntToStr(RecConfig.KuZones[1].Min);
  Ch0MaxEdit.Text:= IntToStr(RecConfig.KuZones[1].Max);
  Ch1MinEdit.Text:= IntToStr(RecConfig.KuZones[2].Min);
  Ch1MaxEdit.Text:= IntToStr(RecConfig.KuZones[2].Max);
  Ch2MinEdit.Text:= IntToStr(RecConfig.KuZones[3].Min);
  Ch2MaxEdit.Text:= IntToStr(RecConfig.KuZones[3].Max);
  Ch4MinEdit.Text:= IntToStr(RecConfig.KuZones[4].Min);
  Ch4MaxEdit.Text:= IntToStr(RecConfig.KuZones[4].Max);
  Ch6MinEdit.Text:= IntToStr(RecConfig.KuZones[5].Min);
  Ch6MaxEdit.Text:= IntToStr(RecConfig.KuZones[5].Max);
  Ch8MinEdit.Text:= IntToStr(RecConfig.KuZones[6].Min);
  Ch8MaxEdit.Text:= IntToStr(RecConfig.KuZones[6].Max);

//  Edit1.Text:= IntToStr(RecConfig.OnePBLen);
//  Edit2.Text:= IntToStr(RecConfig.SomePBLen);
//  Edit3.Text:= IntToStr(RecConfig.Ch0PBLen);


  SpeedEdit.Text:= Format('%3.2f', [RecConfig.MaxSpeed]);
  SkipFlg:= False;
end;


procedure TRecConfigForm.FormShow(Sender: TObject);
begin
  ConfigToControl(nil);
end;

procedure TRecConfigForm.EditChange(Sender: TObject);
begin
  ControlToConfig(nil);
end;

procedure TRecConfigForm.CheckBoxClick(Sender: TObject);
begin
  ControlToConfig(nil);
end;

// -------- TRecConfig ---------------------------------------------------------

constructor TRecConfig.Create(ConfigFileName: string = ''); // �������� �������
begin
  if ConfigFileName = '' then FConfigFileName:= ExtractFilePath(ParamStr(0)) + 'rec.ini' // ��� � ���� INI �����
                         else FConfigFileName:= ConfigFileName;
  FConfig:= TINIFile.Create(FConfigFileName);
end;

destructor TRecConfig.Destroy; // �������� �������
begin
  FConfig.Free;
end;

procedure TRecConfig.Load;
begin
  DecimalSeparator:= '.'; // ��������� ��������������� ������� ��� �������. �����

  KuZones[1].Min:= FConfig.ReadInteger('Settings', 'Ch0Min', 14);
  KuZones[1].Max:= FConfig.ReadInteger('Settings', 'Ch0Max', 20);
  KuZones[2].Min:= FConfig.ReadInteger('Settings', 'Ch1Min', 12);
  KuZones[2].Max:= FConfig.ReadInteger('Settings', 'Ch1Max', 18);
  KuZones[3].Min:= FConfig.ReadInteger('Settings', 'Ch2Min', 10);
  KuZones[3].Max:= FConfig.ReadInteger('Settings', 'Ch2Max', 16);
  KuZones[4].Min:= FConfig.ReadInteger('Settings', 'Ch3Min', 14);
  KuZones[4].Max:= FConfig.ReadInteger('Settings', 'Ch3Max', 20);
  KuZones[5].Min:= FConfig.ReadInteger('Settings', 'Ch4Min', 12);
  KuZones[5].Max:= FConfig.ReadInteger('Settings', 'Ch4Max', 18);
  KuZones[6].Min:= FConfig.ReadInteger('Settings', 'Ch5Min', 14);
  KuZones[6].Max:= FConfig.ReadInteger('Settings', 'Ch5Max', 20);

  MaxSpeed:= FConfig.ReadFloat('Settings', 'MaxSpeed', 4);
  UseMaxSpeed:= FConfig.ReadBool('Settings', 'UseMaxSpeed', True);
  UseKuZones:= FConfig.ReadBool('Settings', 'UseKuZones', True);

//  SearchInBJ:= FConfig.ReadBool('Settings', 'SearchInBJ', True);
//  SearchInSw:= FConfig.ReadBool('Settings', 'SearchInSw', True);
//  DoublePass:= FConfig.ReadBool('Settings', 'DoublePass', True);
//  BJButton:= FConfig.ReadBool('Settings', 'BJButton', True);
//  OnePBLen:= FConfig.ReadInteger('Settings', 'OnePBLen',  Round(9 / 1.8));
//  SomePBLen:= FConfig.ReadInteger('Settings', 'SomePBLen', Round(9 / 1.8));
//  Ch0PBLen:= FConfig.ReadInteger('Settings', 'Ch0PBLen',  Round(30 / 1.8));
//  EndWidth:= 140; // �� - ���� ���������

  Ch0OnePBLen:= 50;

  LenTh[1]:= 15;
  LenTh[2]:= 5;
  LenTh[3]:= 3;
  LenTh[4]:= 3;
  LenTh[5]:= 2;
  LenTh[6]:= 2;
  LenTh[7]:= 2;
  LenTh[8]:= 1;

  BSPiceMinSize:= 3;  // ����������� ������ ����� �� (� ��������) ��� ������� ������� ������ ����� ��
  BSHoleMinSize:= 5;  // ����������� ������ ���� ���������� �� (� ��������)
  BSHoleTh:= 3;       // ������������ ����� ��������� �� � ���� ���������� �� (� ��������)
  BSHoleSkip:= 75;    // ���������� ����� � ������ ����� (� ��������)

  SummZone:= 70;      // ���� ���������� ����� (��)

  PBCountAmpl[2]:= 26;
  PBCountAmpl[3]:= 22;
  PBCountAmpl[4]:= 18;

(*
  CountAmpl: array [1..7, 2..7] of Integer = (
                                 // ����������: 2   3   4   5   6   7
                         // ������� ���������: 15  42  13  12  11  10
                                              (30, 42, 52, 60, 66, 70),  // ����� - 1
                                 // ����������: 2   3   4   5   6   7
                         // ������� ���������: 14  13  12  10   8   6    // ����� - 2..7
                                              (28, 39, 48, 50, 48, 42),  // ����� - 2
                                              (28, 39, 48, 50, 48, 42),  // ����� - 3
                                              (28, 39, 48, 50, 48, 42),  // ����� - 4
                                              (28, 39, 48, 50, 48, 42),  // ����� - 5
                                              (28, 39, 48, 50, 48, 42),  // ����� - 6
                                              (28, 39, 48, 50, 48, 42)); // ����� - 7 *)


end;

procedure TRecConfig.Save;
var
  I, J: Integer;
  S: string;

begin
  try
    FConfig.WriteInteger('Settings', 'Ch0Min', KuZones[1].Min);
    FConfig.WriteInteger('Settings', 'Ch0Max', KuZones[1].Max);
    FConfig.WriteInteger('Settings', 'Ch1Min', KuZones[2].Min);
    FConfig.WriteInteger('Settings', 'Ch1Max', KuZones[2].Max);
    FConfig.WriteInteger('Settings', 'Ch2Min', KuZones[3].Min);
    FConfig.WriteInteger('Settings', 'Ch2Max', KuZones[3].Max);
    FConfig.WriteInteger('Settings', 'Ch3Min', KuZones[4].Min);
    FConfig.WriteInteger('Settings', 'Ch3Max', KuZones[4].Max);
    FConfig.WriteInteger('Settings', 'Ch4Min', KuZones[5].Min);
    FConfig.WriteInteger('Settings', 'Ch4Max', KuZones[5].Max);
    FConfig.WriteInteger('Settings', 'Ch5Min', KuZones[6].Min);
    FConfig.WriteInteger('Settings', 'Ch5Max', KuZones[6].Max);

    FConfig.WriteFloat('Settings', 'MaxSpeed', MaxSpeed);
    FConfig.WriteBool('Settings', 'UseMaxSpeed', UseMaxSpeed);
    FConfig.WriteBool('Settings', 'UseKuZones', UseKuZones);

//    FConfig.WriteBool('Settings', 'SearchInBJ', SearchInBJ);
//    FConfig.WriteBool('Settings', 'SearchInSw', SearchInSw);
//    FConfig.WriteBool('Settings', 'DoublePass', DoublePass);
//    FConfig.WriteBool('Settings', 'BJButton', BJButton);

//    FConfig.WriteInteger('Settings', 'OnePBLen', OnePBLen);
//    FConfig.WriteInteger('Settings', 'SomePBLen', SomePBLen);
//    FConfig.WriteInteger('Settings', 'Ch0PBLen', Ch0PBLen);

    FConfig.UpdateFile;
  except
  end;
end;

initialization

  {$IFDEF REC}
  RecConfig:= TRecConfig.Create;
  RecConfig.Load;
  {$ENDIF}

finalization

  {$IFDEF REC}
  RecConfig.Save;
  RecConfig.Free;
  {$ENDIF}

end.
