unit CustomDirectDrawControl;

interface

uses
  Windows, Classes, Graphics, Controls;

type

  TCustomDirectDrawControl = class(TCustomControl)
  private
  protected
    FTestColor1: DWord;
    FTestColor2: DWord;
    FResColor: DWord;
    FClipRect: TRect;
    FMixColor: Boolean;
    FLastTextOutX: Integer;
    FOnLostData: TNotifyEvent;

  public
    SelRect: TRect;
    DrawSelRect: Boolean;
    DrawLeftCur: Boolean;
    LeftCurTop: TPoint;
    LeftCurHeight: Integer;
    DrawRightCur: Boolean;
    RightCurTop: TPoint;
    RightCurHeight: Integer;
    DrawCentrCur: Boolean;
    CentrCurTop: TPoint;
    CentrCurHeight: Integer;
    NowInit: Boolean;
    StopWork: Boolean;

    procedure InitDirectDraw; virtual; abstract;
    procedure FinDirectDraw; virtual; abstract;
    procedure Chk(hr : HRESULT); virtual; abstract;
    function GetDDColor(Color: TColor): DWORD; virtual; abstract;
    procedure Clear(Color: DWORD); virtual; abstract;
    function LoadFont(FontName: string; FontHeight: Integer; BoldFont: Boolean; FontColor, BGColor: TColor; Vertical: Boolean = False; CreateSurface: Boolean = True): Integer; virtual; abstract;
    function ChangeFontPar(FontIdx: Integer; FontName: string; FontHeight: Integer; FontColor, BGColor: TColor; CreateSurface: Boolean = True): Integer; virtual; abstract;
    function AddPic(NewPic: TBitMap; Transparent: Boolean): Integer; virtual; abstract;
    procedure HLine(X, Y, Len, Width: Integer; Color: DWORD); virtual; abstract;
    procedure VLine(X, Y, Len, Width: Integer; Color: DWORD); virtual; abstract;
    procedure VDashLine(X, Y, Len, Width: Integer; Color: DWORD); virtual; abstract;
    procedure TextOut(FontIdx, X, Y, Align: Integer; Text: string; Transparent: Boolean); virtual; abstract;
    function TextWidth(FontIdx: Integer; Text: string): Integer; virtual; abstract;
    function TextHeight(FontIdx: Integer; Text: string): Integer; virtual; abstract;
    procedure DrawRect(Rct_: TRect; Width: Integer; Color: DWord); virtual; abstract;
    procedure DrawRect2(Rct_: TRect; Width: Integer; Color1, Color2: DWord); virtual; abstract;
    procedure DrawSelRect_(Rct_: TRect; Size, Width: Integer; Color: DWord); virtual; abstract;
    procedure DrawPic(X, Y, Idx: Integer); virtual; abstract;
    function GetPicSize(Idx: Integer): TSize; virtual; abstract;
    procedure FillRect(X1, Y1, X2, Y2: Integer; Color: DWORD); virtual; abstract;
    procedure FillRect_(Rt: TRect; Color: DWord); virtual; abstract;
    procedure FillRgn(Rt: TRect; Idx: Integer); virtual; abstract;
    procedure Shift(WorkArea: TRect; Shift: Integer); virtual; abstract;
    procedure StartP; virtual; abstract;
    procedure EndP; virtual; abstract;
    procedure FastPoint(X, Y, DX, DY, Color: Integer); virtual; abstract;
    procedure SetPoint(X, Y, DX, DY, Color: Integer); virtual; abstract;
    procedure Flip; virtual; abstract;
    procedure GetBitMap(var Res: TBitMap); virtual; abstract;
    procedure SetClipRect(New: TRect); virtual; abstract;
    procedure RestoreAllSurface; virtual; abstract;

    property TestColor1: DWord read FTestColor1 write FTestColor1;
    property TestColor2: DWord read FTestColor2 write FTestColor2;
    property ResColor: DWord read FResColor write FResColor;
    property MixColor: Boolean read FMixColor write FMixColor;
    property ClipRect: TRect read FClipRect;
    property LastTextOutX: Integer read FLastTextOutX;

  published
    property OnLostData: TNotifyEvent read FOnLostData write FOnLostData;
    property Align;
    property Caption;
    property Color;
    property Enabled;
    property Font;
    property Visible;
    property OnClick;
    property OnDblClick;
    property OnEnter;
    property OnExit;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
  end;


implementation

end.
