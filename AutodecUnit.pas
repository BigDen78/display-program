unit AutodecUnit;

{$Define LoadDataAsync}

interface

uses
  Classes, Types, Windows, Generics.Collections, TB2Item, AviconDataSource,
  DisplayUnit, MyTypes,
  Graphics,
  Forms, AutodecDataUnit, GraphUtil, RecData, Controls,
  DefectWarningMessageUnit,
  ConfigUnit, ElementsCollection, Avk11ViewUnit, AutodecTypes,
  AutodecDrawerUnit;

type

  // ����� �������
  TCalculateThread = class(TThread)
  public
    constructor Create;
  protected
    procedure Execute; override;
  private
    fileName: string;
    error: string;
    calcScreenMode: Boolean; // ����� ������� ������ � �������� ������
    procedure OnTerminateEvent(sender: TObject);
    procedure ShowProcessFailMessage();
    procedure LoadChannelsData();
  end;

type
  TProcRef = reference to procedure;
  TDataReadyHandler = procedure;

  // ����� �������� ����� �������� (��� ��������� ������� ������)
  TDelayBeforeCalculatingThread = class(TThread)
  public
    FFileName: string; // ��� �����, ��� �������� ����� �������� ������
    startCalculatingProc: TProcRef;
  protected
    procedure Execute; override;
  end;

  TAutodec = class
  private
    FDisplay: TAvk11Display; // �c���� �� ����� �����������
    FDatSrc: TAvk11DatSrc; // �c���� �� ����� ������
    FDatSrcOld: TAvk11DatSrc; // �c���� �� ����� ������
    drawer: TAutodecDrawer; // ����� ��������� ���������

    FFileName: string; // ��� �����
    FId: Integer; // ������������� ������
    MenuItemsList: TList<TTBCustomItem>; // ����
    // ����� �������
    calculateThread: TCalculateThread;
    // ����� �������� ����� �������� (��� ��������� ������� ������)
    delayThread: TDelayBeforeCalculatingThread;
    // ����� ��� ��������� ������ �� ���������� ���������
    autodecData: TAutodecData;
    // ����� ��������������
    warningForm: TDefectWarningMessageForm;
    // �������� ��������� ������� ��-�� ������ ����������
    badCoordinateOffset: Integer;
    // �����, ��� ������� ���� ��������� ��������� �������
    badCoordinateOffsetSkipBackMotion: bool;
    // ����� ��� ������ ������ ���������
    elementsCollectionForm: TElementsCollectionForm;

    procedure calcBadCoordinateOffset(force: bool);

    // ������� ���������
    procedure Draw(Disp: TAvk11Display; Mode: Integer);
    // ������� ��������� ��������� ������
    procedure DrawTape(Disp: TAvk11Display; Rail: RRail; OutRect: TRect;
      StartDisCoord, EndDisCoord, Index: Integer);
    // ������������ ����������� �������� ������
    procedure showElements(Sender: TObject);
    // ������������ ����������� ������ ���������
    procedure showElementsCollection(Sender: TObject);
    // ������������ ����������� ����������� ��������
    procedure showReflections(Sender: TObject);
    // ����� ����
    procedure showMenu(Sender: TTBCustomItem; FromLink: Boolean);
    // ���������� ����� ��������� ��������� �������� �������
    procedure fillElementsForm();
    // ���������� ������� ����� ��������� ��������� �������� �������
    function fillElementsArray(var ar: TRecRes): Boolean;
    // ����������� ����� ��������������
    procedure showWarningForm();
    // ������ ������ �������
    procedure startCalculateThread();
    // ������ ������ �������� ������� � ������ ������
    procedure startDelayThread(fileData: TAutodecFileData);

    procedure setViewArea();
    procedure startCalcIfNeed();
    procedure ReopenMenu;

  public
    MenuItem: TTBCustomItem;

    constructor Create;
    destructor Destroy; override;
    procedure OnSysClick(Sender: TObject);
    // ��������� ����: �������� ��� / ����� ���������
    function Exists: Boolean; // ������� ����������
    procedure SetActiveFormData(Display: TAvk11Display; DatSrc: TAvk11DatSrc);
    // �������� ������� ������
    procedure loadReadyData();
    procedure clearUnusedData();
    // ���������� ������� ��������������
    function getClassifierItem(classId: integer): TClassifierItem;
    // ��������� ��������� ����� �������� ��� ��������� ��������
    procedure setSkipBackMotion(skip: boolean);

    // ����� ��������� ������ ����������
    function getLibraryLastError(): string;
    // ������ ����������
    function getLibraryVersion(): string;

  end;

var
  libPrefix: string = 'ar';
  libName: string = 'ar.dll';

  startFunctionName: string = 'version';
  readyAutodecData: bool = false;

  // ������ ������������ �������
function showedClasses(): TIntegerArray;
// �������� ����������� ���� �� ������ ������������� ������
function elementsShowed(): Boolean;

// ��������� ��������� �� �������������� ����� ����������
function getLibFunction(funcName: string; showError: boolean = True): Pointer;
// ��������� ��������� ����������� ������
procedure dataReadyHandler;
// ������ ���� ������ �������
procedure ClearChannelsDataCache;

implementation

uses
  SysUtils, Dialogs, Math, MainUnit, SimpleXML, cUnicodeCodecs, AviconTypes;
type
  Dict = TDictionary<Integer, Boolean>;

function showedClasses(): TIntegerArray;
var
  pair: TPair<Integer, Boolean>;
  length: Integer;
begin
  length := 0;
  for pair in Config.autodecShow do
  begin
    if pair.Value then
    begin
      SetLength(Result, length + 1);
      Result[length] := pair.Key;
      length := length + 1;
    end;
  end;
end;

function elementsShowed(): Boolean;
var
  show: Boolean;
begin
  for show in Config.autodecShow.Values do
  begin
    if show then
    begin
      Result := true;
      Exit;
    end;
  end;
  Result := False;
end;

function getLibFunction(funcName: string; showError: boolean): Pointer;
var
  handle: THandle;

begin
  SetCurrentDir(libPrefix);
  handle := LoadLibrary(PChar(libName));
  SetCurrentDir('..');
  Result := GetProcAddress(handle, PChar(funcName));
  if (Result = nil) and showError then
  begin
    ShowMessage(Format('�� ������� �������� ����� ������� "%s" ����������� "%s"',
      [funcname, libName]));
  end;
end;

procedure dataReadyHandler;
begin
  Rostov.loadReadyData();
end;

procedure setCursor(cursor: TCursor);
begin
  if Assigned(Rostov.FDisplay) then
    Rostov.FDisplay.Cursor := cursor;
end;

function CrdStateToStr(set_: TCrdState): string;
begin
  result := '';
  if csOnScreen in set_ then
    result := result + 'csOnScreen ';
  if csOffScreen in set_ then
    result := result + 'csOffScreen ';
  if csMoved in set_ then
    result := result + 'csMoved ';
  if csLeftMoved in set_ then
    result := result + 'csLeftMoved ';
  if csRightMoved in set_ then
    result := result + 'csRightMoved ';
end;

function getTapeByChannel(channel: Integer): Integer;
begin
  Result := ChList[channel].OutLine;
end;

procedure swap(var a: Integer; var b: Integer);
var
  temp: Integer;
begin
  temp := a;
  a := b;
  b := temp;
end;

// ****************************************************
// ******************** TCalculateThread **************
// ****************************************************

constructor TCalculateThread.Create;
begin
  inherited Create(FALSE);
  Priority := tpLower;
  OnTerminate := OnTerminateEvent;
  calcScreenMode := false;
end;

procedure TCalculateThread.Execute;
var
  ViewForm: TAvk11ViewForm;
begin
  setCursor(crAppStart);
  LoadChannelsData;
  Synchronize(Rostov.setViewArea);
  ReturnValue := 1;
end;

procedure TCalculateThread.ShowProcessFailMessage;
begin
  ShowMessage('������ ������� ���������������:' + AnsiString(#13#10) + error);
end;

procedure TCalculateThread.LoadChannelsData;
var
  progress: Integer;
  ready: Boolean;
  status: string;
  ViewForm: TAvk11ViewForm;
  fileData: TAutodecFileData;
  freeData: procedure; cdecl;

begin
  fileName := Rostov.FFileName;
  fileData := Rostov.autodecData.fileData(fileName);
  if fileData.ready or not elementsShowed() then
  begin
    Exit;
  end;

  ViewForm := Rostov.FDisplay.Parent as TAvk11ViewForm;
  ViewForm.SetProgressBarPosition(0, False);
  ViewForm.SetProgressBarCaption('���������� � �������');
  Synchronize(ViewForm.UpdateProgressBar);
  @freeData := getLibFunction('freeData');

  if fileData.calcScreenMode then
  begin
    Rostov.autodecData.setProcessedCoords(fileData.lastDisCoords);
  end;
  Rostov.autodecData.startProcess(fileName);
  progress := 0;
  ready := False;
  while not ready do
  begin
    Sleep(200);
    // ���� ������� ��� ���� - ���� �������, ���������� �������� ������ � ���������� ���� ���������
    if (fileData.fileName = '') then
    begin
      freeData;
      exit;
    end;
    progress := Rostov.autodecData.prcProgress(ready, status);
    ViewForm.SetProgressBarPosition(Round(0.95 * progress + 5), False);
    ViewForm.SetProgressBarCaption(status);
    Synchronize(ViewForm.UpdateProgressBar);
  end;
  error := Rostov.getLibraryLastError();
  if (error <> '') then
  begin
    error := Rostov.getLibraryLastError();
    if (not fileData.calcScreenMode) then
    begin
      Synchronize(ShowProcessFailMessage);
    end;
  end;
  {$IfNDef LoadDataAsync}
  Rostov.autodecData.loadData(fileName, ViewForm.DatSrc);
  {$EndIf}
  freeData;
  // ����������� �������� ������ ���� ���� ������������� ���������
  if not fileData.calcScreenMode then
  begin
    ViewForm.SetProgressBarPosition(0, True);
  end
  else
  begin
    ViewForm.SetProgressBarCaption('������ ������ ��������');
  end;
  Synchronize(ViewForm.UpdateProgressBar);
  readyAutodecData := true;
end;

procedure TCalculateThread.OnTerminateEvent(sender: TObject);
begin
  setCursor(crDefault);
end;

// ****************************************************
// ******************** TCalculateThread **************
// ****************************************************

procedure TDelayBeforeCalculatingThread.Execute;
var
  i: Integer;
  delayStep: Integer;
begin
  delayStep := 100;
  i := 0;
  while (i < 1000) and (not Terminated) do
  begin
    i := i + delayStep;
    Sleep(delayStep);
  end;
  if not Terminated then
  begin
    startCalculatingProc;
  end;
  ReturnValue := 1;
end;

// ****************************************************
// ******************** TAutodec ********************
// ****************************************************

constructor TAutodec.Create;
var
  tmp: TTBCustomItem;
  classId: Integer;
  classItem: TClassifierItem;
  actualShowedClasses: TAutodecShow;
  setDataReadyHandlerFunc: procedure(handler: TDataReadyHandler); cdecl;
begin
  if not Exists() then
  begin
    Exit;
  end;

  MenuItem := TTBCustomItem.Create(nil);
  MenuItem.Caption := '��������� �����';
  MenuItemsList := TList<TTBCustomItem>.Create; // ��������� ������� ����

  MenuItem.OnPopup := showMenu;

  tmp := TTBCustomItem.Create(nil);
  tmp.Caption := '������ ���������� ��������';
  tmp.Checked := Config.isElementsCollectionShow;
  tmp.OnClick := showElementsCollection;
  MenuItemsList.Add(tmp);
  MenuItem.Add(tmp);

  MenuItem.Add(TTBSeparatorItem.Create(nil));

  MenuItem.Add(TTBSeparatorItem.Create(nil));
  tmp := TTBCustomItem.Create(nil);
  tmp.Caption := '��������� ����� ������';
  tmp.Checked := Config.isReflectionsShow;
  tmp.OnClick := showReflections;
  MenuItemsList.Add(tmp);
  MenuItem.Add(tmp);

  MenuItem.Add(TTBSeparatorItem.Create(nil));

  tmp := TTBCustomItem.Create(nil);
  tmp.Caption := '�������� ��';
  tmp.OnClick := OnSysClick;
  tmp.Tag := -1;
  MenuItemsList.Add(tmp);
  MenuItem.Add(tmp);

  tmp := TTBCustomItem.Create(nil);
  tmp.Caption := '����� ���������';
  tmp.OnClick := OnSysClick;
  tmp.Tag := -2;
  MenuItemsList.Add(tmp);
  MenuItem.Add(tmp);

  autodecData := TAutodecData.Create;
  // � ��� ��������� ����������� ������ ������ �� ������ - ��� ���������� ������� ������� � ������������,
  // �.�. ���������� ������� ������� ������ � ������ ��������� ������ ��� ������� ��������� ���������
  actualShowedClasses := TAutodecShow.Create;
  // ������������ ������� ����
  for classId in autodecData.classIdOrdered do
  begin
    classItem := autodecData.classList[classId];
    tmp := TTBCustomItem.Create(nil);
    tmp.Caption := classItem.Name;
    if Config.autodecShow.ContainsKey(classItem.id) then
    begin
      tmp.Checked := Config.autodecShow[classItem.id];
    end
    else
    begin
      tmp.Checked := False;
    end;
    actualShowedClasses.Add(classItem.id, tmp.Checked);

    tmp.OnClick := showElements;
    tmp.Tag := classItem.id;
    MenuItemsList.Add(tmp);
    MenuItem.Add(tmp);
  end;
  // ������ ������ � ������� �� ����������
  Config.autodecShow := actualShowedClasses;
  calculateThread := nil;
  delayThread := TDelayBeforeCalculatingThread.Create(True);
  // �������� ����� ��������� ���������
  elementsCollectionForm := TElementsCollectionForm.Create(nil);
  drawer := TAutodecDrawer.Create(autodecData);
   // ������� ��������� ������ �������� ������� ������
   {$IfDef LoadDataAsync}
   @setDataReadyHandlerFunc := getLibFunction('setDataReadyHandler');
   setDataReadyHandlerFunc(dataReadyHandler);
   {$EndIf}
end;

destructor TAutodec.Destroy;
var
  I: Integer;

begin
  if MenuItemsList <> nil then
  begin
    for I := 0 to MenuItemsList.Count - 1 do
    begin
      MenuItemsList[I].Free;
    end;
    MenuItemsList.Free;
  end;

  if MenuItem <> nil then
  begin
    MenuItem.Free;
  end;

  if autodecData <> nil then
  begin
    autodecData.Free;
  end;

  if drawer <> nil then
  begin
    drawer.Free;
  end;
end;

procedure TAutodec.OnSysClick(Sender: TObject);
var
  classId: Integer;
  item: TTBCustomItem;
  calc: boolean;
begin
  case TTBItem(Sender).Tag of
    -1:
      begin
        calc := false;
        for item in MenuItemsList do
        begin
          classId := item.Tag;
          if classId > 0 then
          begin
            item.Checked := True;
            if not Config.autodecShow[classId] then
            begin
              calc := true;
              Config.autodecShow[classId] := true;
            end;
          end;
        end;

        if calc then
        begin
          startCalcIfNeed();
        end;
      end;
    -2:
      begin
        for item in MenuItemsList do
        begin
          classId := item.Tag;
          if classId > 0 then
          begin
            item.Checked := false;
            Config.autodecShow[classId] := false;
          end;
        end;
      end;
  end;
  setViewArea();
  elementsCollectionForm.UpdateList();
  ReopenMenu;
end;

// �������� �� ������� ����������

function TAutodec.Exists: Boolean;
begin
  result := nil <> getLibFunction(startFunctionName, False);
end;

procedure TAutodec.setViewArea();
const
  List: array[0..1] of Integer = (15, 65);
  // ���������� � ������ (� ������) �������������� ��� ������
var
  area: array[0..1] of Integer;
begin
  if not Assigned(FDisplay) then
    Exit;
  area[0] := 0;
  area[1] := 0;
  calcBadCoordinateOffset(true);
  FDisplay.SetExtendedView(area);
  // ��������� ���������� � ������� �������������� ��� ������
  FDisplay.Refresh();

  if Config.isElementsCollectionShow then
    fillElementsForm();
end;

procedure TAutodec.showElements(Sender: TObject);
var
  item: TTBCustomItem;
  classId: Integer;
begin
  item := Sender as TTBCustomItem;
  classId := item.Tag;
  Config.autodecShow[classId] := not Config.autodecShow[classId];
  item.Checked := Config.autodecShow.items[classId];
  if Config.autodecShow.items[classId] then
  begin
    startCalcIfNeed()
  end;
  setViewArea();
  elementsCollectionForm.UpdateList();
  showMenu(item, true);
  ReopenMenu;
end;

procedure TAutodec.showReflections(Sender: TObject);
var
  item: TTBCustomItem;
begin
  item := Sender as TTBCustomItem;
  Config.isReflectionsShow := not Config.isReflectionsShow;
  item.Checked := Config.isReflectionsShow;
  setViewArea();

  ReopenMenu;
end;

procedure TAutodec.showElementsCollection(Sender: TObject);
var
  item: TTBCustomItem;

begin
  if (FDisplay = nil) then
    Exit;

  item := Sender as TTBCustomItem;
  Config.isElementsCollectionShow := not Config.isElementsCollectionShow;
  item.Checked := Config.isElementsCollectionShow;
  if Config.isElementsCollectionShow then
  begin
    fillElementsForm();
    elementsCollectionForm.ShowForm();
  end
  else
    elementsCollectionForm.HideForm();

  ReopenMenu;
end;

procedure TAutodec.ReopenMenu;
var
  Pt: TPoint;

begin
{* ��������������� ��������� 11.10.2018, ��������� � ����� ����� ���� ������ - �������� ��������: �������, �� ��������� ��� ���������� ������
  Pt := MainForm.MainToolbar.ClientToScreen(Point(MainForm.TopographTBRect.Left,
    MainForm.TopographTBRect.Bottom));
  MenuItem.Popup(Pt.X, Pt.Y, true);
  *}
end;

procedure TAutodec.showMenu(Sender: TTBCustomItem; FromLink: Boolean);
// ����� ����
begin
  //  if MainForm.ActiveViewForm <> MainForm.LastSetROSTOVActiveViewForm then
  begin
    MainForm.LastSetROSTOVActiveViewForm := MainForm.ActiveViewForm;
    ROSTOV.SetActiveFormData(MainForm.ActiveViewForm.Display,
      MainForm.ActiveViewForm.DatSrc);
  end;
end;

procedure TAutodec.fillElementsForm();
var
  engine: TEngine;
begin
  if ((FDatSrc = FDatSrcOld) and (not readyAutodecData)) then
    exit;

  engine := TEngine.Create();
  engine.FDatSrc := FDatSrc;

  if not fillElementsArray(engine.Res) then
    Exit;

  elementsCollectionForm.SetData(engine, MainForm.ActiveViewForm());
  elementsCollectionForm.UpdateList();

  if length(engine.Res) > 0 then
  begin
    FDatSrcOld := FDatSrc;
    readyAutodecData := false;
  end;
end;

procedure TAutodec.setSkipBackMotion(skip: boolean);
begin
  if Config.isElementsCollectionShow then
  begin
    FDatSrcOld := nil;
    fillElementsForm;
  end;
end;

function TAutodec.getClassifierItem(classId: integer): TClassifierItem;
begin
  result := autodecData.classList.Items[classId];
end;

function TAutodec.fillElementsArray(var ar: TRecRes): Boolean;
var
  el: TResItem;
  rail: RRail;
  i, j, count, startCoord, finishCoord: Integer;
  fileData: TAutodecFileData;
  channels: set of 0..255;
  first: Boolean;
  s: string;

  pair: TPair<Integer, Boolean>;
  classId: Integer;
  entity: TEntity;
  items: TEntityList;

  function isRailShowed(rail: RRail): boolean;
  begin
    if Fdisplay = nil then
      result := false
    else
      result :=
        (FDisplay.ViewMode = vmAllLines)
        or
        (
        (FDisplay.ViewMode in [vmOneRailLines, vmOneLine])
        and
        (Ord(FDisplay.ViewRail) = Ord(rail))
        );
  end;

begin
  result := False;
  // ��� ������������� ������ ����������� "� ���� ������" ��������� �� ��������������
  if (FDisplay = nil) or FDisplay.RailView then
    Exit;

  fileData := autodecData.readyFileData(FFileName);
  if (fileData = nil) or (fileData.fileName = '') then
    Exit;

  // ������, ��� ������� �������� �����������
  for pair in Config.autodecShow do
  begin
    classId := pair.key;
    if not fileData.entities.ContainsKey(classId) then
    begin
      continue;
    end;

    for rail := Low(fileData.entities[classId].items) to
      High(fileData.entities[classId].items) do
    begin
      items := fileData.entities[classId].items[rail];
      // ���� ��� ������ rail ��������� ��� ��� ��������� ����������� - �������
      if (items = nil) or (not isRailShowed(rail)) then
        continue;

      for entity in items do
      begin
        if entity.isDublicate then
          continue;

        if not Assigned(FDatSrc) then
          ShowMessage('FDatSrc not assigned !!!');

        if FDatSrc.SkipBackMotion then
        begin
          startCoord := entity.startSysCoordinate;
          finishCoord := entity.finishSysCoordinate;
        end
        else
        begin
          startCoord := entity.startDisCoordinate;
          finishCoord := entity.finishDisCoordinate;
        end;
        if startCoord > finishCoord then
          swap(startCoord, finishCoord);

        // ���������� ���������� ��������: ������ �������
        channels := [];
        for j := low(entity.areas) to High(entity.areas) do
          if (entity.areas[j].channelId >= 0) then
            include(channels, entity.areas[j].channelId);

        // ���� ����� �������� ���������� - ��������� � ��� � �����
        if (entity.channelId >= 0) then
          include(channels, entity.channelId);

        count := 0;
        for j := 0 to 255 do
          if j in channels then
            inc(count);

        el.Params := '';
        if count > 0 then
          if count > 1 then
            el.Params := el.Params + '������: '
          else
            el.Params := el.Params + '�����: ';

        first := true;
        for j := 0 to 255 do
          if j in channels then
          begin
            s := FDisplay.EvalChNumToName(j);
            if trim(s) = '' then
              s := IntToStr(j);

            if not first then
              el.Params := el.Params + ', ';
            first := false;

            el.Params := el.Params + s;
          end;

        // ���������� ��������� ����������
        el.classId := classId;
        el.R := rail;
        el.Bounds.StCrd := startCoord;
        el.Bounds.EdCrd := finishCoord;
        el.Len := finishCoord - startCoord;
        el.inHiddenZone := entity.inHiddenZone;
        setLength(ar, Length(ar) + 1);
        ar[High(ar)] := el;
      end;
    end;
  end;
  Result := True;
end;

procedure TAutodec.showWarningForm();
begin
  if not Config.DfShowWarningMessage then
    Exit;
  if warningForm = nil then
  begin
    warningForm := TDefectWarningMessageForm.Create(nil);
  end;
  warningForm.Show;
end;

function TAutodec.getLibraryLastError(): string;
var
  func: function(): PAnsiChar; cdecl;
begin
  @func := getLibFunction('error');
  Result := func()
end;

function TAutodec.getLibraryVersion(): string;
var
  func: function(): PAnsiChar; cdecl;
begin
  @func := getLibFunction('version');
  Result := func()
end;

procedure TAutodec.startCalculateThread();
begin
  if (FFileName = '') then
  begin
    Exit;
  end;
  SetCurrentDir(ExtractFileDir(Application.ExeName));
  if calculateThread = nil then
  begin
    calculateThread := TCalculateThread.Create;
  end
  else
  begin
    if calculateThread.ReturnValue = 1 then
    begin
      calculateThread.Terminate;
      calculateThread.WaitFor;
      calculateThread.Free;
      calculateThread := TCalculateThread.Create;
    end
  end;
end;

procedure TAutodec.startDelayThread(fileData: TAutodecFileData);
begin
  // ����� ������� � �������� ������
  if ((FDisplay.StartDisCoord < fileData.lastDisCoords.Min) or
    (FDisplay.EndDisCoord > fileData.lastDisCoords.Max)) or
    (FFileName <> delayThread.FFileName) then
  begin
    // ���� ������� �������� ��������� ����� �� ������� �������� ��������� ��� ���� ��������� - ���������� �������
    if not delayThread.Suspended then
    begin
      delayThread.Terminate;
      delayThread.WaitFor;
    end;
    delayThread.Free;
    delayThread := TDelayBeforeCalculatingThread.Create(False);
    delayThread.startCalculatingProc := startCalculateThread;
    delayThread.FFileName := FFileName;
    fileData.lastDisCoords.Min := FDisplay.StartDisCoord;
    fileData.lastDisCoords.Max := FDisplay.EndDisCoord;
    fileData.ready := False;
  end;
end;

procedure TAutodec.startCalcIfNeed();
var
  appended: Boolean;
  fileData: TAutodecFileData;
begin
  if (FFileName = '') or (not elementsShowed) then
  begin
    exit;
  end;

  fileData := autodecData.fileData(FFileName, appended);
  // ���� ���� ����� (�������� � ���������) � ��� ���� �� ������� ������� ������� ���� �����������
  if appended and not Rostov.autodecData.hasReadyResultFile(FFileName) then
  begin
    // �������� ������� ����� � ����������� ��������� � ������ ������
    fileData.isLarge := FDatSrc.DataFileSize > 20 * 1024 * 1024; // > 20 ��
    if fileData.isLarge then
    begin
      fileData.calcScreenMode :=
        MessageDlg(Format('���� ������� %s �������� ������� ����� ��������, ��������������� ����� ������ ��������������� �����. �������� ����� ��������� ������ ������������ �� ������ ����� �������?',
        [FFileName]), mtWarning, mbYesNo, 0) = mrYes;
    end;
  end;

  if fileData.calcScreenMode then
  begin
    startDelayThread(fileData);
  end
  else
  begin
    // ����� ������� ������� ����� - ������ �������, ���� ������ �� ��� �������� �����
    if not fileData.ready then
    begin
      startCalculateThread;
    end;
  end;

end;

procedure TAutodec.SetActiveFormData(Display: TAvk11Display; DatSrc:
  TAvk11DatSrc);
begin
  FDisplay := Display; // ���������� ���������� �� ����� �����������
  FDatSrc := DatSrc; // ���������� ���������� �� ����� ������
  badCoordinateOffset := 0;
  if Assigned(FDisplay) then // ���� �������� ���� � ��������������
  begin
    FId := DatSrc.Id; // ���������� �������������� ������
    FFileName := DatSrc.FileName; // ���������� ����� �����

    if Exists() then
    begin
      FDisplay.BeforeSgnExtDraw := Draw;
      // ��������� ������� ���������
      FDisplay.AfterSgnExtDraw := Draw;
      // ��������� ������� ��������� ��������� ������
      FDisplay.ExtendedViewDraw := DrawTape;
      startCalcIfNeed();
      fillElementsForm;
    end;
  end
  else
  begin
    elementsCollectionForm.clear();
    clearUnusedData();
    FDatSrcOld := nil;
  end;
end;

procedure TAutodec.loadReadyData();
var
  ViewForm: TAvk11ViewForm;
begin
  ViewForm := Rostov.FDisplay.Parent as TAvk11ViewForm;
  autodecData.loadData(FFileName, ViewForm.DatSrc);
  calculateThread.Synchronize(Rostov.setViewArea);
end;

procedure TAutodec.clearUnusedData();
var
  i, l: integer;
  form: TAvk11ViewForm;
  files: array of string;
begin
  for i := 0 to MainForm.MDIChildCount - 1 do
    if (MainForm.MDIChildren[I] is TAvk11ViewForm) then
    begin
      form := TAvk11ViewForm(MainForm.MDIChildren[I]);
      if Assigned(form.DatSrc) then
      begin
        l := length(files);
        setLength(files, l + 1);
        files[l] := form.DatSrc.FileName;
      end;
    end;
  autodecData.clearUnusedData(files);
end;

procedure TAutodec.calcBadCoordinateOffset(force: bool);
var
  i, startCoord: Integer;
begin
  if (FDatSrc.ErrorCount > 0) and (force or (badCoordinateOffsetSkipBackMotion
    <> FDatSrc.SkipBackMotion)) then
  begin
    badCoordinateOffset := 0;
    i := 0;
    startCoord := 0;
    while (i <= 5000) and (startCoord = 0) do
    begin
      i := i + 20;
      startCoord := FDatSrc.SysToDisCoord(i);
    end;
    if startCoord <> 0 then
      badCoordinateOffset := i - startCoord;
    badCoordinateOffsetSkipBackMotion := FDatSrc.SkipBackMotion;
  end;
end;

// ****************************************************
// ******************** TAutodec.Draw *****************
// ****************************************************

procedure TAutodec.Draw(Disp: TAvk11Display; Mode: Integer);
var
  fileData: TAutodecFileData;
  railShowed: array[RRail] of Boolean;
  rail: RRail;

  procedure drawEntities();
  var
    i: Integer;
    classes: TIntegerArray;
    classId: Integer;
    entity: TEntity;
    rail: RRail;
    items: TEntityList;
  begin
    // ������, ��� ������� �������� �����������
    classes := showedClasses();
    // �������������� ����������� �� ��������
    for i := Low(classes) to High(classes) do
    begin
      classId := classes[i];
      if (not fileData.entities.ContainsKey(classId)) then
      begin
        continue;
      end;

      for rail := Low(fileData.entities[classId].items) to
        High(fileData.entities[classId].items) do
      begin
        items := fileData.entities[classId].items[rail];
        // ���� ��� ������ rail ��������� ��� ��� ��������� ����������� - �������
        if (items = nil) or (not railShowed[rail]) then
        begin
          continue;
        end;
        for entity in items do
        begin
          drawer.drawEntity(@entity);
        end;
      end;

    end;

  end;

begin
  // ���� �� ����� ����� ������ ������� - �����
  if not Assigned(FDatSrc) or (FFileName = '') then
  begin
    Exit;
  end;

  // �������� �������, ����� ��������� �� ��������������
  if
    Disp.RailView // � ������ ����������� "� ���� ������"
  or
    (Mode <> 1) {// � ������, �������� �� "������ ��������"} then
  begin
    Exit;
  end;

  fileData := autodecData.fileData(FFileName);
  if (fileData <> nil) and (fileData.calcScreenMode) then
  begin
    startDelayThread(fileData);
  end
  else
  begin
    if (fileData = nil) or (not fileData.ready and elementsShowed()) then
    begin
      startCalcIfNeed();
      Exit;
    end;
  end;

  calcBadCoordinateOffset(false);
  drawer.init(FDisplay, FDatSrc, badCoordinateOffset);

  // ������ ��������� ��������� �� �������
  for rail := r_Left to r_Right do
  begin
    railShowed[rail] :=
      (FDisplay.ViewMode = vmAllLines)
      // ������ !!! - ����: ddRightRail, ddRightTape1, ddRightTape2, ddRightTape3, ddRightTape4 - �����: vmOneRailLines, vmOneLine
    or
      ((FDisplay.ViewMode in [vmOneRailLines, vmOneLine]) and
      (Ord(FDisplay.ViewRail) = Ord(rail)))
      (*      or
((FDisplay.ViewMode in [vmOneRailLines, vmOneLine]) and (rail = r_Right))
*);
  end;
  railShowed[r_Both] := True;
  drawEntities();
  if Config.isReflectionsShow then
  begin
    drawer.drawReflections();
  end;
  drawer.drawCurrentEntityLine(elementsCollectionForm);
end;

procedure TAutodec.DrawTape(Disp: TAvk11Display; Rail: RRail; OutRect:
  TRect;
  StartDisCoord, EndDisCoord, Index: Integer);
// ������� ��������� ��������� ������
// Rail - ���� � ������� ����������� ������
// StartDisCoord, EndDisCoord - ���������� �������������� ����� � ������ ������� ���� �����������
// Index - ����� ������
{
const
  elementHeight: Integer = 6;
var
  i, j, y: Integer;
  start, finish, coordinate, length_: Int64;
  eType: TElementType;
  coords: TRange;
  State1: TCrdState;
  fillRect: TRect;
  rectText: string;
  interval: TMinMax;
  X: Integer;
  }
begin

end;

procedure ClearChannelsDataCache();
begin
  Rostov.autodecData.clearData;
end;

end.

