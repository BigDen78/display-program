object RecConfigForm: TRecConfigForm
  Left = 251
  Top = 170
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080'...'
  ClientHeight = 311
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 270
    Width = 329
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 239
      Top = 0
      Width = 90
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 8
        Top = 7
        Width = 75
        Height = 25
        Caption = 'OK'
        TabOrder = 0
        OnClick = Button1Click
      end
    end
  end
  object ScrollBox1: TScrollBox
    Left = 0
    Top = 0
    Width = 329
    Height = 270
    HorzScrollBar.Visible = False
    VertScrollBar.Tracking = True
    VertScrollBar.Visible = False
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    BorderStyle = bsNone
    TabOrder = 1
    object Label10: TLabel
      Left = 19
      Top = 49
      Width = 143
      Height = 13
      Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1072#1103' '#1076#1086#1087#1091#1089#1090#1080#1084#1072#1103' '
    end
    object Label12: TLabel
      Left = 21
      Top = 127
      Width = 84
      Height = 13
      Caption = #1050#1072#1085#1072#1083' 0      -  '#1086#1090'  '
    end
    object Label13: TLabel
      Left = 21
      Top = 149
      Width = 78
      Height = 13
      Caption = #1050#1072#1085#1072#1083' 1      -  '#1086#1090
    end
    object Label14: TLabel
      Left = 21
      Top = 171
      Width = 78
      Height = 13
      Caption = #1050#1072#1085#1072#1083' 2, 3  -  '#1086#1090
    end
    object Label15: TLabel
      Left = 21
      Top = 193
      Width = 78
      Height = 13
      Caption = #1050#1072#1085#1072#1083' 4, 5  -  '#1086#1090
    end
    object Label16: TLabel
      Left = 21
      Top = 215
      Width = 78
      Height = 13
      Caption = #1050#1072#1085#1072#1083' 6, 7  -  '#1086#1090
    end
    object Label17: TLabel
      Left = 21
      Top = 237
      Width = 78
      Height = 13
      Caption = #1050#1072#1085#1072#1083' 8, 9  -  '#1086#1090
    end
    object Label11: TLabel
      Left = 184
      Top = 127
      Width = 43
      Height = 13
      Caption = #1076#1041'   - '#1076#1086' '
    end
    object Label18: TLabel
      Left = 299
      Top = 127
      Width = 13
      Height = 13
      Caption = #1076#1041
    end
    object Label19: TLabel
      Left = 184
      Top = 149
      Width = 43
      Height = 13
      Caption = #1076#1041'   - '#1076#1086' '
    end
    object Label20: TLabel
      Left = 299
      Top = 149
      Width = 13
      Height = 13
      Caption = #1076#1041
    end
    object Label21: TLabel
      Left = 184
      Top = 193
      Width = 43
      Height = 13
      Caption = #1076#1041'   - '#1076#1086' '
    end
    object Label22: TLabel
      Left = 184
      Top = 171
      Width = 43
      Height = 13
      Caption = #1076#1041'   - '#1076#1086' '
    end
    object Label23: TLabel
      Left = 299
      Top = 171
      Width = 13
      Height = 13
      Caption = #1076#1041
    end
    object Label24: TLabel
      Left = 299
      Top = 193
      Width = 13
      Height = 13
      Caption = #1076#1041
    end
    object Label25: TLabel
      Left = 184
      Top = 237
      Width = 43
      Height = 13
      Caption = #1076#1041'   - '#1076#1086' '
    end
    object Label26: TLabel
      Left = 184
      Top = 215
      Width = 43
      Height = 13
      Caption = #1076#1041'   - '#1076#1086' '
    end
    object Label27: TLabel
      Left = 299
      Top = 215
      Width = 13
      Height = 13
      Caption = #1076#1041
    end
    object Label28: TLabel
      Left = 299
      Top = 237
      Width = 13
      Height = 13
      Caption = #1076#1041
    end
    object Bevel4: TBevel
      Left = 14
      Top = 93
      Width = 305
      Height = 2
    end
    object Bevel7: TBevel
      Left = 5
      Top = 17
      Width = 10
      Height = 2
    end
    object Bevel8: TBevel
      Left = 217
      Top = 17
      Width = 111
      Height = 2
    end
    object Label1: TLabel
      Left = 22
      Top = 10
      Width = 189
      Height = 13
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1090#1077#1093#1085#1086#1083#1086#1075#1080#1080' '#1082#1086#1085#1090#1088#1086#1083#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Bevel11: TBevel
      Left = 13
      Top = 486
      Width = 10
      Height = 2
    end
    object Bevel12: TBevel
      Left = 121
      Top = 278
      Width = 200
      Height = 2
    end
    object Label2: TLabel
      Left = 14
      Top = 272
      Width = 161
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1079#1085#1072#1095#1077#1084#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Bevel1: TBevel
      Left = 7
      Top = 261
      Width = 311
      Height = 2
    end
    object Label4: TLabel
      Left = 22
      Top = 377
      Width = 263
      Height = 26
      AutoSize = False
      Caption = 
        #1052#1080#1085#1080#1084#1072#1083#1100#1085#1072#1103' '#1089#1091#1084#1084#1072#1088#1085#1072#1103'                                           ' +
        ' '#1087#1088#1086#1090#1103#1078#1077#1085#1085#1086#1089#1090#1100' '#1087#1072#1095#1077#1082'                                            ' +
        #1084#1084
      WordWrap = True
    end
    object Label5: TLabel
      Left = 22
      Top = 407
      Width = 267
      Height = 26
      AutoSize = False
      Caption = 
        #1052#1080#1085#1080#1084#1072#1083#1100#1085#1072#1103' '#1076#1083#1080#1085#1085#1072'                                 '#1087#1088#1086#1087#1072#1076#1072#1085#1080#1103' '#1044#1057 +
        '                                                      '#1084#1084
      WordWrap = True
    end
    object Label6: TLabel
      Left = 11
      Top = 330
      Width = 96
      Height = 13
      Caption = #1055#1088#1080#1085#1103#1090#1080#1077' '#1088#1077#1096#1077#1085#1080#1103
    end
    object Label3: TLabel
      Left = 22
      Top = 348
      Width = 286
      Height = 26
      AutoSize = False
      Caption = 
        #1052#1080#1085#1080#1084#1072#1083#1100#1085#1072#1103' '#1087#1088#1086#1090#1103#1078#1077#1085#1085#1086#1089#1090#1100'                            '#1086#1076#1080#1085#1086#1095#1085#1086#1081' '#1087 +
        #1072#1095#1082#1080'                                                    '#1084#1084
      WordWrap = True
    end
    object Label7: TLabel
      Left = 19
      Top = 63
      Width = 275
      Height = 13
      Caption = 
        #1089#1082#1086#1088#1086#1089#1090#1100':                                                       ' +
        '            '#1082#1084'/'#1095
    end
    object SpeedEdit: TEdit
      Left = 210
      Top = 57
      Width = 57
      Height = 21
      TabOrder = 0
      OnChange = EditChange
    end
    object Ch0MinEdit: TEdit
      Left = 116
      Top = 124
      Width = 63
      Height = 21
      TabOrder = 1
      OnChange = EditChange
    end
    object Ch0MaxEdit: TEdit
      Left = 231
      Top = 124
      Width = 63
      Height = 21
      TabOrder = 2
      OnChange = EditChange
    end
    object Ch1MinEdit: TEdit
      Left = 116
      Top = 146
      Width = 63
      Height = 21
      TabOrder = 3
      OnChange = EditChange
    end
    object Ch1MaxEdit: TEdit
      Left = 231
      Top = 146
      Width = 63
      Height = 21
      TabOrder = 4
      OnChange = EditChange
    end
    object Ch2MinEdit: TEdit
      Left = 116
      Top = 168
      Width = 63
      Height = 21
      TabOrder = 5
      OnChange = EditChange
    end
    object Ch4MinEdit: TEdit
      Left = 116
      Top = 190
      Width = 63
      Height = 21
      TabOrder = 6
      OnChange = EditChange
    end
    object Ch2MaxEdit: TEdit
      Left = 231
      Top = 168
      Width = 63
      Height = 21
      TabOrder = 7
      OnChange = EditChange
    end
    object Ch4MaxEdit: TEdit
      Left = 231
      Top = 190
      Width = 63
      Height = 21
      TabOrder = 8
      OnChange = EditChange
    end
    object Ch6MinEdit: TEdit
      Left = 116
      Top = 212
      Width = 63
      Height = 21
      TabOrder = 9
      OnChange = EditChange
    end
    object Ch6MaxEdit: TEdit
      Left = 231
      Top = 212
      Width = 63
      Height = 21
      TabOrder = 10
      OnChange = EditChange
    end
    object Ch8MinEdit: TEdit
      Left = 116
      Top = 234
      Width = 63
      Height = 21
      TabOrder = 11
      OnChange = EditChange
    end
    object Ch8MaxEdit: TEdit
      Left = 231
      Top = 234
      Width = 63
      Height = 21
      TabOrder = 12
      OnChange = EditChange
    end
    object UseMaxSpeed: TCheckBox
      Left = 20
      Top = 28
      Width = 122
      Height = 17
      Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1082#1086#1085#1090#1088#1086#1083#1103' '
      TabOrder = 13
      OnClick = CheckBoxClick
    end
    object UseKuZones: TCheckBox
      Left = 20
      Top = 100
      Width = 122
      Height = 17
      Caption = #1063#1091#1074#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100
      TabOrder = 14
      OnClick = CheckBoxClick
    end
    object CheckBox1: TCheckBox
      Left = 387
      Top = 339
      Width = 197
      Height = 17
      Caption = #1044#1074#1086#1081#1085#1086#1081' '#1087#1088#1086#1093#1086#1076' '#1073#1086#1083#1090#1086#1074#1099#1093' '#1089#1090#1099#1082#1086#1074
      TabOrder = 15
      Visible = False
      OnClick = CheckBoxClick
    end
    object CheckBox2_: TCheckBox
      Left = 387
      Top = 373
      Width = 170
      Height = 17
      Caption = #1053#1077' '#1087#1088#1086#1082#1086#1085#1090#1088#1086#1083#1080#1088#1086#1074#1072#1085#1085#1099#1077' '#1041#1054
      Enabled = False
      TabOrder = 16
      Visible = False
      OnClick = CheckBoxClick
    end
    object CheckBox3: TCheckBox
      Left = 387
      Top = 306
      Width = 152
      Height = 17
      Caption = #1042' '#1079#1086#1085#1077' '#1073#1086#1083#1090#1086#1074#1099#1093' '#1089#1090#1099#1082#1086#1074
      TabOrder = 17
      Visible = False
      OnClick = CheckBoxClick
    end
    object CheckBox4: TCheckBox
      Left = 387
      Top = 323
      Width = 208
      Height = 17
      Caption = #1042' '#1101#1083#1077#1084#1077#1085#1090#1072#1093' '#1089#1090#1088#1077#1083#1086#1095#1085#1099#1077' '#1087#1077#1088#1077#1074#1086#1076#1086#1074
      TabOrder = 18
      Visible = False
      OnClick = CheckBoxClick
    end
    object CheckBox2: TCheckBox
      Left = 387
      Top = 356
      Width = 197
      Height = 17
      Caption = #1053#1072#1078#1072#1090#1080#1077' '#1082#1085#1086#1087#1082#1080' '#1041#1057
      TabOrder = 19
      Visible = False
      OnClick = CheckBoxClick
    end
    object Edit1: TEdit
      Left = 202
      Top = 358
      Width = 57
      Height = 21
      TabOrder = 20
      OnChange = EditChange
    end
    object Edit2: TEdit
      Left = 202
      Top = 387
      Width = 57
      Height = 21
      TabOrder = 21
      OnChange = EditChange
    end
    object Edit3: TEdit
      Left = 202
      Top = 417
      Width = 57
      Height = 21
      TabOrder = 22
      OnChange = EditChange
    end
  end
end
