object NewEntryForm: TNewEntryForm
  Left = 397
  Top = 283
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = #1053#1086#1074#1072#1103' '#1079#1072#1087#1080#1089#1100
  ClientHeight = 337
  ClientWidth = 344
  Color = clBtnFace
  Constraints.MinHeight = 226
  Constraints.MinWidth = 334
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 344
    Height = 300
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object InFilePanel: TPanel
      Left = 0
      Top = 0
      Width = 344
      Height = 300
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label17: TLabel
        Left = 7
        Top = 97
        Width = 63
        Height = 13
        Caption = #1057#1086#1076#1077#1088#1078#1072#1085#1080#1077
      end
      object Label4: TLabel
        Left = 7
        Top = 5
        Width = 116
        Height = 13
        Caption = #1055#1091#1090#1077#1081#1089#1082#1072#1103' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1072
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 7
        Top = 51
        Width = 40
        Height = 13
        Caption = #1044#1077#1092#1077#1082#1090
        Color = clBtnFace
        ParentColor = False
      end
      object PosEdit: TEdit
        Left = 7
        Top = 24
        Width = 327
        Height = 21
        Ctl3D = True
        Enabled = False
        ParentCtl3D = False
        ReadOnly = True
        TabOrder = 0
        OnKeyPress = PosEditKeyPress
      end
      object NameEdit: TEdit
        Left = 7
        Top = 70
        Width = 327
        Height = 21
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 1
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object Memo: TMemo
        Left = 7
        Top = 116
        Width = 327
        Height = 163
        ScrollBars = ssBoth
        TabOrder = 2
      end
    end
    object CVSPanel: TPanel
      Left = 0
      Top = 0
      Width = 344
      Height = 300
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 185
        Top = 53
        Width = 43
        Height = 13
        Caption = #1044#1077#1092#1077#1082#1090':'
        Color = clBtnFace
        ParentColor = False
      end
      object Label2: TLabel
        Left = 12
        Top = 79
        Width = 40
        Height = 13
        Caption = #1053#1072#1095#1072#1083#1086':'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 185
        Top = 79
        Width = 34
        Height = 13
        Caption = #1050#1086#1085#1077#1094':'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 12
        Top = 241
        Width = 70
        Height = 13
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
        Color = clBtnFace
        ParentColor = False
      end
      object Label7: TLabel
        Left = 12
        Top = 151
        Width = 116
        Height = 13
        Caption = #1054#1075#1088#1072#1085#1080#1095#1077#1085#1080#1077' '#1089#1082#1086#1088#1086#1089#1090#1080
      end
      object Label8: TLabel
        Left = 185
        Top = 151
        Width = 84
        Height = 13
        Caption = #1056#1072#1079#1084#1077#1088' '#8470'1 ('#1084#1084')'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 13
        Top = 197
        Width = 84
        Height = 13
        Caption = #1056#1072#1079#1084#1077#1088' '#8470'2 ('#1084#1084')'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 185
        Top = 197
        Width = 84
        Height = 13
        Caption = #1056#1072#1079#1084#1077#1088' '#8470'3 ('#1084#1084')'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 12
        Top = 101
        Width = 41
        Height = 13
        Caption = #1064#1080#1088#1086#1090#1072':'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 12
        Top = 126
        Width = 46
        Height = 13
        Caption = #1044#1086#1083#1075#1086#1090#1072':'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 185
        Top = 128
        Width = 46
        Height = 13
        Caption = #1044#1086#1083#1075#1086#1090#1072':'
        Color = clBtnFace
        ParentColor = False
      end
      object LineLabel: TLabel
        Left = 12
        Top = 26
        Width = 46
        Height = 13
        Caption = #1055#1077#1088#1077#1075#1086#1085':'
        Color = clBtnFace
        ParentColor = False
      end
      object PathLabel: TLabel
        Left = 12
        Top = 53
        Width = 27
        Height = 13
        Caption = #1055#1091#1090#1100':'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 185
        Top = 101
        Width = 41
        Height = 13
        Caption = #1064#1080#1088#1086#1090#1072':'
        Color = clBtnFace
        ParentColor = False
      end
      object Edit1_: TEdit
        Left = 241
        Top = 51
        Width = 90
        Height = 21
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 0
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object GPSStLatEdit: TEdit
        Left = 68
        Top = 98
        Width = 90
        Height = 21
        Ctl3D = True
        Enabled = False
        ParentCtl3D = False
        TabOrder = 1
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object GPSStLonEdit: TEdit
        Left = 68
        Top = 125
        Width = 90
        Height = 21
        Ctl3D = True
        Enabled = False
        ParentCtl3D = False
        TabOrder = 2
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object GPSEdLonEdit: TEdit
        Left = 241
        Top = 125
        Width = 90
        Height = 21
        Ctl3D = True
        Enabled = False
        ParentCtl3D = False
        TabOrder = 3
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object CommentEdit: TEdit
        Left = 12
        Top = 260
        Width = 319
        Height = 21
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 11
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object ComboBox1: TComboBox
        Left = 13
        Top = 170
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 7
        Text = #1053#1077#1090
        Items.Strings = (
          #1053#1077#1090
          #1045#1089#1090#1100)
      end
      object SizeEdit: TEdit
        Left = 185
        Top = 170
        Width = 146
        Height = 21
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 8
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object Size2Edit: TEdit
        Left = 13
        Top = 216
        Width = 145
        Height = 21
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 9
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object Size3Edit: TEdit
        Left = 186
        Top = 216
        Width = 145
        Height = 21
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 10
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object CodeDefectEdit: TComboBox
        Left = 241
        Top = 51
        Width = 90
        Height = 21
        TabOrder = 6
        Items.Strings = (
          '111'
          '112'
          '113'
          '121'
          '122'
          '123'
          '124'
          '125'
          '127'
          '1321'
          '1322'
          '133'
          '134'
          '135'
          '139'
          '153'
          '154'
          '211'
          '212'
          '213'
          '2201'
          '2202'
          '2203'
          '2204'
          '221'
          '2221'
          '2222'
          '2223'
          '223'
          '224'
          '2251'
          '2252'
          '227'
          '2321'
          '2322'
          '233'
          '234'
          '235'
          '236'
          '239'
          '253'
          '254'
          '301'
          '302'
          '303'
          '411'
          '412'
          '421'
          '422'
          '431'
          '432'
          '471'
          '472'
          '481')
      end
      object LineEdit: TEdit
        Left = 68
        Top = 23
        Width = 263
        Height = 21
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 4
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object PathEdit: TEdit
        Left = 68
        Top = 51
        Width = 90
        Height = 21
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 5
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
      object GPSEdLatEdit: TEdit
        Left = 241
        Top = 98
        Width = 90
        Height = 21
        Ctl3D = True
        Enabled = False
        ParentCtl3D = False
        TabOrder = 12
        OnChange = NameEditChange
        OnKeyPress = NameEditKeyPress
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 300
    Width = 344
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 344
      Height = 2
      Align = alTop
    end
    object CancelButton: TButton
      Left = 181
      Top = 7
      Width = 75
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
    object OkButton: TButton
      Left = 87
      Top = 7
      Width = 75
      Height = 25
      Caption = 'OK'
      TabOrder = 0
      OnClick = OkButtonClick
    end
  end
end
