unit LabelsBaseUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, WideStrings, FMTBcd, StdCtrls, ExtCtrls, SqlExpr, DB, DbxSybaseASA, Grids, DBGrids;

type
  TLabelsBaseForm = class(TForm)
    SQLConnection1: TSQLConnection;
    s: TSQLQuery;
    SQLTable1: TSQLTable;
    Button1: TButton;
    OpenDialog: TOpenDialog;
    SQLMonitor1: TSQLMonitor;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LabelsBaseForm: TLabelsBaseForm;

implementation

uses
  DB, SqlExpr, DBXCommon, DBXMySQL;

{$R *.dfm}

procedure TLabelsBaseForm.Button1Click(Sender: TObject);
var
  Results: TDataSet;

begin
  if OpenDialog.Execute() then
  begin

    SQLConnection1.Params.Clear;
    SQLConnection1.Params.Add('Database=' + OpenDialog.FileName);
    SQLConnection1.Params.Add('FailIfMissing=False');
    SQLConnection1.ConnectionName := 'MyConnection';

  try
    // Connect to the database.
    SQLConnection1.Connected := true;
    // Activate the monitor.
    SQLMonitor1.Active := true;
    // Create and populate a table.
 //   PopulateTable(SQLConnection1);
 //   SQLConnection1.Execute('SELECT * FROM ExampleTable', nil, Results);
    // Save the trace list to a file on disk.
 //   SQLMonitor1.SaveToFile('D:\\Log.txt');
  except
    on E: EDatabaseError do
      ShowMessage('Exception raised with message' + E.Message);
  end;
  SQLMonitor1.Active := False;
end;

end.
