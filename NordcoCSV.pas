unit NordcoCSV;

interface

uses
  SysUtils, AviconDataContainer, AviconDataSource, PublicFunc, AviconTypes;

type

  TNordcoCSVFile = class

  private
    FList: array of TNordcoCSVRecordLine;

  public
    constructor Create;
    destructor Destroy;
    procedure AddLine(NewLine: TNordcoCSVRecordLine); overload;
(*    procedure AddLine(DC: TAviconDataContainer;
                          StDisCoord, EdDisCoord: Integer;
                          Lat, Lon: Single;
                          CodeDefect: string;
                          SourceType: TSourceType;
                          IsFailure: Boolean;
                          Size, Size2, Size3: Single; UseSize, UseSize2, UseSize3: Boolean;
                          CommentText: string); overload;
    procedure AddLine(DS: TAviconDataSource;
                      StDisCoord, EdDisCoord: Integer;
                      CodeDefect: string;
                      IsFailure: Boolean;
                      Size, Size2, Size3: Single; UseSize, UseSize2, UseSize3: Boolean;
                      CommentText: string); overload;
    procedure AddLine(DS: TAviconDataSource;
                      Line: string;
                      Track: Integer;
                      StDisCoord, EdDisCoord: Integer;
                      CodeDefect: string;
                      IsFailure: Boolean;
                      Size, Size2, Size3: Single; UseSize, UseSize2, UseSize3: Boolean;
                      CommentText: string); overload; *)
    procedure SaveToFile(FileName: string);
  end;

implementation

uses
  Classes;

constructor TNordcoCSVFile.Create;
begin
  SetLength(FList, 0);
end;

destructor TNordcoCSVFile.Destroy;
begin
  SetLength(FList, 0);
end;

procedure TNordcoCSVFile.AddLine(NewLine: TNordcoCSVRecordLine);
begin
  SetLength(FList, Length(FList) + 1);
  FList[Length(FList) - 1]:= NewLine;
end;

// ��������:
// DC - ��������� �� ���� ������������� ������� ���� �����
// StDisCoord, EnDisCoord - ���������� ������ � ����� ������� (������������� ����������� � ��������� ����������)
// Lat, Lon - ���������� ������� - ������ � ������ (������ ������ �� �����������)
// CodeDefect - ��� �������, ������
// SourceType - ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
// IsFailure - TRUE - ��� ������, ������� �������� ����������� ��������
// Size � UseSize - ������ (��) ������� ��� ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
// Size2 � UseSize2 - ������ (��) ������� ���  ����������� ������� (���� ����� ��� ����������)
// Size3 � UseSize3 - ������ (��) ������� ���  ����������� ������� (���� ����� ��� ����������)
// CommentText - ����������
(*
procedure TNordcoCSVFile.AddLine(DC: TAviconDataContainer;
                                 StDisCoord, EdDisCoord: Integer;
                                 Lat, Lon: Single;
                                 CodeDefect: string;
                                 SourceType: TSourceType;
                                 IsFailure: Boolean;
                                 Size, Size2, Size3: Single; UseSize, UseSize2, UseSize3: Boolean;
                                 CommentText: string);

var
  I: Integer;
  item: TNordcoCSVRecordLine;

begin
  DC. GetRailPathNumber_in_
  item.Line:= HeaderStrToString(DC.Header.PathSectionName); // �������� ��� ������������� �������������� �����
  item.Track:= DC.Header.RailPathNumber); // ������������� ����, � ������� ��������� ������
  item.LocationFrom:= CaCrdToVal(csMetric1kmTurkish, DC.DisToCaCrd(StDisCoord));  // ������ ������������� �������
  item.LocationTo:= CaCrdToVal(csMetric1kmTurkish, DC.DisToCaCrd(EdDisCoord));    // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"
  item.LatitudeFrom      := Lat;                                                     // GPS-������ ��� ���� "Location From"
  item.LatitudeTo        := Lat;                                                     // GPS-������ ��� ���� "Location To"
  item.LongitudeFrom     := Lon;                                                     // GPS-������� ��� ���� "Location From"
  item.LongitudeTo       := Lon;                                                     // GPS-������� ��� ���� "Location To"
  item.Type_:= CodeDefect;                                                           // ��� �������
  item.SurveyDate:= Trunc(Now);
  if SourceType = stUltrasonicTrolley // ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
      then item.Source:= 'Ultrasonic Trolley'
      else item.Source:= 'Visual Inspection';
  item.Status:= 'Open'; //��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
  item.IsFailure:= IsFailure;
  if DC.Header.WorkRailTypeA = 0 then item.AssetType:= 'Left Rail' else item.AssetType:= 'Right Rail'; // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
  item.DataEntryDate:= Trunc(Now);  // ���� ����������� ������ ��������
  if UseSize then
  begin
    item.Size:= Size;              // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
    item.UnitofMeasurement:= 'mm'; //��. ��������� ����
  end;
  if UseSize2 then
  begin
    item.Size2:= Size2;             // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    item.UnitofMeasurement2:= 'mm'; // ��. ��������� ����
  end;
  if UseSize3 then
  begin
    item.Size3:= Size3;             // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    item.UnitofMeasurement3:= 'mm'; // ��. ��������� ����
  end;

  item.Comment:= CommentText;   // ����������
  item.Operator_:= HeaderStrToString(DC.Header.OperatorName); // ��������
  item.Device:= 'USK-004R';
  for I := 0 to DC.Header.UnitsCount - 1 do
    if DC.Header.UnitsInfo[I].UnitType = dutUpUnit then
    begin
      item.Device:= item.Device + ' s/n ' + HeaderStrToString(DC.Header.UnitsInfo[I].WorksNumber);
      Break;
    end;

  SetLength(FList, Length(FList) + 1);
  FList[Length(FList) - 1]:= item;

end;

// ��������:
// DC - ��������� �� ���� ������������� ������� ���� �����
// StDisCoord, EnDisCoord - ���������� ������ � ����� ������� (������������� ����������� � ��������� ����������)
// Lat, Lon - ���������� ������� - ������ � ������ (������ ������ �� �����������)
// CodeDefect - ��� �������, ������
// SourceType - ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
// IsFailure - TRUE - ��� ������, ������� �������� ����������� ��������
// Size � UseSize - ������ (��) ������� ��� ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
// Size2 � UseSize2 - ������ (��) ������� ���  ����������� ������� (���� ����� ��� ����������)
// Size3 � UseSize3 - ������ (��) ������� ���  ����������� ������� (���� ����� ��� ����������)
// CommentText - ����������

procedure TNordcoCSVFile.AddLine(DS: TAviconDataSource;
                                 StDisCoord, EdDisCoord: Integer;
                                 CodeDefect: string;
                                 IsFailure: Boolean;
                                 Size, Size2, Size3: Single;
                                 UseSize, UseSize2, UseSize3: Boolean;
                                 CommentText: string);

var
  I: Integer;
  item: TNordcoCSVRecordLine;

begin
  item.Line:= HeaderStrToString(DS.Header.PathSectionName); // �������� ��� ������������� �������������� �����
  item.Track:= IntToStr(DS.Header.RailPathNumber); // ������������� ����, � ������� ��������� ������
  item.LocationFrom:= CaCrdToVal(csMetric1kmTurkish, DS.DisToCaCrd(StDisCoord));  // ������ ������������� �������
  item.LocationTo:= CaCrdToVal(csMetric1kmTurkish, DS.DisToCaCrd(EdDisCoord));    // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"


  item.LatitudeFrom      := DS.GetGPSCoord(StDisCoord).Lat;  // GPS-������ ��� ���� "Location From"
  item.LatitudeTo        := DS.GetGPSCoord(EdDisCoord).Lat;  // GPS-������ ��� ���� "Location To"
  item.LongitudeFrom     := DS.GetGPSCoord(StDisCoord).Lon;  // GPS-������� ��� ���� "Location From"
  item.LongitudeTo       := DS.GetGPSCoord(EdDisCoord).Lon;  // GPS-������� ��� ���� "Location To"
  item.Type_:= CodeDefect; // ��� �������
  item.SurveyDate:= Trunc(Now);
  item.Source:= 'Ultrasonic Trolley';
  item.Status:= 'Open'; //��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
  item.IsFailure:= IsFailure;
  if DS.Header.WorkRailTypeA = 0 then item.AssetType:= 'Left Rail' else item.AssetType:= 'Right Rail'; // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
  item.DataEntryDate:= Trunc(Now);  // ���� ����������� ������ ��������
  if UseSize then
  begin
    item.Size:= Size;              // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
    item.UnitofMeasurement:= 'mm'; //��. ��������� ����
  end;
  if UseSize2 then
  begin
    item.Size2:= Size2;             // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    item.UnitofMeasurement2:= 'mm'; // ��. ��������� ����
  end;
  if UseSize3 then
  begin
    item.Size3:= Size3;             // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    item.UnitofMeasurement3:= 'mm'; // ��. ��������� ����
  end;

  item.Comment:= CommentText;   // ����������
  item.Operator_:= HeaderStrToString(DS.Header.OperatorName); // ��������
  item.Device:= 'USK-004R';
  for I := 0 to DS.Header.UnitsCount - 1 do
    if DS.Header.UnitsInfo[I].UnitType = dutUpUnit then
    begin
      item.Device:= item.Device + ' s/n ' + HeaderStrToString(DS.Header.UnitsInfo[I].WorksNumber);
      Break;
    end;

  SetLength(FList, Length(FList) + 1);
  FList[Length(FList) - 1]:= item;
end;

procedure TNordcoCSVFile.AddLine(DS: TAviconDataSource;
                                 Line: string;
                                 Track: Integer;
                                 StDisCoord, EdDisCoord: Integer;
                                 CodeDefect: string;
                                 IsFailure: Boolean;
                                 Size, Size2, Size3: Single;
                                 UseSize, UseSize2, UseSize3: Boolean;
                                 CommentText: string);

var
  I: Integer;
  item: TNordcoCSVRecordLine;

begin
  item.Line:= Line; // HeaderStrToString(DS.Header.PathSectionName); // �������� ��� ������������� �������������� �����
  item.Track:= IntToStr(Track); //IntToStr(DS.Header.RailPathNumber); // ������������� ����, � ������� ��������� ������
  item.LocationFrom:= CaCrdToVal(csMetric1kmTurkish, DS.DisToCaCrd(StDisCoord));  // ������ ������������� �������
  item.LocationTo:= CaCrdToVal(csMetric1kmTurkish, DS.DisToCaCrd(EdDisCoord));    // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"


  item.LatitudeFrom      := DS.GetGPSCoord(StDisCoord).Lat;  // GPS-������ ��� ���� "Location From"
  item.LatitudeTo        := DS.GetGPSCoord(EdDisCoord).Lat;  // GPS-������ ��� ���� "Location To"
  item.LongitudeFrom     := DS.GetGPSCoord(StDisCoord).Lon;  // GPS-������� ��� ���� "Location From"
  item.LongitudeTo       := DS.GetGPSCoord(EdDisCoord).Lon;  // GPS-������� ��� ���� "Location To"
  item.Type_:= CodeDefect; // ��� �������
  item.SurveyDate:= Trunc(Now);
  item.Source:= 'Ultrasonic Trolley';
  item.Status:= 'Open'; //��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
  item.IsFailure:= IsFailure;
  if DS.Header.WorkRailTypeA = 0 then item.AssetType:= 'Left Rail' else item.AssetType:= 'Right Rail'; // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
  item.DataEntryDate:= Trunc(Now);  // ���� ����������� ������ ��������
  if UseSize then
  begin
    item.Size:= Size;              // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
    item.UnitofMeasurement:= 'mm'; //��. ��������� ����
  end;
  if UseSize2 then
  begin
    item.Size2:= Size2;             // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    item.UnitofMeasurement2:= 'mm'; // ��. ��������� ����
  end;
  if UseSize3 then
  begin
    item.Size3:= Size3;             // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    item.UnitofMeasurement3:= 'mm'; // ��. ��������� ����
  end;

  item.Comment:= CommentText;   // ����������
  item.Operator_:= HeaderStrToString(DS.Header.OperatorName); // ��������
  item.Device:= 'USK-004R';
  for I := 0 to DS.Header.UnitsCount - 1 do
    if DS.Header.UnitsInfo[I].UnitType = dutUpUnit then
    begin
      item.Device:= item.Device + ' s/n ' + HeaderStrToString(DS.Header.UnitsInfo[I].WorksNumber);
      Break;
    end;

  SetLength(FList, Length(FList) + 1);
  FList[Length(FList) - 1]:= item;
end;
*)

procedure TNordcoCSVFile.SaveToFile(FileName: string);
var
  I: Integer;
  sl: TStringList;
  Str: string;
  SaveDecimalSeparator: Char;


function MyDateToStr(Date: TDateTime): string;
begin
  Result:= FormatDateTime('yyyy.mm.dd', Date);
end;

function GPSSingleToText( A: Single; Lat: Boolean; Mode: Integer = 1 ): string; //  ������ = Latitude
var
  D, M, S: LongInt;
  SW: string;

begin
  try
    if Lat then //  ������ = Latitude
    begin

      if (A >= -90) and (A <= 90) then
      begin
//        if A >= 0 then SW:= 'N' else SW:= 'S';
        if A >= 0 then SW:= '' else SW:= '-';
        if Mode = 1 then
        begin
          D:= Trunc( A );
          M:= Trunc( ( A - D ) * 60 );
          S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
          Result:= Format( '%s%d�%d.%d', [SW, D, M, S ] );
        end
        else Result:= Format( '%s%2.10f', [SW, A] );
      end else Result:= '';
    end
    else
    begin
      if (A >= -180) and (A <= 180) then
      begin
//        if A >= 0 then SW:= 'E' else SW:= 'W';
        if A >= 0 then SW:= '' else SW:= '-';
        if Mode = 1 then
        begin
          D:= Trunc( A );
          M:= Trunc( ( A - D ) * 60 );
          S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
          Result:= Format( '%s%d�%d.%d', [SW, D, M, S ] );
        end
        else Result:= Format( '%s%2.10f', [SW, A] );
      end else Result:= '';
    end;

  except
    Result:= '';
  end;
end;


begin
  SaveDecimalSeparator:= DecimalSeparator;
  DecimalSeparator:= ',';
  sl:= TStringList.Create;

  if FileExists(FileName) then sl.LoadFromFile(FileName) else
  begin
    Str:= 'Line;Track;Location From;Location To;Location Max;Measured Lenght;Latitude From;Latitude To;Longitude From;Longitude To;Latitude Max;Longitude Max;Type;Survey Date;Source;Status;Is Failure;Failure Impact;Failure Object;Failure Speed Value;Failure Ti';
    Str:= Str + 'me Value;Anticipated Finish;Reason;Removed By;Import;External Id;ExternalTypeId;Asset Type;Asset Positon;Asset Name;Data Entry Date;Closing Date;Priority;Size;Size 2;Size 3;Unit of Measurement;Unit of Measurement 2;Unit of Measurement 3;Threshold Cl';
    Str:= Str + 'ass ;Threshold Value;Threshold Class 2;Threshold Value 2;Threshold Class 3;Threshold Value 3;Comment;Attachment;Operator��������������;Device';
    sl.Add(Str);

    Str:= ';;[km] n,nnnn;[km] n,nnnn;[km] n,nnnn;[m] n,n;[�];[�];[�];[�];[�];[�];;YYYY.MM.DD;;;True/False;;;[km/h] n,nnnn;;YYYY.MM.DD;;;;;;*;;;YYYY.MM.DD;YYYY.MM.DD;;n,n;n,n;n,n;;;;;n,n;;n,n;;n,n;;;;';
    sl.Add(Str);


    Str:= ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;';
    sl.Add(Str);
  end;

  for I := 0 to Length(FList) - 1 do
  begin
    Str:= '';
    Str:= Str + FList[I].Line + ';';                                            // �������� ��� ������������� �������������� �����
    Str:= Str + FList[I].Track + ';';                                           // ������������� ����, � ������� ��������� ������
    Str:= Str + Format('%3.3f', [FList[I].LocationFrom]) + ';';                 // ������ ������������� �������
    Str:= Str + Format('%3.3f', [FList[I].LocationTo]) + ';';                   // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"
    Str:= Str + ';';
    Str:= Str + ';';

    Str:= Str + GPSSingleToText(FList[I].LatitudeFrom, True, 2) + ';';          // GPS-������ ��� ���� "Location From"
    Str:= Str + GPSSingleToText(FList[I].LatitudeTo, True, 2) + ';';            // GPS-������ ��� ���� "Location To"
    Str:= Str + GPSSingleToText(FList[I].LongitudeFrom, False, 2) + ';';        // GPS-������� ��� ���� "Location From"
    Str:= Str + GPSSingleToText(FList[I].LongitudeTo, False, 2) + ';';          // GPS-������� ��� ���� "Location To"

//  Str:= Str + Format('%3.10f', [FList[I].LatitudeFrom]) + ';';                // GPS-������ ��� ���� "Location From"
//  Str:= Str + Format('%3.10f', [FList[I].LatitudeTo]) + ';';                  // GPS-������ ��� ���� "Location To"
//  Str:= Str + Format('%3.10f', [FList[I].LongitudeFrom]) + ';';               // GPS-������� ��� ���� "Location From"
//  Str:= Str + Format('%3.10f', [FList[I].LongitudeTo]) + ';';                 // GPS-������� ��� ���� "Location To"
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + FList[I].Type_ + ';';                                           // ��� ������� �� UIC 712
    Str:= Str + MyDateToStr(FList[I].SurveyDate) + ';';                         // ���� ����������� �������
    Str:= Str + FList[I].Source + ';';                                          // ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
    Str:= Str + FList[I].Status + ';';                                          // ��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
    if FList[I].IsFailure
      then Str:= Str + 'TRUE;'                                                  // ���� ���������� ������ ������� �����, �� ��� ���� ������ ����� �������� TRUE. ������� ����� ������� ����� ������, ������� �������� ����������� ��������
      else Str:= Str + 'FALSE;';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + FList[I].AssetType + ';';                                       // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + MyDateToStr(FList[I].DataEntryDate) + ';';                      // ���� ����������� ������ ��������
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + Format('%3.1f', [FList[I].Size ]) + ';';                        // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
    Str:= Str + Format('%3.1f', [FList[I].Size2]) + ';';                        // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    Str:= Str + Format('%3.1f', [FList[I].Size3]) + ';';                        // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    Str:= Str + FList[I].UnitofMeasurement + ';';                               // ��. ��������� ���� "Size"
    Str:= Str + FList[I].UnitofMeasurement2 + ';';                              // ��. ��������� ���� "Size2"
    Str:= Str + FList[I].UnitofMeasurement3 + ';';                              // ��. ��������� ���� "Size3"
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + ';';
    Str:= Str + FList[I].Comment + ';';                                         // ����������
    Str:= Str + ';';
    Str:= Str + FList[I].Operator_ + ';';                                       // ��������
    Str:= Str + FList[I].Device + ';';                                          // ������
    sl.Add(Str);
  end;

  if sl.Count > 3 then sl.SaveToFile(FileName);
  sl.Free;

  DecimalSeparator:= SaveDecimalSeparator;
end;


end.

