object RecDataForm: TRecDataForm
  Left = 44
  Top = 142
  Width = 1236
  Height = 696
  Caption = 'RecDataForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShortCut = FormShortCut
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 456
    Width = 1228
    Height = 6
    Cursor = crVSplit
    Align = alBottom
    AutoSnap = False
    Beveled = True
    MinSize = 60
    ResizeStyle = rsUpdate
  end
  object ScrollBar: TScrollBar
    Left = 0
    Top = 439
    Width = 1228
    Height = 17
    Align = alBottom
    PageSize = 0
    TabOrder = 0
    OnChange = MyRefresh
  end
  object ScrollBox: TScrollBox
    Left = 0
    Top = 57
    Width = 1228
    Height = 382
    VertScrollBar.Tracking = True
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    BorderStyle = bsNone
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    OnResize = ScrollBoxResize
    object PaintBox: TPaintBox
      Left = 0
      Top = 0
      Width = 841
      Height = 313
      OnClick = PaintBoxClick
      OnDblClick = PaintBoxDblClick
      OnMouseMove = PaintBoxMouseMove
      OnPaint = MyRefresh
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1228
    Height = 57
    Align = alTop
    TabOrder = 2
    object X1: TSpeedButton
      Left = 36
      Top = 30
      Width = 23
      Height = 22
      GroupIndex = 1
      Down = True
      Caption = 'x1'
      OnClick = ScrollBoxResize
    end
    object X2: TSpeedButton
      Left = 60
      Top = 30
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'x2'
      OnClick = ScrollBoxResize
    end
    object X3: TSpeedButton
      Left = 84
      Top = 30
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'x3'
      OnClick = ScrollBoxResize
    end
    object X4: TSpeedButton
      Left = 108
      Top = 30
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'x4'
      OnClick = ScrollBoxResize
    end
    object X5: TSpeedButton
      Left = 132
      Top = 30
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'x5'
      OnClick = ScrollBoxResize
    end
    object X6: TSpeedButton
      Left = 156
      Top = 30
      Width = 23
      Height = 22
      GroupIndex = 1
      Caption = 'x6'
      OnClick = ScrollBoxResize
    end
    object Label2: TLabel
      Left = 257
      Top = 0
      Width = 53
      Height = 13
      AutoSize = False
      Caption = 'Reduction:'
    end
    object SpeedButton1: TSpeedButton
      Left = 256
      Top = 15
      Width = 18
      Height = 21
      GroupIndex = 3
      Down = True
      Caption = '0'
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 274
      Top = 15
      Width = 18
      Height = 21
      GroupIndex = 3
      Caption = '1'
      OnClick = SpeedButton2Click
    end
    object SpeedButton3: TSpeedButton
      Left = 292
      Top = 15
      Width = 18
      Height = 21
      GroupIndex = 3
      Caption = '2'
      OnClick = SpeedButton3Click
    end
    object Label11: TLabel
      Left = 344
      Top = 39
      Width = 55
      Height = 13
      Caption = 'DisCoord = '
    end
    object Label12: TLabel
      Left = 486
      Top = 5
      Width = 109
      Height = 22
      Caption = 'DisCoord = '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 22
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label21: TLabel
      Left = 214
      Top = 3
      Width = 8
      Height = 13
      Alignment = taCenter
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 487
      Top = 27
      Width = 79
      Height = 22
      Caption = 'Delay = '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 22
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object ZoomBar: TTrackBar
      Left = 36
      Top = 2
      Width = 148
      Height = 24
      Max = 50
      Position = 20
      TabOrder = 0
      TickStyle = tsNone
      OnChange = ZoomBarChange
    end
    object Panel7: TPanel
      Left = 36
      Top = 25
      Width = 148
      Height = 2
      TabOrder = 1
    end
    object Panel8: TPanel
      Left = 333
      Top = 3
      Width = 2
      Height = 49
      TabOrder = 2
    end
    object CheckBox2: TCheckBox
      Left = 257
      Top = 39
      Width = 56
      Height = 15
      Caption = 'Sep Ch'
      TabOrder = 3
      OnClick = CheckBox2Click
    end
    object Panel2: TPanel
      Left = 182
      Top = 3
      Width = 2
      Height = 49
      TabOrder = 4
    end
    object ViewTh: TTrackBar
      Left = 187
      Top = 2
      Width = 24
      Height = 53
      Max = 15
      Min = 3
      Orientation = trVertical
      Position = 3
      TabOrder = 5
      TickMarks = tmTopLeft
      TickStyle = tsNone
      OnChange = ViewThChange
    end
    object cbZoomIn: TCheckBox
      Left = 214
      Top = 20
      Width = 36
      Height = 17
      Caption = '-->'
      Checked = True
      State = cbChecked
      TabOrder = 6
      OnClick = MyRefresh
    end
    object cbZoomOut: TCheckBox
      Left = 214
      Top = 36
      Width = 36
      Height = 17
      Caption = '<--'
      Checked = True
      State = cbChecked
      TabOrder = 7
      OnClick = MyRefresh
    end
    object ViewStrip: TCheckListBox
      Left = 2
      Top = 2
      Width = 30
      Height = 52
      OnClickCheck = ViewStripClickCheck
      BorderStyle = bsNone
      ItemHeight = 13
      Items.Strings = (
        '1'
        '2'
        '3'
        '4')
      TabOrder = 8
    end
    object Test: TCheckBox
      Left = 342
      Top = 5
      Width = 97
      Height = 17
      Caption = #1062#1077#1085#1090#1088' '#1101#1082#1088#1072#1085#1072
      TabOrder = 9
      OnClick = MyRefresh
    end
    object Panel19: TPanel
      Left = 477
      Top = 3
      Width = 2
      Height = 49
      TabOrder = 10
    end
    object cbCursor: TCheckBox
      Left = 342
      Top = 21
      Width = 97
      Height = 17
      Caption = #1050#1091#1088#1089#1086#1088
      Checked = True
      State = cbChecked
      TabOrder = 11
      OnClick = MyRefresh
    end
    object cbEchoSize: TComboBox
      Left = 768
      Top = 7
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 1
      TabOrder = 12
      Text = '2x2'
      OnChange = MyRefresh
      Items.Strings = (
        '1x1'
        '2x2'
        '3x3'
        '4x4')
    end
    object cbViewMode: TComboBox
      Left = 768
      Top = 29
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 13
      Text = #1053#1077#1090
      OnChange = cbViewModeChange
      Items.Strings = (
        #1053#1077#1090
        #1044#1086#1085#1085#1099#1081' '#1089#1080#1075#1085#1072#1083)
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 462
    Width = 1228
    Height = 200
    ActivePage = TabSheet7
    Align = alBottom
    TabOrder = 3
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = '    X'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object TabSheet11: TTabSheet
      Caption = #1047#1086#1085#1099' '#1076#1074#1080#1078#1077#1085#1080#1103' '#1085#1072#1079#1072#1076
      ImageIndex = 10
      object Panel21: TPanel
        Left = 0
        Top = 0
        Width = 208
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label22: TLabel
          Left = 6
          Top = 73
          Width = 57
          Height = 13
          Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077':'
        end
        object Label23: TLabel
          Left = 6
          Top = 90
          Width = 70
          Height = 13
          Caption = #1048#1085#1076#1077#1082#1089' '#1079#1086#1085#1099':'
        end
        object Bevel1: TBevel
          Left = 8
          Top = 65
          Width = 191
          Height = 2
        end
        object Bevel4: TBevel
          Left = 8
          Top = 113
          Width = 191
          Height = 2
        end
        object CheckBox26: TCheckBox
          Left = 6
          Top = 8
          Width = 201
          Height = 17
          Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1087#1077#1088#1077#1085#1086#1089' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1099
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = MyRefresh
        end
        object CheckBox27: TCheckBox
          Left = 6
          Top = 25
          Width = 201
          Height = 17
          Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1075#1088#1072#1085#1080#1094#1099' '#1079#1086#1085#1099
          TabOrder = 1
          OnClick = MyRefresh
        end
        object CheckBox28: TCheckBox
          Left = 6
          Top = 42
          Width = 201
          Height = 17
          Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1079#1086#1085#1099
          TabOrder = 2
          OnClick = MyRefresh
        end
        object CheckBox32: TCheckBox
          Left = 8
          Top = 120
          Width = 129
          Height = 17
          Caption = #1057#1080#1085#1093#1088#1086#1085#1080#1079#1080#1088#1086#1074#1072#1090#1100
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object Button7: TButton
          Left = 120
          Top = 141
          Width = 75
          Height = 25
          Caption = #1055#1086' '#1074#1099#1089#1086#1090#1077
          TabOrder = 4
          Visible = False
          OnClick = Button7Click
        end
        object CheckBox33: TCheckBox
          Left = 8
          Top = 136
          Width = 96
          Height = 33
          Caption = #1047#1086#1085#1099' '#1074#1087#1077#1088#1077#1076' '#1073#1077#1079' '#1087#1086#1074#1090#1086#1088#1072
          TabOrder = 5
          WordWrap = True
          OnClick = MyRefresh
        end
      end
      object Chart12: TChart
        Left = 208
        Top = 0
        Width = 1014
        Height = 172
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        Legend.Visible = False
        View3D = False
        OnBeforeDrawSeries = Chart1BeforeDrawSeries
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object LineSeries7: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          ShowInLegend = False
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
    end
    object TabSheet7: TTabSheet
      Caption = #1044#1086#1085#1085#1099#1081' '#1089#1080#1075#1085#1072#1083
      ImageIndex = 6
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 145
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object BSCheck: TCheckListBox
          Left = 2
          Top = 2
          Width = 137
          Height = 167
          OnClickCheck = MyRefresh2
          Ctl3D = False
          ItemHeight = 13
          Items.Strings = (
            #1047#1086#1085#1072' '#1076#1086#1085#1085#1086#1075#1086' '#1089#1080#1075#1085#1072#1083#1072' '
            #1059#1079#1083#1099
            #1044#1099#1088#1082#1080
            #1041#1086#1083#1100#1096#1080#1077' '#1044#1099#1088#1082#1080
            #1048#1085#1076#1077#1082#1089#1099
            #1052#1072#1083#1077#1085#1100#1082#1080#1077' '#1076#1099#1088#1082#1080
            #1044#1099#1088#1082#1080' '#1082#1088#1086#1084#1077' '#1047#1044#1053
            #1044#1099#1088#1082#1080' '#1074' '#1047#1044#1053
            #1044#1099#1088#1082#1080' '#1082#1088#1086#1084#1077' '#1087#1077#1088#1077#1087#1088#1086#1074#1077#1088#1077#1085#1099#1093
            #1044#1099#1088#1082#1080' '#1087#1077#1088#1077#1087#1088#1086#1074#1077#1088#1077#1085#1099#1077
            #1047#1086#1085#1099' '#1088#1072#1089#1095#1077#1090#1072' '#1075#1080#1089#1090#1086#1075#1088#1072#1084#1084#1099
            #1054#1089#1090#1088#1103#1082)
          ParentCtl3D = False
          TabOrder = 0
        end
      end
      object Panel14: TPanel
        Left = 145
        Top = 0
        Width = 73
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object CheckBox6: TCheckBox
          Left = 0
          Top = 0
          Width = 67
          Height = 17
          Caption = #1042#1082#1083
          TabOrder = 0
        end
        object CheckBox12: TCheckBox
          Left = 0
          Top = 16
          Width = 67
          Height = 17
          Caption = #1051#1077#1074#1072#1103
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object CheckBox13: TCheckBox
          Left = 0
          Top = 33
          Width = 67
          Height = 17
          Caption = #1055#1088#1072#1074#1072#1103
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object Memo1: TMemo
          Left = 1
          Top = 52
          Width = 68
          Height = 51
          TabOrder = 3
        end
      end
      object Chart6: TChart
        Left = 218
        Top = 0
        Width = 1002
        Height = 172
        BackWall.Brush.Color = clWhite
        BackWall.Color = clWhite
        MarginBottom = 1
        MarginLeft = 1
        MarginRight = 1
        MarginTop = 1
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BackColor = clWhite
        BottomAxis.DateTimeFormat = 'dd.MM.yyyy'
        Legend.Visible = False
        View3D = False
        Align = alClient
        Color = clWhite
        TabOrder = 2
        object LineSeries3: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object LineSeries4: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clGreen
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = #1055#1072#1095#1082#1080
      ImageIndex = 5
      object Label3: TLabel
        Left = 5
        Top = 0
        Width = 37
        Height = 13
        Caption = #1055#1072#1095#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 144
        Top = 145
        Width = 85
        Height = 16
        Caption = 'SysCoord = '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object PBCheck: TCheckListBox
        Left = 2
        Top = 17
        Width = 130
        Height = 128
        OnClickCheck = MyRefresh
        Ctl3D = False
        ItemHeight = 13
        Items.Strings = (
          #1042#1089#1077' ('#1050#1088#1086#1084#1077' '#1091#1076#1072#1083#1077#1085#1085#1099#1093')'
          #1042' '#1079#1086#1085#1077' '#1044#1053
          #1055#1072#1095#1082#1072' '#1082#1086#1085'-'#1075#1086' '#1101#1083#1077#1084#1077#1085#1090#1072
          #1055#1072#1095#1082#1072' '#1095#1080#1089#1090#1086#1075#1086' '#1088#1077#1083#1100#1089#1072
          #1053#1077' '#1087#1086#1089#1083#1077#1076#1085#1099#1081' '#1082#1086#1085#1090#1088#1086#1083#1100
          #1055#1077#1088#1077#1087#1088#1086#1074#1077#1088#1077#1085#1085#1072#1103' ('#1050' = 0)'
          #1053#1055#1050#1059#1095
          #1044#1077#1092#1077#1082#1090
          #1059#1076#1072#1083#1077#1085#1085#1099#1077)
        ParentCtl3D = False
        TabOrder = 0
      end
      object CheckBox16: TCheckBox
        Left = 141
        Top = 25
        Width = 113
        Height = 17
        Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090#1099
        TabOrder = 1
        OnClick = MyRefresh2
      end
      object CheckBox17: TCheckBox
        Left = 141
        Top = 41
        Width = 113
        Height = 17
        Caption = #1048#1085#1076#1077#1082#1089
        TabOrder = 2
        OnClick = MyRefresh2
      end
      object CheckBox29: TCheckBox
        Left = 141
        Top = 59
        Width = 113
        Height = 23
        Caption = #1042#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1085#1072#1103' '#1079#1086#1085#1072' '#1087#1072#1095#1082#1080
        TabOrder = 3
        WordWrap = True
        OnClick = MyRefresh
      end
      object PBInfo: TListBox
        Left = 264
        Top = 0
        Width = 209
        Height = 169
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 4
      end
      object ListBox2: TListBox
        Left = 475
        Top = 0
        Width = 422
        Height = 169
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 5
      end
      object CheckBox37: TCheckBox
        Left = 141
        Top = 120
        Width = 97
        Height = 17
        Caption = #1047#1086#1085#1072' '#1087#1086#1076#1086#1096#1074#1099
        TabOrder = 6
        OnClick = MyRefresh
      end
      object CheckBox39: TCheckBox
        Left = 141
        Top = 84
        Width = 113
        Height = 17
        Caption = #1043#1083#1091#1073#1080#1080#1085#1072
        TabOrder = 7
        OnClick = MyRefresh2
      end
      object CheckBox40: TCheckBox
        Left = 7
        Top = 151
        Width = 113
        Height = 17
        Caption = #1050#1088#1086#1084#1077' '#1091#1076#1072#1083#1077#1085#1085#1099#1093
        TabOrder = 8
        OnClick = MyRefresh
      end
      object CheckBox41: TCheckBox
        Left = 141
        Top = 100
        Width = 113
        Height = 17
        Caption = 'Text'
        TabOrder = 9
        OnClick = MyRefresh2
      end
      object ListBox4: TListBox
        Left = 991
        Top = 0
        Width = 282
        Height = 169
        BevelInner = bvNone
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 10
        OnDblClick = ListBox4DblClick
      end
      object Button12: TButton
        Left = 919
        Top = 64
        Width = 54
        Height = 25
        Caption = 'Button12'
        TabOrder = 11
        OnClick = Button12Click
      end
    end
    object TabSheet4: TTabSheet
      Caption = #1058#1086#1088#1094#1099
      ImageIndex = 4
      object Chart2: TChart
        Left = 0
        Top = 21
        Width = 1220
        Height = 151
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 2
        MarginLeft = 0
        MarginRight = 0
        MarginTop = 5
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.Maximum = 25.000000000000000000
        LeftAxis.Visible = False
        Legend.Visible = False
        View3D = False
        OnBeforeDrawSeries = Chart1BeforeDrawSeries
        Align = alClient
        Color = clWindow
        TabOrder = 0
        object LineSeries1: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Width = 2
          Pointer.Brush.Color = clRed
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object LineSeries2: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 51813
          LinePen.Width = 2
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series8: TLineSeries
          Active = False
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Style = psDash
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series9: TLineSeries
          Active = False
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clLime
          LinePen.Style = psDash
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1220
        Height = 21
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object CheckBox5: TCheckBox
          Left = 0
          Top = 1
          Width = 55
          Height = 17
          Caption = #1054#1089#1100' Y'
          TabOrder = 0
          OnClick = CheckBox5Click
        end
        object cbEnd: TCheckBox
          Left = 58
          Top = 1
          Width = 67
          Height = 17
          Caption = #1058#1086#1088#1094#1099
          TabOrder = 1
          OnClick = MyRefresh
        end
        object CheckBox34: TCheckBox
          Left = 421
          Top = 1
          Width = 123
          Height = 17
          Caption = #1058#1086#1088#1094#1099' 2 '#1056#1077#1079#1091#1083#1100#1090#1072#1090
          TabOrder = 2
          OnClick = MyRefresh
        end
        object ShowBJMask: TCheckBox
          Left = 552
          Top = 1
          Width = 59
          Height = 17
          Caption = #1052#1072#1089#1082#1072
          TabOrder = 3
          OnClick = MyRefresh
        end
        object ComboBox17: TComboBox
          Left = 655
          Top = 0
          Width = 74
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 4
          Text = 'SUMM'
          Items.Strings = (
            'SUMM'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7')
        end
        object CheckBox47: TCheckBox
          Left = 952
          Top = 2
          Width = 97
          Height = 17
          Caption = #1056#1077#1078#1080#1084
          TabOrder = 5
          OnClick = MyRefresh
        end
        object ComboBox18: TComboBox
          Left = 736
          Top = 0
          Width = 65
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 6
          Text = #1051#1077#1074#1072#1103
          Items.Strings = (
            #1051#1077#1074#1072#1103
            #1055#1088#1072#1074#1072#1103)
        end
        object cbBSHole: TCheckBox
          Left = 116
          Top = 1
          Width = 67
          Height = 17
          Caption = #1044#1099#1088#1082#1080
          TabOrder = 7
          OnClick = MyRefresh
        end
        object cbBoltHoles: TCheckBox
          Left = 183
          Top = 1
          Width = 82
          Height = 17
          Caption = #1054#1090#1074#1077#1088#1089#1090#1080#1103
          TabOrder = 8
          OnClick = MyRefresh
        end
      end
    end
    object TabSheet19: TTabSheet
      Caption = #1054#1090#1074#1077#1088#1089#1090#1080#1103
      ImageIndex = 17
      object Panel27: TPanel
        Left = 0
        Top = 0
        Width = 137
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label19: TLabel
          Left = 8
          Top = 137
          Width = 41
          Height = 13
          Caption = #1042#1099#1089#1086#1090#1072':'
        end
        object BJSign: TCheckBox
          Left = 8
          Top = 17
          Width = 97
          Height = 17
          Caption = #1055#1088#1080#1079#1085#1072#1082#1080
          TabOrder = 0
          OnClick = BHCheckClickCheck
        end
        object CheckBox48: TCheckBox
          Left = 17
          Top = 35
          Width = 97
          Height = 17
          Caption = #1050#1072#1085#1072#1083' 0'
          Checked = True
          State = cbChecked
          TabOrder = 1
          OnClick = MyRefresh
        end
        object CheckBox49: TCheckBox
          Left = 17
          Top = 51
          Width = 97
          Height = 17
          Caption = #1050#1072#1085#1072#1083' 1'
          Checked = True
          State = cbChecked
          TabOrder = 2
          OnClick = MyRefresh
        end
        object CheckBox50: TCheckBox
          Left = 17
          Top = 67
          Width = 97
          Height = 17
          Caption = #1050#1072#1085#1072#1083' 6'
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = MyRefresh
        end
        object CheckBox51: TCheckBox
          Left = 17
          Top = 83
          Width = 97
          Height = 17
          Caption = #1050#1072#1085#1072#1083' 7'
          Checked = True
          State = cbChecked
          TabOrder = 4
          OnClick = MyRefresh
        end
        object ScrollBar4: TScrollBar
          Left = 8
          Top = 156
          Width = 120
          Height = 14
          Max = 500
          Min = 50
          PageSize = 0
          Position = 50
          TabOrder = 5
          OnChange = ScrollBar4Change
        end
        object ShowBJ: TCheckBox
          Left = 8
          Top = 1
          Width = 80
          Height = 17
          Caption = #1054#1090#1074#1077#1088#1089#1090#1080#1103
          TabOrder = 6
          OnClick = MyRefresh
        end
        object BJSignIdx: TCheckBox
          Left = 8
          Top = 102
          Width = 129
          Height = 17
          Caption = #1048#1085#1076#1077#1082#1089#1099' '#1087#1088#1080#1079#1085#1072#1082#1086#1074
          Checked = True
          State = cbChecked
          TabOrder = 7
          OnClick = MyRefresh
        end
        object ShowBJIdx: TCheckBox
          Left = 8
          Top = 118
          Width = 129
          Height = 17
          Caption = #1048#1085#1076#1077#1082#1089' '#1086#1090#1074#1077#1088#1089#1090#1080#1103
          TabOrder = 8
          OnClick = MyRefresh
        end
      end
      object Panel28: TPanel
        Left = 137
        Top = 0
        Width = 185
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label18: TLabel
          Left = 11
          Top = 111
          Width = 55
          Height = 13
          Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090':'
        end
        object eTestIdx1: TEdit
          Left = 10
          Top = 83
          Width = 50
          Height = 21
          TabOrder = 0
          Text = '0'
        end
        object eTestIdx2: TEdit
          Left = 80
          Top = 83
          Width = 50
          Height = 21
          TabOrder = 1
          Text = '1'
        end
        object Button11: TButton
          Left = 34
          Top = 132
          Width = 75
          Height = 25
          Caption = 'Test'
          TabOrder = 2
          OnClick = Button11Click
        end
        object cbTestRail: TComboBox
          Left = 8
          Top = 35
          Width = 124
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 3
          Text = #1051#1077#1074#1072#1103
          OnChange = ComboBox13Change
          Items.Strings = (
            #1051#1077#1074#1072#1103
            #1055#1088#1072#1074#1072#1103)
        end
        object cbTestMode: TComboBox
          Left = 8
          Top = 12
          Width = 125
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 4
          Text = #1055#1077#1088#1077#1089#1077#1095#1077#1085#1080#1077
          Items.Strings = (
            #1055#1077#1088#1077#1089#1077#1095#1077#1085#1080#1077
            #1042#1099#1089#1086#1090#1072)
        end
        object cbTestCh: TComboBox
          Left = 8
          Top = 57
          Width = 124
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 5
          Text = #1050#1072#1085#1072#1083' 0'
          OnChange = ComboBox2Change
          Items.Strings = (
            #1050#1072#1085#1072#1083' 0'
            #1050#1072#1085#1072#1083' 1'
            #1050#1072#1085#1072#1083' 2'
            #1050#1072#1085#1072#1083' 3'
            #1050#1072#1085#1072#1083' 4'
            #1050#1072#1085#1072#1083' 5'
            #1050#1072#1085#1072#1083' 6'
            #1050#1072#1085#1072#1083' 7')
        end
      end
    end
    object TabSheet12: TTabSheet
      Caption = #1069#1083#1077#1084#1077#1085#1090#1099' '#1087#1091#1090#1080
      ImageIndex = 11
      object Label24: TLabel
        Left = 143
        Top = 26
        Width = 33
        Height = 13
        Caption = #1058#1086#1095#1082#1080':'
      end
      object Label4: TLabel
        Left = 262
        Top = 4
        Width = 149
        Height = 13
        Caption = #1047#1086#1085#1099' '#1076#1083#1103' '#1087#1086#1080#1089#1082#1072' '#1074#1085#1077' '#1051#1069
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Bevel2: TBevel
        Left = 253
        Top = 4
        Width = 2
        Height = 164
      end
      object Bevel3: TBevel
        Left = 137
        Top = 4
        Width = 2
        Height = 164
      end
      object Label14: TLabel
        Left = 264
        Top = 112
        Width = 201
        Height = 13
        Caption = #1053#1077#1087#1088#1086#1082#1086#1085#1090#1088#1086#1083#1080#1088#1086#1074#1072#1085#1085#1099#1077' '#1091#1095#1072#1089#1090#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object CheckBox9: TCheckBox
        Left = 143
        Top = 4
        Width = 97
        Height = 17
        Caption = #1047#1086#1085#1099' '#1050#1069
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnClick = MyRefresh
      end
      object PointCB1: TRadioButton
        Left = 152
        Top = 65
        Width = 99
        Height = 17
        Caption = #1048#1089#1093#1086#1076#1085#1099#1077
        TabOrder = 1
        OnClick = MyRefresh
      end
      object PointCB2: TRadioButton
        Left = 152
        Top = 83
        Width = 99
        Height = 17
        Caption = #1055#1077#1088#1077#1085#1077#1089#1077#1085#1085#1099#1077
        TabOrder = 2
        OnClick = MyRefresh
      end
      object RadioButton1: TRadioButton
        Left = 152
        Top = 47
        Width = 99
        Height = 17
        Caption = #1042#1099#1082#1083
        Checked = True
        TabOrder = 3
        TabStop = True
        OnClick = MyRefresh
      end
      object CheckBox23: TCheckBox
        Left = 262
        Top = 26
        Width = 97
        Height = 17
        Caption = #1056#1072#1073#1086#1095#1080#1077' '#1079#1086#1085#1099
        TabOrder = 4
        OnClick = MyRefresh
      end
      object PointWZ: TCheckBox
        Left = 262
        Top = 68
        Width = 165
        Height = 17
        Caption = #1058#1086#1095#1082#1080' '#1088#1072#1089#1095#1077#1090#1072' '#1088#1072#1073#1086#1095#1080#1093' '#1079#1086#1085
        TabOrder = 5
        OnClick = MyRefresh
      end
      object BHCheck: TCheckListBox
        Left = 2
        Top = 3
        Width = 130
        Height = 105
        OnClickCheck = BHCheckClickCheck
        BevelInner = bvNone
        BevelOuter = bvNone
        Ctl3D = False
        ItemHeight = 13
        Items.Strings = (
          ''
          #1041#1086#1083#1090#1086#1074#1099#1077' '#1054#1090#1074#1077#1088#1089#1090#1080#1103
          #1058#1086#1088#1094#1099
          #1041#1086#1083#1100#1096#1080#1077' '#1044#1099#1088#1082#1080
          #1054#1089#1090#1088#1103#1082#1080)
        ParentCtl3D = False
        TabOrder = 6
      end
      object CheckBox8: TCheckBox
        Left = 262
        Top = 45
        Width = 139
        Height = 17
        Caption = #1053#1086#1084#1077#1088#1072' '#1088#1072#1073#1086#1095#1080#1093' '#1079#1086#1085
        TabOrder = 7
        OnClick = MyRefresh
      end
      object ConstCheck: TCheckListBox
        Left = 546
        Top = 3
        Width = 183
        Height = 142
        OnClickCheck = MyRefresh
        BevelInner = bvNone
        BevelOuter = bvNone
        Ctl3D = False
        ItemHeight = 13
        Items.Strings = (
          #1042#1089#1077
          #1054#1076#1080#1085#1086#1095#1085#1086#1077' '#1086#1090#1074#1077#1088#1089#1090#1080#1077
          #1064#1090#1077#1087#1089#1077#1083#1100
          #1057#1074#1072#1088#1085#1086#1081' '#1089#1090#1099#1082
          #1041#1086#1083#1090#1086#1074#1086#1081' '#1089#1090#1099#1082
          #1050#1086#1085#1090#1088' '#1088#1077#1083#1100#1089
          #1054#1089#1090#1088#1103#1082' '#1089#1090#1088#1077#1083#1086#1095#1085#1086#1075#1086' '#1087#1077#1088#1077#1074#1086#1076#1072
          #1050#1088#1077#1089#1090#1086#1074#1080#1085#1072
          #1056#1072#1084#1085#1099#1081' '#1088#1077#1083#1100#1089)
        ParentCtl3D = False
        TabOrder = 8
      end
      object CheckBox31: TCheckBox
        Left = 264
        Top = 89
        Width = 97
        Height = 17
        Caption = #1044#1077#1092#1077#1082#1090#1099
        TabOrder = 9
        OnClick = MyRefresh
      end
      object CheckBox15: TCheckBox
        Left = 262
        Top = 130
        Width = 75
        Height = 17
        Caption = #1042#1089#1077' '#1090#1086#1095#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        WordWrap = True
        OnClick = MyRefresh
      end
      object CheckBox36: TCheckBox
        Left = 342
        Top = 130
        Width = 155
        Height = 17
        Caption = #1054#1090#1092#1080#1083#1100#1090#1088#1086#1074#1072#1085#1085#1099#1077' '#1090#1086#1095#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
        WordWrap = True
        OnClick = MyRefresh
      end
      object CheckBox35: TCheckBox
        Left = 262
        Top = 149
        Width = 155
        Height = 17
        Caption = #1047#1086#1085#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 12
        WordWrap = True
        OnClick = MyRefresh
      end
    end
    object TabSheet9: TTabSheet
      Caption = #1064#1091#1084#1086#1074#1099#1077' '#1076#1086#1088#1086#1078#1082#1080
      ImageIndex = 8
      object Chart5: TChart
        Left = 264
        Top = 0
        Width = 771
        Height = 172
        BackWall.Brush.Color = clWhite
        MarginBottom = 1
        MarginLeft = 1
        MarginRight = 1
        MarginTop = 1
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.DateTimeFormat = 'dd.MM.yyyy'
        Legend.Visible = False
        View3D = False
        Align = alClient
        BevelOuter = bvNone
        Color = clWindow
        TabOrder = 0
        object Series4: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clGreen
          LinePen.Width = 2
          Pointer.HorizSize = 3
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.VertSize = 3
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series5: TPointSeries
          Marks.ArrowLength = 0
          Marks.Visible = False
          SeriesColor = clYellow
          Pointer.InflateMargins = True
          Pointer.Style = psDownTriangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series6: TPointSeries
          Marks.ArrowLength = 0
          Marks.Visible = False
          SeriesColor = clBlue
          Pointer.Brush.Color = clYellow
          Pointer.InflateMargins = True
          Pointer.Style = psTriangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object Panel15: TPanel
        Left = 0
        Top = 0
        Width = 264
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 161
          Height = 169
          ActivePage = TabSheet14
          TabOrder = 0
          object Debug: TTabSheet
            Caption = 'Debug'
            TabVisible = False
            object Button2: TButton
              Left = 3
              Top = 5
              Width = 144
              Height = 25
              Caption = 'Get'
              TabOrder = 0
              OnClick = Button2Click
            end
            object ComboBox7: TComboBox
              Left = 3
              Top = 33
              Width = 145
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              ItemIndex = 0
              TabOrder = 1
              Text = #1051#1077#1074#1072#1103
              OnChange = ComboBox2Change
              Items.Strings = (
                #1051#1077#1074#1072#1103
                #1055#1088#1072#1074#1072#1103)
            end
            object ComboBox8: TComboBox
              Left = 3
              Top = 56
              Width = 145
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 2
              OnChange = ComboBox2Change
              Items.Strings = (
                #1050#1072#1085#1072#1083' 1'
                #1050#1072#1085#1072#1083' 2'
                #1050#1072#1085#1072#1083' 3'
                #1050#1072#1085#1072#1083' 4'
                #1050#1072#1085#1072#1083' 5'
                #1050#1072#1085#1072#1083' 6'
                #1050#1072#1085#1072#1083' 7')
            end
            object CheckBox24: TCheckBox
              Left = 6
              Top = 80
              Width = 105
              Height = 17
              Caption = #1060#1080#1083#1100#1090#1088#1072#1094#1080#1103
              Checked = True
              State = cbChecked
              TabOrder = 3
            end
            object CheckBox4: TCheckBox
              Left = 6
              Top = 95
              Width = 141
              Height = 18
              Caption = #1042#1086#1089#1090'. '#1079#1086#1085#1072' '#1087#1072#1095#1082#1080
              Checked = True
              State = cbChecked
              TabOrder = 4
              WordWrap = True
            end
          end
          object TabSheet14: TTabSheet
            Caption = 'View Res'
            ImageIndex = 1
            object Label15: TLabel
              Left = 4
              Top = 0
              Width = 25
              Height = 13
              Caption = #1053#1080#1090#1100
            end
            object Label16: TLabel
              Left = 4
              Top = 39
              Width = 25
              Height = 13
              Caption = #1047#1086#1085#1072
            end
            object Label17: TLabel
              Left = 4
              Top = 77
              Width = 31
              Height = 13
              Caption = #1050#1072#1085#1072#1083
            end
            object ComboBox9: TComboBox
              Left = 3
              Top = 15
              Width = 145
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              ItemIndex = 0
              TabOrder = 0
              Text = #1051#1077#1074#1072#1103
              OnChange = ComboBox13Change
              Items.Strings = (
                #1051#1077#1074#1072#1103
                #1055#1088#1072#1074#1072#1103)
            end
            object ComboBox10: TComboBox
              Left = 3
              Top = 54
              Width = 145
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 1
              OnChange = ComboBox13Change
              Items.Strings = (
                #1050#1072#1085#1072#1083' 1'
                #1050#1072#1085#1072#1083' 2'
                #1050#1072#1085#1072#1083' 3'
                #1050#1072#1085#1072#1083' 4'
                #1050#1072#1085#1072#1083' 5'
                #1050#1072#1085#1072#1083' 6'
                #1050#1072#1085#1072#1083' 7')
            end
            object ComboBox13: TComboBox
              Left = 3
              Top = 92
              Width = 145
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 2
              OnChange = ComboBox13Change
              Items.Strings = (
                #1050#1072#1085#1072#1083' 1'
                #1050#1072#1085#1072#1083' 2'
                #1050#1072#1085#1072#1083' 3'
                #1050#1072#1085#1072#1083' 4'
                #1050#1072#1085#1072#1083' 5'
                #1050#1072#1085#1072#1083' 6'
                #1050#1072#1085#1072#1083' 7')
            end
          end
        end
        object CheckBox22: TCheckBox
          Left = 167
          Top = 132
          Width = 90
          Height = 33
          Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1085#1072'  '#1041' '#1088#1072#1079#1074#1077#1088#1082#1077
          Checked = True
          State = cbChecked
          TabOrder = 1
          Visible = False
          WordWrap = True
          OnClick = MyRefresh
        end
        object CheckBox14: TCheckBox
          Left = 168
          Top = 6
          Width = 90
          Height = 33
          Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1085#1072'  '#1041' '#1088#1072#1079#1074#1077#1088#1082#1077
          TabOrder = 2
          WordWrap = True
          OnClick = MyRefresh
        end
      end
      object Panel18: TPanel
        Left = 1035
        Top = 0
        Width = 185
        Height = 172
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object Memo2: TMemo
          Left = 0
          Top = 0
          Width = 185
          Height = 172
          Align = alClient
          ScrollBars = ssVertical
          TabOrder = 0
          Visible = False
        end
      end
    end
    object TabSheet13: TTabSheet
      Caption = #1058#1077#1093#1085#1086#1083#1086#1075#1080#1103
      ImageIndex = 12
      object CheckBox10: TCheckBox
        Left = 8
        Top = 8
        Width = 129
        Height = 17
        Caption = #1044#1074#1086#1081#1085#1086#1081' '#1087#1088#1086#1093#1086#1076' '#1041#1057
        TabOrder = 0
        OnClick = MyRefresh
      end
      object CheckBox30: TCheckBox
        Left = 8
        Top = 28
        Width = 129
        Height = 17
        Caption = #1056#1077#1078#1080#1084' '#1082#1086#1085#1090#1088#1086#1083#1103' '#1041#1057
        TabOrder = 1
        OnClick = MyRefresh
      end
    end
    object TabSheet8: TTabSheet
      Caption = #1040#1085#1072#1083#1080#1079' '#1087#1072#1095#1077#1082
      ImageIndex = 7
      object Splitter1: TSplitter
        Left = 889
        Top = 0
        Width = 6
        Height = 172
        ResizeStyle = rsUpdate
      end
      object DelayChart: TChart
        Left = 895
        Top = 0
        Width = 325
        Height = 172
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Legend.Visible = False
        View3D = False
        Align = alClient
        TabOrder = 0
        object SrcDelaySeries: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object DstDelaySeries: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clLime
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 161
        Height = 172
        Align = alLeft
        TabOrder = 1
        Visible = False
        object Label5: TLabel
          Left = 8
          Top = 83
          Width = 14
          Height = 13
          Caption = 'Idx'
        end
        object ComboBox2: TComboBox
          Left = 8
          Top = 8
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 0
          Text = #1051#1077#1074#1072#1103
          OnChange = ComboBox2Change
          Items.Strings = (
            #1051#1077#1074#1072#1103
            #1055#1088#1072#1074#1072#1103)
        end
        object ScrollBar1: TScrollBar
          Left = 8
          Top = 59
          Width = 143
          Height = 17
          PageSize = 0
          TabOrder = 1
        end
        object ComboBox5: TComboBox
          Left = 8
          Top = 32
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 3
          TabOrder = 2
          Text = #1050#1072#1085#1072#1083' 0'
          OnChange = ComboBox2Change
          Items.Strings = (
            #1050#1072#1085#1072#1083' 6'
            #1050#1072#1085#1072#1083' 7'
            #1050#1072#1085#1072#1083' 1'
            #1050#1072#1085#1072#1083' 0')
        end
        object SpinEdit1: TSpinEdit
          Left = 8
          Top = 104
          Width = 65
          Height = 22
          MaxValue = 999999
          MinValue = 0
          TabOrder = 3
          Value = 0
        end
        object Button4: TButton
          Left = 80
          Top = 104
          Width = 49
          Height = 29
          Caption = 'Set'
          TabOrder = 4
        end
      end
      object AmplChart: TChart
        Left = 433
        Top = 0
        Width = 456
        Height = 172
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Legend.Visible = False
        View3D = False
        Align = alLeft
        TabOrder = 2
        object ScrollBar3: TScrollBar
          Left = 0
          Top = 0
          Width = 121
          Height = 17
          Max = 12
          Min = 1
          PageSize = 0
          Position = 1
          TabOrder = 0
          OnChange = ScrollBar3Change
        end
        object SrcAmplSeries: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object DstAmplSeries: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 65408
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object Panel16: TPanel
        Left = 161
        Top = 0
        Width = 272
        Height = 172
        Align = alLeft
        TabOrder = 3
        object Label7: TLabel
          Left = 225
          Top = 22
          Width = 32
          Height = 13
          Caption = 'Label7'
        end
        object Label8: TLabel
          Left = 225
          Top = 59
          Width = 32
          Height = 13
          Caption = 'Label7'
        end
        object Label9: TLabel
          Left = 225
          Top = 94
          Width = 32
          Height = 13
          Caption = 'Label7'
        end
        object Label10: TLabel
          Left = 225
          Top = 128
          Width = 32
          Height = 13
          Caption = 'Label7'
        end
        object TrackBar1: TTrackBar
          Left = 3
          Top = 22
          Width = 214
          Height = 15
          Max = 100
          Position = 50
          TabOrder = 0
          ThumbLength = 12
          TickStyle = tsNone
          OnChange = PBTHChange
        end
        object CheckBox18: TCheckBox
          Left = 8
          Top = 4
          Width = 97
          Height = 17
          Caption = #1047#1072#1076#1077#1088#1078#1082#1072
          TabOrder = 1
        end
        object TrackBar2: TTrackBar
          Left = 3
          Top = 59
          Width = 214
          Height = 15
          Max = 100
          Position = 50
          TabOrder = 2
          ThumbLength = 12
          TickStyle = tsNone
          OnChange = PBTHChange
        end
        object CheckBox19: TCheckBox
          Left = 8
          Top = 41
          Width = 97
          Height = 17
          Caption = #1040#1084#1087#1083#1080#1090#1091#1076#1072
          TabOrder = 3
        end
        object TrackBar3: TTrackBar
          Left = 3
          Top = 94
          Width = 214
          Height = 15
          Max = 200
          Position = 100
          TabOrder = 4
          ThumbLength = 12
          TickStyle = tsNone
          OnChange = PBTHChange
        end
        object CheckBox20: TCheckBox
          Left = 8
          Top = 76
          Width = 97
          Height = 17
          Caption = #1057#1091#1084#1084#1072
          TabOrder = 5
        end
        object TrackBar4: TTrackBar
          Left = 3
          Top = 128
          Width = 214
          Height = 15
          Max = 100
          Position = 50
          TabOrder = 6
          ThumbLength = 12
          TickStyle = tsNone
          OnChange = PBTHChange
        end
        object CheckBox21: TCheckBox
          Left = 8
          Top = 110
          Width = 97
          Height = 17
          Caption = #1047#1072#1087#1086#1083#1085#1077#1085#1080#1077
          TabOrder = 7
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1057#1080#1085#1093#1088#1086#1085#1080#1079#1072#1094#1080#1103
      ImageIndex = 3
      object Chart1: TChart
        Left = 347
        Top = 26
        Width = 873
        Height = 146
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 2
        MarginLeft = 1
        MarginRight = 1
        MarginTop = 13
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        Legend.Visible = False
        View3D = False
        OnBeforeDrawSeries = Chart1BeforeDrawSeries
        Align = alClient
        TabOrder = 0
        object Series1: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object BMGraphList: TCheckListBox
        Left = 0
        Top = 26
        Width = 160
        Height = 146
        OnClickCheck = MyRefresh
        Align = alLeft
        ItemHeight = 13
        TabOrder = 1
        OnClick = BMGraphListClick
      end
      object BMListItems: TCheckListBox
        Left = 160
        Top = 26
        Width = 187
        Height = 146
        OnClickCheck = BMListItemsClickCheck
        Align = alLeft
        ItemHeight = 13
        TabOrder = 2
        OnDblClick = BMListItemsDblClick
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 1220
        Height = 26
        Align = alTop
        TabOrder = 3
        object CheckBox1: TCheckBox
          Left = 5
          Top = 4
          Width = 52
          Height = 17
          Caption = #1042#1082#1083' 1'
          TabOrder = 0
          OnClick = MyRefresh
        end
        object Panel4: TPanel
          Left = 1058
          Top = 1
          Width = 161
          Height = 24
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object ComboBox1: TComboBox
            Left = 69
            Top = 2
            Width = 90
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 0
            Text = #1051#1077#1074#1099#1081
            OnChange = ComboBox1Change
            Items.Strings = (
              #1051#1077#1074#1099#1081
              #1055#1088#1072#1074#1099#1081)
          end
          object CheckBox7: TCheckBox
            Left = 3
            Top = 3
            Width = 62
            Height = 17
            Caption = #1057#1087#1080#1089#1082#1080
            Checked = True
            State = cbChecked
            TabOrder = 1
            OnClick = CheckBox7Click
          end
        end
        object CheckBox3: TCheckBox
          Left = 60
          Top = 4
          Width = 52
          Height = 17
          Caption = #1042#1082#1083' 2'
          TabOrder = 2
          OnClick = MyRefresh
        end
        object ComboBox6: TComboBox
          Left = 118
          Top = 3
          Width = 61
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 3
          OnChange = MyRefresh
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = #1057#1080#1085#1093#1088#1086'2'
      ImageIndex = 7
      object Splitter3: TSplitter
        Left = 969
        Top = 0
        Width = 5
        Height = 172
        Align = alRight
        ResizeStyle = rsUpdate
      end
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 65
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 65
          Height = 62
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Button1: TButton
            Left = 1
            Top = 5
            Width = 58
            Height = 25
            Caption = 'Get'
            TabOrder = 0
          end
          object ComboBox3: TComboBox
            Left = 1
            Top = 36
            Width = 59
            Height = 21
            Style = csDropDownList
            ItemHeight = 0
            TabOrder = 1
            OnChange = ListBox1Click
          end
        end
        object Panel13: TPanel
          Left = 0
          Top = 62
          Width = 65
          Height = 110
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label6: TLabel
            Left = 22
            Top = 26
            Width = 16
            Height = 13
            Caption = 'P: -'
          end
          object ScrollBar2: TScrollBar
            Left = 0
            Top = 0
            Width = 16
            Height = 110
            Align = alLeft
            Kind = sbVertical
            Max = 50
            Min = -50
            PageSize = 0
            TabOrder = 0
            OnChange = ScrollBar2Change
          end
          object CheckBox11: TCheckBox
            Left = 25
            Top = 6
            Width = 33
            Height = 17
            Caption = 'G'
            TabOrder = 1
          end
          object ComboBox4: TComboBox
            Left = 22
            Top = 45
            Width = 39
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 2
            Text = #1051#1077#1074#1099#1081
            OnChange = ListBox1Click
            Items.Strings = (
              #1051#1077#1074#1099#1081
              #1055#1088#1072#1074#1099#1081)
          end
        end
      end
      object ListBox1: TListBox
        Left = 65
        Top = 0
        Width = 80
        Height = 172
        Align = alLeft
        ItemHeight = 13
        TabOrder = 1
        OnClick = ListBox1Click
      end
      object Panel12: TPanel
        Left = 145
        Top = 0
        Width = 824
        Height = 172
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object PaintBox1: TPaintBox
          Left = 0
          Top = 0
          Width = 826
          Height = 93
          Align = alClient
        end
        object Chart3: TChart
          Left = 0
          Top = 93
          Width = 826
          Height = 79
          BackWall.Brush.Color = clWhite
          BackWall.Brush.Style = bsClear
          MarginLeft = 0
          MarginRight = 0
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          LeftAxis.EndPosition = 99.000000000000000000
          LeftAxis.Visible = False
          Legend.Visible = False
          View3D = False
          Align = alBottom
          TabOrder = 0
          object Series2: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clRed
            LinePen.Width = 2
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
        end
      end
      object Chart4: TChart
        Left = 974
        Top = 0
        Width = 246
        Height = 172
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        Legend.Visible = False
        View3D = False
        Align = alRight
        TabOrder = 3
        object Series3: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'KF'
      ImageIndex = 8
      object Chart7: TChart
        Left = 0
        Top = 0
        Width = 552
        Height = 172
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Legend.Visible = False
        View3D = False
        Align = alLeft
        TabOrder = 0
        object LineSeries5: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object Chart8: TChart
        Left = 552
        Top = 0
        Width = 508
        Height = 172
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Legend.Visible = False
        View3D = False
        Align = alClient
        TabOrder = 1
        object LineSeries6: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
    end
    object TabSheet10: TTabSheet
      Caption = 'Ampl'
      ImageIndex = 9
      object Panel17: TPanel
        Left = 0
        Top = 0
        Width = 161
        Height = 172
        Align = alLeft
        TabOrder = 0
        object ComboBox11: TComboBox
          Left = 8
          Top = 8
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 0
          Text = #1051#1077#1074#1072#1103
          OnChange = ComboBox2Change
          Items.Strings = (
            #1051#1077#1074#1072#1103
            #1055#1088#1072#1074#1072#1103)
        end
        object ComboBox12: TComboBox
          Left = 8
          Top = 32
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 2
          TabOrder = 1
          Text = #1050#1072#1085#1072#1083' 2'
          OnChange = ComboBox2Change
          Items.Strings = (
            #1050#1072#1085#1072#1083' 0'
            #1050#1072#1085#1072#1083' 1'
            #1050#1072#1085#1072#1083' 2'
            #1050#1072#1085#1072#1083' 3'
            #1050#1072#1085#1072#1083' 4'
            #1050#1072#1085#1072#1083' 5'
            #1050#1072#1085#1072#1083' 6'
            #1050#1072#1085#1072#1083' 7')
        end
        object Button6: TButton
          Left = 56
          Top = 80
          Width = 49
          Height = 29
          Caption = 'Get'
          TabOrder = 2
          OnClick = Button6Click
        end
      end
      object Chart9: TChart
        Left = 161
        Top = 0
        Width = 1059
        Height = 172
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        Legend.Visible = False
        View3D = False
        View3DWalls = False
        Align = alClient
        TabOrder = 1
        object Series7: TLineSeries
          Marks.ArrowLength = 20
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Width = 4
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
    end
    object TabSheet15: TTabSheet
      Caption = #1054#1090#1074#1077#1088#1089#1090#1080#1103
      ImageIndex = 13
      object HoleList: TListBox
        Left = 84
        Top = 0
        Width = 177
        Height = 172
        Align = alLeft
        ItemHeight = 13
        TabOrder = 0
        OnClick = HoleListClick
      end
      object Chart11: TChart
        Left = 798
        Top = 0
        Width = 422
        Height = 172
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        Legend.Visible = False
        View3D = False
        Align = alClient
        TabOrder = 1
        Visible = False
        object Series10: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Width = 3
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object Panel20: TPanel
        Left = 0
        Top = 0
        Width = 84
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        object Button3: TButton
          Left = 2
          Top = 5
          Width = 75
          Height = 21
          Caption = 'List'
          TabOrder = 0
          OnClick = Button3Click
        end
        object Button5: TButton
          Left = 1
          Top = 32
          Width = 75
          Height = 21
          Caption = 'Graph'
          TabOrder = 1
          OnClick = Button5Click
        end
        object Button8: TButton
          Left = 1
          Top = 61
          Width = 75
          Height = 21
          Caption = '>>>'
          TabOrder = 2
          OnClick = Button8Click
        end
        object Button9: TButton
          Left = 1
          Top = 88
          Width = 75
          Height = 21
          Caption = '<<<'
          TabOrder = 3
          OnClick = Button9Click
        end
        object Panel23: TPanel
          Left = 8
          Top = 120
          Width = 57
          Height = 33
          Caption = #1044#1077#1092'.'
          TabOrder = 4
        end
      end
      object Panel22: TPanel
        Left = 261
        Top = 0
        Width = 452
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 3
        OnResize = Panel22Resize
        object Chart10: TChart
          Left = 0
          Top = 85
          Width = 452
          Height = 87
          BackWall.Brush.Color = clWhite
          BackWall.Brush.Style = bsClear
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.DateTimeFormat = 'dd.MM.yyyy'
          Legend.Visible = False
          View3D = False
          View3DOptions.Elevation = 352
          View3DOptions.Orthogonal = False
          View3DOptions.Rotation = 294
          View3DOptions.Zoom = 69
          View3DWalls = False
          Align = alClient
          TabOrder = 0
        end
        object Chart13: TChart
          Left = 0
          Top = 0
          Width = 452
          Height = 85
          BackWall.Brush.Color = clWhite
          BackWall.Brush.Style = bsClear
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.DateTimeFormat = 'dd.MM.yyyy'
          Legend.Visible = False
          View3D = False
          View3DOptions.Elevation = 352
          View3DOptions.Orthogonal = False
          View3DOptions.Rotation = 294
          View3DOptions.Zoom = 69
          View3DWalls = False
          Align = alTop
          TabOrder = 1
        end
      end
      object Chart14: TChart
        Left = 713
        Top = 0
        Width = 85
        Height = 172
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.DateTimeFormat = 'dd.MM.yyyy'
        Legend.Visible = False
        View3D = False
        View3DOptions.Elevation = 352
        View3DOptions.Orthogonal = False
        View3DOptions.Rotation = 294
        View3DOptions.Zoom = 69
        View3DWalls = False
        Align = alLeft
        TabOrder = 4
      end
    end
    object TabSheet18: TTabSheet
      Caption = #1060#1080#1083#1100#1090#1088#1072#1094#1080#1103
      ImageIndex = 16
      object Panel24: TPanel
        Left = 120
        Top = 0
        Width = 169
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object CheckBox42: TCheckBox
          Left = 6
          Top = 4
          Width = 45
          Height = 17
          Caption = 'Hide'
          TabOrder = 0
          OnClick = MyRefresh
        end
        object CheckBox43: TCheckBox
          Left = 6
          Top = 24
          Width = 50
          Height = 17
          Caption = 'Color'
          TabOrder = 1
          OnClick = MyRefresh
        end
        object CheckBox45: TCheckBox
          Left = 6
          Top = 44
          Width = 139
          Height = 17
          Caption = #1060#1080#1083#1100#1090#1088#1072#1094#1080#1103' - '#1082#1086#1088#1080#1076#1086#1088
          TabOrder = 2
          OnClick = MyRefresh
        end
        object CheckBox46: TCheckBox
          Left = 6
          Top = 64
          Width = 97
          Height = 17
          Caption = #1060#1080#1083#1100#1090#1088#1072#1094#1080#1103' 2'
          TabOrder = 3
          OnClick = MyRefresh
        end
        object CheckBox52: TCheckBox
          Left = 6
          Top = 84
          Width = 139
          Height = 17
          Caption = #1047#1072#1076#1077#1088#1078#1082#1072' + '#1040#1084#1087#1083#1080#1090#1091#1076#1072
          TabOrder = 4
          OnClick = MyRefresh
        end
        object CheckBox53: TCheckBox
          Left = 6
          Top = 104
          Width = 147
          Height = 17
          Caption = #1052#1072#1082#1089#1080#1084#1091#1084' '#1087#1086' 1 '#1082#1072#1085#1072#1083#1091
          TabOrder = 5
          OnClick = MyRefresh
        end
      end
      object Panel25: TPanel
        Left = 0
        Top = 0
        Width = 120
        Height = 172
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object CheckListBox1: TCheckListBox
          Left = 0
          Top = 19
          Width = 121
          Height = 153
          OnClickCheck = MyRefresh
          Align = alLeft
          ItemHeight = 13
          Items.Strings = (
            '- 6 '#1076#1041
            '- 4 '#1076#1041
            '- 2 '#1076#1041
            '  0 '#1076#1041
            '  2 '#1076#1041
            '  4 '#1076#1041
            '  6 '#1076#1041
            '  8 '#1076#1041
            ' 10 '#1076#1041
            ' 12 '#1076#1041
            ' 14 '#1076#1041
            ' 16 '#1076#1041
            ' 18 '#1076#1041)
          TabOrder = 0
        end
        object Panel26: TPanel
          Left = 0
          Top = 0
          Width = 120
          Height = 19
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object CheckBox44: TCheckBox
            Left = 8
            Top = 0
            Width = 97
            Height = 17
            Caption = #1042#1082#1083'.'
            TabOrder = 0
            OnClick = MyRefresh
          end
        end
      end
    end
    object Memo: TTabSheet
      Caption = #1047#1086#1085#1099' '#1087#1086#1080#1089#1082#1072
      ImageIndex = 17
      object CheckBox38: TCheckBox
        Left = 1
        Top = 20
        Width = 281
        Height = 19
        Caption = #1050#1085#1086#1087#1082#1072' '#1041#1057' ('#1087#1086' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080') '#1089' '#1087#1077#1088#1077#1085#1086#1089#1086#1084' '#1080#1079' '#1047#1044#1053
        TabOrder = 0
        WordWrap = True
        OnClick = MyRefresh
      end
      object CheckBox25: TCheckBox
        Left = 1
        Top = 3
        Width = 128
        Height = 17
        Caption = #1047#1086#1085#1072' '#1041#1057' - '#1087#1086' '#1082#1085#1086#1087#1082#1077
        TabOrder = 1
        OnClick = MyRefresh
      end
      object CheckBox54: TCheckBox
        Left = 1
        Top = 38
        Width = 281
        Height = 19
        Caption = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1085#1099#1077' '#1079#1086#1085#1099' '#1086#1090#1084#1077#1090#1082#1080' '#1041#1057
        TabOrder = 2
        WordWrap = True
        OnClick = MyRefresh
      end
      object CheckBox55: TCheckBox
        Left = 1
        Top = 57
        Width = 281
        Height = 19
        Caption = #1058#1086#1095#1082#1080' '#1086#1090#1084#1077#1090#1082#1080' '#1041#1057
        TabOrder = 3
        Visible = False
        WordWrap = True
        OnClick = MyRefresh
      end
    end
  end
end
