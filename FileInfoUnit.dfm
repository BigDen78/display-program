object FileInfoForm: TFileInfoForm
  Left = 379
  Top = 208
  BorderIcons = [biSystemMenu]
  Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1092#1072#1081#1083#1077
  ClientHeight = 539
  ClientWidth = 709
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShortCut = FormShortCut
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 709
    Height = 505
    Align = alClient
    Ctl3D = True
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Courier New'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    WordWrap = False
    OnClick = Memo1Click
    OnMouseMove = Memo1MouseMove
  end
  object Panel1: TPanel
    Left = 0
    Top = 505
    Width = 709
    Height = 34
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 625
      Top = 1
      Width = 83
      Height = 32
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object CloseButton: TButton
        Left = 4
        Top = 4
        Width = 75
        Height = 25
        Caption = 'CloseButton'
        TabOrder = 0
        OnClick = CloseButtonClick
      end
    end
  end
end
