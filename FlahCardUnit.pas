{$I DEF.INC}
unit FlahCardUnit;  {Language 10}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Mask, TB2Item, TB2Dock, TB2Toolbar, Registry,
  ImgList, ComCtrls, FileCtrl, Buttons, Math, TB2ExtItems, ShellApi,
  MyTypes, LanguageUnit;

type

  TStringData = packed record
    SaveShift: array [1..4] of Byte;
    CanCmd: array [1..3] of Byte;
    Name: array [0..37] of Byte;
    OpperList: array [0..9] of array [0..19] of Byte;
    PeregList: array [0..99] of array [0..19] of Byte;
    SpLabList: array [0..29] of array [0..19] of Byte;
    CharSet: Byte;
    Unuse: array [1..1000] of Byte;
  end;

  TFlashPage = packed record
    ID: Byte;
    Chip: Byte;
    Page: Word;
    Data: array [1..1046] of Byte;
    ControlSum: Byte;
    Reserv: array [1..5] of Byte;
  end;

  TFlashCardForm = class(TForm)
    ImageList1: TImageList;
    Timer1: TTimer;
    SaveDialog1: TSaveDialog;
    PageControl2: TPageControl;
    WaitSheet1: TTabSheet;
    WaitSheet2: TTabSheet;
    WaitSheet3: TTabSheet;
    Panel8: TPanel;
    Panel5: TPanel;
    Panel10: TPanel;
    Label8: TLabel;
    ProgressBar1: TProgressBar;
    Panel11: TPanel;
    Button1: TButton;
    Panel12: TPanel;
    Panel13: TPanel;
    Label6: TLabel;
    Animate2: TAnimate;
    Panel14: TPanel;
    Panel15: TPanel;
    CheckBox3: TCheckBox;
    SaveDialog2: TSaveDialog;
    Timer2: TTimer;
    OpenDialog1: TOpenDialog;
    Label11: TLabel;
    WaitSheet4: TTabSheet;
    Panel24: TPanel;
    Label12: TLabel;
    Panel28: TPanel;
    Panel29: TPanel;
    Button2: TButton;
    Panel30: TPanel;
    ImageList2: TImageList;
    ReadFiles: TTabSheet;
    Panel1: TPanel;
    TBDock1: TTBDock;
    MainToolbar: TTBToolbar;
    Button7: TTBItem;
    Button6: TTBItem;
    TBItem1: TTBItem;
    TBControlItem1: TTBControlItem;
    Button10: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem2: TTBItem;
    CheckBox1: TCheckBox;
    ListView1: TListView;
    TBSeparatorItem1: TTBSeparatorItem;
    TBSeparatorItem3: TTBSeparatorItem;
    Panel32: TPanel;
    Panel34: TPanel;
    Button4: TButton;
    SetParam: TTabSheet;
    Panel4: TPanel;
    Panel9: TPanel;
    Button3: TButton;
    Panel45: TPanel;
    LeftImage: TImage;
    RightImage: TImage;
    LeftRImage: TImage;
    RightRImage: TImage;
    LeftGImage: TImage;
    RightGImage: TImage;
    LeftMImage: TImage;
    RightMImage: TImage;
    PageControl1: TPageControl;
    tsRegData: TTabSheet;
    tsSnapShots: TTabSheet;
    stNotes: TTabSheet;
    PageControl4: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    Panel3: TPanel;
    Panel17: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Panel6: TPanel;
    ListBox11: TListBox;
    Panel16: TPanel;
    ListBox12: TListBox;
    BtnAdd1: TBitBtn;
    BtnDel1: TBitBtn;
    BtnMove1: TBitBtn;
    BtnCopy1: TBitBtn;
    Panel18: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    BtnMove2: TBitBtn;
    Panel20: TPanel;
    ListBox21: TListBox;
    BtnAdd2: TBitBtn;
    BtnDel2: TBitBtn;
    BtnCopy2: TBitBtn;
    Panel21: TPanel;
    ListBox22: TListBox;
    BtnAdd3: TBitBtn;
    BtnDel3: TBitBtn;
    BtnMove3: TBitBtn;
    BtnCopy3: TBitBtn;
    Panel19: TPanel;
    Label5: TLabel;
    Label7: TLabel;
    Panel22: TPanel;
    ListBox31: TListBox;
    Panel23: TPanel;
    ListBox32: TListBox;
    RichEdit2: TRichEdit;
    Panel44: TPanel;
    Label9: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Error1: TImage;
    Error2: TImage;
    Shape7: TShape;
    Panel33: TPanel;
    Shape6: TShape;
    SHPanel: TPanel;
    ScreenShot: TImage;
    Panel2: TPanel;
    Panel7: TPanel;
    Panel25: TPanel;
    Panel31: TPanel;
    TBDock3: TTBDock;
    TBToolbar2: TTBToolbar;
    tbLoad: TTBItem;
    tbSave: TTBItem;
    TBDock2: TTBDock;
    TBToolbar1: TTBToolbar;
    ClearNoteBook: TTBItem;
    ImportNoteBook: TTBItem;
    ExportNoteBook: TTBItem;
    TBDock4: TTBDock;
    TBToolbar3: TTBToolbar;
    TBControlItem3: TTBControlItem;
    ListBox1: TComboBox;
    SavePic: TTBItem;
    TBSeparatorItem4: TTBSeparatorItem;
    ButtonB: TButton;
    Memo1: TMemo;
    Label10: TLabel;
    TBDock5: TTBDock;
    TBToolbar4: TTBToolbar;
    TBItem7: TTBItem;
    TBItem8: TTBItem;
    TBControlItem4: TTBControlItem;
    Panel35: TPanel;
    Label16: TLabel;
    Edit1: TEdit;
    TBSeparatorItem6: TTBSeparatorItem;
    OpenDialog2: TOpenDialog;
    SaveDialog3: TSaveDialog;
    RichEdit1__: TMemo;
    TBControlItem2: TTBControlItem;
    Panel26: TPanel;
    Label13: TLabel;
    procedure ListBox11DblClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure CreateFileList;
    procedure Timer1Timer(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure LoadListClick(Sender: TObject);
    procedure SaveListClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ListBox11MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure LoadScreenShotClick(Sender: TObject);
    procedure SetMode(ModeIdx_: Integer);
    procedure SavePicClick(Sender: TObject);
    procedure Button32Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure ImportNoteBookClick(Sender: TObject);
    procedure BtnAdd1Click(Sender: TObject);
    procedure BtnDel1Click(Sender: TObject);
    procedure BtnCopy1Click(Sender: TObject);
    procedure BtnMove1Click(Sender: TObject);
    procedure ListBox21MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnAdd2Click(Sender: TObject);
    procedure BtnDel2Click(Sender: TObject);
    procedure BtnCopy2Click(Sender: TObject);
    procedure BtnMove2Click(Sender: TObject);
    procedure ListBox31MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BtnAdd3Click(Sender: TObject);
    procedure BtnDel3Click(Sender: TObject);
    procedure BtnCopy3Click(Sender: TObject);
    procedure BtnMove3Click(Sender: TObject);
    procedure LoadNoteBookClick(Sender: TObject);
    procedure SaveNoteBookClick(Sender: TObject);
    procedure ClearNoteBookClick(Sender: TObject);
    procedure TabSheet6Resize(Sender: TObject);
    procedure TestState;
    procedure OnChangeLanguage(Sender: TObject);
    procedure TBItem2Click(Sender: TObject);
    procedure tbLoadClick(Sender: TObject);
    procedure tbSaveClick(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ButtonBClick(Sender: TObject);
    procedure TBItem4Click(Sender: TObject);
    procedure TBItem8Click(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);

  private
    WorkID: Byte;
    AKPState: Boolean;
    BUIState: Boolean;
    BusyFlag: Boolean;
    ChipCount: Integer;
    ModeIdx: Integer;
    CurrCharSet: Integer;
    CurrCharSet2: Integer;
    SelectSS: Integer;
    FileList: array [1..511] of TFlashPage;
    SnapShot: array [1..8] of TBitMap;
    GLC: Integer;
    FLog: TextFile;

    FSave1: string;
    FSave2: string;
    FSave3: string;
    FSave4: string;
    FSave5: string;
    ErrorFlg: Boolean;

    procedure AddToLog(S: string; Save: Boolean = False);
    procedure CreateLog;
    procedure CloseLog;
    procedure LoadToRegistry;
    procedure SaveToRegistry;
  public
    { Public declarations }
  end;

var
  FlashCardForm: TFlashCardForm;

implementation

uses
  {AKPDllDescriptor, } ConfigUnit;

var
  ProcRes: Integer;

{$R *.DFM}

procedure LineArrToChipPage(LineAddr, ChipCount: Integer; var Chip, Page: Integer);
var
  I: Integer;
begin
  Chip:= 0;
  Page:= 0;
  for I:= 1 to LineAddr do
  begin
    Inc(Chip);
    if Chip >= ChipCount then
    begin
      Chip:= 0;
      Inc(Page);
    end;
  end;
end;

function CodeToDate(Day, Month, Year: Byte; Razdelitel: Char) : string;
var
  S: string;
  I: Integer;

begin
  Result:= Format('%d' + Razdelitel + '%d' + Razdelitel + '%d', [(Day   shr 4 and $0F) * 10 + (Day   and $0F),
                                                 (Month shr 4 and $0F) * 10 + (Month and $0F),
                                                 (Year  shr 4 and $0F) * 10 + (Year  and $0F + 2000)]);
end;

function CodeToTime(Hour, Minute: Byte) : string;
var
  S: string;
  I: Integer;

begin
  Result:= Format('%d:%d', [(Hour   shr 4 and $0F) * 10 + (Hour   and $0F),
                            (Minute shr 4 and $0F) * 10 + (Minute and $0F)]);
end;

// ---------------------------------------------------------------------------------------------------------

procedure TFlashCardForm.FormCreate(Sender: TObject);
var
  I: Integer;
  Count: Integer;

begin
  ErrorFlg:= False;
  DeleteFile(ExtractFilePath(Application.ExeName) + 'FlashCard.log');
  CreateLog;

  PageControl1.ActivePageIndex:= 0;
  PageControl4.ActivePageIndex:= 0;

  ListBox11MouseUp(Sender, mbLeft, [], 0, 0);
  ListBox21MouseUp(Sender, mbLeft, [], 0, 0);
  ListBox31MouseUp(Sender, mbLeft, [], 0, 0);

  WaitSheet1.TabVisible:= False;
  WaitSheet2.TabVisible:= False;
  WaitSheet3.TabVisible:= False;
  WaitSheet4.TabVisible:= False;

  CheckBox1.Checked:= Config.AskFlashName;
  Self.Refresh;

  ReadFiles.TabVisible:= False;
  SetParam.TabVisible:= False;

  LoadToRegistry;

 { if Config.ShowUSBLog then
  begin
    Panel14.Visible:= True;
    Self.Height:= Self.Height + Panel14.Height;
  end;    }
  OnChangeLanguage(nil);
end;

procedure TFlashCardForm.CreateLog;
begin
  AssignFile(FLog, ExtractFilePath(Application.ExeName) + 'FlashCard.log');
  ReWrite(FLog);
end;

procedure TFlashCardForm.CloseLog;
begin
  CloseFile(FLog);
end;

procedure TFlashCardForm.AddToLog(S: string; Save: Boolean = False);
begin
  Inc(GLC);
  Writeln(FLog, S);
end;

procedure TFlashCardForm.LoadToRegistry;
var
  I, Count: Integer;

begin
  with TRegistry.Create do
  try
    ListBox12.Items.Clear;
    ListBox22.Items.Clear;
    ListBox32.Items.Clear;

    RootKey := HKEY_LOCAL_MACHINE;
    if OpenKey('\SOFTWARE\Avikon-11\Peregon', True) and ValueExists('IntemCount') then
    begin
      Count:= ReadInteger('IntemCount');
      for I:= 0 to Count - 1 do ListBox12.Items.Add(ReadString(Format('Item_%d', [I]))); // ������ ������
      CloseKey;
    end;
    if OpenKey('\SOFTWARE\Avikon-11\Operators', True) and ValueExists('IntemCount') then
    begin
      Count:= ReadInteger('IntemCount');
      for I:= 0 to Count - 1 do ListBox22.Items.Add(ReadString(Format('Item_%d', [I]))); // ������ ������
      CloseKey;
    end;
    if OpenKey('\SOFTWARE\Avikon-11\Labels', True) and ValueExists('IntemCount') then
    begin
      Count:= ReadInteger('IntemCount');
      for I:= 0 to Count - 1 do ListBox32.Items.Add(ReadString(Format('Item_%d', [I]))); // ������ ������
      CloseKey;
    end;
  finally
    Free;
  end;
end;

procedure TFlashCardForm.SaveToRegistry;
var
  I: Integer;

begin
  with TRegistry.Create do
  try
    RootKey := HKEY_LOCAL_MACHINE;
    if OpenKey('\SOFTWARE\Avikon-11\Peregon', True) then
    begin
      WriteInteger('IntemCount', ListBox12.Items.Count);
      for I:= 0 to ListBox12.Items.Count - 1 do WriteString(Format('Item_%d', [I]), ListBox12.Items.Strings[I]);
      CloseKey;
    end;
    if OpenKey('\SOFTWARE\Avikon-11\Operators', True) then
    begin
      WriteInteger('IntemCount', ListBox22.Items.Count);
      for I:= 0 to ListBox22.Items.Count - 1 do WriteString(Format('Item_%d', [I]), ListBox22.Items.Strings[I]);
      CloseKey;
    end;
    if OpenKey('\SOFTWARE\Avikon-11\Labels', True) then
    begin
      WriteInteger('IntemCount', ListBox32.Items.Count);
      for I:= 0 to ListBox32.Items.Count - 1 do WriteString(Format('Item_%d', [I]), ListBox32.Items.Strings[I]);
      CloseKey;
    end;
  finally
    Free;
  end;
end;

procedure TFlashCardForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  I, Pic: Integer;
  Name: string;
  Name2: string;

begin
  if (FSave1 <> ListBox11.Items.Text) or
     (FSave2 <> ListBox21.Items.Text) or
     (FSave3 <> ListBox31.Items.Text) or
     (FSave4 <> Edit1.Text) or
     (FSave5 <> RichEdit2.Lines.Text) then

    if MessageBox(Handle, PChar(LangTable.Caption['USB:SavechangesinCDU']), PChar(LangTable.Caption['Common:Attention']), 33) = IDOK then tbSaveClick(nil);

  SaveToRegistry;

  for Pic:= 1 to 8 do
    if Assigned(SnapShot[Pic]) then
    begin
      SnapShot[Pic].Free;
      SnapShot[Pic]:= nil;
    end;

  CloseLog;

  if ErrorFlg then               // ���������� ����� ��� ����� ���� ��� �� ������ ����� ������ �� ����������� ������
  begin
{    Name:= ExtractFilePath(Application.ExeName) + 'FlashCard.log';
    Name2:= ExtractFilePath(Application.ExeName) + 'send_' + IntToStr(Config.SendIdx) + '.log';
    Config.SendIdx:= Config.SendIdx + 1;
    if FileExists(Name) then
      CopyFile(PChar(Name), PChar(Name2), True); }
  end;
end;

procedure TFlashCardForm.ListBox11DblClick(Sender: TObject);
var
  Value: string;

begin
  if Sender is TListBox then
    if TListBox(Sender).ItemIndex <> - 1 then
    begin
      Value:= TListBox(Sender).Items.Strings[TListBox(Sender).ItemIndex];
      if InputQuery(LangTable.Caption['USB:Editing'], LangTable.Caption['USB:Enternewvalue'], Value) then
        TListBox(Sender).Items.Strings[TListBox(Sender).ItemIndex]:= Value;
    end;
end;

procedure TFlashCardForm.Timer1Timer(Sender: TObject);
label
  L1;

var
  AKPState_: Boolean;
  BUIState_: Boolean;
  Res: Integer;
  Res2: Integer;

begin
 (*
  AKPState_:= AKPState;
  BUIState_:= BUIState;

  Res:= InitLib;

  if Res = 0 then
  begin
    Res2:= get_initerror(WorkID);
    AddToLog(Format('%d. get_initerror Res = %d', [GLC, Res2]));
    if (Res2 = - 7) or (Res2 = - 8) then
    begin
      Timer1.Enabled:= False;
      ShowMessage(LangTable.Caption['USB:PleaseupdateMCAdriver']);
    //  goto L1;
    end;
{    if Res2 = - 9 then
    begin
      Timer1.Enabled:= False;
      ShowMessage(LangTable.Caption['USB:PleaseupdateMCAfirmware']);
    //  goto L1;
    end; }
  end;

  AKPState:= (Res and 1) <> 0;
  BUIState:= (Res and 2) <> 0;

  AddToLog(Format('%d. InitLib Res = %d', [GLC, Res]));

  if (AKPState_ <> AKPState) or (BUIState_ <> BUIState) then TestState;
  exit;
L1:
  Button4.Click;
  *)
end;

procedure TFlashCardForm.TestState;
begin
  case ModeIdx of
    1: if AKPState or BUIState then
       begin
         CreateFileList;
         ReadFiles.Show;
       end
       else
       begin
         ReadFiles.Show;
         WaitSheet3.Show;
       end;
    2: if BUIState then
       begin
         tbLoadClick(nil);
         SetParam.Show
       end else WaitSheet4.Show;
  end;
  Self.Refresh;
end;

procedure TFlashCardForm.CreateFileList;
var
  S1: string;
  S2: string;
  I: Integer;
  Res: Integer;
  Chip: Integer;
  Page: Integer;
  Item: TListItem;
  DecCount: Integer;
  TestCRCFlag: Boolean;
  ss: Integer;
  LastCalcCS: Byte;
  Flg: Boolean;

  MS: TMemoryStream;
  B1: Byte;
  B2: Byte;

function TestCRC(var FP: TFlashPage): Boolean;
var
  CS: Word;
  I: Integer;

begin
  CS:= 0;
  for I:= 1 to 1046 do
  begin
    CS:= CS + FP.Data[I];
    CS:= Lo(CS);
  end;
  LastCalcCS:= Lo(CS);
  Result:= FP.ControlSum = Lo(CS);
end;

begin
(*
  ProgressBar1.Position:= 0;
  WaitSheet1.Show;
  Self.Refresh;

  ListView1.Items.Clear;

  ChipCount:= 0;
  WorkID:= 0;

  I:= Power_ON(0);
  AddToLog(Format('%d. Power_On Res = %d ', [GLC, I]));
  if I <> 1 then
  begin
    ProcRes:= Power_Off(0);
    AddToLog(Format('%d. Power_Off Res = %d', [GLC, ProcRes]));
  end
  else
  begin
    Sleep(1000);
    ChipCount:= ChipCntr(0);
    AddToLog(Format('%d. ChipCntr Res = %d', [GLC, ChipCount]));

    DecCount:= 10;
    while (ChipCount = 0) and (DecCount <> 0) do
    begin
      ChipCount:= ChipCntr(0);
      AddToLog(Format('%d. ChipCntr Res = %d', [GLC, ChipCount]));
      Dec(DecCount);
    end;
  end;

  if AKPState then
  begin
    MS:= TMemoryStream.Create;
    MS.SetSize(8);
    Res:= get_version(MS.Memory, 0);
    MS.ReadBuffer(B1, 1);
    MS.ReadBuffer(B2, 1);
    MS.Free;

    if Res = 8 then AddToLog(Format('%d. AKP firmware version: %d.%d', [GLC, B2, B1]))
               else AddToLog(Format('%d. Error geting AKP firmware version', [GLC]));
  end;

  if ChipCount < 1 then
  begin
    ProcRes:= Power_Off(0);
    AddToLog(Format('%d. Power_Off Res = %d', [GLC, ProcRes]));
    WorkID:= 1;
    ChipCount:= ChipCntr(1);
    AddToLog(Format('%d. ChipCntr Res = %d', [GLC, ChipCount]));
    if ChipCount < 1 then
    begin
      ReadFiles.Show;
      Self.Refresh;
      AddToLog(' ', True);
      Exit;
    end;
  end;

  Memo1.Lines.Clear;

  if ChipCount <> 8 then
  begin
    AddToLog('������ ��� ������ � ������ ������');
    ShowMessage('������ ��� ������ � ������ ������');
    ReadFiles.Show;
    Self.Refresh;
    AddToLog(' ', True);
    Exit;
  end;

  AddToLog('������� ���������� ������');

  ProgressBar1.Min:= 0;
  ProgressBar1.Max:= 511;
  for I:= 1 to 511 do
  begin
    ProgressBar1.Position:= I;

    LineArrToChipPage(I, ChipCount, Chip, Page);
    Res:= ReadPages(@FileList[I], Page, Chip, 1, WorkID);

    if FileList[I].ID <> $FF then
    begin
      Flg:= True;
      if (FileList[I].Chip <> $FF) and          // ���� ����� ����� ������ 1-� ��������
         (not TestCRC(FileList[I])) then        // ����� ��������� ����������� �����
      begin
         Flg:= False;
         AddToLog(Format('%d. CS Error (Page: %d; Chip: %d);', [GLC, Page, Chip]));
         ss:= 30;                                           // ������������ ������� ��������
         repeat
           Res:= ReadPages(@FileList[I], Page, Chip, 1, WorkID);
           TestCRC(FileList[I]);
           AddToLog(Format('%d. ReTry Reload [%d] Page: %d; Chip: %d; Read Bytes: %d; CS: %d', [GLC, 31 - SS, Page, Chip, Res, LastCalcCS]));
           Dec(SS);
         until TestCRC(FileList[I]) or (SS = 0) or (Res < 0);
         Flg:= TestCRC(FileList[I]);
      end;

      Memo1.Lines.Add(Format('I: %d; Page: %d; Chip: %d; -> Chip: %d; Page: %d', [I, Page, Chip, FileList[I].Chip, FileList[I].Page]));
      AddToLog(Format('���� �: %d; Page: %d; Chip: %d; -> Chip: %d; Page: %d', [I, Page, Chip, FileList[I].Chip, FileList[I].Page]));

      Item:= ListView1.Items.Add;
      Item.Caption:= '';
      Item.Data:= Pointer(I);

      Item.SubItems.Add(IntToStr(I));
(*
      with TAvk11Header2((@FileList[I].Data)^) do
      begin
        S1:= IntToStr(Day and $0F + (Day shr 4) * 10);
        while Length(S1) < 2 do S1:= '0' + S1;
        S2:= S1 + ':';
        S1:= IntToStr(Month and $0F + (Month shr 4) * 10);
        while Length(S1) < 2 do S1:= '0' + S1;
        S2:= S2 + S1 + ':';
        S1:= IntToStr(Year and $0F + (Year shr 4) * 10);
        while Length(S1) < 2 do S1:= '0' + S1;
        S2:= S2 + S1;

        Item.SubItems.Add(S2);
        Item.SubItems.Add(IntToStr(StartKM));
        Item.SubItems.Add(IntToStr(StartPk));
        Item.SubItems.Add(IntToStr(StartMetre));
        Item.SubItems.Add(IntToStr(Path));
        Item.SubItems.Add(CodeToText(Operator, CharSet{Config.WorkLang}));
        Item.SubItems.Add(CodeToText(SecName, CharSet{Config.WorkLang}));
        Item.SubItems.Add(CodeToText(Name, CharSet{Config.WorkLang}));
        Item.SubItems.Add('');
      end; * )
      if not Flg then AddToLog(Format('���� � %d: - ������ �������� ���������', [I]));

    end else Break;
  end;

  AddToLog(Format('�������� %d ������', [ListView1.Items.Count]));

  ProgressBar1.Position:= 511;
  Sleep(100);

  if WorkID <> 1 then
  begin
    ProcRes:= Power_Off(WorkID);
    AddToLog(Format('%d. Power_Off Res = %d', [GLC, ProcRes]));
  end;

  ReadFiles.Show;
  Self.Refresh;

  AddToLog(' ', True);
  *)
end;

procedure TFlashCardForm.Button5Click(Sender: TObject);
begin
(*
  if WorkID <> 1 then
  begin
    ProcRes:= Power_Off(WorkID);
    AddToLog(Format('%d. Power_Off Res = %d', [GLC, ProcRes]));
  end; *)
end;

procedure TFlashCardForm.Button2Click(Sender: TObject);
label
  L1;

var
  I, J, K, QQ: Integer;
  Chip: Integer;
  Page: Integer;
  Item: TListItem;
  Res: Integer;
  ResFile: TMemoryStream;
  ResFile_: file;
  FP: TFlashPage;
  Flag: Boolean;
  T1: Integer;
  SumRes: Single;
  FileNum: Integer;
  S, SS: Integer;
  S1, S2: string;
  F: string;
  FName: string;
  FName2: string;
  Dir: string;
  LogName: string;
  RetryCount: Integer;
  Temp: array [-1..61] of TFlashPage;
  Chip_: array [0..61] of Integer;
  Page_: array [0..61] of Integer;
  LastCalcCS: Byte;
  TestCRCFlag: Boolean;
  CurID: Integer;
  NameEd: string;
  OperatorEd: string;
  SecNameEd: string;
  LogPos: Integer;
  Log2: TStringList;
  RetryLoad: Integer;
  GoodFile: Boolean;
  SummLoadSize: Integer;
  FileIdx: Integer;

procedure AddPageLog(Page: Word; Chip: Byte; ID: Byte; ChectSum: Byte; ChectSumError: Boolean; FileOffset: Integer; NextPage: Word; NextChip: Byte);
begin
  Log2.Add(Format('%9d | %9d | %4d |  %4d  |  %4d  | %6d | %5d | %10d | %9d | %9d |', [Page, Chip, ID, LastCalcCS, ChectSum, Ord(ChectSumError), RetryLoad, FileOffset, NextPage, NextChip]));
end;

procedure AddPageLogHead();
begin
  Log2.Add('Load Page | Load Chip |  ID  | CalcCS | LoadCS | LoadOk | ReTry | FileOffset | Next Page | Next Chip |');
end;

function TestCRC(var FP: TFlashPage): Boolean;
var
  CS: Word;
  I: Integer;

begin
  CS:= 0;
  for I:= 1 to 1046 do
  begin
    CS:= CS + FP.Data[I];
    CS:= Lo(CS);
  end;
  LastCalcCS:= Lo(CS);
  Result:= FP.ControlSum = Lo(CS);
end;

begin
 (*
  SummLoadSize:= 0;
  Log2:= TStringList.Create;
  J:= 0;
  for I:= 0 to ListView1.Items.Count - 1 do if ListView1.Items.Item[I].Checked then Inc(J);
  if J = 0 then Exit;

  if not (AKPState or BUIState) then Exit;

  if AKPState then
  begin
    WorkID:= 0;
    if WorkID <> 1 then
    begin
      Res:= power_on(WorkID);
      AddToLog(Format('%d. power_on(%d) Res = %d', [GLC, WorkID, Res]));
      if Res <= 0 then
      begin
        AddToLog(' ', True);
        Exit;
      end;
      Sleep(500);
    end;
    ChipCount:= chipcntr(WorkID);
    AddToLog(Format('%d. ChipCntr(%d) Res = %d', [GLC, WorkID, ChipCount]));
    if ChipCount <= 0 then
    begin
      AddToLog(' ', True);
      Exit;
    end;
  end
  else
  begin
    WorkID:= 1;
    ChipCount:= chipcntr(WorkID);
    AddToLog(Format('%d. ChipCntr(%d) Res = %d', [GLC, WorkID, ChipCount]));
    if ChipCount <= 0 then
    begin
      AddToLog(' ', True);
      Exit;
    end;
  end;

  if ChipCount <> 8 then
  begin
    AddToLog('������ ��� ������ � ������ ������');
    ShowMessage('������ ��� ������ � ������ ������');
    AddToLog(' ', True);
    Exit;
  end;

  AddToLog(IntToStr(GLC) + '. � ������ ' + IntToStr(ListView1.Items.Count) + ' ������');

  if not CheckBox1.Checked then
  begin
{    Dir:= Config.LastFlashDir;
    if MySelectDirectory(LangTable.Caption['Messages:'], '', Dir) then Config.LastFlashDir:= Dir
                                                                else begin
                                                                     //  FilesSheet.Show;
                                                                       ReadFiles.Show;
                                                                       Self.Refresh;
                                                                       AddToLog(' ', True);
                                                                       Exit;
                                                                     end;
}  end;

  WaitSheet2.Show;
  Self.Refresh;
  T1:= GetTickCount;

  SumRes:= 0;
  FileNum:= 0;

  for I:= 0 to ListView1.Items.Count - 1 do
    if ListView1.Items.Item[I].Checked then
    begin

      FileIdx:= Integer(ListView1.Items.Item[I].Data);

      Log2.Clear;
      Log2.add('');
      Log2.add(Format('����� �����: %d', [FileIdx]));
      AddPageLogHead;
      GoodFile:= True;

      Inc(FileNum);

      {$IFDEF CREATELOG}
      RichEdit1.Clear;
      {$ENDIF}

      AddToLog(IntToStr(GLC) + '. ����� � ���������� ����� �!' + IntToStr(FileIdx));

//      LineArrToChipPage(I + 1, ChipCount, Chip, Page);
      LineArrToChipPage(FileIdx, ChipCount, Chip, Page);


//      with TAvk11Header2((@FileList[I + 1].Data)^) do   // ������������ ����� �����
(*      with TAvk11Header2((@FileList[FileIdx].Data)^) do   // ������������ ����� �����
      begin

        S1:= IntToStr(Day and $0F + (Day shr 4) * 10);
        while Length(S1) < 2 do S1:= '0' + S1;
        S2:= S1;
        S1:= IntToStr(Month and $0F + (Month shr 4) * 10);
        while Length(S1) < 2 do S1:= '0' + S1;
        S2:= S2 + S1;
        S1:= IntToStr(Year and $0F + (Year shr 4) * 10);
        while Length(S1) < 2 do S1:= '0' + S1;
        S2:= S2 + S1;

        FName:= '';
        for J:= 0 to 15 do
          if Config.FNStruc[J].On_ then
          begin
            case Config.FNStruc[J].Idx of
              9900001: F:= S2; //CodeToDate(Day, Month, Year, '_');                       // ���� �������
              9900002: F:= Format('%d' + LangTable.Caption['common:km'] + '%d' + LangTable.Caption['common:pk'], [StartKM, StartPk]);                        // ��������� ����������
              9900003: F:= Format('%d', [FileIdx]);                                       // ���������� ����� �����
              9900004: F:= Format('No%d', [UpDeviceID]);                                  // ��������� ����� ���
              9900005: F:= Format('No%d', [DwnDeviceID]);                                 // ��������� ����� ���
              9900006: begin                                                              // ����� ������ �������
                         S1:= IntToStr((Hour   shr 4 and $0F) * 10 + (Hour   and $0F));
                         S2:= IntToStr((Minute shr 4 and $0F) * 10 + (Minute and $0F));
                         while Length(S1) < 2 do S1:= '0' + S1;
                         while Length(S2) < 2 do S2:= '0' + S2;
                         F:= S1 + '-'+ S2;
                       end;
              9900007: F:= CodeToText(Operator, CharSet);                                 // ��������
              9900008: F:= CodeToText(Name, CharSet);                                     // �������� ������������� ��� ���
              9900009: F:= Format('%d', [StartKM]);                                       // ��������� ��������
              9900010: F:= Format('%d', [StartPk]);                                       // ��������� �����
              9900011: F:= Format('%d', [StartMetre]);                                    // ��������� ����
              9900012: F:= Format('%d', [Zveno]);                                         // ��������� �����
              9900013: F:= Format('%d', [DirCode]);                                       // ��� �����������
              9900014: F:= CodeToText(SecName, CharSet);                                  // �������
              9900015: F:= Format('%d', [Path]);                                          // ����
              9900016: if MoveDir = 0 then F:= '[-]' else F:= '[+]';                      // ����������� ��������
            end;
            FName:= FName + F + Config.FNSpace;
          end;
        Delete(FName, Length(FName), 1);

        CurID:= ID;

        {$IFDEF FILUSX17}
        FName:= FName + '.a11';
        {$ENDIF}
        {$IFNDEF FILUSX17}
        if ID = $FF14 then FName:= FName + '.a12' else FName:= FName + '.a11';
        {$ENDIF}
      end;
      * )
      SaveDialog1.FileName:= FName;
      if CheckBox1.Checked then
      begin
        if not SaveDialog1.Execute then
        begin
          AddToLog(IntToStr(GLC) + '. ������ ���������� ����� �!' + IntToStr(FileIdx));
          Continue;
        end else Config.LastFlashDir:= ExtractFilePath(SaveDialog1.FileName);
      end;
      Self.Refresh;

      if CheckBox1.Checked then
      begin
        if (not FileExists(SaveDialog1.FileName)) or
           (FileExists(SaveDialog1.FileName) and (MessageBox(Handle, PChar(LangTable.Caption['USB:Filealreadyexist!'] + #10 + SaveDialog1.FileName + #10 + LangTable.Caption['USB:Rewrite?']), PChar(LangTable.Caption['common:Attention!']), 36) = IDYES)) then
        begin
          {$IFDEF FILUSX17}
          if ExtractfileExt(SaveDialog1.FileName) = '' then
            SaveDialog1.FileName:= SaveDialog1.FileName + '.a11';
          {$ENDIF}
          {$IFNDEF FILUSX17}
          if CurID = $FF14 then
          begin
            if ExtractfileExt(SaveDialog1.FileName) = '' then
              SaveDialog1.FileName:= SaveDialog1.FileName + '.a12';
          end
          else
          begin
            if ExtractfileExt(SaveDialog1.FileName) = '' then
              SaveDialog1.FileName:= SaveDialog1.FileName + '.a11';
          end;
          {$ENDIF}

          AssignFile(ResFile_, SaveDialog1.FileName);
          LogName:= SaveDialog1.FileName;
          Config.AddToDownLoadFilesHistory(SaveDialog1.FileName);
        end
        else
        begin
          AddToLog(IntToStr(GLC) + '. ������ ���������� ����� �!' + IntToStr(FileIdx));
          Continue;
        end;
      end
      else
      begin
        FName2:= Config.LastFlashDir + '\' + FName;
        if (not FileExists(FName2)) or
           (FileExists(FName2) and (MessageBox(Handle, PChar(LangTable.Caption['USB:Filealreadyexist!'] + #10 + FName2 + #10 + LangTable.Caption['USB:Rewrite?']), PChar(LangTable.Caption['common:Attention!']), 36) = IDYES)) then
        begin
          {$IFDEF FILUSX17}
          if ExtractfileExt(FName2) = '' then
            FName2:= FName2 + '.a11';
          {$ENDIF}
          {$IFNDEF FILUSX17}
          if CurID = $FF14 then
          begin
            if ExtractfileExt(FName2) = '' then
              FName2:= FName2 + '.a12';
          end
          else
          begin
            if ExtractfileExt(FName2) = '' then
              FName2:= FName2 + '.a11';
          end;
          {$ENDIF}

          AssignFile(ResFile_, FName2);
          LogName:= FName2;
          Config.AddToDownLoadFilesHistory(FName2);
        end
        else
        begin
          AddToLog(IntToStr(GLC) + '. ������ ���������� ����� �!' + IntToStr(FileIdx));
          Continue;
        end;
      end;

      ReWrite(ResFile_, 1);                          // �������� ������ �������� �����

      RetryCount:= 30;
      repeat
        Res:= ReadPages(@FP, Page, Chip, 1, WorkID);
        TestCRCFlag:= TestCRC(FP) or (FP.Chip = $FF);
        if RetryCount = 30 then AddToLog(Format('%d. ReadPages: from: Page: %d, Chip: %d; Read Bytes: %d', [GLC, Page, Chip, Res]))
                           else AddToLog(Format('%d. Retry Load !!! - Page: %d; Chip: %d; Read Bytes: %d', [GLC, Page, Chip, Res]));
        if not TestCRCFlag then AddToLog(Format('%d. CS Error (Page: %d; Chip: %d);', [GLC, Page, Chip]));
        if (Res <> 1056) or (not TestCRCFlag) then Dec(RetryCount);
      until (RetryCount = 0) or ((Res = 1056) and TestCRCFlag);
      RetryLoad:= 30 - RetryCount;
      GoodFile:= GoodFile and TestCRCFlag;

      if Res > 0 then
      begin
        SumRes:= SumRes + Res;
        Label10.Caption:= Format(LangTable.Caption['USB:File%d;%.0nbytereceiv'], [FileNum, SumRes]);
        Label10.Refresh;
      end;

      if Res <= 0 then
      begin
//        ShowMessage('������ ������ ����� �' + IntToStr(I + 1));
        ShowMessage('������ ������ ����� �' + IntToStr(FileIdx));
        CloseFile(ResFile_);
        Continue;
      end;

//      if not CheckBox3.Checked then RichEdit1_.Refresh;

      if FP.Chip = $FF then // �������� ���� - ������� � 1 ��������
      begin
        TestCRCFlag:= True;
        SS:= 0;                  // ����� ����� �����
        for S:= 129 to 1046 do
        begin
          if FP.Data[S] = $FF then Inc(SS)
                              else SS:= 0;
          if SS = 9 then
          begin
            BlockWrite(ResFile_, FP.Data[1], S);
            AddPageLog(Page, Chip, FP.ID, FP.ControlSum, TestCRCFlag, 0, FP.Page, FP.Chip);
            Log2.Add('������ ����� ����� - ' + IntToStr(S) + ' ���� �� ��������� ��������');
            goto L1;
          end;
        end;
        BlockWrite(ResFile_, FP.Data[1], 1046); // ����� �� ������ - ���������� ��� ��������
        AddPageLog(Page, Chip, FP.ID, FP.ControlSum, TestCRCFlag, 0, FP.Page, FP.Chip);
        Log2.Add('����� ����� �� ������');
        ErrorFlg:= True;
        goto L1;
      end
      else
      begin
        BlockWrite(ResFile_, FP.Data[1], 1046);
        AddPageLog(Page, Chip, FP.ID, FP.ControlSum, TestCRCFlag, 0, FP.Page, FP.Chip);
      end;

      Temp[-1]:= FP;  // ��������� ��������� �������� �� ������ ���� � ��� ����� ������� ����� �����

  //    if not TestCRCFlag then goto L1; - ���� ���� � ������ �������� �� ������ �� ������

      Flag:= True;
      Page:= FP.Page;    // ������ �������� �������� ��������
      Chip:= FP.Chip;

      repeat
        Application.ProcessMessages;
        RetryCount:= 10;
        repeat                                                 // �������� 62 �������
          Res:= ReadPages(@Temp[0], Page, Chip, 62, WorkID);
            if RetryCount = 10 then AddToLog(Format('%d. Page: %d; Chip: %d; Read Bytes: %d', [GLC, Page, Chip, Res]))
                               else AddToLog(Format('%d. Retry Load !!! - Page: %d; Chip: %d; Read Bytes: %d', [GLC, Page, Chip, Res]));

//          if not CheckBox3.Checked then RichEdit1_.Refresh;
          if Res <> 62 * 1056 then Dec(RetryCount);
        until (RetryCount = 0) or (Res = 62 * 1056);

        if Res > 0 then
        begin
          SumRes:= SumRes + Res;
          Label10.Caption:= Format(LangTable.Caption['USB:File%d;%.0nbytereceiv'], [FileNum, SumRes]);
          Label10.Refresh;
        end;

        if Res <= 0 then
        begin
//          ShowMessage('������ ������ ����� �' + IntToStr(I + 1));
          ShowMessage('������ ������ ����� �' + IntToStr(FileIdx));
          Break;
        end;

        for K:= 1 to 61 do // ����������� ����� ������ ����� �������� �������� - Page, Chip
        begin
          Chip_[K - 1]:= Chip;
          Page_[K - 1]:= Page;
          Inc(Chip);
          if Chip >= ChipCount then
          begin
            Chip:= 0;
            Inc(Page);
          end;
        end;

        J:= 0;             // �������� ����������� ����� � ����� ����� �����
        repeat
          LogPos:= FilePos(ResFile_);
          RetryLoad:= 0;
          if (Temp[J].Chip = $FF) or (Temp[J + 1].ID <> $10) then // ��������� �������� ?
          begin
            SS:= 0;                                               // ���� ����� ����� - � ����� ���������� ��������
            for S:= 1039 to 1046 do
              if Temp[J - 1].Data[S] = $FF then Inc(SS)
                                       else SS:= 0;
            for S:= 1 to 1046 do                                  // ���� ����� ����� - ���������� � ������� ��������
            begin
              if Temp[J].Data[S] = $FF then Inc(SS)
                                       else SS:= 0;
              if SS = 9 then
              begin
                BlockWrite(ResFile_, Temp[J].Data[1], S);


                AddPageLog(Page_[J], Chip_[J], Temp[J].ID, Temp[J].ControlSum, TestCRCFlag, LogPos, Temp[J].Page, Temp[J].Chip);
                Log2.Add('������ ����� ����� - ' + IntToStr(S)  + ' ���� �� ��������� ��������');
                goto L1;
              end;
            end;
            BlockWrite(ResFile_, Temp[J].Data[1], 1046);
            AddPageLog(Page_[J], Chip_[J], Temp[J].ID, Temp[J].ControlSum, TestCRCFlag, LogPos, Temp[J].Page, Temp[J].Chip);
            if Temp[J + 1].ID <> $10 then Log2.Add('ID ��������� �������� ' + IntToStr(Temp[J + 1].ID));
            Log2.Add('����� ����� �� ������');
            ErrorFlg:= True;
            goto L1;
          end
          else
          begin
            TestCRCFlag:= True;
            if not TestCRC(Temp[J]) then                          // �������� ����������� �����
            begin
               TestCRCFlag:= False;
               AddToLog(Format('%d. CS Error (Page: %d; Chip: %d);', [GLC, Page_[J], Chip_[J]]));

               ss:= 30;                                           // ������������ ������� ��������
               repeat
                 Res:= ReadPages(@Temp[J], Page_[J], Chip_[J], 1, WorkID);
                 TestCRC(Temp[J]);
                 AddToLog(Format('%d. ReTry Reload [%d] Page: %d; Chip: %d; Read Bytes: %d; CS: %d', [GLC, 31 - SS, Page_[J], Chip_[J], Res, LastCalcCS]));
                 Dec(SS);
               until TestCRC(Temp[J]) or (SS = 0) or (Res < 0);
               TestCRCFlag:= TestCRC(Temp[J]);
               GoodFile:= GoodFile and TestCRCFlag;
               RetryLoad:= 30 - SS - 1;
            end;
            BlockWrite(ResFile_, Temp[J].Data[1], 1046);
          end;

          AddPageLog(Page_[J], Chip_[J], Temp[J].ID, Temp[J].ControlSum, TestCRCFlag, LogPos, Temp[J].Page, Temp[J].Chip);

          Inc(J);
          if J = 61 then Break;
        until False;

        Temp[-1]:= Temp[61]; // ��������� ��������� �������� �� ������ ���� � ��� ����� ������� ����� �����

      until False;
L1:
      if not GoodFile then
        ListView1.Items.Item[I].SubItems.Strings[9] := '���� �������� ������� �������';

//      with TAvk11Header2((@FileList[I + 1].Data)^) do
(*      with TAvk11Header2((@FileList[FileIdx].Data)^) do
      begin
        SecNameEd:=  CodeToText(SecName, CharSet);
        NameEd:=     CodeToText(Name, CharSet);
        OperatorEd:= CodeToText(Operator, CharSet);

      {
        if (NameEd = '') or (OperatorEd = '') or (SecNameEd = '') then
        begin
          with TFileCorrectForm.Create(nil) do
          begin
            Work(NameEd, OperatorEd, SecNameEd, LogName);
            Free;
          end;

          TextToCode(SecNameEd, 20, CharSet, SecName);
          TextToCode(NameEd, 38, CharSet, Name);
          TextToCode(OperatorEd, 20, CharSet, Operator);

          Seek(ResFile_, 0);
          BlockWrite(ResFile_, FileList[I + 1].Data[1], 100);
        end;
      }

      end;
      * )
      SummLoadSize:= SummLoadSize + FileSize(ResFile_);
      CloseFile(ResFile_);
      AddToLog(IntToStr(GLC) + '. ����������� ����: ' + FName);
//      if not CheckBox3.Checked then RichEdit1_.Refresh;

      for QQ:= 0 to Log2.Count - 1 do AddToLog(Log2.Strings[QQ]);
//      AddToLog(Log2.Text);
//      {$IFDEF CREATELOG}
//      RichEdit1.Lines.SaveToFile(LogName + '.log');
//      {$ENDIF}
    end;

  AddToLog(IntToStr(GLC) + '. GetTickCount - T1 = ' + IntToStr(GetTickCount - T1));

  if WorkID <> 1 then
  begin
    ProcRes:= Power_Off(WorkID);
    AddToLog(Format('%d. Power_Off Res = %d', [GLC, ProcRes]));
  end;

  Log2.Free;
  ReadFiles.Show;
  Self.Refresh;

  AddToLog(' ', True); *)
end;

procedure TFlashCardForm.Button3Click(Sender: TObject);
begin
  Timer1.Enabled:= False;
end;

procedure TFlashCardForm.Button7Click(Sender: TObject);
begin
  CreateFileList;
end;

procedure TFlashCardForm.CheckBox1Click(Sender: TObject);
begin
  Config.AskFlashName:= CheckBox1.Checked;
end;

procedure TFlashCardForm.Button10Click(Sender: TObject);
label
  L1;

var
  ExB: WORD;
  I, J, K: Integer;
  Chip: Integer;
  Page: Integer;
  Chip_: Integer;
  Page_: Integer;
  Item: TListItem;
  Res: Integer;
  ResFile: TMemoryStream;
  FP: TFlashPage;
  Flag: Boolean;
  SumRes: Integer;
  ErrCnt: Integer;
  Temp: array [0..61] of TFlashPage;
  EraseTime: DWord;
  WaitTime: DWord;
  TC1, TC2, TC3: DWord;
  SumEraseTime: DWord;
  SumWaitTime: DWord;

begin
(*
  ExB:= MessageBox(handle,pchar(LangTable.Caption['USB:Deleteallfiles?']), pchar(LangTable.Caption['common:Attention!']), 305);
  if Exb = IDOK then
  begin
    if not (AKPState or BUIState) then Exit;

    if AKPState then
    begin
      WorkID:= 0;
      Res:= power_on(WorkID);
      AddToLog(Format('%d. power_on(%d) Res = %d', [GLC, WorkID, Res]));
      if Res <= 0 then
      begin
        AddToLog(' ', True);
        Exit;
      end;
      Sleep(1000);
      ChipCount:= chipcntr(WorkID);
      AddToLog(Format('%d. ChipCntr Res = %d', [GLC, ChipCount]));
      if ChipCount <= 0 then
      begin
        AddToLog(' ', True);
        Exit;
      end;
    end
    else
    begin
      WorkID:= 1;
      Sleep(1000);
      ChipCount:= chipcntr(WorkID);
      AddToLog(Format('%d. ChipCntr Res = %d', [GLC, ChipCount]));
      if ChipCount <= 0 then
      begin
        AddToLog(' ', True);
        Exit;
      end;
    end;

    if ChipCount <> 8 then
    begin
      AddToLog('������ ��� ������ � ������ ������');
      ShowMessage('������ ��� ������ � ������ ������');
      AddToLog(' ', True);
      Exit;
    end;

    WaitSheet1.Show;
    ProgressBar1.Max:= 100;
    Self.Refresh;

    SumEraseTime:= 0;
    SumWaitTime:= 0;
    TC3:= GetTickCount;
    for I:= 0 to 1023 do
      for Chip:= 0 to ChipCount - 1 do
      begin
        TC2:= GetTickCount;
        EraseTime:= 0;          // ����� ������ ��������
        ErrCnt:= 10;
        repeat
          TC1:= GetTickCount;
          Res:= EraseBlks(I, Chip, 1, WorkID);
          Inc(EraseTime, GetTickCount - TC1);     // ������������ ����� ��������
          if Res <> 1 then
          begin
            AddToLog(Format('%d. Bad Erase Operation !!!!!!!!', [GLC]));
//            if not CheckBox3.Checked then RichEdit1_.Refresh;
            Sleep(50);
          end
          else Sleep(ceil(16 / ChipCount));
          Dec(ErrCnt);
        until (Res = 1) or (ErrCnt <= 0);
//        if not CheckBox3.Checked then RichEdit1_.Refresh;
        K:= 100 * (Chip + I * ChipCount) div (ChipCount * 1024);
        if K <> ProgressBar1.Position then ProgressBar1.Position:= K;
        WaitTime:= GetTickCount - TC2;           // ����� �������� � ����. �����. ���������
        AddToLog(Format('%d. Erased Chip: %d; Block: %d; Res: %d; WaitTime: %d; EraseTime: %d', [GLC, Chip, I, Res, WaitTime, EraseTime]));
        Inc(SumEraseTime, EraseTime);
        Inc(SumWaitTime, WaitTime);
      end;

    AddToLog(Format('Full Time %d; Sum (Erase + Wait) Time: %d (Erase: %d, Wait: %d)', [GetTickCount - TC3, SumEraseTime + SumWaitTime, SumEraseTime, SumWaitTime]));

    Sleep(100);

    if WorkID <> 1 then
    begin
      AddToLog(IntToStr(GLC) + '. ������ ��������� power_off');
      Res:= power_off(WorkID);
      AddToLog(Format('%d. power_off - Res: %d', [GLC, Res]));
    end;

    ReadFiles.Show;
    CreateFileList;
    Self.Refresh;
  end;
  AddToLog(' ', True); *)
end;

procedure TFlashCardForm.FormDestroy(Sender: TObject);
begin
//  if AKPState then Power_Off(0);
//  FinishLib;
end;

procedure TFlashCardForm.FormResize(Sender: TObject);
begin
  Panel10.Left:= (WaitSheet1.Width - Panel10.Width) div 2;
  Panel10.Top:= (WaitSheet1.Height - Panel10.Height) div 2;
  Panel13.Left:= (WaitSheet2.Width - Panel13.Width) div 2;
  Panel13.Top:= (WaitSheet2.Height - Panel13.Height) div 2;
end;

procedure TFlashCardForm.LoadListClick(Sender: TObject);
var
  Buff: TStringData;
  Res_: array [0..65535] of Byte;
  OldRes, Res, I, J, K: Integer;
  StartTime: Integer;
  S: string;

begin
(*
  if BusyFlag then Exit;
  BusyFlag:= True;

  Res_[1]:= 0;
  Res_[2]:= 0;
  Res_[3]:= 0;
  Res_[4]:= 0;
  Res_[5]:= $D5;
  Res_[6]:= $01;
  Res_[7]:= $00;

  Res:= SendInf(@Res_[1], 3, @Buff.Name[0]);
  if Res <> 0 then
  begin
    ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
    Exit;
  end;

  OldRes:= MaxInt;
  StartTime:= GetTickCount;
  repeat
    Res:= GetState;
    if OldRes <> Res then AddToLog(Format('%d. GetState Res = %d', [GLC, Res]));
    OldRes:= Res;
  until (Res = 2839) or (GetTickCount - StartTime > 5000);

  if Res <> 2839 then
  begin
    ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
    Exit;
  end;

  CurrCharSet:= Buff.CharSet;

//  Edit1.Text:= CodeToText(Buff.Name, CurrCharSet);

  ListBox11.Items.Clear;
  for I:= 0 to 99 do
  begin
//    S:= CodeToText(Buff.PeregList[I], CurrCharSet);
    if S <> '' then ListBox11.Items.Add(S);
  end;

  ListBox21.Items.Clear;
  for I:= 0 to 9 do
  begin
//    S:= CodeToText(Buff.OpperList[I], CurrCharSet);
    if S <> '' then ListBox21.Items.Add(S);
  end;

  ListBox31.Items.Clear;
  for I:= 0 to 29 do
  begin
//    S:= CodeToText(Buff.SpLabList[I], CurrCharSet);
    if S <> '' then ListBox31.Items.Add(S);
  end;

  BusyFlag:= False;
  *)
end;

procedure TFlashCardForm.SaveListClick(Sender: TObject);
var
  I: Integer;
  OldRes, Res: Integer;
  Buff: TStringData;
  StartTime: Integer;
  Res_: array [0..65535] of Byte;
  InBox: array [0..65535] of Byte;

begin
(*  Timer1.Enabled:= False;

// ---------- ��������� ����� ������� ------------------------------------------

  AddToLog('��������� ����� �������');

  Res_[1]:= 0;
  Res_[2]:= 0;
  Res_[3]:= 0;
  Res_[4]:= 0;
  Res_[5]:= $D6;
  Res_[6]:= $01;
  Res_[7]:= $00;

  Res:= SendInf(@Res_[1], 3, @InBox[0]);
  if Res <> 0 then
  begin
    ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
    Exit;
  end;

  OldRes:= MaxInt;
  StartTime:= GetTickCount;
  repeat
    Res:= GetState;
    if OldRes <> Res then AddToLog(Format('%d. GetState Res = %d', [GLC, Res]));
    OldRes:= Res;
  until (Res = 3) or (GetTickCount - StartTime > 5000);

  if Res <> 3 then
  begin
    ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
    Exit;
  end;

  CurrCharSet:= InBox[2];

  AddToLog('��������� ����� ������� - OK');

// -----------------------------------------------------------------------------

//  TextToCode(Edit1.Text, 38, CurrCharSet, Buff.Name);

  for I:= 0 to 99 do
  begin
//    if I < ListBox11.Items.Count then TextToCode(ListBox11.Items.Strings[I], 20, CurrCharSet, Buff.PeregList[I])
//                                 else TextToCode('', 20, 0, Buff.PeregList[I]);
    Buff.PeregList[I, 19]:= $FF;
  end;

  for I:= 0 to 9 do
  begin
//    if I < ListBox21.Items.Count then TextToCode(ListBox21.Items.Strings[I], 20, CurrCharSet, Buff.OpperList[I])
//                               else TextToCode('', 20, 0, Buff.OpperList[I]);
    Buff.OpperList[I, 19]:= $FF;
  end;

  for I:= 0 to 29 do
  begin
//    if I < ListBox31.Items.Count then TextToCode(ListBox31.Items.Strings[I], 20, CurrCharSet, Buff.SpLabList[I])
//                                 else TextToCode('', 20, 0, Buff.SpLabList[I]);
    Buff.SpLabList[I, 19]:= $FF;
  end;

  AddToLog('������ ���������� �����');

  Buff.CanCmd[1]:= $D4;
  Buff.CanCmd[2]:= $01;
  Buff.CanCmd[3]:= $00;
  Res:= SendInf(@Buff, 2838 + 3, @Res_);
  if Res <> 0 then
  begin
    ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
    Exit;
  end;

  OldRes:= MaxInt;
  StartTime:= GetTickCount;
  repeat
    Res:= GetState;
    if OldRes <> Res then AddToLog(Format('%d. GetState Res = %d', [GLC, Res]));
    OldRes:= Res;
  until (Res = 3) or (GetTickCount - StartTime > 500);
  AddToLog('������ ���������� ����� - OK');

  if Res <> 3 then
  begin
    ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
    Exit;
  end;

  Timer1.Enabled:= True;
  *)
end;

procedure TFlashCardForm.SetMode(ModeIdx_: Integer);
begin
  ModeIdx:= ModeIdx_;
  case ModeIdx of
    1: begin
         Self.Caption:= LangTable.Caption['USB:Filereadingfrommemorycard'];   
         TestState;
         Self.Width := 748;
       end;
    2: begin
         Self.Caption:= LangTable.Caption['USB:OperationwithControlandDisplayUnit'];
         TestState;
         Self.Width := 539;
       end;
  end;
end;

procedure TFlashCardForm.Button32Click(Sender: TObject);
begin
  RichEdit2.Clear;
end;

// -----[ ������ �� �������� ]----------------------------------------------------

procedure TFlashCardForm.ListBox11MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  I: Integer;

begin
  if Sender = ListBox11 then
  begin
    ListBox11.Tag:= 1;
    TPanel(ListBox11.Parent).Color:= clBlack;
    TPanel(ListBox12.Parent).Color:= clBtnFace;

    BtnAdd1.Glyph.Assign(LeftGImage.Picture.Bitmap);
    BtnDel1.Glyph.Assign(LeftGImage.Picture.Bitmap);
    BtnCopy1.Glyph.Assign(RightGImage.Picture.Bitmap);
    BtnMove1.Glyph.Assign(RightGImage.Picture.Bitmap);
  end
  else
  begin
    ListBox11.Tag:= 2;
    TPanel(ListBox11.Parent).Color:= clBtnFace;
    TPanel(ListBox12.Parent).Color:= clBlack;

    BtnAdd1.Glyph.Assign(RightGImage.Picture.Bitmap);
    BtnDel1.Glyph.Assign(RightGImage.Picture.Bitmap);
    BtnCopy1.Glyph.Assign(LeftGImage.Picture.Bitmap);
    BtnMove1.Glyph.Assign(LeftGImage.Picture.Bitmap);
  end;
end;

procedure TFlashCardForm.BtnAdd1Click(Sender: TObject); // �������
var
  I: Integer;
  Value: string;
  WorkBox: TListBox;

begin
  if ListBox11.Tag = 1 then WorkBox:= ListBox11
                       else WorkBox:= ListBox12;

  for I:= 0 to Self.ComponentCount - 1 do
    if (Self.Components[I] is TListBox) and (Self.Components[I] as TListBox).Focused then
    begin
      WorkBox:= TListBox(Self.Components[I]);
      Break;
    end;

  if (WorkBox = ListBox11) and (WorkBox.Items.Count >= 100) then Exit;
  if (WorkBox = ListBox12) and (WorkBox.Items.Count >= 300) then Exit;
  Value:= '';
  if InputQuery(LangTable.Caption['USB:Adding'], LangTable.Caption['FileBase:Enternewvalue'], Value) then WorkBox.ItemIndex:= WorkBox.Items.Add(Value);
end;

procedure TFlashCardForm.BtnDel1Click(Sender: TObject);
var
  I, J: Integer;
  Ok: Boolean;
  Tmp: Integer;
  Value: string;
  WorkBox: TListBox;

begin
  if ListBox11.Tag = 1 then WorkBox:= ListBox11
                       else WorkBox:= ListBox12;

  J:= 0;
  for I:= WorkBox.Items.Count - 1 downto 0 do if WorkBox.Selected[I] then Inc(J);

  if J = 1 then Ok:= MessageBox(Handle, PChar(LangTable.Caption['USB:SureDel']), PChar(LangTable.Caption['common:Attention!']), 33) = IDOK
           else if J > 1 then Ok:= MessageBox(Handle, PChar(LangTable.Caption['USB:Areyousurewanttodeletethisrecords?']), PChar(LangTable.Caption['common:Attention!']), 33) = IDOK
                         else Ok:= False;

  if OK then
    for I:= WorkBox.Items.Count - 1 downto 0 do if WorkBox.Selected[I] then WorkBox.Items.Delete(I);

end;

procedure TFlashCardForm.BtnCopy1Click(Sender: TObject);
var
  I: Integer;
  WorkBox1: TListBox;
  WorkBox2: TListBox;

begin
  if ListBox11.Tag = 1 then
  begin
    WorkBox1:= ListBox11;
    WorkBox2:= ListBox12;
  end
  else
  begin
    WorkBox1:= ListBox12;
    WorkBox2:= ListBox11;
  end;

  for I:= WorkBox1.Items.Count - 1 downto 0 do
    if WorkBox1.Selected[I] then
      if ((WorkBox2 = ListBox11) and (WorkBox2.Items.Count < 100)) or
         ((WorkBox2 = ListBox12) and (WorkBox2.Items.Count < 300)) then WorkBox2.Items.Add(WorkBox1.Items.Strings[I]);
end;

procedure TFlashCardForm.BtnMove1Click(Sender: TObject);
var
  I: Integer;
  WorkBox1: TListBox;
  WorkBox2: TListBox;

begin
  if ListBox11.Tag = 1 then
  begin
    WorkBox1:= ListBox11;
    WorkBox2:= ListBox12;
  end
  else
  begin
    WorkBox1:= ListBox12;
    WorkBox2:= ListBox11;
  end;

  for I:= WorkBox1.Items.Count - 1 downto 0 do
    if WorkBox1.Selected[I] then
      if ((WorkBox2 = ListBox11) and (WorkBox2.Items.Count < 100)) or
         ((WorkBox2 = ListBox12) and (WorkBox2.Items.Count < 300)) then
      begin
        WorkBox2.Items.Add(WorkBox1.Items.Strings[I]);
        WorkBox1.Items.Delete(I);
      end;
end;

procedure TFlashCardForm.ListBox21MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  I: Integer;

begin
  if Sender = ListBox21 then
  begin
    ListBox21.Tag:= 1;
    TPanel(ListBox21.Parent).Color:= clBlack;
    TPanel(ListBox22.Parent).Color:= clBtnFace;

    BtnAdd2.Glyph.Assign(LeftGImage.Picture.Bitmap);
    BtnDel2.Glyph.Assign(LeftGImage.Picture.Bitmap);
    BtnCopy2.Glyph.Assign(RightGImage.Picture.Bitmap);
    BtnMove2.Glyph.Assign(RightGImage.Picture.Bitmap);
  end
  else
  begin
    ListBox21.Tag:= 2;
    TPanel(ListBox21.Parent).Color:= clBtnFace;
    TPanel(ListBox22.Parent).Color:= clBlack;

    BtnAdd2.Glyph.Assign(RightGImage.Picture.Bitmap);
    BtnDel2.Glyph.Assign(RightGImage.Picture.Bitmap);
    BtnCopy2.Glyph.Assign(LeftGImage.Picture.Bitmap);
    BtnMove2.Glyph.Assign(LeftGImage.Picture.Bitmap);
  end;
end;

procedure TFlashCardForm.BtnAdd2Click(Sender: TObject); // ���������
var
  I: Integer;
  Value: string;
  WorkBox: TListBox;

begin
  if ListBox21.Tag = 1 then WorkBox:= ListBox21
                       else WorkBox:= ListBox22;

  for I:= 0 to Self.ComponentCount - 1 do
    if (Self.Components[I] is TListBox) and (Self.Components[I] as TListBox).Focused then
    begin
      WorkBox:= TListBox(Self.Components[I]);
      Break;
    end;

  if (WorkBox = ListBox21) and (WorkBox.Items.Count >= 10) then Exit;
  if (WorkBox = ListBox22) and (WorkBox.Items.Count >= 100) then Exit;

  Value:= '';
  if InputQuery(LangTable.Caption['USB:Adding'], LangTable.Caption['FileBase:Enternewvalue'], Value) then WorkBox.ItemIndex:= WorkBox.Items.Add(Value);

end;

procedure TFlashCardForm.BtnDel2Click(Sender: TObject);
var
  I, J: Integer;
  Ok: Boolean;
  Tmp: Integer;
  Value: string;
  WorkBox: TListBox;

begin
  if ListBox21.Tag = 1 then WorkBox:= ListBox21
                       else WorkBox:= ListBox22;

  J:= 0;
  for I:= WorkBox.Items.Count - 1 downto 0 do if WorkBox.Selected[I] then Inc(J);

  if J = 1 then Ok:= MessageBox(Handle, PChar(LangTable.Caption['USB:SureDel']), PChar(LangTable.Caption['common:Attention!']), 33) = IDOK
           else if J > 1 then Ok:= MessageBox(Handle, PChar(LangTable.Caption['USB:Areyousurewanttodeletethisrecords?']), PChar(LangTable.Caption['common:Attention!']), 33) = IDOK
                         else Ok:= False;

  if OK then
    for I:= WorkBox.Items.Count - 1 downto 0 do if WorkBox.Selected[I] then WorkBox.Items.Delete(I);

end;

procedure TFlashCardForm.BtnCopy2Click(Sender: TObject);
var
  I: Integer;
  WorkBox1: TListBox;
  WorkBox2: TListBox;

begin
  if ListBox21.Tag = 1 then
  begin
    WorkBox1:= ListBox21;
    WorkBox2:= ListBox22;
  end
  else
  begin
    WorkBox1:= ListBox22;
    WorkBox2:= ListBox21;
  end;

  for I:= WorkBox1.Items.Count - 1 downto 0 do
    if WorkBox1.Selected[I] then
      if ((WorkBox2 = ListBox21) and (WorkBox2.Items.Count < 10)) or
         ((WorkBox2 = ListBox22) and (WorkBox2.Items.Count < 100)) then WorkBox2.Items.Add(WorkBox1.Items.Strings[I]);

end;

procedure TFlashCardForm.BtnMove2Click(Sender: TObject);
var
  I: Integer;
  WorkBox1: TListBox;
  WorkBox2: TListBox;

begin
  if ListBox21.Tag = 1 then
  begin
    WorkBox1:= ListBox21;
    WorkBox2:= ListBox22;
  end
  else
  begin
    WorkBox1:= ListBox22;
    WorkBox2:= ListBox21;
  end;

  for I:= WorkBox1.Items.Count - 1 downto 0 do
    if WorkBox1.Selected[I] then
      if ((WorkBox2 = ListBox21) and (WorkBox2.Items.Count < 10)) or
         ((WorkBox2 = ListBox22) and (WorkBox2.Items.Count < 100)) then
        begin
          WorkBox2.Items.Add(WorkBox1.Items.Strings[I]);
          WorkBox1.Items.Delete(I);
        end;
end;

procedure TFlashCardForm.ListBox31MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  I: Integer;

begin
  if Sender = ListBox31 then
  begin
    ListBox31.Tag:= 1;
    TPanel(ListBox31.Parent).Color:= clBlack;
    TPanel(ListBox32.Parent).Color:= clBtnFace;

    BtnAdd3.Glyph.Assign(LeftGImage.Picture.Bitmap);
    BtnDel3.Glyph.Assign(LeftGImage.Picture.Bitmap);
    BtnCopy3.Glyph.Assign(RightGImage.Picture.Bitmap);
    BtnMove3.Glyph.Assign(RightGImage.Picture.Bitmap);
  end
  else
  begin
    ListBox31.Tag:= 2;
    TPanel(ListBox31.Parent).Color:= clBtnFace;
    TPanel(ListBox32.Parent).Color:= clBlack;

    BtnAdd3.Glyph.Assign(RightGImage.Picture.Bitmap);
    BtnDel3.Glyph.Assign(RightGImage.Picture.Bitmap);
    BtnCopy3.Glyph.Assign(LeftGImage.Picture.Bitmap);
    BtnMove3.Glyph.Assign(LeftGImage.Picture.Bitmap);
  end;
end;

procedure TFlashCardForm.BtnAdd3Click(Sender: TObject); // ������ �������
var
  I: Integer;
  Value: string;
  WorkBox: TListBox;

begin
  if ListBox31.Tag = 1 then WorkBox:= ListBox31
                       else WorkBox:= ListBox32;

  for I:= 0 to Self.ComponentCount - 1 do
    if (Self.Components[I] is TListBox) and (Self.Components[I] as TListBox).Focused then
    begin
      WorkBox:= TListBox(Self.Components[I]);
      Break;
    end;

  if (WorkBox = ListBox31) and (WorkBox.Items.Count >= 30) then Exit;
  if (WorkBox = ListBox32) and (WorkBox.Items.Count >= 100) then Exit;

  Value:= '';
  if InputQuery(LangTable.Caption['USB:Adding'], LangTable.Caption['FileBase:Enternewvalue'], Value) then WorkBox.ItemIndex:= WorkBox.Items.Add(Value);
end;

procedure TFlashCardForm.BtnDel3Click(Sender: TObject);
var
  I, J: Integer;
  Ok: Boolean;
  Tmp: Integer;
  Value: string;
  WorkBox: TListBox;

begin
  if ListBox31.Tag = 1 then WorkBox:= ListBox31
                       else WorkBox:= ListBox32;

  J:= 0;
  for I:= WorkBox.Items.Count - 1 downto 0 do if WorkBox.Selected[I] then Inc(J);

  if J = 1 then Ok:= MessageBox(Handle, PChar(LangTable.Caption['USB:SureDel']), PChar(LangTable.Caption['common:Attention!']), 33) = IDOK
           else if J > 1 then Ok:= MessageBox(Handle, PChar(LangTable.Caption['USB:Areyousurewanttodeletethisrecords?']), PChar(LangTable.Caption['common:Attention!']), 33) = IDOK
                         else Ok:= False;

  if OK then
    for I:= WorkBox.Items.Count - 1 downto 0 do if WorkBox.Selected[I] then WorkBox.Items.Delete(I);

end;

procedure TFlashCardForm.BtnCopy3Click(Sender: TObject);
var
  I: Integer;
  WorkBox1: TListBox;
  WorkBox2: TListBox;

begin
  if ListBox31.Tag = 1 then
  begin
    WorkBox1:= ListBox31;
    WorkBox2:= ListBox32;
  end
  else
  begin
    WorkBox1:= ListBox32;
    WorkBox2:= ListBox31;
  end;

  for I:= WorkBox1.Items.Count - 1 downto 0 do
    if WorkBox1.Selected[I] then
      if ((WorkBox2 = ListBox31) and (WorkBox2.Items.Count < 30)) or
         ((WorkBox2 = ListBox32) and (WorkBox2.Items.Count < 100)) then WorkBox2.Items.Add(WorkBox1.Items.Strings[I]);
end;

procedure TFlashCardForm.BtnMove3Click(Sender: TObject);
var
  I: Integer;
  WorkBox1: TListBox;
  WorkBox2: TListBox;

begin
  if ListBox31.Tag = 1 then
  begin
    WorkBox1:= ListBox31;
    WorkBox2:= ListBox32;
  end
  else
  begin
    WorkBox1:= ListBox32;
    WorkBox2:= ListBox31;
  end;

  for I:= WorkBox1.Items.Count - 1 downto 0 do
    if WorkBox1.Selected[I] then
      if ((WorkBox2 = ListBox31) and (WorkBox2.Items.Count < 30)) or
         ((WorkBox2 = ListBox32) and (WorkBox2.Items.Count < 100)) then
        begin
          WorkBox2.Items.Add(WorkBox1.Items.Strings[I]);
          WorkBox1.Items.Delete(I);
        end;
end;

// -----[ ������ �� ����� ������ ]----------------------------------------------------

procedure TFlashCardForm.LoadScreenShotClick(Sender: TObject);
var
  Buff: array [0..239, 0..39] of Byte;
  Res_: array [0..65535] of Byte;
  Res, I, J, K, X, Y, Pic: Integer;
  StartTime: Integer;
  S: string;
  WorkImage: TImage;
  OldRes: Integer;

begin
(*
  WaitSheet1.Show;
  Self.Refresh;
  ProgressBar1.Min:= 1;
  ProgressBar1.Max:= 8;

  for Pic:= 1 to 8 do
  begin

    if Assigned(SnapShot[Pic]) then
    begin
      SnapShot[Pic].Free;
      SnapShot[Pic]:= nil;
    end;

    SnapShot[Pic]:= TBitMap.Create;
    SnapShot[Pic].Width:= 320;
    SnapShot[Pic].Height:= 240;

    Res_[1]:= 0;
    Res_[2]:= 0;
    Res_[3]:= 0;
    Res_[4]:= 0;
    Res_[5]:= $D1;
    Res_[6]:= $01;
    Res_[7]:= Pic;

    Res:= SendInf(@Res_[1], 3, @Buff[0, 0]);
    if Res <> 0 then
    begin
      ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
      Exit;
    end;

    OldRes:= MaxInt;
    StartTime:= GetTickCount;
    repeat
      Res:= GetState;
      if OldRes <> Res then AddToLog(Format('%d. GetState Res = %d', [GLC, Res]));
      OldRes:= Res;
    until (Res = 9600) or (GetTickCount - StartTime > 5000);

    if Res <> 9600 then
    begin
      ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
      Exit;
    end;

    with SnapShot[Pic].Canvas do
    begin
      Brush.Color:= clWhite;
      FillRect(Rect(0, 0, 320, 240));

      for X:= 0 to 39 do
        for Y:= 0 to 239 do
        begin
          if Buff[Y, X] and $80 <> 0 then Pixels[X * 8 + 0, Y]:= clBlack;
          if Buff[Y, X] and $40 <> 0 then Pixels[X * 8 + 1, Y]:= clBlack;
          if Buff[Y, X] and $20 <> 0 then Pixels[X * 8 + 2, Y]:= clBlack;
          if Buff[Y, X] and $10 <> 0 then Pixels[X * 8 + 3, Y]:= clBlack;
          if Buff[Y, X] and $08 <> 0 then Pixels[X * 8 + 4, Y]:= clBlack;
          if Buff[Y, X] and $04 <> 0 then Pixels[X * 8 + 5, Y]:= clBlack;
          if Buff[Y, X] and $02 <> 0 then Pixels[X * 8 + 6, Y]:= clBlack;
          if Buff[Y, X] and $01 <> 0 then Pixels[X * 8 + 7, Y]:= clBlack;
        end;
    end;
    ProgressBar1.Position:= Pic;
  end;
  SetParam.Show; *)
end;

procedure TFlashCardForm.SavePicClick(Sender: TObject);
begin
  SaveDialog2.FileName:= Format(LangTable.Caption['USB:SnapshotNo'] + ' %d.bmp', [SelectSS]);
  if SaveDialog2.Execute then
    if (not FileExists(SaveDialog2.FileName)) or
       (FileExists(SaveDialog2.FileName) and (MessageBox(Handle, PChar(LangTable.Caption['USB:Filealreadyexist!'] + #10 + SaveDialog2.FileName + #10 + LangTable.Caption['USB:Filealreadyexist!']), PChar(LangTable.Caption['common:Attention!']), 36) = IDYES)) then
      ScreenShot.Picture.Bitmap.SaveToFile(SaveDialog2.FileName);
end;

procedure TFlashCardForm.TabSheet6Resize(Sender: TObject);
begin
end;

// -----[ ������ � ��������� ]----------------------------------------------------

procedure TFlashCardForm.LoadNoteBookClick(Sender: TObject);
var
  Res: Integer;
  Page: Integer;
  Line: Integer;
  Char_: Integer;
  Block: Integer;
  StartTime: Integer;
  OutDat: array [1..100] of Byte;
  BlockDat: packed record
    Text: array [1..16, 0..37] of Byte;
    Skip: array [1..448] of Byte;
  end;
  OldRes: Integer;

begin
(*
  if BusyFlag then Exit;
  BusyFlag:= True;

  WaitSheet1.Show;
  Self.Refresh;
  ProgressBar1.Min:= 0;
  ProgressBar1.Max:= 31;

  RichEdit2.Clear;
  RichEdit2.Lines.BeginUpdate;

  OutDat[1]:= 0;
  OutDat[2]:= 0;
  OutDat[3]:= 0;
  OutDat[4]:= 0;
  OutDat[5]:= $D2;
  OutDat[6]:= $01;

  CurrCharSet2:= 0;
  ProgressBar1.Position:= 0;

  for Block:= 31 downto 0 do
  begin
    OutDat[7]:= Block;

    Res:= SendInf(@OutDat[1], 3, @BlockDat);
    if Res <> 0 then
    begin
      ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
      Exit;
    end;

    StartTime:= GetTickCount;
    OldRes:= MaxInt;
    repeat
      Res:= GetState;
      if OldRes <> Res then AddToLog(Format('%d. GetState Res = %d', [GLC, Res]));
      OldRes:= Res;
    until (Res = 1056) or (GetTickCount - StartTime > 35000);

    if Res <> 1056 then
    begin
      ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
      Exit;
    end;

    if Block = 31 then CurrCharSet2:= BlockDat.Skip[448];

//    for Line:= 16 downto 1 do
//      RichEdit2.Lines.Insert(0, CodeToText(BlockDat.Text[Line], CurrCharSet2));

    ProgressBar1.Position:= 31 - Block;
  end;

  RichEdit2.Lines.EndUpdate;
  SetParam.Show;
  BusyFlag:= False;
  *)
end;

procedure TFlashCardForm.SaveNoteBookClick(Sender: TObject);
var
  Res_: array [0..65535] of Byte;
  InBox: array [0..65535] of Byte;

  Res: Integer;
  Idx: Integer;
  Page: Integer;
  Line: Integer;
  Char_: Integer;
  Block: Integer;
  StartTime: Integer;
  MemoLine: Integer;
  OldRes: Integer;

  InDat: array [0..33791] of Byte;
  LineDat: array [0..37] of Byte;

  BlockDat: packed record
    OutDat: array [1..7] of Byte;
    Text: array [1..16, 0..37] of Byte;
    Skip: array [1..448] of Byte;
  end;

begin
(*
  Timer1.Enabled:= False;

// ---------- ��������� ����� ������� ------------------------------------------

  AddToLog('��������� ����� �������');

  Res_[1]:= 0;
  Res_[2]:= 0;
  Res_[3]:= 0;
  Res_[4]:= 0;
  Res_[5]:= $D6;
  Res_[6]:= $01;
  Res_[7]:= $00;

  Res:= SendInf(@Res_[1], 3, @InBox[0]);
  if Res <> 0 then
  begin
    ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
    Exit;
  end;

  StartTime:= GetTickCount;
  OldRes:= MaxInt;
  repeat
    Res:= GetState;
    if OldRes <> Res then AddToLog(Format('%d. GetState Res = %d', [GLC, Res]));
    OldRes:= Res;
  until (Res = 3) or (GetTickCount - StartTime > 5000);

  if Res <> 3 then
  begin
    ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
    Exit;
  end;

  CurrCharSet2:= InBox[2];

  AddToLog('��������� ����� ������� - OK');

// -----------------------------------------------------------------------------

  BlockDat.OutDat[1]:= 0;
  BlockDat.OutDat[2]:= 0;
  BlockDat.OutDat[3]:= 0;
  BlockDat.OutDat[4]:= 0;
  BlockDat.OutDat[5]:= $D3;
  BlockDat.OutDat[6]:= $01;

  FillChar(BlockDat.Skip[1], 448, $FF);

  WaitSheet1.Show;
  Self.Refresh;
  ProgressBar1.Min:= 0;
  ProgressBar1.Max:= 31;

  for Block:= 0 to 31 do
  begin
    AddToLog(Format('������ �������� ����: %d', [Block]));

    BlockDat.OutDat[7]:= Block;

    for Line:= 0 to 15 do
    begin
      MemoLine:= Block * 16 + Line;
//      if MemoLine < RichEdit2.Lines.Count then TextToCode(RichEdit2.Lines.Strings[MemoLine], 38, CurrCharSet2, BlockDat.Text[Line + 1])
//                                          else TextToCode('', 38, CurrCharSet2, BlockDat.Text[Line + 1]);
    end;

    Res:= SendInf(@BlockDat, 1056 + 3, @InDat[0]);
    if Res <> 0 then
    begin
      ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
      Exit;
    end;

    OldRes:= MaxInt;
    StartTime:= GetTickCount;
    repeat
      Res:= GetState;
      if OldRes <> Res then AddToLog(Format('%d. GetState Res = %d', [GLC, Res]));
      OldRes:= Res;
    until (Res = 3) or (GetTickCount - StartTime > 5000);

    if Res <> 3 then
    begin
      ShowMessage(LangTable.Caption['USB:ErrorworkingwithCDU']);
      Exit;
    end;

    AddToLog(Format('������ �������� ����: %d - OK', [Block]));

    Sleep(400);
    ProgressBar1.Position:= Block;
  end;

  SetParam.Show;

  Timer1.Enabled:= True;
  *)
end;

procedure TFlashCardForm.ClearNoteBookClick(Sender: TObject);
begin
  RichEdit2.Clear;
end;

procedure TFlashCardForm.ImportNoteBookClick(Sender: TObject);
begin
  if OpenDialog1.Execute then RichEdit2.Lines.LoadFromFile(OpenDialog1.FileName);
end;

procedure TFlashCardForm.Timer2Timer(Sender: TObject);
var
  Col: Integer;
  Row: Integer;
  Page: Integer;
  I, EC: Integer;

begin
  Page:= RichEdit2.CaretPos.Y div 16;
  Col:= RichEdit2.CaretPos.X;
  Row:= RichEdit2.CaretPos.Y - Page * 16;

  Label9.Caption:= LangTable.Caption['USB:Page:'] + Format(' %d', [Page + 1]);
  Label19.Caption:= LangTable.Caption['USB:Column:'] + Format(' %d', [Col + 1]);
  Label20.Caption:= LangTable.Caption['USB:Line:'] + Format(' %d', [Row + 1]);

  Error2.Visible:= Col + 1 > 38;
  for I:= 0 to RichEdit2.Lines.Count - 1 do
    if Length(RichEdit2.Lines.Strings[I]) > 38 then
    begin
      Error2.Visible:= True;
      Break;
    end;

  Error1.Visible:= (Page + 1 > 32) or (RichEdit2.Lines.Count > 32 * 16);
end;

procedure TFlashCardForm.OnChangeLanguage(Sender: TObject);
var
  Item: TTreeNode;

begin
  Button7.Caption             := LangTable.Caption['USB:Update'];
  Button6.Caption             := LangTable.Caption['USB:Load'];
  Button10.Caption            := LangTable.Caption['USB:Delete'];
  CheckBox1.Caption           := LangTable.Caption['USB:Requestfilename'];
  Button3.Caption             := LangTable.Caption['USB:Close'];
  Button4.Caption             := LangTable.Caption['USB:Close'];
  Button1.Caption             := LangTable.Caption['USB:Close'];
  Button2.Caption             := LangTable.Caption['USB:Close'];

  ListView1.Column[1].Caption := LangTable.Caption['common:No'];
  ListView1.Column[2].Caption := LangTable.Caption['USB:Date'];
  ListView1.Column[3].Caption := LangTable.Caption['common:km'];
  ListView1.Column[4].Caption := LangTable.Caption['common:pk'];
  ListView1.Column[5].Caption := LangTable.Caption['common:m'];
  ListView1.Column[6].Caption := LangTable.Caption['common:Tracknumber'];
  ListView1.Column[7].Caption := LangTable.Caption['USB:Operator'];
  ListView1.Column[8].Caption := LangTable.Caption['common:Tracksection'];
  ListView1.Column[9].Caption := LangTable.Caption['USB:Railroadname'];

  BtnAdd1.Caption             := LangTable.Caption['USB:Add'];
  BtnAdd2.Caption             := LangTable.Caption['USB:Add'];
  BtnAdd3.Caption             := LangTable.Caption['USB:Add'];
  BtnDel1.Caption             := LangTable.Caption['USB:Delete'];
  BtnDel2.Caption             := LangTable.Caption['USB:Delete'];
  BtnDel3.Caption             := LangTable.Caption['USB:Delete'];
  BtnCopy1.Caption            := LangTable.Caption['USB:Copy'];
  BtnCopy2.Caption            := LangTable.Caption['USB:Copy'];
  BtnCopy3.Caption            := LangTable.Caption['USB:Copy'];
  BtnMove1.Caption            := LangTable.Caption['USB:Move'];
  BtnMove2.Caption            := LangTable.Caption['USB:Move'];
  BtnMove3.Caption            := LangTable.Caption['USB:Move'];

  Label8.Caption              := LangTable.Caption['USB:Pleasewait'];
  Label6.Caption              := LangTable.Caption['USB:Pleasewait'];

  ClearNoteBook.Caption       := LangTable.Caption['USB:Clear'];

  Label9.Caption              := LangTable.Caption['USB:Page:'];
  Label19.Caption             := LangTable.Caption['USB:Column:'];
  Label20.Caption             := LangTable.Caption['USB:Line:'];

  Label1.Caption              := LangTable.Caption['USB:CDUlist'];
  Label3.Caption              := LangTable.Caption['USB:CDUlist'];
  Label5.Caption              := LangTable.Caption['USB:CDUlist'];

  Label2.Caption              := LangTable.Caption['USB:Archives'];
  Label7.Caption              := LangTable.Caption['USB:Archives'];
  Label4.Caption              := LangTable.Caption['USB:Archives'];

  TBItem2.Caption             := LangTable.Caption['USB:SelectAll'];

  Label11.Caption:= LangTable.Caption['USB:Usingmemorycardadapter(MCA)'] + #10 + #13 +
                    LangTable.Caption['USB:1.InsertmemorycardinMCA'] + #10 + #13 +
                    LangTable.Caption['USB:2.ConnectMCAtoPC'] + #10 + #13 +
                    LangTable.Caption['USB:3.SwichonMCA'] + #10 + #13 +
                    '' + #10 + #13 +
                    LangTable.Caption['USB:Usingcontrolanddisplayunit(CDU)'] + #10 + #13 +
                    LangTable.Caption['USB:1.InsertmemorycardinCDU'] + #10 + #13 +
                    LangTable.Caption['USB:2.ConnectCDUtoPC'] + #10 + #13 +
                    LangTable.Caption['USB:3.ConnectpowersupplytoCDU'] + #10 + #13 +
                    LangTable.Caption['USB:4.SwichonCDU'] + #10 + #13 +
                    LangTable.Caption['USB:5.SelectinCDUmenu"Options\USBmode"'];

  Label12.Caption:= LangTable.Caption['USB:1.Connectcontrolanddisplayunit(CDU)toPC'] + #10 + #13 +
                    LangTable.Caption['USB:2.ConnectpowersupplytoCDU'] + #10 + #13 +
                    LangTable.Caption['USB:3.SwichonCDU'] + #10 + #13 +
                    LangTable.Caption['USB:4.SelectinCDUmenu"Options\USBmode"'];

  {$IFDEF FILUSX17}
  Label11.Caption:= Label12.Caption;
  {$ENDIF}


  TBItem7.Caption:= LangTable.Caption['USB:ImportArchive'];
  TBItem8.Caption:= LangTable.Caption['USB:ExportArchive'];

  SavePic.Caption:= LangTable.Caption['USB:Save'] + ' ' + LangTable.Caption['USB:tofile'];
  ImportNoteBook.Caption:= LangTable.Caption['USB:Insert'] + ' ' + LangTable.Caption['USB:fromfile'];
  ExportNoteBook.Caption:= LangTable.Caption['USB:Save'] + ' ' + LangTable.Caption['USB:tofile'];

  tbLoad.Caption:= LangTable.Caption['USB:Download'] + ' ' + LangTable.Caption['USB:fromCDU'];
  tbSave.Caption:= LangTable.Caption['USB:Save'] + ' ' + LangTable.Caption['USB:inCDU'];
  Label16.Caption:= LangTable.Caption['USB:DivisionName'] + ':';
  TabSheet1.Caption:= LangTable.Caption['USB:Operators'];
  TabSheet9.Caption:= LangTable.Caption['USB:Tracksectionnames'];
  TabSheet10.Caption:= LangTable.Caption['USB:SpecialLabels'];

  tsRegData.Caption:= LangTable.Caption['USB:RegistrationDate'];
  tsSnapShots.Caption:= LangTable.Caption['USB:SaveSnapshots'];
  stNotes.Caption:= LangTable.Caption['USB:Personalnotes'];

  ListBox1.Clear;
  ListBox1.Items.AddObject(LangTable.Caption['USB:SnapshotNo'] + '1', Pointer(51));
  ListBox1.Items.AddObject(LangTable.Caption['USB:SnapshotNo'] + '2', Pointer(52));
  ListBox1.Items.AddObject(LangTable.Caption['USB:SnapshotNo'] + '3', Pointer(53));
  ListBox1.Items.AddObject(LangTable.Caption['USB:SnapshotNo'] + '4', Pointer(54));
  ListBox1.Items.AddObject(LangTable.Caption['USB:SnapshotNo'] + '5', Pointer(55));
  ListBox1.Items.AddObject(LangTable.Caption['USB:SnapshotNo'] + '6', Pointer(56));
  ListBox1.Items.AddObject(LangTable.Caption['USB:SnapshotNo'] + '7', Pointer(57));
  ListBox1.Items.AddObject(LangTable.Caption['USB:SnapshotNo'] + '8', Pointer(58));
end;

procedure TFlashCardForm.TBItem2Click(Sender: TObject);
var
  I: Integer;

begin

  for I:= 0 to ListView1.Items.Count - 1 do
    ListView1.Items.Item[I].Checked:= TBItem2.Tag = 0;

  if TBItem2.Tag = 0 then TBItem2.Tag:= 1
                     else TBItem2.Tag:= 0;
end;

procedure TFlashCardForm.tbLoadClick(Sender: TObject);
begin
  LoadListClick(Sender);
  LoadScreenShotClick(Sender);
  LoadNoteBookClick(Sender);
  ListBox1.ItemIndex:= 0;
  ListBox1Click(nil);

  FSave1:= ListBox11.Items.Text;
  FSave2:= ListBox21.Items.Text;
  FSave3:= ListBox31.Items.Text;
  FSave4:= Edit1.Text;
  FSave5:= RichEdit2.Lines.Text;
end;

procedure TFlashCardForm.tbSaveClick(Sender: TObject);
begin
  SaveListClick(Sender);
  SaveNoteBookClick(Sender);

  FSave1:= ListBox11.Items.Text;
  FSave2:= ListBox21.Items.Text;
  FSave3:= ListBox31.Items.Text;
  FSave4:= Edit1.Text;
  FSave5:= RichEdit2.Lines.Text;
end;

procedure TFlashCardForm.ListBox1Click(Sender: TObject);
var
  I: Integer;

begin
  if ListBox1.ItemIndex = - 1 then Exit;
  I:= Integer(ListBox1.Items.Objects[ListBox1.ItemIndex]) - 50;
  SelectSS:= I;
  if Assigned(SnapShot[I]) then ScreenShot.Picture.Bitmap.Assign(SnapShot[I]);
end;

procedure TFlashCardForm.ButtonBClick(Sender: TObject);
begin
//  RichEdit1_.SelectAll;
end;

function ExecuteAndWait(FileName: string; HideApplication: boolean): boolean;
var
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
  exitc: cardinal;
begin
  FillChar(StartupInfo, sizeof(StartupInfo), 0);
  with StartupInfo do begin
    cb := Sizeof(StartupInfo);
    dwFlags := STARTF_USESHOWWINDOW;
    wShowWindow := SW_SHOW;
  end;
  if not CreateProcess(nil, PChar(FileName), nil, nil, false,
    CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS, nil, nil,
    StartupInfo, ProcessInfo) then result := false
  else begin
    if HideApplication then begin
      Application.Minimize;
      ShowWindow(Application.Handle, SW_HIDE);
      WaitforSingleObject(ProcessInfo.hProcess, INFINITE);
    end else
      while WaitforSingleObject(ProcessInfo.hProcess, 100) =
        WAIT_TIMEOUT do begin
        Application.ProcessMessages;
        if Application.Terminated
          then TerminateProcess(ProcessInfo.hProcess, 0);
      end;
    GetExitCodeProcess(ProcessInfo.hProcess, exitc);
    result := (exitc = 0);
    if HideApplication then begin
      ShowWindow(Application.Handle, SW_SHOW);
      Application.Restore;
      Application.BringToFront;
    end;
  end;
end;

procedure TFlashCardForm.TBItem4Click(Sender: TObject);
begin
  if OpenDialog2.Execute then
  begin
    with TRegistry.Create do
      try
        RootKey := HKEY_LOCAL_MACHINE;
        DeleteKey('\SOFTWARE\Avikon-11')
      finally
        Free;
      end;

    if not ExecuteAndWait(Format('regedit.exe -s "%s"', [OpenDialog2.FileName]), False) then
      MessageBox(Handle, PChar(LangTable.Caption['USB:ErrorArchiveImport']), PChar(LangTable.Caption['Messages:Attention']), 16);
    LoadToRegistry;
  end;
end;

procedure TFlashCardForm.TBItem8Click(Sender: TObject);
var
  FileName, Key: string;

begin
  if SaveDialog3.Execute then
  begin
    SaveToRegistry;
    FileName := SaveDialog3.FileName;
    Key:= 'HKEY_LOCAL_MACHINE\SOFTWARE\Avikon-11';
    if ShellExecute(Handle, 'open', 'regedit.exe', PChar(Format('/e "%s" "%s"', [FileName, Key])), '', SW_SHOWDEFAULT) <= 32 then
      MessageBox(Handle, PChar(LangTable.Caption['USB:ErrorArchiveExport']), PChar(LangTable.Caption['Messages:Attention']), 16);
  end;
end;

procedure TFlashCardForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
var
  s: string;

begin
  if ModeIdx = 1 then
    if Msg.CharCode = 76 then
    begin
      s:= ExtractFilePath(Application.ExeName) + 'FlashCard.log';
      if ShellExecute(Handle, 'open', 'notepad.exe', PChar(s), '', SW_SHOWDEFAULT) <= 32 then
    end;
end;

end.
