unit FileCorrectUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Registry, LanguageUnit, ExtCtrls;

type
  TFileCorrectForm = class(TForm)
    Edit1: TComboBox;
    Edit2: TComboBox;
    Edit3: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
    Panel1: TPanel;
    Image1: TImage;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure OnChangeLanguage(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
  public
    procedure Work(var Name, Operator, SecName: string; FileName: string);
  end;

var
  FileCorrectForm: TFileCorrectForm;

implementation

{$R *.dfm}

procedure TFileCorrectForm.FormCreate(Sender: TObject);
var
  I, Count: Integer;

begin
  OnChangeLanguage(Self);
  with TRegistry.Create do
  try
    Edit1.Items.Clear;
    Edit2.Items.Clear;
    Edit3.Items.Clear;

    RootKey := HKEY_LOCAL_MACHINE;
    if OpenKey('\SOFTWARE\Avikon-11\Peregon', True) and ValueExists('IntemCount') then
    begin
      Count:= ReadInteger('IntemCount');
      for I:= 0 to Count - 1 do Edit1.Items.Add(ReadString(Format('Item_%d', [I]))); // ������ ������
      CloseKey;
    end;
    if OpenKey('\SOFTWARE\Avikon-11\Operators', True) and ValueExists('IntemCount') then
    begin
      Count:= ReadInteger('IntemCount');
      for I:= 0 to Count - 1 do Edit2.Items.Add(ReadString(Format('Item_%d', [I]))); // ������ ������
      CloseKey;
    end;
    if OpenKey('\SOFTWARE\Avikon-11\Labels', True) and ValueExists('IntemCount') then
    begin
      Count:= ReadInteger('IntemCount');
      for I:= 0 to Count - 1 do Edit3.Items.Add(ReadString(Format('Item_%d', [I]))); // ������ ������
      CloseKey;
    end;
  finally
    Free;
  end;
end;

procedure TFileCorrectForm.OnChangeLanguage(Sender: TObject);
begin
  Label1.Caption:= LangTable.Caption['FileInfo:Organization'];
  Label2.Caption:= LangTable.Caption['FileInfo:Operator'];
  Label3.Caption:= LangTable.Caption['FileInfo:Tracksection'];
  Self.Caption:= LangTable.Caption['Misc:Attention!'];
end;

procedure TFileCorrectForm.Work(var Name, Operator, SecName: string; FileName: string);
begin
  Label4.Caption:= Format(LangTable.Caption['Messages:'], [FileName]);

  if Name <> '' then
  begin
    Edit1.Text:= Name;
    Edit1.Enabled:= False;
  end;
  if Operator <> '' then
  begin
    Edit2.Text:= Operator;
    Edit2.Enabled:= False;
  end;
  if SecName <> '' then
  begin
    Edit3.Text:= SecName;
    Edit3.Enabled:= False;
  end;

  if ShowModal = mrOk then
  begin
    Name:=     Edit1.Text;
    Operator:= Edit2.Text;
    SecName:=  Edit3.Text;
  end;
end;

procedure TFileCorrectForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose:= (Edit1.Text <> '') and (Edit2.Text <> '') and (Edit3.Text <> '');
end;

end.
