unit BaseCfg2Unit; {Language 4}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, ComCtrls, ExtCtrls;

type
  TBaseCfg2Form = class(TForm)
    MoveUpButton: TButton;
    MoveDownButton: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Label2_: TPageControl;
    Label2: TTabSheet;
    ListBox1: TListBox;
    PageControl1: TPageControl;
    Label1: TTabSheet;
    ListBox2: TCheckListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    CheckBox1: TCheckBox;
    procedure MoveUpButtonClick(Sender: TObject);
    procedure MoveDownButtonClick(Sender: TObject);
    procedure CreateList(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure ListBox2ClickCheck(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure OnChangeLanguage(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox2Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox2DblClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    OwnerForm: TObject;
  end;

var
  BaseCfg2Form: TBaseCfg2Form;

implementation

{$R *.dfm}

uses
  FileBaseUnit, ConfigUnit, LanguageUnit;


procedure TBaseCfg2Form.CreateList(Sender: TObject);
var
  I: Integer;
//  Mode: string;

begin
  ListBox1.Clear;
  ListBox2.Clear;
  for I:= 0 to High(Config.Fields) do
  begin
  {
   if Config.Fields[I].Enabled then
    case Config.Fields[I].Mode of
      fmVisible: Mode:= 'Visible';
         fmHide: Mode:= 'Hide';
         fmTree: Mode:= 'Tree';
    end;

    Memo1.Lines.Add(Format('Index: %d; Enabled: %d; Mode: %s; Name: %s', [I, Ord(Config.Fields[I].Enabled), Mode, Config.Fields[I].Name]));
  }
   if Config.Fields[I].Enabled then
    case Config.Fields[I].Mode of
      fmVisible: ListBox2.Checked[ListBox2.Items.AddObject(Config.Fields[I].Name, Pointer(I))]:= True;
         fmHide: ListBox2.Checked[ListBox2.Items.AddObject(Config.Fields[I].Name, Pointer(I))]:= False;
         fmTree: ListBox1.Items.AddObject(Config.Fields[I].Name, Pointer(I));
    end;

  end;
  OnChangeLanguage(nil);
  CheckBox1.Checked:= Config.OnlyName;
end;

procedure TBaseCfg2Form.MoveUpButtonClick(Sender: TObject);
var
  tmp: TMyField;
  I, J: Integer;
  Flg: Boolean;

begin
  if ListBox1.ItemIndex > 0 then
  begin
    I:= Integer(ListBox1.Items.Objects[ListBox1.ItemIndex]);
    J:= I - 1;
    Flg:= False;
    while J >= 0 do
    begin
      if Config.Fields[J].Enabled and (Config.Fields[J].Mode = fmTree) then
      begin
        Flg:= True;
        Break;
      end;
      Dec(J);
    end;
    if Flg then
    begin
      tmp:= Config.Fields[J];
      Config.Fields[J]:= Config.Fields[I];
      Config.Fields[I]:= tmp;
      I:= ListBox1.ItemIndex;
      CreateList(nil);
      ListBox1.ItemIndex:= I - 1;
    end;
  end;
  if ListBox2.ItemIndex > 0 then
  begin
    I:= Integer(ListBox2.Items.Objects[ListBox2.ItemIndex]);
    J:= I - 1;
    Flg:= False;
    while J >= 0 do
    begin
      if Config.Fields[J].Enabled and (Config.Fields[J].Mode in [fmVisible, fmHide]) then
      begin
        Flg:= True;
        Break;
      end;
      Dec(J);
    end;
    if Flg then
    begin
      tmp:= Config.Fields[J];
      Config.Fields[J]:= Config.Fields[I];
      Config.Fields[I]:= tmp;
      I:= ListBox2.ItemIndex;
      CreateList(nil);
      ListBox2.ItemIndex:= I - 1;
    end;
  end;
end;

procedure TBaseCfg2Form.MoveDownButtonClick(Sender: TObject);
var
  tmp: TMyField;
  I, J: Integer;
  Flg: Boolean;

begin
  if (ListBox1.ItemIndex <> - 1) and (ListBox1.ItemIndex <> ListBox1.Count - 1) then
  begin
    I:= Integer(ListBox1.Items.Objects[ListBox1.ItemIndex]);
    J:= I + 1;
    Flg:= False;

    while J < Length(Config.Fields) do
    begin
      if Config.Fields[J].Enabled and (Config.Fields[J].Mode = fmTree) then
      begin
        Flg:= True;
        Break;
      end;
      Inc(J);
    end;
    if Flg then
    begin
      tmp:= Config.Fields[J];
      Config.Fields[J]:= Config.Fields[I];
      Config.Fields[I]:= tmp;
      I:= ListBox1.ItemIndex;
      CreateList(nil);
      ListBox1.ItemIndex:= I + 1;
    end;
  end;
  if (ListBox2.ItemIndex <> - 1) and (ListBox2.ItemIndex <> ListBox2.Count - 1) then
  begin
    I:= Integer(ListBox2.Items.Objects[ListBox2.ItemIndex]);
    J:= I + 1;
    Flg:= False;

    while J < Length(Config.Fields) do
    begin
      if Config.Fields[J].Enabled and (Config.Fields[J].Mode in [fmVisible, fmHide]) then
      begin
        Flg:= True;
        Break;
      end;
      Inc(J);
    end;
    if Flg then
    begin
      tmp:= Config.Fields[J];
      Config.Fields[J]:= Config.Fields[I];
      Config.Fields[I]:= tmp;
      I:= ListBox2.ItemIndex;
      CreateList(nil);
      ListBox2.ItemIndex:= I + 1;
    end;
  end;
end;

procedure TBaseCfg2Form.Button4Click(Sender: TObject);
var
  I: Integer;
  tmp: TMyField;
  Flag: Boolean;

begin
  if (ListBox2.ItemIndex <> - 1) and
     ((Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 11) or
      (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 14) or
      (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 10) or
      (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 9) or
//      (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 6) or
      (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 4) or
      (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 15)) then
  begin
    Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].Mode:= fmTree;

    repeat
      Flag:= True;
      for I:= 1 to High(Config.Fields) do
        if (Config.Fields[I - 1].Mode <> fmTree) and (Config.Fields[I].Mode = fmTree) then
        begin
          tmp:= Config.Fields[I - 1];
          Config.Fields[I - 1]:= Config.Fields[I];
          Config.Fields[I]:= tmp;
          Flag:= False;
        end;
    until Flag;

    CreateList(nil);
    // TFileBaseForm(OwnerForm).SetUpColums;
  end;
end;

procedure TBaseCfg2Form.ListBox2ClickCheck(Sender: TObject);
var
  I: Integer;

begin
  for I:= 0 to ListBox2.Items.Count - 1 do
    case ListBox2.Checked[I] of
      True: Config.Fields[Integer(ListBox2.Items.Objects[I])].Mode:= fmVisible;
     False: Config.Fields[Integer(ListBox2.Items.Objects[I])].Mode:= fmHide;
    end;
  // TFileBaseForm(OwnerForm).SetUpColums;
end;

procedure TBaseCfg2Form.Button5Click(Sender: TObject);
var
  I: Integer;
  tmp: TMyField;
  Flag: Boolean;

begin
  if ListBox1.ItemIndex <> - 1 then
  begin
    Config.Fields[Integer(ListBox1.Items.Objects[ListBox1.ItemIndex])].Mode:= fmVisible;
    repeat
      Flag:= True;
      for I:= 1 to High(Config.Fields) do
        if (Config.Fields[I - 1].Mode <> fmTree) and (Config.Fields[I].Mode = fmTree) then
        begin
          tmp:= Config.Fields[I - 1];
          Config.Fields[I - 1]:= Config.Fields[I];
          Config.Fields[I]:= tmp;
          Flag:= False;
        end;
    until Flag;

    CreateList(nil);
    // TFileBaseForm(OwnerForm).SetUpColums;
  end;
end;

procedure TBaseCfg2Form.Button1Click(Sender: TObject);
begin
  ListBox1.Refresh;
  ListBox2.Refresh;
end;

procedure TBaseCfg2Form.Button3Click(Sender: TObject);
begin
  TFileBaseForm(OwnerForm).SetUpColums;
end;

procedure TBaseCfg2Form.OnChangeLanguage(Sender: TObject);
begin
  Self.Font.Name        := LangTable.Caption['General:FontName'];

  Self.Caption                := LangTable.Caption['FileBaseTable:Setupcolumns'];
  Label2.Caption              := LangTable.Caption['FileBaseTable:Limboftree'];
  Label1.Caption              := LangTable.Caption['FileBaseTable:Tablecolumns'];
  MoveUpButton.Caption        := LangTable.Caption['FileBaseTable:MoveUp'];
  MoveDownButton.Caption      := LangTable.Caption['FileBaseTable:MoveDown'];
  CheckBox1.Caption           := LangTable.Caption['FileBaseTable:Hidefilepath'];
end;

procedure TBaseCfg2Form.ListBox1Click(Sender: TObject);
begin
  ListBox2.ItemIndex:= - 1;
end;

procedure TBaseCfg2Form.ListBox2Click(Sender: TObject);
var
  Flg: Boolean;

begin
  ListBox1.ItemIndex:= - 1;

  if ListBox2.ItemIndex <> - 1 then
  begin
    Flg:= (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 11) or
          (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 14) or
          (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 10) or
          (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 9) or
//          (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 6) or
          (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 4) or
          (Config.Fields[Integer(ListBox2.Items.Objects[ListBox2.ItemIndex])].DataID = 15);
    Button4.Enabled:= Flg;
    Button5.Enabled:= Flg;
  end;
end;

procedure TBaseCfg2Form.CheckBox1Click(Sender: TObject);
begin
  Config.OnlyName:= CheckBox1.Checked;
end;

procedure TBaseCfg2Form.ListBox1DblClick(Sender: TObject);
begin
  Button5.Click;
end;

procedure TBaseCfg2Form.ListBox2DblClick(Sender: TObject);
begin
  Button4.Click;
end;

end.
