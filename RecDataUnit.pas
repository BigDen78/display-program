// ��������� 1 - ���������� ����� ������������ �������
// ��������� 2 - ���������� ����� 1 ������
// ������� ��������� �� � ����� ��������
// �������� �������� ������ ����� �� �������� �� TestUnit2
// �������� ����������� ������������ �������
// ������ � ����
//--------------------------------------------------------------------------------
// + ����������
//   ���������� ���� ������� ���������
//   �� ����� 1 ����� (6 ��� 7 ��� 1) + ������
//   ��������� �������� �����
//   ��������� ��������� ����
//   ��������� ����� � 1 - � ����������
// + ������������� ��������� ������������� ������������� ������� ����� 0 � 1 �������
// + ������������� ������ ����� 1 ������



{$I DEF.INC}

unit RecDataUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls, MyTypes, Math, Avk11Engine,
  CheckLst, Series, TeEngine, TeeProcs, Chart, Types, RecEngine, RecTypes, {AvDataNet,}
  Grids, ArrowCha, Spin, TeeShape, RecDebugUnit, FiltrUnit;

type
  TFindHoleParam = record
    Ch: Integer;
    BtmSglMin: Integer;
    BtmSglMax: Integer;
    DZoneMin: Integer;
    DZoneMax: Integer;
    AngleMin: Single;
    AngleMax: Single;
  end;

const

  EchoSize: array [0..3, 0..3] of Integer = ((1, 1, 0, 0), (1, 1, 1, 1), (1, 1, 2, 2), (2, 2, 2, 2));

  AlgNo_BHSearch   = 99001;
  AlgNo_PBDeadZone = 99002;

  AlgNameList: array [AlgNo_BHSearch..AlgNo_PBDeadZone] of string =

                                                         ('����� ��',
                                                          '�������� ������ ��� ����� ���. 1, 6, 7');

  FindHoleParam: array [0..5] of TFindHoleParam =   // ���� (�� ��������) ����� ����� (��� ���������) �� �������� ��

  ((Ch: 1; BtmSglMin: 57 * 3; BtmSglMax: 63 * 3; DZoneMin: 56; DZoneMax: 111; AngleMin:   - 1; AngleMax:     1),     // �-65
   (Ch: 6; BtmSglMin: 57 * 3; BtmSglMax: 63 * 3; DZoneMin: 43; DZoneMax: 102; AngleMin: - 1.5; AngleMax: - 0.3),     // �-65
   (Ch: 7; BtmSglMin: 57 * 3; BtmSglMax: 63 * 3; DZoneMin: 43; DZoneMax: 102; AngleMin:   0.3; AngleMax:   1.5),     // �-65
   (Ch: 1; BtmSglMin: 46 * 3; BtmSglMax: 49 * 3; DZoneMin: 57; DZoneMax:  84; AngleMin:   - 1; AngleMax:     1),     // ������
   (Ch: 6; BtmSglMin: 46 * 3; BtmSglMax: 49 * 3; DZoneMin: 46; DZoneMax:  74{67}; AngleMin: - 1.5; AngleMax: - 0.3),     // ������
   (Ch: 7; BtmSglMin: 46 * 3; BtmSglMax: 49 * 3; DZoneMin: 46; DZoneMax:  74{67}; AngleMin:   0.3; AngleMax:   1.5));    // ������

                   // ���� ����� ��
  TBSGisMin = 105; // 35 ���
  TBSGisMax = 255; // 85 ���

//  FDelayStep: array [1..7] of Integer = (2, 2, 2, 2, 2, 2, 2);
//  FCountTh: array [1..7] of Integer = (6, 6, 6, 6, 6, 6, 6);

//  FDelayStep: array [1..7] of Integer = (6, 2, 2, 2, 2, 2, 2);
//  FCountTh: array [1..7] of Integer = (10, 6, 6, 6, 6, 6, 6);


type
  {$IFDEF RECDEBUG}
  TOutInfoItem = record
    Rail: RRail;
    Ch: Integer;
    Sgl_Rt: TRect;
    Dbg_Rt: TRect;
  end;

  TOutInfo = array of TOutInfoItem;

  TOutLink = record
    Rt: TRect;
    Visible: Boolean;
  end;

  TOnScrPBItem = record
    PtList: array of TPoint;
    PBIdx: Integer;
  end;
  {$ENDIF}
(*
  TFtrCh1GisItem = packed record
    ECount: Byte;
    StGlue: Integer;
    LastCrd: Integer;
    MaxLen: Byte;
  end;

// ����� 1
// ��������� �������� 9..204
// ���  6
// ��������� 0..34
//
// ����� 2..7
// ��������� �������� 3..184
// ���  2
// ��������� 0..92

  TFiltrData = packed record
    Reg: TRange;
    Ch1Gis: array [r_Left..r_Right, 3..15, 0..34] of TFtrCh1GisItem;
    Ch27Gis: array [r_Left..r_Right, 2..7, 3..15, 0..92] of Byte;
  end;

//    FFlgBuff: array [r_Left..r_Right, 1..7] of array of Byte;
//    FFtrGis: array [r_Left..r_Right, 1..7, 3..15, 0..255] of Integer;
//    FFtrCh1Gis: array [r_Left..r_Right, 3..15, 0..255] of TFtrCh1GisItem;
//    FFtrCh27Gis: array [r_Left..r_Right, 2..7, 3..15, 0..255] of Byte;
*)

  TRecDataForm = class(TForm)
    ScrollBar: TScrollBar;
    ScrollBox: TScrollBox;
    PaintBox: TPaintBox;
    Panel1: TPanel;
    X1: TSpeedButton;
    X2: TSpeedButton;
    X3: TSpeedButton;
    X4: TSpeedButton;
    X5: TSpeedButton;
    X6: TSpeedButton;
    ZoomBar: TTrackBar;
    Panel7: TPanel;
    Panel8: TPanel;
    CheckBox2: TCheckBox;
    Panel2: TPanel;
    ViewTh: TTrackBar;
    cbZoomIn: TCheckBox;
    cbZoomOut: TCheckBox;
    PageControl1: TPageControl;
    ViewStrip: TCheckListBox;
    Panel3: TPanel;
    BSCheck: TCheckListBox;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Test: TCheckBox;
    Chart1: TChart;
    Series1: TLineSeries;
    BMGraphList: TCheckListBox;
    BMListItems: TCheckListBox;
    Panel5: TPanel;
    CheckBox1: TCheckBox;
    Panel4: TPanel;
    ComboBox1: TComboBox;
    Chart2: TChart;
    LineSeries1: TLineSeries;
    LineSeries2: TLineSeries;
    Panel6: TPanel;
    CheckBox5: TCheckBox;
    DelayChart: TChart;
    SrcDelaySeries: TLineSeries;
    Panel9: TPanel;
    ComboBox2: TComboBox;
    ScrollBar1: TScrollBar;
    Splitter1: TSplitter;
    AmplChart: TChart;
    SrcAmplSeries: TLineSeries;
    DstAmplSeries: TLineSeries;
    DstDelaySeries: TLineSeries;
    Label5: TLabel;
    CheckBox7: TCheckBox;
    Splitter2: TSplitter;
    Panel10: TPanel;
    ListBox1: TListBox;
    Panel11: TPanel;
    Button1: TButton;
    ComboBox3: TComboBox;
    Panel12: TPanel;
    Chart3: TChart;
    PaintBox1: TPaintBox;
    Series2: TLineSeries;
    Chart4: TChart;
    Panel13: TPanel;
    ScrollBar2: TScrollBar;
    CheckBox11: TCheckBox;
    Series3: TLineSeries;
    Label6: TLabel;
    ComboBox4: TComboBox;
    Splitter3: TSplitter;
    ComboBox5: TComboBox;
    CheckBox3: TCheckBox;
    ComboBox6: TComboBox;
    Chart5: TChart;
    Series4: TLineSeries;
    Series5: TPointSeries;
    Series6: TPointSeries;
    Panel15: TPanel;
    PBCheck: TCheckListBox;
    Label3: TLabel;
    Panel14: TPanel;
    CheckBox6: TCheckBox;
    CheckBox12: TCheckBox;
    CheckBox13: TCheckBox;
    Memo1: TMemo;
    Chart6: TChart;
    LineSeries3: TLineSeries;
    LineSeries4: TLineSeries;
    CheckBox16: TCheckBox;
    CheckBox17: TCheckBox;
    SpinEdit1: TSpinEdit;
    Button4: TButton;
    Panel16: TPanel;
    Label7: TLabel;
    TrackBar1: TTrackBar;
    CheckBox18: TCheckBox;
    Label8: TLabel;
    TrackBar2: TTrackBar;
    CheckBox19: TCheckBox;
    Label9: TLabel;
    TrackBar3: TTrackBar;
    CheckBox20: TCheckBox;
    Label10: TLabel;
    TrackBar4: TTrackBar;
    CheckBox21: TCheckBox;
    ScrollBar3: TScrollBar;
    Label11: TLabel;
    Label12: TLabel;
    Chart7: TChart;
    LineSeries5: TLineSeries;
    Chart8: TChart;
    LineSeries6: TLineSeries;
    Panel17: TPanel;
    ComboBox11: TComboBox;
    ComboBox12: TComboBox;
    Button6: TButton;
    Chart9: TChart;
    Series7: TLineSeries;
    Label21: TLabel;
    Series8: TLineSeries;
    Series9: TLineSeries;
    CheckBox9: TCheckBox;
    PointCB1: TRadioButton;
    PointCB2: TRadioButton;
    RadioButton1: TRadioButton;
    Label24: TLabel;
    CheckBox29: TCheckBox;
    CheckBox23: TCheckBox;
    PointWZ: TCheckBox;
    BHCheck: TCheckListBox;
    PBInfo: TListBox;
    Label4: TLabel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    PageControl2: TPageControl;
    Button2: TButton;
    ComboBox7: TComboBox;
    ComboBox8: TComboBox;
    CheckBox24: TCheckBox;
    CheckBox4: TCheckBox;
    ComboBox9: TComboBox;
    ComboBox10: TComboBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    ComboBox13: TComboBox;
    CheckBox8: TCheckBox;
    Panel18: TPanel;
    Memo2: TMemo;
    CheckBox22: TCheckBox;
    CheckBox14: TCheckBox;
    Panel19: TPanel;
    Label13: TLabel;
    ListBox2: TListBox;
    CheckBox10: TCheckBox;
    CheckBox30: TCheckBox;
    ConstCheck: TCheckListBox;
    CheckBox31: TCheckBox;
    HoleList: TListBox;
    Chart11: TChart;
    Panel20: TPanel;
    Button3: TButton;
    Button5: TButton;
    Series10: TLineSeries;

    Debug: TTabSheet;
    TabSheet14: TTabSheet;

    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    TabSheet15: TTabSheet;
    Panel21: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Bevel1: TBevel;
    CheckBox26: TCheckBox;
    CheckBox27: TCheckBox;
    CheckBox28: TCheckBox;
    Chart12: TChart;
    LineSeries7: TLineSeries;
    CheckBox32: TCheckBox;
    cbEnd: TCheckBox;
    CheckBox34: TCheckBox;
    CheckBox15: TCheckBox;
    CheckBox36: TCheckBox;
    Label14: TLabel;
    CheckBox35: TCheckBox;
    Bevel4: TBevel;
    Button7: TButton;
    CheckBox37: TCheckBox;
    Label1: TLabel;
    ShowBJMask: TCheckBox;
    Panel22: TPanel;
    Chart10: TChart;
    Chart13: TChart;
    Chart14: TChart;
    CheckBox39: TCheckBox;
    CheckBox40: TCheckBox;
    Button8: TButton;
    Button9: TButton;
    CheckBox41: TCheckBox;
    Panel23: TPanel;
    TabSheet18: TTabSheet;
    Panel24: TPanel;
    CheckBox42: TCheckBox;
    CheckBox43: TCheckBox;
    Panel25: TPanel;
    CheckListBox1: TCheckListBox;
    Panel26: TPanel;
    CheckBox44: TCheckBox;
    ComboBox17: TComboBox;
    CheckBox47: TCheckBox;
    ComboBox18: TComboBox;
    TabSheet19: TTabSheet;
    Panel27: TPanel;
    BJSign: TCheckBox;
    CheckBox48: TCheckBox;
    CheckBox49: TCheckBox;
    CheckBox50: TCheckBox;
    CheckBox51: TCheckBox;
    ScrollBar4: TScrollBar;
    ShowBJ: TCheckBox;
    BJSignIdx: TCheckBox;
    ShowBJIdx: TCheckBox;
    Panel28: TPanel;
    eTestIdx1: TEdit;
    eTestIdx2: TEdit;
    Label18: TLabel;
    Button11: TButton;
    cbTestRail: TComboBox;
    ListBox4: TListBox;
    Button12: TButton;
    Label19: TLabel;
    cbTestMode: TComboBox;
    cbTestCh: TComboBox;
    cbBSHole: TCheckBox;
    cbBoltHoles: TCheckBox;
    Memo: TTabSheet;
    CheckBox45: TCheckBox;
    CheckBox46: TCheckBox;
    CheckBox52: TCheckBox;
    CheckBox53: TCheckBox;
    cbCursor: TCheckBox;
    cbEchoSize: TComboBox;
    cbViewMode: TComboBox;
    CheckBox33: TCheckBox;
    CheckBox38: TCheckBox;
    CheckBox25: TCheckBox;
    CheckBox54: TCheckBox;
    CheckBox55: TCheckBox;

    procedure ZoomBarChange(Sender: TObject);
    procedure ScrollBoxResize(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure PaintBoxDblClick(Sender: TObject);
    procedure PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure FormResize(Sender: TObject);
    procedure ViewThChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BSRightChartBeforeDrawSeries(Sender: TObject);
    procedure ViewStripClickCheck(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure BHCheckClickCheck(Sender: TObject);
    procedure BMGraphListClick(Sender: TObject);
    procedure Chart1BeforeDrawSeries(Sender: TObject);
    procedure BMListItemsDblClick(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure ComboBox2Change(Sender: TObject);
    procedure MakeKFGraph(R: RRail; Ch, Idx: Integer);
    procedure CheckBox7Click(Sender: TObject);
    procedure BMListItemsClickCheck(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ScrollBar2Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MyRefresh(Sender: TObject);
    procedure MyRefresh2(Sender: TObject);
    procedure PBTHChange(Sender: TObject);
    procedure ScrollBar3Change(Sender: TObject);
    procedure PaintBoxClick(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure ComboBox13Change(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure HoleListClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Panel22Resize(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure ScrollBar4Change(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure ListBox4DblClick(Sender: TObject);
    procedure ListBox5DblClick(Sender: TObject);
    procedure cbViewModeChange(Sender: TObject);
  { $ ENDIF}
//    procedure Button10Click(Sender: TObject);
//    procedure GisChange(Sender: TObject);
  private
  {$IFDEF RECDEBUG}
    FRec: Integer;
    FXZoom: Single;
    FOutInfo: TOutInfo;
    FOutLink: array [r_Left..r_Right, 0..7] of TOutLink;
    FDbgLink: array [r_Left..r_Right, 0..7] of TOutLink;
    FDatSrc: TAvk11DatSrc;

    FCoordMode: Integer;
    FStDrawCrd: Integer;
    FEdDrawCrd: Integer;

    FLastRail: RRail;
    FLastCh: Integer;
    FLastIdx: Integer;

    FMaxShift: Integer;
    FScanStep: Single;
    FSaveMouseX: Integer;

    FRecData: Pointer;
    FSeriesRefresh: TNotifyEvent;

    Min_: Integer;
    Max_: Integer;
    FMouseX: Integer;
    FMouseY: Integer;
    FStartDisCrd: Integer;

    FStCrd: Integer;
    FEdCrd: Integer;

    FDebugRect: Boolean;
    FResRect: Boolean;
    FResRectHeight: Integer;
    FResRt: array [r_Left..r_Right] of TRect;
    FXorLine: TIntegerDynArray;
    FOnScrPB: array [r_Left..r_Right, 0..7] of array of TOnScrPBItem;
    FSavePoint: TIntegerDynArray;
    FSaveYPoint: Integer;
    FPBIdx: array [r_Left..r_Right, 1..7] of Integer;

    FSrcCrd: Integer;
    FEchoSizeIdx: Integer;
//    FFiltr: TFiltr;
//    FFiltrIdx: Integer;         // ������� ������ ������ ����������
//    Filtr: array of TFiltrData;

    procedure CreateImage(Sender: TObject);
    function GetScrPos(R: RRail; Ch, Crd, Delay: Integer; BM: Boolean; var Pos: TPoint; var OutOfRange: Boolean): Boolean;
    function GetScrPosNoRed(R: RRail; Ch, Crd, Delay: Integer; BM: Boolean; var Pos: TPoint; var OutOfRange: Boolean): Boolean;
    function GetScrPos2(R: RRail; Ch, Crd: Integer; var X, Y1, Y2: Integer; NoRed: Boolean = False): Boolean;
    function GetScrPos3(R: RRail; Crd1, Crd2, StStrip, EdStrip: Integer; var Rt: TRect): Boolean;
    function GetDebugPos(R: RRail; Ch, Crd, Delay: Integer; BM: Boolean; var Pos: TPoint): Boolean;
    function TestScrPos(Crd: Integer; var X: Integer): Integer;
    function GetDisCrd(ScrX: Integer): Integer;
    function GetScrCrd(DisCrd: Integer): Integer;
    function BlockOK_BMFon(StartDisCoord: Integer): Boolean;
    function BlockOK_Echo(StartDisCoord: Integer): Boolean;
    function BlockOK_Echo2(StartDisCoord: Integer): Boolean;
    procedure CreateOutInfo;
    procedure CMChildKey(var Message: TCMChildKey); message CM_CHILDKEY;
    function BlkOk(StartDisCoord: Integer): Boolean;
    procedure CheckBox33Click(Sender: TObject);
    procedure ByHeight(Sender: TObject);
  {$ENDIF}
  public
  {$IFDEF RECDEBUG}
    procedure SetData(DatSrc: TAvk11DatSrc; RecBlock: Pointer; CenterDisCoord: Integer);
  {$ENDIF}
  end;

implementation

{$R *.dfm}

uses
  RecConfigUnit, RecData;

const
  GE: Integer = 0;

{$IFDEF RECDEBUG}
function RecData(Form: TRecDataForm): TRecData;
begin
  Result:= TRecData(Form.FRecData);
end;
{$ENDIF}

// --------[ TRecBlockForm ]----------------------------------------------------

procedure TRecDataForm.ZoomBarChange(Sender: TObject);
begin
  {$IFDEF RECDEBUG}
  if ZoomBar.Position = ZoomBar.Tag then Exit;
  ZoomBar.Tag:= ZoomBar.Position;
  FXZoom:= Exp((ZoomBar.Position / 10) + 1) / 20;
  CreateImage(nil);
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;
  {$ENDIF}
end;

procedure TRecDataForm.ScrollBoxResize(Sender: TObject);
begin
  {$IFDEF RECDEBUG}
  PaintBox.Left:= 0;
  PaintBox.Top:= - ScrollBox.VertScrollBar.Position;
  PaintBox.Width:= ScrollBox.ClientWidth;
  if X1.Down then PaintBox.Height:= ScrollBox.ClientHeight;
  if X2.Down then PaintBox.Height:= ScrollBox.ClientHeight * 2;
  if X3.Down then PaintBox.Height:= ScrollBox.ClientHeight * 3;
  if X4.Down then PaintBox.Height:= ScrollBox.ClientHeight * 4;
  if X5.Down then PaintBox.Height:= ScrollBox.ClientHeight * 5;
  if X6.Down then PaintBox.Height:= ScrollBox.ClientHeight * 6;
  Self.Resize;
  {$ENDIF}
end;

procedure TRecDataForm.CheckBox2Click(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  CreateOutInfo;
  CreateImage(Sender);
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;
{$ENDIF}
end;

procedure TRecDataForm.PaintBoxDblClick(Sender: TObject);
{$IFDEF RECDEBUG}
var
  S: Integer;
  E: Integer;
{$ENDIF}

begin
{$IFDEF RECDEBUG}
  ScrollBar.Position:= ScrollBar.Position + Round((FSaveMouseX - PaintBox.Width / 2) * FXZoom);
{$ENDIF}
end;

procedure TRecDataForm.ScrollBar3Change(Sender: TObject);
begin
  {$IFDEF RECDEBUG}
  MakeKFGraph(FLastRail, FLastCh, FLastIdx);
  {$ENDIF}
end;

procedure TRecDataForm.PaintBoxClick(Sender: TObject);
begin
  {$IFDEF RECDEBUG}
  FStartDisCrd:= GetDisCrd(FMouseX);
  {$ENDIF}
end;

procedure TRecDataForm.PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
{$IFDEF RECDEBUG}
label L1;
var
  Ch_: Integer;
  Idx: Integer;
  Ch: Integer;
  I: Integer;
  J: Integer;
  R: RRail;
  R_: RRail;
  RGN: HRGN;
  Flg: Boolean;
  BM: Boolean;
  Idx1: Integer;
  Idx2: Integer;
  S: string;
  PBTyp: TPBTypItems;
{$ENDIF}

begin
{$IFDEF RECDEBUG}
  FSrcCrd:= GetDisCrd(X);
  Chart12.Refresh;
//  Chart1BeforeDrawSeries(nil);

//  Label1.Caption:= Format('SysCoord = %d', [FDatSrc.DisToSysCoord(FSrcCrd)]);
  Label12.Caption:= Format('DisCoord = %d / %d', [FSrcCrd, FDatSrc.DisToSysCoord(FSrcCrd)]);
  RecData(Self).GetCoordState(FSrcCrd, Idx1, Idx2, BM);
  Label23.Caption:= '������ ����: ' + IntToStr(Idx1) + ' : ' + IntToStr(Idx2);

  if BM then Label22.Caption:= '���������: Backward'
        else Label22.Caption:= '���������: Forward';
  Label11.Caption:= Format('����� = %3.1f ��', [{(}Abs(FStartDisCrd - FSrcCrd) {+ 1)} * FScanStep]);


  FMouseX:= X;
  FMouseY:= Y;

  Flg:= False;
  for R:= r_Left to r_Right do
    for Ch_:= 0 to 7 do
      for I:= 0 to High(FOnScrPB[R, Ch_]) do
        if Length(FOnScrPB[R, Ch_, I].PtList) <> 0 then
        begin
          RGN:= CreatePolygonRgn(FOnScrPB[R, Ch_, I].PtList[0], Length(FOnScrPB[R, Ch_, I].PtList), Alternate);
          if PtInRegion(RGN, X, Y) then
          begin
            Flg:= True;
            Idx:= FOnScrPB[R, Ch_, I].PBIdx;
            Ch:= Ch_;
            R_:= R;
          end;
          DeleteObject(RGN);
          if Flg then goto L1;
        end;
L1:
  if Flg then
  begin
    FLastRail:= R_;
    FLastCh:= Ch;
    FLastIdx:= Idx;
//    MakeKFGraph(R_, Ch, Idx);

    PBInfo.Items.Clear;
    PBInfo.Items.Add(Format('Channel: %d', [Ch]));
    PBInfo.Items.Add(Format('Index: %d', [Idx]));
    PBInfo.Items.Add(Format('Len: %d pt', [RecData(Self).FPBList[R_, Ch, Idx].EchoCount]));
    if Ch = 0 then PBInfo.Items.Add(Format('Len: %3.1f mm', [(GetLen(RecData(Self).FPBList[R_, Ch, Idx].Crd) - 1) * FScanStep]))
              else PBInfo.Items.Add(Format('Len: %3.1f mm', [GetLen(RecData(Self).FPBList[R_, Ch, Idx].Crd) * FScanStep]));

//    PBInfo.Items.Add(Format('Delay Coeff: %1.2f', [RecData(Self).FPBList[R_, Ch, Idx].KfDelay]));
//    PBInfo.Items.Add(Format('Ampl Coeff: %1.2f', [RecData(Self).FPBList[R_, Ch, Idx].KfAmpl]));
//    PBInfo.Items.Add(Format('Fill Coeff: %1.2f', [RecData(Self).FPBList[R_, Ch, Idx].KfFill]));
    PBInfo.Items.Add(Format('Angle: %d', [RecData(Self). GetPBAngleKF(RecData(Self).FPBList[R_, Ch, Idx])]));

    PBInfo.Items.Add(Format('Echo Count: %d', [RecData(Self).FPBList[R_, Ch, Idx].EchoCount]));
//    PBInfo.Items.Add(Format('Summ Ampl: %d', [RecData(Self).FPBList[R_, Ch, Idx].SummAmpl_]));

    if Ch = 1 then
    begin
      PBInfo.Items.Add(Format('Ampl 3: %d', [RecData(Self).FPBList[R_, Ch, Idx].Dat[3]]));
      PBInfo.Items.Add(Format('Ampl 4: %d', [RecData(Self).FPBList[R_, Ch, Idx].Dat[4]]));
      PBInfo.Items.Add(Format('Ampl 5: %d', [RecData(Self).FPBList[R_, Ch, Idx].Dat[5]]));
      PBInfo.Items.Add(Format('Ampl 6: %d', [RecData(Self).FPBList[R_, Ch, Idx].Dat[6]]));
      PBInfo.Items.Add(Format('Ampl 7: %d', [RecData(Self).FPBList[R_, Ch, Idx].Dat[7]]));
    end;

//    PBInfo.Items.Add(Format('E: %3.1f', [RecData(Self).FPBList[R_, Ch, Idx].E]));
{    if (Ch = 1) and (RecData(Self).FPBList[R_, Ch, Idx].Count <> - 1) then
    begin
      PBInfo.Items.Add(Format('Ampl Th: %d', [RecData(Self).FPBList[R_, Ch, Idx].Th]));
      PBInfo.Items.Add(Format('Echo Out: %d', [RecData(Self).FPBList[R_, Ch, Idx].KOut]));
      PBInfo.Items.Add(Format('Echo In: %d', [RecData(Self).FPBList[R_, Ch, Idx].KIn]));
      PBInfo.Items.Add(Format('Echo Count: %d', [RecData(Self).FPBList[R_, Ch, Idx].Count]));
    end;
}
    ListBox2.Clear;
    for PBTyp:= Low(TPBTypItems) to High(TPBTypItems) do
      if PBTyp in RecData(Self).FPBList[R_, Ch, Idx].Typ then
        case PBTyp of
            pbtiInBMZ: ListBox2.Items.Add('� ���� ��');
        pbtiConstruct: ListBox2.Items.Add('����� ��');
            pbtiClear: ListBox2.Items.Add('����� ������� ������');
          pbtiNotLast: ListBox2.Items.Add('�� ��������� ��������');
          pbtiReCheck: ListBox2.Items.Add('��������-�� ����-� ��');
              pbtiUCS: ListBox2.Items.Add('����� �����');
           pbtiDefect: ListBox2.Items.Add('����� �������');
              pbtiDel: ListBox2.Items.Add('����� ��������');
        end;


(*    for I:= 0 to High(RecData(Self).FPBList[R_, Ch, Idx].TypList) do
    begin
      case RecData(Self).FPBList[R_, Ch, Idx].TypList[I].Typ of
       pbtNotSet: S:= '�� ������';
        pbtClear: S:= '������ �����';
    pbtConstruct: S:= '��';
          pbtUCS: S:= '�����';

{      pbtForHole: S:= '��� ������ ���������';
  pbtForHoleBolt: S:= '��� ������ ��������� �� ���������� �����';
   pbtForHoleDef: S:= '��� ������ ��������� �� �������';
     pbtHoleMain: S:= '�� ���������';
     pbtHoleBolt: S:= '�� ��������� - ��������� ����';
      pbtHoleDef: S:= '�� ��������� - ������';
          pbtEnd: S:= '�����';
          pbtUCS: S:= '���������������������';
      pbtUnknown: S:= '����� � ���'; }
//        pbtNoise: S:= '���';
       pbtForDef: S:= '��� �������';
//          pbtDel: S:= '���������';
// pbtForHole_Bolt: S:= '��� ��������� �� �����';
//  pbtForHole_Def: S:= '��� ��������� 53';
      end;

      J:= RecData(Self).FPBList[R_, Ch, Idx].TypList[I].AlgNo;
      if (J >= Low(AlgNameList)) and (J <= High(AlgNameList)) then ListBox2.Items.Add(Format('%s; ' + S, [AlgNameList[J]]))
                                                              else ListBox2.Items.Add(Format('Alg: %d; ' + S, [J]));
    end; *)
  end;

  if cbCursor.Checked then
  begin
    PaintBox.Canvas.Pen.Color:= clWhite;
    PaintBox.Canvas.Pen.Width:= 1;
    PaintBox.Canvas.Pen.Style:= psDot;
    PaintBox.Canvas.Pen.Mode:= pmXor;
    for J:= 0 to High(FSavePoint) do
    begin
      PaintBox.Canvas.MoveTo(FSavePoint[J], 0);
      PaintBox.Canvas.LineTo(FSavePoint[J], PaintBox.Height);
    end;

    if CheckBox26.Checked then
    begin
      SetLength(FSavePoint, 0);
      FDatSrc.DisToDisCoords(FSrcCrd, FSavePoint);
      for J:= 0 to High(FSavePoint) do
      begin
        FSavePoint[J]:= GetScrCrd(FSavePoint[J]);
        PaintBox.Canvas.MoveTo(FSavePoint[J], 0);
        PaintBox.Canvas.LineTo(FSavePoint[J], PaintBox.Height);
      end
    end
    else
    begin
      SetLength(FSavePoint, 1);
      FSavePoint[0]:= GetScrCrd(FSrcCrd);
      PaintBox.Canvas.MoveTo(FSavePoint[0], 0);
      PaintBox.Canvas.LineTo(FSavePoint[0], PaintBox.Height);
    end;

    if FSaveYPoint <> - 1 then
    begin
      PaintBox.Canvas.MoveTo(0, FSaveYPoint);
      PaintBox.Canvas.LineTo(Width, FSaveYPoint);
    end;
    FSaveYPoint:= Y;
    PaintBox.Canvas.MoveTo(0, FSaveYPoint);
    PaintBox.Canvas.LineTo(Width, FSaveYPoint);

    PaintBox.Canvas.Pen.Mode:= pmCopy;
  end;

  for I:= 0 to High(FOutInfo) do
    if PtInRect(FOutInfo[I].Sgl_Rt, Point(X, Y)) then
    begin
      with FOutInfo[I], Sgl_Rt, ChList[Ch] do
        Label13.Caption:= Format('Delay = %3.1f', [DelayMin + ((Bottom - Top) - (Y - Top)) / (Bottom - Top) * (DelayMax - DelayMin)]);
      Break;
    end;

// --------------------------------------------------------------------------------
{
  for I:= 0 to High(FOutInfo) do
    if PtInRect(FOutInfo[I].Sgl_Rt, Point(X, Y)) then
    begin
      Label1.Caption:= Format('Bottom Delay = %d', [RecData(Self).GetBtmDelay(FOutInfo[I].Rail, FSrcCrd)]);
      Break;
    end;
}
{$ENDIF}
end;

procedure TRecDataForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
{$IFDEF RECDEBUG}
var
  W: Integer;
{$ENDIF}

begin
{$IFDEF RECDEBUG}
  W:= Round(Width * FXZoom / 2);

  case Msg.CharCode of
     VK_LEFT: ScrollBar.Position:= ScrollBar.Position - W div (4 + 36 * Ord((GetKeyState(VK_SHIFT) shr 8) <> 0));
    VK_RIGHT: ScrollBar.Position:= ScrollBar.Position + W div (4 + 36 * Ord((GetKeyState(VK_SHIFT) shr 8) <> 0));
     VK_DOWN: ZoomBar.Position:= ZoomBar.Position - 1;
       VK_UP: ZoomBar.Position:= ZoomBar.Position + 1;
  end;
{$ENDIF}
end;

procedure TRecDataForm.FormResize(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  CreateOutInfo;
//  BSChart1.Width:= TabSheet7.Width div 2;
{$ENDIF}
end;

procedure TRecDataForm.ViewThChange(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  if ViewTh.Position = ViewTh.Tag then Exit;
  ViewTh.Tag:= ViewTh.Position;
  Label21.Caption:= IntToStr(ViewTh.Position);
  CreateImage(nil);
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;
{$ENDIF}
end;

procedure TRecDataForm.PageControl1Change(Sender: TObject);
begin
{$IFDEF RECDEBUG}

  if PageControl1.ActivePage = TabSheet15 then PageControl1.Height:= 400 else
//  if PageControl1.ActivePage = TabSheet16 then PageControl1.Height:= 400 else
  if PageControl1.ActivePageIndex = 0 then PageControl1.Height:= 22
                                      else PageControl1.Height:= 200;

{$ENDIF}
end;

procedure TRecDataForm.BSRightChartBeforeDrawSeries(Sender: TObject);
begin
{  with TRecData(FRecData) do
  begin
    ChartXZone(BSRightChart, FBSPos[r_Right].Min, FBSPos[r_Right].Max, $0080FFFF);
    ChartXZone(BSRightChart, FBladePos[r_Right].Min, FBladePos[r_Right].Max, $00808FFF);
  end; }
end;

procedure TRecDataForm.ViewStripClickCheck(Sender: TObject);
{$IFDEF RECDEBUG}
var
  I: Integer;
  F: Boolean;
{$ENDIF}

begin
{$IFDEF RECDEBUG}
  for I:= 0 to 3 do
    F:= F or ViewStrip.Checked[I];
  if not F then ViewStrip.Checked[0]:= True;
  CreateOutInfo;
  CreateImage(nil);
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;
{$ENDIF}
end;

procedure TRecDataForm.SpeedButton1Click(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  FRec:= 0;
  CreateImage(nil);
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;
{$ENDIF}
end;

procedure TRecDataForm.SpeedButton2Click(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  FRec:= 1;
  CreateImage(nil);
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;
{$ENDIF}
end;

procedure TRecDataForm.SpeedButton3Click(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  FRec:= 2;
  CreateImage(nil);
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;
{$ENDIF}
end;

procedure TRecDataForm.BHCheckClickCheck(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  FResRect:= BJSign.Checked;
  CreateOutInfo;
  CreateImage(nil);
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;
{$ENDIF}
end;

procedure TRecDataForm.BMGraphListClick(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  I: Integer;
  J: Integer;
  Y: Integer;
{$ENDIF}

begin
  (*
  if BMGraphList.ItemIndex <> - 1 then
  begin
    FSeriesRefresh:= BMGraphListClick;

    Series1.Clear;

    with RecData(Self).FBMGraph[BMGraphList.ItemIndex].Range do
      ScrollBar.Position:= (StCrd + EdCrd) div 2;

    ComboBox6.Clear;
    BMListItems.Clear;
    for I:= 0 to High(RecData(Self).FBMGraph[BMGraphList.ItemIndex].Items) do
    begin
      BMListItems.Checked[BMListItems.Items.Add(Format('%d: [%d - %d]', [I, RecData(Self).FBMGraph[BMGraphList.ItemIndex].Items[I].Range.StCrd, RecData(Self).FBMGraph[BMGraphList.ItemIndex].Items[I].Range.EdCrd]))]:= RecData(Self).FBMGraph[BMGraphList.ItemIndex].Items[I].Hide;
      ComboBox6.Items.Add(IntToStr(I));
    end;

    case ComboBox1.ItemIndex of
      0: R:= r_Left;
      1: R:= r_Right;
    end;

    with RecData(Self).FBMGraph[BMGraphList.ItemIndex].Range do
      ScrollBar.Position:= (StCrd + EdCrd) div 2;
    CreateImage(nil);
  end;
  *)
end;

procedure TRecDataForm.Chart1BeforeDrawSeries(Sender: TObject);
{$IFDEF RECDEBUG}
var
  I: Integer;
{$ENDIF}

begin
{$IFDEF RECDEBUG}
  for I:= 0 to High(RecData(Self).FBMList.FBMData) do
    begin
      ChartXZone(Chart12, RecData(Self).FBMList.FBMData[I].DisRange.StCrd, RecData(Self).FBMList.FBMData[I].DisRange.StCrd, 2, clGreen);
      ChartXZone(Chart12, RecData(Self).FBMList.FBMData[I].DisRange.EdCrd, RecData(Self).FBMList.FBMData[I].DisRange.EdCrd, 2, clGreen);
    end;

  ChartXZone(Chart12, FSrcCrd, FSrcCrd, 4, clRed);


{
  for I:= 0 to Series1.Count - 1 do
    ChartText(Chart1, Series1.XValue[I], Series1.YValues[I], IntToStr(I));
}
 (*
  if BMGraphList.ItemIndex <> - 1 then
    with RecData(Self).FBMGraph[BMGraphList.ItemIndex] do
      for I:= 0 to High(Items) do
        ChartXZone(Chart1, Items[I].Range.StCrd - Items[0].Range.StCrd, Items[I].Range.StCrd - Items[0].Range.StCrd + 2, clGreen);
  *)
{$ENDIF}
end;

procedure TRecDataForm.BMListItemsDblClick(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  I: Integer;
{$ENDIF}

begin
 (*
  if (BMGraphList.ItemIndex <> - 1) and
     (BMListItems.ItemIndex <> - 1) then
  begin
    with RecData(Self).FBMGraph[BMGraphList.ItemIndex].Items[BMListItems.ItemIndex].Range do
      ScrollBar.Position:= (StCrd + EdCrd) div 2;
    CreateImage(nil);
  end;
  *)
end;

procedure TRecDataForm.BMListItemsClickCheck(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  I: Integer;
{$ENDIF}

begin
{ if (BMGraphList.ItemIndex <> - 1) and
     (BMListItems.ItemIndex <> - 1) then
  begin
    FSeriesRefresh:= BMListItemsDblClick;

    Series1.Clear;

    case ComboBox1.ItemIndex of
      0: R:= r_Left;
      1: R:= r_Right;
    end;

    with RecData(Self).FBMGraph[BMGraphList.ItemIndex].Items[BMListItems.ItemIndex] do
      for I:= 0 to High(Delta[R]) do
        Series1.AddXY(I, Delta[R, I]);

    RecData(Self).FBMGraph[BMGraphList.ItemIndex].Items[BMListItems.ItemIndex].Hide:= BMListItems.Checked[BMListItems.ItemIndex];
    CreateImage(nil);
  end; }
end;

procedure TRecDataForm.ComboBox1Change(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  if Assigned(FSeriesRefresh) then FSeriesRefresh(Sender);
{$ENDIF}
end;

procedure TRecDataForm.CheckBox5Click(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  Chart2.LeftAxis.Visible:= CheckBox5.Checked;
{$ENDIF}
end;

procedure TRecDataForm.StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
(*
{$IFDEF RECDEBUG}
var
  R: RRail;
  I: Integer;
{$ENDIF}
*)
begin
(*
{$IFDEF RECDEBUG}
  I:= ACol div 2;
  if I > Length(RecData(Self).FSingleObj[r_Left]) then
  begin
    R:= r_Right;
    I:= I - Length(RecData(Self).FSingleObj[r_Left]);
  end
  else R:= r_Left;

//  ScrollBar.Position:= (RecData(Self).FSingleObj[R, I].StCrd + RecData(Self).FSingleObj[R, I].EdCrd) div 2;
  ScrollBar.Position:= GetCentr(RecData(Self).FSingleObj[R, I].Range);
{$ENDIF}
*)
end;

procedure TRecDataForm.ComboBox2Change(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  I: Integer;
  Ch: Integer;
{$ENDIF}

begin
{$IFDEF RECDEBUG}
  case ComboBox2.ItemIndex of
    0: R:= r_Left;
    1: R:= r_Right;
  end;

  case ComboBox5.ItemIndex of
    0: Ch:= 6;
    1: Ch:= 7;
    2: Ch:= 1;
    3: Ch:= 0;
  end;

  if Ch in [1, 6, 7] then ScrollBar1.SetParams(0, 0, High(RecData(Self).FPBList[R, Ch]))
                     else ScrollBar1.SetParams(0, 0, High(RecData(Self).FPBList[R, 0]));
{$ENDIF}
end;

procedure TRecDataForm.MakeKFGraph(R: RRail; Ch, Idx: Integer);
{$IFDEF RECDEBUG}
var
  I, J, K, DL: Integer;
  KfD, KfA: Single;
  List: TMaskItemList;

//  A1, A2: array of FFTFloat;

{$ENDIF}

begin
  {$IFDEF RECDEBUG}    {
  case ComboBox5.ItemIndex of
    0: Ch:= 6;
    1: Ch:= 7;
    2: Ch:= 1;
    3: Ch:= 0;
  end;

  Idx:= ScrollBar1.Position;
  Label5.Caption:= 'Idx: ' + IntToStr(Idx);
  case ComboBox2.ItemIndex of
    0: R:= r_Left;
    1: R:= r_Right;
  end;
             }
  if Ch = 0 then
  begin
   (*
    DL:= RecData(Self).FBSHole[R, Idx].AmplR1.EdCrd - RecData(Self).FBSHole[R, Idx].AmplR1.StCrd;
    for Typ:= iSrc to iComp do
      for Sgn:= iDelay to iAmpl do SetLength(Graph[Sgn, Typ], DL + 1);

    for I:= 0 to DL do
    begin
      Graph[iAmpl,  iSrc, I]:= RecData(Self).FBSAmpl[R, RecData(Self).FBSHole[R, Idx].AmplR1.StCrd + I];
      Graph[iDelay, iSrc, I]:= RecData(Self).FBSAmpl[R, RecData(Self).FBSHole[R, Idx].AmplR1.StCrd + I];
    end;

    for Sgn:= iDelay to iAmpl do
    begin
      Min_[Sgn]:= MaxInt;
      Max_[Sgn]:= - MaxInt;
      for I:= 0 to DL do
      begin
        if Min_[Sgn] > Graph[Sgn, iSrc, I] then Min_[Sgn]:= Graph[Sgn, iSrc, I];
        if Max_[Sgn] < Graph[Sgn, iSrc, I] then Max_[Sgn]:= Graph[Sgn, iSrc, I];
      end;
    end;

    for Sgn:= iDelay to iAmpl do
      if Max_[Sgn] - Min_[Sgn] <> 0 then
        for I:= 0 to DL do
          Graph[Sgn, iSrc, I]:= 2 * (Graph[Sgn, iSrc, I] - Min_[Sgn]) / (Max_[Sgn] - Min_[Sgn]) - 1;

    for I:= 0 to DL do Graph[iAmpl, iComp, I]:= 1 - 2 * Exp( - Power((I - DL) / 3, 2));
    for I:= 0 to DL do Graph[iDelay, iComp, I]:= 2 * Ord(I <> DL) - 1;


    SrcAmplSeries.Clear;
    DstAmplSeries.Clear;

    SrcDelaySeries.Clear;
    DstDelaySeries.Clear;

    for I:= 0 to DL do
    begin
      SrcDelaySeries.AddXY(I, Graph[iDelay, iSrc, I]);
      DstDelaySeries.AddXY(I, Graph[iDelay, iComp, I]);
      SrcAmplSeries.AddXY(I, Graph[iAmpl, iSrc, I]);
      DstAmplSeries.AddXY(I, Graph[iAmpl, iComp, I]);
    end;

    for Typ:= iSrc to iComp do
      for Sgn:= iDelay to iAmpl do
      begin
        Cen[Sgn, Typ]:= 0;
        for K:= 0 to DL do
          Cen[Sgn, Typ]:= Cen[Sgn, Typ] + Graph[Sgn, Typ, K];
        Cen[Sgn, Typ]:= Cen[Sgn, Typ] / (DL + 1);
      end;

    for Sgn:= iDelay to iAmpl do
    begin
      S1:= 0;
      S2:= 0;
      S3:= 0;
      for K:= 0 to DL do
      begin
        S1:= S1 + (Graph[Sgn, iSrc, K] - Cen[Sgn, iSrc]) * (Graph[Sgn, iComp, K] - Cen[Sgn, iComp]);
        S2:= S2 + Sqr(Graph[Sgn, iSrc, K] - Cen[Sgn, iSrc]);
        S3:= S3 + Sqr(Graph[Sgn, iComp, K] - Cen[Sgn, iComp]);
      end;
      if (S2 <> 0) and (S3 <> 0) then
        case Sgn of
          iDelay: DelayChart.Title.Text.Text:= Format('%3.3f', [S1 / (Sqrt(S2) * Sqrt(S3))]);
           iAmpl: AmplChart.Title.Text.Text:=  Format('%3.3f', [S1 / (Sqrt(S2) * Sqrt(S3))]);
        end
        else
        case Sgn of
          iDelay: DelayChart.Title.Text.Text:= '?';
           iAmpl: AmplChart.Title.Text.Text:=  '?';
        end;
    end;

    for Typ:= iSrc to iComp do
      for Sgn:= iDelay to iAmpl do SetLength(Graph[Sgn, Typ], 0);

    ScrollBar.Position:= (RecData(Self).FBSHole[R, Idx].StCrd + RecData(Self).FBSHole[R, Idx].EdCrd) div 2;
  *)
  end
  else
  begin
    SetLength(List, 0);
    for I:= 0 to High(RecData(Self).FPBList[R, Ch, Idx].Items) do
    begin
      if ScrollBar3.Position <= RecData(Self).FPBList[R, Ch, Idx].Items[I].Ampl then
      begin
        SetLength(List, Length(List) + 1);
        List[High(List)].Coord:= RecData(Self).FPBList[R, Ch, Idx].Items[I].Crd;
//        List[High(List)].Delay:= (RecData(Self).FPBList[R, Ch, Idx].Items[I].StDelay + RecData(Self).FPBList[R, Ch, Idx].Items[I].EdDelay) div 2;
        List[High(List)].Delay:= RecData(Self).FPBList[R, Ch, Idx].Items[I].Delay;
        List[High(List)].Ampl:= RecData(Self).FPBList[R, Ch, Idx].Items[I].Ampl;
      end;
    end;

    RecData(Self).CalcCoeff(Ch, False, List, KfD, KfA);

    SrcAmplSeries.Clear;
    DstAmplSeries.Clear;
    SrcDelaySeries.Clear;
    DstDelaySeries.Clear;

    for I:= 0 to High(RecData(Self).FDebugGraph[0, 0]) do
    begin
      SrcDelaySeries.AddXY(I, RecData(Self).FDebugGraph[0, 0, I]);
      DstDelaySeries.AddXY(I, RecData(Self).FDebugGraph[0, 1, I]);
      SrcAmplSeries.AddXY(I, RecData(Self).FDebugGraph[1, 0, I]);
      DstAmplSeries.AddXY(I, RecData(Self).FDebugGraph[1, 1, I]);
    end;

    DelayChart.Title.Text.Text:= Format('%3.3f', [KfD]);
    AmplChart.Title.Text.Text:=  Format('%3.3f', [KfA]);
(*
// ---- BPF --------------------------------------------------------------------

    SetLength(A1, 64);
    SetLength(A2, 64);

    LineSeries5.Clear;
    LineSeries6.Clear;

    J:= High(RecData(Self).FDebugGraph[0, 0]);

    for I:= 0 to 63 do
    begin

      K:= Trunc(J * I / 63);
      if Frac(J * I / 63) <> 0 then A1[I]:= RecData(Self).FDebugGraph[1, 0, K] * (1 - Frac(J * I / 63)) + RecData(Self).FDebugGraph[1, 0, K + 1] * Frac(J * I / 63)
                               else A1[I]:= RecData(Self).FDebugGraph[1, 0, K] * (1 - Frac(J * I / 63));
      LineSeries5.AddXY(I, A1[I]+1);
      A2[I]:= 0;
    end;

      ( *
    for I:= 0 to J do
    begin
      SrcAmplSeries.AddXY(I, RecData(Self).FDebugGraph[1, 0, I]);
      A1[I + (64 - J) div 2]:= RecData(Self).FDebugGraph[1, 0, I];
      A2[I]:= 0;
    end;
        * )
    FFT(A1, A2);

    for I:= 0 to 63 do
    begin
      LineSeries6.AddXY(I, arctan2(A2[I], A1[I]));
//      LineSeries6.AddXY(I, sqrt(A2[I]*A2[I] + A1[I]*A1[I]));
    end;
        *)

  //  ScrollBar.Position:= RecData(Self).FPBList[R, Ch, ScrollBar1.Position].Items[0].Crd;
    SetLength(List, 0);
  end;
  {$ENDIF}
end;

procedure TRecDataForm.CheckBox7Click(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  BMGraphList.Visible:= CheckBox7.Checked;
  BMListItems.Visible:= CheckBox7.Checked;
{$ENDIF}
end;

procedure TRecDataForm.ListBox1Click(Sender: TObject);
{$IFDEF RECDEBUG}
var
  I, J, Ch, X, Y, Cr, Cr2, D, DD, Pos1, Pos2, ResDD: Integer;
  F, FM, Step: Double;
  R: RRail;
  KFDat: array [-50..50] of array of Integer;

function Func(Idx, X1, X2: Integer): Integer;
var
  I, J, K: Integer;
  Ch: Integer;
  DX: Integer;

begin
{  Result:= 0;
  with TRecData(FRecData) do
    for Ch:= 1 to 7 do
      for I:= 1 to FBMGraph[Idx].Graph[R, X1, Ch].Count do
        for DX:= - 2 to 2 do
        begin
          K:= X2 + DX;
          if K < 0 then K:= 0;
          if K > High(FBMGraph[Idx].Graph[R]) then K:= High(FBMGraph[Idx].Graph[R]);
          for J:= 1 to FBMGraph[Idx].Graph[R, K, Ch].Count do
            if Abs(FBMGraph[Idx].Graph[R, K, Ch].Delay[J] - FBMGraph[Idx].Graph[R, X1, Ch].Delay[I]) < 3 then Result:= Result + 1;
        end; }
end;

function Func2(Cr, Cr2: Integer): Single;
begin
  if (Cr >= Cr2 - Step / 2) and (Cr <= Cr2 + Step / 2) then Result:= Exp( - Power(Abs((Cr - Cr2) / (Step * 1.75)), 2)) else
  if (Cr >= Cr2 + Step / 2) then Result:= Exp( - Power(Abs((Cr - Cr2 - Step / 2) / (Step / 2)), 0.5)) else
  if (Cr <= Cr2 - Step / 2) then Result:= Exp( - Power(Abs((Cr - Cr2 + Step / 2) / (Step / 2)), 0.5));
end;
{$ENDIF}

begin
  (*
  case ComboBox4.ItemIndex of
    0: R:= r_Left;
    1: R:= r_Right;
  end;
  Series2.Clear;
  Series3.Clear;
  if (ListBox1.ItemIndex <> - 1) and (ComboBox3.ItemIndex <> - 1) then
    with PaintBox1, Canvas, RecData(Self).FBMGraph[ListBox1.ItemIndex] do
    begin
      R1:= Items[ComboBox3.ItemIndex].Range;
      R2:= Items[ComboBox3.ItemIndex + 1].Range;
      Len:= Min((R1.EdCrd - R1.StCrd), (R2.EdCrd - R2.StCrd));

      for DD:= ScrollBar2.Min to ScrollBar2.Max do SetLength(KFDat[DD], Len);

      for DD:= ScrollBar2.Min to ScrollBar2.Max do
        for Cr:= 0 to Len - 1 do
        begin
          Pos1:= R1.EdCrd - Cr;
          Pos2:= R2.StCrd + Cr + DD;
          if (Pos1 >= R1.StCrd) and (Pos1 <= R1.EdCrd) and
             (Pos2 >= R2.StCrd) and (Pos2 <= R2.EdCrd) then KFDat[DD, Cr]:= Func(ListBox1.ItemIndex, Pos1 - Range.StCrd, Pos2 - Range.StCrd);
        end;

      Step:= 30 / FScanStep;
      for I:= 0 to Round(Len / Step) do
      begin
        Cr2:= Round(I * Step);
        FM:= - 1;
        ResDD:= - MaxInt;
        for DD:= ScrollBar2.Min to ScrollBar2.Max do
        begin
          F:= 0;
          for Cr:= 0 to Len - 1 do F:= F + KFDat[DD, Cr] * Func2(Cr, Cr2);
          if F > FM then
          begin
            FM:= F;
            ResDD:= DD;
          end;
        end;
        if ResDD <> - MaxInt then Series3.AddXY(I, ResDD);
      end;

      for DD:= ScrollBar2.Min to ScrollBar2.Max do SetLength(KFDat[DD], 0);
    end;
   *)
end;

procedure TRecDataForm.ScrollBar2Change(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  F: Double;
  X, Cr, Pos1, Pos2: Integer;
{
procedure DrawCoord(X: Integer; Dat: TBMGEcho);
const
  YCh: array [1..7] of Integer = (4, 1, 1, 2, 2, 3, 3);

var
  I, Y, Ch: Integer;

begin
  with PaintBox1, Canvas do
    for Ch:= 1 to 7 do
    begin
      if Odd(Ch) then Brush.Color:= clBlue
                 else Brush.Color:= clRed;
      for I:= 1 to Dat[Ch].Count do
      begin
        Y:= YCh[Ch] * Height div 4 - Dat[Ch].Delay[I] * Height div (4 * 256);
        FillRect(Rect(X - 1, Y - 1, X + 1, Y + 1));
      end;
    end;
end;
}
function Func(Idx, X1, X2: Integer): Double;
// const
//  Koeff: array [-3..3] of Integer = (1, 10, 100, 1000, 100, 10, 1);
//  Koeff: array [-3..3] of Integer = (1, 1, 1, 1, 1, 1, 1);
{
var
  I, J, K: Integer;
  Ch: Integer;
  DX: Integer; }

begin
{  Result:= 0;
  with TRecData(FRecData) do
    for Ch:= 1 to 7 do
      for I:= 1 to FBMGraph[Idx].Graph[R, X1, Ch].Count do
        for DX:= - 2 to 2 do
        begin
          K:= X2 + DX;
          if K < 0 then K:= 0;
          if K > High(FBMGraph[Idx].Graph[R]) then K:= High(FBMGraph[Idx].Graph[R]);
          for J:= 1 to FBMGraph[Idx].Graph[R, K, Ch].Count do
            if Abs(FBMGraph[Idx].Graph[R, K, Ch].Delay[J] - FBMGraph[Idx].Graph[R, X1, Ch].Delay[I]) < 3 then Result:= Result + 1;
        end;  }
end;
{$ENDIF}

begin
(*  case ComboBox4.ItemIndex of
    0: R:= r_Left;
    1: R:= r_Right;
  end;
  Series2.Clear;
  Label6.Caption:= Format('P: %d', [ScrollBar2.Position]);
  if (ListBox1.ItemIndex <> - 1) and (ComboBox3.ItemIndex <> - 1) then
    with PaintBox1, Canvas, RecData(Self).FBMGraph[ListBox1.ItemIndex] do
    begin
      Brush.Color:= clWhite;
      FillRect(Rect(0, 0, Width, Height));
      for Cr:= 0 to Len - 1 do
      begin
        Pos1:= R1.EdCrd - Cr;
        Pos2:= R2.StCrd + Cr + ScrollBar2.Position;
        if (Pos1 >= R1.StCrd) and (Pos1 <= R1.EdCrd) and
           (Pos2 >= R2.StCrd) and (Pos2 <= R2.EdCrd) then
        begin
          X:= Cr * Width div Len;
          DrawCoord(X, Graph[R, Pos1 - Range.StCrd]);
          DrawCoord(X, Graph[R, Pos2 - Range.StCrd]);

          if CheckBox11.Checked then
          begin
            F:= Func(ListBox1.ItemIndex, Pos1 - Range.StCrd, Pos2 - Range.StCrd);
            Series2.AddXY(X, F);
          end;
        end
        else
        begin
          X:= Cr * Width div Len;
          Pen.Color:= clRed;
          MoveTo(X, 0);
          LineTo(X, Height);
        end;
      end;
    end; *)
end;

{$IFDEF RECDEBUG}
function TRecDataForm.BlkOk(StartDisCoord: Integer): Boolean;
(*
var
  Pt: TPoint;
  OutOfRange: Boolean;

  R: RRail;
  Flg: Boolean;
  I, J, X, Ch, DL, DH, D: Integer;
  Buff: TDestDat;
*)
begin
   (*
  if FDatSrc.BackMotion then Exit;  // ���� �������� ����� ����������

  for R:= r_Left to r_Right do
    for Ch:= 1 to 7 do
      if (Ch in [1, 6, 7]) and (FDatSrc.CurEcho[R, Ch].Count <> 0) then
        if CheckBox24.Checked then    // ���������� �������� ������������ �� �����
        begin

  // --------------------------------------------------------------------------------------------------------------

          Buff:= FDatSrc.CurEcho[R, Ch];

          Flg:= False;
          J:= FPBIdx[R, Ch];                                       // ����������� ����� �� ������� ����������

          if Length(RecData(Self).FPBList[R, Ch]) <> 0 then            // ���� ��� ����� ��� � ������
          begin
            while (RecData(Self).FPBList[R, Ch, J].Crd.EdCrd < FDatSrc.CurDisCoord) do
              if J < High(RecData(Self).FPBList[R, Ch]) then Inc(J)
                                                    else Break;

            FPBIdx[R, Ch]:= J;                                       // ������������ ����� ���������� ������� ����������

            while RecData(Self).FPBList[R, Ch, J].Crd.EdCrd - RecData(Self).FPBMaxLen[R, Ch] <= FDatSrc.CurDisCoord do
            begin
              if (RecData(Self).FPBList[R, Ch, J].Crd.StCrd <= FDatSrc.CurDisCoord) and
                 (RecData(Self).FPBList[R, Ch, J].Crd.EdCrd >= FDatSrc.CurDisCoord) and
                 ((RecData(Self).FPBList[R, Ch, J].Typ = pbtHole) or
                  (RecData(Self).FPBList[R, Ch, J].Typ = pbtEnd) or
                  (RecData(Self).FPBList[R, Ch, J].Typ = pbtUnknown)) then
              begin
       {
                if (Ch = 6) and
                   (R = r_Left) and
                   (FDatSrc.CurDisCoord >= 345476) and
                   (FDatSrc.CurDisCoord <= 345535) then
                begin
                  Memo2.Lines.Add(Format('Crd: %d; Idx: %d', [FDatSrc.CurDisCoord, J]));
                end;
       }
                if GetLen(RecData(Self).FPBList[R, Ch, J].Crd) < 4 then        // ���� ����� �������� - ����� �� �������� �� ������ � �����
                begin
                  DL:= RecData(Self).FPBList[R, Ch, J].Crd.EdCrd - RecData(Self).FPBList[R, Ch, J].Crd.StCrd;
                  DH:= RecData(Self).FPBList[R, Ch, J].Del.EdCrd - RecData(Self).FPBList[R, Ch, J].Del.StCrd;
                  X:= Abs(RecData(Self).FPBList[R, Ch, J].Crd.StCrd - FDatSrc.CurDisCoord);
                  D:= RecData(Self).FPBList[R, Ch, J].Del.StCrd + Round(X * DH / DL); // ���������� �������� ��� ������� ����������
                end
                else                                               // ���� ����� ������ - ����� �� �������� �� 3 ������
                begin
                  if FDatSrc.CurDisCoord <= RecData(Self).FPBList[R, Ch, J].CCrd then // �������� � ������ ��������
                  begin
                    DL:= RecData(Self).FPBList[R, Ch, J].CCrd - RecData(Self).FPBList[R, Ch, J].Crd.StCrd;
                    DH:= RecData(Self).FPBList[R, Ch, J].CDel - RecData(Self).FPBList[R, Ch, J].Del.StCrd;
                    X:= Abs(RecData(Self).FPBList[R, Ch, J].Crd.StCrd - FDatSrc.CurDisCoord);
                    D:= RecData(Self).FPBList[R, Ch, J].Del.StCrd + Round(X * DH / DL); // ���������� �������� ��� ������� ����������
                  end
                  else                                                            // �������� �� ������ ��������
                  begin
                    DL:= RecData(Self).FPBList[R, Ch, J].Crd.EdCrd - RecData(Self).FPBList[R, Ch, J].CCrd;
                    DH:= RecData(Self).FPBList[R, Ch, J].Del.EdCrd - RecData(Self).FPBList[R, Ch, J].CDel;
                    X:= Abs(RecData(Self).FPBList[R, Ch, J].CCrd - FDatSrc.CurDisCoord);
                    D:= RecData(Self).FPBList[R, Ch, J].CDel + Round(X * DH / DL); // ���������� �������� ��� ������� ����������
                  end;
                end;

                for I:= 1 to Buff.Count do         // �������� ������� ���������� � �����
                  if (Buff.Delay[I] >= D - 5) and (Buff.Delay[I] <= D + 5) then Buff.Ampl[I]:= 255;

                if CheckBox4.Checked then
                  with PaintBox, PaintBox.Canvas do
                  begin
                    Brush.Color:= clGreen;
                    if GetScrPos(R, Ch, FDatSrc.CurDisCoord, D - 5, False, Pt, OutOfRange) then
                      FillRect(Rect(Pt.X - 1, Pt.Y - 1, Pt.X + 1, Pt.Y + 1));
                    if GetScrPos(R, Ch, FDatSrc.CurDisCoord, D + 5, False, Pt, OutOfRange) then
                      FillRect(Rect(Pt.X - 1, Pt.Y - 1, Pt.X + 1, Pt.Y + 1));
                  end;
              end;

              Inc(J);
              if J > High(RecData(Self).FPBList[R, Ch]) then Break;
            end;
          end;

          for I:= 1 to Buff.Count do         // ������� ���������� � ����� - ���������� - �� ���������� ��� ���������� �����������
            if Buff.Ampl[I] <> 255 then
              Inc(RecData(Self).FAmplGis[R, Ch, Buff.Delay[I]], Sqr(16 - Buff.Ampl[I]));


  // --------------------------------------------------------------------------------------------------------------


        end
        else                                 // ��� ��������� - ��� �������
          for I:= 1 to FDatSrc.CurEcho[R, Ch].Count do
            Inc(RecData(Self).FAmplGis[R, Ch, FDatSrc.CurEcho[R, Ch].Delay[I]], Sqr(16 - FDatSrc.CurEcho[R, Ch].Ampl[I]));

  Result:= True; *)
end;
{$ENDIF}

procedure TRecDataForm.Button2Click(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  Ch, I: Integer;
{$ENDIF}

begin
(*
{$IFDEF RECDEBUG}
  Memo2.Clear;
  for R:= r_Left to r_Right do
    for Ch:= 1 to 7 do
    begin
      FPBIdx[R, Ch]:= 0;
      FillChar(RecData(Self).FAmplGis[R, Ch, 0], 256 * 4, 0);
    end;

  FDatSrc.LoadData(FStDrawCrd, FEdDrawCrd, 0, BlkOk);
  case ComboBox7.ItemIndex of
    0: R:= r_Left;
    1: R:= r_Right;
  end;
  Ch:= ComboBox8.ItemIndex + 1;

  if not (Ch in [1..7]) then Exit;

  Series4.Clear;
  for I:= 0 to 255 do
    Series4.AddXY(I,  RecData(Self).FAmplGis[R, Ch, I]);
{$ENDIF}
*)
end;

procedure TRecDataForm.MyRefresh(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  I, C: Integer;
{$ENDIF}

begin
{$IFDEF RECDEBUG}
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;
  CreateImage(nil);
{
  if PageControl1.ActivePage = TabSheet16 then
  with TestPB, TestPB.Canvas do
  begin
    Brush.Color:= clWhite;
    FillRect(Rect(0, 0, Width, Height));
  //  C:= (FStDrawCrd + FEdDrawCrd) div 2;
    FDatSrc.LoadData(FStDrawCrd, FEdDrawCrd, 0, DataNotify);
  end;
}

  if CheckBox6.Checked then
  begin
  (*
    for R:= r_Left to r_Right do
      for I:= 0 to 255 do
        RecData(Self).FTestGis[R, I]:= 0;
    RecData(Self).FFuncIdx:= 99;
    RecData(Self).RecLoadData(ScrollBar.Position - Round(500 / FScanStep), ScrollBar.Position + Round(500 / FScanStep), RecData(Self).BlockOK);
    LineSeries3.Clear;
    for I:= 0 to 255 do LineSeries3.AddXY(I, RecData(Self).FTestGis[r_Left, I]);
    LineSeries4.Clear;
    for I:= 0 to 255 do LineSeries4.AddXY(I, RecData(Self).FTestGis[r_Right, I]);
    *)

    C:= (ScrollBar.Position - RecData(Self).FStCrd) div RecData(Self).FBSignal.FGisStep;
    LineSeries3.Clear;
    if CheckBox12.Checked then
      for I:= 0 to 255 do LineSeries3.AddXY(I, RecData(Self).FBSignal.FDebugGis[r_Left, C].Gis[I]);
    LineSeries4.Clear;
    if CheckBox13.Checked then
      for I:= 0 to 255 do LineSeries4.AddXY(I, RecData(Self).FBSignal.FDebugGis[r_Right, C].Gis[I]);

    Memo1.Clear;
    Memo1.Lines.Add(Format('> %d', [RecData(Self).FBSignal.FDebugGis[r_Left, C].Range.StCrd]));
    Memo1.Lines.Add(Format('- %d', [ScrollBar.Position]));
    Memo1.Lines.Add(Format('< %d', [RecData(Self).FBSignal.FDebugGis[r_Left, C].Range.EdCrd]));

    Panel18.Caption:= Format('%d:%d - %d:%d',
                                   [RecData(Self).FBSignal.FDebugGis[r_Left, C].MaxPos[1],
                                    RecData(Self).FBSignal.FDebugGis[r_Left, C].MaxPos[2],
                                    RecData(Self).FBSignal.FDebugGis[r_Right, C].MaxPos[1],
                                    RecData(Self).FBSignal.FDebugGis[r_Right, C].MaxPos[2] ])

  end;
{$ENDIF}
end;

procedure TRecDataForm.MyRefresh2(Sender: TObject);
begin
{$IFDEF RECDEBUG}
//  FRec:= 2;
//  SpeedButton3.Down:= True;
  CreateImage(nil);
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;
{$ENDIF}
end;

{$IFDEF RECDEBUG}
procedure TRecDataForm.CreateOutInfo;
const
  ChSeq: array [0..7] of Integer = (2, 3, 4, 5, 6, 7, 0, 1);

var
  R: RRail;
  I: Integer;
  J: Integer;
  Y: Integer;
  H: Integer;
  M: Integer;
  K: Integer;
  Ch: Integer;

begin
  if not CheckBox2.Checked then
  begin
    M:= Ord(ViewStrip.State[0]) +
        Ord(ViewStrip.State[1]) +
        Ord(ViewStrip.State[2]) +
        Ord(ViewStrip.State[3]);

    if M = 0 then Exit;

    if not FResRect then H:= PaintBox.Height div (M * 2)
                    else H:= (PaintBox.Height - 2 * FResRectHeight) div (M * 2);

    SetLength(FOutInfo, M * 4);
    K:= 0;
    Y:= 0;
    for R:= r_Left to r_Right do
    begin
      for I:= 0 to 7 do
        if ((ViewStrip.State[0] = cbChecked) and (ChSeq[I] in [2, 3])) or
           ((ViewStrip.State[1] = cbChecked) and (ChSeq[I] in [4, 5])) or
           ((ViewStrip.State[2] = cbChecked) and (ChSeq[I] in [6, 7])) or
           ((ViewStrip.State[3] = cbChecked) and (ChSeq[I] in [0, 1])) then
        begin
          FOutInfo[K].Rail:= R;
          FOutInfo[K].Ch:= ChSeq[I];
          FOutInfo[K].Sgl_Rt.Left:= 0;
          FOutInfo[K].Sgl_Rt.Right:= PaintBox.Width;
          FOutInfo[K].Sgl_Rt.Top:= Y;
          FOutInfo[K].Sgl_Rt.Bottom:= Y + H;
          if ChSeq[I] in [3, 5, 7, 1] then Inc(Y, H);
          Inc(K);
        end;
      if FResRect then
      begin
        FResRt[R].Left:= 0;
        FResRt[R].Right:= PaintBox.Width;
        FResRt[R].Top:= Y;
        Inc(Y, FResRectHeight);
        FResRt[R].Bottom:= Y;
      end;
    end;
  end
  else
  begin
    Y:= 0;

    M:= 2 * Ord(ViewStrip.State[0]) +
        2 * Ord(ViewStrip.State[1]) +
        2 * Ord(ViewStrip.State[2]) +
        2 * Ord(ViewStrip.State[3]);

    M:= M * 2;

    if M = 0 then Exit;
    K:= 0;


    if not FResRect then H:= PaintBox.Height div M
                    else H:= (PaintBox.Height - 2 * FResRectHeight) div M;

    SetLength(FOutInfo, M);
    for R:= r_Left to r_Right do
    begin
      for I:= 0 to 7 do
        if ((ViewStrip.State[0] = cbChecked) and (ChSeq[I] in [2, 3])) or
           ((ViewStrip.State[1] = cbChecked) and (ChSeq[I] in [4, 5])) or
           ((ViewStrip.State[2] = cbChecked) and (ChSeq[I] in [6, 7])) or
           ((ViewStrip.State[3] = cbChecked) and (ChSeq[I] in [0, 1])) then
        begin
          FOutInfo[K].Rail:= R;
          FOutInfo[K].Ch:= ChSeq[I];
          FOutInfo[K].Sgl_Rt.Left:= 0;
          FOutInfo[K].Sgl_Rt.Right:= PaintBox.Width;
          FOutInfo[K].Sgl_Rt.Top:= Y;
          FOutInfo[K].Sgl_Rt.Bottom:= Y + H;
          Inc(Y, H);
          Inc(K);
        end;
      if FResRect then
      begin
        FResRt[R].Left:= 0;
        FResRt[R].Right:= PaintBox.Width;
        FResRt[R].Top:= Y;
        Inc(Y, FResRectHeight);
        FResRt[R].Bottom:= Y;
      end;
    end;
  end;

  if FDebugRect then
    for I:= 0 to High(FOutInfo) do
    begin
      if CheckBox2.Checked then
      begin
        FOutInfo[I].Dbg_Rt:= FOutInfo[I].Sgl_Rt;
        FOutInfo[I].Sgl_Rt.Bottom:= FOutInfo[I].Sgl_Rt.Bottom - 20;
        FOutInfo[I].Dbg_Rt.Top:= FOutInfo[I].Sgl_Rt.Bottom;
      end
      else
      begin
        FOutInfo[I].Dbg_Rt:= FOutInfo[I].Sgl_Rt;
        FOutInfo[I].Sgl_Rt.Bottom:= FOutInfo[I].Sgl_Rt.Bottom - 20;
        if Odd(I) then
        begin
          FOutInfo[I].Dbg_Rt.Top:= FOutInfo[I].Sgl_Rt.Bottom;
          FOutInfo[I].Dbg_Rt.Bottom:= FOutInfo[I].Dbg_Rt.Bottom - 10;
        end
        else FOutInfo[I].Dbg_Rt.Top:= FOutInfo[I].Sgl_Rt.Bottom + 10;
      end;
    end;

  for R:= r_Left to r_Right do
    for Ch:= 0 to 7 do
    begin
      FOutLink[R, Ch].Visible:= False;
      FDbgLink[R, Ch].Visible:= False;
    end;

  for I:= 0 to High(FOutInfo) do
  begin
    FOutLink[FOutInfo[I].Rail, FOutInfo[I].Ch].Visible:= True;
    FOutLink[FOutInfo[I].Rail, FOutInfo[I].Ch].Rt:= FOutInfo[I].Sgl_Rt;
  end;

  for I:= 0 to High(FOutInfo) do
  begin
    FDbgLink[FOutInfo[I].Rail, FOutInfo[I].Ch].Visible:= True;
    FDbgLink[FOutInfo[I].Rail, FOutInfo[I].Ch].Rt:= FOutInfo[I].Dbg_Rt;
  end;

end;

function TRecDataForm.BlockOK_BMFon(StartDisCoord: Integer): Boolean;
begin
end;

function TRecDataForm.BlockOK_Echo(StartDisCoord: Integer): Boolean;
var
  R: RRail;
  Pos: TPoint;
  Ch, X, Y1, Y2: Integer;
  I, J, K, L, M, Delta: Integer;
  Crd: array [r_left..r_Right] of Integer;
  OutOfRange: Boolean;
  Flg: Boolean;
  Mode: Integer;
  StCrd: Integer;
  Dir: Integer;

begin
//  if FCoordMode = 1 then RecData(Self).FCurRecCoord:= FDatSrc.CurDisCoord;
  Min_:= Min(Min_, FDatSrc.CurDisCoord);
  Max_:= Max(Max_, FDatSrc.CurDisCoord);

  Result:= True;
(*
  Mode:= 0;
  if CheckBox1.Checked then
  begin
    for J:= 0 to High(RecData(Self).FBMGraph) do
      if  BMGraphList.Checked[J] and
         (RecData(Self).FCurRecCoord >= RecData(Self).FBMGraph[J].Range.StCrd) and
         (RecData(Self).FCurRecCoord <= RecData(Self).FBMGraph[J].Range.EdCrd) then
      for K:= 0 to High(RecData(Self).FBMGraph[J].Items) do
        if BMListItems.Checked[K] and
           (RecData(Self).FCurRecCoord >= RecData(Self).FBMGraph[J].Items[K].Range.StCrd) and
           (RecData(Self).FCurRecCoord <= RecData(Self).FBMGraph[J].Items[K].Range.EdCrd) then
        begin
          Mode:= 1;
          Crd[r_Left]:=  RecData(Self).FBMGraph[J].Range.StCrd + RecData(Self).MoveFromBM(RecData(Self).FCurRecCoord, r_Left)  - FDatSrc.DisToSysCoord(RecData(Self).FBMGraph[J].Range.StCrd);
          Crd[r_Right]:= RecData(Self).FBMGraph[J].Range.StCrd + RecData(Self).MoveFromBM(RecData(Self).FCurRecCoord, r_Right) - FDatSrc.DisToSysCoord(RecData(Self).FBMGraph[J].Range.StCrd);
          Break;
        end;
  end; *)
(*  if CheckBox3.Checked then
  begin
    for J:= 0 to High(RecData(Self).FBMGraph) do
      if  BMGraphList.Checked[J] and
         (RecData(Self).FCurRecCoord >= RecData(Self).FBMGraph[J].Range.StCrd) and
         (RecData(Self).FCurRecCoord <= RecData(Self).FBMGraph[J].Range.EdCrd) then
      for K:= 0 to High(RecData(Self).FBMGraph[J].Items) do
        if BMListItems.Checked[K] and
           (RecData(Self).FCurRecCoord >= RecData(Self).FBMGraph[J].Items[K].Range.StCrd) and
           (RecData(Self).FCurRecCoord <= RecData(Self).FBMGraph[J].Items[K].Range.EdCrd) then
        begin
          Mode:= 1;

          Crd[r_Left]:=  RecData(Self).FBMGraph[J].Range.StCrd + RecData(Self).MoveFromBM(RecData(Self).FCurRecCoord, r_Left)  - FDatSrc.DisToSysCoord(RecData(Self).FBMGraph[J].Range.StCrd);
          Crd[r_Right]:= RecData(Self).FBMGraph[J].Range.StCrd + RecData(Self).MoveFromBM(RecData(Self).FCurRecCoord, r_Right) - FDatSrc.DisToSysCoord(RecData(Self).FBMGraph[J].Range.StCrd);

          RecData(Self).BackToBM( Crd[r_Left],  r_Left, ComboBox6.ItemIndex {RecData(Self).BackToBM_MaxIdx(Crd[r_Left]) }, Crd[r_Left]);
          RecData(Self).BackToBM(Crd[r_Right], r_Right, ComboBox6.ItemIndex {RecData(Self).BackToBM_MaxIdx(Crd[r_Right])}, Crd[r_Right]);

//          Crd[r_Left]:= FDatSrc.SysToDisCoord(RecData(Self).MoveFromBM(FDatSrc.CurDisCoord, r_Left));
//          Crd[r_Right]:= FDatSrc.SysToDisCoord(RecData(Self).MoveFromBM(FDatSrc.CurDisCoord, r_Right));


          Break;
        end;
  end;
  *)

//  if Mode = 0 then
  begin
    Crd[r_Left] := FDatSrc.CurDisCoord;
    Crd[r_Right]:= FDatSrc.CurDisCoord;
  end;

  with PaintBox, PaintBox.Canvas do
    for I:= 0 to High(FOutInfo) do
      with FDatSrc.CurEcho[FOutInfo[I].Rail, FOutInfo[I].Ch] do
        if Count <> 0 then
        begin
          if not (((FOutInfo[I].Ch in [2, 4, 6, 8]) and cbZoomIn.Checked) or
                  ((FOutInfo[I].Ch in [1, 3, 5, 7]) and cbZoomOut.Checked)) then Continue;

          case FOutInfo[I].Ch of
            1, 3, 5, 7: Brush.Color:= clBlue;
            2, 4, 6, 8: Brush.Color:= clRed;
                   else Brush.Color:= clBlack;
          end;

          for J:= 1 to Count do
          begin
                                        // ����������

            if CheckBox44.Checked and (not CheckListBox1.Checked[Ampl[J] - 3]) then Continue;

            if CheckBox42.Checked and Assigned(RecData(Self).Filtr) then
              if not RecData(Self).Filtr.TestEcho(FDatSrc.CurDisCoord, FOutInfo[I].Rail, FOutInfo[I].Ch, Ampl[J], Delay[J]) then Continue;

            if CheckBox43.Checked and Assigned(RecData(Self).Filtr) then
            begin
              if not RecData(Self).Filtr.TestEcho(FDatSrc.CurDisCoord, FOutInfo[I].Rail, FOutInfo[I].Ch, Ampl[J], Delay[J]) then Brush.Color:= clSilver else
              case FOutInfo[I].Ch of
                1, 3, 5, 7: Brush.Color:= clBlue;
                2, 4, 6, 8: Brush.Color:= clRed;
                       else Brush.Color:= clBlack;
              end;
            end;
         {
            if CheckBox45.Checked and
               ((FOutInfo[I].Ch in [2, 3, 4, 5]) or
                ((FOutInfo[I].Ch in [6, 7]) and ((Delay[J] < 50) or (Delay[J] > 100))))  then
            begin
              if Odd(FOutInfo[I].Ch) then Delta:= - 1 else Delta:= 1;
              Flg:= FDatSrc.CurDisCoord - RecData(Self).FFilter1[FOutInfo[I].Rail, FOutInfo[I].Ch, Delay[J] div 2 + Delta] < 4;
              RecData(Self).FFilter1[FOutInfo[I].Rail, FOutInfo[I].Ch, Delay[J] div 2]:= FDatSrc.CurDisCoord;
              if not Flg then Continue;
            end;

            if CheckBox46.Checked then
            begin
              Flg:= FDatSrc.CurDisCoord - RecData(Self).FFilter2[FOutInfo[I].Rail, FOutInfo[I].Ch, Delay[J]] < 600;
              RecData(Self).FFilter2[FOutInfo[I].Rail, FOutInfo[I].Ch, Delay[J]]:= FDatSrc.CurDisCoord;
              if Flg then Continue;
            end;
          }
            if CheckBox53.Checked and (FOutInfo[I].Ch = 1) then
            begin
              Flg:= False;
              for K:= 1 to Count do
                if Ampl[J] < Ampl[K] then
                begin
                  Flg:= True;
                  Break;
                end;
              if Flg then Continue;
            end;

            if not (Ampl[J] >= ViewTh.Position) then Continue;
        //    if (FOutInfo[I].Ch = 1) and (Ampl[J] < 6) then Continue;

            if not CheckBox52.Checked then
            begin
              if GetScrPos(FOutInfo[I].Rail, FOutInfo[I].Ch, Crd[FOutInfo[I].Rail], Delay[J], FDatSrc.BackMotion, Pos, OutOfRange) and (not OutOfRange) then
//                FillRect(Rect(Pos.X - 1, Pos.Y - 1, Pos.X + 1, Pos.Y + 1));
                FillRect(Rect(Pos.X - EchoSize[FEchoSizeIdx, 0], Pos.Y - EchoSize[FEchoSizeIdx, 1], Pos.X + EchoSize[FEchoSizeIdx, 2], Pos.Y + EchoSize[FEchoSizeIdx, 3]));

//              Brush.Style:= bsClear;
//              Font.Size:= 6;
//              if Odd(FDatSrc.CurDisCoord) then TextOut(Pos.X - 1, Pos.Y - 10, IntToStr(Ampl[J]));
            end
            else
            begin
           {   if FOutInfo[I].Ch in [2..7] then
              begin
                if GetScrPos(FOutInfo[I].Rail, FOutInfo[I].Ch, Crd[FOutInfo[I].Rail], Delay[J], FDatSrc.BackMotion, Pos, OutOfRange) and (not OutOfRange) then
                  FillRect(Rect(Pos.X - EchoSize[0], Pos.Y - EchoSize[1], Pos.X + EchoSize[2], Pos.Y + EchoSize[3]));
              end
              else
              if (FOutInfo[I].Ch = 1) then }
                if GetScrPos(FOutInfo[I].Rail, FOutInfo[I].Ch, Crd[FOutInfo[I].Rail], Delay[J] + (15 - Ampl[J]) , FDatSrc.BackMotion, Pos, OutOfRange) and (not OutOfRange) then
                  FillRect(Rect(Pos.X - EchoSize[FEchoSizeIdx, 0], Pos.Y - EchoSize[FEchoSizeIdx, 1], Pos.X + EchoSize[FEchoSizeIdx, 2], Pos.Y + EchoSize[FEchoSizeIdx, 3]));
//                  FillRect(Rect(Pos.X - 1, Pos.Y - 1, Pos.X + 1, Pos.Y + 1));

            end;
          end;
              {
          if (FOutInfo[I].Ch = 1) and (FOutLink[FOutInfo[I].Rail, FOutInfo[I].Ch].Visible) then
          begin
            Dir:= 0;
            for J:= 1 to Count do Dir:= Dir + Ampl[J];
            Brush.Color:= clBlack;
            Pos.X:= Round((FDatSrc.CurDisCoord - FStDrawCrd) / FXZoom);
            with FOutLink[FOutInfo[I].Rail, FOutInfo[I].Ch].Rt do Pos.Y:= Round(Bottom - (Bottom - Top) * Dir / 30);
            FillRect(Rect(Pos.X - 1, Pos.Y - 1, Pos.X + 1, Pos.Y + 1));
          end;
               }
    {
          if CheckBox1.Checked and (Mode = 1) then
          begin
            if CheckBox3.Checked then Brush.Color:= ColorBox1.Selected;
            for J:= 1 to Count do
            begin
              if not (Ampl[J] >= ViewTh.Position) then Continue;
              if C2 = - 1 then Flg:= GetScrPos(FOutInfo[I].Rail, FOutInfo[I].Ch,  - (FDatSrc.CurDisCoord - StCrd) + C1 + Delta[FOutInfo[I].Rail], Delay[J],     FDatSrc.BackMotion, Pos, OutOfRange)
                          else Flg:= GetScrPos(FOutInfo[I].Rail, FOutInfo[I].Ch,     FDatSrc.CurDisCoord - StCrd  + C1 - Delta[FOutInfo[I].Rail], Delay[J], not FDatSrc.BackMotion, Pos, OutOfRange);
              if Flg then FillRect(Rect(Pos.X - 1, Pos.Y - 1, Pos.X + 1, Pos.Y + 1));
            end;
          end
          else
          for J:= 1 to Count do
          begin
            if not (Ampl[J] >= ViewTh.Position) then Continue;
            if GetScrPos(FOutInfo[I].Rail, FOutInfo[I].Ch, FDatSrc.CurDisCoord, Delay[J], FDatSrc.BackMotion, Pos, OutOfRange) and (not OutOfRange) then
              FillRect(Rect(Pos.X - 1, Pos.Y - 1, Pos.X + 1, Pos.Y + 1));
          end;
    }
        end;
end;

function TRecDataForm.GetScrPos3(R: RRail; Crd1, Crd2, StStrip, EdStrip: Integer; var Rt: TRect): Boolean;
const
  ChList: array [0..3] of Integer = (2, 4, 6, 1);

begin
  if R = r_Both then Exit;
  Result:= True;
  Rt.Left:=  Round((Crd1 - FStDrawCrd) / FXZoom);
  if Rt.Left < FOutLink[R, 1].Rt.Left then Rt.Left:= FOutLink[R, 1].Rt.Left;
  if Rt.Left > FOutLink[R, 1].Rt.Right then Result:= False;
  Rt.Right:= Round((Crd2 - FStDrawCrd) / FXZoom);
  if Rt.Right > FOutLink[R, 1].Rt.Right then Rt.Right:= FOutLink[R, 1].Rt.Right;
  if Rt.Right < FOutLink[R, 1].Rt.Left then Result:= False;
  Rt.Top:= FOutLink[R, ChList[StStrip]].Rt.Top;
  Rt.Bottom:= FOutLink[R, ChList[EdStrip]].Rt.Bottom;
end;

function TRecDataForm.GetScrPos(R: RRail; Ch, Crd, Delay: Integer; BM: Boolean; var Pos: TPoint; var OutOfRange: Boolean): Boolean;
var
  Dir: Integer;

begin
  if not FOutLink[R, Ch].Visible then
  begin
    Result:= False;
    Exit;
  end;

  if not (Ch in [0, 1]) then
  begin
    with ChList[Ch] do
      if (Delay < DelayMin) or (Delay > DelayMax) then
      begin
        Result:= False;
        Exit;
      end;
  end
  else
  begin
    with ChList[Ch] do
      if (Delay < DelayMin * 3) or (Delay > DelayMax * 3) then
      begin
        Result:= False;
        Exit;
      end;
  end;

  Result:= True;
  OutOfRange:= False;

  if BM then Dir:= - 1
        else Dir:= + 1;

  case FRec of
    0: Pos.X:= Round((Crd - FStDrawCrd) / FXZoom);
    1: Pos.X:= Round((Crd - FStDrawCrd) / FXZoom + Dir * ((ChList[Ch].Shift[1] + ChList[Ch].RedPar[1] * Delay) / FScanStep) / FXZoom);
    2: Pos.X:= Round((Crd - FStDrawCrd) / FXZoom + Dir * ((ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * Delay) / FScanStep) / FXZoom);
  end;

  if CheckBox22.Checked then
    with FOutLink[R, Ch].Rt, ChList[Ch] do
      Pos.Y:= Round(Bottom - (Bottom - Top) * (Delay * DelayStep / 100 - DelayMin) / (DelayMax - DelayMin)) else
    with FOutLink[R, Ch].Rt, ChList[Ch] do
      Pos.Y:= Round(Bottom - (Bottom - Top) * (Delay * DelayStep / 100 - 0) / (255));

  if (Pos.X < FOutLink[R, Ch].Rt.Left) or
     (Pos.X > FOutLink[R, Ch].Rt.Right) then
  begin
    OutOfRange:= True;
    Exit;
  end;
end;

function TRecDataForm.GetScrPosNoRed(R: RRail; Ch, Crd, Delay: Integer; BM: Boolean; var Pos: TPoint; var OutOfRange: Boolean): Boolean;
var
  Dir: Integer;

begin
  if not FOutLink[R, Ch].Visible then
  begin
    Result:= False;
    Exit;
  end;

  Result:= True;
  OutOfRange:= False;

  if BM then Dir:= - 1
        else Dir:= + 1;

  Pos.X:= Round((Crd - FStDrawCrd) / FXZoom);

  with FOutLink[R, Ch].Rt, ChList[Ch] do
    Pos.Y:= Round(Bottom - (Bottom - Top) * (Delay * DelayStep / 100 - DelayMin) / (DelayMax - DelayMin));

  if (Pos.X < FOutLink[R, Ch].Rt.Left) or
     (Pos.X > FOutLink[R, Ch].Rt.Right) then
  begin
    OutOfRange:= True;
    Exit;
  end;
end;

function TRecDataForm.GetScrPos2(R: RRail; Ch, Crd: Integer; var X, Y1, Y2: Integer; NoRed: Boolean = False): Boolean;
begin
  if not FOutLink[R, Ch].Visible then
  begin
    Result:= False;
    Exit;
  end;

  if NoRed then X:= Round((Crd - FStDrawCrd) / FXZoom)
           else case FRec of
                  0: X:= Round((Crd - FStDrawCrd) / FXZoom);
                  1: X:= Round((Crd - FStDrawCrd) / FXZoom + ((ChList[Ch].Shift[1]) / FScanStep) / FXZoom);
                  2: X:= Round((Crd - FStDrawCrd) / FXZoom + ((ChList[Ch].Shift[2]) / FScanStep) / FXZoom);
                end;

  if (X < FOutLink[R, Ch].Rt.Left) or
     (X > FOutLink[R, Ch].Rt.Right) then
  begin
    if X < FOutLink[R, Ch].Rt.Left then X:= FOutLink[R, Ch].Rt.Left;
    if X > FOutLink[R, Ch].Rt.Right then X:= FOutLink[R, Ch].Rt.Right;
    Result:= False;
    Exit;
  end;

  with FOutLink[R, Ch].Rt, ChList[Ch] do
  begin
    Y1:= Top;
    Y2:= Bottom;
  end;
  Result:= True;
end;

function TRecDataForm.TestScrPos(Crd: Integer; var X: Integer): Integer;
begin
  X:= Round((Crd - FStDrawCrd) / FXZoom);
  if X < FOutLink[r_Left, 2].Rt.Left then
  begin
    X:= FOutLink[r_Left, 2].Rt.Left;
    Result:= - 1;
  end
  else
  if X > FOutLink[r_Left, 2].Rt.Right then
  begin
    X:= FOutLink[r_Left, 2].Rt.Right;
    Result:= 1;
  end
  else Result:= 0;
end;

function TRecDataForm.GetDebugPos(R: RRail; Ch, Crd, Delay: Integer; BM: Boolean; var Pos: TPoint): Boolean;
var
  Dir: Integer;

begin
  if not FOutLink[R, Ch].Visible then
  begin
    Result:= False;
    Exit;
  end;

  if BM then Dir:= - 1
        else Dir:= + 1;

  case FRec of
    0: Pos.X:= Round((Crd - FStDrawCrd) / FXZoom);
    1: Pos.X:= Round((Crd - FStDrawCrd) / FXZoom + Dir * ((ChList[Ch].Shift[1] + ChList[Ch].RedPar[1] * Delay) / FScanStep) / FXZoom);
    2: Pos.X:= Round((Crd - FStDrawCrd) / FXZoom + Dir * ((ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * Delay) / FScanStep) / FXZoom);
  end;

  Result:= True;
  with FDbgLink[R, Ch].Rt, ChList[Ch] do
    Pos.Y:= (Bottom + Top) div 2;

  if (Pos.X < FOutLink[R, Ch].Rt.Left) or
     (Pos.X > FOutLink[R, Ch].Rt.Right) then
  begin
    Result:= False;
    Exit;
  end;
end;

function TRecDataForm.GetDisCrd(ScrX: Integer): Integer;
begin
  Result:= Round(FStDrawCrd + ScrX * FXZoom);
end;

function TRecDataForm.GetScrCrd(DisCrd: Integer): Integer;
begin
  Result:= Round((DisCrd - FStDrawCrd) / FXZoom);
end;

procedure TRecDataForm.CreateImage(Sender: TObject);
var
  R: RRail;
  R_: RRail;
  X: Integer;
  XX: Integer;
  X1: Integer;
  X2: Integer;
  K: Integer;
  Y: Integer;
  Y1: Integer;
  Y2: Integer;
  Y3: Integer;
  Y4: Integer;
  I: Integer;
  J: Integer;
  Ch: Integer;
  Idx: Integer;
  StStrip, EdStrip: Integer;

  NewTopPos_: TPoint;
  NewTopPos: TPoint;
  NewBtmPos: TPoint;
  TopPos: TPoint;
  TopPos_: TPoint;
  BtmPos: TPoint;
  Flg: Boolean;
  Flg1: Boolean;
  Flg2: Boolean;
  Flg3: Boolean;
  Flg4: Boolean;

  NewPos: array [1..2] of TPoint;
  OldPos: array [1..2] of TPoint;
  OutOfRange, Flag: Boolean;

  QQ: Integer;
  S: string;
  Rt: TRect;
  Rt1: TRect;

  State: array [TPointTyp] of Integer;

  Log: TStringList;
  T: DWord;
  SavePt: TPoint;
  Pt, Pt1, Pt2: TPoint;
  DL, DH, D, HH: Integer;
  StR, EdR: RRail;

  OT: TSingleObjTyp;
  Mask: array of TRange;
  XRange: TRange;

procedure DrawSel(Can: TCanvas; X1, X2, Y1, Y2, E: Integer);
begin
  with Can do
  begin
    MoveTo(X1 + E, Y1 + E);
    LineTo(X1,     Y1 + E);
    LineTo(X1,     Y2 - E);
    LineTo(X1 + E, Y2 - E);
    MoveTo(X2 - E, Y1 + E);
    LineTo(X2,     Y1 + E);
    LineTo(X2,     Y2 - E);
    LineTo(X2 - E, Y2 - E);
  end;
end;

begin
  if not Assigned(FDatSrc) then Exit;

  with PaintBox, PaintBox.Canvas do
  begin
    Font.Size:= 8;
    Font.Name:= 'Verdana';

// ---------------- Log - ���� ��������� ---------------------------------------

    Log:= TStringList.Create;
//    if FileExists('X:\draw.log') then Log.LoadFromFile('X:\draw.log');

    T:= GetTickCount;
    Log.Add(Format('%d - ����', [GetTickCount - T]));  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T:= GetTickCount;

// ---------------- ������� ----------------------------------------------------

    Pen.Style:= psSolid;
    Brush.Color:= clWhite;
    FillRect(Rect(0, 0, Width, Height));

    Pen.Color:= clBlack;
    Pen.Width:= 1;
    Brush.Style:= bsClear;
    for I:= 0 to High(FOutInfo) do
      Rectangle(FOutInfo[I].Sgl_Rt);

// ---------------- ������� ----------------------------------------------------

    FStDrawCrd:= Round(ScrollBar.Position - Width * FXZoom / 2);
    FEdDrawCrd:= Round(ScrollBar.Position + Width * FXZoom / 2);

    Log.Add(Format('%d - �������', [GetTickCount - T])); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T:= GetTickCount;

// ---------------- ��� --------------------------------------------------------

    if FCoordMode = 1 then
    begin
      J:= 0;
      with FDatSrc, PaintBox.Canvas do
      begin
        Brush.Color:= clYellow;
        for I:= 0 to High(FDatSrc.FCDList) do
        begin
          if Event[FCDList[I]].Event.ID = EID_BwdDir then J:= Event[FCDList[I]].Event.DisCoord;
          if Event[FCDList[I]].Event.ID = EID_FwdDir then
          begin
            K:= Event[FCDList[I]].Event.DisCoord;
            if (K >= Max(FStCrd, FStDrawCrd)) and
               (J <= Min(FEdCrd, FEdDrawCrd)) then
            begin
              for R:= r_Left to r_Right do
                for Ch:= 0 to 3 do
                begin
                  GetScrPos2(R, Ch * 2, Max(J, Max(FStCrd, FStDrawCrd)), X1, Y1, Y2, True);
                  GetScrPos2(R, Ch * 2, Min(K, Min(FEdCrd, FEdDrawCrd)), X2, Y1, Y2, True);
                  FillRect(Rect(X1, Y1 + 2, X2, Y2 - 2));
                end;
            end;
          end;
        end;
      end;
    end;

    Log.Add(Format('%d - ���', [GetTickCount - T])); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T:= GetTickCount;

// ---------------- ����� ������ ��������� ��������� --------------------------------------

    if ShowBJMask.Checked then
    begin
      SetLength(Mask, 8);
      Pen.Color:= clBlack;
      Pen.Width:= 2;
      for R_:= r_Left to r_Right do
      begin
        Mask[0]:= Range( - 480, - 440 );
        Mask[1]:= Range( - 340, - 300 );               // ����
        Mask[2]:= Range( - 110,  - 90 );
        Mask[3]:= Range(  - 10,    10 );
        Mask[4]:= Range(  - 10,    10 );
        Mask[5]:= Range(    90,   110 );
        Mask[6]:= Range(   300,   340 );
        Mask[7]:= Range(   440,   480 );
        for I:= 0 to 7 do                               // ��������� � DisCoord
        begin
          Mask[I].StCrd:= Round((FStDrawCrd + FEdDrawCrd) / 2 + Mask[I].StCrd / FScanStep);
          Mask[I].EdCrd:= Round((FStDrawCrd + FEdDrawCrd) / 2 + Mask[I].EdCrd / FScanStep);

          if GetScrPos3(R_, Mask[I].StCrd, Mask[I].EdCrd, 2, 3, Rt) then
            DrawSel(PaintBox.Canvas, Rt.Left, Rt.Right, Rt.Top, Rt.Bottom, 5);
        end;
      end;
      Pen.Width:= 1;
      SetLength(Mask, 0);
    end;

// ---------------- ������ �� --------------------------------------

    if CheckBox25.Checked then
      for I:= 0 to High(RecData(Self).FBJButton) do
      begin
        if (GetScrPos3(r_Left, RecData(Self).FBJButton[I].StCrd, RecData(Self).FBJButton[I].EdCrd, 0, 3, Rt)) then
        begin
          Brush.Color:= $0082FF82;
          FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
          TextOut(Rt.Left, Rt.Top + 1, IntToStr(I));
        end;
        if (GetScrPos3(r_Right, RecData(Self).FBJButton[I].StCrd, RecData(Self).FBJButton[I].EdCrd, 0, 3, Rt)) then
        begin
          Brush.Color:= $0082FF82;
          FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
        end;
      end;

// ---------------- ���� �� �� ������ -------------------------------------------

    if CheckBox33.Checked then
      with TBMList(RecData(Self).FBMList) do
      for I:= 0 to High(FBMData) do
        for J:= 0 to High(FBMData[I].LastPass) do
        begin
          if (GetScrPos3(r_Left, FBMData[I].LastPass[J].StCrd, FBMData[I].LastPass[J].EdCrd, 0, 3, Rt)) then
          begin
            Brush.Color:= $0082FF82;
            FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
          end;
          if (GetScrPos3(r_Right, FBMData[I].LastPass[J].StCrd, FBMData[I].LastPass[J].EdCrd, 0, 3, Rt)) then
          begin
            Brush.Color:= $0082FF82;
            FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
          end;
        end;

// ---------------- ���� �� � ��������� �� ��� ------------------------------------------

    if CheckBox38.Checked then
    begin
      J:= 10;
      Pen.Color:= clBlack;
      Pen.Width:= 3;
      Brush.Style:= bsClear;

      with RecData(Self) do
      for I:= 0 to High(FBJSearch) do
      begin
        if (GetScrPos3(r_Left, FBJSearch[I].StCrd, FBJSearch[I].EdCrd, 0, 3, Rt)) and
           (GetScrPos3(r_Right, FBJSearch[I].StCrd, FBJSearch[I].EdCrd, 0, 3, Rt1)) then
        begin
          Rectangle(Rect(Rt.Left, J, Rt.Right, Rt1.Bottom - 1));
          TextOut(Rt.Left, J, IntToStr(FBJSearch[I].StCrd) + ' ' + IntToStr(I));
          TextOut(Rt.Right, J, IntToStr(FBJSearch[I].EdCrd));
          Inc(J, 15);
        end;
      end;
      Pen.Width:= 1;
    end;

    if CheckBox54.Checked then
    begin
      J:= 10;
      Pen.Color:= clBlack;
      Pen.Width:= 3;
      Brush.Style:= bsClear;

      with RecData(Self) do
      for I:= 0 to High(FBJView) do
      begin
        if (GetScrPos3(r_Right, FBJView[I].StCrd, FBJView[I].EdCrd, 0, 3, Rt)) then
        begin
//          Brush.Color:= $0082FF82;
          Rectangle(Rect(Rt.Left, J, Rt.Right, Rt.Bottom - 1));
//          FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
//          Brush.Color:= clBlack;
//          FillRect(Rect(Rt.Left, J, Rt.Right, J + 5));
        end;
      end;
      Pen.Width:= 1;
    end;
{
    if CheckBox55.Checked then
    begin
      J:= 10;
      Pen.Color:= clBlack;
      Pen.Width:= 3;
      Brush.Style:= bsClear;

      with RecData(Self) do
      for I:= 0 to High(FBJPtList) do
      begin
        GetScrPos3(r_Left, FBJPtList[I], FBJPtList[I], 0, 3, Rt1);
        if (GetScrPos3(r_Right, FBJPtList[I], FBJPtList[I], 0, 3, Rt)) then
        begin
          Rectangle(Rect(Rt.Left, Rt1.Top, Rt.Left + 1, Rt.Bottom - 1));
          TextOut(Rt.Left, J, IntToStr(FBJPtList[I]) + ' ' + IntToStr(I));
          Inc(J, 15);
        end;
      end;
      Pen.Width:= 1;
    end;
}
               {
    if CheckBox33.Checked then
      with TBMList(RecData(Self).FBMList) do
      for I:= 0 to High(WorkZones) do
      begin
        if (GetScrPos3(r_Left, WorkZones[I].StCrd, WorkZones[I].EdCrd, 0, 3, Rt)) then
        begin
          Brush.Color:= $0082FF82;
          FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
        end;
        if (GetScrPos3(r_Right, WorkZones[I].StCrd, WorkZones[I].EdCrd, 0, 3, Rt)) then
        begin
          Brush.Color:= $0082FF82;
          FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
        end;
      end;
                   }

 // ---------------- ����� 2 --------------------------------------
 (*
    if cbEnd.Checked or cbBSHole.Checked or cbBoltHoles.Checked then
    begin
      Pen.Color:= clGreen;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FEnd2[R]) do
          if ((cbEnd.Checked and (RecData(Self).FEnd2[R, I].Typ = 2)) or
              (cbBSHole.Checked and (RecData(Self).FEnd2[R, I].Typ = 1)) or
              (cbBoltHoles.Checked and (RecData(Self).FEnd2[R, I].Typ = 0))) then
          begin
            if CheckBox47.Checked then Flg:= GetScrPos3(R, RecData(Self).FEnd2[R, I].Range.StCrd, RecData(Self).FEnd2[R, I].Range.EdCrd, 2, 3, Rt)
                                  else Flg:= GetScrPos3(R, GetCentr(RecData(Self).FEnd2[R, I].Range), GetCentr(RecData(Self).FEnd2[R, I].Range), 2, 3, Rt);

            if Flg then
            begin
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);

              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);

              S:= Format('%d', [RecData(Self).FEnd2[R, I].Typ]);
              TextOut((Rt.Left + Rt.Right) div 2 - TextWidth(S) div 2, Rt.Top + RecData(Self).FEnd2[R, I].Typ * 14 + 10, S);
            end;


      {
//          S:= IntToStr(Ord(RecData(Self).FEnd2[R, I].Hole));
//          TextOut(Rt.Left, Rt.Top + 10, S);
//          S:= Format('%d ' + RecData(Self).FEnd2[R, I].Debug, [RecData(Self).FEnd2[R, I].MaxVal]);
            if (I <> 0) and (GetScrPos3(R, RecData(Self).FEnd2[R, I].Crd, RecData(Self).FEnd2[R, I].Crd, 2, 3, Rt)) then
            begin
//            S:= Format(' %3f', [(RecData(Self).FEnd2[R, I].Crd - RecData(Self).FEnd2[R, I - 1].Crd) * FScanStep]);
              S:= Format('%d', []);
              TextOut((Rt.Left + Rt.Right) div 2 - TextWidth(S) div 2, Rt.Top + RecData(Self).FEnd2[R, I].Typ * 14 + 10, S);
            end; }
          end;
    end; *)
(*
    if CheckBox34.Checked then
    begin
      Pen.Color:= clGreen;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FEnd3[R]) do
          if GetScrPos3(R, RecData(Self).FEnd3[R, I].Crd, RecData(Self).FEnd3[R, I].Crd, 0, 1, Rt) then
          begin
            MoveTo(Rt.Left, Rt.Top);
            LineTo(Rt.Left, Rt.Bottom);
            S:= Format('Idx: %d; Val: %d', [I, RecData(Self).FEnd3[R, I].Value]);
//            S:= Format('Idx: %d; Val: %d; Ok: %d; Width Ok %d', [I, RecData(Self).FEnd3[R, I].Value,  {Ord(RecData(Self).FEnd3[R, I].Ok),} Ord(RecData(Self).FEnd3[R, I].WidthOk)]);
            TextOut((Rt.Left + Rt.Right) div 2 - TextWidth(S) div 2, Rt.Top + 10, S);
          end;
    end;
*)
// ---------------- ��������������� ������� - 2 --------------------------------

    if ConstCheck.Checked[0] or
       ConstCheck.Checked[1] or
       ConstCheck.Checked[2] or
       ConstCheck.Checked[3] or
       ConstCheck.Checked[4] or
       ConstCheck.Checked[5] or
       ConstCheck.Checked[6] or
       ConstCheck.Checked[7] or
       ConstCheck.Checked[8] then
    begin
      Pen.Width:= 2;
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FConstruct[R]) do

          if ConstCheck.Checked[0] or
            (ConstCheck.Checked[1] and (RecData(Self).FConstruct[R, I].Typ =     ct_OneHole)) or
            (ConstCheck.Checked[2] and (RecData(Self).FConstruct[R, I].Typ =        ct_Plug)) or
            (ConstCheck.Checked[3] and (RecData(Self).FConstruct[R, I].Typ = ct_WeldedJoint)) or
            (ConstCheck.Checked[4] and (RecData(Self).FConstruct[R, I].Typ =   ct_BoltJoint)) or
            (ConstCheck.Checked[5] and (RecData(Self).FConstruct[R, I].Typ =  ct_FlangeRail)) or
            (ConstCheck.Checked[6] and (RecData(Self).FConstruct[R, I].Typ =       ct_Blade)) or
            (ConstCheck.Checked[7] and (RecData(Self).FConstruct[R, I].Typ =        ct_Frog)) or
            (ConstCheck.Checked[8] and (RecData(Self).FConstruct[R, I].Typ = ct_SupportingRail)) then

        begin
          Pen.Color:= clBlack;
          if (RecData(Self).FConstruct[R, I].Typ = ct_BoltJoint) and
             (GetScrPos3(R, RecData(Self).FConstruct[R, I].Head.StCrd, RecData(Self).FConstruct[R, I].Head.EdCrd, 0, 1, Rt)) then
          begin
            MoveTo(Rt.Left, Rt.Top);
            LineTo(Rt.Left, Rt.Bottom);
            MoveTo(Rt.Right, Rt.Top);
            LineTo(Rt.Right, Rt.Bottom);
            Brush.Color:= $008AFFC5;
            FillRect(Rect(Rt.Left, Rt.Top, Rt.Right, Rt.Bottom));
            TextOut(Rt.Left + 3, Rt.Top + 2, Format('%d', [I]));
          end;

          if (GetScrPos3(R, RecData(Self).FConstruct[R, I].Body.StCrd, RecData(Self).FConstruct[R, I].Body.EdCrd, 2, 3, Rt)) then
          begin
            MoveTo(Rt.Left, Rt.Top);
            LineTo(Rt.Left, Rt.Bottom);
            MoveTo(Rt.Right, Rt.Top);
            LineTo(Rt.Right, Rt.Bottom);
            Brush.Color:= $008AFFC5;
            FillRect(Rect(Rt.Left, Rt.Top, Rt.Right, Rt.Bottom));
            case RecData(Self).FConstruct[R, I].Typ of
              ct_OneHole: S:= '��������� ���������';
                 ct_Plug: S:= '��������';
          ct_WeldedJoint: S:= '������� ����';
            ct_BoltJoint: S:= '�������� ����';
           ct_FlangeRail: S:= '����������';
                ct_Blade: S:= '������ ����������� ��������';
                 ct_Frog: S:= '����������';
       ct_SupportingRail: S:= '������ �����';
            end;
            TextOut(Rt.Left + 3, Rt.Top + 2, IntToStr(I) + ' - ' + S);

            S:= '';
//            S:= S + Format('���-%d; ', [RecData(Self).FConstruct[R, I].TypCnt[so_BoltHole]]);
//            S:= S + Format('���-%d; ', [RecData(Self).FConstruct[R, I].TypCnt[so_End]]);
//            S:= S + Format('���-%d; ', [RecData(Self).FConstruct[R, I].TypCnt[so_BSHole]]);
//            S:= S + Format('���-%d; ', [RecData(Self).FConstruct[R, I].TypCnt[so_Blade]]);
            TextOut(Rt.Left + 3, Rt.Top + 22, S);

            S:= '';
            for J:= 0 to High(RecData(Self).FConstruct[R, I].Idx) do
              S:= S + Format('%d ', [RecData(Self).FConstruct[R, I].Idx[J]]);
            TextOut(Rt.Left + 3, Rt.Top + 42, S);

//            S:= S + Format('Flg-%d', [Ord(RecData(Self).FRailObj[R, I].Flg)]);
//            for OT:= Low(TSingleObjTyp) to High(TSingleObjTyp) do
//            S:= S + Format('%d - %d; ', [Ord(OT), RecData(Self).FRailObj[R, I].TypCount[OT]]);
          end;
        end;
    end;

// ---------------- ��������������� ������� - �������������� �������� ------------
   (*
    if CheckBox9.Checked then
    begin
      Pen.Width:= 2;
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FRailObj[R]) do
        begin
          Pen.Color:= clBlack;
          for J:= 0 to High(RecData(Self).FRailObj[R, I].HeadZones) do
            if (GetScrPos3(R, RecData(Self).FRailObj[R, I].HeadZones[J].StCrd, RecData(Self).FRailObj[R, I].HeadZones[J].EdCrd, 0, 1, Rt)) then
            begin
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
              Brush.Color:= $008AFFC5;
              FillRect(Rect(Rt.Left, Rt.Top, Rt.Right, Rt.Bottom));
              TextOut(Rt.Left + 3, Rt.Top + 2, Format('%d.%d - %d', [I, J + 1, Ord(RecData(Self).FRailObj[R, I].F1)]));
          end;

          for J:= 0 to High(RecData(Self).FRailObj[R, I].BodyZones) do
            if (GetScrPos3(R, RecData(Self).FRailObj[R, I].BodyZones[J].Range.StCrd, RecData(Self).FRailObj[R, I].BodyZones[J].Range.EdCrd, 2, 3, Rt)) then
            begin

              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
              Brush.Color:= $008AFFC5;
              FillRect(Rect(Rt.Left, Rt.Top, Rt.Right, Rt.Bottom));
              TextOut(Rt.Left + 3, Rt.Top + 2, Format('%d.%d - %d', [I, J + 1, Ord(RecData(Self).FRailObj[R, I].F1)]));

              S:= '';
              for K:= 0 to High(RecData(Self).FRailObj[R, I].BodyZones[J].ObjList) do
                S:= S + IntToStr(RecData(Self).FRailObj[R, I].BodyZones[J].ObjList[K]) + ', ';
              TextOut(Rt.Left + 3, Rt.Top + 22, S);


        {
              S:= '';
              S:= S + Format('���-%d; ', [RecData(Self).FRailObj[R, I].TypCount[so_Hole]]);
              S:= S + Format('���-%d; ', [RecData(Self).FRailObj[R, I].TypCount[so_End]]);
              S:= S + Format('���-%d; ', [RecData(Self).FRailObj[R, I].TypCount[so_BSHole]]);
              S:= S + Format('���-%d; ', [RecData(Self).FRailObj[R, I].TypCount[so_Blade]]);
              S:= S + Format('Flg-%d', [Ord(RecData(Self).FRailObj[R, I].Flg)]);
        }
//              for OT:= Low(TSingleObjTyp) to High(TSingleObjTyp) do
//                S:= S + Format('%d - %d; ', [Ord(OT), RecData(Self).FRailObj[R, I].TypCount[OT]]);
        //      TextOut(Rt.Left + 3, Rt.Top + 22, S);
            end;
        end;
    end;

// ---------------- ������� ���� �� ������� -------------------------------------------------

    if CheckBox23.Checked then
    begin
      Pen.Width:= 2;
      Pen.Color:= clBlack;
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FHZone[R]) do
          for J:= 0 to High(RecData(Self).FHZone[R, I].Items) do
          begin
            if (GetScrPos3(R, RecData(Self).FHZone[R, I].Items[J].StCrd, RecData(Self).FHZone[R, I].Items[J].EdCrd, 0, 0, Rt)) then
            begin
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
              Brush.Color:= $008AFFC5;
              FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
              TextOut(Rt.Left + 3, Rt.Top + 2, Format('%d - %d', [I, J + 1]));
            end;
            if (GetScrPos3(R, RecData(Self).FHZone[R, I].Items[J].StCrd, RecData(Self).FHZone[R, I].Items[J].EdCrd, 1, 1, Rt)) then
            begin
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
              Brush.Color:= $008AFFC5;
              FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
              TextOut(Rt.Left + 3, Rt.Top + 2, Format('%d - %d', [I, J + 1]));
            end;
          end;
    end;

    if CheckBox8.Checked then
    begin
      Pen.Width:= 2;
      Pen.Color:= clBlack;
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FHZone[R]) do
          for J:= 0 to High(RecData(Self).FHZone[R, I].Items) do
            if (GetScrPos3(R, RecData(Self).FHZone[R, I].Items[J].StCrd, RecData(Self).FHZone[R, I].Items[J].EdCrd, 0, 1, Rt)) then
            begin
              Font.Height:= 40;
              TextOut((Rt.Left + Rt.Right) div 2 - TextWidth(Format('%d', [I])) div 2,
                      (Rt.Top + Rt.Bottom) div 2 - TextHeight(Format('%d', [I])) div 2, Format('%d', [I]));
            end;
    end;

// ---------------- ������� ���� �� ����� -------------------------------------------------

    if CheckBox23.Checked then
    begin
      Pen.Width:= 2;
      Pen.Color:= clBlack;
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FBZone[R]) do
          for J:= 0 to High(RecData(Self).FBZone[R, I].Items) do
          begin
            if (GetScrPos3(R, RecData(Self).FBZone[R, I].Items[J].StCrd, RecData(Self).FBZone[R, I].Items[J].EdCrd, 2, 2, Rt)) then
            begin
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
              Brush.Color:= $008AFFC5;
              FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
              Font.Size:= 8;
              TextOut(Rt.Left + 3, Rt.Top + 2, Format('%d - %d', [I, J + 1]));
            end;
            if (GetScrPos3(R, RecData(Self).FBZone[R, I].Items[J].StCrd, RecData(Self).FBZone[R, I].Items[J].EdCrd, 3, 3, Rt)) then
            begin
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
              Brush.Color:= $008AFFC5;
              FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
            end;
          end;
    end;

// ---------------- ������ ������� ���  -------------------------------------------------

    if CheckBox8.Checked then
    begin
      Pen.Width:= 2;
      Pen.Color:= clBlack;
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FBZone[R]) do
          for J:= 0 to High(RecData(Self).FBZone[R, I].Items) do
            if (GetScrPos3(R, RecData(Self).FBZone[R, I].Items[J].StCrd, RecData(Self).FBZone[R, I].Items[J].EdCrd, 2, 3, Rt)) then
            begin
              Font.Height:= 40;
              TextOut((Rt.Left + Rt.Right) div 2 - TextWidth(Format('%d', [I])) div 2,
                      (Rt.Top + Rt.Bottom) div 2 - TextHeight(Format('%d', [I])) div 2, Format('%d', [I]));
            end;
    end;
    Font.Size:= 8;
     *)
// ---------------- ��������� ����������  -------------------------------------------------

    if CheckBox10.Checked or
       CheckBox30.Checked then
    begin
      Pen.Width:= 2;
      Pen.Color:= clBlack;
      Font.Size:= 8;
      for I:= 0 to High(RecData(Self).TechnoError) do
        if (CheckBox10.Checked and (RecData(Self).TechnoError[I].Typ = teDoublePass)) or
           (CheckBox30.Checked and (RecData(Self).TechnoError[I].Typ = teBJButton)) then
        begin
          case RecData(Self).TechnoError[I].R of
            r_Left: begin
                      StR:= r_Left;
                      EdR:= r_Left;
                    end;
            r_Right:begin
                      StR:= r_Right;
                      EdR:= r_Right;
                    end;
            r_Both: begin
                      StR:= r_Left;
                      EdR:= r_Right;
                    end;
          end;

          for R:= StR to EdR do
            if (GetScrPos3(R, RecData(Self).TechnoError[I].Range.StCrd, RecData(Self).TechnoError[I].Range.EdCrd, 0, 3, Rt)) then
            begin
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
              Brush.Color:= $00C0BCFA;
              FillRect(Rect(Rt.Left, Rt.Top + 1, Rt.Right, Rt.Bottom - 1));
              Brush.Style:= bsClear;
              case RecData(Self).TechnoError[I].Typ of
                teDoublePass: TextOut(Rt.Right + 5, Rt.Top, '������� ������');
                  teBJButton: TextOut(Rt.Right + 5, Rt.Top + 16, '������ ��');
              end;
            end;
        end;
    end;

// ---------------- ���� ������� �� ��������� ������� --------------------------
  {
    if CheckBox37.Checked then
    begin
      Pen.Width:= 2;
      Pen.Color:= clGreen;
      for R:= r_Left to r_Right do
      begin
        Flg:= False;
        for I:= FStDrawCrd to FEdDrawCrd do
          if GetScrPos(R, 6, I, RecData(Self).GetBtmDelay(R, I), False, Pt, OutOfRange) then
          begin
            if not Flg then
            begin
              MoveTo(Pt.X, Pt.Y);
              Flg:= True;
            end else LineTo(Pt.X, Pt.Y);
          end;
      end;
    end;
  }
{        for I:= 0 to High(RecData(Self).FBtmPtList[R]) do
          if GetScrPos(R, 6, RecData(Self).FBtmPtList[R, I].Crd, RecData(Self).FBtmPtList[R, I].Delay, False, Pt, OutOfRange) then
          begin
            MoveTo(Pt.X - 100, Pt.Y);
            LineTo(Pt.X + 100, Pt.Y);
          end;
}

// ---------------- ������ -----------------------------------------------------
   {
    if BSCheck.Checked[11] then
    begin
      Pen.Width:= 2;
      Brush.Style:= bsClear;

      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FBlades[R]) do
          if GetScrPos3(R, RecData(Self).FBlades[R, I].StCrd, RecData(Self).FBlades[R, I].EdCrd, 2, 3, Rt) then
          begin
            Brush.Color:= clLime;
            FillRect(Rect(Rt.Left, Rt.Top, Rt.Right + 50, Rt.Bottom));
          end;
    end; }

// ---------------- ��������� ����� �������������� ��������� -------------------
    (*
    if ShowBJ.Checked or
       BHCheck.Checked[0] or
       BHCheck.Checked[1] or
       BHCheck.Checked[2] or
       BHCheck.Checked[3] or
       BHCheck.Checked[4] then
    begin
      Pen.Width:= 2;
      Brush.Style:= bsClear;

      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FSingleObj[R]) do
        begin

         // (so_Hole, so_End, so_BSHole, so_Blade)

          if ((ShowBJ.Checked or BHCheck.Checked[1]) and (RecData(Self).FSingleObj[R, I].Typ in [so_BHOk, so_BHUnCtrl, so_BHDef]))  or
              (BHCheck.Checked[2] and (RecData(Self).FSingleObj[R, I].Typ = so_End))  or
              (BHCheck.Checked[3] and (RecData(Self).FSingleObj[R, I].Typ = so_BSHole)) or
              (BHCheck.Checked[4] and (RecData(Self).FSingleObj[R, I].Typ = so_Blade)) { or
             (BHCheck.Checked[5] and (RecData(Self).FSingleObj[R, I].Typ = 4)) }  then

          begin
            if GetScrPos3(R, RecData(Self).FSingleObj[R, I].DisRange.StCrd, RecData(Self).FSingleObj[R, I].DisRange.EdCrd, 2, 3, Rt) then
            begin
      //        S:= IntToStr(RecData(Self).FSingleObj[R, I].Typ);
      //        TextOut(X1 - TextWidth(S) div 2, Y1 + 10, S);
      {        MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom); }

//              J:= Random(8) - 16;
//              DrawSel(PaintBox.Canvas, Rt.Left, Rt.Right, Rt.Top + J, Rt.Bottom + J, 5);


              case RecData(Self).FSingleObj[R, I].Typ of
                so_BHOk: Brush.Color:= clLime;
            so_BHUnCtrl: Brush.Color:= clYellow;
               so_BHDef: Brush.Color:= clRed;
              end;

              Brush.Color:= clRed;
              FillRect(Rect(Rt.Left, Rt.Top, Rt.Right + 50, Rt.Bottom));

//              RecData(Self).FSingleObj[R, I].Name

           //   TextOut(Rt.Left, Rt.Bottom - 12, IntToStr(I));
           //   TextOut(Rt.Left, Rt.Top + 12, RecData(Self).FSingleObj[R, I].Name);
            end;
          end;

                    (*
          if BHCheck.Checked[2] and (RecData(Self).FSingleObj[R, I].Typ = so_End) then // �����
          begin
            Pen.Color:= clGreen;
            if GetScrPos3(R, RecData(Self).FSingleObj[R, I].DisRange.StCrd, RecData(Self).FSingleObj[R, I].DisRange.EdCrd, 0, 2, Rt) then
            begin
              DrawSel(PaintBox.Canvas, Rt.Left, Rt.Right, Rt.Top, Rt.Bottom - Abs(Rt.Top - Rt.Bottom) div 6, 5);
              TextOut(Rt.Left + 3, Rt.Bottom - 12, IntToStr(I));
            end;
          end;


          if {(BHCheck.Checked[2] and (RecData(Self).FSingleObj[R, I].Typ = 1)) or}
            // (BHCheck.Checked[3] and (RecData(Self).FSingleObj[R, I].Typ = so_End)) or
             (BHCheck.Checked[3] and (RecData(Self).FSingleObj[R, I].Typ = so_BSHole)) or
             (BHCheck.Checked[4] and (RecData(Self).FSingleObj[R, I].Typ = so_Blade)) then
          begin
            Pen.Color:= clGreen;
            if GetScrPos3(R, GetCentr(RecData(Self).FSingleObj[R, I].DisRange), GetCentr(RecData(Self).FSingleObj[R, I].DisRange), 2, 3, Rt) then
            begin
           //   S:= IntToStr(Ord(RecData(Self).FSingleObj[R, I].Typ));
           //   TextOut(X1 - TextWidth(S) div 2, Y1 + 10, S);
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
            end;
          end; * )
        end;
    end;

    Log.Add(Format('%d - ���������', [GetTickCount - T])); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    *)
    T:= GetTickCount;

// ---------------- ��������� �������� -----------------------------------------

    Min_:= maxint;
    Max_:= 0;

    if Assigned(RecData(Self).Filtr) then RecData(Self).Filtr.StartFilter(Max(FStCrd, FStDrawCrd - 200 * Ord(FRec <> 0)));

//    if CheckBox45.Checked then FillChar(RecData(Self).FFilter1[r_Left, 0, 0], 2 * 8 * 256 * 4, 0);
//    if CheckBox46.Checked then FillChar(RecData(Self).FFilter2[r_Left, 0, 0], 2 * 8 * 256 * 4, 0);

    FEchoSizeIdx:= cbEchoSize.ItemIndex;
    FDatSrc.LoadData(Max(FStCrd, FStDrawCrd - 200 * Ord(FRec <> 0)), Min(FEdCrd, FEdDrawCrd + 200 * Ord(FRec <> 0)), 0, BlockOK_Echo);
    Self.Caption:= Format('%d-%d', [Min_, Max_]);

    Log.Add(Format('%d - �������', [GetTickCount - T])); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T:= GetTickCount;

// ---------------- ���� �� ----------------------------------------------------

    if BSCheck.Checked[0] then
    begin
      Pen.Color:= clRed;
      Pen.Width:= 2;
      for R:= r_Left to r_Right do
      begin
        Flg:= False;
        for I:= 0 to High(RecData(Self).FBSignal.FBSPos[R]) do
        begin
          GetScrPosNoRed(R, 1, RecData(Self).FBSignal.FBSPos[R, I].Crd, Round(RecData(Self).FBSignal.FBSPos[R, I].MinDelay {- 9}), False, NewTopPos, OutOfRange);
          GetScrPosNoRed(R, 1, RecData(Self).FBSignal.FBSPos[R, I].Crd, Round(RecData(Self).FBSignal.FBSPos[R, I].MaxDelay {+ 9}), False, NewBtmPos, OutOfRange);
          GetScrPosNoRed(R, 6, RecData(Self).FBSignal.FBSPos[R, I].Crd, Round(105 + (RecData(Self).FBSignal.FBSPos[R, I].MinDelay / 3 - 49) * 23 / 10), False, NewTopPos_, OutOfRange);

          if Flg then
          begin
            Brush.Style:= bsClear;

            if BSCheck.Checked[4] then
            //  if Odd(I) then
                with TopPos do
                  TextOut(X + 3, Y + (1 - 2 * Ord(Odd(I))) * 14, IntToStr(I));

            Brush.Color:= clBlack;
            if BSCheck.Checked[1] then with TopPos do FillRect(Rect(X - 2, Y - 2, X + 2, Y + 2));
            MoveTo(TopPos.X, TopPos.Y);
            LineTo(NewTopPos.X, NewTopPos.Y);
            if BSCheck.Checked[1] then with BtmPos do FillRect(Rect(X - 2, Y - 2, X + 2, Y + 2));
            MoveTo(BtmPos.X, BtmPos.Y);
            LineTo(NewBtmPos.X, NewBtmPos.Y);
          end;
          Flg:= True;
          TopPos:= NewTopPos;
          BtmPos:= NewBtmPos;
          TopPos_:= NewTopPos_;
        end;
      end;
    end;

    if BSCheck.Checked[10] then
    begin
      Pen.Color:= clBlack;
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
      begin
        for I:= 0 to High(RecData(Self).FBSignal.FDebugGis[R]) do
          if ((FStDrawCrd + FEdDrawCrd) div 2 >= RecData(Self).FBSignal.FDebugGis[R, I].Range.StCrd) and
             ((FStDrawCrd + FEdDrawCrd) div 2 <= RecData(Self).FBSignal.FDebugGis[R, I].Range.EdCrd) and
             GetScrPos3(R, RecData(Self).FBSignal.FDebugGis[R, I].Range.StCrd, RecData(Self).FBSignal.FDebugGis[R, I].Range.EdCrd, 3, 3, Rt) then
          begin
            Rectangle(Rect(Rt.Left, Rt.Top, Rt.Right, Rt.Bottom));
            TextOut((Rt.Left + Rt.Right) div 2 - TextWidth(IntToStr(I)) div 2,
                     Rt.Top + J + 10 - TextHeight(IntToStr(I)) div 2, IntToStr(I));
//            Inc(J, 20);
          end;
      end;
    end;

    Log.Add(Format('%d - ���� ��', [GetTickCount - T])); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T:= GetTickCount;


// ---------------- ������� � �� ------------------------------------------------

    if BSCheck.Checked[5] then
    begin
      Pen.Width:= 2;
//      Brush.Color:= clBlack;
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FBSPice[R]) do
          begin
            if GetScrPos3(R, RecData(Self).FBSPice[R, I].R.StCrd, RecData(Self).FBSPice[R, I].R.EdCrd, 3, 3, Rt) then
            begin
              if RecData(Self).FBSPice[R, I].BM then Pen.Color:= clRed
                                           else Pen.Color:= clGreen;

              DrawSel(PaintBox.Canvas, Rt.Left, Rt.Right, Rt.Top, Rt.Bottom, 5);
              if BSCheck.Checked[4] then
                TextOut(Rt.Left, Rt.Top, IntToStr(I));
            end;
          end;
    end;

// ---------------- ����� � �� -------------------------------------------------

    if BSCheck.Checked[2] or
       BSCheck.Checked[6] or
       BSCheck.Checked[7] or
       BSCheck.Checked[8] or
       BSCheck.Checked[9] then
    begin
      Pen.Color:= clGreen;
      Pen.Width:= 2;
//      Brush.Color:= clBlack;
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FPBList[R, 0]) do
          if ((not BSCheck.Checked[6]) and (not BSCheck.Checked[7]) and (not BSCheck.Checked[8]) and (not BSCheck.Checked[9])) or
             (BSCheck.Checked[6] and (not (pbtiInBMZ in RecData(Self).FPBList[R, 0, I].Typ))) or
             (BSCheck.Checked[7] and (pbtiInBMZ in RecData(Self).FPBList[R, 0, I].Typ)) or
             (BSCheck.Checked[8] and (not (pbtiReCheck in RecData(Self).FPBList[R, 0, I].Typ))) or
             (BSCheck.Checked[9] and (pbtiReCheck in RecData(Self).FPBList[R, 0, I].Typ)) then
          begin
            if GetScrPos3(R, RecData(Self).FPBList[R, 0, I].Crd.StCrd, RecData(Self).FPBList[R, 0, I].Crd.EdCrd, 3, 3, Rt) then
            begin
              if pbtiInBMZ in RecData(Self).FPBList[R, 0, I].Typ then Pen.Color:= clRed
                                                                 else Pen.Color:= clGreen;

              DrawSel(PaintBox.Canvas, Rt.Left + 2, Rt.Right - 2, Rt.Top, Rt.Bottom, 5);
              if BSCheck.Checked[4] then
                TextOut(Rt.Left, Rt.Top, IntToStr(I));
//              if RecData(Self).FPBList[R, 0, I].Dat[4] = 1 then
//                TextOut(Rt.Left, Rt.Top, '[+]');
            end;
          end;
    end;

    Log.Add(Format('%d - ����� � ��', [GetTickCount - T])); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T:= GetTickCount;

// ---------------- ����� ------------------------------------------------------

    for R:= r_Left to r_Right do                    // ������� ������ ��������������� - �����
      for Ch:= 0 to 7 do
      begin
        for I:= 0 to High(FOnScrPB[R, Ch]) do
          SetLength(FOnScrPB[R, Ch, I].PtList, 0);
        SetLength(FOnScrPB[R, Ch], 0);
      end;

    if PBCheck.Checked[0] or                           // ������ � �����
       PBCheck.Checked[1] or
       PBCheck.Checked[2] or
       PBCheck.Checked[3] or
       PBCheck.Checked[4] or
       PBCheck.Checked[5] or
       PBCheck.Checked[6] or
       PBCheck.Checked[7] or
       PBCheck.Checked[8] then
    begin
      for R:= r_Left to r_Right do
        for Ch:= 0 to 7 do
          if (((Ch in [0, 2, 4, 6]) and cbZoomIn.Checked) or
              ((Ch in [1, 3, 5, 7]) and cbZoomOut.Checked)) then

          for I:= 0 to High(RecData(Self).FPBList[R, Ch]) do
          begin
            if (RecData(Self).FPBList[R, Ch, I].Crd {SwedCrd} .StCrd - FStDrawCrd) / FXZoom >= Width + 250 then Break;
            if (RecData(Self).FPBList[R, Ch, I].Crd {SwedCrd} .EdCrd - FStDrawCrd) / FXZoom >= 0 then
              with RecData(Self).FPBList[R, Ch, I] do

                if (PBCheck.Checked[0] and not (pbtiDel   in Typ)) or     // ��� �����
                   (PBCheck.Checked[1] and (pbtiInBMZ     in Typ)) or
                   (PBCheck.Checked[2] and (pbtiConstruct in Typ)) or
                   (PBCheck.Checked[3] and (pbtiClear     in Typ)) or
                   (PBCheck.Checked[4] and (pbtiNotLast   in Typ)) or
                   (PBCheck.Checked[5] and (pbtiReCheck   in Typ)) or
                   (PBCheck.Checked[6] and (pbtiUCS       in Typ)) or
                   (PBCheck.Checked[7] and (pbtiDefect    in Typ)) or
                   (PBCheck.Checked[8] and (pbtiDel       in Typ)) then

                begin
                                                     // �������������� ���� �����
                  if CheckBox29.Checked and (Ch <> 0) then
                  begin
                    Brush.Color:= clBlack;
                    DL:= GetLen(RecData(Self).FPBList[R, Ch, I].Crd);
                    if DL < 4 then // ���� ����� ������� �������� ������ �� ������ � �����
                    begin
                      DH:= RecData(Self).FPBList[R, Ch, I].Del.EdCrd - RecData(Self).FPBList[R, Ch, I].Del.StCrd;
                      for X:= 0 to DL do
                      begin
                        D:= RecData(Self).FPBList[R, Ch, I].Del.StCrd + Round(X * DH / DL);
                        if GetScrPos(R, Ch, RecData(Self).FPBList[R, Ch, I].Crd.StCrd + X, D - 2, False, Pt, OutOfRange) then
                          FillRect(Rect(Pt.X - 1, Pt.Y - 1, Pt.X + 1, Pt.Y + 1));
                        if GetScrPos(R, Ch, RecData(Self).FPBList[R, Ch, I].Crd.StCrd + X, D + 2, False, Pt, OutOfRange) then
                          FillRect(Rect(Pt.X - 1, Pt.Y - 1, Pt.X + 1, Pt.Y + 1));
                      end;
                    end
                    else           // ���� ����� ������� �������� �� 3-� ������
                    for X:= RecData(Self).FPBList[R, Ch, I].Crd.StCrd to RecData(Self).FPBList[R, Ch, I].Crd.EdCrd do
                    begin
                      if X <= RecData(Self).FPBList[R, Ch, I].CCrd then // �������� � ������ ��������
                      begin
                        DL:= RecData(Self).FPBList[R, Ch, I].CCrd - RecData(Self).FPBList[R, Ch, I].Crd.StCrd;
                        DH:= RecData(Self).FPBList[R, Ch, I].CDel - RecData(Self).FPBList[R, Ch, I].Del.StCrd;
                        XX:= X - RecData(Self).FPBList[R, Ch, I].Crd.StCrd;
                        D:= RecData(Self).FPBList[R, Ch, I].Del.StCrd + Round(XX * DH / DL);
                      end
                      else                                           // �������� �� ������ ��������
                      begin
                        DL:= RecData(Self).FPBList[R, Ch, I].Crd.EdCrd - RecData(Self).FPBList[R, Ch, I].CCrd;
                        DH:= RecData(Self).FPBList[R, Ch, I].Del.EdCrd - RecData(Self).FPBList[R, Ch, I].CDel;
                        XX:= X - RecData(Self).FPBList[R, Ch, I].CCrd;
                        D:= RecData(Self).FPBList[R, Ch, I].CDel + Round(XX * DH / DL);
                      end;
                      if GetScrPos(R, Ch, X, D - 2, False, Pt, OutOfRange) then
                        FillRect(Rect(Pt.X - 1, Pt.Y - 1, Pt.X + 1, Pt.Y + 1));
                      if GetScrPos(R, Ch, X, D + 2, False, Pt, OutOfRange) then
                        FillRect(Rect(Pt.X - 1, Pt.Y - 1, Pt.X + 1, Pt.Y + 1));
                    end;
                                                     // ��������� ����������� �����
                    if GetScrPos(R, Ch, RecData(Self).FPBList[R, Ch, I].CCrd, RecData(Self).FPBList[R, Ch, I].CDel, False, Pt, OutOfRange) then
                    begin
                      Pen.Color:= clWhite;
                      Pen.Width:= 3;
                      MoveTo(Pt.X - 5, Pt.Y);
                      LineTo(Pt.X + 5, Pt.Y);
                      MoveTo(Pt.X, Pt.Y - 5);
                      LineTo(Pt.X, Pt.Y + 5);
                      Pen.Color:= clBlack;
                      Pen.Width:= 1;
                      MoveTo(Pt.X - 5, Pt.Y);
                      LineTo(Pt.X + 5, Pt.Y);
                      MoveTo(Pt.X, Pt.Y - 5);
                      LineTo(Pt.X, Pt.Y + 5);
                    end;
                  end
                  else
                  begin
                    X1:= Length(FOnScrPB[R, Ch]);
                    SetLength(FOnScrPB[R, Ch], X1 + 1);
                    FOnScrPB[R, Ch, X1].PBIdx:= I;

                    for J:= 0 to High(RecData(Self).FPBList[R, Ch, I].Items) do
                      if GetScrPos(R, Ch, RecData(Self).FPBList[R, Ch, I].Items[J].Crd, RecData(Self).FPBList[R, Ch, I].Items[J].Delay, False, Pt, OutOfRange) then
                      begin
                        X2:= Length(FOnScrPB[R, Ch, X1].PtList);
                        SetLength(FOnScrPB[R, Ch, X1].PtList, X2 + 1);
                        FOnScrPB[R, Ch, X1].PtList[X2]:= Pt;
                        Inc(FOnScrPB[R, Ch, X1].PtList[X2].Y, 4);
                        if J = 0 then Dec(FOnScrPB[R, Ch, X1].PtList[X2].X, 3);
                        if J = High(RecData(Self).FPBList[R, Ch, I].Items) then Inc(FOnScrPB[R, Ch, X1].PtList[X2].X, 3);
                      end;

                    for J:= High(RecData(Self).FPBList[R, Ch, I].Items) downto 0 do
                      if GetScrPos(R, Ch, RecData(Self).FPBList[R, Ch, I].Items[J].Crd, RecData(Self).FPBList[R, Ch, I].Items[J].Delay, False, Pt, OutOfRange) then
                      begin
                        X2:= Length(FOnScrPB[R, Ch, X1].PtList);
                        SetLength(FOnScrPB[R, Ch, X1].PtList, X2 + 1);
                        FOnScrPB[R, Ch, X1].PtList[X2]:= Pt;
                        Dec(FOnScrPB[R, Ch, X1].PtList[X2].Y, 4);
                        if J = 0 then Dec(FOnScrPB[R, Ch, X1].PtList[X2].X, 3);
                        if J = High(RecData(Self).FPBList[R, Ch, I].Items) then Inc(FOnScrPB[R, Ch, X1].PtList[X2].X, 3);
                      end;


{                    for J:= 0 to High(RecData(Self).FPBList[R, Ch, I].Items) do
                      if GetScrPos(R, Ch, RecData(Self).FPBList[R, Ch, I].Items[J].Crd, RecData(Self).FPBList[R, Ch, I].Items[J].StDelay, False, Pt, OutOfRange) then
                      begin
                        X2:= Length(FOnScrPB[R, Ch, X1].PtList);
                        SetLength(FOnScrPB[R, Ch, X1].PtList, X2 + 1);
                        FOnScrPB[R, Ch, X1].PtList[X2]:= Pt;
                        Inc(FOnScrPB[R, Ch, X1].PtList[X2].Y, 2);
                        if J = 0 then Dec(FOnScrPB[R, Ch, X1].PtList[X2].X, 3);
                        if J = High(RecData(Self).FPBList[R, Ch, I].Items) then Inc(FOnScrPB[R, Ch, X1].PtList[X2].X, 3);
                      end;

                    for J:= High(RecData(Self).FPBList[R, Ch, I].Items) downto 0 do
                      if GetScrPos(R, Ch, RecData(Self).FPBList[R, Ch, I].Items[J].Crd, RecData(Self).FPBList[R, Ch, I].Items[J].EdDelay, False, Pt, OutOfRange) then
                      begin
                        X2:= Length(FOnScrPB[R, Ch, X1].PtList);
                        SetLength(FOnScrPB[R, Ch, X1].PtList, X2 + 1);
                        FOnScrPB[R, Ch, X1].PtList[X2]:= Pt;
                        Dec(FOnScrPB[R, Ch, X1].PtList[X2].Y, 2);
                        if J = 0 then Dec(FOnScrPB[R, Ch, X1].PtList[X2].X, 3);
                        if J = High(RecData(Self).FPBList[R, Ch, I].Items) then Inc(FOnScrPB[R, Ch, X1].PtList[X2].X, 3);
                      end; }
                  end;
                end;
          end;
    end;

    Font.Name:= 'Arial';                            // ������ �� ������ �� �����
    Font.Height:= 12;
    Brush.Style:= bsClear;
    Pen.Width:= 2;
    for R:= r_Left to r_Right do
      for Ch:= 0 to 7 do
        for I:= 0 to High(FOnScrPB[R, Ch]) do
        begin
          Pen.Color:= clGreen;
          Pen.Width:= 2;
          Polygon(FOnScrPB[R, Ch, I].PtList);
       {
          TextOut(FOnScrPB[R, Ch, I].PtList[0].X,
                  FOnScrPB[R, Ch, I].PtList[0].Y - 18, Format('%d', [RecData(Self).FPBList[R, Ch, FOnScrPB[R, Ch, I].PBIdx].Dat[7]]));
       }
          if (Ch <> 0) and (CheckBox16.Checked or CheckBox17.Checked or CheckBox39.Checked or CheckBox41.Checked) then
          begin
//            J:= 20 - Random(40);
            Pen.Color:= clBlack;
            Pen.Width:= 1;

            if High(FOnScrPB[R, Ch, I].PtList) = - 1 then continue;

            Pt1:= FOnScrPB[R, Ch, I].PtList[High(FOnScrPB[R, Ch, I].PtList)];
            Pt2:= Pt1;
//            if Pt1.Y > Abs(FOutLink[R, Ch].Rt.Top + FOutLink[R, Ch].Rt.Bottom) div 2
            if Odd(I)
              then Pt2.Y:= FOutLink[R, Ch].Rt.Bottom - 15
              else Pt2.Y:= FOutLink[R, Ch].Rt.Top + 15;

            MoveTo(Pt1.X, Pt1.Y);
            LineTo(Pt2.X, Pt2.Y);
{
            if CheckBox16.Checked then
              TextOut(PenPos.X + 2, PenPos.Y + 2, Format('%1.2f/%1.2f/%1.2f', [RecData(Self).FPBList[R, Ch, FOnScrPB[R, Ch, I].PBIdx].KfDelay,
                                                                               RecData(Self).FPBList[R, Ch, FOnScrPB[R, Ch, I].PBIdx].KfAmpl,
                                                                               RecData(Self).FPBList[R, Ch, FOnScrPB[R, Ch, I].PBIdx].KfFill]));
}            if CheckBox17.Checked then
              TextOut(PenPos.X + 2, PenPos.Y + 2, Format('%d', [FOnScrPB[R, Ch, I].PBIdx]));
{
            if CheckBox41.Checked then
              TextOut(PenPos.X + 2, PenPos.Y + 2, RecData(Self).FPBList[R, Ch, FOnScrPB[R, Ch, I].PBIdx].Text);
}
            if CheckBox39.Checked and (Ch in [1, 6, 7]) then
            begin
              case Ch of
                1: HH:= Round(2.95 * RecData(Self).FPBList[R, Ch, FOnScrPB[R, Ch, I].PBIdx].CDel / 3);
             6, 7: HH:= Round(1.15 * RecData(Self).FPBList[R, Ch, FOnScrPB[R, Ch, I].PBIdx].CDel);
              end;
              TextOut(PenPos.X + 2, PenPos.Y + 2, Format('%d', [HH]));
            end;
          end;
        end;

    Log.Add(Format('%d - �����', [GetTickCount - T])); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T:= GetTickCount;

// ---------------- �������� ��� ������ ��������� ------------------------------
    (*
    if FResRect then
        for R:= r_Left to r_Right do
          if BJSign.Checked then
            for Idx:= 0 to High(RecData(Self).FPoints2[R]) do

              if  ((RecData(Self).FPoints2[R, Idx].Ch = 0) and CheckBox48.Checked) or
                  ((RecData(Self).FPoints2[R, Idx].Ch = 1) and CheckBox49.Checked) or
                  ((RecData(Self).FPoints2[R, Idx].Ch = 6) and CheckBox50.Checked) or
                  ((RecData(Self).FPoints2[R, Idx].Ch = 7) and CheckBox51.Checked) then

              begin
                X1:= Round((RecData(Self).FPoints2[R, Idx].Crd.StCrd - FStDrawCrd) / FXZoom);
                X2:= Round((RecData(Self).FPoints2[R, Idx].Crd.EdCrd - FStDrawCrd) / FXZoom);

                Y1:= FResRt[R].Top + Round(Abs(FResRt[R].Bottom - FResRt[R].Top) * RecData(Self).FPoints2[R, Idx].Depth.StCrd / 180);
                Y2:= FResRt[R].Top + Round(Abs(FResRt[R].Bottom - FResRt[R].Top) * RecData(Self).FPoints2[R, Idx].Depth.EdCrd / 180);

                case RecData(Self).FPoints2[R, Idx].Ch of
                  0: Pen.Color:= clBlack;
                  1: Pen.Color:= clGreen;
                  6: Pen.Color:= clRed;
                  7: Pen.Color:= clBlue;
                end;

                Brush.Style:= bsClear;
                Rectangle(Rect(X1, Y1, X2, Y2 + 1));
                Brush.Color:= clWhite;
                if BJSignIdx.Checked then TextOut(X1, Y1, IntToStr(Idx));
                if ShowBJIdx.Checked then TextOut(X1, Y1, IntToStr(RecData(Self).FPoints2[R, Idx].HoleIdx));
                if RecData(Self).FPoints2[R, Idx].Crd.EdCrd > FEdDrawCrd then Break;
              end;


    Brush.Style:= bsClear;
    Log.Add(Format('%d - ��������', [GetTickCount - T])); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T:= GetTickCount;
      *)

// ---------------- ��������������� ������� - ������������ �������� ------------
(*
    if CheckBox23.Checked then
    begin
      Pen.Width:= 2;
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FRailObj2[R]) do
        begin
          Pen.Color:= clBlack;

          if (RecData(Self).FRailObj2[R, I].HeadZones.StCrd <> - 1) and
             (RecData(Self).FRailObj2[R, I].HeadZones.EdCrd <> - 1) and
             (GetScrPos3(R, RecData(Self).FRailObj2[R, I].HeadZones.StCrd, RecData(Self).FRailObj2[R, I].HeadZones.EdCrd, 0, 1, Rt)) then
          begin
            MoveTo(Rt.Left, Rt.Top);
            LineTo(Rt.Left, Rt.Bottom);
            Brush.Color:= clWhite;
//            TextOut(Rt.Left, Rt.Top + RecData(Self).FRailObj2[R, I].Typ * 11, Format('Typ: %d' { Idx: %d'}, [RecData(Self).FRailObj2[R, I].Typ{, I}]));
            Brush.Style:= bsClear;
            MoveTo(Rt.Right, Rt.Top);
            LineTo(Rt.Right, Rt.Bottom);
          end;

          if GetScrPos3(R, RecData(Self).FRailObj2[R, I].BodyZones.StCrd, RecData(Self).FRailObj2[R, I].BodyZones.EdCrd, 2, 3, Rt) then
          begin
            MoveTo(Rt.Left, Rt.Top);
            LineTo(Rt.Left, Rt.Bottom);
            Brush.Color:= clWhite;
//            TextOut(Rt.Left, Rt.Top + 33, Format('Typ: %d; L:%d; R:%d', [RecData(Self).FRailObj2[R, I].Typ, RecData(Self).FRailObj2[R, I].LeftRail, RecData(Self).FRailObj2[R, I].RightRail]));
//            TextOut(Rt.Left, Rt.Top + RecData(Self).FRailObj2[R, I].Typ * 11, Format('Typ: %d; TC:%d; L[1]:%d; L[2]:%d' { Idx: %d'}, [RecData(Self).FRailObj2[R, I].Typ, RecData(Self).FRailObj2[R, I].TCount, RecData(Self).FRailObj2[R, I].TList[1], RecData(Self).FRailObj2[R, I].TList[2]]));
            Brush.Style:= bsClear;
            MoveTo(Rt.Right, Rt.Top);
            LineTo(Rt.Right, Rt.Bottom);
          end;
        end;
    end;
 *)
// ---------------- ������ -----------------------------------------------------
(*
    if CheckBox10.Checked then
    begin
      Brush.Color:= $00BCEDA5;
      Pen.Color:= clBlack;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FRails[R]) do
        begin
          for J:= 0 to High(RecData(Self).FRails[R, I].Body) do
            if GetScrPos3(R, RecData(Self).FRails[R, I].Body[J].StCrd, RecData(Self).FRails[R, I].Body[J].EdCrd, 2, 3, Rt) then
            begin
              FillRect(Rt);
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
            end;

          for J:= 0 to High(RecData(Self).FRails[R, I].Head) do
            if GetScrPos3(R, RecData(Self).FRails[R, I].Head[J].StCrd, RecData(Self).FRails[R, I].Head[J].EdCrd, 0, 1, Rt) then
            begin
              FillRect(Rt);
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
              if J = 0 then TextOut(Rt.Left + 3, Rt.Top + 3, Format('����� %d', [I]));
            end;
        end;
    end;

// ---------------- ������ ������� -----------------------------------------------------

    if CheckBox25.Checked then
    begin
      Brush.Color:= $00BCEDA5;
      Pen.Color:= clBlack;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).StrelkaBody[R]) do
        begin
          for J:= 0 to High(RecData(Self).StrelkaBody[R, I].Body) do
            if GetScrPos3(R, RecData(Self).StrelkaBody[R, I].Body[J].StCrd, RecData(Self).StrelkaBody[R, I].Body[J].EdCrd, 2, 3, Rt) then
            begin
              FillRect(Rt);
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
              TextOut(Rt.Left, Rt.Top + 33, Format('Idx:%d', [I]));
            end;
        end;
    end;
    *)
// ---------------- ��������������������� ������� ------------------------------

    if CheckBox15.Checked or CheckBox36.Checked then
    begin
      Pen.Color:= clGreen;
      Pen.Width:= 2;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FUnCtrlZ[R]) do
          if CheckBox15.Checked or (CheckBox36.Checked and (RecData(Self).FUnCtrlZ[R, I].Rng.StCrd <> - MaxInt)) then
          begin
            GetScrPos2(R, 1, RecData(Self).FUnCtrlZ[R, I].Rng.EdCrd, X1, Y1, Y2, True);
            GetScrPos2(R, 1, RecData(Self).FUnCtrlZ[R, I].Rng.EdCrd, X2, Y1, Y2, True);
            MoveTo(X1 + 5, Y1 + 5);
            LineTo(X1,     Y1 + 5);
            LineTo(X1,     Y2 - 5);
            LineTo(X1 + 5, Y2 - 5);
            MoveTo(X2 - 5, Y1 + 5);
            LineTo(X2,     Y1 + 5);
            LineTo(X2,     Y2 - 5);
            LineTo(X2 - 5, Y2 - 5);
          end;
    end;

    if CheckBox35.Checked then
    begin
      Pen.Color:= clGreen;
      Pen.Width:= 2;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FUnCtrlZ[R]) do
          begin
            GetScrPos2(R, 1, RecData(Self).FUnCtrlZ[R, I].Rng.StCrd, X1, Y1, Y2, True);
            GetScrPos2(R, 1, RecData(Self).FUnCtrlZ[R, I].Rng.EdCrd, X2, Y1, Y2, True);
            MoveTo(X1 + 5, Y1 + 5);
            LineTo(X1,     Y1 + 5);
            LineTo(X1,     Y2 - 5);
            LineTo(X1 + 5, Y2 - 5);
            MoveTo(X2 - 5, Y1 + 5);
            LineTo(X2,     Y1 + 5);
            LineTo(X2,     Y2 - 5);
            LineTo(X2 - 5, Y2 - 5);
          end;
    end;

// ---------------- ������� ������� --------------------------------------------
(*
    if CheckBox14.Checked then // ������� ������� - �� �����
    begin
      Pen.Color:= clGreen;
      Pen.Width:= 2;
      Brush.Style:= bsClear;

      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FBZone[R]) do
          for Ch:= 1 to 7 do
            if (((Ch in [   6]) and cbZoomIn.Checked) or
                ((Ch in [1, 7]) and cbZoomOut.Checked)) then
            begin
              for J:= 0 to High(RecData(Self).FBZone[R, I].Gis[Ch].DelayReg) do
                for K:= 0 to High(RecData(Self).FBZone[R, I].Items) do
                begin
                  Y1:= TestScrPos(RecData(Self).FBZone[R, I].Items[K].StCrd, X1);
                  Y2:= TestScrPos(RecData(Self).FBZone[R, I].Items[K].EdCrd, X2);

                  if not (((Y1 = - 1) and (Y2 = - 1)) or ((Y1 = 1) and (Y2 = 1))) then
                  begin
                    GetScrPos(R, Ch, RecData(Self).FBZone[R, I].Items[K].StCrd, RecData(Self).FBZone[R, I].Gis[Ch].DelayReg[J].StDelay, False, NewPos[1], OutOfRange);
                    GetScrPos(R, Ch, RecData(Self).FBZone[R, I].Items[K].EdCrd, RecData(Self).FBZone[R, I].Gis[Ch].DelayReg[J].EdDelay, False, NewPos[2], OutOfRange);
                    MoveTo(X1, NewPos[1].Y);
                    LineTo(X2, NewPos[1].Y);
                    MoveTo(X1, NewPos[2].Y);
                    LineTo(X2, NewPos[2].Y);

                    S:= Format('Med: %3.1f; Th: %3.1f ', [RecData(Self).FBZone[R, I].Gis[Ch].DelayReg[J].EMedium, RecData(Self).FBZone[R, I].Gis[Ch].DelayReg[J].ETh]);
//                    S:= ' ' + S + Format('[%3.1f - %3.1f]', [RecData(Self).FBZone[R, I].Gis[Ch].DelayReg[K].EMedium - 2 * RecData(Self).FBZone[R, I].Gis[Ch].DelayReg[K].ETh, RecData(Self).FBZone[R, I].Gis[Ch].DelayReg[K].EMedium + 2 * RecData(Self).FBZone[R, I].Gis[Ch].DelayReg[K].ETh]);
                    S:= ' ' + S + Format('Zone: %d; Ch: %d; Idx: %d', [I, Ch, J]);

                //  TextOut(X1 + 10, (NewPos[1].Y + NewPos[2].Y) div 2 - TextHeight(S), S);
                    TextOut(X1 + 10, NewPos[1].Y + 4, S);
                  end;
                end;
            end;

      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FHZone[R]) do
          for Ch:= 1 to 7 do
            if (((Ch in [2, 4]) and cbZoomIn.Checked) or
                ((Ch in [3, 5]) and cbZoomOut.Checked)) then
            begin
              for J:= 0 to High(RecData(Self).FHZone[R, I].Gis[Ch].DelayReg) do
                for K:= 0 to High(RecData(Self).FHZone[R, I].Items) do
                begin
                  Y1:= TestScrPos(RecData(Self).FHZone[R, I].Items[K].StCrd, X1);
                  Y2:= TestScrPos(RecData(Self).FHZone[R, I].Items[K].EdCrd, X2);
                  if not (((Y1 = - 1) and (Y2 = - 1)) or ((Y1 = 1) and (Y2 = 1))) then
                  begin
                    GetScrPos(R, Ch, RecData(Self).FHZone[R, I].Items[K].StCrd, RecData(Self).FHZone[R, I].Gis[Ch].DelayReg[J].StDelay, False, NewPos[1], OutOfRange);
                    GetScrPos(R, Ch, RecData(Self).FHZone[R, I].Items[K].EdCrd, RecData(Self).FHZone[R, I].Gis[Ch].DelayReg[J].EdDelay, False, NewPos[2], OutOfRange);
                    MoveTo(X1, NewPos[1].Y);
                    LineTo(X2, NewPos[1].Y);
                    MoveTo(X1, NewPos[2].Y);
                    LineTo(X2, NewPos[2].Y);

                    S:= Format('Med: %3.1f; Th: %3.1f ', [RecData(Self).FHZone[R, I].Gis[Ch].DelayReg[J].EMedium, RecData(Self).FHZone[R, I].Gis[Ch].DelayReg[J].ETh]);
//                    S:= S + ' ' + Format('[%3.1f - %3.1f]', [RecData(Self).FHZone[R, I].Gis[Ch].DelayReg[J].EMedium - 2 * RecData(Self).FHZone[R, I].Gis[Ch].DelayReg[J].ETh, RecData(Self).FHZone[R, I].Gis[Ch].DelayReg[J].EMedium + 2 * RecData(Self).FHZone[R, I].Gis[Ch].DelayReg[J].ETh]);
                    S:= S + ' ' + Format('Zone: %d; Ch: %d; Idx: %d', [I, Ch, J]);

//                    TextOut(X1 + 10, (NewPos[1].Y + NewPos[2].Y) div 2 - TextHeight(S) + J * 8, S);
                    TextOut(X1 + 10, NewPos[2].Y - 14, S);

                  end;
                end;
            end;
    *)
(*


              RecData(Self).FWorkZone[WZ, R, I].DelayReg[Ch, J].

              if Ch in [2..5] then
              begin
                for J:= 0 to High(RecData(Self).FWorkZone[WZ, R, I].DelayReg[Ch]) do
                begin
                  Y1:= TestScrPos(RecData(Self).FWorkZone[WZ, R, I].Idx DelayReg[Ch]. .StCrd, X1);
                  Y2:= TestScrPos(RecData(Self).FRails[R, I].Head[J].EdCrd, X2);
                  if not (((Y1 = - 1) and (Y2 = - 1)) or ((Y1 = 1) and (Y2 = 1))) then
                  begin
                    for K:= 0 to High(RecData(Self).FRails[R, I].DelayReg[Ch]) do
                    begin
                      GetScrPos(R, Ch, RecData(Self).FRails[R, I].Head[J].StCrd, RecData(Self).FRails[R, I].DelayReg[Ch, K].StDelay, False, NewPos[1], OutOfRange);
                      GetScrPos(R, Ch, RecData(Self).FRails[R, I].Head[J].StCrd, RecData(Self).FRails[R, I].DelayReg[Ch, K].EdDelay, False, NewPos[2], OutOfRange);
                      MoveTo(X1, NewPos[1].Y);
                      LineTo(X2, NewPos[1].Y);
                      MoveTo(X1, NewPos[2].Y);
                      LineTo(X2, NewPos[2].Y);
                    end;
                  end;
                end;
              end
              else   // ������ 1, 6, 7
                for J:= 0 to High(RecData(Self).FRails[R, I].Body) do
                begin
                  Y1:= TestScrPos(RecData(Self).FRails[R, I].Body[J].StCrd, X1);
                  Y2:= TestScrPos(RecData(Self).FRails[R, I].Body[J].EdCrd, X2);
                  if not (((Y1 = - 1) and (Y2 = - 1)) or ((Y1 = 1) and (Y2 = 1))) then
                  begin
                    for K:= 0 to High(RecData(Self).FRails[R, I].DelayReg[Ch]) do
                    begin
                      GetScrPos(R, Ch, RecData(Self).FRails[R, I].Body[J].StCrd, RecData(Self).FRails[R, I].DelayReg[Ch, K].StDelay, False, NewPos[1], OutOfRange);
                      GetScrPos(R, Ch, RecData(Self).FRails[R, I].Body[J].StCrd, RecData(Self).FRails[R, I].DelayReg[Ch, K].EdDelay, False, NewPos[2], OutOfRange);
                      MoveTo(X1, NewPos[1].Y);
                      LineTo(X2, NewPos[1].Y);
                      MoveTo(X1, NewPos[2].Y);
                      LineTo(X2, NewPos[2].Y);
                    end;
                  end;
                end; 
    end;           *)

// ---------------- ������� ������� 2 --------------------------------------------
 (*
    if CheckBox14.Checked then // ������� �������
    begin
      Pen.Color:= clGreen;
      Pen.Width:= 2;
      Brush.Color:= clBlack;

      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).StrelkaBody[R]) do
          for Ch:= 1 to 7 do
            if Ch in [1, 6, 7] then
              for J:= 0 to High(RecData(Self).StrelkaBody[R, I].Body) do
              begin
                Y1:= TestScrPos(RecData(Self).StrelkaBody[R, I].Body[J].StCrd, X1);
                Y2:= TestScrPos(RecData(Self).StrelkaBody[R, I].Body[J].EdCrd, X2);
                if not (((Y1 = - 1) and (Y2 = - 1)) or ((Y1 = 1) and (Y2 = 1))) then
                begin
                  for K:= 0 to High(RecData(Self).StrelkaBody[R, I].DelayReg[Ch]) do
                  begin
                    GetScrPos(R, Ch, RecData(Self).StrelkaBody[R, I].Body[J].StCrd, RecData(Self).StrelkaBody[R, I].DelayReg[Ch, K].StDelay, False, NewPos[1], OutOfRange);
                    GetScrPos(R, Ch, RecData(Self).StrelkaBody[R, I].Body[J].StCrd, RecData(Self).StrelkaBody[R, I].DelayReg[Ch, K].EdDelay, False, NewPos[2], OutOfRange);
                    MoveTo(X1, NewPos[1].Y);
                    LineTo(X2, NewPos[1].Y);
                    MoveTo(X1, NewPos[2].Y);
                    LineTo(X2, NewPos[2].Y);
                  end;
                end;
              end;
    end;
    *)
// ---------------- ������� ----------------------------------------------------

    if CheckBox31.Checked then
    begin
      Brush.Style:= bsClear;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FDefList[R]) do
          with RecData(Self).FDefList[R, I] do
          begin
          {  Flg1:= (ChList[2] <> 0) or (ChList[3] <> 0);
            Flg2:= (ChList[4] <> 0) or (ChList[5] <> 0);
            Flg3:= (ChList[6] <> 0) or (ChList[7] <> 0);
            Flg4:= (ChList[0] <> 0) or (ChList[1] <> 0);

            if Flg1 then StStrip:= 0 else
            if Flg2 then StStrip:= 1 else
            if Flg3 then StStrip:= 2 else
            if Flg4 then StStrip:= 3;

            if Flg4 then EdStrip:= 3 else
            if Flg3 then EdStrip:= 2 else
            if Flg2 then EdStrip:= 1 else
            if Flg1 then EdStrip:= 0;
           }


            XRange.StCrd:= MaxInt;
            XRange.EdCrd:= - MaxInt;
            for J:= 0 to High(PBList) do
            begin
              if GetScrPos(R, PBList[J].Ch, PBList[J].Crd.StCrd, PBList[J].Dly.StCrd, False, Pt, OutOfRange) then
              begin
                XRange.StCrd:= Min(XRange.StCrd, Pt.X);
                XRange.EdCrd:= Max(XRange.EdCrd, Pt.X);
              end;
              if GetScrPos(R, PBList[J].Ch, PBList[J].Crd.EdCrd, PBList[J].Dly.EdCrd, False, Pt, OutOfRange) then
              begin
                XRange.StCrd:= Min(XRange.StCrd, Pt.X);
                XRange.EdCrd:= Max(XRange.EdCrd, Pt.X);
              end;
            end;

            if (XRange.StCrd <> MaxInt) and
               (XRange.EdCrd <> - MaxInt) and
                GetScrPos3(R, Rgn.StCrd, Rgn.EdCrd, 0, 3, Rt) then
            begin
              Rt.Left:= XRange.StCrd;
              Rt.Right:= XRange.EdCrd;
              
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);

              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);

              case Typ of
                dtHead: S:= '� �������';
                dtBody: S:= '� �����';
              dtBottom: S:= '� �������';
             dtUnknown: S:= '�� ����������';
             dtLostBtm: S:= '���������� ������� ������� ';
          pbtiUnChkCon: S:= '�� ���������� ��';
              end;

              TextOut(Rt.Left + 15, Rt.Top + 15, S);
              Brush.Style:= bsClear;
              for J:= 0 to High(PBList) do
              begin
                S:= Format('Ch: %d; Len: %3.1f', [PBList[J].Ch, GetLen(PBList[J].Crd) * FScanStep]);
                TextOut(Rt.Left + 15, Rt.Top + 30 + 15 * J, S);

               {
                GetScrPos2(R, PBList[J].Ch, Max(J, Max(FStCrd, FStDrawCrd)), X1, Y1, Y2, True);
                GetScrPos2(R, PBList[J].Ch, Min(K, Min(FEdCrd, FEdDrawCrd)), X2, Y1, Y2, True);
                if GetScrPos(R, PBList[J].Ch, PBList[J].Crd.StCrd, PBList[J].Dly. D - 5, False, Pt, OutOfRange) then
                      FillRect(Rect(Pt.X - 1, Pt.Y - 1, Pt.X + 1, Pt.Y + 1));
                FillRect(Rect(X1, Y1 + 2, X2, Y2 - 2));
               }
              end;
              S:= Format('SummLen: %3.1f', [SummLen * FScanStep]);
//              TextOut(Rt.Left + 15, Rt.Top + 30 + 15 * PBCount, S);
            end;
          end;
    end;

// ---------------- ���� �������� ����� ---------------------------------------------------------

    if CheckBox27.Checked then
    begin
      Pen.Width:= 1;
      Pen.Color:= clBlack;
      Brush.Style:= bsClear;
      for I:= 0 to High(RecData(Self).FBMList.FBMData) do
        for R:= r_Left to r_Right do
          if GetScrPos3(R, RecData(Self).FBMList.FBMData[I].DisRange.StCrd, RecData(Self).FBMList.FBMData[I].DisRange.EdCrd, 0, 3, Rt) then
          begin
            TextOut(Rt.Left + 3, Rt.Top + 3, Format('%d', [I]));
            MoveTo(Rt.Left, Rt.Top);
            LineTo(Rt.Left, Rt.Bottom);
            MoveTo(Rt.Right, Rt.Top);
            LineTo(Rt.Right, Rt.Bottom);
          end;
    end;

    if CheckBox28.Checked then
    begin
      Pen.Width:= 1;
      Pen.Color:= clBlack;
      Brush.Style:= bsClear;
      for I:= 0 to High(RecData(Self).FBMList.FBMData) do
        for J:= 0 to High(RecData(Self).FBMList.FBMData[I].DisItems) do
          for R:= r_Left to r_Right do
            if GetScrPos3(R, RecData(Self).FBMList.FBMData[I].DisItems[J].StCrd, RecData(Self).FBMList.FBMData[I].DisItems[J].EdCrd, 0, 3, Rt) then
            begin
              TextOut(Rt.Left + 3, Rt.Top + 3, Format('%d.%d', [I, J + 1]));
              MoveTo(Rt.Left, Rt.Top);
              LineTo(Rt.Left, Rt.Bottom);
              MoveTo(Rt.Right, Rt.Top);
              LineTo(Rt.Right, Rt.Bottom);
            end;
    end;


// ----------------------------------------------------------------------------------------------

    if PointCB1.Checked or PointCB2.Checked then
    begin
      Pen.Color:= clRed;
      Pen.Width:= 4;
      for R:= r_Left to r_Right do
        for I:= 0 to High(RecData(Self).FPointList[R]) do
          if GetScrPos3(R, RecData(Self).FPointList[R, I].Crd, RecData(Self).FPointList[R, I].Crd, 0, 3, Rt) and
             ((PointCB1.Checked and (RecData(Self).FPointList[R, I].Typ = 1)) or
              (PointCB2.Checked and (RecData(Self).FPointList[R, I].Typ = 2))) then
          begin
            TextOut(Rt.Left + 3, Rt.Top + 3, Format('%d', [I]));
            MoveTo(Rt.Right, Rt.Top);
            LineTo(Rt.Right, Rt.Bottom);
          end;
    end;

// ----- ����� ������� ��� ----------------------------------------------------------------------
           {
    if PointWZ.Checked then
    begin
      Pen.Color:= clRed;
      Pen.Width:= 4;
      for R:= r_Left to r_Right do
      begin
        for I:= 0 to High(RecData(Self).FPtn1[R]) do
          if GetScrPos3(R, RecData(Self).FPtn1[R, I].Crd, RecData(Self).FPtn1[R, I].Crd, 0, 3, Rt) then
          begin
            TextOut(Rt.Left + 3, Rt.Bottom - 12 - Ord(Odd(I)) * 12, Format('%d:%d - %d', [I, RecData(Self).FPtn1[R, I].Typ, RecData(Self).FPtn1[R, I].Idx1]));
            MoveTo(Rt.Right, Rt.Top);
            LineTo(Rt.Right, Rt.Bottom);
          end;
        for I:= 0 to High(RecData(Self).FPtn2[R]) do
          if GetScrPos3(R, RecData(Self).FPtn2[R, I].Crd, RecData(Self).FPtn2[R, I].Crd, 0, 3, Rt) then
          begin
            TextOut(Rt.Left + 3, Rt.Bottom - 12 - Ord(Odd(I)) * 12, Format('%d:%d - %d', [I, RecData(Self).FPtn2[R, I].Typ, RecData(Self).FPtn2[R, I].Idx1]));
            MoveTo(Rt.Right, Rt.Top);
            LineTo(Rt.Right, Rt.Bottom);
          end;
      end;
    end;        }

// ---------------- ���������� ������ ������� � ����������� �������� ----------------------------
(*
    Pen.Width:= 3;
    Pen.Color:= clRed;
    for R:= r_Left to r_Right do
    begin
      if GetScrPos3(R, RecData(Self).FNextAnalyze[R].Head, RecData(Self).FNextAnalyze[R].Head, 0, 1, Rt) then
      begin
        MoveTo(Rt.Left, Rt.Top);
        LineTo(Rt.Left, Rt.Bottom);
      end;
      if GetScrPos3(R, RecData(Self).FNextAnalyze[R].Body, RecData(Self).FNextAnalyze[R].Body, 2, 3, Rt) then
      begin
        MoveTo(Rt.Left, Rt.Top);
        LineTo(Rt.Left, Rt.Bottom);
      end;

      if GetScrPos3(R, RecData(Self).FCurrAnalyze[R].Head, RecData(Self).FCurrAnalyze[R].Head, 0, 1, Rt) then
      begin
        MoveTo(Rt.Left, Rt.Top);
        LineTo(Rt.Left, Rt.Bottom);
      end;
      if GetScrPos3(R, RecData(Self).FCurrAnalyze[R].Body, RecData(Self).FCurrAnalyze[R].Body, 2, 3, Rt) then
      begin
        MoveTo(Rt.Left, Rt.Top);
        LineTo(Rt.Left, Rt.Bottom);
      end;
    end;
    Pen.Width:= 1;
    *)
// ---------------- ����� �� ������ ������ -------------------------------------

    if Test.Checked then
    begin
     {
      Pen.Width:= 1;
      Pen.Color:= clBlack;
      Pen.Style:= psDashDot;
      Brush.Style:= bsClear;
      MoveTo(Width div 2 - Round(500 / FScanStep / FXZoom), 0);
      LineTo(Width div 2 - Round(500 / FScanStep / FXZoom), Height);
      Pen.Style:= psSolid;

      Pen.Width:= 1;
      Pen.Color:= clBlack;
      Pen.Style:= psDashDot;
      Brush.Style:= bsClear;
      MoveTo(Width div 2 + Round(500 / FScanStep / FXZoom), 0);
      LineTo(Width div 2 + Round(500 / FScanStep / FXZoom), Height);
      Pen.Style:= psSolid;
     }
      Pen.Width:= 1;
      Pen.Color:= clBlack;
      Pen.Style:= psDashDot;
      Brush.Style:= bsClear;
      MoveTo(Width div 2, 0);
      LineTo(Width div 2, Height);
      Pen.Style:= psSolid;

    end;

    Log.Add(Format('%d - ����� �� ������ ������', [GetTickCount - T])); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T:= GetTickCount;

  end;
  SetLength(FXorLine, 0);

  if CheckBox32.Checked then
  begin
    Chart12.BottomAxis.SetMinMax(FStDrawCrd, FEdDrawCrd);
    Button7Click(nil);
  end;

//  if RecData(Self).FBJHeadBlock <> 0 then
//    Chart2.BottomAxis.SetMinMax((FStDrawCrd - FStCrd) div RecData(Self).FBJHeadBlock, (FEdDrawCrd - FStCrd) div RecData(Self).FBJHeadBlock);
//  Log.Add(Format('%d - Charts.SetMinMax', [GetTickCount - T])); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end;

procedure TRecDataForm.SetData(DatSrc: TAvk11DatSrc; RecBlock: Pointer; CenterDisCoord: Integer);
var
  I: Integer;

begin
  {$IFDEF RECDEBUG}
  FDatSrc:= DatSrc;
  FRecData:= RecBlock;
  FScanStep:= FDatSrc.Header.ScanStep / 100;

  ZoomBarChange(nil);

//  FStDisCrd:= RecData(Self).FStDisCoord;
//  FEdDisCrd:= RecData(Self).FEdDisCoord;
//  FStRecCrd:= RecData(Self).FStRecCoord;
//  FEdRecCrd:= RecData(Self).FEdRecCoord;
  FStCrd:= RecData(Self).FStCrd;
  FEdCrd:= RecData(Self).FEdCrd;

  ScrollBar.ControlStyle:= ScrollBar.ControlStyle + [csNoStdEvents];
  ZoomBar.ControlStyle:= ZoomBar.ControlStyle + [csNoStdEvents];
  ScrollBar.SetParams(CenterDisCoord, FStCrd, FEdCrd);

  FMaxShift:= 0;
  for I:= 0 to 7 do
    FMaxShift:= Max(FMaxShift, Abs(Round(ChList[I].Shift[1] + ChList[I].RedPar[1] * 255 / FScanStep)));

  CreateOutInfo;

  TRecData(FRecData):= RecBlock;
        (*
  with TRecData(FRecData) do
  begin
{    BSLeft.Clear;                                       // ������ ������ ���������� ��������� ������� �������
    BSRight.Clear;
    for I:= 0 to 255 do BSLeft.AddXY(I / 3, FBSGis[r_Left, I]);
    for I:= 0 to 255 do BSRight.AddXY(I / 3, FBSGis[r_Right, I]);
 }
                                                        // ������ ��� �������� �����
{    BMGraphList.Clear;
    for I:= 0 to High(FBMGraph) do
      with FBMGraph[I].Range do
        BMGraphList.Items.Add(Format('S: %d - E: %d', [StCrd, EdCrd]));
 }
 {  LineSeries1.Clear;
    LineSeries2.Clear;
    if RadioButton6.Checked then
    begin
      for I:= 0 to High(FTHead[r_Left, 1]) do LineSeries1.AddXY(I, FTHead[r_Left, 1, I]);
      for I:= 0 to High(FTHead[r_Right, 2]) do LineSeries2.AddXY(I, FTHead[r_Right, 1, I]);
    end
    else
    begin
      for I:= 0 to High(FNoise[r_Left]) do LineSeries1.AddXY(I, FNoise[r_Left, I]);
      for I:= 0 to High(FNoise[r_Right]) do LineSeries2.AddXY(I, FNoise[r_Right, I]);
    end }

    with TRecData(FRecData) do
    begin
      LineSeries1.Clear;
      LineSeries2.Clear;
      for I:= 0 to High(FTHead[r_Left, 1]) do LineSeries1.AddXY(I, FTHead[r_Left, 1, I]);
      for I:= 0 to High(FTHead[r_Right, 2]) do LineSeries2.AddXY(I, FTHead[r_Right, 1, I]);

      Series8.Clear;
      Series8.AddXY(0, FEndTh[r_Left]);
      Series8.AddXY(High(FTHead[r_Left, 1]), FEndTh[r_Left]);

      Series9.Clear;
      Series9.AddXY(0, FEndTh[r_Right]);
      Series9.AddXY(High(FTHead[r_Right, 2]), FEndTh[r_Right]);
    end;
  end;
     *)
  SpeedButton3.Down:= True;
  SpeedButton3Click(SpeedButton3);
//  CheckBox9.Checked:= True;
//  MyRefresh(nil);
  ComboBox13Change(nil);

  CreateImage(nil);
  SetLength(FSavePoint, 0);
  FSaveYPoint:= -1;

  LineSeries7.Clear;
  with FDatSrc do
    for I:= 0 to High(FCDList) do
      LineSeries7.AddXY(Event[FCDList[I]].Event.DisCoord, Event[FCDList[I]].Event.SysCoord);
  CheckBox33Click(nil);

  {$ENDIF}
end;

procedure TRecDataForm.ByHeight(Sender: TObject);
var
  I: Integer;
  MinVal: Integer;
  MaxVal: Integer;

begin
  MinVal:= MaxInt;
  MaxVal:= - MaxInt;
  with FDatSrc do
    for I:= 0 to High(FCDList) do
      if (Event[FCDList[I]].Event.DisCoord >= Chart12.BottomAxis.Minimum) and
         (Event[FCDList[I]].Event.DisCoord <= Chart12.BottomAxis.Maximum) then
      begin
        MinVal:= Min(MinVal, Event[FCDList[I]].Event.SysCoord);
        MaxVal:= Max(MaxVal, Event[FCDList[I]].Event.SysCoord);
      end;

  if (MinVal = MaxInt) or (MaxVal = MaxInt) then Exit;
  Chart12.LeftAxis.SetMinMax(MinVal - (MaxVal - MinVal) * 0.1, MaxVal + (MaxVal - MinVal) * 0.1);
end;

procedure TRecDataForm.CheckBox33Click(Sender: TObject);
var
  I: Integer;
  J: Integer;
  K: Integer;
  L: Integer;
  R: RRail;

begin
(*  with TRecData(FRecData) do
  begin
    LineSeries1.Clear;
    LineSeries2.Clear;

    case ComboBox18.ItemIndex of
      0: R:= r_Left;
      1: R:= r_Right;
    end;

    J:= ComboBox17.ItemIndex + 1;
    if J <> 1 then
    begin
      for I:= 1 to High(FTHead[R, J]) - 1 do LineSeries1.AddXY(I, FTHead[R, J, I]);
    end
    else
    begin
      for I:= 0 to High(FTHead[R, 1]) do
      begin
        LineSeries1.AddXY(I, FTHead[R, 1, I]);

      {
        K:= 0;
        L:= 0;
        for J:= 2 to 7 do L:= L + FTHead[R, J, I];
        for J:= 2 to 7 do K:= K + Ord(FTHead[R, J, I] > 60);
        LineSeries1.AddXY(I, L * K * K);
       }
      end;



        (*
      if CheckBox45.Checked then
        for I:= 0 to High(FTHead[r_Left, 1]) do
        begin
          K:= 0;
          for J:= 2 to 7 do K:= K + Ord(FTHead[r_Left, J, I] > 60);
          LineSeries1.AddXY(I + 0.5, K);
        end;

      if CheckBox46.Checked then
        for I:= 0 to High(FTHead[r_Right, 1]) do
        begin
          K:= 0;
          for J:= 2 to 7 do K:= K + Ord(FTHead[r_Right, J, I] > 60);
          LineSeries2.AddXY(I + 0.5, K);
        end;

        * )

    end;

//    for I:= 1 to High(FTHead[r_Left, J]) - 1 do LineSeries1.AddXY(I, FTHead[r_Left, J, I] + FTHead[r_Left, J, I - 1] + FTHead[r_Left, J, I + 1]);
//    for I:= 1 to High(FTHead[r_Right, J]) - 1 do LineSeries2.AddXY(I, FTHead[r_Right, J, I] + FTHead[r_Right, J, I - 1] + FTHead[r_Right, J, I + 1]);
//    Series8.Clear;
//    Series8.AddXY(0, FEndTh[r_Left]);
//    Series8.AddXY(High(FTHead[r_Left, 1]), FEndTh[r_Left]);
//    Series9.Clear;
//    Series9.AddXY(0, FEndTh[r_Right]);
//    Series9.AddXY(High(FTHead[r_Right, 2]), FEndTh[r_Right]);
  end; *)
end;

procedure TRecDataForm.CMChildKey(var Message: TCMChildKey);
begin
  if (Message.Sender is TWinControl) and
     (Message.CharCode in [VK_RIGHT,
                           VK_LEFT,
                           VK_UP,
                           VK_DOWN,
                           VK_END,
                           VK_HOME,
                           VK_PRIOR,
                           VK_NEXT,
                           VK_RETURN]) then Message.Result := 1
                                       else inherited;
end;
(*
procedure TRecBlockForm.MyLoadData(StCoord, EdCoord: Integer; BlockOk: TDataNotifyEvent);
begin
  case FCoordMode of
    1: FDatSrc.LoadData(StCoord, EdCoord, 0, BlockOk {BlockOK_Echo} );
    2: RecData(Self).RecLoadData(StCoord, EdCoord, BlockOk {BlockOK_Echo} );
  end;
end;
*)
{$ENDIF}

procedure TRecDataForm.FormCreate(Sender: TObject);
begin
  {$IFDEF RECDEBUG}

//  FFiltr:= nil;
  TabSheet1.TabVisible:= True;
  TabSheet2.TabVisible:= False;
  TabSheet3.TabVisible:= False;
  TabSheet4.TabVisible:= True;
  TabSheet5.TabVisible:= True;
  TabSheet6.TabVisible:= False;
  TabSheet7.TabVisible:= True;
  TabSheet8.TabVisible:= False;
  TabSheet9.TabVisible:= True;
  TabSheet10.TabVisible:= False;
  TabSheet11.TabVisible:= True;
  TabSheet12.TabVisible:= True;
  TabSheet13.TabVisible:= True;
  TabSheet15.TabVisible:= True;

  FSeriesRefresh:= nil;
  FRec:= 0;
  FResRectHeight:= 300;
  ViewStrip.Checked[0]:= True;
  ViewStrip.Checked[1]:= True;
  ViewStrip.Checked[2]:= True;
  ViewStrip.Checked[3]:= True;

  PBCheck.Checked[0]:= False;
  CheckBox17.Checked:= False;

  PageControl1.ActivePageIndex:= 0;
  PageControl1.Height:= 22;
  FCoordMode:= 1;
//  DeleteFile('X:\draw.log');
//  BSCheck.Checked[0]:= True;
  {$ENDIF}
end;

{$IFDEF RECDEBUG}
procedure ChartLine(Chart: TChart; X1, Y1, X2, Y2, Width: Integer; Color: TColor);

function GetChartPos(X, Y: Integer): TPoint;
begin
  Result.X:= Chart.BottomAxis.CalcPosValue(X);
  Result.Y:= Chart.LeftAxis.CalcPosValue(Y);
end;

begin
  with Chart.Canvas do
  begin
    Pen.Width:= Width;
    Pen.Color:= Color;
    MoveTo(GetChartPos(X1, Y1).X, GetChartPos(X1, Y1).Y);
    LineTo(GetChartPos(X2, Y2).X, GetChartPos(X2, Y2).Y);
  end;
end;
  {$ENDIF}
{
procedure ChartXZone(Chart: TChart; X1, X2: Integer; Color: TColor);

function GetChartPos(X, Y: Integer): TPoint;
begin
  Result.X:= Chart.BottomAxis.CalcPosValue(X);
  Result.Y:= Chart.LeftAxis.CalcPosValue(Y);
end;

begin
  with Chart.Canvas do
  begin
    Pen.Style:= psClear;
    Brush.Color:= Color;
    FillRect(Rect(Chart.BottomAxis.CalcPosValue(X1),
                  Chart.LeftAxis.CalcPosValue(Chart.LeftAxis.Minimum),
                  Chart.BottomAxis.CalcPosValue(X2),
                  Chart.LeftAxis.CalcPosValue(Chart.LeftAxis.Maximum)));
  end;
end;
}

procedure TRecDataForm.FormDestroy(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  I: Integer;
  Ch: Integer;
{$ENDIF}

begin
  {$IFDEF RECDEBUG}
//  FFiltr.Free;
  SetLength(FXorLine, 0);
  SetLength(FOutInfo, 0);
  for R:= r_Left to r_Right do                    // ������� ������ ��������������� - �����
    for Ch:= 0 to 7 do
    begin
      for I:= 0 to High(FOnScrPB[R, Ch]) do
        SetLength(FOnScrPB[R, Ch, I].PtList, 0);
      SetLength(FOnScrPB[R, Ch], 0);
    end;
  {$ENDIF}
end;

procedure TRecDataForm.PBTHChange(Sender: TObject);
begin
{$IFDEF RECDEBUG}
  Label7.Caption:= Format('%3.2f', [TrackBar1.Position / 50 - 1]);
  Label8.Caption:= Format('%3.2f', [TrackBar2.Position / 50 - 1]);
  Label9.Caption:= Format('%3.2f', [TrackBar3.Position / 50 - 2]);
  Label10.Caption:= Format('%3.2f', [TrackBar4.Position / 50 - 1]);
  MyRefresh(Sender);
{$ENDIF}
end;

{$IFDEF RECDEBUG}
var
  I, K, J: Integer;
  ExR: RRail;
  ExCh: Integer;
  ExA: array [0..15] of Integer;
{$ENDIF}

{$IFDEF RECDEBUG}
function TRecDataForm.BlockOK_Echo2(StartDisCoord: Integer): Boolean;
var
  J: Integer;

begin
  with FDatSrc.CurEcho[ExR, ExCh] do
   for J:= 1 to Count do
     Inc(ExA[Ampl[J]]);
  Result:= True;
end;
{$ENDIF}

procedure TRecDataForm.Button6Click(Sender: TObject);
{$IFDEF RECDEBUG}
var
  I: Integer;
{$ENDIF}

begin
{$IFDEF RECDEBUG}
  case ComboBox2.ItemIndex of
    0: ExR:= r_Left;
    1: ExR:= r_Right;
  end;

  ExCh:= ComboBox12.ItemIndex;

  for I:= 0 to 15 do ExA[I]:=0;
  FDatSrc.LoadData(FStDrawCrd, FEdDrawCrd, 0, BlockOK_Echo2);

  Series7.Clear;
  for I:= 3 to 15 do
    Series7.AddXY(I, ExA[I]);

{$ENDIF}
end;

procedure TRecDataForm.ComboBox13Change(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  I: Integer;
  J: Integer;
  Ch: Integer;
  Zone: Integer;
{$ENDIF}

begin
{$IFDEF RECDEBUG}
  case ComboBox9.ItemIndex of
    0: R:= r_Left;
    1: R:= r_Right;
  end;
{  if ComboBox10.Items.Count <> Length(RecData(Self).FBZone[R]) then
  begin
    J:= ComboBox10.ItemIndex;
    ComboBox10.Items.Clear;
    for I:= 0 to High(RecData(Self).FBZone[R]) do            // ������������� ����
      ComboBox10.Items.Add('���� ' + IntToStr(I));
    ComboBox10.ItemIndex:= J;
  end; }
  if ComboBox13.ItemIndex = - 1 then Exit;
  Ch:= ComboBox13.ItemIndex + 1;
  Zone:= ComboBox10.ItemIndex;

  Series4.Clear;
  Series5.Clear;
  Series6.Clear;
{  if Ch in [2, 3, 4, 5] then
  begin
    for I:= 0 to 255 do
      Series4.AddXY(I,  RecData(Self).FHZone[R, Zone].Gis[Ch].AmplGis[I]);
    ScrollBar.Position:= (RecData(Self).FHZone[R, Zone].Items[0].StCrd + RecData(Self).FHZone[R, Zone].Items[High(RecData(Self).FHZone[R, Zone].Items)].EdCrd) div 2;

    for J:= 0 to High(RecData(Self).FHZone[R, Zone].Gis[Ch].DelayReg) do
    begin
      Series5.AddXY(RecData(Self).FHZone[R, Zone].Gis[Ch].DelayReg[J].StDelay, 0);
      Series6.AddXY(RecData(Self).FHZone[R, Zone].Gis[Ch].DelayReg[J].EdDelay, 0);
    end;
  end
  else
  begin
    for I:= 0 to 255 do
      Series4.AddXY(I,  RecData(Self).FBZone[R, Zone].Gis[Ch].AmplGis[I]);
    ScrollBar.Position:= (RecData(Self).FBZone[R, Zone].Items[0].StCrd + RecData(Self).FBZone[R, Zone].Items[High(RecData(Self).FBZone[R, Zone].Items)].EdCrd) div 2;

    for J:= 0 to High(RecData(Self).FBZone[R, Zone].Gis[Ch].DelayReg) do
    begin
      Series5.AddXY(RecData(Self).FBZone[R, Zone].Gis[Ch].DelayReg[J].StDelay, 0);
      Series6.AddXY(RecData(Self).FBZone[R, Zone].Gis[Ch].DelayReg[J].EdDelay, 0);
    end;
  end;    }

//  RecData(Self).FWorkZone[WZ, R, I].DelayReg[Ch, J].StDelay
//  RecData(Self).FWorkZone[WZ, R, I].DelayReg[Ch, J].EdDelay

  {$ENDIF}
end;

procedure TRecDataForm.Button3Click(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  I: Integer;
  J: Integer;
  K: Integer;

  BPL: array [0..7] of array of Integer;
  Ok: Boolean;

{$ENDIF}

begin
  {$IFDEF RECDEBUG}
{  HoleList.Clear;
  for R:= r_Left to r_Right do
    for I:= 0 to High(RecData(Self).FSingleObj[R]) do
      with RecData(Self).FSingleObj[R, I] do
        if Typ = so_BoltHole then
        begin
          for J:= 0 to 7 do SetLength(BPL[J], 0);

          for J:= 0 to High(PBList) do
          begin
            K:= Length(BPL[PBList[J].Ch]);
            SetLength(BPL[PBList[J].Ch], K + 1);
            BPL[PBList[J].Ch, K]:= PBList[J].Idx;
          end;

          Ok:= (Length(BPL[0]) in [1..3]) and
               (Length(BPL[1]) = 1) and
               (Length(BPL[6]) = 1) and
               (Length(BPL[7]) = 1);

          case R of
           r_Left: HoleList.Items.AddObject(Format(' ����� - %d %d', [GetCentr(RecData(Self).FSingleObj[R, I].DisRange), Ord(OK)]), Pointer(I + Ord(R) * $10000));
          r_Right: HoleList.Items.AddObject(Format('������ - %d %d', [GetCentr(RecData(Self).FSingleObj[R, I].DisRange), Ord(OK)]), Pointer(I + Ord(R) * $10000));
          end;
        end; }
  {$ENDIF}
end;

procedure TRecDataForm.Button8Click(Sender: TObject);
begin
  if HoleList.ItemIndex < HoleList.Items.Count - 1 then HoleList.ItemIndex:= HoleList.ItemIndex + 1;
  HoleListClick(Sender);
end;

procedure TRecDataForm.Button9Click(Sender: TObject);
begin
  if HoleList.ItemIndex > 0 then HoleList.ItemIndex:= HoleList.ItemIndex - 1;
  HoleListClick(Sender);
end;

procedure TRecDataForm.Button5Click(Sender: TObject);
{$IFDEF RECDEBUG}
var
  R: RRail;
  I, J: Integer;
  PBIdx1, PBCh1: Integer;
  PBIdx2, PBCh2: Integer;
  Flg1, Flg2: Boolean;
  New: TPBItem;
  Angle: Integer;
  AngleGis: array [0..90] of Integer;
  {$ENDIF}

begin
{$IFDEF RECDEBUG}
{  for Angle:= 0 to 90 do AngleGis[Angle]:= 0;

  for R:= r_Left to r_Right do
    for I:= 0 to High(RecData(Self).FSingleObj[R]) do
      if (RecData(Self).FSingleObj[R, I].Typ = so_BoltHole) then
      begin
        for J:= 0 to High(RecData(Self).FSingleObj[R, I].PBList) do
        begin
          PBCh1:= RecData(Self).FSingleObj[R, I].PBList[J].Ch;
          PBIdx1:= RecData(Self).FSingleObj[R, I].PBList[J].Idx;
          if (PBCh1 = 6) and (GetLen(RecData(Self).FPBList[R, PBCh1, PBIdx1].Crd) > 20 / FScanStep) then
          begin
            Flg1:= True;
            Break;
          end;
        end;

        for J:= 0 to High(RecData(Self).FSingleObj[R, I].PBList) do
        begin
          PBCh2:= RecData(Self).FSingleObj[R, I].PBList[J].Ch;
          PBIdx2:= RecData(Self).FSingleObj[R, I].PBList[J].Idx;
          if (PBCh1 = 7) and (GetLen(RecData(Self).FPBList[R, PBCh2, PBIdx2].Crd) > 20 / FScanStep) then
          begin
            Flg2:= True;
            Break;
          end;
        end;

        if Flg1 and Flg2 then
        begin
          New:= RecData(Self).FPBList[R, PBCh1, PBIdx1];
          Angle:= Abs(Round(ArcTan2(New.Del.EdCrd - New.Del.StCrd + 1, New.Crd.EdCrd - New.Crd.StCrd + 1) * 180 / Pi));
          Inc(AngleGis[Angle]);
        end;
      end;

  Series10.Clear;
  for Angle:= 0 to 90 do
    Series10.AddXY(Angle, AngleGis[Angle]); }
  {$ENDIF}
end;

procedure TRecDataForm.HoleListClick(Sender: TObject);
{$IFDEF RECDEBUG}
type
  TListItem1 = record
    Idx: Integer;
    State: Integer;
  end;

  TListItem2 = record
    Idx: Integer;
    Item: array of TListItem1;
  end;

var
  R: RRail;
  I, PBIdx, PBCh, Ch: Integer;
  Color: TColor;
  L: TChartShape;
  Z1: TRect;
  Z2: TRect;
  Btm: TPoint;
  Main: Boolean;

//  List: array [1..7] of array of TListItem2;

  PBs: array [1..7] of Integer;
  Idx: Integer;
  Len: Integer;
  MainCrd: TRange;
  MainDel: TRange;
  SlaveCrd: TRange;
  SlaveDel: TRange;

{$ENDIF}

begin
  {$IFDEF RECDEBUG}
(*
  Panel23.Visible:= False;

  if (Integer(HoleList.Items.Objects[HoleList.ItemIndex]) and $10000 = 0) then R:= r_Left else R:= r_Right;
  I:= Integer(HoleList.Items.Objects[HoleList.ItemIndex]) and $FFFF;

//  for R:= r_Left to r_Right do
//    for I:= 0 to High(RecData(Self).FSingleObj[R]) do
      if RecData(Self).FSingleObj[R, I].Typ = so_BoltHole then  // ����� ���������
      begin
        PBs[1]:= - 1;
        PBs[6]:= - 1;
        PBs[7]:= - 1;

        for Ch:= 1 to 7 do                                // ���� ������������ (�� �����) ����� � ������� 1, 6, 7
          if Ch in [1, 6, 7] then
          begin
            Len:= - MaxInt;
            for J:= 0 to High(RecData(Self).FSingleObj[R, I].PBList) do
              if Ch = RecData(Self).FSingleObj[R, I].PBList[J].Ch then
              begin
                Idx:= RecData(Self).FSingleObj[R, I].PBList[J].Idx;
                if GetLen(RecData(Self).FPBList[R, Ch, Idx].Crd) > Len then
                begin
                  PBs[Ch]:= J;
                  Len:= GetLen(RecData(Self).FPBList[R, Ch, Idx].Crd);
                end;
              end;
          end;

        for Ch:= 1 to 7 do
          if (Ch in [1, 6, 7]) and (PBs[Ch] <> - 1) then
          begin
            Idx:= RecData(Self).FSingleObj[R, I].PBList[PBs[Ch]].Idx;
            MainCrd:= RecData(Self).FPBList[R, Ch, Idx].Crd;     // �������� �����
            MainDel:= RecData(Self).FPBList[R, Ch, Idx].Del;
            for J:= 0 to High(RecData(Self).FSingleObj[R, I].PBList) do
              if (Ch = RecData(Self).FSingleObj[R, I].PBList[J].Ch) and (J <> PBs[Ch]) then
              begin
                SlaveCrd:= RecData(Self).FPBList[R, Ch, RecData(Self).FSingleObj[R, I].PBList[J].Idx].Crd;
                SlaveDel:= RecData(Self).FPBList[R, Ch, RecData(Self).FSingleObj[R, I].PBList[J].Idx].Del;

                if ((MainCrd.StCrd >= SlaveCrd.StCrd) and (MainCrd.StCrd <= SlaveCrd.EdCrd)) or
                   ((MainCrd.EdCrd >= SlaveCrd.StCrd) and (MainCrd.EdCrd <= SlaveCrd.EdCrd)) then Panel23.Visible:= True;
              end;
          end;
      end;

              (*
  for J:= 0 to High(RecData(Self).FSingleObj[R, I].PBList) do      // ������������� ��� ����� ���������
  begin
    PBCh:= RecData(Self).FSingleObj[R, I].PBList[J].Ch;

    SetLength(List[PBCh], Length(List[PBCh]) + 1);
    List[PBCh, High(List[PBCh])].Idx:= J;

    for K:= 0 to High(RecData(Self).FSingleObj[R, I].PBList) do   // ������� ������ ����� ���� �� ������
      if (J <> K) and
         (RecData(Self).FSingleObj[R, I].PBList[K].Ch = PBCh) then
      begin
        with List[PBCh, High(List[PBCh])] do
        begin
          SetLength(Item, Length(Item) + 1);
          Item[High(Item)].Idx:= K;
          Item[High(Item)].State:= 0;
                  {
          RecData(Self).FSingleObj[R, I].PBList[J].Ch
          RecData(Self).FSingleObj[R, I].PBList[J].Idx

          RecData(Self).FSingleObj[R, I].PBList[K].Ch
          RecData(Self).FSingleObj[R, I].PBList[J].Idx
                   }
      end;
  end;

  for PBCh:= 1 to 7 do
    if PBCh in [1, 6, 7] then
      if Length(List[PBCh]) > 1 then

        for J:= 0 to High(List[PBCh]) do

        for K:= 0 to High(List[PBCh, J].Item) do

        List[PBCh, J].Item[K].State

        RecData(Self).FPBList[R, Ch, Idx].Crd.StCrd;
                *)

  (*
  for J:= 0 to High(RecData(Self).FSingleObj[R, I].PBList) do      // ������������� ��� ����� ���������
  begin
    PBCh:= RecData(Self).FSingleObj[R, I].PBList[J].Ch;

    SetLength(List[PBCh], Length(List[PBCh]) + 1);
    List[PBCh, High(List[PBCh])].Idx:= J;

    for K:= 0 to High(RecData(Self).FSingleObj[R, I].PBList) do   // ������� ������ ����� ���� �� ������
      if (J <> K) and
         (RecData(Self).FSingleObj[R, I].PBList[K].Ch = PBCh) then
      begin
        with List[PBCh, High(List[PBCh])] do
        begin
          SetLength(Item, Length(Item) + 1);
          Item[High(Item)].Idx:= K;
          Item[High(Item)].State:= 0;
                  {
          RecData(Self).FSingleObj[R, I].PBList[J].Ch
          RecData(Self).FSingleObj[R, I].PBList[J].Idx

          RecData(Self).FSingleObj[R, I].PBList[K].Ch
          RecData(Self).FSingleObj[R, I].PBList[J].Idx
                   }
      end;
  end;

  for PBCh:= 1 to 7 do
    if PBCh in [1, 6, 7] then
      if Length(List[PBCh]) > 1 then

        for J:= 0 to High(List[PBCh]) do

        for K:= 0 to High(List[PBCh, J].Item) do

        List[PBCh, J].Item[K].State
  *)



                                                  // ��������� ����� �� �������
(*
  if (Integer(HoleList.Items.Objects[HoleList.ItemIndex]) and $10000 = 0) then R:= r_Left else R:= r_Right;
  I:= Integer(HoleList.Items.Objects[HoleList.ItemIndex]) and $FFFF;

  ScrollBar.Position:= GetCentr(RecData(Self).FSingleObj[R, I].DisRange);

  Chart10.SeriesList.Clear;
  Chart10.Refresh;

  Chart13.SeriesList.Clear;
  Chart13.Refresh;

  Z1:= Rect(MaxInt, MaxInt, - MaxInt, - MaxInt);
  Z2:= Rect(MaxInt, MaxInt, - MaxInt, - MaxInt);

  for J:= 0 to High(RecData(Self).FSingleObj[R, I].PBList) do      // ������������� ��� ����� ���������
  begin
    PBCh:= RecData(Self).FSingleObj[R, I].PBList[J].Ch;
    Main:= RecData(Self).FSingleObj[R, I].PBList[J].Main;

    if PBCh in [6, 7] then
    begin
      PBIdx:= RecData(Self).FSingleObj[R, I].PBList[J].Idx;

      if Main then
      begin
        if PBCh = 6 then Color:= clRed
                    else Color:= clBlue;
      end else Color:= clSilver;

      L:= TChartShape.Create(Chart13);
      L.Style:= chasLine;
      L.Pen.Width:= 4;
      L.Pen.Color:= Color;
      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd;
      L.Y0:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].CCrd;
      L.Y1:= RecData(Self).FPBList[R, PBCh, PBIdx].CDel;
      Z1.Left   := Min(Z1.Left   , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd);
      Z1.Left   := Min(Z1.Left   , RecData(Self).FPBList[R, PBCh, PBIdx].CCrd);
      Z1.Right  := Max(Z1.Right  , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd);
      Z1.Right  := Max(Z1.Right  , RecData(Self).FPBList[R, PBCh, PBIdx].CCrd);
      Z1.Top    := Min(Z1.Top    , RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd);
      Z1.Top    := Min(Z1.Top    , RecData(Self).FPBList[R, PBCh, PBIdx].CDel);
      Z1.Bottom := Max(Z1.Bottom , RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd);
      Z1.Bottom := Max(Z1.Bottom , RecData(Self).FPBList[R, PBCh, PBIdx].CDel);
      Chart13.AddSeries(L);

      L:= TChartShape.Create(Chart13);
      L.Style:= chasLine;
      L.Pen.Width:= 2;
      L.Pen.Color:= clBlack;
      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd;
      L.Y0:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd;
      L.Y1:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd + 32;
      Chart13.AddSeries(L);

      L:= TChartShape.Create(Chart13);
      L.Style:= chasLine;
      L.Pen.Width:= 4;
      L.Pen.Color:= Color;
      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].CCrd;
      L.Y0:= RecData(Self).FPBList[R, PBCh, PBIdx].CDel;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd;
      L.Y1:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd;
      Z1.Left   := Min(Z1.Left   , RecData(Self).FPBList[R, PBCh, PBIdx].CCrd);
      Z1.Left   := Min(Z1.Left   , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd);
      Z1.Right  := Max(Z1.Right  , RecData(Self).FPBList[R, PBCh, PBIdx].CCrd);
      Z1.Right  := Max(Z1.Right  , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd);
      Z1.Top    := Min(Z1.Top    , RecData(Self).FPBList[R, PBCh, PBIdx].CDel);
      Z1.Top    := Min(Z1.Top    , RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd);
      Z1.Bottom := Max(Z1.Bottom , RecData(Self).FPBList[R, PBCh, PBIdx].CDel);
      Z1.Bottom := Max(Z1.Bottom , RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd);
      Chart13.AddSeries(L);

      L:= TChartShape.Create(Chart13);
      L.Style:= chasLine;
      L.Pen.Width:= 2;
      L.Pen.Color:= clBlack;

      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd;
      L.Y0:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd;
      L.Y1:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd + 32;

      Chart13.AddSeries(L);

      L:= TChartShape.Create(Chart13);
      L.Style:= chasLine;
      L.Pen.Width:= 2;
      L.Pen.Color:= clBlack;
      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd;
      L.Y0:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd;
      L.Y1:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd + 32;
      Chart13.AddSeries(L);

      L:= TChartShape.Create(Chart13);
      L.Style:= chasLine;
      L.Pen.Width:= 2;
      L.Pen.Color:= clBlack;
      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd;
      L.Y0:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd + 32;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd;
      L.Y1:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd + 32;
      Chart13.AddSeries(L);

    end;

    if PBCh in [1] then
    begin
      PBIdx:= RecData(Self).FSingleObj[R, I].PBList[J].Idx;

      if Main then Color:= clBlue
              else Color:= clSilver;

      L:= TChartShape.Create(Chart10);
      L.Style:= chasLine;
      L.Pen.Width:= 4;
      L.Pen.Color:= Color;
      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd;
      L.Y0:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd / 3;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].CCrd;
      L.Y1:= RecData(Self).FPBList[R, PBCh, PBIdx].CDel / 3;
      Z2.Left   := Min(Z2.Left   , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd);
      Z2.Left   := Min(Z2.Left   , RecData(Self).FPBList[R, PBCh, PBIdx].CCrd);
      Z2.Right  := Max(Z2.Right  , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd);
      Z2.Right  := Max(Z2.Right  , RecData(Self).FPBList[R, PBCh, PBIdx].CCrd);
      Z2.Top    := Min(Z2.Top    , RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd div 3);
      Z2.Top    := Min(Z2.Top    , RecData(Self).FPBList[R, PBCh, PBIdx].CDel div 3);
      Z2.Bottom := Max(Z2.Bottom , RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd div 3);
      Z2.Bottom := Max(Z2.Bottom , RecData(Self).FPBList[R, PBCh, PBIdx].CDel div 3);
      Chart10.AddSeries(L);

      L:= TChartShape.Create(Chart13);
      L.Style:= chasLine;
      L.Pen.Width:= 2;
      L.Pen.Color:= clBlack;
      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd;
      L.Y0:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd / 3;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd;
      L.Y1:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd / 3 + 32;
      Chart10.AddSeries(L);

      L:= TChartShape.Create(Chart10);
      L.Style:= chasLine;
      L.Pen.Width:= 4;
      L.Pen.Color:= Color;
      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].CCrd;
      L.Y0:= RecData(Self).FPBList[R, PBCh, PBIdx].CDel / 3;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd;
      L.Y1:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd / 3;
      Z2.Left   := Min(Z2.Left   , RecData(Self).FPBList[R, PBCh, PBIdx].CCrd);
      Z2.Left   := Min(Z2.Left   , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd);
      Z2.Right  := Max(Z2.Right  , RecData(Self).FPBList[R, PBCh, PBIdx].CCrd);
      Z2.Right  := Max(Z2.Right  , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd);
      Z2.Top    := Min(Z2.Top    , RecData(Self).FPBList[R, PBCh, PBIdx].CDel div 3);
      Z2.Top    := Min(Z2.Top    , RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd div 3);
      Z2.Bottom := Max(Z2.Bottom , RecData(Self).FPBList[R, PBCh, PBIdx].CDel div 3);
      Z2.Bottom := Max(Z2.Bottom , RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd div 3);
      Chart10.AddSeries(L);

      L:= TChartShape.Create(Chart13);
      L.Style:= chasLine;
      L.Pen.Width:= 2;
      L.Pen.Color:= clBlack;
      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd;
      L.Y0:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd / 3;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd;
      L.Y1:= RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd / 3 + 32;
      Chart10.AddSeries(L);
    end;

    if PBCh = 0 then
    begin
      PBIdx:= RecData(Self).FSingleObj[R, I].PBList[J].Idx;


      if Main then Color:= clRed
              else Color:= clSilver;
      L:= TChartShape.Create(Chart10);
      L.Style:= chasLine;
      L.Pen.Width:= 4;
      L.Pen.Color:= Color;
      L.X0:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd;
      L.Y0:= (RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd + RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd) / 6;
      L.X1:= RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd;
      L.Y1:= (RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd + RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd) / 6;
      Chart10.AddSeries(L);

      Z2.Left   := Min(Z2.Left   , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd);
      Z2.Left   := Min(Z2.Left   , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd);
      Z2.Right  := Max(Z2.Right  , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.StCrd);
      Z2.Right  := Max(Z2.Right  , RecData(Self).FPBList[R, PBCh, PBIdx].Crd.EdCrd);
      Z2.Top    := Min(Z2.Top    , Round((RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd + RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd) / 6));
      Z2.Top    := Min(Z2.Top    , Round((RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd + RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd) / 6));
      Z2.Bottom := Max(Z2.Bottom , Round((RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd + RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd) / 6));
      Z2.Bottom := Max(Z2.Bottom , Round((RecData(Self).FPBList[R, PBCh, PBIdx].Del.StCrd + RecData(Self).FPBList[R, PBCh, PBIdx].Del.EdCrd) / 6));
    end;
  end;

  if (Z1.Left <> MaxInt) and (Z1.Right <> - MaxInt) and
     (Z2.Left <> MaxInt) and (Z2.Right <> - MaxInt) then
  begin
    Btm.X:= Min(Round(Z1.Left - (Z1.Right - Z1.Left) * 0.2), Round(Z2.Left - (Z2.Right - Z2.Left) * 0.2));
    Btm.Y:= Max(Round(Z1.Right + (Z1.Right - Z1.Left) * 0.2), Round(Z2.Right + (Z2.Right - Z2.Left) * 0.2));
    Chart13.BottomAxis.SetMinMax(Btm.X, Btm.Y);
    Chart10.BottomAxis.SetMinMax(Btm.X, Btm.Y);
  end else
  if (Z1.Left <> MaxInt) and (Z1.Right <> - MaxInt) and
     not ((Z2.Left <> MaxInt) and (Z2.Right <> - MaxInt)) then
  begin
    Btm.X:= Round(Z1.Left - (Z1.Right - Z1.Left) * 0.2);
    Btm.Y:= Round(Z1.Right + (Z1.Right - Z1.Left) * 0.2);
    Chart13.BottomAxis.SetMinMax(Btm.X, Btm.Y);
    Chart10.BottomAxis.SetMinMax(Btm.X, Btm.Y);
  end else
  if not ((Z1.Left <> MaxInt) and (Z1.Right <> - MaxInt)) and
     (Z2.Left <> MaxInt) and (Z2.Right <> - MaxInt) then
  begin
    Btm.X:= Round(Z2.Left - (Z2.Right - Z2.Left) * 0.2);
    Btm.Y:= Round(Z2.Right + (Z2.Right - Z2.Left) * 0.2);
    Chart13.BottomAxis.SetMinMax(Btm.X, Btm.Y);
    Chart10.BottomAxis.SetMinMax(Btm.X, Btm.Y);
  end;

  if (Z1.Top <> MaxInt) and (Z1.Bottom <> - MaxInt) then
  begin
    Btm.X:= Z1.Top - Round((Z1.Bottom - Z1.Top) * 0.2);
    Btm.Y:= Z1.Bottom + Round((Z1.Bottom - Z1.Top) * 0.2);
    if Abs(Btm.Y - Btm.X) < 90 then
    begin
      Btm.X:= (Btm.X + Btm.Y) div 2 - 45;
      Btm.Y:= (Btm.X + Btm.Y) div 2 + 45;
    end;
    Chart13.LeftAxis.SetMinMax(Btm.X, Btm.Y);
  end;

  if (Z2.Top <> MaxInt) and (Z2.Bottom <> - MaxInt) then
  begin
    Btm.X:= Z2.Top - Round((Z2.Bottom - Z2.Top) * 0.2);
    Btm.Y:= Z2.Bottom + Round((Z2.Bottom - Z2.Top) * 0.2);
    if Abs(Btm.Y - Btm.X) < 90 then
    begin
      Btm.X:= (Btm.X + Btm.Y) div 2 - 45;
      Btm.Y:= (Btm.X + Btm.Y) div 2 + 45;
    end;
    Chart10.LeftAxis.SetMinMax(Btm.X, Btm.Y);
  end;

//    Chart10.LeftAxis.SetMinMax(Z2.Top - (Z2.Bottom - Z2.Top) * 2, Z2.Bottom + (Z2.Bottom - Z2.Top) * 2);

  Chart13.Refresh;
  Chart10.Refresh; *)
  {$ENDIF}
end;


procedure TRecDataForm.Button7Click(Sender: TObject);
begin
  {$IFDEF RECDEBUG}
  ByHeight(nil);
  {$ENDIF}
end;

procedure TRecDataForm.Panel22Resize(Sender: TObject);
begin
  Chart13.Height:= Panel22.Height div 2;
end;

{$IFDEF RECDEBUG}
(*
function TRecBlockForm.DataNotify2(StartDisCoord: Integer): Boolean;
var
  R: RRail;
  I: Integer;
  Ch: Integer;
  DelZone: Integer;

begin
  if not FDatSrc.BackMotion then
  begin
    for R:= r_Left to r_Right do
    begin
      for Ch:= 2 to 7 do
        with FDatSrc.CurEcho[R, Ch] do
          for I:= 1 to Count do
          begin
            DelZone:= Delay[I] div FDelayStep[Ch];
            if (DelZone >= 0) and
               (DelZone <= 92) and
               (Filtr[FFiltrIdx].Ch27Gis[R, Ch, Ampl[I], DelZone] < 255) then Inc(Filtr[FFiltrIdx].Ch27Gis[R, Ch, Ampl[I], DelZone]);
          end;

      Ch:= 1;
      with FDatSrc.CurEcho[R, Ch] do
        for I:= 1 to Count do
        begin
          DelZone:= Delay[I] div FDelayStep[Ch];
          if (DelZone >= 0) and (DelZone <= 34) then
            with Filtr[FFiltrIdx].Ch1Gis[R, Ampl[I], DelZone] do
            begin
                                                      // ��������� ����������
              if FDatSrc.CurDisCoord - LastCrd > 30 then
              begin
                if ECount < 255 then Inc(ECount);
                StGlue:= FDatSrc.CurDisCoord;
              end;
              LastCrd:= FDatSrc.CurDisCoord;
              MaxLen:= Min(255, Max(MaxLen, FDatSrc.CurDisCoord - StGlue));
            end;
        end;
    end;
  end;
  Result:= True;
end;
 *)
{$ENDIF}

procedure TRecDataForm.ScrollBar4Change(Sender: TObject);
begin
  {$IFDEF RECDEBUG}
  FResRectHeight:= ScrollBar4.Position;
  CreateOutInfo;
  MyRefresh(nil);
  {$ENDIF}
end;

procedure TRecDataForm.Button11Click(Sender: TObject);
var
  R: RRail;
  Ch, L1, L2: Integer;

begin
  {$IFDEF RECDEBUG}
(*  case cbTestRail.ItemIndex of
    0: R:= r_Left;
    1: R:= r_Right;
  end;
  Ch:= cbTestCh.ItemIndex;

  if cbTestMode.ItemIndex = 0 then
    Label18.Caption:= Format('���������:  %d%%', [IntersecSquare(RecData(Self).FPoints2[R, StrToInt(eTestIdx1.Text)].Crd,
                                                                 RecData(Self).FPoints2[R, StrToInt(eTestIdx1.Text)].Depth,
                                                                 RecData(Self).FPoints2[R, StrToInt(eTestIdx2.Text)].Crd,
                                                                 RecData(Self).FPoints2[R, StrToInt(eTestIdx2.Text)].Depth)]) else
  begin
    L1:= PointToLine(Point(RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx2.Text)].Crd.StCrd, RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx2.Text)].Del.StCrd),
                     Point(RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx1.Text)].Crd.StCrd, RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx1.Text)].Del.StCrd),
                     Point(RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx1.Text)].Crd.EdCrd, RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx1.Text)].Del.EdCrd));

    L2:= PointToLine(Point(RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx2.Text)].Crd.EdCrd, RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx2.Text)].Del.EdCrd),
                     Point(RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx1.Text)].Crd.StCrd, RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx1.Text)].Del.StCrd),
                     Point(RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx1.Text)].Crd.EdCrd, RecData(Self).FPBList[R, Ch, StrToInt(eTestIdx1.Text)].Del.EdCrd));

    Label18.Caption:= Format('���������: %d - %d', [L1, L2]);
  end; *)
  {$ENDIF}
end;

procedure TRecDataForm.Button12Click(Sender: TObject);
var
  R: RRail;
  I, Ch, CompIdx: Integer;
  s: string;

begin
  {$IFDEF RECDEBUG}
  {
  ListBox4.Items.Clear;

  for R:= r_Left to r_Right do                       // �������� ������� ��� �����
  begin
    case R of
      r_Left: s:= '���';
     r_Right: s:= '����';
    end;

    for Ch:= 1 to 7 do
      if Ch in [1, 6, 7] then
        for I:= 0 to High(RecData(Self).FPBList[R, Ch]) do
          if RecData(Self).FPBList[R, Ch, I].Typ in [pbtForHoleDef, pbtForHoleBolt] then
          begin
            CompIdx:= (Ch * 10000 + (I + 1)) * (1 - 2 * Ord(R = r_Left));
            ListBox4.Items.AddObject(Format('%s; Ch: %d; Idx: %d; State: %d', [s, Ch, I, Ord(RecData(Self).FPBList[R, Ch, I].Typ)]), Pointer(CompIdx));
          end;
  end; }
  {$ENDIF}
end;

procedure TRecDataForm.ListBox4DblClick(Sender: TObject);
var
  R: RRail;
  I, Ch, Idx: Integer;

begin
  {$IFDEF RECDEBUG}
  if ListBox4.ItemIndex = - 1 then Exit;
  I:= Integer(ListBox4.Items.Objects[ListBox4.ItemIndex]);
  if I > 0 then R:= r_Right else R:= r_Left;
  Ch:= Trunc(Abs(I) / 10000);
  Idx:= Round(10000 * Frac(Abs(I) / 10000)) - 1;
  ScrollBar.Position:= GetCentr(RecData(Self).FPBList[R, Ch, Idx].Crd);
  CreateImage(nil);
  {$ENDIF}
end;

procedure TRecDataForm.ListBox5DblClick(Sender: TObject);
var
  R: RRail;
  I, Ch, Idx: Integer;

begin
  {$IFDEF RECDEBUG}
//  if ListBox5.ItemIndex = - 1 then Exit;
//  I:= Integer(ListBox5.Items.Objects[ListBox5.ItemIndex]);
  if I > 0 then R:= r_Right else R:= r_Left;
  Ch:= Trunc(Abs(I) / 10000);
  Idx:= Round(10000 * Frac(Abs(I) / 10000)) - 1;
  ScrollBar.Position:= GetCentr(RecData(Self).FPBList[R, Ch, Idx].Crd);
  CreateImage(nil);
  {$ENDIF}
end;

procedure TRecDataForm.cbViewModeChange(Sender: TObject);
begin
  case cbViewMode.ItemIndex of
    0: begin
         ViewStrip.Checked[0]:= True;
         ViewStrip.Checked[1]:= True;
         ViewStrip.Checked[2]:= True;
         ViewStrip.Checked[3]:= True;
         ViewStripClickCheck(Sender);
         BSCheck.Checked[0]:= False;
       end;
    1: begin
         ViewStrip.Checked[0]:= False;
         ViewStrip.Checked[1]:= False;
         ViewStrip.Checked[2]:= False;
         ViewStrip.Checked[3]:= True;
         ViewStripClickCheck(Sender);
         BSCheck.Checked[0]:= True;
       end;
  end;
  MyRefresh(nil);
end;

end.

{
��� �����
��� ������ ���.
����� ���������
����� �����
����� ����
����� Unknows
���
����� ��������
���������
}
