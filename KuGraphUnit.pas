unit KuGraphUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, ExtCtrls, TeeProcs, Chart,
  StdCtrls, MyTypes, ConfigUnit, Math;

type
  TKuGraphForm = class(TForm)
    Panel2: TPanel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    Label1: TLabel;
    RadioButton9: TRadioButton;
    RadioButton10: TRadioButton;
    RadioButton11: TRadioButton;
    RadioButton12: TRadioButton;
    RadioButton13: TRadioButton;
    RadioButton14: TRadioButton;
    RadioButton15: TRadioButton;
    RadioButton16: TRadioButton;
    RadioButton17: TRadioButton;
    RadioButton18: TRadioButton;
    Label2: TLabel;
    RadioButton19: TRadioButton;
    RadioButton20: TRadioButton;
    Panel1: TPanel;
    Panel3: TPanel;
    Chart1: TChart;
    Series1: TLineSeries;
    Series2: TLineSeries;
    Series3: TLineSeries;
    Chart2: TChart;
    LineSeries1: TLineSeries;
    procedure RadioButton1Click(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure OnChangeLanguage(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
//    FDatSrc: TAvk11DatSrc;
  public
//    procedure SetDate(DatSrc: TAvk11DatSrc);
  end;

var
  KuGraphForm: TKuGraphForm;

implementation

uses
  LanguageUnit;

{$R *.dfm}
(*
procedure TKuGraphForm.SetDate(DatSrc: TAvk11DatSrc);
{
var
  I: Integer;
  J: Integer;
  R: TRail;
  Ch: Integer;
  Idx: Integer;
  Flg: Boolean;
  Params: TMiasParams;
  Sender: TRadioButton;
 }
begin
{
  FDatSrc:= DatSrc;
  RadioButton1.Checked:= True;
  RadioButton1Click(RadioButton1);

  for J:= 1 to 20 do
  begin
    case J of
      1: begin R:= r_Left; Ch:= 0; end;
      2: begin R:= r_Left; Ch:= 1; end;
      3: begin R:= r_Left; Ch:= 2; end;
      4: begin R:= r_Left; Ch:= 3; end;
      5: begin R:= r_Left; Ch:= 4; end;
      6: begin R:= r_Left; Ch:= 5; end;
      7: begin R:= r_Left; Ch:= 6; end;
      8: begin R:= r_Left; Ch:= 7; end;
      9: begin R:= r_Left; Ch:= 8; end;
     10: begin R:= r_Left; Ch:= 9; end;
     11: begin R:= r_Right; Ch:= 0; end;
     12: begin R:= r_Right; Ch:= 1; end;
     13: begin R:= r_Right; Ch:= 2; end;
     14: begin R:= r_Right; Ch:= 3; end;
     15: begin R:= r_Right; Ch:= 4; end;
     16: begin R:= r_Right; Ch:= 5; end;
     17: begin R:= r_Right; Ch:= 6; end;
     18: begin R:= r_Right; Ch:= 7; end;
     19: begin R:= r_Right; Ch:= 8; end;
     20: begin R:= r_Right; Ch:= 9; end;
    end;

    case Ch of
      0: Idx:= 1;
      1: Idx:= 2;
      2: Idx:= 3;
      3: Idx:= 3;
      4: Idx:= 4;
      5: Idx:= 4;
      6: Idx:= 5;
      7: Idx:= 5;
      8: Idx:= 6;
      9: Idx:= 6;
    end;

    Flg:= False;
    FDatSrc.GetParamFirst(0, Params);
    for I:= 0 to FDatSrc.EventCount - 1 do
    begin
      FDatSrc.GetParamNext(FDatSrc.Event[I].Event.DisCoord, Params);
      if (Params.Sens[R, Ch] < Config.KuZones[Idx].Min) or
         (Params.Sens[R, Ch] > Config.KuZones[Idx].Max) then Flg:= True;
    end;

    case J of
      1: Sender:= RadioButton1;
      2: Sender:= RadioButton2;
      3: Sender:= RadioButton3;
      4: Sender:= RadioButton4;
      5: Sender:= RadioButton5;
      6: Sender:= RadioButton6;
      7: Sender:= RadioButton7;
      8: Sender:= RadioButton8;
      9: Sender:= RadioButton9;
     10: Sender:= RadioButton10;
     11: Sender:= RadioButton11;
     12: Sender:= RadioButton12;
     13: Sender:= RadioButton13;
     14: Sender:= RadioButton14;
     15: Sender:= RadioButton15;
     16: Sender:= RadioButton16;
     17: Sender:= RadioButton17;
     18: Sender:= RadioButton18;
     19: Sender:= RadioButton19;
     20: Sender:= RadioButton20;
    end;

    if Flg then TRadioButton(Sender).Font.Style:= [fsBold];
  end; }
end;
*)
procedure TKuGraphForm.RadioButton1Click(Sender: TObject);
var
  R: RRail;
  Ch: Integer;
  I: Integer;
  Idx: Integer;
  Flg: Boolean;
//  Params: TMiasParams;
  MinVal: Integer;
  MaxVal: Integer;

begin
{
  case TRadioButton(Sender).Tag of
    1: begin R:= r_Left; Ch:= 0; end;
    2: begin R:= r_Left; Ch:= 1; end;
    3: begin R:= r_Left; Ch:= 2; end;
    4: begin R:= r_Left; Ch:= 3; end;
    5: begin R:= r_Left; Ch:= 4; end;
    6: begin R:= r_Left; Ch:= 5; end;
    7: begin R:= r_Left; Ch:= 6; end;
    8: begin R:= r_Left; Ch:= 7; end;
    9: begin R:= r_Left; Ch:= 8; end;
   10: begin R:= r_Left; Ch:= 9; end;
   11: begin R:= r_Right; Ch:= 0; end;
   12: begin R:= r_Right; Ch:= 1; end;
   13: begin R:= r_Right; Ch:= 2; end;
   14: begin R:= r_Right; Ch:= 3; end;
   15: begin R:= r_Right; Ch:= 4; end;
   16: begin R:= r_Right; Ch:= 5; end;
   17: begin R:= r_Right; Ch:= 6; end;
   18: begin R:= r_Right; Ch:= 7; end;
   19: begin R:= r_Right; Ch:= 8; end;
   20: begin R:= r_Right; Ch:= 9; end;
  end;

  case Ch of
    0: Idx:= 1;
    1: Idx:= 2;
    2: Idx:= 3;
    3: Idx:= 3;
    4: Idx:= 4;
    5: Idx:= 4;
    6: Idx:= 5;
    7: Idx:= 5;
    8: Idx:= 6;
    9: Idx:= 6;
  end;

  MinVal:= MaxInt;
  MaxVal:= - MaxInt;
  Flg:= False;
  Series1.Clear;
  FDatSrc.GetParamFirst(0, Params);
  for I:= 0 to FDatSrc.EventCount - 1 do
  begin
    FDatSrc.GetParamNext(FDatSrc.Event[I].Event.DisCoord, Params);
    Series1.AddXY(FDatSrc.Event[I].Event.DisCoord, Params.Sens[R, Ch]);
    if (Params.Sens[R, Ch] < Config.KuZones[Idx].Min) or
       (Params.Sens[R, Ch] > Config.KuZones[Idx].Max) then Flg:= True;

    MinVal:= Min(MinVal, Params.Sens[R, Ch]);
    MaxVal:= Max(MaxVal, Params.Sens[R, Ch]);
  end;

  MinVal:= Min(MinVal, Config.KuZones[Idx].Min);
  MaxVal:= Max(MaxVal, Config.KuZones[Idx].Min);

  MinVal:= Min(MinVal, Config.KuZones[Idx].Max);
  MaxVal:= Max(MaxVal, Config.KuZones[Idx].Max);

  Chart1.LeftAxis.SetMinMax(MinVal - 1, MaxVal + 1);

  if Flg then TRadioButton(Sender).Font.Style:= [fsBold];

  Series2.Clear;
  Series2.AddXY(                  0, Config.KuZones[Idx].Min);
  Series2.AddXY(FDatSrc.MaxDisCoord, Config.KuZones[Idx].Min);

  Series3.Clear;
  Series3.AddXY(                  0, Config.KuZones[Idx].Max);
  Series3.AddXY(FDatSrc.MaxDisCoord, Config.KuZones[Idx].Max);

  // Пороговая чувствительность

  MinVal:= MaxInt;
  MaxVal:= - MaxInt;
  LineSeries1.Clear;
  FDatSrc.GetParamFirst(0, Params);
  for I:= 0 to FDatSrc.EventCount - 1 do
  begin
    FDatSrc.GetParamNext(FDatSrc.Event[I].Event.DisCoord, Params);
    LineSeries1.AddXY(FDatSrc.Event[I].Event.DisCoord, Params.Att[R, Ch] - Params.Sens[R, Ch]);
    MinVal:= Min(MinVal,                               Params.Att[R, Ch] - Params.Sens[R, Ch]);
    MaxVal:= Max(MaxVal,                               Params.Att[R, Ch] - Params.Sens[R, Ch]);
  end;

  Chart2.LeftAxis.SetMinMax(MinVal - 4, MaxVal + 4);
  }
end;

procedure TKuGraphForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if Msg.CharCode = 27 then Self.Close;
end;

procedure TKuGraphForm.OnChangeLanguage(Sender: TObject);
begin
  Self.Caption                := LanguageUnit.LangTable.Caption['SensDiag:SensitivityDiagram'];
  RadioButton1.Caption        := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 0';
  RadioButton2.Caption        := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 1';
  RadioButton3.Caption        := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 2';
  RadioButton4.Caption        := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 3';
  RadioButton5.Caption        := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 4';
  RadioButton6.Caption        := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 5';
  RadioButton7.Caption        := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 6';
  RadioButton8.Caption        := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 7';
  RadioButton9.Caption        := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 8';
  RadioButton10.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 9';
  RadioButton11.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 0';
  RadioButton12.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 1';
  RadioButton13.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 2';
  RadioButton14.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 3';
  RadioButton15.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 4';
  RadioButton16.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 5';
  RadioButton17.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 6';
  RadioButton18.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 7';
  RadioButton19.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 8';
  RadioButton20.Caption       := LanguageUnit.LangTable.Caption['SensDiag:Channel'] + ' 9';

  Chart1.Title.Text.Text      := LanguageUnit.LangTable.Caption['SensDiag:RelativeSensitivity'];
  Chart1.LeftAxis.Title.Caption:= LanguageUnit.LangTable.Caption['SensDiag:Ks[dB]'];

  Chart2.Title.Text.Text      := LanguageUnit.LangTable.Caption['SensDiag:ThresholdSensitivity'];
  Chart2.LeftAxis.Title.Caption:= LanguageUnit.LangTable.Caption['SensDiag:Kt[dB]'];

  Label1.Caption              := LanguageUnit.LangTable.Caption['SensDiag:Rail:Left'];
  Label2.Caption              := LanguageUnit.LangTable.Caption['SensDiag:Rail:Right'];
end;

procedure TKuGraphForm.FormCreate(Sender: TObject);
begin
  OnChangeLanguage(nil);
end;

end.
