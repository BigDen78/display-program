{$I DEF.INC}
unit CoordGraphUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, TeeProcs, Chart, StdCtrls, ExtCtrls,
  TeeShape, AviconDataSource;

type
  TCoordGraphForm = class(TForm)
    Chart1: TChart;
    Dis_Crd: TLineSeries;
    Sys_Crd: TLineSeries;
    Panel1: TPanel;
    DisCrd: TCheckBox;
    SysCrd: TCheckBox;
    AL: TCheckBox;
    BL: TCheckBox;
    AR: TCheckBox;
    BR: TCheckBox;
    Bevel1: TBevel;
    Bevel2: TBevel;
    cbShowPoints: TCheckBox;
    UMUA_Left: TLineSeries;
    UMUB_Left: TLineSeries;
    UMUA_Right: TLineSeries;
    UMUB_Right: TLineSeries;
//    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DisCrdClick(Sender: TObject);
    procedure SysCrdClick(Sender: TObject);
    function DrawSample(StartDisCoord: Integer): Boolean;
    procedure cbShowPointsClick(Sender: TObject);
    procedure BRClick(Sender: TObject);
    procedure ARClick(Sender: TObject);
    procedure ALClick(Sender: TObject);
    procedure BLClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    {
    procedure CheckBox1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    }
  private
//    I: Integer;
    FSrc: TAvk11DatSrc;
    Bags: array of TChartShape;

  public
    procedure CalcData(Src: TAvk11DatSrc);
//   procedure CalcData1(Src: TAvk11DatSrc);
  end;

implementation

{$R *.dfm}
(*
procedure TCoordGraphForm.CalcData1(Src: TAvk11DatSrc);
var
  I: Integer;

begin
  Series1.Clear;
  Series2.Clear;
  for I:= 0 to Src.EventCount - 1 do
  begin
//    Series1.AddXY(I, Src.Event[I].Event.SysCoord);
//    Series2.AddXY(I, Src.Event[I].Event.DisCoord);

    Series1.AddXY(Src.Event[I].Event.DisCoord, Src.Event[I].Event.SysCoord);
  end;

  for I:= 0 to High(Bags) do Bags[I].Free;

  SetLength(Bags, 1);
  Bags[0]:= TChartShape.Create(nil);
  Bags[0].ParentChart:= Chart2;
  Bags[0].Style:= chasRectangle;
  Bags[0].X0:= 0;
  Bags[0].X1:= 100;
  Bags[0].Y0:= 0;
  Bags[0].Y1:= 1;
  Bags[0].Brush.Color:= clWhite;

  for I:= 0 to Src.ErrorCount - 1 do
  begin
    SetLength(Bags, Length(Bags) + 1);
    Bags[High(Bags)]:= TChartShape.Create(nil);
    with Bags[High(Bags)] do
    begin
      ParentChart:= Chart2;
      Style:= chasRectangle;
      X0:= Src.Error[I].Offset * 100 div Src.FileBody.Size;
      X1:= (Src.Error[I].Offset + Src.Error[I].Size) * 100 div Src.FileBody.Size;
      Y0:= 0;
      Y1:= 1;
      Brush.Color:= clRed;
    end;
  end;
end;
 *)

procedure TCoordGraphForm.cbShowPointsClick(Sender: TObject);
begin
  Dis_Crd.Pointer.Visible:= cbShowPoints.Checked;
  Sys_Crd.Pointer.Visible:= cbShowPoints.Checked;
  UMUA_Left.Pointer.Visible:= cbShowPoints.Checked;
  UMUB_Left.Pointer.Visible:= cbShowPoints.Checked;
  UMUA_Right.Pointer.Visible:= cbShowPoints.Checked;
  UMUB_Right.Pointer.Visible:= cbShowPoints.Checked;
end;

procedure TCoordGraphForm.DisCrdClick(Sender: TObject);
begin
  Dis_Crd.Visible:= DisCrd.Checked;
end;
    {
procedure TCoordGraphForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  I: Integer;

begin
  for I:= 0 to High(Bags) do Bags[I].Free;
  Self.Release;
end;
      }
procedure TCoordGraphForm.SysCrdClick(Sender: TObject);
begin
  Sys_Crd.Visible:= SysCrd.Checked;
end;

procedure TCoordGraphForm.ALClick(Sender: TObject);
begin
   UMUA_Left.Visible:= AL.Checked;
end;

procedure TCoordGraphForm.ARClick(Sender: TObject);
begin
   UMUA_Right.Visible:= AR.Checked;
end;

procedure TCoordGraphForm.BLClick(Sender: TObject);
begin
   UMUB_Left.Visible:= BL.Checked;
end;

procedure TCoordGraphForm.BRClick(Sender: TObject);
begin
   UMUB_Right.Visible:= BR.Checked;
end;

procedure TCoordGraphForm.CalcData(Src: TAvk11DatSrc);
var
  I: Integer;
  BMFlag: Boolean;
  DisCoord: Integer;

begin

  I:= 0;
  FSrc:= Src;
  UMUA_Left.Clear;
  UMUB_Left.Clear;
  UMUA_Right.Clear;
  UMUB_Right.Clear;
  Dis_Crd.Clear;
  Sys_Crd.Clear;
  Src.LoadData(0, Src.MaxDisCoord - 100, 0, DrawSample);

end;

function TCoordGraphForm.DrawSample(StartDisCoord: Integer): Boolean;
begin
  Dis_Crd.AddXY(FSrc.FileBody.Position, FSrc.CurDisCoord);
  Sys_Crd.AddXY(FSrc.FileBody.Position, FSrc.CurSysCoord);

  UMUA_Left.AddXY(FSrc.FileBody.Position, FSrc.FCurLoadSysCoord_UMUA_Left);
  UMUB_Left.AddXY(FSrc.FileBody.Position, FSrc.FCurLoadSysCoord_UMUB_Left);
  UMUA_Right.AddXY(FSrc.FileBody.Position, FSrc.FCurLoadSysCoord_UMUA_Right);
  UMUB_Right.AddXY(FSrc.FileBody.Position, FSrc.FCurLoadSysCoord_UMUB_Right);

{

  Series3.AddXY(FSrc.FileBody.Position, FSrc.FCurLoadDelta_UMUA_Left);
  Series4.AddXY(FSrc.FileBody.Position, FSrc.FCurLoadDelta_UMUB_Left);
  Series5.AddXY(FSrc.FileBody.Position, FSrc.FCurLoadDelta_UMUA_Right);
  Series6.AddXY(FSrc.FileBody.Position, FSrc.FCurLoadDelta_UMUB_Right);
}
  Result:= True;
end;

procedure TCoordGraphForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Self.Release;
end;

procedure TCoordGraphForm.FormCreate(Sender: TObject);
begin
  UMUA_Left.Visible:= AL.Checked;
  UMUA_Right.Visible:= AR.Checked;
  UMUB_Left.Visible:= BL.Checked;
  UMUB_Right.Visible:= BR.Checked;
  Dis_Crd.Visible:= DisCrd.Checked;
  Sys_Crd.Visible:= SysCrd.Checked;
end;

end.
 {
procedure TCoordGraphForm.CheckBox1Click(Sender: TObject);
begin
  Series1.Pointer.Visible:= CheckBox1.Checked;
end;

procedure TCoordGraphForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Chart1.Visible:= False;
  Release;
end;

procedure TCoordGraphForm.FormCreate(Sender: TObject);
begin
  ComboBox1.ItemIndex:= 0;
  ComboBox2.ItemIndex:= 0;
end;

procedure TCoordGraphForm.ComboBox1Change(Sender: TObject);
begin
  CalcData(FSrc);
end;

procedure TCoordGraphForm.ComboBox2Change(Sender: TObject);
begin
  CalcData(FSrc);
end;
   }

