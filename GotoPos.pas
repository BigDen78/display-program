{$I DEF.INC}
unit GotoPos; {Language 11}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Mask, Avk11ViewUnit, LanguageUnit, AviconTypes;

type

  TGotoForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label13: TLabel;
    Panel2: TPanel;
    Label2: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    cbKm: TComboBox;
    cbPk: TComboBox;
    Edit2: TEdit;
    Panel6: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    Panel1: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    procedure NewCoordKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure cbKmChange(Sender: TObject);
    procedure cbPkChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    FVF: TAvk11ViewForm;

  public
    ResDisCoord: Integer;

    procedure SetData(VF: TAvk11ViewForm);
    procedure OnChangeLanguage(Sender: TObject);
  end;

var
  NewRealCoord: Extended;

implementation

{$R *.DFM}

procedure TGotoForm.NewCoordKeyPress(Sender: TObject; var Key: Char);
begin
  case Key of
    #13: Button1.Click;
    #27: Button2.Click;
  end;
end;

procedure TGotoForm.SetData(VF: TAvk11ViewForm);
var
  I: Integer;
  GotoForm: TGotoForm;
  Temp: TAvk11ViewForm;
  FindKm: Integer;
  FindPk: Integer;
  FindMetr: Integer;
  Code: Integer;
  ID: Byte;
//  pData: PEventData;

begin
  FVF:= VF;
  Label1.Caption:= LangTable.Caption[Format('InfoBar:PathCoordSystem_id%d', [Ord(VF.DatSrc.CoordSys)])];

  if VF.DatSrc.CoordSys = csMetricRF then Panel2.BringToFront
                                     else Panel1.BringToFront;
  for I:= 0 to High(FVF.DatSrc.CoordList) do
    cbKm.Items.AddObject(IntToStr(FVF.DatSrc.CoordList[I].KM), Pointer(I));

  cbKm.ItemIndex:= FVF.FLastSelKM;
  cbKmChange(nil);
  cbPk.ItemIndex:= FVF.FLastSelPk;
  cbPkChange(nil);
  Edit2.Text:= FVF.FLastSelMetre;

  Edit1.Text:= FVF.FLastSelCa;
  if Edit1.Text = '' then Edit1.Text:= CaCrdToStr(VF.DatSrc.CoordSys, FVF.DatSrc.StartCaCrd);
end;

procedure TGotoForm.OnChangeLanguage(Sender: TObject);
begin
  Self.Font.Name              := LangTable.Caption['General:FontName'];
  Self.Caption                := LangTable.Caption['Goto:Gototocoord'];
  Label13.Caption             := LangTable.Caption['Goto:Newcoord'];
  Label2.Caption              := LangTable.Caption['Common:km'];
  Label11.Caption             := LangTable.Caption['Common:pk'];
  Label12.Caption             := LangTable.Caption['Common:m'];
  Button2.Caption             := LangTable.Caption['Common:Cancel'];
  Label6.Caption              := LangTable.Caption['Goto:Range'];
end;

procedure TGotoForm.FormCreate(Sender: TObject);
begin
  OnChangeLanguage(nil);
end;

procedure TGotoForm.cbKmChange(Sender: TObject);
var
  I, J: Integer;

begin
  FVF.FLastSelKM:= cbKm.ItemIndex;

  cbPk.Items.Clear;
  J:= Integer(cbKm.Items.Objects[cbKm.ItemIndex]);
  for I:= 0 to High(FVF.DatSrc.CoordList[J].Content) do
    cbPk.Items.AddObject(IntToStr(FVF.DatSrc.CoordList[J].Content[I].Pk), Pointer(I));
  if Sender <>  nil then cbPk.ItemIndex:= 0;
  cbPkChange(nil);
end;

procedure TGotoForm.cbPkChange(Sender: TObject);
var
  I, J: Integer;

begin
  if Sender <>  nil then FVF.FLastSelPk:= cbPk.ItemIndex;

  if (cbKm.ItemIndex = - 1) or (cbPk.ItemIndex = - 1) then Exit;
  I:= Integer(cbKm.Items.Objects[cbKm.ItemIndex]);
  J:= Integer(cbPk.Items.Objects[cbPk.ItemIndex]);

  if FVF.DatSrc.Header.MoveDir < 0 then
    begin
         if (I = cbKm.Items.Count - 1) and (J = cbPk.Items.Count - 1)
           then Label7.Caption:= Format('100...%3.1f', [100 - FVF.DatSrc.CoordList[I].Content[J].Len / 1000])
           else Label7.Caption:= Format('%3.1f...0', [FVF.DatSrc.CoordList[I].Content[J].Len / 1000]);
    end
    else
    begin
         if (cbKm.ItemIndex = 0) and (cbPk.ItemIndex = 0)
           then Label7.Caption:= Format('%d...%3.1f', [FVF.DatSrc.Header.StartMetre, FVF.DatSrc.Header.StartMetre + FVF.DatSrc.CoordList[I].Content[J].Len / 1000])
           else Label7.Caption:= Format('0...%3.1f', [FVF.DatSrc.CoordList[I].Content[J].Len / 1000]);
    end;
end;

procedure TGotoForm.Button1Click(Sender: TObject);
var
  Metr: Extended;
  Code: Integer;
  I, J: Integer;
  MRFCrd: TMRFCrd;
  CaCrd: TCaCrd;

begin

  if FVF.DatSrc.CoordSys = csMetricRF then
  begin

    if (cbKm.ItemIndex = - 1) or (cbPk.ItemIndex = - 1) then Exit;
    I:= Integer(cbKm.Items.Objects[cbKm.ItemIndex]);
    J:= Integer(cbPk.Items.Objects[cbPk.ItemIndex]);

    Val(Edit2.Text, Metr, Code);
    if Code = 0 then
    begin
      MRFCrd.Km:= StrToInt(cbKm.Items.Strings[cbKm.ItemIndex]);
      MRFCrd.Pk:= StrToInt(cbPk.Items.Strings[cbPk.ItemIndex]);
      MRFCrd.mm:= Round(Metr * 1000);
      FVF.DatSrc.MRFCrdToDisCrd(MRFCrd, ResDisCoord);
//      ResDisCoord:= FVF.DatSrc.SysToDisCoord(FVF.DatSrc.GetSysCoordByRealIdx(I, J, Metr));
      ModalResult:= mrOk;
    end
    else
    begin
      Edit2.SetFocus;
      Exit;
    end;
  end
  else
  begin

    CaCrd:= StrToCaCrd(FVF.DatSrc.CoordSys, Edit1.Text);
//    Val(Edit1.Text, Metr, Code);
//    if Code = 0 then
    begin
   //   CaCrd.XXX:= Trunc(Metr);
   //   CaCrd.YYY:= Trunc(Frac(Metr) * 1000);
      FVF.DatSrc.CaCrdToDis(CaCrd, ResDisCoord);

//      ResDisCoord:= FVF.DatSrc.SysToDisCoord(FVF.DatSrc.GetSysCoordByRealIdx(I, J, Metr));

      ModalResult:= mrOk;
    end
{    else
    begin
      Edit1.SetFocus;
      Exit;
    end} ;
  end
end;

procedure TGotoForm.Edit1Change(Sender: TObject);
begin
  FVF.FLastSelCa:= Edit1.Text;
end;

procedure TGotoForm.Edit2Change(Sender: TObject);
begin
  FVF.FLastSelMetre:= Edit2.Text;
end;

end.
