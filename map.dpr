{$I DEF.INC}
program map;

uses
  Forms,
  Windows,
  MainUnit in 'MainUnit.pas' {MainForm},
  Avk11ViewUnit in 'Avk11ViewUnit.pas' {Avk11ViewForm},
  ConfigUnit in 'ConfigUnit.pas' {ConfigForm},
  DisplayUnit in 'DisplayUnit.pas',
  CoordGraphUnit in 'CoordGraphUnit.pas' {CoordGraphForm},
  FileBaseUnit in 'FileBaseUnit.pas' {FileBaseForm},
  BaseCfg2Unit in 'BaseCfg2Unit.pas' {BaseCfg2Form},
  CreateBaseProg in 'CreateBaseProg.pas' {CreateBaseProgForm},
  NewEntry in 'NewEntry.pas' {NewEntryForm},
  NoteBook in 'NoteBook.pas' {NoteBookForm},
  GotoPos in 'GotoPos.pas' {GotoForm},
  HandScan in 'HandScan.pas' {HandScanForm},
  CreateReportClass in 'CreateReportClass.pas',
  ViewEventUnit in 'ViewEventUnit.pas' {ViewEventForm},
  ViewFileErrorUnit in 'ViewFileErrorUnit.pas' {ViewFileErrorForm},
  DataBase in 'DataBase.pas',
  BoxerUnit in 'BoxerUnit.pas',
  InfoBarUnit in 'InfoBarUnit.pas' {InfoBarForm},
  FileInfoUnit in 'FileInfoUnit.pas' {FileInfoForm},
  FileAnUnit in 'FileAnUnit.pas' {FileAnForm},
  AssignCtrl in 'AssignCtrl.pas' {AssignCtrlForm},
  PGBarExport in 'PGBarExport.pas' {GPExportForm},
  Lupa in 'Lupa.pas' {LupaForm},
  SaveAs in 'SaveAs.pas' {SaveAsForm},
  KuGraphUnit in 'KuGraphUnit.pas' {KuGraphForm},
  MyNotes in 'MyNotes.pas',
  FiltrUnit in 'FiltrUnit.pas',
  FiltrParamUnit in 'FiltrParamUnit.pas' {FiltrParamForm},
  FlahCardUnit2 in 'FlahCardUnit2.pas' {FlashCardForm2},
  CfgTablesUnit_2010 in 'CfgTablesUnit_2010.pas',
  AbountUnit_ in 'AbountUnit_.pas' {AboutForm_},
  MyTypes in 'MyTypes.pas',
  CommonUnit in 'CommonUnit.pas',
  EventListUnit in 'EventListUnit.pas' {EventListForm},
  DebugFormUnit in 'DebugFormUnit.pas' {DebugForm},
  NordcoCSV in 'NordcoCSV.pas',
  InfoBarUnit2 in 'InfoBarUnit2.pas' {InfoBarForm2},
  RecProgressUnit in 'RecProgressUnit.pas' {RecProgressForm},
  AutodecUnit in 'AutodecUnit.pas',
  ElementsCollection in 'ElementsCollection.pas' {ElementsCollectionForm},
  AutodecDataUnit in 'AutodecDataUnit.pas',
  DefectWarningMessageUnit in 'DefectWarningMessageUnit.pas' {DefectWarningMessageForm},
  cUnicodeCodecs in 'FundamentalsUnicode416\cUnicodeCodecs.pas',
  mapView in 'mapView.pas',
  UCSUnit in 'UCSUnit.pas' {UCSForm},
  OperatorLog in 'OperatorLog.pas' {OperatorLogForm},
  TemperatureChartUnit in 'TemperatureChartUnit.pas' {TemperatureChart},
  VideoViewWin in 'VideoViewWin.pas' {VideoView},
  PictureViewWin in 'PictureViewWin.pas' {PictureView},
  AudioViewWin in 'AudioViewWin.pas' {AudioView},
  HeadScan in 'HeadScan.pas' {HeadScanForm},
  LanguageUnit in 'LanguageUnit.pas',
  AviconTypes in '..\Components\Avicon Engine\AviconTypes.pas',
  AviconDataSource in '..\Components\Avicon Engine\AviconDataSource.pas',
  DataFileConfig in '..\Components\Avicon Engine\DataFileConfig.pas',
  CheckFileAccessUnit in 'CheckFileAccessUnit.pas',
  AutodecDrawerUnit in 'AutodecDrawerUnit.pas',
  AutodecTypes in 'AutodecTypes.pas',
  EVideoUnit in 'EVideoUnit.pas' {EVideoForm};

{$R map.res}
{$R FuckUAC.Res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TDebugForm, DebugForm);
  Application.CreateForm(TRecProgressForm, RecProgressForm);
  Application.CreateForm(TDefectWarningMessageForm, DefectWarningMessageForm);
  Application.CreateForm(TUCSForm, UCSForm);
  Application.CreateForm(TOperatorLogForm, OperatorLogForm);
  Application.CreateForm(TTemperatureChart, TemperatureChart);
  Application.Run;
end.
