unit ElementsCollection;

interface

uses
  Windows, Messages, SysUtils, Generics.Collections, Variants, Classes,
  Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, CheckLst, Types, MyTypes,
  Avk11ViewUnit, ImgList, TB2Dock, TB2Toolbar, AviconDataSource, TB2ToolWindow,
  TB2Item, Menus, LanguageUnit, ConfigUnit, AutodecTypes, AutodecDataUnit;

type
  TSortBy = (
    byCoordinate,
    byType,
    byRail,
    byLength,
    byDescription,
    noSorting
    );

  TSortDir = (
    Ascent,
    Descent
    );

  Comparator = function(index1, index2: Integer): Boolean of object;

  TResItem = record
    R: RRail;
    classId: Integer;
    Bounds: TRange;
    Len: Integer;
    Params: string;
    inHiddenZone: boolean;
  end;

  TRecRes = array of TResItem;

  TEngine = class
  public
    Res: TRecRes;
    FDatSrc: TAvk11DatSrc;
  end;

  TElementsCollectionForm = class(TForm)
    ImageList2: TImageList;
    Panel2: TPanel;
    ImageList1: TImageList;
    Panel5: TPanel;
    TBToolbar1: TTBToolbar;
    tbForward: TTBItem;
    TBToolbar3: TTBToolbar;
    tbBackward: TTBItem;
//    tbObjView: TTBSubmenuItem;
    Panel6: TPanel;
    Panel7: TPanel;
    TBToolbar5: TTBToolbar;
    tbViewMode: TTBSubmenuItem;
    Panel8: TPanel;
    ListView: TListView;
    TBToolbar2: TTBToolbar;
    TBItem1: TTBItem;
    Panel1: TPanel;
    bottomString: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure OnItemClick1(Sender: TObject);
    procedure OnItemClick2(Sender: TObject);
    procedure SelItem(SelIdx: Integer);
    procedure tbBackward_Click(Sender: TObject);
    procedure tbForward_Click(Sender: TObject);
//    procedure OnSysClick(Sender: TObject);
    procedure UpdateList;
    function isHideItem(idx: integer): boolean;
    procedure ListViewData(Sender: TObject; Item: TListItem);
    procedure ListViewClick(Sender: TObject);
    procedure OnSelResItem(Sender: TObject);
    procedure TBItem1Click(Sender: TObject);
    function ResTypeToGroup(classId: Integer): string;
    procedure ShowForm();
    procedure HideForm();
    procedure Clear();
    function verticalLine(rail: RRail): Integer;
    procedure ListViewColumnClick(Sender: TObject; Column: TListColumn);
    procedure qSortRes(var A: TRecRes; min, max: Integer);
    procedure ListViewCustomDrawItem(Sender: TCustomListView; Item: TListItem;
      State: TCustomDrawState; var DefaultDraw: Boolean);

  private
    FRE: TEngine;
    FRE_orig: TEngine;
    FVF: TAvk11ViewForm;
    FLink: TIntegerDynArray;
    FViewMode: Integer;
    FDock: TTBDock;
//    ViewObj: array[TClassGroup] of Boolean;
    railVerticalLine: RRail;
    coordVerticalLine: Integer;
    sortBy: TSortBy;
    sortDir: TSortDir;
    comparatorTable: array[TSortBy] of array[TSortDir] of Comparator;
    columnToSort: Integer;
    columnCaptions: array of string;

    procedure CreateList;
    function GetResPos(Idx: Integer): Integer;
    function GetCoordinate(res: TResItem): Integer;
    procedure CMChildKey(var Message: TCMChildKey); message CM_CHILDKEY;
    function SearchInRes(Crd, Dir: Integer): Integer;
    procedure sortList();
    function emptyComparator(index1, index2: Integer): Boolean;
    function coordComparatorAscent(index1, index2: Integer): Boolean;
    function coordComparatorDescent(index1, index2: Integer): Boolean;
    function typeComparatorAscent(index1, index2: Integer): Boolean;
    function typeComparatorDescent(index1, index2: Integer): Boolean;
    function lengthComparatorAscent(index1, index2: Integer): Boolean;
    function lengthComparatorDescent(index1, index2: Integer): Boolean;
    function railComparatorAscent(index1, index2: Integer): Boolean;
    function railComparatorDescent(index1, index2: Integer): Boolean;
    function descriptionComparatorAscent(index1, index2: Integer): Boolean;
    function descriptionComparatorDescent(index1, index2: Integer): Boolean;
    function equalData(eng1, eng2: TEngine): Boolean;
    procedure copyData(src, dst: TEngine);
//    procedure initObjView;
    function isClassShowed(classId: integer): boolean;

  public
    FChgByList: Boolean;
//    FChgByListCrd: Integer;

    procedure SetData(RE: TEngine; VF: TAvk11ViewForm);
    procedure PrevObj;
    procedure NextObj;
  end;

//const
//  groupNames: array[TClassGroup] of string = (
//    '��� ������',
//    '��������� ���������',
//    '����',
//    '�������',
//    '���������� �� ������',
//    '���� ��������',
//    '��������� ���������� ��������'
//  );

implementation

{$R *.dfm}

uses
  NewEntry, MainUnit, AviconTypes;

procedure TElementsCollectionForm.CMChildKey(var Message: TCMChildKey);
begin
  if (Message.Sender is TWinControl) and
    (Message.CharCode in [VK_RIGHT,
    VK_LEFT,
      VK_UP,
      VK_DOWN,
      VK_END,
      VK_HOME,
      VK_PRIOR,
      VK_NEXT,
      VK_RETURN]) then
  begin
    Message.Result := 1;
    FVF.SetFocus_;
  end
  else
    inherited;

end;

procedure TElementsCollectionForm.FormCreate(Sender: TObject);
var
  I: TClassGroup;
  J: Integer;
  sb: TSortBy;
  sd: TSortDir;
  AItem: TTBCustomItem;
  Item: TTBItem;
  FToolWin: TTBToolWindow;
begin
  // -----------------------------------------------------------------------------
  FDock := TTBDock.Create(nil);
  FDock.Parent := MainForm;
  FDock.Position := dpBottom;
  FDock.Visible := False;
  FDock.FixAlign := True;

  FToolWin := TTBToolWindow.Create(nil);
  FToolWin.Parent := FDock;
  FToolWin.Visible := True;
  FToolWin.DefaultDock := FDock;
  FToolWin.BorderStyle := bsNone;
  FToolWin.DockableTo := [];
  FToolWin.DockMode := dmCannotFloat;
  FToolWin.DragHandleStyle := dhNone;
  FToolWin.FullSize := True;

  Self.BorderStyle := bsNone;
  Self.Parent := FToolWin;
  Self.Visible := True;
  FToolWin.Width := Self.Width;
  FToolWin.Height := 200;
  Self.Align := alClient;
  // -----------------------------------------------------------------------------
//  for i := low(TClassGroup) to high(TClassGroup) do
//    ViewObj[i] := false;
//  ViewObj[cgDefect] := true;

//  initObjView;

  FChgByList := False;
  coordVerticalLine := -1;
  FRE_orig := TEngine.Create();

  // �������� �������� �������� � ������
  setLength(columnCaptions, listView.Columns.Count);
  for j := 1 to ListView.Columns.Count do
    columnCaptions[j - 1] := listView.Columns.Items[j - 1].Caption;

  sortBy := byCoordinate;
  sortDir := Ascent;
  ListViewColumnClick(listView, listView.Columns.Items[4]);

  // ������������� ������������� ������������
  for sb := Low(TSortBy) to High(TSortBy) do
    for sd := Low(TSortDir) to High(TSortDir) do
      comparatorTable[sb][sd] := emptyComparator;
  comparatorTable[byCoordinate][Ascent] := coordComparatorAscent;
  comparatorTable[byCoordinate][Descent] := coordComparatorDescent;
  comparatorTable[byType][Ascent] := typeComparatorAscent;
  comparatorTable[byType][Descent] := typeComparatorDescent;
  comparatorTable[byLength][Ascent] := lengthComparatorAscent;
  comparatorTable[byLength][Descent] := lengthComparatorDescent;
  comparatorTable[byRail][Ascent] := railComparatorAscent;
  comparatorTable[byRail][Descent] := railComparatorDescent;
  comparatorTable[byDescription][Ascent] := descriptionComparatorAscent;
  comparatorTable[byDescription][Descent] := descriptionComparatorDescent;

  tbBackward.ShortCut := ShortCut(Word('{'), []);
  tbForward.ShortCut := ShortCut(Word('}'), []);

  J := 0;
  tbViewMode.Clear;
  AItem := TTBCustomItem.Create(nil);
  AItem.Name := 'ViewMode_' + IntToStr(Ord(J));
  AItem.Tag := Ord(J);
  AItem.AutoCheck := True;
  AItem.Caption := '�� ������';
  AItem.OnClick := OnItemClick2;
  AItem.Checked := False;
  AItem.GroupIndex := 1;
  AItem.Tag := 0;
  tbViewMode.Add(AItem);

  J := 1;
  AItem := TTBCustomItem.Create(nil);
  AItem.Name := 'ViewMode_' + IntToStr(Ord(J));
  AItem.Tag := Ord(J);
  AItem.AutoCheck := True;
  AItem.Caption := '�� ������';
  AItem.OnClick := OnItemClick2;
  AItem.Checked := True;
  AItem.GroupIndex := 1;
  AItem.Tag := 1;
  tbViewMode.Add(AItem);

  J := 2;
  AItem := TTBCustomItem.Create(nil);
  AItem.Name := 'ViewMode_' + IntToStr(Ord(J));
  AItem.Tag := Ord(J);
  AItem.AutoCheck := True;
  AItem.Caption := '�� �����';
  AItem.OnClick := OnItemClick2;
  AItem.Checked := False;
  AItem.GroupIndex := 1;
  AItem.Tag := 2;
  tbViewMode.Add(AItem);

  FViewMode := 1;
end;

procedure TElementsCollectionForm.FormDestroy(Sender: TObject);
begin
  SetLength(FLink, 0);
//  tbObjView.Clear;
end;

{
procedure TElementsCollectionForm.OnSysClick(Sender: TObject);
var
  I: Integer;

begin
  case TTBItem(Sender).Tag of
    -1: for I := 0 to tbObjView.Count - 1 do
          if tbObjView.Items[I].Tag >= 0 then
          begin
            tbObjView.Items[I].Checked := True;
            ViewObj[TClassGroup(tbObjView.Items[I].Tag)] := True;
          end;
    -2: for I := 0 to tbObjView.Count - 1 do
          if tbObjView.Items[I].Tag >= 0 then
          begin
            tbObjView.Items[I].Checked := False;
            ViewObj[TClassGroup(tbObjView.Items[I].Tag)] := false;
          end;
  end;
  UpdateList;
end;
}

function TElementsCollectionForm.GetResPos(Idx: Integer): Integer;
begin
  Result := 0;
  case FViewMode of
    0: Result := FRE.Res[Idx].Bounds.StCrd; // �� ������
    1: Result := GetCentr(FRE.Res[Idx].Bounds); // �� ������
    2: Result := FRE.Res[Idx].Bounds.EdCrd; // �� �����
  end;
end;

function TElementsCollectionForm.GetCoordinate(res: TResItem): Integer;
begin
  Result := 0;
  case FViewMode of
    0: Result := res.Bounds.StCrd; // �� ������
    1: Result := GetCentr(res.Bounds); // �� ������
    2: Result := res.Bounds.EdCrd; // �� �����
  end;
end;

procedure TElementsCollectionForm.CreateList;
var
  I: Integer;
  Cap: Integer;
  Count: Integer;
  Len: Single;
  m, sm, mm: Integer;
  groupId: TClassGroup;
begin
  Count := -1;
  if Assigned(FRE) then
  begin
    // ��������� ������ �� ����������
    qSortRes(FRE.Res, 0, High(FRE.Res));

    Cap := 100;
    SetLength(FLink, Cap);
    for I := 0 to High(FRE.Res) do // ����������
      if isClassShowed(FRE.Res[I].classId) then
      begin
        Inc(Count);
        if Count >= Cap then
        begin
          Cap := Cap * 2;
          SetLength(FLink, Cap);
        end;
        FLink[Count] := I;
      end;

    SetLength(FLink, Count + 1);
    // ���������� �� ����������
    sortList();

    ListView.Items.Count := Length(FLink);

    // ����� ����������������� ��������
    Len := 0;
    for i := 0 to High(FRE.Res) do
    begin
      groupId := Rostov.getClassifierItem(FRE.Res[i].classId).group;
      if groupId = TClassGroup.cgDefect then
        Len := Len + GetLen(FRE.Res[i].Bounds) * FVF.DatSrc.Header.ScanStep / 100;
    end;

    m := Trunc(Len / 1000);
    sm := Trunc((Len - 1000 * m) / 10);
    mm := Round(Len) mod 10;
    bottomString.Caption := '                      ' +
      Format('����� ����������������� �������� ���������� %d � %d �� %d ��', [m,
      sm, mm]);
  end;
  if Count = -1 then
    ListView.Items.Count := 0;
  ListView.Enabled := Count <> -1;
end;

function TElementsCollectionForm.equalData(eng1, eng2: TEngine): Boolean;
var
  i: integer;
begin
  Result := true;
  if Length(eng1.Res) <> Length(eng2.Res) then
  begin
    Result := false;
    exit;
  end;

  for i := Low(eng1.Res) to High(eng1.Res) do
  begin
    if (eng1.Res[i].R <> eng2.Res[i].R)
      or
      (eng1.Res[i].classId <> eng2.Res[i].classId)
      or
      (eng1.Res[i].Bounds.StCrd <> eng2.Res[i].Bounds.StCrd) or
      (eng1.Res[i].Bounds.EdCrd <> eng2.Res[i].Bounds.EdCrd)
      or
      (eng1.Res[i].Len <> eng2.Res[i].Len)
      or
      (eng1.Res[i].Params <> eng2.Res[i].Params) then
    begin
      Result := false;
      exit;
    end;
  end;
end;

procedure TElementsCollectionForm.copyData(src, dst: TEngine);
var
  i: integer;
begin
  setLength(dst.Res, Length(src.Res));
  for i := Low(src.Res) to High(src.Res) do
    dst.Res[i] := src.Res[i];
end;

procedure TElementsCollectionForm.SetData(RE: TEngine; VF: TAvk11ViewForm);
begin
  if not equalData(RE, FRE_orig) then
    coordVerticalLine := -1;

  if FRE <> nil then
    FRE.Destroy();

  FRE := RE;
  copyData(RE, FRE_orig);
  FVF := VF;
  CreateList;
end;

{
procedure TElementsCollectionForm.OnItemClick1(Sender: TObject);
begin
  ViewObj[TClassGroup(TTBCustomItem(Sender).Tag)] := TTBCustomItem(Sender).Checked;
  UpdateList;
end;
}

procedure TElementsCollectionForm.OnItemClick2(Sender: TObject);
begin
  FViewMode := TTBCustomItem(Sender).Tag;
  UpdateList;
end;

procedure TElementsCollectionForm.UpdateList;
begin
  CreateList;
  ListView.Refresh;
  if Assigned(FVF) then
  begin
        FVF.SetFocus_;
//        FVF.Display.Refresh;
  end;
end;

procedure TElementsCollectionForm.tbBackward_Click(Sender: TObject);
var
  Idx: Integer;
begin
  if Assigned(FVF) then
  begin
    if not FChgByList then
    begin
      Idx := SearchInRes(FVF.Display.CenterDisCoord, -1);
      if (Idx >= 0) and (Idx < ListView.Items.Count) then
      begin
        ListView.Selected := ListView.Items[Idx];
        ListView.Selected.MakeVisible(True);
        SelItem(Idx);
      end;
    end
    else if Assigned(ListView.Selected) and (ListView.Selected.Index > 0) then
    begin
      idx := ListView.Selected.Index;
      for idx := ListView.Selected.Index - 1 downto 0 do
        if not isHideItem(Flink[idx]) then
          break;

      if  (idx < 0) or isHideItem(Flink[idx]) then
        exit;

      ListView.Selected := ListView.Items[idx];
      ListView.Selected.MakeVisible(True);
      SelItem(idx);
    end;
  end;
end;

procedure TElementsCollectionForm.tbForward_Click(Sender: TObject);
var
  Idx: Integer;

begin
  if Assigned(FVF) then
  begin
    if not FChgByList then
    begin
      Idx := SearchInRes(FVF.Display.CenterDisCoord, +1);
      if (Idx >= 0) and (Idx < ListView.Items.Count) then
      begin
        ListView.Selected := ListView.Items[Idx];
        ListView.Selected.MakeVisible(True);
        SelItem(Idx);
      end;
    end
    else if Assigned(ListView.Selected) then
    begin
      idx := ListView.Selected.Index;
      for idx := ListView.Selected.Index + 1 to ListView.Items.Count - 1 do
        if not isHideItem(Flink[idx]) then
          break;

      if (idx >= ListView.Items.Count) or isHideItem(Flink[idx]) then
        exit;

      ListView.Selected := ListView.Items[idx];
      ListView.Selected.MakeVisible(True);
      SelItem(idx);
    end;
  end;
end;

procedure TElementsCollectionForm.PrevObj;
begin
  //  tbBackwardClick(nil);
end;

procedure TElementsCollectionForm.NextObj;
begin
  //  tbForwardClick(nil);
end;

{
procedure TElementsCollectionForm.initObjView;
var
  sep: TTBSeparatorItem;
  item: TTBItem;
  customItem: TTBCustomItem;
  i: TClassGroup;
begin
  tbObjView.clear;

  item := TTBItem.Create(tbObjView);
  item.Caption := '�������� ��';
  item.ShortCut := ShortCut(Word('A'), [ssAlt]);
  item.OnClick := OnSysClick;
  item.Tag := -1;
  tbObjView.Add(item);

  item := TTBItem.Create(tbObjView);
  item.Caption := '����� ���������';
  item.ShortCut := ShortCut(Word('V'), [ssAlt]);
  item.OnClick := OnSysClick;
  item.Tag := -2;
  tbObjView.Add(item);

  sep := TTBSeparatorItem.Create(tbObjView);
  tbObjView.Add(sep);

//  for i := TClassGroup.cgHole to High(TClassGroup) do
  for i := low(TClassGroup) to High(TClassGroup) do
  begin
    customItem := TTBCustomItem.Create(nil);
    customItem.Name := 'ObjView_' + IntToStr(ord(i));
    customItem.Tag := ord(i);
    customItem.AutoCheck := True;
    customItem.Caption := groupNames[i];
    customItem.OnClick := OnItemClick1;
    customItem.Checked := ViewObj[i];
    tbObjView.Add(customItem);
  end;
end;
}

function TElementsCollectionForm.isClassShowed(classId: integer): boolean;
var
  groupId: TClassGroup;
begin
  result := Config.autodecShow[classId];
//  groupId := Rostov.getClassifierItem(classId).group;
//  result := ViewObj[groupId];
end;

function TElementsCollectionForm.SearchInRes(Crd, Dir: Integer): Integer;
var
  I, J: Integer;

begin
  Result := 0;
  J := MaxInt;
  for I := 0 to High(FLink) do
  begin
    if (Dir > 0) and
      not isHideItem(Flink[i]) and
      (Crd < GetResPos(FLink[I])) and
      (J > GetResPos(FLink[I]) - Crd) then
    begin
      J := Abs(Crd - GetResPos(FLink[I]));
      Result := I;
    end;

    if (Dir < 0) and
      not isHideItem(Flink[i]) and
      (Crd > GetResPos(FLink[I])) and
      (J > Crd - GetResPos(FLink[I])) then
    begin
      J := Abs(Crd - GetResPos(FLink[I]));
      Result := I;
    end;
  end;
  if (J = MaxInt) and (Dir < 0) then
    Result := 0;
  if (J = MaxInt) and (Dir > 0) then
    Result := High(FLink);
end;

function TElementsCollectionForm.isHideItem(idx: integer): boolean;
begin
    result := FVF.Display.SkipBackMotion and FRE.Res[idx].inHiddenZone;
end;

procedure TElementsCollectionForm.ListViewData(Sender: TObject; Item:
  TListItem);
var
  Len: Single;
  Idx: Integer;
  Crd: TRealCoord;
  GroupName: string;
  S: string;

begin
  if (Item.Index >= 0) and (Item.Index <= High(FLink)) then
  begin
    Idx := FLink[Item.Index];
    GroupName := ResTypeToGroup(FRE.Res[Idx].classId);

    // �������������� ������, ���� ��� ��������� � ��������� ����
    if isHideItem(idx) then
      ListView.Canvas.Font.Color:= clGray // clSilver
    else
      ListView.Canvas.Font.Color:= clBlack;

    Item.Caption := IntToStr(Item.Index + 1);
    Item.SubItems.Add(GroupName);

    S := FRE.Res[Idx].Params;
    if Pos(Chr(13), S) <> 0 then
      Delete(S, Pos(Chr(13), S), 255);

    Item.SubItems.Add(S);
    {
    if FRE.Res[Idx].Typ = rt_Defect then
      Item.SubItems.Add(S)
    else
      Item.SubItems.Add(ResNames[FRE.Res[Idx].Typ] + S);
    }

    case FRE.Res[Idx].R of
      r_Left: Item.SubItems.Add(LanguageUnit.LangTable.Caption['Common:Left']);
      r_Right:
        Item.SubItems.Add(LanguageUnit.LangTable.Caption['Common:Right']);
      r_Both:
        Item.SubItems.Add(LanguageUnit.LangTable.Caption['ToolBar:BothRail']);
    end;
    //    Item.SubItems.Add(RailToStr(FRE.FDatSrc.FileDevice, FRE.Res[Idx].R, 0));

    Crd := FRE.FDatSrc.DisToRealCoord(GetResPos(Idx));
    Item.SubItems.Add(Format('%d �� %d �� %d � %d ��', [Crd.MRFCrd.Km,
      Crd.MRFCrd.Pk, Crd.MRFCrd.MM div 1000,
        (Crd.MRFCrd.MM - 1000 * (Crd.MRFCrd.MM div 1000)) div 10]));

    Len := GetLen(FRE.Res[Idx].Bounds) * FVF.DatSrc.Header.ScanStep / 100 /
      1000;
    if Len < 1 then
      Item.SubItems.Add(Format('%d ��', [Round(Len * 1000)]))
    else
      Item.SubItems.Add(Format('%3.1f �', [Len]));

    Item.SubItems.Add(Format('St: %d - Ed: %d', [FRE.Res[Idx].Bounds.StCrd,
      FRE.Res[Idx].Bounds.EdCrd]));
  end;
end;

procedure TElementsCollectionForm.SelItem(SelIdx: Integer);
var
  Crd: Integer;
begin
  if isHideItem(Flink[selIdx]) then
  begin
    ListView.ClearSelection;
    FChgByList := false;
    exit;
  end;

  if Assigned(FRE) and Assigned(FVF) and (SelIdx <> -1) then
  begin
    Crd := GetResPos(FLink[SelIdx]);

    coordVerticalLine := Crd;
    railVerticalLine := FRE.Res[FLink[SelIdx]].R;

    FVF.Display.CenterDisCoord := Crd;
    FVF.Display.FullRefresh;
    FVF.SetFocus_;
    FChgByList := True;
//    FChgByListCrd := FVF.Display.CenterDisCoord;

    SetFocus;
  end;
end;

procedure TElementsCollectionForm.ListViewClick(Sender: TObject);
begin
  if Assigned(ListView.Selected) then
    if Assigned(FRE) and Assigned(FVF) then
        SelItem(ListView.Selected.Index);
end;

procedure TElementsCollectionForm.OnSelResItem(Sender: TObject);
var
  I: Integer;

begin
  for I := 0 to High(FLink) do
    if FLink[I] = Integer(Sender) then
    begin
      ListView.ItemIndex := I;
      ListView.Selected.MakeVisible(True);
      Break;
    end;
end;

procedure TElementsCollectionForm.TBItem1Click(Sender: TObject);
var
  Idx: Integer;

begin
  if Assigned(FVF) then
    if Assigned(ListView.Selected) then
      if (ListView.Selected.Index >= 0) and (ListView.Selected.Index <=
        High(FLink)) then
      begin
        Idx := FLink[ListView.Selected.Index];
        with TNewEntryForm.Create(nil) do
        begin
          Prepare(FVF, GetCentr(FRE.Res[Idx].Bounds), TRail(FRE.Res[Idx].R)
            {, FVF.DatSrc.GetRailPathNumber_in_String});
          // ������ RRail ����������� � TRail
          ShowModal;
          Free;
        end;
      end;
end;

function TElementsCollectionForm.ResTypeToGroup(classId: Integer): string;
begin
  result := Rostov.getClassifierItem(classId).name;

  //  case Typ of
  //    rt_OneHole, // ��������� ���������
  //    rt_Plug, // ��������
  //    rt_WeldedJoint, // ������� ����
  //    rt_BoltJoint, // �������� ����
  //    rt_FlangeRail, // ����� �����
  //    rt_Blade, // ������ ����������� ��������
  //    rt_Switch, // �������
  //    rt_Frog, // ����������
  //    rt_SupportingRail: // ������ �����
  //      Result := '�������������� �������';
  //
  //    rt_Defect: // ������
  //      Result := '���������� �� ������';
  //
  //    rt_UCSite, // ��������������������� �������
  //    rt_TEDoublePass, // ��� �������� ������� �� ��
  //    rt_TESkipBJButton, // �� ������ ������ ��
  //    rt_TESens, // ��������� ����������������
  //    rt_TESpeed, // ��������� �������� ��������
  //    rt_TEBSLost:
  //      Result := '��������� ���������� ��������';
  //  end;
end;

procedure TElementsCollectionForm.ShowForm();
begin
  FDock.Show();
end;

procedure TElementsCollectionForm.HideForm();
begin
  FDock.Hide();
end;

procedure TElementsCollectionForm.Clear();
begin
  if Assigned(FRE) then
    setLength(FRE.Res, 0);
  setLength(FLink, 0);
  listView.Clear();
end;

function TElementsCollectionForm.verticalLine(rail: RRail): Integer;
begin
  Result := -1;
  if (railVerticalLine = r_Both) or (rail = railVerticalLine) then
    Result := coordVerticalLine;
end;

procedure TElementsCollectionForm.sortList();
var
  I, J, L: Integer;
begin
  // ������������� ������� �������
//  for I := 0 to High(FLink) do
//    FLink[i] := I;

  // ���������� ������� �������� ������� ��������
  for I := 1 to High(FLink) do
  begin
    for J := I - 1 downto 0 do
    begin
      if not comparatorTable[sortBy][sortDir](J, J + 1) then
        break;

      L := FLink[J];
      FLink[J] := FLink[J + 1];
      FLink[J + 1] := L;
    end;
  end;
end;

function TElementsCollectionForm.emptyComparator(index1, index2: Integer):
  Boolean;
begin
  Result := false;
end;

function TElementsCollectionForm.coordComparatorAscent(index1, index2: Integer):
  Boolean;
begin
  Result := GetResPos(FLink[index1]) > GetResPos(FLink[index2]);
end;

function TElementsCollectionForm.coordComparatorDescent(index1, index2:
  Integer): Boolean;
begin
  Result := GetResPos(FLink[index1]) < GetResPos(FLink[index2]);
end;

function TElementsCollectionForm.typeComparatorAscent(index1, index2: Integer):
  Boolean;
begin
  Result := (CompareText(ResTypeToGroup(FRE.Res[FLink[index1]].classId),
    ResTypeToGroup(FRE.Res[FLink[index2]].classId)) > 0);
end;

function TElementsCollectionForm.typeComparatorDescent(index1, index2: Integer):
  Boolean;
begin
  Result := (CompareText(ResTypeToGroup(FRE.Res[FLink[index1]].classId),
    ResTypeToGroup(FRE.Res[FLink[index2]].classId)) < 0);
end;

function TElementsCollectionForm.lengthComparatorAscent(index1, index2:
  Integer): Boolean;
begin
  Result := (FRE.Res[FLink[index1]].Len > FRE.Res[FLink[index2]].Len);
end;

function TElementsCollectionForm.lengthComparatorDescent(index1, index2:
  Integer): Boolean;
begin
  Result := (FRE.Res[FLink[index1]].Len < FRE.Res[FLink[index2]].Len);
end;

function TElementsCollectionForm.railComparatorAscent(index1, index2: Integer):
  Boolean;
begin
  Result := (FRE.Res[FLink[index1]].R > FRE.Res[FLink[index2]].R);
end;

function TElementsCollectionForm.railComparatorDescent(index1, index2: Integer):
  Boolean;
begin
  Result := (FRE.Res[FLink[index1]].R < FRE.Res[FLink[index2]].R);
end;

function TElementsCollectionForm.descriptionComparatorAscent(index1, index2:
  Integer): Boolean;
begin
  Result := (CompareText(FRE.Res[FLink[index1]].Params,
    FRE.Res[FLink[index2]].Params) > 0);
end;

function TElementsCollectionForm.descriptionComparatorDescent(index1, index2:
  Integer): Boolean;
begin
  Result := (CompareText(FRE.Res[FLink[index1]].Params,
    FRE.Res[FLink[index2]].Params) < 0);
end;

procedure TElementsCollectionForm.ListViewColumnClick(Sender: TObject; Column:
  TListColumn);
var
  arrow: WideChar;
  j: Integer;
begin
  case Column.Index of
    1: sortBy := byType;
    2: sortBy := byDescription;
    3: sortBy := byRail;
    4: sortBy := byCoordinate;
    5: sortBy := byLength;
  else
    sortBy := noSorting;
  end;

  if ColumnToSort = Column.Index then
  begin
    if sortDir = Ascent then
      sortDir := Descent
    else
      sortDir := Ascent;
  end;

  case SortDir of
    Ascent: arrow := WideChar(8593);
  else
    arrow := WideChar(8595);
  end;

  ColumnToSort := Column.Index;

  for j := 1 to ListView.Columns.Count do
    listView.Columns.Items[j - 1].Caption := columnCaptions[j - 1];
  Column.Caption := columnCaptions[Column.Index] + ' ' + arrow;

  UpdateList();
end;

procedure TElementsCollectionForm.ListViewCustomDrawItem(
  Sender: TCustomListView; Item: TListItem; State: TCustomDrawState;
  var DefaultDraw: Boolean);
begin
  defaultDraw := true;
end;

procedure TElementsCollectionForm.qSortRes(var A: TRecRes; min, max: Integer);
var
  i, j: Integer;
  supp, tmp: TResItem;

  function compareLess(el1, el2: TResItem): Boolean;
  begin
    Result := getCoordinate(el1) < getCoordinate(el2)
  end;

  function compareGreat(el1, el2: TResItem): Boolean;
  begin
    Result := getCoordinate(el1) > getCoordinate(el2)
  end;

begin
  if Length(A) = 0 then
    Exit;

  supp := A[max - ((max - min) div 2)];
  i := min;
  j := max;
  while i < j do
  begin
    while compareLess(A[i], supp) do
      i := i + 1;
    while compareGreat(A[j], supp) do
      j := j - 1;

    if i <= j then
    begin
      tmp := A[i];
      A[i] := A[j];
      A[j] := tmp;
      i := i + 1;
      j := j - 1;
    end;
  end;

  if min < j then
    qSortRes(A, min, j);
  if i < max then
    qSortRes(A, i, max);
end;

end.

