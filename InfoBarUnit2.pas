﻿{$I DEF.INC}
unit InfoBarUnit2; {Language 12}

interface

uses
  Windows, Types, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ExtCtrls, StdCtrls, TeeProcs, TeEngine, Chart, Grids, Math,
  Buttons, ValEdit, ComCtrls, MyTypes, ConfigUnit, LanguageUnit,
  Series, TeeShape, DisplayUnit, AviconDataSource, AviconTypes;

type
  TInfoBarForm2 = class(TForm)
    FileInfoPanel: TPanel;
    SpeedPanel: TPanel;
    Bevel12: TBevel;
    Label2: TLabel;
    Bevel3: TBevel;
    TempPanel: TPanel;
    Bevel6: TBevel;
    Bevel5: TBevel;
    LDhV: TLabel;
    LDhN: TLabel;
    Bevel4: TBevel;
    LDyN: TLabel;
    LDyV: TLabel;
    TimePanel: TPanel;
    Bevel11: TBevel;
    Label40V: TLabel;
    Label43: TLabel;
    Label26: TLabel;
    Label1: TLabel;
    Bevel2: TBevel;
    procedure FormCreate(Sender: TObject);
    procedure ChangeMouseMode(Sender: TObject);

  private
    AScan1: array of TChartShape;
    AScan2: array of TChartShape;
    HSAScan: array of TChartShape;
    BlackItem1: array of TPoint;
    BlackItem2: array of TPoint;
    FOnInfoClick: TNotifyEvent;
    FOnNewRecordClick: TNotifyEvent;
    FCoordMode: Boolean;

    FvFileInfo: Boolean;
    FvNotebook: Boolean;
    FvRail: Boolean;
    FvCoord1: Boolean;
    FvCoord2: Boolean;
    FvTime: Boolean;

    FBuffer: TBitMap;
    FDatSrc: TAvk11DatSrc;
    FDisplay: TAvk11Display;
    FSavePageIdx: Integer;


  public
    DebugInfo: Boolean;
    AmplThTrackBarPosition: Integer;

    procedure RefreshDat(X, Y: Integer);
    procedure SetControl(DatSrc: TAvk11DatSrc; Display: TAvk11Display);
    procedure OnChangeLanguage(Sender: TObject);

    property CoordMode: Boolean read FCoordMode write FCoordMode;

    property OnInfoClick: TNotifyEvent read FOnInfoClick write FOnInfoClick;
    property OnNewRecordClick: TNotifyEvent read FOnNewRecordClick write FOnNewRecordClick;
  end;

implementation

uses MainUnit, DebugFormUnit;

{$R *.dfm}

procedure KillSelection(StringGrid: TStringGrid);
var
  GridRect: TGridRect;

begin
  GridRect:= StringGrid.Selection;
  GridRect.Top:= 100;
  GridRect.Bottom:= 100;
  GridRect.Left:= 100;
  GridRect.Right:= 100;
  StringGrid.Selection:= GridRect;
end;

procedure TInfoBarForm2.FormCreate(Sender: TObject);
begin
  OnChangeLanguage(nil);
end;

procedure TInfoBarForm2.SetControl(DatSrc: TAvk11DatSrc; Display: TAvk11Display);
var
  I: Integer;

begin
  FDatSrc:= DatSrc;
  FDisplay:= Display;

  if (not Assigned(DatSrc)) or (not Assigned(Display)) then Exit;
end;

procedure TInfoBarForm2.RefreshDat(X, Y: Integer);
var
  R, RRR: TRail;
  Ch: Integer;
  I, J, I1, I2: Integer;
  S: string;
  DisCoord1, DisCoord2, Ch1Delay1, Ch1Delay2, Ch2Delay1, Ch2Delay2, Ch1, Ch2: Integer;
  SameRect: Boolean;
  Flg: Boolean;
  FHName1: string;
  FHName2: string;
  ResDisCoord: Int64;
  ResSysCoord, ResOffset: Integer;
  Col, Row, ChNum: Integer;
  RC: TRealCoord;
  VVV: Integer;
  ResDtD: TIntegerDynArray;
  SpeedExists: Boolean;
  TempExists: Boolean;
  Temp: Single;
  Speed: Single;
  State: Integer;

begin
  if (not Assigned(FDatSrc)) or (not Assigned(FDisplay)) then Exit;

  case FDisplay.MouseMode of

          mmIdle,
  mmSelRailPoint: begin

                   SpeedExists:= FDatSrc.GetSpeedState(FDisplay.MousePosDat.MouseDisCoord, Speed, State);
                    if FDatSrc.GetTemperature(FDisplay.MousePosDat.MouseDisCoord, Temp)
                      then LDyV.Caption:= FloatToStr(Trunc(Temp)) + '°C'
                      else LDyV.Caption:= '-';

                    Label40V.Caption:= FDatSrc.GetTime(FDisplay.MousePosDat.MouseDisCoord);

                    if SpeedExists then
                    begin
                      if Speed < 10 then Label2.Caption:= Format('%3.1f', [Speed])
                                    else Label2.Caption:= Format('%3.0f', [Speed]);
                      Label1.Caption:= LangTable.Caption['Common:km/h'];

                      if State = 1 then Label2.Font.Color:= clRed
                                   else Label2.Font.Color:= clBlack;
                    end
                    else
                    begin
                      Label2.Caption:= '-';
                      Label2.Font.Color:= clBlack;
                      Label1.Caption:= LangTable.Caption['Common:km/h'];
                    end;
                  end;
  end;
end;

procedure TInfoBarForm2.ChangeMouseMode(Sender: TObject);
begin
  if (not Assigned(FDatSrc)) or (not Assigned(FDisplay)) then Exit;
end;

procedure TInfoBarForm2.OnChangeLanguage(Sender: TObject);
begin
  Self.Font.Name              := LangTable.Caption['General:FontName'];
  Label40V.Font.Name          := LangTable.Caption['General:FontName'];
  Label2.Font.Name            := LangTable.Caption['General:FontName'];
  LDyV.Font.Name              := LangTable.Caption['General:FontName'];
  LDhV.Font.Name              := LangTable.Caption['General:FontName'];
  Label43.Caption             := LangTable.Caption['Common:Time'];
  Label26.Caption             := LangTable.Caption['Common:Speed'];
  LDyN.Caption                := LangTable.Caption['Common:Temperature'];
end;

end.
