unit VideoViewWin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, ComCtrls, StdCtrls, Buttons,
  DirectShow9, ActiveX;

type
  TPlayerMode = (Stop, Play, Paused); // ����� ���������������
  TVideoView = class(TForm)
    OpenDialog1: TOpenDialog;
    Timer1: TTimer;
    Panel2: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Panel4: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    BasePanel: TPanel;
    Panel1: TPanel;
    procedure Initializ;
    procedure Player;
//    procedure AddPlayList;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Panel1Resize(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer; var Resize: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    FOnCloseMediaForm: TNotifyEvent;

  public
    procedure Open_and_Play(FN: string; OnCloseMediaForm: TNotifyEvent);
  end;

var
  VideoView: TVideoView;
  hr: HRESULT = 1; //������ ��������� �������� ����
  pCurrent, pDuration: Double;// ������� ��������� � ������������ ������
  Mode: TPlayerMode; // ����� ���������������
  Rate: Double;// ���������� �������� ���������������
  FullScreen: boolean = false; //��������� �������� � ������������� �����
  i: integer = 0;// ������� ����������� ������
  FileName: string;//��� �����
  xn, yn : integer; //��� �������� ��������� ����
  mouse: tmouse; //���������� ����

  //���������� ��� ���������� � ���������� ������
  pGraphBuilder        : IGraphBuilder         = nil; //��� ����
  pMediaControl        : IMediaControl         = nil; //���������� ������
  pMediaEvent          : IMediaEvent           = nil; //���������� �������
  pVideoWindow         : IVideoWindow          = nil; //������ ���� ��� ������
  pMediaPosition       : IMediaPosition        = nil; //������� ������������
  pBasicAudio          : IBasicAudio           = nil; //���������� ������

implementation

{$R *.dfm}

procedure TVideoView.Initializ;
//��������� ���������� �����
begin
//����������� ������������ ����������
  if Assigned(pMediaPosition) then pMediaPosition := nil;
  if Assigned(pBasicAudio) then pBasicAudio  := nil;
  if Assigned(pVideoWindow) then pVideoWindow := nil;
  if Assigned(pMediaEvent) then pMediaEvent := nil;
  if Assigned(pMediaControl) then pMediaControl := nil;
  if Assigned(pGraphBuilder) then pGraphBuilder := nil;
//�������� ��������� ���������� �����
  hr := CoCreateInstance(CLSID_FilterGraph, nil, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, pGraphBuilder);
  if hr<>0 then begin
    ShowMessage('Error');
   // ShowMessage('�� ������� ������� ����');
    exit;
  end;
//�������� ��������� ����������
  hr := pGraphBuilder.QueryInterface(IID_IMediaControl, pMediaControl);
  if hr<>0 then begin
    ShowMessage('Error');
  //  ShowMessage('�� ������� �������� ��������� IMediaControl');
    exit;
  end;
//�������� ��������� �������
   hr := pGraphBuilder.QueryInterface(IID_IMediaEvent, pMediaEvent);
   if hr<>0 then begin
    ShowMessage('Error');
   // ShowMessage('�� ������� �������� ��������� �������');
    exit;
  end;
//�������� ��������� ���������� ����� ������ �����
  hr := pGraphBuilder.QueryInterface(IID_IVideoWindow, pVideoWindow);
  if hr<>0 then begin
    ShowMessage('Error');
   // ShowMessage('�� ������� �������� IVideoWindow');
    exit;
  end;
//�������� ��������� ���������� ������
   hr := pGraphBuilder.QueryInterface(IBasicAudio, pBasicAudio);
  if hr<>0 then begin
    ShowMessage('Error');
   // ShowMessage('�� ������� �������� ����� ���������');
    exit;
  end;
//�������� ���������  ���������� �������� ������������
  hr := pGraphBuilder.QueryInterface(IID_IMediaPosition, pMediaPosition);
   if hr<>0 then begin
    ShowMessage('Error');
  //  ShowMessage('�� ������� �������� ��������� ���������� ��������');
    exit;
  end;
//��������� ���� ��� ������������
  hr := pGraphBuilder.RenderFile(StringToOleStr(PChar(filename)), '');
  if hr<>0 then begin
    ShowMessage('Error');
  //  ShowMessage('�� ������� ������������ ����');
    exit;
  end;

//����������� ������ � ����� �� ������
   pVideoWindow.Put_Owner(Panel1.Handle);//������������� "���������" ����, � ����� ������ Panel1
   pVideoWindow.Put_WindowStyle(WS_CHILD OR WS_CLIPSIBLINGS);//����� ����
   pVideoWindow.put_MessageDrain(Panel1.Handle);//��������� ��� Panel1 ����� �������� ��������� ����� ����
   pVideoWindow.SetWindowPosition(0,0,Panel1.ClientRect.Right,Panel1.ClientRect.Bottom); //�������
end;


procedure TVideoView.Player;
//��������� ������������ �����
begin
if mode<>paused then begin
//��������� ���������� �� ���� ����������� �� PlayList
//���� ���� �� ����������, �� �������
//if not FileExists(FileName) then begin ShowMessage('���� �� ����������');exit;end;
//����������� ����� ���������������
Initializ;
end;
//��������� ��������� ������������
pMediaControl.Run;
//�������� �������� ���������������
pMediaPosition.get_Rate(Rate);
//����������� ��������� ����� ��� �������������� �����
//Form1.Caption:=ExtractFileName(FileName);
//������������� ����� ��������������� PlayMode - play
mode:=play;
end;

//��������� �������� ������ � ��������
(*
procedure TVideoView.AddPlayList;
var
 j: Integer;
begin
OpenDialog1.Options:=[ofHideReadOnly,ofAllowMultiSelect,ofEnableSizing];
OpenDialog1.Title  := '�������� ������';
//������ ��� ������
OpenDialog1.Filter := '����� ����������� |*.mp3;*.wma;*.wav;*.vob;*.avi;*.mpg;*.mp4;*.mov;*.mpeg;*.flv;*.wmv;*.qt;|��� �����|*.*';
//��������� ���� PlayList �� ������ �� ���������� ����� ������� ������
//����� ������������� ����� ������ 0 (������ ������� � PlayList)
//������ �������� �����
if not OpenDialog1.Execute then exit;
     //���������� ��� ����� ������� ������ � ���������
     Filename:= OpenDialog1.FileName;
end;
*)

procedure TVideoView.FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer; var Resize: Boolean);
var
  tmp: Integer;

begin


  if BasePanel.Height * 1.25 > BasePanel.Width then
  begin
    tmp:= Round(BasePanel.Width / 1.25);
    Panel1.Left:= 0;
    Panel1.Top:= (BasePanel.Height - tmp) div 2;
    Panel1.Width:= BasePanel.Width;
    Panel1.Height:= tmp;
  end
  else
  begin
    tmp:= Round(BasePanel.Height * 1.25);
    Panel1.Left:= (BasePanel.Width - tmp) div 2;
    Panel1.Top:= 0;
    Panel1.Width:= tmp;
    Panel1.Height:= BasePanel.Height;
  end;

//  if (NewHeight <> Height) and
//     (NewWidth <> Width) then NewHeight:= Round(NewWidth / 1.25) else
//  if NewHeight <> Height then NewWidth:= Round(NewHeight * 1.25) else
//  if NewWidth <> Width then NewHeight:= Round(NewWidth / 1.25);
  Resize:= True;
end;

procedure TVideoView.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  pMediaControl.Stop;
  mode:=Stop;//������������� playmode -> ����
//����������� ������������ ����������
  if Assigned(pMediaPosition) then pMediaPosition := nil;
  if Assigned(pBasicAudio) then pBasicAudio  := nil;
  if Assigned(pVideoWindow) then pVideoWindow := nil;
  if Assigned(pMediaEvent) then pMediaEvent := nil;
  if Assigned(pMediaControl) then pMediaControl := nil;
  if Assigned(pGraphBuilder) then pGraphBuilder := nil;
//  player;
 //������ ��������� ��������� ������������
  if (self.Tag = 0) and Assigned(FOnCloseMediaForm) then FOnCloseMediaForm(self);
  Self.Release;
end;

procedure TVideoView.FormCreate(Sender: TObject);
begin
  CoInitialize(nil);// ���������������� OLE
  FOnCloseMediaForm:= nil;
end;

procedure TVideoView.FormDestroy(Sender: TObject);
begin
  CoUninitialize;// ������������������ OLE
end;

procedure TVideoView.FormShow(Sender: TObject);
var
  Resize: Boolean;
  W, H: Integer;

begin
  W:= self.Width;
  H:= self.Height;
  FormCanResize(Sender, W, H, Resize);
end;

procedure TVideoView.N3Click(Sender: TObject);
begin

end;


//��������� ��������� ������� ���� ������������ ��� ��������� �������� ������
procedure TVideoView.Panel1Resize(Sender: TObject);
begin
 if mode=play then
 begin
 pVideoWindow.SetWindowPosition(0,0,Panel1.ClientRect.Right,Panel1.ClientRect.Bottom);
end;
end;

//��������� ���������������
procedure TVideoView.SpeedButton1Click(Sender: TObject);
begin
  //��������� ���� ��������������� ��� ���� �� �������������
  //���������� �������� ��������������� � �������
  if mode=play then begin pMediaPosition.put_Rate(Rate);exit;end ;
  Player;
end;

//��������� �����
procedure TVideoView.SpeedButton2Click(Sender: TObject);
begin
 //��������� ���� �� ���������������
 if mode=play then
 begin
   pMediaControl.Pause;
   mode:=paused;//������������� playmode -> �����
 end;
end;

procedure TVideoView.Timer1Timer(Sender: TObject);
var
  TrackLen, TrackPos: Double;
  ValPos: Double;
  ValLen: Double;
  plVolume:Longint;
  db: Integer;

begin
  //��������� ����� ���������������, ���� �� Play �� �������
  if hr <> 0 then Exit;
  //����� ������������ ������
  //��������� ��� ����� ������ � ��������
  pMediaPosition.get_Duration(pDuration);
  //��������� ������� ������ ������ �� ������ ���������������
  pMediaPosition.get_CurrentPosition(pCurrent);
  //��������������� ���������� ������
  //���� ����� ������������ ����� ����� ������ �� �������,
  if pCurrent=pDuration then
  begin
    player;
  end;

  //������ ���������� �������
  TrackLen:=pDuration;
  TrackPos:=pCurrent;
  //��������� ������� � ����
  ValPos:=TrackPos / (24 * 3600);
  ValLen:=TrackLen / (24 * 3600);
  //������� ������ � ������� �� ����� � Label1 � Label2
  Label2.Caption:=FormatDateTime('hh:mm:ss',ValPos);
  Label3.Caption:=FormatDateTime('hh:mm:ss',ValLen);
end;

procedure TVideoView.Open_and_Play(FN: string; OnCloseMediaForm: TNotifyEvent);
begin
  FOnCloseMediaForm:= OnCloseMediaForm;
  Filename:= FN;
  SpeedButton1Click(nil);
end;

end.
