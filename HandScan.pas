{$I DEF.INC}

unit HandScan; {Language 13}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ImgList, ToolWin, ExtCtrls, Menus, Grids, StdCtrls,
  TeEngine, Series, TeeProcs, Chart, Math, MyTypes, LanguageUnit, InfoBarUnit, AviconDataSource, AviconTypes;

type
  THandScanForm = class(TForm)
    Button3: TButton;
    Button4: TButton;
    ScrollBar1: TScrollBar;
    Chart1: TChart;
    Series1: TLineSeries;
    Panel1: TPanel;
    PaintBox1: TPaintBox;
    procedure PaintBoxPaint(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure ScrollBar1Change(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure SetScrollBarMax;
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure ModifyInfo;
    procedure PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure PaintBoxMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PaintBoxMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure PaintBox1Click(Sender: TObject);
    procedure Panel1Resize(Sender: TObject);
  private
    FDataOK: Boolean;
    Zoom: Single;
    HSData: THSListItem;
    HeadSData: PHeadScanListItem;
    DataExists: Boolean;
    MouseX, MouseY: Integer;
    InfoRectFlag: Boolean;
    InfoRect: TRect;
    InfoRectDrawFlag: Boolean;
    FInfoBarForm: TInfoBarForm;
    SavePage: Integer;
    Buffer: TBitMap;
    Buffer2: TBitMap;
    FN: string;
    Idx: Integer;
    FDS: TAvk11DatSrc;

//    procedure WMEraseBkgnd(var message: TWMEraseBkgnd); message WM_ERASEBKGND;
  public
    AmplThTrackBarPosition: Integer;
    FormMode: Integer;

    function MinDelay: Integer;
    function MaxDelay: Integer;
    procedure SetFocus_;
    procedure OnChangeLanguage(Sender: TObject);
    procedure SetHSData(Data: THSListItem; InfoBarForm: TInfoBarForm; FN: string; DS_: TAvk11DatSrc; Idx_: Integer = - 1);
//    procedure SetHScanData(Data_: PHeadScanListItem; InfoBarForm: TInfoBarForm; FN: string; DS_: TAvk11DatSrc; Idx_: Integer = - 1);
    procedure FillInfoBar;
    procedure Refresh;
    procedure CreateImage;
    procedure CreateImage2;
    procedure ZoomIn;
    procedure ZoomOut;
    property DatSrc: TAvk11DatSrc read FDS write FDS;
  end;

implementation

uses
  MainUnit, ConfigUnit;

var
    NeedClear: Boolean;

{$R *.DFM}
{
procedure THandScanForm.WMEraseBkgnd(var message: TWMEraseBkgnd); // ��������� ����������� ���� �����
begin
  Message.Result:= 1;
//  if FormMode = 1 then Message.Result:= 1
                  else DefWindowProc(self.Handle, message.Msg, 0, 0);
end;
}
function THandScanForm.MinDelay: Integer;
begin
  if not Config.InvertDelayAxis then
  begin
    if HSData.Header.HandChNum in [0, 1] then Result:= 0
                                         else Result:= 0;
  end
  else
  begin
    if HSData.Header.HandChNum in [0, 1] then Result:= Round(255 / 3)
                                         else Result:= 255;
  end;
end;

function THandScanForm.MaxDelay: Integer;
begin
  if not Config.InvertDelayAxis then
  begin
    if HSData.Header.HandChNum in [0, 1] then MaxDelay:= Round(255 / 3)
                                         else MaxDelay:= 255;
  end
  else
  begin
    if HSData.Header.HandChNum in [0, 1] then MaxDelay:= 0
                                         else MaxDelay:= 0;
  end;
end;

procedure THandScanForm.SetHSData(Data: THSListItem; InfoBarForm: TInfoBarForm; FN: string; DS_: TAvk11DatSrc; Idx_: Integer = - 1);
var
  I, J: Integer;

begin

//g

  FDS:= DS_;
  DataExists:= True;
  FDataOK:= True;
  Self.FN:= FN;
//  Idx:= Idx_;
  Idx:= - 1;

  Zoom:= 1;
  HSData:= Data;
  FormMode:= 1;

//  if HSData.Header. EvalCh in [0, 1] then InfoBarForm.SetHSAScanDelayZone(0, 85)
//                                     else InfoBarForm.SetHSAScanDelayZone(0, 255);

  FInfoBarForm:= InfoBarForm;
  FillInfoBar;
  FormResize(nil);

  Panel1.Visible:= False;
  ScrollBar1.Visible:= True;
  OnMouseMove:= PaintBoxMouseMove;
  OnMouseDown:= PaintBoxMouseDown;
  OnMouseUp:= PaintBoxMouseUp;
//  OnPaint:= PaintBoxPaint;
//  OnResize:= FormResize;
end;
(*
procedure THandScanForm.SetHScanData(Data_: PHeadScanListItem; InfoBarForm: TInfoBarForm; FN: string; DS_: TAvk11DatSrc; Idx_: Integer = - 1);
var
  I, J: Integer;

begin
  FDS:= DS_;
  DataExists:= True;
  FDataOK:= True;
  Self.FN:= FN;
  Idx:= - 1;
  Zoom:= 1;
//  HSData:= Data;
  HeadSData:= Data_;

//  Image1.Picture.Assign(Data.Cross_Image);
//  Image2.Picture.Assign(Data.Vertically_Image);
//  Image3.Picture.Assign(Data.Horizontal_Image);
  FormMode:= 2;
//  Left:= 25;
//  Top:= 25;
//  Width:= 976;
//  Height:= 532;

  FInfoBarForm:= InfoBarForm;
  FillInfoBar;
  FormResize(nil);

  Panel1.Visible:= True;
  ScrollBar1.Visible:= False;
  OnMouseMove:= nil;
  OnMouseDown:= nil;
  OnMouseUp:= nil;
//  OnPaint:= nil;
//  OnResize:= nil;
end;
*)
procedure THandScanForm.FillInfoBar;
begin
  if not DataExists then Exit;
  with HSData do
  begin
//    case Header.Rail of
//      rLeft: FInfoBarForm.HSParam.Cells[1, 0]:= LangTable.Caption['common:traLeft']; // '�����';
//      rRight: FInfoBarForm.HSParam.Cells[1, 0]:= LangTable.Caption['Misc:Right']; // '������';
//    end;
//    FInfoBarForm.HSParam.Cells[1, 1]:= IntToStr(Header.HandChNum);
//    FInfoBarForm.HSParam.Cells[1, 2]:= IntToStr(Header.ScanSurface);
//    FInfoBarForm.HSParam.Cells[1, 3]:= IntToStr(Header.Att);
//    FInfoBarForm.HSParam.Cells[1, 4]:= IntToStr(Header.Ku);
//    FInfoBarForm.HSParam.Cells[1, 5]:= IntToStr(Header.TVG);
  end;
end;

procedure THandScanForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Buffer.Free;
  Buffer:= nil;
  Buffer2.Free;
  Buffer2:= nil;
  Self.Release;
end;

procedure THandScanForm.PaintBoxPaint(Sender: TObject);
begin
   if FormMode = 1 then begin if Assigned(Buffer) then Canvas.Draw(0, 0, Buffer); end
                   else begin {Canvas.FillRect(Rect(0, 0, Width, Height)); } if Assigned(Buffer2) then PaintBox1.Canvas.Draw(0, 0, Buffer2); end;

end;

procedure THandScanForm.Panel1Resize(Sender: TObject);
begin
  PaintBox1Paint(nil);
end;

procedure THandScanForm.CreateImage;
var
  Step, I, J, X, Y: Integer;
  OutWidth: Integer;
  OutRect: TRect;
  S: string;

begin
  if Assigned(Buffer) then
    with HSData, Buffer, Canvas do
    begin
      Font.Color:= Config.ActivDColors.Text;
      Brush.Style:= bsSolid;
      Brush.Color:= Config.ActivDColors.Fon;
      FillRect(Rect(0, 0, Width, Height));


      OutRect.Left:= 30;
      OutRect.Right:= Width - 15;
      OutRect.Top:= 10;
      OutRect.Bottom:= Height - 25;

      OutWidth:= OutRect.Right - OutRect.Left;

      Pen.Color:= Config.ActivDColors.Border;
      Pen.Style:= psSolid;
      Rectangle(OutRect);

      Brush.Style:= bsClear;
      for I:= 0 to 10 do
      begin
        S:= IntToStr(MinDelay + (MaxDelay - MinDelay) * (10 - I) div 10);
        TextOut(OutRect.Left - TextWidth(S) div 2 - 18,
                OutRect.Top + I * (OutRect.Bottom - OutRect.Top) div 10 - TextHeight(S) div 2, S);
        MoveTo(OutRect.Left - 5, OutRect.Top + I * (OutRect.Bottom - OutRect.Top - 1) div 10);
        LineTo(PenPos.X + 10, PenPos.Y);
      end;

      Step:= Round(100 / Zoom);
      I:= ScrollBar1.Position - 2 * OutWidth;

      if I <> 0 then
        I:= Round(I + Step * (1 - Frac(I / Step)));

      if Length(Samples) > 1 then
        while I < ScrollBar1.Position +  Round(OutWidth / Zoom) do
        begin
          if (I >= 0) and (I <= Samples[High(Samples) - 1].Idx) then
          begin
            S:= IntToStr(I);
            X:= OutRect.Left + OutWidth div 2 + Round((I - ScrollBar1.Position) * Zoom);

            if (X >= OutRect.Left) and (X <= OutRect.Right) then
            begin
              TextOut(X - TextWidth(S) div 2, OutRect.Bottom + 8, S);
              MoveTo(X, OutRect.Bottom - 5);
              LineTo(PenPos.X, PenPos.Y + 10);
            end;
          end;
          Inc(I, Step);
        end;

      Brush.Style:= bsSolid;
      Brush.Color:= Config.ActivDColors.Border;

      Series1.Clear;
      for I:= 0 to High(Samples) do
        if Samples[I].Idx < 65000 then Series1.AddXY(I, Samples[I].Idx);

      for I:= 0 to High(Samples) do
      begin

        X:= OutWidth div 2 + Round((Samples[I].Idx - ScrollBar1.Position) * Zoom);
        if X < 0 then Continue;
        if X > (OutRect.Right - OutRect.Left) then Continue;
        X:= X + OutRect.Left;

        for J:= 1 to Samples[I].Count do
          if Samples[I].Ampl[J] >= AmplThTrackBarPosition then
          begin
            if not Config.InvertDelayAxis then Y:= OutRect.Top + (OutRect.Bottom - OutRect.Top) * (255 - Samples[I].Delay[J]) div 255
                                          else Y:= OutRect.Top + (OutRect.Bottom - OutRect.Top) * (Samples[I].Delay[J]) div 255;

            FillRect(Rect(X - 1, Y - 1, X + 1, Y + 1));
          end;
      end;

      Pen.Color:= clBlack;
      X:= OutRect.Left + OutWidth div 2 + Round((0 - ScrollBar1.Position) * Zoom);
      MoveTo(X, OutRect.Top);
      LineTo(X, OutRect.Bottom);
      if Length(Samples) > 1 then X:= OutRect.Left + OutWidth div 2 + Round((Samples[High(Samples) - 1].Idx - ScrollBar1.Position) * Zoom);
      MoveTo(X, OutRect.Top);
      LineTo(X, OutRect.Bottom);



      X:= 40;
      Y:= 20;
      Brush.Style:= bsClear;
      if HSData.Header.Rail = rLeft then S:= LangTable.Caption['Common:Rail'] + ': ' + LangTable.Caption['Common:Left']
                                    else S:= LangTable.Caption['Common:Rail'] + ': ' + LangTable.Caption['Common:Right'];
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

      // Number

      //S:= Format(LangTable.Caption['Common:Channel'] + ': %d', [HSData.Header.HandChNum]);
      S:= Format(LangTable.Caption['Common:Channel'] + ': %d� %s', [FDS.Config.HandChannelByIdx[HSData.Header.HandChNum].Angle, LangTable.Caption['Common:' + GetInspMethodTextId(FDS.Config.HandChannelByIdx[HSData.Header.HandChNum].Method, False)]]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;


      S:= Format(LangTable.Caption['HandScan:Surface'] + ': %d', [HSData.Header.ScanSurface]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

      S:= Format(LangTable.Caption['Common:ChannelParams_SHORT:Att'] + ': %d ' + LangTable.Caption['Common:dB'], [HSData.Header.Att]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;


      S:= Format(LangTable.Caption['Common:ChannelParams_SHORT:Ku'] + ': %d ' + LangTable.Caption['Common:dB'], [HSData.Header.Ku]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

      S:= Format(LangTable.Caption['Common:ChannelParams_SHORT:TVG'] + ': %d ' + LangTable.Caption['Common:us'], [HSData.Header.TVG]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

      if HSData.PrismDelayFlag then
      begin
        S:= Format(LangTable.Caption['Common:ChannelParams_SHORT:PrismDelay'] + ': %3.1f ' + LangTable.Caption['Common:us'], [HSData.PrismDelay / 10]);
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;
      end;



//                  !!!!!!!!!                            //NOTRANSLATE
{      if FDS.isA31 then S:= Format(LangTable.Caption['HandScan:ScanTime'] + ': %d ���', [Round(Length(HSData.Samples) / 70)])
                   else }
      S:= Format(LangTable.Caption['HandScan:ScanTime'] + ': %d ���', [Round(Length(HSData.Samples) / 50)]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

{      S:= Format(LangTable.Caption['HandScan:ScanTime'] + ': %3.2f ���', [Length(HSData.Samples) / 50]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;

      S:= Format('%d', [Length(HSData.Samples)]);
      TextOut(X, Y, S);
      Y:= Y + TextHeight(S) + 2;
 }
    end;
end;

procedure THandScanForm.FormActivate(Sender: TObject);
begin
  MainForm.DisplayModeToControl({nil} Self);
  FillInfoBar;
end;

procedure THandScanForm.ScrollBar1Change(Sender: TObject);
begin
  Self.DefocusControl(ScrollBar1, true);
  CreateImage;
  Repaint;
end;

procedure THandScanForm.Refresh;
begin
  if Assigned(OnPaint) then PaintBoxPaint(nil);
end;

procedure THandScanForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
var
  W: Integer;

begin
  W:= Round(ClientWidth / Zoom);
  case Msg.CharCode of
    38: ZoomOut;
    40: ZoomIn;
    37: begin
          if ScrollBar1.Enabled then
            ScrollBar1.Position:= ScrollBar1.Position - W div (4 + 16 * Ord(ssShift in []{Self.ShiftState}));
          CreateImage;
          Repaint;
        end;
    39: begin
          if ScrollBar1.Enabled then
            ScrollBar1.Position:= ScrollBar1.Position + W div (4 + 16 * Ord(ssShift in []{Self.ShiftState}));
          CreateImage;
          Repaint;
        end;
    34: begin
          if ScrollBar1.Enabled then
            ScrollBar1.Position:= ScrollBar1.Position - W;
          CreateImage;
          Repaint;
        end;
    33: begin
          if ScrollBar1.Enabled then
            ScrollBar1.Position:= ScrollBar1.Position + W;
          CreateImage;
          Repaint;
        end;
  end;

  PaintBoxMouseMove(nil, [], MouseX, MouseY);
end;

procedure THandScanForm.SetScrollBarMax;
begin
  if not FDataOK then Exit;
  if Length(HSData.Samples) > 1 then ScrollBar1.SetParams(ScrollBar1.Position, 0, HSData.Samples[High(HSData.Samples) - 1].Idx)
                                else ScrollBar1.SetParams(ScrollBar1.Position, 0, 0);
end;

procedure THandScanForm.ZoomIn;
begin
  if Zoom < 4 then
  begin
    Zoom:= Zoom * 1.2;
    SetScrollBarMax;
    CreateImage;
    Repaint;
    SetScrollBarMax;
  end;
end;

procedure THandScanForm.ZoomOut;
begin
  if Zoom > 0.1 then
  begin
    Zoom:= Zoom * 0.8;
    SetScrollBarMax;
    CreateImage;
    Repaint;
  end;
end;

procedure THandScanForm.StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  S1, S2: string;
begin
  with (Sender as TStringGrid), Canvas do
  begin
    Brush.Style:= bsSolid;
    FillRect(Rect);
    Brush.Style:= bsClear;
    if TextWidth(Cells[ACol, ARow]) < (Rect.Right - Rect.Left) then
      TextOut((Rect.Left + Rect.Right - TextWidth(Cells[ACol, ARow])) div 2,
              (Rect.Top + Rect.Bottom - TextHeight(Cells[ACol, ARow])) div 2,
              Cells[ACol, ARow]) else
    begin
      S1:= Copy(Cells[ACol, ARow], 1, Pos(' ', Cells[ACol, ARow]) - 1);
      S2:= Copy(Cells[ACol, ARow], Pos(' ', Cells[ACol, ARow]) + 1, MaxInt);
      TextOut((Rect.Left + Rect.Right - TextWidth(S1)) div 2,
              Rect.Top + (Rect.Bottom - Rect.Top) div 4 - TextHeight(S1) div 2 - 2, S1);
      TextOut((Rect.Left + Rect.Right - TextWidth(S2)) div 2,
              Rect.Top + 3*(Rect.Bottom - Rect.Top) div 4 - TextHeight(S2) div 2 - 2, S2);
    end;
  end;
end;

procedure THandScanForm.ModifyInfo;
const
  Names: array [0..15] of string = ('-12', '-10', '-8', '-6', '-4', '-2', '0', '2', '4', '6', '8', '10', '12', '14', '16', '18');

var
  Old, Pos, EndDisplayCoord, ScanWidth, U, I, J, Row, Delay, Probe: Integer;
  OutRect: TRect;
  S: string;
  Flag1, Flag2: Boolean;

begin

  OutRect.Left:= 30;
  OutRect.Right:= ClientWidth - 15;
  OutRect.Top:= 10;
  OutRect.Bottom:= ClientHeight - 25 - ScrollBar1.Height;

  if (MouseX >= OutRect.Left) and (MouseX <= OutRect.Right) and
     (MouseY >= OutRect.Top) and (MouseY <= OutRect.Bottom) then
    begin

      if HSData.Header.HandChNum in [0, 1] then
      begin
        if not Config.InvertDelayAxis then Delay:= Round(0.33 * 0 + 0.33 * 255 * (1 - ((MouseY - OutRect.Top) / (OutRect.Bottom - OutRect.Top))))
                                      else Delay:= Round(0.33 * 0 + 0.33 * 255 * ((MouseY - OutRect.Top) / (OutRect.Bottom - OutRect.Top)));
      end
      else
      begin
        if not Config.InvertDelayAxis then Delay:= Round(255 * (1 - ((MouseY - OutRect.Top) / (OutRect.Bottom - OutRect.Top))))
                                      else Delay:= Round(255 * ((MouseY - OutRect.Top) / (OutRect.Bottom - OutRect.Top)));
      end;

      Probe:= ScrollBar1.Position + Round((MouseX - OutRect.Left - (OutRect.Right - OutRect.Left) div 2) / Zoom);
//      FInfoBarForm.Label15.Caption := LangTable.Caption['HandScan:Probing'] + ' ' + LangTable.Caption['HandScan:Cycle:'] + ' ' + IntToStr(Probe);
//      FInfoBarForm.Label10.Caption := LangTable.Caption['HandScan:Delay:'] + ' ' + IntToStr(Delay) + ' ' + LangTable.Caption['Misc:us'];

      Row:= 1;

      for I:= 0 to High(HSData.Samples) do
        if HSData.Samples[I].Idx = Probe then
        begin
          for J:= 1 to HSData.Samples[I].Count do
          begin
//            FInfoBarForm.HSEcho.Cells[0, Row]:= Names[HSData.Samples[I].Ampl[J]];
//            FInfoBarForm.HSEcho.Cells[1, Row]:= IntToStr(HSData.Samples[I].Delay[J]);
            Inc(Row);
          end;
        end;

      FInfoBarForm.ClearHSAScan;
      for I:= 0 to High(HSData.Samples) do
        if (HSData.Samples[I].Idx >= Probe - 5) and
           (HSData.Samples[I].Idx <= Probe + 5) then
          for J:= 1 to HSData.Samples[I].Count do
            FInfoBarForm.AddHSAScan(HSData.Samples[I].Delay[J], HSData.Samples[I].Ampl[J], clBlack);
      FInfoBarForm.EndHSAScan;

      for I:= Row to 8 do
      begin
//        FInfoBarForm.HSEcho.Cells[0, I]:= '';
//        FInfoBarForm.HSEcho.Cells[1, I]:= '';
      end;
    end;
end;

procedure THandScanForm.PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  S: string;
  Delay1, Delay2, Probe1, Probe2: Integer;
  OutRect: TRect;

begin
  if InfoRectFlag then
    with Canvas do
    begin
      if (X <> MouseX) or (Y <> MouseY) then
      begin
        OutRect.Left:= 30;
        OutRect.Right:= ClientWidth - 15;
        OutRect.Top:= 10;
        OutRect.Bottom:= ClientHeight - 25 - ScrollBar1.Height;

        Application.HideHint;

        Pen.Mode:= pmXor;
        Pen.Color:= clWhite;
        Pen.Style:= psDot;
        Brush.Style:= bsClear;

        if InfoRectDrawFlag then Rectangle(InfoRect);
        InfoRect.BottomRight:= Point(X, Y);

        Pen.Mode:= pmXor;
        Pen.Color:= clWhite;
        Pen.Style:= psDot;
        Brush.Style:= bsClear;
        Rectangle(InfoRect);

        Pen.Mode:= pmCopy;

        S:= '';

        if not Config.InvertDelayAxis then
        begin
          if HSData.Header.HandChNum in [0, 1] then
          begin
            Delay1:= Round(0.33 * 0 + 0.33 * 255 * (1 - ((InfoRect.Top - OutRect.Top) / (OutRect.Bottom - OutRect.Top))));
            Delay2:= Round(0.33 * 0 + 0.33 * 255 * (1 - ((InfoRect.Bottom - OutRect.Top) / (OutRect.Bottom - OutRect.Top))));
          end
          else
          begin
            Delay1:= Round(255 * (1 - ((InfoRect.Top - OutRect.Top) / (OutRect.Bottom - OutRect.Top))));
            Delay2:= Round(255 * (1 - ((InfoRect.Bottom - OutRect.Top) / (OutRect.Bottom - OutRect.Top))));
          end;
        end
        else
        begin
          if HSData.Header.HandChNum in [0, 1] then
          begin
            Delay1:= Round(0.33 * 0 + 0.33 * 255 * ((InfoRect.Top - OutRect.Top) / (OutRect.Bottom - OutRect.Top)));
            Delay2:= Round(0.33 * 0 + 0.33 * 255 * ((InfoRect.Bottom - OutRect.Top) / (OutRect.Bottom - OutRect.Top)));
          end
          else
          begin
            Delay1:= Round(255 * ((InfoRect.Top - OutRect.Top) / (OutRect.Bottom - OutRect.Top)));
            Delay2:= Round(255 * ((InfoRect.Bottom - OutRect.Top) / (OutRect.Bottom - OutRect.Top)));
          end;
        end;

        Probe1:= ScrollBar1.Position + Round(Math.Min(InfoRect.Left, InfoRect.Right) / Zoom);
        Probe2:= ScrollBar1.Position + Round(Max(InfoRect.Left, InfoRect.Right) / Zoom);

{        FInfoBarForm.Label57.Caption:= IntToStr(Probe2 - Probe1) + LangTable.Caption['Misc:s'];
        FInfoBarForm.Label66.Caption:= FloatToStr(Delay1) + ' ' + LangTable.Caption['Misc:us'];
        FInfoBarForm.Label67.Caption:= FloatToStr(Delay2) + ' ' + LangTable.Caption['Misc:us'];
        FInfoBarForm.Label73.Caption:= FloatToStr(Abs(Delay1 - Delay2)) + ' ' + LangTable.Caption['Misc:us'];
 }
        InfoRectDrawFlag:= True;
        Application.ActivateHint(Mouse.CursorPos);
      end;
    end;

  MouseX:= X;
  MouseY:= Y;
  ModifyInfo;
end;

procedure THandScanForm.PaintBox1Click(Sender: TObject);
begin
 //
end;

procedure THandScanForm.CreateImage2;
var
  Step, I, J, X, Y: Integer;
  OutWidth: Integer;
  OutRect: TRect;
  S: string;
  Cross_Rect: TRect;
  Horizontal_Rect: TRect;
  Vertically_Rect: TRect;
  ZeroProbe_Rect: TRect;
  X0, Y0: Integer;
  SrcWidth: Integer;
  SrcHeight: Integer;
  DstWidth: Integer;
  DstHeight: Integer;
  Koef: Single;

  Cross_Width: Integer;
  Cross_Height: Integer;
  Horizontal_Width: Integer;
  Horizontal_Height: Integer;
  Vertically_Width: Integer;
  Vertically_Height: Integer;
  Space: Integer;

procedure MoveRect(var Rt: TRect; Delta: TPoint);
begin
  Rt:= Rect(Rt.Left + Delta.X, Rt.Top + Delta.Y, Rt.Right + Delta.X, Rt.Bottom + Delta.Y);
end;

function RectWidth(Rt: TRect): Integer;
begin
  Result:= Rt.Right - Rt.Left;
end;

function RectHeight(Rt: TRect): Integer;
begin
  Result:= Rt.Bottom - Rt.Top;
end;

procedure TextOut_(Rt: TRect; Text: string);
begin
  if Assigned(Buffer2) then
    with Buffer2, Canvas do TextOut(Rt.Left + (RectWidth(Rt) - TextWidth(Text)) div 2, Rt.Top - Round(TextHeight(Text) * 1.5), Text);

end;


begin
  if Assigned(HeadSData) and
     Assigned(HeadSData.Cross_Image) and
     Assigned(HeadSData.Horizontal_Image) and
     Assigned(HeadSData.Vertically_Image) then

    if Assigned(Buffer2) then
      with Buffer2, Canvas do
      begin
        X0:= 330;
        Y0:= 0;
        Space:= 10;
        SrcWidth:= 206 + 413 + Space * 2;
        SrcHeight:= 164 + 206 + 60 + Space * 2;
        DstWidth:= Buffer2.Width - X0;
        DstHeight:= Buffer2.Height - Y0;
        Cross_Width:= 206;
        Cross_Height:= 164;
        Vertically_Width:= 413;
        Vertically_Height:= 164;
        Horizontal_Width:= 413;
        Horizontal_Height:= 206;

                {
        if DstWidth / SrcWidth < DstHeight / SrcHeight then Koef:= DstWidth / SrcWidth
                                                       else Koef:= DstHeight / SrcHeight;

        Y0:= Round((DstHeight - Koef * SrcHeight) / 2);

        Cross_Rect:=      Rect(X0                            , Y0,
                               X0 + Round(Cross_Width * Koef), Y0 + Round(Cross_Height * Koef));

        Vertically_Rect:= Rect(Cross_Rect.Right + Space                                 , Cross_Rect.Top,
                               Cross_Rect.Right + Space + Round(Vertically_Width * Koef), Cross_Rect.Top + Round(Vertically_Height * Koef));

        Horizontal_Rect:= Rect(Cross_Rect.Right + Space                                 , Vertically_Rect.Bottom + Space,
                               Cross_Rect.Right + Space + Round(Horizontal_Width * Koef), Vertically_Rect.Bottom + Space + Round(Horizontal_Height * Koef));

        ZeroProbe_Rect := Rect(Cross_Rect.Right + Space                                 , Horizontal_Rect.Bottom + Space,
                               Cross_Rect.Right + Space + Round(Horizontal_Width * Koef), Horizontal_Rect.Bottom + Space + Round(Horizontal_Height * Koef));
            }

        Brush.Color:= clWhite;
        FillRect(Rect(0, 0, 320, Height));
        Brush.Color:= 239 or (235 shl 8) or (231 shl 16);
        FillRect(Rect(320, 0, Width, Height));

        X:= 5;
        Y:= 5; // Horizontal_Rect.Top;
        Brush.Style:= bsClear;
        if HeadSData.Rail = rLeft then S:= LangTable.Caption['Common:Rail'] + ': ' + LangTable.Caption['Common:Left']
                                  else S:= LangTable.Caption['Common:Rail'] + ': ' + LangTable.Caption['Common:Right'];
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        if HeadSData.WorkSide = 1 then S:= '������� �����: ������� ������� 1'  //NO_LANGUAGE
                                  else S:= '������� �����: ������� ������� 2'; //NO_LANGUAGE
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;


        if HeadSData.ThresholdUnit = 1 then // ������� ��������� ������ ������������ ��������������� �����.: 1 - ���������; 2 - % (�� 0 �� 100)
        begin
          S:= Format('����� ������������ ��������������� �����������: %d ��', [HeadSData.ThresholdValue]);  //NO_LANGUAGE
        end
        else // ������� ��������� ������ ������������ ��������������� �����.: 2 - % (�� 0 �� 100)
        begin
          S:= Format('����� ������������ ��������������� �����������: %d %%', [HeadSData.ThresholdValue]);   //NO_LANGUAGE
        end;
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        S:= Format(LangTable.Caption['HandScan:ScanTime'] + ': %d ���', [HeadSData.OperatingTime]); //NO_LANGUAGE
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        S:= Format('��� ������� %s', [HeaderBigStrToString_(HeadSData.CodeDefect)]); //NO_LANGUAGE
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        S:= Format('�������: %s', [HeaderBigStrToString_(HeadSData.Sizes)]); //NO_LANGUAGE
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        S:= Format('�����������: %s', [HeaderBigStrToString_(HeadSData.Comments)]); //NO_LANGUAGE
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        Cross_Rect:= Rect(0, 0, HeadSData.Cross_Image.Width, HeadSData.Cross_Image.Height);
        Vertically_Rect:= Rect(0, 0, HeadSData.Horizontal_Image.Width, HeadSData.Horizontal_Image.Height);
        Horizontal_Rect:= Rect(0, 0, HeadSData.Vertically_Image.Width, HeadSData.Vertically_Image.Height);
        ZeroProbe_Rect:= Rect(0, 0, HeadSData.Zero_Probe_Image.Width, HeadSData.Zero_Probe_Image.Height);

        MoveRect(Cross_Rect, Point(0, 2 * Abs(Font.Height)));
        MoveRect(Vertically_Rect, Point(0, 2 * Abs(Font.Height)));
        MoveRect(Horizontal_Rect, Point(0, 4 * Abs(Font.Height)));
        MoveRect(ZeroProbe_Rect, Point(0, 4 * Abs(Font.Height)));

        MoveRect(Cross_Rect, Point(X0 + Space, 0));
        MoveRect(Vertically_Rect, Point(X0 + Space + RectWidth(Cross_Rect), 0));
        MoveRect(Horizontal_Rect, Point(X0 + Space + RectWidth(Cross_Rect), Vertically_Rect.Bottom));
        MoveRect(ZeroProbe_Rect, Point(X0 + Space + RectWidth(Cross_Rect) + RectWidth(Horizontal_Rect) - RectWidth(ZeroProbe_Rect), Horizontal_Rect.Bottom));

        X:= (DstWidth - RectWidth(Horizontal_Rect) - Space - RectWidth(Cross_Rect)) div 2;
        Y:= (DstHeight - ZeroProbe_Rect.Bottom) div 2;

        MoveRect(Cross_Rect, Point(X, Y));
        MoveRect(Vertically_Rect, Point(X, Y));
        MoveRect(Horizontal_Rect, Point(X, Y));
        MoveRect(ZeroProbe_Rect, Point(X, Y));


        TextOut_(Cross_Rect, '���������� �������, ��');  //NO_LANGUAGE
        TextOut_(Vertically_Rect, '������������ �������, ��');  //NO_LANGUAGE
        TextOut_(Horizontal_Rect, '�������������� �������, ��');  //NO_LANGUAGE

//        if Assigned(HeadSData.Cross_Image) then HeadSData.Cross_Image.Draw(Buffer2.Canvas, );
  //      if Assigned(HeadSData.Horizontal_Image) then HeadSData.Horizontal_Image.Draw(Buffer2.Canvas, Vertically_Rect);
//        if Assigned(HeadSData.Vertically_Image) then HeadSData.Vertically_Image.Draw(Buffer2.Canvas, Horizontal_Rect);
//        if Assigned(HeadSData.Zero_Probe_Image) then HeadSData.Zero_Probe_Image.Draw(Buffer2.Canvas, ZeroProbe_Rect);

       // Buffer2.Canvas.Brush.Color:= clRed;
       // Buffer2.Canvas.FillRect(Rect(0, 0, Buffer2.Width, Buffer2.Height));


        if Assigned(HeadSData.Cross_Image) then HeadSData.Cross_Image.Draw(Buffer2.Canvas, Cross_Rect);
        if Assigned(HeadSData.Horizontal_Image) then HeadSData.Horizontal_Image.Draw(Buffer2.Canvas, Vertically_Rect);
        if Assigned(HeadSData.Vertically_Image) then HeadSData.Vertically_Image.Draw(Buffer2.Canvas, Horizontal_Rect);

        if Assigned(HeadSData.Zero_Probe_Image) then HeadSData.Zero_Probe_Image.Draw(Buffer2.Canvas, ZeroProbe_Rect);

//        HeadSData.Zero_Probe_Image.SaveToFile('C:\temp\test!!!.png ');
      end;
end;

procedure THandScanForm.PaintBox1Paint(Sender: TObject);
begin
  CreateImage2;
  PaintBox1.Canvas.Draw(0, 0, Buffer2);
end;

procedure THandScanForm.PaintBoxMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  InfoRectFlag:= (Button = mbLeft);
  if InfoRectFlag then
  begin
    InfoRect.TopLeft:= Point(X, Y);
    InfoRect.BottomRight:= Point(X, Y);
//    SavePage:= FInfoBarForm.PageControl2.ActivePageIndex;
//    FInfoBarForm.PageControl2.ActivePageIndex:= 2;
  end;
end;

procedure THandScanForm.PaintBoxMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if InfoRectFlag then
    with Canvas do
    begin
      InfoRectFlag:= False;

      Pen.Mode:= pmXor;
      Pen.Color:= clWhite;
      Pen.Style:= psDot;
      Brush.Style:= bsClear;

      if InfoRectDrawFlag then Rectangle(InfoRect);
      InfoRectDrawFlag:= False;
      Pen.Mode:= pmCopy;
{
      if not CompareMem(@InfoRect.TopLeft, @InfoRect.BottomRight, SizeOf(TPoint)) then
        if false then
        begin
//          ShowMessage(PaintBox.Hint);
//          PaintBox.Hint:= '';
        end;
}
//      FInfoBarForm.PageControl2.ActivePageIndex:= SavePage;
    end;
end;

procedure THandScanForm.FormCreate(Sender: TObject);
begin
//  Panel.DoubleBuffered:= True;
  DataExists:= False;
  HeadSData:= nil;
  FDataOK:= False;
  Buffer:= nil;
  Buffer2:= nil;
  NeedClear:= False;
  Repaint;
  OnChangeLanguage(nil);
end;

procedure THandScanForm.OnChangeLanguage(Sender: TObject);
begin
  Self.Caption:= LangTable.Caption['HandScan:HandScan'] + ': ' + FN;
  if Idx <> - 1 then Self.Caption:= Self.Caption + ' Idx:' + IntToStr(Idx);
end;

procedure THandScanForm.SetFocus_;
begin
  if Button3.Tag = 0 then
  begin
    Button3.SetFocus;
    Button3.Tag:= 1;
  end
  else
  begin
    Button4.SetFocus;
    Button3.Tag:= 0;
  end;
end;

procedure THandScanForm.FormResize(Sender: TObject);
begin

//  self.Caption:= IntToStr(self.Width) + ' ' + IntToStr(self.Height);

  OnChangeLanguage(nil);
  if not FDataOK then Exit;

  if FormMode = 1 then
  begin
    SetScrollBarMax;
    if Assigned(Buffer) then Buffer.FreeImage
                        else Buffer:= TBitMap.Create;

    Buffer.Width:= ClientWidth;
    Buffer.Height:= ClientHeight - ScrollBar1.Height;
    CreateImage;
    Repaint;
  end
  else
  begin
    if Assigned(Buffer2) then Buffer2.FreeImage
                         else Buffer2:= TBitMap.Create;

    Buffer2.Width:= PaintBox1.Width;
    Buffer2.Height:= PaintBox1.Height;
    CreateImage2;
    if Assigned(Buffer2) then PaintBox1.Canvas.Draw(0, 0, Buffer2);
  end;
end;

end.
