unit BoxerUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TSizeType = (ctPixel, ctPercent);
  TColAlign = (caMain, caLeft, caRight);

  TStrip = record
    A: Integer;
    B: Integer;
  end;

  TRowItem = record
    RowType: TSizeType;
    Size: Single
  end;

  TColItem = record
    Align: TColAlign;
    ColType: TSizeType;
    Size: Single;
  end;



  TBoxer = class
  protected
    FRows: array of TRowItem;
    FCols: array of TColItem;
    FRowsPoint: array of TStrip;
    FColsPoint: array of TStrip;
    FStartX: Integer;
    FStartY: Integer;
  private
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    function AddRow(RowType: TSizeType; Size: Single): Integer;
    procedure AddCol(Align: TColAlign; ColType: TSizeType; Size: Integer);
    procedure SetSize(Width, Height, Border: Single);
    procedure SetSizeHeight(Height, Border: Single);
    function GetRect(Col, Row: Integer): TRect;
    function GetBounds(): TRect;
    function GetFullSize(Col, Row: Integer): TSize;
    function GetIndex(X, I: Integer; var iCol, iRow: Integer): Boolean;
    function ColPixelWidth: Integer;
    property StartX: Integer read FStartX write FStartX;
    property StartY: Integer read FStartY write FStartY;
  end;

function StepOut(Src: TRect): TRect;
function StepIn(Src: TRect): TRect;

implementation


function StepOut(Src: TRect): TRect;
begin
  Result:= Rect(Src.Left - 1, Src.Top - 1, Src.Right + 1, Src.Bottom + 1);
end;

function StepIn(Src: TRect): TRect;
begin
  Result:= Rect(Src.Left + 1, Src.Top + 1, Src.Right - 1, Src.Bottom - 1);
end;

// ----[ TBoxer ]------------------------------------------------------------------------------------------

constructor TBoxer.Create;
begin
  FStartX:= 0;
  FStartY:= 0;
end;

destructor TBoxer.Destroy;
begin
  SetLength(FRows, 0);
  SetLength(FCols, 0);
end;

procedure TBoxer.Clear;
begin
  SetLength(FRows, 0);
  SetLength(FCols, 0);
end;

function TBoxer.AddRow(RowType: TSizeType; Size: Single): Integer;
begin
  SetLength(FRows, Length(FRows) + 1);
  FRows[High(FRows)].RowType:= RowType;
  FRows[High(FRows)].Size:= Size;
  Result:= High(FRows);
end;

procedure TBoxer.AddCol(Align: TColAlign; ColType: TSizeType; Size: Integer);
begin
  SetLength(FCols, Length(FCols) + 1);
  FCols[High(FCols)].Align:= Align;
  FCols[High(FCols)].ColType:= ColType;
  FCols[High(FCols)].Size:= Size;
end;

function TBoxer.ColPixelWidth: Integer;
var
  I: Integer;
  S: Real;

begin
  S:= 0;
  for I:= 0 to High(FCols) do
    if FCols[I].ColType = ctPixel then S:= S + FCols[I].Size;
  Result:= Round(S);
end;

procedure TBoxer.SetSize(Width, Height, Border: Single);
var
  I, J: Integer;
  Y1, Y2: Single;
  Tmp: Single;
  Tmp1: Single;
  Flag: Boolean;

begin

  // ������

  Tmp:= Height;
  for I:= 0 to High(FRows) do
    if FRows[I].RowType = ctPixel then Tmp:= Tmp - FRows[I].Size;

  Tmp:= Tmp - Border * (Length(FRows) + 1);

  Y1:= Border;
  for I:= 0 to High(FRows) do
    case FRows[I].RowType of
      ctPixel: Y1:= Y1 + FRows[I].Size + Border;
    ctPercent: Y1:= Y1 + Tmp * FRows[I].Size / 100 + Border;
    end;

  Tmp1:= Height - (Y1 - 1) - Border;

  J:= 0;
  SetLength(FRowsPoint, 100);
  Y1:= Border;
  Flag:= True;
  for I:= 0 to High(FRows) do
  begin
    Y2:= Y1;
    case FRows[I].RowType of
        ctPixel: if Flag then
                 begin
                   Y1:= Y1 +FRows[I].Size + Tmp1;
                   Flag:= False;
                 end
                 else Y1:= Y1 +FRows[I].Size;
      ctPercent: Y1:= Y1 + Tmp * FRows[I].Size / 100;
    end;
    FRowsPoint[J].A:= Round(Y2);
    FRowsPoint[J].B:= Round(Y1);
    Inc(J);
    Y1:= Y1 + Border;
  end;
  SetLength(FRowsPoint, J);

  // �������

  Tmp:= Width;
  for I:= 0 to High(FCols) do
    if FCols[I].ColType = ctPixel then Tmp:= Tmp - FCols[I].Size;

  Tmp:= Tmp + Border * (Length(FCols) + 1);

  Y1:= Border;
  for I:= 0 to High(FCols) do
    case FCols[I].ColType of
      ctPixel: Y1:= Y1 + FCols[I].Size + Border;
    ctPercent: Y1:= Y1 + Tmp * FCols[I].Size / 100 + Border;
    end;

  Tmp1:= Width - (Y1 - 1) - Border;

  J:= 0;
  SetLength(FColsPoint, 100);
  Y1:= Border;
  Flag:= True;
  for I:= 0 to High(FCols) do
  begin
    Y2:= Y1;
    case FCols[I].ColType of
        ctPixel: if Flag then
                 begin
                   Y1:= Y1 + FCols[I].Size + Tmp1;
                   Flag:= False;
                 end
                 else Y1:= Y1 + FCols[I].Size;
      ctPercent: Y1:= Y1 + Tmp * FCols[I].Size / 100;
    end;
    FColsPoint[J].A:= Round(Y2);
    FColsPoint[J].B:= Round(Y1);
    Inc(J);
    Y1:= Y1 + Border;
  end;

  SetLength(FColsPoint, J);
end;

procedure TBoxer.SetSizeHeight(Height, Border: Single);
begin
  SetSize(ColPixelWidth, Height, Border);
end;

function TBoxer.GetRect(Col, Row: Integer): TRect;
begin
  if (Col < Length(FColsPoint)) and (Row < Length(FRowsPoint)) then
    Result:= Rect(FStartX + FColsPoint[Col].A, FStartY + FRowsPoint[Row].A,
                  FStartX + FColsPoint[Col].B, FStartY + FRowsPoint[Row].B)
 // else ShowMessage('!');
end;

function TBoxer.GetBounds(): TRect;
begin
  Result:= Rect(FColsPoint[0].A, FRowsPoint[0].A, FColsPoint[High(FColsPoint)].B, FRowsPoint[High(FRowsPoint)].B);
end;

function TBoxer.GetFullSize(Col, Row: Integer): TSize;
begin
  Result.cx:= FColsPoint[High(FColsPoint)].B;
  Result.cy:= FRowsPoint[High(FRowsPoint)].B;
end;


function TBoxer.GetIndex(X, I: Integer; var iCol, iRow: Integer): Boolean;
begin
        //
end;

end.
