object KuGraphForm: TKuGraphForm
  Left = 0
  Top = 219
  BorderStyle = bsDialog
  Caption = #1043#1088#1072#1092#1080#1082#1080' '#1095#1091#1074#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086#1089#1090#1080
  ClientHeight = 537
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShortCut = FormShortCut
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 101
    Height = 537
    Align = alLeft
    TabOrder = 0
    object Label1: TLabel
      Left = 5
      Top = 6
      Width = 75
      Height = 13
      Caption = #1053#1080#1090#1100': '#1051#1077#1074#1072#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 4
      Top = 274
      Width = 82
      Height = 13
      Caption = #1053#1080#1090#1100': '#1055#1088#1072#1074#1072#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object RadioButton1: TRadioButton
      Tag = 1
      Left = 9
      Top = 28
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 0'
      TabOrder = 0
      OnClick = RadioButton1Click
    end
    object RadioButton2: TRadioButton
      Tag = 2
      Left = 9
      Top = 51
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 1'
      TabOrder = 1
      OnClick = RadioButton1Click
    end
    object RadioButton3: TRadioButton
      Tag = 3
      Left = 9
      Top = 74
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 2'
      TabOrder = 2
      OnClick = RadioButton1Click
    end
    object RadioButton4: TRadioButton
      Tag = 4
      Left = 9
      Top = 97
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 3'
      TabOrder = 3
      OnClick = RadioButton1Click
    end
    object RadioButton5: TRadioButton
      Tag = 5
      Left = 9
      Top = 120
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 4'
      TabOrder = 4
      OnClick = RadioButton1Click
    end
    object RadioButton6: TRadioButton
      Tag = 6
      Left = 9
      Top = 143
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 5'
      TabOrder = 5
      OnClick = RadioButton1Click
    end
    object RadioButton7: TRadioButton
      Tag = 7
      Left = 9
      Top = 166
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 6'
      TabOrder = 6
      OnClick = RadioButton1Click
    end
    object RadioButton8: TRadioButton
      Tag = 8
      Left = 9
      Top = 189
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 7'
      TabOrder = 7
      OnClick = RadioButton1Click
    end
    object RadioButton9: TRadioButton
      Tag = 9
      Left = 9
      Top = 212
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 8'
      TabOrder = 8
      OnClick = RadioButton1Click
    end
    object RadioButton10: TRadioButton
      Tag = 10
      Left = 9
      Top = 235
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 9'
      TabOrder = 9
      OnClick = RadioButton1Click
    end
    object RadioButton11: TRadioButton
      Tag = 11
      Left = 9
      Top = 298
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 0'
      TabOrder = 10
      OnClick = RadioButton1Click
    end
    object RadioButton12: TRadioButton
      Tag = 12
      Left = 9
      Top = 321
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 1'
      TabOrder = 11
      OnClick = RadioButton1Click
    end
    object RadioButton13: TRadioButton
      Tag = 13
      Left = 9
      Top = 344
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 2'
      TabOrder = 12
      OnClick = RadioButton1Click
    end
    object RadioButton14: TRadioButton
      Tag = 14
      Left = 9
      Top = 367
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 3'
      TabOrder = 13
      OnClick = RadioButton1Click
    end
    object RadioButton15: TRadioButton
      Tag = 15
      Left = 9
      Top = 390
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 4'
      TabOrder = 14
      OnClick = RadioButton1Click
    end
    object RadioButton16: TRadioButton
      Tag = 16
      Left = 9
      Top = 413
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 5'
      TabOrder = 15
      OnClick = RadioButton1Click
    end
    object RadioButton17: TRadioButton
      Tag = 17
      Left = 9
      Top = 436
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 6'
      TabOrder = 16
      OnClick = RadioButton1Click
    end
    object RadioButton18: TRadioButton
      Tag = 18
      Left = 9
      Top = 459
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 7'
      TabOrder = 17
      OnClick = RadioButton1Click
    end
    object RadioButton19: TRadioButton
      Tag = 19
      Left = 9
      Top = 482
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 8'
      TabOrder = 18
      OnClick = RadioButton1Click
    end
    object RadioButton20: TRadioButton
      Tag = 20
      Left = 9
      Top = 505
      Width = 77
      Height = 21
      Caption = #1050#1072#1085#1072#1083' 9'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 19
      OnClick = RadioButton1Click
    end
    object Panel1: TPanel
      Left = 4
      Top = 261
      Width = 90
      Height = 3
      TabOrder = 20
    end
  end
  object Panel3: TPanel
    Left = 101
    Top = 0
    Width = 915
    Height = 537
    Align = alClient
    TabOrder = 1
    object Chart1: TChart
      Left = 1
      Top = 1
      Width = 913
      Height = 262
      BackWall.Brush.Color = clWhite
      BackWall.Brush.Style = bsClear
      Legend.LegendStyle = lsSeries
      Legend.Visible = False
      Title.Text.Strings = (
        #1059#1089#1083#1086#1074#1085#1072#1103' '#1095#1091#1074#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100)
      BottomAxis.Visible = False
      LeftAxis.Automatic = False
      LeftAxis.AutomaticMaximum = False
      LeftAxis.AutomaticMinimum = False
      LeftAxis.Maximum = 537.000000000000000000
      LeftAxis.Minimum = 199.000000000000000000
      LeftAxis.Title.Caption = #1050#1091' ['#1076#1041']'
      LeftAxis.Title.Font.Height = -13
      LeftAxis.TitleSize = 1
      View3D = False
      Align = alClient
      BevelOuter = bvNone
      Color = clWindow
      TabOrder = 0
      object Series1: TLineSeries
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.Visible = False
        SeriesColor = 61440
        LinePen.Color = clLime
        LinePen.Width = 4
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
      object Series2: TLineSeries
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.Visible = False
        Brush.Style = bsHorizontal
        LineBrush = bsHorizontal
        LinePen.Style = psDash
        LinePen.Width = 2
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
      object Series3: TLineSeries
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.Visible = False
        SeriesColor = clRed
        LinePen.Style = psDash
        LinePen.Width = 2
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
    end
    object Chart2: TChart
      Left = 1
      Top = 263
      Width = 913
      Height = 273
      BackWall.Brush.Color = clWhite
      BackWall.Brush.Style = bsClear
      Legend.LegendStyle = lsSeries
      Legend.Visible = False
      Title.Text.Strings = (
        #1055#1086#1088#1086#1075#1086#1074#1072#1103' '#1095#1091#1074#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100)
      BottomAxis.Visible = False
      LeftAxis.Automatic = False
      LeftAxis.AutomaticMaximum = False
      LeftAxis.AutomaticMinimum = False
      LeftAxis.Maximum = 936.000000000000000000
      LeftAxis.Minimum = 529.000000000000000000
      LeftAxis.Title.Caption = #1050#1087' ['#1076#1041']'
      LeftAxis.Title.Font.Height = -13
      LeftAxis.TitleSize = 1
      View3D = False
      Align = alBottom
      BevelOuter = bvNone
      Color = clWindow
      TabOrder = 1
      object LineSeries1: TLineSeries
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.Visible = False
        SeriesColor = 61440
        LinePen.Color = clLime
        LinePen.Width = 4
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
    end
  end
end
