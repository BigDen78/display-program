object SaveAsForm: TSaveAsForm
  Left = 379
  Top = 296
  BorderStyle = bsDialog
  Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082'...'
  ClientHeight = 242
  ClientWidth = 304
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShortCut = FormShortCut
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 38
    Top = 64
    Width = 229
    Height = 121
    Caption = ' '#1057#1086#1093#1088#1072#1085#1103#1077#1084#1099#1081' '#1091#1095#1072#1089#1090#1086#1082' '
    TabOrder = 0
    object Label3: TLabel
      Left = 15
      Top = 64
      Width = 6
      Height = 13
      Caption = #1089
      Enabled = False
    end
    object Label4: TLabel
      Left = 14
      Top = 88
      Width = 14
      Height = 13
      AutoSize = False
      Caption = #1087#1086
      Enabled = False
    end
    object SpeedButton1: TSpeedButton
      Left = 184
      Top = 60
      Width = 23
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 184
      Top = 84
      Width = 23
      Height = 21
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object RadioButton2: TRadioButton
      Tag = 2
      Left = 8
      Top = 40
      Width = 113
      Height = 17
      Caption = #1059#1082#1072#1079#1072#1085#1085#1091#1102' '#1095#1072#1089#1090#1100
      TabOrder = 0
      OnClick = RadioButton2Click
    end
    object RadioButton3: TRadioButton
      Tag = 1
      Left = 8
      Top = 20
      Width = 113
      Height = 17
      Caption = #1058#1086' '#1095#1090#1086' '#1085#1072' '#1101#1082#1088#1072#1085#1077
      Checked = True
      TabOrder = 1
      TabStop = True
      OnClick = RadioButton2Click
    end
    object Edit2: TEdit
      Left = 46
      Top = 60
      Width = 133
      Height = 21
      AutoSize = False
      Enabled = False
      ReadOnly = True
      TabOrder = 2
    end
    object Edit3: TEdit
      Left = 46
      Top = 84
      Width = 133
      Height = 21
      AutoSize = False
      Enabled = False
      ReadOnly = True
      TabOrder = 3
    end
  end
  object GroupBox2: TGroupBox
    Left = 7
    Top = 8
    Width = 291
    Height = 49
    Caption = ' '#1048#1084#1103' '#1092#1072#1081#1083#1072' '
    TabOrder = 1
    object Button1: TButton
      Left = 224
      Top = 16
      Width = 59
      Height = 25
      Caption = #1054#1073#1079#1086#1088'...'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object Button2: TButton
    Left = 71
    Top = 212
    Width = 75
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 167
    Top = 212
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 3
  end
  object CheckBox1: TCheckBox
    Left = 24
    Top = 190
    Width = 249
    Height = 17
    Caption = #1057#1086#1093#1088#1072#1085#1103#1090#1100' '#1086#1090#1084#1077#1090#1082#1080' '#1073#1083#1086#1082#1085#1086#1090#1072
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object Edit1: TComboBox
    Left = 14
    Top = 26
    Width = 213
    Height = 21
    TabOrder = 5
  end
  object SaveDialog1: TSaveDialog
    Filter = #1042#1089#1077' '#1092#1072#1081#1083#1099'|*.*'
    InitialDir = 'c:\'
    Title = #1042#1099#1073#1077#1088#1080#1090#1077' '#1080#1084#1103' '#1092#1072#1081#1083#1072' '#1080' '#1082#1072#1090#1072#1083#1086#1075
    Left = 8
    Top = 72
  end
end
