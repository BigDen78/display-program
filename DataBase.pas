unit DataBase;

interface

uses
  Windows, Messages, SysUtils, Classes, ComCtrls, Forms, LanguageUnit, AviconDataSource, AviconTypes, Registry;

type
  TDBFileItem = packed record
    FileName       : string[255];                 // ��� �����
    FileSize       : Integer;                     // ������ �����
    FileDateTime   : TDateTime;                   // ���� � ����� �������� �����
    EndMRFCrd      : TMRFCrd;                     // �������� ����������
    EndCaCrd       : TCaCrd;
    Length         : Integer;                     // ����� ����� � ������

    DeviceID       : TAviconID;                   // ������������� �������
    DeviceVer      : Byte;                        // ������ �������
    HeaderVer      : Byte;                        // ������ ���������
    MoveDir        : Integer;                     // ����������� ��������: + 1 � ������� ���������� ��������� ����������; - 1 � ������� ���������� ��������� ����������
    PathCoordSystem: Byte;                        // ������� ������� ��������� ����������
    UsedItems      : TUsedItemsList;              // ����� ������������ ������������ ������
    UnitsCount     : Byte;                        // ���������� ������ �������
    UnitsInfo      : TUnitsInfoList;              // ���������� � ������ �������
    Organization   : THeaderStr;                  // �������� ����������� �������������� ��������
    RailRoadName   : THeaderStr;                  // �������� �������� ������
    DirectionCode  : DWord;                       // ��� �����������
    PathSectionName: THeaderStr;                  // �������� ������� ������
    OperatorName   : THeaderStr;                  // ��� ���������
    RailPathNumber : THeaderStr;                  // ����� �/� ����
    RailSection    : Integer;                     // ����� ������
    Year           : Word;                        // ���� ����� ��������
    Month          : Word;
    Day            : Word;
    Hour           : Word;
    Minute         : Word;
    StartKM        : Integer;                     // ��������� ���������� - �����������
    StartPk        : Integer;
    StartMetre     : Integer;
    WorkRailTypeA  : Byte;                        // ������� ���� (��� ������������ ��������): 0 � �����, 1 � ������
    StartChainage  : TCaCrd;                      // ��������� ���������� � ����������� �� PathCoordSystem
    WorkRailTypeB  : TCardinalPoints;             // ������� ���� (��� ������������ ��������): NR, SR, WR, ER
    TrackDirection : TCardinalPoints;             // ��� �����������:                          NB, SB, EB, WB, CT, PT, RA, TT
    TrackID        : TCardinalPoints;             // Track ID:                                 NR, SR, ER, WR
  end;

  TDataBaseArray = array of TDBFileItem;

  TDataBase = class
  protected
    FData: TMemoryStream;
    FItems: TDataBaseArray;
    FDataFileName: string;
    FNeedFullUpdate: Boolean;
  private
    function GetCount: Integer;
    function GetItem(Index: Integer): TDBFileItem;
    function GetItemDate(Index: Integer): TDate;
    function GetItemStartRealCoord(Index: Integer): TRealCoord;
    function GetItemEndRealCoord(Index: Integer): TRealCoord;
    procedure PutItem(Index: Integer; New: TDBFileItem);
    procedure OnChangeLP(Sender: TObject);
  public
    constructor Create(FileName: string);
    destructor Destroy; override;
    function GetIndex(FileName: string): Integer;
    procedure Load;
    procedure Save;
    function Update(BasePath: string): Boolean;
    property ItemDate[Index: Integer]: TDate read GetItemDate;
    property ItemStartRealCoord[Index: Integer]: TRealCoord read GetItemStartRealCoord;
    property ItemEndRealCoord[Index: Integer]: TRealCoord read GetItemEndRealCoord;
    property Item[Index: Integer]: TDBFileItem read GetItem write PutItem;
    property Count: Integer read GetCount;
  end;

implementation

uses
  CreateBaseProg;

// -----[ TDataBase ]-----------------------------------------------------------

constructor TDataBase.Create(FileName: string); // �������� �������
begin
  FNeedFullUpdate:= False;
  FData:= TMemoryStream.Create;
  FDataFileName:= FileName;
end;

destructor TDataBase.Destroy; // �������� �������
begin
  FData.Free;
  SetLength(FItems, 0);
end;

function TDataBase.GetCount: Integer;
begin
  Result:= Length(FItems);
end;

function TDataBase.GetItem(Index: Integer): TDBFileItem;
begin
  Result:= FItems[Index];
end;

function TDataBase.GetItemDate(Index: Integer): TDate;
begin
  try
    Result:= EncodeDate(FItems[Index].Year, FItems[Index].Month, FItems[Index].Day);
  finally

  end;
end;

function TDataBase.GetItemStartRealCoord(Index: Integer): TRealCoord;
begin
  Result.Sys:= TCoordSys(FItems[Index].PathCoordSystem);
  case Result.Sys of
    csMetricRF: begin
                  Result.MRFCrd.Km:= FItems[Index].StartKM;
                  Result.MRFCrd.Pk:= FItems[Index].StartPk;
                  Result.MRFCrd.mm:= FItems[Index].StartMetre * 1000;
                end;
    csImperial,
 csMetric0_1km,
   csMetric1km,
csMetric1kmTurkish: Result.CaCrd:= FItems[Index].StartChainage;
  end;
end;

function TDataBase.GetItemEndRealCoord(Index: Integer): TRealCoord;
begin
  Result.Sys:= TCoordSys(FItems[Index].PathCoordSystem);
  case Result.Sys of
    csMetricRF: Result.MRFCrd:= FItems[Index].EndMRFCrd;
    csImperial,
 csMetric0_1km,
   csMetric1km,
csMetric1kmTurkish: Result.CaCrd:= FItems[Index].EndCaCrd;
  end;
end;

procedure TDataBase.PutItem(Index: Integer; New: TDBFileItem);
begin
  FItems[Index]:= New;
end;

procedure TDataBase.Load;
var
  J, I: Integer;
  F: file;
  VersionID: string;
  ItemCount: Integer;

begin
  if FileExists(FDataFileName) then // �������� �����
  begin
    FNeedFullUpdate:= False;
    AssignFile(F, FDataFileName);
    try
      Reset(F, 1);
    except
      FileMode:= 0;
      try
        Reset(F, 1);
        FileMode:= 2;
      except
        FileMode:= 2;
        Exit;
      end;
    end;
    SetLength(VersionID, 3);
    BlockRead(F, VersionID[1], 6);
    BlockRead(F, ItemCount, 4);
    if VersionID = 'A31' then
    begin
//      FNeedFullUpdate:= True;
      SetLength(FItems, ItemCount);
      if ItemCount <> 0 then BlockRead(F, FItems[0], SizeOf(FItems[0]) * ItemCount);
      CloseFile(F);
    end;
  end;
end;

procedure TDataBase.Save;
var
  I: Integer;
  VersionID: string;
  ItemCount: Integer;
  F: file;

begin
  AssignFile(F, FDataFileName);
  ReWrite(F, 1);
  ItemCount:= Length(FItems);
  VersionID:= 'A31';
  BlockWrite(F, VersionID[1], 6);
  BlockWrite(F, ItemCount, 4);
  if ItemCount <> 0 then BlockWrite(F, FItems[0], SizeOf(FItems[0]) * ItemCount);
  CloseFile(F);
end;

function TDataBase.GetIndex(FileName: string): Integer;
var
  I: Integer;

begin
  for I:= 0 to GetCount - 1 do
    if AnsiUpperCase(FileName) = AnsiUpperCase(FItems[I].FileName) then
    begin
      Result:= I;
      Exit;
    end;
  Result:= - 1;
end;

procedure TDataBase.OnChangeLP(Sender: TObject);
begin
end;

function TDataBase.Update(BasePath: string): Boolean;
label
  StopUpdate;

var
  J, I: Integer;
  Flag1, Flag2: Boolean;
  SystemTime: TSystemTime;
  NewDBItems: TDataBaseArray;
  FDS: TAviconDataSource;
  P: Pointer;
  SS: string;
  Size: Integer;
  S: array [0..2047] of Byte;
  UCSFlag, BlkUpdate, Flagg: Boolean;
  ListItem: TListItem;
  FileList: TStringList;
  RES_, OKFlag: Boolean;
  CreateBaseProgForm: TCreateBaseProgForm;

procedure ScanDir(StartDir: string; Mask: string; List: TStrings);
var
  SearchRec: TSearchRec;

begin
  if Mask = '' then Mask := '*.*';
  if StartDir[Length(StartDir)] <> '\' then
  StartDir := StartDir + '\';
  if FindFirst(StartDir + Mask, faAnyFile, SearchRec) = 0 then
  begin
    repeat
    //  Application.ProcessMessages;
      if (SearchRec.Attr and faDirectory) <> faDirectory
        then List.AddObject(StartDir + SearchRec.Name, Pointer(SearchRec.Size))
        else if (SearchRec.Name <> '..') and (SearchRec.Name <> '.') then
        begin
          List.AddObject(StartDir + SearchRec.Name + '\', Pointer(0));
          ScanDir(StartDir + SearchRec.Name + '\', Mask, List);
        end;
    until FindNext(SearchRec) <> 0;
    FindClose(SearchRec);
  end;
end;

begin
  Result:= False;

  CreateBaseProgForm:= TCreateBaseProgForm.Create(nil);
  CreateBaseProgForm.Visible:= True;
  CreateBaseProgForm.BringToFront;
  CreateBaseProgForm.SetFocus;

  FileList:= TStringList.Create;
  ScanDir(BasePath, '*.*', FileList);
{
  with CreateBaseProgForm.ListView do
  begin
    Items.BeginUpdate;
    Items.Clear;
    for I:= 0 to FileList.Count - 1 do
    begin
      ListItem:= Items.Add;
      ListItem.Caption:= FileList.Strings[I];
    end;
    Items.EndUpdate;
  end;
}
  SetLength(NewDBItems, 0); // ����� ������ (����� ��� ������������ �� ������� ������ FItems)
  for J:= 0 to FileList.Count - 1 do
    if FileExists(FileList.Strings[J]) and     // ����������� ��� ��������� ����� � ������ >= HEAD ������
       (Integer(FileList.Objects[J]) >= SizeOf(TFileHeader)) then
    begin
{
      with CreateBaseProgForm.ListView do
      if (J >= VisibleRowCount div 2) and
         (J <= FileList.Count - 1 - VisibleRowCount div 2) then Items.Item[J + VisibleRowCount div 2].MakeVisible(True);
      CreateBaseProgForm.Gauge1.Progress:= 0;
      CreateBaseProgForm.Panel1.Top:= CreateBaseProgForm.ListView.Items.Item[J].Position.Y + 2;
      CreateBaseProgForm.Repaint; }

      CreateBaseProgForm.Panel2.Caption:= ExtractFilePath(FileList.Strings[J]);
      CreateBaseProgForm.Panel3.Caption:= ExtractFileName(FileList.Strings[J]);
      CreateBaseProgForm.Gauge1.Position:= 100 * J div FileList.Count - 1;

      Application.ProcessMessages();

      if not FNeedFullUpdate then
      begin
        I:= GetIndex(FileList[J]); // ��������� ���� �� ���� ����� ��� � ���� ������
        if I <> - 1 then // ���� ���� � ���� ������ ��� ���� �� ������� ��� ���� �������� � ������
          if (FItems[I].FileDateTime = FileDateToDateTime(FileAge(FileList.Strings[J]))) and
             (FItems[I].FileSize = Integer(FileList.Objects[J])) then
          begin
                             // ���� ��� ��������� �� �������, ��� ��� ���-�� ����
            SetLength(NewDBItems, Length(NewDBItems) + 1); // ��������� ��� � ����� ������
            NewDBItems[High(NewDBItems)]:= FItems[I];
                                  // ��������� ����
            Continue;
          end;
      end;
                    // ���� ����� � ����� ������ � �� ��� �� ��������� ��� ���������

      Result:= True;
      try
        FDS:= TAviconDataSource.Create;
        try
          RES_:= FDS.LoadFromFile(FileList[J], False);
    //      FileList[J]:= FileList[J] + ' OK';
        except
    //      FileList[J]:= FileList[J] + ' BAD';
          RES_:= False;
        end;
    //    FileList.SaveToFile('CreateBD.log');


        if RES_ then
        begin
          if (FDS.CoordSys = csMetricRF) and
             (FDS.ExHeader.EndMRFCrd.Km = 0) and
             (FDS.ExHeader.EndMRFCrd.Pk = 0) then FDS.ReAnalyze; // ��������

          I:= Length(NewDBItems);
          SetLength(NewDBItems, I + 1);
          NewDBItems[I].FileName:=      FileList.Strings[J];                                       // ��� �����
          NewDBItems[I].FileSize:=      Integer(FileList.Objects[J]);                              // ������ �����
          NewDBItems[I].FileDateTime:=  FileDateToDateTime(FileAge(FileList.Strings[J]));          // ���� � ����� �������� �����


          NewDBItems[I].DeviceID       := FDS.Header.DeviceID           ;
          NewDBItems[I].DeviceVer      := FDS.Header.DeviceVer          ;
          NewDBItems[I].HeaderVer      := FDS.Header.HeaderVer          ;
          NewDBItems[I].MoveDir        := FDS.Header.MoveDir            ;
          NewDBItems[I].PathCoordSystem:= FDS.Header.PathCoordSystem    ;
          NewDBItems[I].UsedItems      := FDS.Header.UsedItems          ;
          NewDBItems[I].UnitsCount     := FDS.Header.UnitsCount         ;
          NewDBItems[I].UnitsInfo      := FDS.Header.UnitsInfo          ;
          NewDBItems[I].Organization   := FDS.Header.Organization       ;
          NewDBItems[I].RailRoadName   := FDS.Header.RailRoadName       ;
          NewDBItems[I].DirectionCode  := FDS.Header.DirectionCode      ;
          NewDBItems[I].PathSectionName:= FDS.Header.PathSectionName    ;
          NewDBItems[I].OperatorName   := FDS.Header.OperatorName       ;

          {if FDS.Header.UsedItems[uiRailPathNumber] = 1 then }
          NewDBItems[I].RailPathNumber := FDS.GetRailPathNumber_in_HeadStr;
          //if FDS.Header.UsedItems[uiRailPathTextNumber] = 1 then NewDBItems[I].RailPathNumber := FDS.Header.RailPathTextNumber;

          NewDBItems[I].RailSection    := FDS.Header.RailSection        ;
          NewDBItems[I].Year           := FDS.Header.Year               ;
          NewDBItems[I].Month          := FDS.Header.Month              ;
          NewDBItems[I].Day            := FDS.Header.Day                ;
          NewDBItems[I].Hour           := FDS.Header.Hour               ;
          NewDBItems[I].Minute         := FDS.Header.Minute             ;
          NewDBItems[I].StartKM        := FDS.Header.StartKM            ;
          NewDBItems[I].StartPk        := FDS.Header.StartPk            ;
          NewDBItems[I].StartMetre     := FDS.Header.StartMetre         ;
          NewDBItems[I].WorkRailTypeA  := FDS.Header.WorkRailTypeA      ;
          NewDBItems[I].StartChainage  := FDS.Header.StartChainage      ;
          NewDBItems[I].WorkRailTypeB  := FDS.Header.WorkRailTypeB      ;
          NewDBItems[I].TrackDirection := FDS.Header.TrackDirection     ;
          NewDBItems[I].TrackID        := FDS.Header.TrackID            ;



          NewDBItems[I].Length:=        Round(FDS.DisToSysCoord(FDS.MaxDisCoord) * FDS.Header.ScanStep / 100 / 1000);
          NewDBItems[I].EndMRFCrd:=     FDS.ExHeader.EndMRFCrd;             // �������� ����������
          NewDBItems[I].EndCaCrd:=      FDS.ExHeader.EndCaCrd;
        end;
        FDS.Free;
        FDS:= nil;
        OKFlag:= True;
      except
        FDS.Free;
        FDS:= nil;
        OKFlag:= False;
      end;

      if CreateBaseProgForm.BaseUpdateStopFlag then goto StopUpdate;

{      if OKFlag then CreateBaseProgForm.ListView.Items.Item[J].SubItems.Add(LangTable.Caption['Common:OK'])
                else CreateBaseProgForm.ListView.Items.Item[J].SubItems.Add(LangTable.Caption['Common:Error']);
      CreateBaseProgForm.Repaint; }
    end;

  if FileList.Count = 0 then Result:= True;
  if Length(FItems) <> Length(NewDBItems) then Result:= True;

  CreateBaseProgForm.Release;
  SetLength(FItems, 0);
  FItems:= NewDBItems;
  SetLength(NewDBItems, 0);
  if Result then Save;
  FileList.Free;
  Exit;

StopUpdate:

  Result:= False;
  CreateBaseProgForm.Release;
  SetLength(NewDBItems, 0);
  FileList.Free;

end;

var
reg : TRegistry;
s   : string;

begin

//  reg:= TRegistry.Create;
//  reg.WriteBinaryData RootKey:= HKEY_CURRENT_USER;
//  reg.

end.
