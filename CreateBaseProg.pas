unit CreateBaseProg; {Language 7}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Gauges, ExtCtrls, StdCtrls, LanguageUnit;

type
  TCreateBaseProgForm = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Button1: TButton;
    Gauge1: TProgressBar;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure OnChangeLanguage(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
  public
    BaseUpdateStopFlag: Boolean;
  end;


implementation


{$R *.DFM}

procedure TCreateBaseProgForm.FormShow(Sender: TObject);
begin
//  self.Top:= 1000;
end;

procedure TCreateBaseProgForm.FormCreate(Sender: TObject);
begin
  OnChangeLanguage(nil);
  BaseUpdateStopFlag:= False;
//  self.Position:= poDefault;
end;

procedure TCreateBaseProgForm.Button1Click(Sender: TObject);
begin
  Self.Close;
end;

procedure TCreateBaseProgForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  ExB: WORD;

begin
  ExB:= MessageBox(Handle, PChar(LangTable.Caption['CreateBase:Abort']), PChar(LangTable.Caption['Common:Attention']), 36);
  if Exb = IDYES then
  begin
    BaseUpdateStopFlag:= True;
    CanClose:= True;
  end;
  if Exb = IDNO then
  begin
    BaseUpdateStopFlag:= False;
    CanClose:= False;
  end;
end;

procedure TCreateBaseProgForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if Msg.CharCode = 27 then Self.Close;
end;

procedure TCreateBaseProgForm.OnChangeLanguage(Sender: TObject);
begin
  Button1.Caption                   := LangTable.Caption['Common:Cancel'];
  Self.Font.Name                    := LangTable.Caption['General:FontName'];
//  ListView.Font.Name                := LangTable.Caption['General:FontName'];
  Self.Caption                      := LangTable.Caption['CreateBase:Updating'];
//  ListView.Columns.Items[0].Caption := LangTable.Caption['CreateBase:File'];
//  ListView.Columns.Items[1].Caption := LangTable.Caption['CreateBase:Status'];
end;

end.



