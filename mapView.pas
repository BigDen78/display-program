unit mapView;

interface

uses
  SysUtils,
  Classes,
  Dialogs,
  Forms,
  ShellAPI,
  Windows,
  AviconDataSource,
  AviconTypes,
  LanguageUnit,
  Math;

function showPathOnMap(SrcData: TAvk11DatSrc): integer;
function MakeGPXfile(SrcData: TAvk11DatSrc; OutFileName: string): integer;


  type TMarkedPoint = class
    private
      _x   : double;
      _y   : double;
      _lbl : UnicodeString;

    public
      property x : double
        read _x write _x;
      property y : double
        read _y write _y;
      property lbl : UnicodeString
        read _lbl write _lbl;

    constructor Create(const __x : double = 0.0; const __y : double = 0.0; const __lbl : UnicodeString = '');
  end;

implementation

constructor TMarkedPoint.Create(const __x: Double; const __y: Double; const __lbl: UnicodeString);
begin
  self._x   := __x;
  self._y   := __y;
  self._lbl := __lbl;
end;

// -----------------------------

procedure form_load(path: TList);
var
  str           : TStringStream;
  size          : integer;
  point         : TMarkedPoint;
  saveFilePath  : UnicodeString;
  ds            : char;
  mapfn         : string;

procedure correctList(lst: TList);
const
  MAX_ERROR = 0.01;
var
  size   : integer;
  i      : integer;
  avgLat,
  avgLon : double;
begin
  avgLat := 0.0;
  avgLon := 0.0;
  size   := lst.Count;

  for i := 0 to size-1 do
    begin
      avgLat := avgLat + TMarkedPoint( lst.Items[i] ).x;
      avgLon := avgLon + TMarkedPoint( lst.Items[i] ).y;
    end;

  avgLat := avgLat / size;
  avgLon := avgLon / size;

  i := 0;
  while (i < size-1) do
  begin
    if ( (abs(TMarkedPoint(lst.Items[i]).x - avgLat) / avgLat > MAX_ERROR)
      OR (abs(TMarkedPoint(lst.Items[i]).y - avgLon) / avgLon > MAX_ERROR) ) then
      begin
        lst.Remove( lst.Items[i] );
        dec( size );
      end;
    inc( i );
  end;
end;

begin
  ds := DecimalSeparator;
  DecimalSeparator := '.';
  str := TStringStream.Create;

  correctList(path);

  // Initialization HTML's headers
	str.WriteString('<!Doctype html>');
	str.WriteString('<html>');
	str.WriteString('<head>');
	str.WriteString('<meta name="viewport" content="initial-scale=1.0, user-scalable=no">');
	str.WriteString('<title>Path</title>');
	str.WriteString('<meta http-equiv="Content-Type" content="text/html; charset=CP1251" />');
	str.WriteString('        <style>');
	str.WriteString('            html, body, #map-canvas {');
	str.WriteString('            height: 100%;');
	str.WriteString('            margin: 0px;');
	str.WriteString('        	  padding: 0px}');
	str.WriteString('        </style> ');
	str.WriteString('		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>');
	str.WriteString('        <script>');

  // Set intermediate marked point
	str.WriteString('function setMarkers(map,locations) {');
	str.WriteString('	var i, marker;');
	str.WriteString('	for (i = 0; i < locations.length; i++) {');
	str.WriteString('		var lat = locations[i][0];');
	str.WriteString('		var lon = locations[i][1];');
	str.WriteString('		var caption = locations[i][2];');
	str.WriteString('		var langSet = new google.maps.LatLng(lat, lon);');
	str.WriteString('		var marker = new google.maps.Marker({map: map, title: ''������������� �����'', position: langSet, icon: ''http://maps.google.com/mapfiles/ms/icons/blue-dot.png''});');
	str.WriteString('		var content = ''<b>'' + caption + ''</b>'';');
	str.WriteString('		var infowindow = new google.maps.InfoWindow({maxWidth: 150});');

	str.WriteString('		google.maps.event.addListener(marker,''click'', (function(marker,content,infowindow){ ');
	str.WriteString('		return function() {');
	str.WriteString('			infowindow.setContent(content);');
	str.WriteString('			infowindow.open(map,marker);}');
	str.WriteString('		})(marker,content,infowindow)); ');
	str.WriteString('}}');

  // map initialization
	str.WriteString('        function initialize(){');
	str.WriteString('            var mapOptions = {');
	str.WriteString('				zoom: 12,');


  point := TMarkedPoint.Create;
	size  := 0;

  repeat
    if (size = path.Count / 2) then
    begin
       point.x    := TMarkedPoint(path.Items[size]).x;
       point.y    := TMarkedPOint(path.Items[size]).y;
       point.lbl  := TMarkedPoint(path.Items[size]).lbl;
       break;
    end;
		inc( size );
  until (size = path.Count);

  str.WriteString('				center: new google.maps.LatLng(' + FloatToStrF( point.x, ffGeneral, 9, 6 ) + ',' + FloatToStrF( point.y, ffGeneral, 9, 6 ) + '),');
	str.WriteString('				mapTypeId: google.maps.MapTypeId.ROADMAP};');
	str.WriteString('  	  var map = new google.maps.Map(document.getElementById(''map-canvas''),mapOptions);');


  // Define path coordinates
	str.WriteString('var pathCoordinates = [');
	size := 0;

  repeat
    point.x   := TMarkedPoint(path.Items[size]).x;
    point.y   := TMarkedPOint(path.Items[size]).y;
    point.lbl := TMarkedPoint(path.Items[size]).lbl;

		str.WriteString('new google.maps.LatLng(' + FloatToStrF( point.x, ffGeneral, 9, 6 ) + ', ' + FloatToStrF( point.y, ffGeneral, 9, 6 ) + ')');
		if (size <> path.Count-1) then
			str.WriteString(',');

		inc( size );
	until (size = path.Count);

  str.WriteString('];');

	// Add coordinates for markers
	str.WriteString('var markedCoordinates = [');
	size := 0;

  repeat
		if (TMarkedPoint(path.Items[size]).lbl <> '') then
    begin
      point.x   := TMarkedPoint(path.Items[size]).x;
      point.y   := TMarkedPOint(path.Items[size]).y;
      point.lbl := TMarkedPoint(path.Items[size]).lbl;

			str.WriteString('[' + FloatToStrF( point.x, ffGeneral, 9, 6 ) + ', ' + FloatToStrF( point.y, ffGeneral, 9, 6 ) + ' , ''' + point.lbl + ''']');
			str.WriteString(',');
    end;
		inc( size );
	until (size = path.Count);

  str.WriteString('];');

  // Define polyline options
	str.WriteString('			var Line = new google.maps.Polyline({');
	str.WriteString('				path: pathCoordinates,');
	str.WriteString('				geodesic: true,');
	str.WriteString('				strokeColor: ''#0000FF'',');
	str.WriteString('				strokeOpacity: 1.0,');
	str.WriteString('				strokeWeight: 2');
	str.WriteString('			});');

	// Add line at map
	str.WriteString(' 			Line.setMap(map);');

  // Add markers start and end points
	str.WriteString('		var startMarker = new google.maps.Marker({');
  point.x   := TMarkedPoint(path.First()).x;
  point.y   := TMarkedPOint(path.First()).y;
  point.lbl := TMarkedPoint(path.First()).lbl;
  str.WriteString('			position: new google.maps.LatLng(' + FloatToStrF( point.x, ffGeneral, 9, 6 ) + ', ' + FloatToStrF( point.y, ffGeneral, 9, 6 ) + '),');
  str.WriteString('			map: map,');
	str.WriteString('			title: ''��������� �����''});'); //NO_LANGUAGE

	str.WriteString('		var endMarker = new google.maps.Marker({');
  point.x   := TMarkedPoint(path.Last()).x;
  point.y   := TMarkedPOint(path.Last()).y;
  point.lbl := TMarkedPoint(path.Last()).lbl;
	str.WriteString('			position: new google.maps.LatLng(' + FloatToStrF( point.x, ffGeneral, 9, 6 ) + ', ' + FloatToStrF( point.y, ffGeneral, 9, 6 ) + '),');
	str.WriteString('			map: map,');
	str.WriteString('			title: ''�������� �����''});'); //NO_LANGUAGE

	// Add marker description
	str.WriteString('		var startString = "<h2><b>��������� �����</b></h2>";'); //NO_LANGUAGE
	str.WriteString('		var endString = "<h2><b>�������� �����</b></h2>";'); //NO_LANGUAGE

  // Add infowindows
	str.WriteString('		var startInfoWindow = new google.maps.InfoWindow({content: startString, maxWidth: 150});');
	str.WriteString('		var endInfoWindow = new google.maps.InfoWindow({content: endString, maxWidth: 150});');

  	// Add listener
	str.WriteString('google.maps.event.addListener(startMarker, ''click'', function() {startInfoWindow.open(map,startMarker);});');
	str.WriteString('google.maps.event.addListener(endMarker, ''click'', function() {endInfoWindow.open(map,endMarker);});');

	// Add bounds for auto zoom
	str.WriteString('	var bounds = new google.maps.LatLngBounds();');
	str.WriteString('	for (var i = 0, LtLgLen = pathCoordinates.length; i < LtLgLen; i++) {');
	str.WriteString('		bounds.extend (pathCoordinates[i]);}');
	str.WriteString('	map.fitBounds(bounds);');

	// Add marked intermediate points at map
	str.WriteString('setMarkers(map, markedCoordinates);');

	str.WriteString('	}');

  // Load defined window
	str.WriteString('google.maps.event.addDomListener(window, ''load'', initialize);');
	str.WriteString('    </script></head>');
	str.WriteString('    <body scroll="no"><div id="map-canvas"></div><body>');
	str.WriteString('</html>');

  saveFilePath := ExtractFilePath( Application.ExeName );
  str.SaveToFile( saveFilePath + 'map.html' );

  str.Free;

  mapfn:= saveFilePath + 'map.html';
  ShellExecute(0, nil, PChar(mapfn), nil, nil, SW_RESTORE);

  DecimalSeparator := ds;
end;

// -----------------------------

function showPathOnMap(SrcData: TAvk11DatSrc): integer;
var
  I: Integer;
  pData: PEventData;
  ID_: Byte;
  count : integer;
  list : TList;
  str : UnicodeString;
  crdstr: UnicodeString;
  Lat_: Single;
  Lon_: Single;
  CrdParams: TCrdParams;

begin

  list := TList.Create;
  count := 0;
  str := '';

  Lat_:= 0;
  Lon_:= 0;

  with SrcData do
  for I := 0 to EventCount do
  begin

//  ������ ������ ���, ����������, �������� �����������, ������� � �������


    crdstr:= '';
    DisToCoordParams(Event[I].DisCoord, CrdParams);
    crdstr:= RealCrdToStr(CrdParamsToRealCrd(CrdParams, Header.MoveDir)) + ' ';

    if (Event[I].ID = EID_HandScan) then
    begin
      inc(count);
      str:= str + crdstr + LangTable.Caption['HandScan:HandScan'] + ', ';
    end;

    if (Event[I].ID = EID_Media) then
    begin
      if Event[I].Data[1] = 1 then str:= str + crdstr + '�������� �����������' + ', ';    // NO_LANGUAGE
      if Event[I].Data[1] = 2 then str:= str + crdstr + '����������' + ', ';              // NO_LANGUAGE
      if Event[I].Data[1] = 3 then str:= str + crdstr + '�����' + ', ';                   // NO_LANGUAGE
      inc(count);
    end;

    if (Event[I].ID = EID_DefLabel) then
    begin
      inc(count);
      str:= str + crdstr + LangTable.Caption['Config:Defect'] + ', ';
//      list.Add(TMarkedPoint.Create(pGPSCoord(pData)^.Lat, pGPSCoord(pData)^.Lon, LangTable.Caption['Config:Defect'] + ', Test'));
    end;

    if (Event[I].ID = EID_GPSCoord) then
    begin
      GetEventData(I, ID_, pData);

      try
//        if (Trunc(Lat_ * 100000) <> Trunc(pedGPSCoord(pData)^.Lat * 100000))  or
//          (Trunc(Lon_ * 100000) <> Trunc(pedGPSCoord(pData)^.Lon * 100000)) then
        begin
          if (not isnan(pedGPSCoord(pData)^.Lat)) and
             (not isnan(pedGPSCoord(pData)^.Lon)) then
          begin
            Lat_:= pedGPSCoord(pData)^.Lat;
            Lon_:= pedGPSCoord(pData)^.Lon;
            SetLength(str, Length(str) - 2);
            list.Add(TMarkedPoint.Create(pedGPSCoord(pData)^.Lat, pedGPSCoord(pData)^.Lon, str));
            str:= '';
          end;
        end;
      finally

      end;
    end;

    if (Event[I].ID = EID_GPSCoord2) then
    begin
      GetEventData(I, ID_, pData);

      try
//        if (Trunc(Lat_ * 100000) <> Trunc(pedGPSCoord2(pData)^.Lat * 100000)) or
//           (Trunc(Lon_ * 100000) <> Trunc(pedGPSCoord2(pData)^.Lon * 100000)) then
        begin
          if (not isnan(pedGPSCoord2(pData)^.Lat)) and
             (not isnan(pedGPSCoord2(pData)^.Lon)) then
          begin
            Lat_:= pedGPSCoord2(pData)^.Lat;
            Lon_:= pedGPSCoord2(pData)^.Lon;
            SetLength(str, Length(str) - 2);
            list.Add(TMarkedPoint.Create(pedGPSCoord2(pData)^.Lat, pedGPSCoord2(pData)^.Lon, str));
            str:= '';
          end;
        end;
      finally

      end;
    end;
  end;


  if ( list.Count = 0 ) then
  begin
    ShowMessage('There is nothing to show!'); //NOTRANSLATE
    Result := -1;
  end
  else
  begin
    try
      form_load( list);
    except
      Result := -1;
    end;
  end;

end;

function MakeGPXfile(SrcData: TAvk11DatSrc; OutFileName: string): integer;
var
  I: Integer;
  pData: PEventData;
  ID_: Byte;
  count : integer;
//  list : TList;
  str : UnicodeString;
  crdstr: UnicodeString;
  Lat_: Single;
  Lon_: Single;
  CrdParams: TCrdParams;
  res: TStringList;

  Time: string;
  LastTime: string;

begin
  res:= TStringList.Create;

//  list := TList.Create;
  count := 0;
  str := '';

  Lat_:= 0;
  Lon_:= 0;

  res.Add('<?xml version="1.0" encoding="UTF-8"?>');
  res.Add('<gpx');
  res.Add('xmlns="http://www.topografix.com/GPX/1/1"');
  res.Add('creator="Radioavionica"');
  res.Add('version="1.1">');

  res.Add('<metadata>');
  res.Add('<name>Name</name>');
  res.Add('</metadata>');
  res.Add('<trk>');
  res.Add('<name>' + ExtractFileName(SrcData.FileName) + '</name>');
  res.Add('<trkseg>');


  LastTime:= '';
  with SrcData do
  for I := 0 to EventCount do
  begin

//  ������ ������ ���, ����������, �������� �����������, ������� � �������

      {
    Time:= GetTime(Event[I].DisCoord);
    if LastTime <> Time then
    begin
      res.Add(Format('<time>%s</time>', [Time]));
      LastTime:= Time;
    end;
          }
    crdstr:= '';
    DisToCoordParams(Event[I].DisCoord, CrdParams);
    crdstr:= RealCrdToStr(CrdParamsToRealCrd(CrdParams, Header.MoveDir)) + ' ';

    if (Event[I].ID = EID_HandScan) then
    begin
      inc(count);
      str:= str + crdstr + LangTable.Caption['HandScan:HandScan'] + ', ';
    end;

    if (Event[I].ID = EID_RailHeadScaner) then
    begin
      inc(count);
      str:= str + crdstr + LangTable.Caption['EventsName:ManualScan'] + ' (������ ������� ������)'; //NO_LANGUAGE

    end;

    if (Event[I].ID = EID_Media) then
    begin
      if Event[I].Data[1] = 1 then str:= str + crdstr + '�������� �����������' + ', ';    // NO_LANGUAGE
      if Event[I].Data[1] = 2 then str:= str + crdstr + '����������' + ', ';              // NO_LANGUAGE
      if Event[I].Data[1] = 3 then str:= str + crdstr + '�����' + ', ';                   // NO_LANGUAGE
      inc(count);
    end;

    if (Event[I].ID = EID_DefLabel) then
    begin
      inc(count);
      str:= str + crdstr + LangTable.Caption['Config:Defect'] + ', ';
//      list.Add(TMarkedPoint.Create(pGPSCoord(pData)^.Lat, pGPSCoord(pData)^.Lon, LangTable.Caption['Config:Defect'] + ', Test'));
    end;

    if (Event[I].ID = EID_GPSCoord) then
    begin
      GetEventData(I, ID_, pData);

      try
        begin
          if (not isnan(pedGPSCoord(pData)^.Lat)) and
             (not isnan(pedGPSCoord(pData)^.Lon)) then
          begin
            Lat_:= pedGPSCoord(pData)^.Lat;
            Lon_:= pedGPSCoord(pData)^.Lon;
            SetLength(str, Length(str) - 2);

            res.Add(Format('<trkpt lat="%2.9f" lon="%2.9f">', [pedGPSCoord(pData)^.Lat, pedGPSCoord(pData)^.Lon]));
            res.Add(Format('<time>%d-%s-%sT%s:00Z</time>', [Header.Year, UpgradeInt(Header.Month, '0', 2), UpgradeInt(Header.Day, '0', 2), GetTime(Event[I].DisCoord)]));
            res.Add('<fix>2d</fix>');
            //res.Add(Format('<sat>%d</sat>', []));
            //res.Add(Format('<name>"%s"</name>', [str]));
//            res.Add(Format('<time>%s</time>', [GetTime(Event[I].DisCoord)]));
            //res.Add(Format('<desc>%s</desc>', [str]));
            res.Add('</trkpt>');
            str:= '';
          end;
        end;
      finally

      end;
    end;

    if (Event[I].ID = EID_GPSCoord2) then
    begin
      GetEventData(I, ID_, pData);

      try
        begin
          if (not isnan(pedGPSCoord2(pData)^.Lat)) and
             (not isnan(pedGPSCoord2(pData)^.Lon)) then
          begin
            Lat_:= pedGPSCoord2(pData)^.Lat;
            Lon_:= pedGPSCoord2(pData)^.Lon;
            SetLength(str, Length(str) - 2);



            res.Add(Format('<trkpt lat="%2.9f" lon="%2.9f">', [pedGPSCoord2(pData)^.Lat, pedGPSCoord2(pData)^.Lon]));
            //res.Add(Format('<time>2011-09-22T18:56:51Z</time>', [pedGPSCoord(pData)^.Lat, pedGPSCoord(pData)^.Lon]));
            res.Add('<fix>2d</fix>');
            //res.Add(Format('<sat>%d</sat>', []));
            //res.Add(Format('<name>"%s"</name>', [str]));
            //res.Add(Format('<desc>%s</desc>', [str]));
            res.Add(Format('<speed>%3.2f</speed>', [pedGPSCoord2(pData)^.Speed / 3.6]));
            res.Add(Format('<time>%d-%s-%sT%s:00Z</time>', [Header.Year, UpgradeInt(Header.Month, '0', 2), UpgradeInt(Header.Day, '0', 2), GetTime(Event[I].DisCoord)]));
            res.Add('</trkpt>');

            str:= '';
          end;
        end;
      finally

      end;
    end;

    if str <> '' then
    begin
      res.Add(Format('<wpt lat="%2.9f" lon="%2.9f">', [Lat_, Lon_]));
      res.Add(Format('<name>Test</name>', [str]));
      res.Add(Format('</wpt>', [str]));

    end;

  end;

{
  if ( list.Count = 0 ) then
  begin
    ShowMessage('There is nothing to show!'); //NOTRANSLATE
    Result := -1;
  end
  else
  begin
    try
      form_load( list);
    except
      Result := -1;
    end;
  end;
}
  res.Add('</trkseg>');
  res.Add('</trk>');
  res.Add('</gpx>');
  res.SaveToFile(OutFileName);
  res.free;
end;


end.

// http://share.mapbbcode.org
// http://www.gpsvisualizer.com
// http://www.openstreetmap.org
