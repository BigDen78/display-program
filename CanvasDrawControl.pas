unit CanvasDrawControl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {DirectDraw,} ExtCtrls, Math, {DirectDrawControl,} CustomDirectDrawControl;

type
  TDDFont = record
    FontName: string;
    FontHeight: Integer;
    FontColor: TColor;
    BGColor: TColor;
    Vertical: Boolean;
    Bold: Boolean;
    Font: TFont;
  end;

  TDDPic = record
    Surf: TBitMap;
    Size: TSize;
  end;

  TCanvasDrawControl = class(TCustomDirectDrawControl)
  private
  protected
    BackBuffer: TBitMap;
    FPic: array of TDDPic;
    FFont: array of TDDFont;
    FOnLostData: TNotifyEvent;
    OutDir: Boolean;
    FClipRect: TRect;
    FResColor:  DWord;
    FMixColor: Boolean;
    FLastTextOutX: Integer;

    procedure Paint; override;
    procedure Resize; override;

  public
    SelRect: TRect;
    DrawSelRect: Boolean;
    DrawLeftCur: Boolean;
    LeftCurTop: TPoint;
    LeftCurHeight: Integer;
    DrawRightCur: Boolean;
    RightCurTop: TPoint;
    RightCurHeight: Integer;
    DrawCentrCur: Boolean;
    CentrCurTop: TPoint;
    CentrCurHeight: Integer;
    NowInit: Boolean;

    procedure InitDirectDraw; override;
    procedure FinDirectDraw; override;
    procedure Chk(hr : HRESULT); override;
    function GetDDColor(Color: TColor): DWORD; override;
    procedure Clear(Color: DWORD); override;
    function LoadFont(FontName: string; FontHeight: Integer; BoldFont: Boolean; FontColor, BGColor: TColor; Vertical: Boolean = False; CreateSurface: Boolean = True): Integer; override;
    function ChangeFontPar(FontIdx: Integer; FontName: string; FontHeight: Integer; FontColor, BGColor: TColor; CreateSurface: Boolean = True): Integer; override;
    function AddPic(NewPic: TBitMap; Transparent: Boolean): Integer; override;
    procedure HLine(X, Y, Len, Width: Integer; Color: DWORD); override;
    procedure VLine(X, Y, Len, Width: Integer; Color: DWORD); override;
    procedure TextOut(FontIdx, X, Y, Align: Integer; Text: string; Transparent: Boolean); override;
    procedure ClippTextOut(FontIdx, X, Y, Align: Integer; Text: string; Transparent: Boolean; ClippRect: TRect);
    procedure TextRect(FontIdx: Integer; Rt: TRect; Align: Integer; Text: string; Transparent: Boolean);
    procedure TextRectColor(FontIdx: Integer; Rt: TRect; Align: Integer; Text: string; Transparent: Boolean; Color: TColor);
    procedure GhostTextOut(FontIdx, X, Y, Align: Integer; Text: string; BGCoolor: DWord);
    function TextWidth(FontIdx: Integer; Text: string): Integer; override;
    function TextHeight(FontIdx: Integer; Text: string): Integer; override;
    procedure DrawRect(Rct_: TRect; Width: Integer; Color: DWord); override;
    procedure DrawRect2(Rct_: TRect; Width: Integer; Color1, Color2: DWord); override;
    procedure DrawSelRect_(Rct_: TRect; Size, Width: Integer; Color: DWord);
    procedure DrawPic(X, Y, Idx: Integer); override;
    function GetPicSize(Idx: Integer): TSize; override;
    procedure FillRect(X1, Y1, X2, Y2: Integer; Color: DWORD); override;
    procedure FillRect_(Rt: TRect; Color: DWord); override;
    procedure FillRgn(Rt: TRect; Idx: Integer); override; // ��� ����������
    procedure Shift(WorkArea: TRect; Shift: Integer); override; // ��� ����������
    procedure StartP; override;
    procedure EndP; override;
    procedure FastPoint(X, Y, DX, DY, Color: Integer); override;
    procedure SetPoint(X, Y, DX, DY, Color: Integer); override;
    procedure Flip; override;
    procedure GetBitMap(var Res: TBitMap); override;
    procedure SetClipRect(New: TRect); override;
    procedure ReSetClipRect();
    procedure RestoreAllSurface; override;

    property TestColor1: DWord read FTestColor1 write FTestColor1;
    property TestColor2: DWord read FTestColor2 write FTestColor2;
    property ResColor: DWord read FResColor write FResColor;
    property MixColor: Boolean read FMixColor write FMixColor;
    property ClipRect: TRect read FClipRect;
    property LastTextOutX: Integer read FLastTextOutX;
    property Pic: TBitMap read BackBuffer;
       
  published
    property OnLostData: TNotifyEvent read FOnLostData write FOnLostData;
    property Align;
    property Caption;
    property Color;
    property Enabled;
    property Font;
    property Visible;
    property OnClick;
    property OnDblClick;
    property OnEnter;
    property OnExit;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
  end;

//procedure register;

implementation

uses Types;

var
  bmpFont: TBitMap;

procedure TCanvasDrawControl.InitDirectDraw;
begin
  OutDir:= True;

  Self.ControlStyle:= Self.ControlStyle - [csOpaque];
  Self.ControlState:= Self.ControlState - [csCustomPaint];

  BackBuffer:= TBitMap.Create;
  BackBuffer.Width:= Self.Width;
  BackBuffer.Height:= Self.Height;
  NowInit:= True;
  FMixColor:= False;
end;

procedure TCanvasDrawControl.FinDirectDraw;
var
  I: Integer;

begin
  NowInit:= False;
  BackBuffer.Free;
  for I:= 0 to High(FPic) do FPic[I].Surf.Free;
  for I:= 0 to High(FFont) do FFont[I].Font.Free;
  SetLength(FFont, 0);
  SetLength(FPic, 0);
end;

procedure TCanvasDrawControl.Flip;
begin
  Paint;
end;

procedure TCanvasDrawControl.GetBitMap(var Res: TBitMap);
begin
  Res:= TBitMap.Create;
  Res.Width:= Width;
  Res.Height:= Height;
  Res.Canvas.CopyRect(Rect(0, 0, Width, Height), BackBuffer.Canvas, Rect(0, 0, Width, Height));
end;

procedure TCanvasDrawControl.SetClipRect(New: TRect);
var
  RGN: HRGN;

begin
  FClipRect:= New;
  Inc(FClipRect.Right);
  Inc(FClipRect.Bottom);

  RGN:= CreateRectRgn(New.Left, New.Top, New.Right, New.Bottom);
  SelectClipRgn(BackBuffer.Canvas.Handle, RGN);
  DeleteObject(RGN);
end;

procedure TCanvasDrawControl.ReSetClipRect();
var
  RGN: HRGN;

begin
  RGN:= CreateRectRgn(0, 0, BackBuffer.Width, BackBuffer.Height);
  SelectClipRgn(BackBuffer.Canvas.Handle, RGN);
  DeleteObject(RGN);
end;

procedure TCanvasDrawControl.RestoreAllSurface;
begin
  //
end;

procedure TCanvasDrawControl.Paint;
begin
  if not Assigned(BackBuffer) then Exit;

  Canvas.CopyRect(Rect(0, 0, Width, Height), BackBuffer.Canvas, Rect(0, 0, Width, Height));

  if DrawSelRect then
  begin
    OutDir:= False;
    DrawRect(Rect(SelRect.Left, SelRect.Top, SelRect.Right, SelRect.Bottom), 1, 0);
    OutDir:= True;
  end;

  if DrawLeftCur then
  begin
    OutDir:= False;
    HLine(LeftCurTop.X, LeftCurTop.Y,                 5,                1, 0);
    HLine(LeftCurTop.X, LeftCurTop.Y + LeftCurHeight, 5,                1, 0);
    VLine(LeftCurTop.X, LeftCurTop.Y                   , LeftCurHeight, 1, 0);
    OutDir:= True;
  end;

  if DrawRightCur then
  begin
    OutDir:= False;
    HLine(RightCurTop.X - 5, RightCurTop.Y,                  5,                 1, 0);
    HLine(RightCurTop.X - 5, RightCurTop.Y + RightCurHeight, 5,                 1, 0);
    VLine(RightCurTop.X,     RightCurTop.Y                    , RightCurHeight, 1, 0);
    OutDir:= True;
  end;

  if DrawCentrCur then
  begin
    OutDir:= False;
    HLine(CentrCurTop.X - 5, CentrCurTop.Y,                  10,                 1, 0);
    HLine(CentrCurTop.X - 5, CentrCurTop.Y + CentrCurHeight, 10,                 1, 0);
    VLine(CentrCurTop.X,     CentrCurTop.Y                     , CentrCurHeight, 1, 0);
    OutDir:= True;
  end;
end;

procedure TCanvasDrawControl.Clear(Color: DWord);
begin
  BackBuffer.Canvas.Brush.Color:= Color;
  BackBuffer.Canvas.FillRect(Rect(0, 0, Width, Height));
end;

procedure TCanvasDrawControl.HLine(X, Y, Len, Width: Integer; Color: DWord);
begin
  FillRect(X, Y, X + Abs(Len) - 1, Y + Width - 1, Color);
end;

procedure TCanvasDrawControl.VLine(X, Y, Len, Width: Integer; Color: DWord);
begin
  FillRect(X, Y, X + Width - 1, Y + Abs(Len) - 1, Color);
end;

procedure TCanvasDrawControl.DrawRect(Rct_: TRect; Width: Integer; Color: DWord);
var
  Rct: TRect;

begin
  Rct.Left:= Min(Rct_.Left, Rct_.Right);
  Rct.Right:= Max(Rct_.Left, Rct_.Right);
  Rct.Top:= Min(Rct_.Top, Rct_.Bottom);
  Rct.Bottom:= Max(Rct_.Top, Rct_.Bottom);
  HLine(Rct.Left,  Rct.Top,    Rct.Right - Rct.Left + 1, Width, Color);
  HLine(Rct.Left,  Rct.Bottom, Rct.Right - Rct.Left + 1, Width, Color);
  VLine(Rct.Left,  Rct.Top,    Rct.Bottom - Rct.Top + 1, Width, Color);
  VLine(Rct.Right, Rct.Top,    Rct.Bottom - Rct.Top + 1, Width, Color);
end;

procedure TCanvasDrawControl.DrawRect2(Rct_: TRect; Width: Integer; Color1, Color2: DWord);
var
  Rct: TRect;

begin
  Rct.Left:= Min(Rct_.Left, Rct_.Right);
  Rct.Right:= Max(Rct_.Left, Rct_.Right);
  Rct.Top:= Min(Rct_.Top, Rct_.Bottom);
  Rct.Bottom:= Max(Rct_.Top, Rct_.Bottom);
  HLine(Rct.Left,  Rct.Top,    Rct.Right - Rct.Left + 1, Width, Color1);
  HLine(Rct.Left,  Rct.Bottom, Rct.Right - Rct.Left + 1, Width, Color2);
  VLine(Rct.Left,  Rct.Top,    Rct.Bottom - Rct.Top + 1, Width, Color1);
  VLine(Rct.Right, Rct.Top,    Rct.Bottom - Rct.Top + 1, Width, Color2);
end;

procedure TCanvasDrawControl.DrawSelRect_(Rct_: TRect; Size, Width: Integer; Color: DWord);
var
  Rct: TRect;

begin
  Rct.Left:= Min(Rct_.Left, Rct_.Right);
  Rct.Right:= Max(Rct_.Left, Rct_.Right);
  Rct.Top:= Min(Rct_.Top, Rct_.Bottom);
  Rct.Bottom:= Max(Rct_.Top, Rct_.Bottom);

  HLine(Rct.Left,         Rct.Top,           Size + 1, Width, Color);
  HLine(Rct.Right - Size, Rct.Top,           Size + 1, Width, Color);

  HLine(Rct.Left,         Rct.Bottom,        Size + 1, Width, Color);
  HLine(Rct.Right - Size, Rct.Bottom,        Size + 1, Width, Color);

  VLine(Rct.Left,         Rct.Top,           Size + 1, Width, Color);
  VLine(Rct.Left,         Rct.Bottom - Size, Size + 1, Width, Color);

  VLine(Rct.Right,        Rct.Top,           Size + 1, Width, Color);
  VLine(Rct.Right,        Rct.Bottom - Size, Size + 1, Width, Color);
end;

procedure TCanvasDrawControl.StartP;
begin
end;

procedure TCanvasDrawControl.EndP;
begin
end;

procedure TCanvasDrawControl.FastPoint(X, Y, DX, DY, Color: Integer);
begin

  if PtInRect(FClipRect, Point(X, Y)) then
  begin
    BackBuffer.Canvas.Brush.Color:= Color;
    BackBuffer.Canvas.FillRect(Rect(X, Y, X + DX, Y + DY));
  end;
end;

procedure TCanvasDrawControl.SetPoint(X, Y, DX, DY, Color: Integer);
begin
  if PtInRect(FClipRect, Point(X, Y)) then
  begin
    BackBuffer.Canvas.Brush.Color:= Color;
    BackBuffer.Canvas.FillRect(Rect(X, Y, X + DX, Y + DY));
  end;
end;

procedure TCanvasDrawControl.FillRect(X1, Y1, X2, Y2: Integer; Color: DWord);
begin
  if OutDir then
  begin
    BackBuffer.Canvas.Brush.Color:= Color;
    BackBuffer.Canvas.FillRect(Rect(X1, Y1, X2 + 1, Y2 + 1));
  end
  else
  begin
    Self.Canvas.Brush.Color:= Color;
    Self.Canvas.FillRect(Rect(X1, Y1, X2 + 1, Y2 + 1));
  end
end;

procedure TCanvasDrawControl.FillRect_(Rt: TRect; Color: DWord);
begin
  FillRect(Rt.Left, Rt.Top, Rt.Right, Rt.Bottom, Color);
end;

procedure TCanvasDrawControl.FillRgn(Rt: TRect; Idx: Integer);
begin
   //
end; 

procedure TCanvasDrawControl.Shift(WorkArea: TRect; Shift: Integer);
begin
end;

procedure TCanvasDrawControl.Resize;
begin
  inherited Resize;
  if NowInit then
  begin
    BackBuffer.FreeImage;
    BackBuffer.Width:= Self.ClientWidth;
    BackBuffer.Height:= Self.ClientHeight;
    Clear(clWhite);
  end;
end;

procedure TCanvasDrawControl.Chk(hr : HRESULT);
begin
end;

function TCanvasDrawControl.GetDDColor(Color: TColor): DWORD;
begin
  Result:= Color;
end;

procedure TCanvasDrawControl.TextRect(FontIdx: Integer; Rt: TRect; Align: Integer; Text: string; Transparent: Boolean);
begin
  TextOut(FontIdx, (Rt.Right + Rt.Left) div 2, (Rt.Bottom + Rt.Top) div 2, 1, Text, Transparent);
end;

procedure TCanvasDrawControl.TextRectColor(FontIdx: Integer; Rt: TRect; Align: Integer; Text: string; Transparent: Boolean; Color: TColor);
var
  X, Y: Integer;
  I: Integer;            //      0 -------- 4 ----------
  AX: Integer;           //      :                     :
  AY: Integer;           //      :                     :
  Rt1: TRect;            //      2          1          3
  Rt2: TRect;            //      :                     :
  W: Integer;            //      :                     :
  H: Integer;            //      ---------- 5 ----------

begin

  X:= (Rt.Right + Rt.Left) div 2;
  Y:= (Rt.Bottom + Rt.Top) div 2;
  Align:= 1;

  if (not NowInit) or (FontIdx > High(FFont)) then Exit;
  {
  BackBuffer.Canvas.Font.Name:= FFont[FontIdx].FontName;
  BackBuffer.Canvas.Font.Height:= FFont[FontIdx].FontHeight;
  BackBuffer.Canvas.Font.Color:= FFont[FontIdx].FontColor;
   }

  BackBuffer.Canvas.Font.Assign(FFont[FontIdx].Font);
  BackBuffer.Canvas.Font.Color:= Color;

  if Transparent then BackBuffer.Canvas.Brush.Style:= bsClear
                 else BackBuffer.Canvas.Brush.Color:= FFont[FontIdx].BGColor;

  if FFont[FontIdx].Vertical then
  begin
    H:= BackBuffer.Canvas.TextWidth(Text);
    W:= BackBuffer.Canvas.TextHeight(Text);
    case Align of
      0: ;
      1: begin
           X:= X - W div 2;
           Y:= Y + H div 2;
         end;
      2: Y:= Y + H div 2;
      3: begin
           X:= X - W;
           Y:= Y + H div 2;
         end;
      4: X:= X - W div 2;
      5: begin
           X:= X - W div 2;
           Y:= Y + H;
         end;
    end;
  end
  else
  begin
    W:= BackBuffer.Canvas.TextWidth(Text);
    H:= BackBuffer.Canvas.TextHeight(Text);
    case Align of
      0: ;
      1: begin
           X:= X - W div 2;
           Y:= Y - H div 2;
         end;
      2: Y:= Y - H div 2;
      3: begin
           X:= X - W;
           Y:= Y - H div 2;
         end;
      4: X:= X - W div 2;
      5: begin
           X:= X - W div 2;
           Y:= Y - H;
         end;
    end;
  end;

  BackBuffer.Canvas.TextOut(X, Y, Text);
  FLastTextOutX:= X + BackBuffer.Canvas.TextWidth(Text);

end;

procedure DrawClippedText_(Canvas: TCanvas; X, Y: Integer; ClippRect: TRect; Text: string);
begin
  ExtTextOut(Canvas.Handle, X, Y, ETO_CLIPPED, @ClippRect, Text, Length(Text), nil);
end;

procedure TCanvasDrawControl.ClippTextOut(FontIdx, X, Y, Align: Integer; Text: string; Transparent: Boolean; ClippRect: TRect);
var
  I: Integer;            //      0 -------- 4 ----------
  AX: Integer;           //      :                     :
  AY: Integer;           //      :                     :
  Rt1: TRect;            //      2          1          3
  Rt2: TRect;            //      :                     :
  W: Integer;            //      :                     :
  H: Integer;            //      ---------- 5 ----------

begin
  if (not NowInit) or (FontIdx > High(FFont)) then Exit;
  {
  BackBuffer.Canvas.Font.Name:= FFont[FontIdx].FontName;
  BackBuffer.Canvas.Font.Height:= FFont[FontIdx].FontHeight;
  BackBuffer.Canvas.Font.Color:= FFont[FontIdx].FontColor;
   }

  BackBuffer.Canvas.Font.Assign(FFont[FontIdx].Font);

  if Transparent then BackBuffer.Canvas.Brush.Style:= bsClear
                 else BackBuffer.Canvas.Brush.Color:= FFont[FontIdx].BGColor;

  if FFont[FontIdx].Vertical then
  begin
    H:= BackBuffer.Canvas.TextWidth(Text);
    W:= BackBuffer.Canvas.TextHeight(Text);
    case Align of
      0: ;
      1: begin
           X:= X - W div 2;
           Y:= Y + H div 2;
         end;
      2: Y:= Y + H div 2;
      3: begin
           X:= X - W;
           Y:= Y + H div 2;
         end;
      4: X:= X - W div 2;
      5: begin
           X:= X - W div 2;
           Y:= Y + H;
         end;
    end;
  end
  else
  begin
    W:= BackBuffer.Canvas.TextWidth(Text);
    H:= BackBuffer.Canvas.TextHeight(Text);
    case Align of
      0: ;
      1: begin
           X:= X - W div 2;
           Y:= Y - H div 2;
         end;
      2: Y:= Y - H div 2;
      3: begin
           X:= X - W;
           Y:= Y - H div 2;
         end;
      4: X:= X - W div 2;
      5: begin
           X:= X - W div 2;
           Y:= Y - H;
         end;
    end;
  end;

  DrawClippedText_(BackBuffer.Canvas, X, Y, ClippRect, Text);
  FLastTextOutX:= X + BackBuffer.Canvas.TextWidth(Text);
end;


procedure TCanvasDrawControl.TextOut(FontIdx, X, Y, Align: Integer; Text: string; Transparent: Boolean);
var
  I: Integer;            //      0 -------- 4 ----------
  AX: Integer;           //      :                     :
  AY: Integer;           //      :                     :
  Rt1: TRect;            //      2          1          3
  Rt2: TRect;            //      :                     :
  W: Integer;            //      :                     :
  H: Integer;            //      ---------- 5 ----------

begin
  if (not NowInit) or (FontIdx > High(FFont)) then Exit;
  {
  BackBuffer.Canvas.Font.Name:= FFont[FontIdx].FontName;
  BackBuffer.Canvas.Font.Height:= FFont[FontIdx].FontHeight;
  BackBuffer.Canvas.Font.Color:= FFont[FontIdx].FontColor;
   }

  BackBuffer.Canvas.Font.Assign(FFont[FontIdx].Font);

  if Transparent then BackBuffer.Canvas.Brush.Style:= bsClear
                 else BackBuffer.Canvas.Brush.Color:= FFont[FontIdx].BGColor;

  if FFont[FontIdx].Vertical then
  begin
    H:= BackBuffer.Canvas.TextWidth(Text);
    W:= BackBuffer.Canvas.TextHeight(Text);
    case Align of
      0: ;
      1: begin
           X:= X - W div 2;
           Y:= Y + H div 2;
         end;
      2: Y:= Y + H div 2;
      3: begin
           X:= X - W;
           Y:= Y + H div 2;
         end;
      4: X:= X - W div 2;
      5: begin
           X:= X - W div 2;
           Y:= Y + H;
         end;
    end;
  end
  else
  begin
    W:= BackBuffer.Canvas.TextWidth(Text);
    H:= BackBuffer.Canvas.TextHeight(Text);
    case Align of
      0: ;
      1: begin
           X:= X - W div 2;
           Y:= Y - H div 2;
         end;
      2: Y:= Y - H div 2;
      3: begin
           X:= X - W;
           Y:= Y - H div 2;
         end;
      4: X:= X - W div 2;
      5: begin
           X:= X - W div 2;
           Y:= Y - H;
         end;
    end;
  end;

  BackBuffer.Canvas.TextOut(X, Y, Text);
  FLastTextOutX:= X + BackBuffer.Canvas.TextWidth(Text);
end;

procedure TCanvasDrawControl.GhostTextOut(FontIdx, X, Y, Align: Integer; Text: string; BGCoolor: DWord);
const
  GhostList: array [1..7] of TPoint = ((X: - 1; Y:   0),
                                       (X: - 1; Y: - 1),
                                       (X:   0; Y: - 1),
                                       (X: + 1; Y: - 1),
                                       (X: + 1; Y:   0),
                                       (X: + 1; Y: + 1),
                                       (X:   0; Y: + 1));

var
  I: Integer;            //      0 -------- 4 ----------
  AX: Integer;           //      :                     :
  AY: Integer;           //      :                     :
  Rt1: TRect;            //      2          1          3
  Rt2: TRect;            //      :                     :
  W: Integer;            //      :                     :
  H: Integer;            //      ---------- 5 ----------

begin
  if (not NowInit) or (FontIdx > High(FFont)) then Exit;
  {
  BackBuffer.Canvas.Font.Name:= FFont[FontIdx].FontName;
  BackBuffer.Canvas.Font.Height:= FFont[FontIdx].FontHeight;
  BackBuffer.Canvas.Font.Color:= FFont[FontIdx].FontColor;
   }

  BackBuffer.Canvas.Font.Assign(FFont[FontIdx].Font);

  BackBuffer.Canvas.Brush.Style:= bsClear;

  if FFont[FontIdx].Vertical then
  begin
    H:= BackBuffer.Canvas.TextWidth(Text);
    W:= BackBuffer.Canvas.TextHeight(Text);
    case Align of
      0: ;
      1: begin
           X:= X - W div 2;
           Y:= Y + H div 2;
         end;
      2: Y:= Y + H div 2;
      3: begin
           X:= X - W;
           Y:= Y + H div 2;
         end;
      4: X:= X - W div 2;
      5: begin
           X:= X - W div 2;
           Y:= Y + H;
         end;
    end;
  end
  else
  begin
    W:= BackBuffer.Canvas.TextWidth(Text);
    H:= BackBuffer.Canvas.TextHeight(Text);
    case Align of
      0: ;
      1: begin
           X:= X - W div 2;
           Y:= Y - H div 2;
         end;
      2: Y:= Y - H div 2;
      3: begin
           X:= X - W;
           Y:= Y - H div 2;
         end;
      4: X:= X - W div 2;
      5: begin
           X:= X - W div 2;
           Y:= Y - H;
         end;
    end;
  end;

  BackBuffer.Canvas.Font.Color:= BGCoolor;
  for I := 1 to 7 do BackBuffer.Canvas.TextOut(X + GhostList[I].X, Y + GhostList[I].Y, Text);
  BackBuffer.Canvas.Font.Assign(FFont[FontIdx].Font);
  BackBuffer.Canvas.TextOut(X, Y, Text);
  FLastTextOutX:= X + BackBuffer.Canvas.TextWidth(Text);
end;

function TCanvasDrawControl.TextWidth(FontIdx: Integer; Text: string): Integer;
begin
//  BackBuffer.Canvas.Font.Name:= FFont[FontIdx].FontName;
//  BackBuffer.Canvas.Font.Height:= FFont[FontIdx].FontHeight;
  BackBuffer.Canvas.Font.Assign(FFont[FontIdx].Font);

  if FFont[FontIdx].Vertical then Result:= BackBuffer.Canvas.TextHeight(Text)
                             else Result:= BackBuffer.Canvas.TextWidth(Text);
end;

function TCanvasDrawControl.TextHeight(FontIdx: Integer; Text: string): Integer;
begin
//  BackBuffer.Canvas.Font.Name:= FFont[FontIdx].FontName;
//  BackBuffer.Canvas.Font.Height:= FFont[FontIdx].FontHeight;
  BackBuffer.Canvas.Font.Assign(FFont[FontIdx].Font);

  if FFont[FontIdx].Vertical then Result:= BackBuffer.Canvas.TextWidth(Text)
                             else Result:= BackBuffer.Canvas.TextHeight(Text);

//  Result:= BackBuffer.Canvas.TextHeight(Text);
end;

function TCanvasDrawControl.LoadFont(FontName: string; FontHeight: Integer; BoldFont: Boolean; FontColor, BGColor: TColor; Vertical: Boolean = False; CreateSurface: Boolean = True): Integer;
var
  FontIdx: Integer;
  LFont: TLogFont;
//  hNewFont: HFont;

begin
  if not NowInit then
  begin
    Result:= - 1;
    Exit;
  end;

  SetLength(FFont, Length(FFont) + 1);
  FontIdx:= High(FFont);

  FFont[FontIdx].FontName:= FontName;
  FFont[FontIdx].FontHeight:= FontHeight;
  FFont[FontIdx].FontColor:= FontColor;
  FFont[FontIdx].BGColor:= BGColor;
  FFont[FontIdx].Vertical:= Vertical;
  FFont[FontIdx].Bold:= BoldFont;
//  FFont[FontIdx].Font.
  FFont[FontIdx].Font:= TFont.Create;
  FFont[FontIdx].Font.Name:= FontName;
  FFont[FontIdx].Font.Height:= FontHeight;
  FFont[FontIdx].Font.Color:= FontColor;
  if BoldFont then FFont[FontIdx].Font.Style:= FFont[FontIdx].Font.Style + [fsBold]
              else FFont[FontIdx].Font.Style:= FFont[FontIdx].Font.Style - [fsBold];

  if Vertical then
  begin

    GetObject(FFont[FontIdx].Font.Handle, SizeOf(LFont), Addr(LFont));
    LFont.lfEscapement := 900;
//    DeleteObject(FFont[FontIdx].Font.Handle);
    FFont[FontIdx].Font.Handle:= CreateFontIndirect(LFont);
//    SelectObject(FFont[FontIdx].Font.Handle, hNewFont);
//    GetObject(FFont[FontIdx].Font.Handle, SizeOf(LFont), Addr(LFont));
//    DeleteObject(hNewFont);
  end;

  
end;

function TCanvasDrawControl.ChangeFontPar(FontIdx: Integer; FontName: string; FontHeight: Integer; FontColor, BGColor: TColor; CreateSurface: Boolean = True): Integer;
begin
  FFont[FontIdx].FontName:= FontName;
  FFont[FontIdx].FontHeight:= FontHeight;
  FFont[FontIdx].FontColor:= FontColor;
  FFont[FontIdx].BGColor:= BGColor;
end;

function TCanvasDrawControl.AddPic(NewPic: TBitMap; Transparent: Boolean): Integer;
var
  PicIdx: Integer;

begin
  if not NowInit then
  begin
    Result:= - 1;
    Exit;
  end;

  SetLength(FPic, Length(FPic) + 1);
  PicIdx:= High(FPic);

  FPic[PicIdx].Size.CX:= NewPic.Width;
  FPic[PicIdx].Size.CY:= NewPic.Height;
  FPic[PicIdx].Surf:= TBitMap.Create;
  FPic[PicIdx].Surf.Assign(NewPic);

  Result:= PicIdx;
end;

procedure TCanvasDrawControl.DrawPic(X, Y, Idx: Integer);
begin
  BackBuffer.Canvas.Draw(X, Y, FPic[Idx].Surf);
end;

function TCanvasDrawControl.GetPicSize(Idx: Integer): TSize;
begin
  Result:= FPic[Idx].Size;
end;

//procedure Register;
//begin
//  RegisterComponents('Samples', [TDirectDrawControl]);
//end;

end.
