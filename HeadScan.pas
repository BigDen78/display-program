{$I DEF.INC}

unit HeadScan; {Language 13}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ImgList, ToolWin, ExtCtrls, Menus, Grids, StdCtrls,
  TeEngine, Series, TeeProcs, Chart, Math, MyTypes, LanguageUnit, InfoBarUnit, AviconDataSource, AviconTypes;

type
  THeadScanForm = class(TForm)
    Button3: TButton;
    Button4: TButton;
    Panel1: TPanel;
    HeadInfoPaintBox: TPaintBox;
    ScrollBox1: TScrollBox;
    PaintBox1: TPaintBox;
    procedure PaintBoxPaint(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure Panel1Resize(Sender: TObject);
    procedure PaintBox2Paint(Sender: TObject);
    procedure ScrollBox1Resize(Sender: TObject);
  private
    FDataOK: Boolean;
    Zoom: Single;
    HSData: THSListItem;
    HeadSData: PHeadScanListItem;
    DataExists: Boolean;
    MouseX, MouseY: Integer;
    InfoRectFlag: Boolean;
    InfoRect: TRect;
    InfoRectDrawFlag: Boolean;
    FInfoBarForm: TInfoBarForm;
    SavePage: Integer;
    HeadInfoBuffer: TBitMap;
    Buffer2: TBitMap;
    FN: string;
    Idx: Integer;
    FDS: TAvk11DatSrc;

//    procedure WMEraseBkgnd(var message: TWMEraseBkgnd); message WM_ERASEBKGND;
  public
    AmplThTrackBarPosition: Integer;
    FormMode: Integer;

    function MinDelay: Integer;
    function MaxDelay: Integer;
    procedure SetFocus_;
    procedure OnChangeLanguage(Sender: TObject);
    procedure SetHScanData(Data_: PHeadScanListItem; InfoBarForm: TInfoBarForm; FN: string; DS_: TAvk11DatSrc; Idx_: Integer = - 1);
    procedure FillInfoBar;
    procedure Refresh;
    procedure CreateImage1;
    procedure CreateImage2;
    procedure ZoomIn;
    procedure ZoomOut;
    property DatSrc: TAvk11DatSrc read FDS write FDS;
  end;

implementation

uses
  MainUnit, ConfigUnit;

var
    NeedClear: Boolean;

{$R *.DFM}

function THeadScanForm.MinDelay: Integer;
begin
  if not Config.InvertDelayAxis then
  begin
    if HSData.Header.HandChNum in [0, 1] then Result:= 0
                                         else Result:= 0;
  end
  else
  begin
    if HSData.Header.HandChNum in [0, 1] then Result:= Round(255 / 3)
                                         else Result:= 255;
  end;
end;

function THeadScanForm.MaxDelay: Integer;
begin
  if not Config.InvertDelayAxis then
  begin
    if HSData.Header.HandChNum in [0, 1] then MaxDelay:= Round(255 / 3)
                                         else MaxDelay:= 255;
  end
  else
  begin
    if HSData.Header.HandChNum in [0, 1] then MaxDelay:= 0
                                         else MaxDelay:= 0;
  end;
end;

procedure THeadScanForm.SetHScanData(Data_: PHeadScanListItem; InfoBarForm: TInfoBarForm; FN: string; DS_: TAvk11DatSrc; Idx_: Integer = - 1);
var
  I, J: Integer;

begin
  FDS:= DS_;
  DataExists:= True;
  FDataOK:= True;
  Self.FN:= FN;
  Idx:= - 1;
  Zoom:= 1;
//  HSData:= Data;
  HeadSData:= Data_;

//  Image1.Picture.Assign(Data.Cross_Image);
//  Image2.Picture.Assign(Data.Vertically_Image);
//  Image3.Picture.Assign(Data.Horizontal_Image);
  FormMode:= 2;
//  Left:= 25;
//  Top:= 25;
//  Width:= 976;
//  Height:= 532;

  FInfoBarForm:= InfoBarForm;
  FillInfoBar;
  FormResize(nil);

  Panel1.Visible:= True;
  OnMouseMove:= nil;
  OnMouseDown:= nil;
  OnMouseUp:= nil;
//  OnPaint:= nil;
//  OnResize:= nil;
end;

procedure THeadScanForm.FillInfoBar;
begin
  if not DataExists then Exit;
  with HSData do
  begin
//    case Header.Rail of
//      rLeft: FInfoBarForm.HSParam.Cells[1, 0]:= LangTable.Caption['common:traLeft']; // '�����';
//      rRight: FInfoBarForm.HSParam.Cells[1, 0]:= LangTable.Caption['Misc:Right']; // '������';
//    end;
//    FInfoBarForm.HSParam.Cells[1, 1]:= IntToStr(Header.HandChNum);
//    FInfoBarForm.HSParam.Cells[1, 2]:= IntToStr(Header.ScanSurface);
//    FInfoBarForm.HSParam.Cells[1, 3]:= IntToStr(Header.Att);
//    FInfoBarForm.HSParam.Cells[1, 4]:= IntToStr(Header.Ku);
//    FInfoBarForm.HSParam.Cells[1, 5]:= IntToStr(Header.TVG);
  end;
end;

procedure THeadScanForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  HeadInfoBuffer.Free;
  HeadInfoBuffer:= nil;
  Buffer2.Free;
  Buffer2:= nil;
  Self.Release;
end;

procedure THeadScanForm.PaintBoxPaint(Sender: TObject);
begin
   if Assigned(HeadInfoBuffer) then HeadInfoPaintBox.Canvas.Draw(0, 0, HeadInfoBuffer);
   if Assigned(Buffer2) then PaintBox1.Canvas.Draw(0, 0, Buffer2);
end;

procedure THeadScanForm.Panel1Resize(Sender: TObject);
begin
  PaintBox1Paint(nil);
end;

procedure THeadScanForm.FormActivate(Sender: TObject);
begin
  MainForm.DisplayModeToControl({nil} Self);
  FillInfoBar;
end;

procedure THeadScanForm.Refresh;
begin
  if Assigned(OnPaint) then PaintBoxPaint(nil);
end;

procedure THeadScanForm.ZoomIn;
begin
end;

procedure THeadScanForm.ZoomOut;
begin
end;

procedure THeadScanForm.StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  S1, S2: string;
begin
  with (Sender as TStringGrid), Canvas do
  begin
    Brush.Style:= bsSolid;
    FillRect(Rect);
    Brush.Style:= bsClear;
    if TextWidth(Cells[ACol, ARow]) < (Rect.Right - Rect.Left) then
      TextOut((Rect.Left + Rect.Right - TextWidth(Cells[ACol, ARow])) div 2,
              (Rect.Top + Rect.Bottom - TextHeight(Cells[ACol, ARow])) div 2,
              Cells[ACol, ARow]) else
    begin
      S1:= Copy(Cells[ACol, ARow], 1, Pos(' ', Cells[ACol, ARow]) - 1);
      S2:= Copy(Cells[ACol, ARow], Pos(' ', Cells[ACol, ARow]) + 1, MaxInt);
      TextOut((Rect.Left + Rect.Right - TextWidth(S1)) div 2,
              Rect.Top + (Rect.Bottom - Rect.Top) div 4 - TextHeight(S1) div 2 - 2, S1);
      TextOut((Rect.Left + Rect.Right - TextWidth(S2)) div 2,
              Rect.Top + 3*(Rect.Bottom - Rect.Top) div 4 - TextHeight(S2) div 2 - 2, S2);
    end;
  end;
end;

procedure THeadScanForm.CreateImage2;
var
  Step, I, J, X, Y: Integer;
  OutWidth: Integer;
  OutRect: TRect;
  S: string;
  Cross_Rect: TRect;
  Horizontal_Rect: TRect;
  Vertically_Rect: TRect;
  ZeroProbe_Rect: TRect;
  X0, Y0: Integer;
  SrcWidth: Integer;
  SrcHeight: Integer;
  DstWidth: Integer;
  DstHeight: Integer;
  Koef: Single;

  Cross_Width: Integer;
  Cross_Height: Integer;
  Horizontal_Width: Integer;
  Horizontal_Height: Integer;
  Vertically_Width: Integer;
  Vertically_Height: Integer;
  Space: Integer;

procedure MoveRect(var Rt: TRect; Delta: TPoint);
begin
  Rt:= Rect(Rt.Left + Delta.X, Rt.Top + Delta.Y, Rt.Right + Delta.X, Rt.Bottom + Delta.Y);
end;

function RectWidth(Rt: TRect): Integer;
begin
  Result:= Rt.Right - Rt.Left;
end;

function RectHeight(Rt: TRect): Integer;
begin
  Result:= Rt.Bottom - Rt.Top;
end;

procedure TextOut_(Rt: TRect; Text: string);
begin
  if Assigned(Buffer2) then
    with Buffer2, Canvas do TextOut(Rt.Left + (RectWidth(Rt) - TextWidth(Text)) div 2, Rt.Top - Round(TextHeight(Text) * 1.5), Text);

end;


begin
 { if Assigned(HeadSData) and
     Assigned(HeadSData.Cross_Image) and
     Assigned(HeadSData.Horizontal_Image) and
     Assigned(HeadSData.Vertically_Image) then
                                                }
    if Assigned(Buffer2) then
      with Buffer2, Canvas do
      begin
        X0:= 0;
        Y0:= 0;
        Space:= 10;
        SrcWidth:= 206 + 413 + Space * 2;
        SrcHeight:= 164 + 206 + 60 + Space * 2;
        DstWidth:= Buffer2.Width - X0;
        DstHeight:= Buffer2.Height - Y0;
        Cross_Width:= 206;
        Cross_Height:= 164;
        Vertically_Width:= 413;
        Vertically_Height:= 164;
        Horizontal_Width:= 413;
        Horizontal_Height:= 206;

        Brush.Color:= 239 or (235 shl 8) or (231 shl 16);
        FillRect(Rect(0, 0, Width, Height));

        Cross_Rect:= Rect(0, 0, 0, 0);
        Vertically_Rect:= Rect(0, 0, 0, 0);
        Horizontal_Rect:= Rect(0, 0, 0, 0);
        ZeroProbe_Rect:= Rect(0, 0, 0, 0);


        if Assigned(HeadSData.Cross_Image) then Cross_Rect:= Rect(0, 0, HeadSData.Cross_Image.Width, HeadSData.Cross_Image.Height);
        if Assigned(HeadSData.Vertically_Image) then Vertically_Rect:= Rect(0, 0, HeadSData.Horizontal_Image.Width, HeadSData.Horizontal_Image.Height);
        if Assigned(HeadSData.Horizontal_Image) then Horizontal_Rect:= Rect(0, 0, HeadSData.Vertically_Image.Width, HeadSData.Vertically_Image.Height);
        if Assigned(HeadSData.Zero_Probe_Image) then ZeroProbe_Rect:= Rect(0, 0, HeadSData.Zero_Probe_Image.Width, HeadSData.Zero_Probe_Image.Height);

        MoveRect(Cross_Rect, Point(0, 2 * Abs(Font.Height)));
        MoveRect(Vertically_Rect, Point(0, 2 * Abs(Font.Height)));
        MoveRect(Horizontal_Rect, Point(0, 4 * Abs(Font.Height)));
        MoveRect(ZeroProbe_Rect, Point(0, 4 * Abs(Font.Height)));

        MoveRect(Cross_Rect, Point(X0 + Space, 0));
        MoveRect(Vertically_Rect, Point(X0 + Space + RectWidth(Cross_Rect), 0));
        MoveRect(Horizontal_Rect, Point(X0 + Space + RectWidth(Cross_Rect), Vertically_Rect.Bottom));
        MoveRect(ZeroProbe_Rect, Point(X0 + Space + RectWidth(Cross_Rect) + RectWidth(Horizontal_Rect) - RectWidth(ZeroProbe_Rect), Horizontal_Rect.Bottom));

        X:= (DstWidth - RectWidth(Horizontal_Rect) - Space - RectWidth(Cross_Rect)) div 2;
        Y:= (DstHeight - ZeroProbe_Rect.Bottom) div 2;

        MoveRect(Cross_Rect, Point(X, Y));
        MoveRect(Vertically_Rect, Point(X, Y));
        MoveRect(Horizontal_Rect, Point(X, Y));
        MoveRect(ZeroProbe_Rect, Point(X, Y));

        TextOut_(Cross_Rect, '���������� �������, ��');  //NO_LANGUAGE
        TextOut_(Vertically_Rect, '������������ �������, ��');  //NO_LANGUAGE
        TextOut_(Horizontal_Rect, '�������������� �������, ��');  //NO_LANGUAGE

        try

          if Assigned(HeadSData.Cross_Image) then HeadSData.Cross_Image.Draw(Buffer2.Canvas, Cross_Rect);
          if Assigned(HeadSData.Horizontal_Image) then HeadSData.Horizontal_Image.Draw(Buffer2.Canvas, Vertically_Rect);
          if Assigned(HeadSData.Vertically_Image) then HeadSData.Vertically_Image.Draw(Buffer2.Canvas, Horizontal_Rect);
          if Assigned(HeadSData.Zero_Probe_Image) then HeadSData.Zero_Probe_Image.Draw(Buffer2.Canvas, ZeroProbe_Rect);

        except

        end;
      end;
end;

procedure THeadScanForm.CreateImage1;
var
  Step, I, J, X, Y: Integer;
  OutWidth: Integer;
  OutRect: TRect;
  S: string;
  Cross_Rect: TRect;
  Horizontal_Rect: TRect;
  Vertically_Rect: TRect;
  ZeroProbe_Rect: TRect;
  X0, Y0: Integer;
  SrcWidth: Integer;
  SrcHeight: Integer;
  DstWidth: Integer;
  DstHeight: Integer;
  Koef: Single;

  Cross_Width: Integer;
  Cross_Height: Integer;
  Horizontal_Width: Integer;
  Horizontal_Height: Integer;
  Vertically_Width: Integer;
  Vertically_Height: Integer;
  Space: Integer;

begin
{  if Assigned(HeadSData) and
     Assigned(HeadSData.Cross_Image) and
     Assigned(HeadSData.Horizontal_Image) and
     Assigned(HeadSData.Vertically_Image) then
 }
    if Assigned(HeadInfoBuffer) then
      with HeadInfoBuffer, Canvas do
      begin
        X0:= 330;
        Y0:= 0;
        Space:= 10;
        SrcWidth:= 206 + 413 + Space * 2;
        SrcHeight:= 164 + 206 + 60 + Space * 2;
        DstWidth:= Buffer2.Width - X0;
        DstHeight:= Buffer2.Height - Y0;
        Cross_Width:= 206;
        Cross_Height:= 164;
        Vertically_Width:= 413;
        Vertically_Height:= 164;
        Horizontal_Width:= 413;
        Horizontal_Height:= 206;

        Brush.Color:= clWhite;
        FillRect(Rect(0, 0, Width, Height));

        X:= 5;
        Y:= 5; // Horizontal_Rect.Top;
        Brush.Style:= bsClear;
        if HeadSData.Rail = rLeft then S:= LangTable.Caption['Common:Rail'] + ': ' + LangTable.Caption['Common:Left']
                                  else S:= LangTable.Caption['Common:Rail'] + ': ' + LangTable.Caption['Common:Right'];
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        if HeadSData.WorkSide = 1 then S:= '������� �����: ������� ������� 1'  //NO_LANGUAGE
                                  else S:= '������� �����: ������� ������� 2'; //NO_LANGUAGE
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;


        if HeadSData.ThresholdUnit = 1 then // ������� ��������� ������ ������������ ��������������� �����.: 1 - ���������; 2 - % (�� 0 �� 100)
        begin
          S:= Format('����� ������������ ��������������� �����������: %d ��', [HeadSData.ThresholdValue]);  //NO_LANGUAGE
        end
        else // ������� ��������� ������ ������������ ��������������� �����.: 2 - % (�� 0 �� 100)
        begin
          S:= Format('����� ������������ ��������������� �����������: %d %%', [HeadSData.ThresholdValue]);   //NO_LANGUAGE
        end;
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        S:= Format(LangTable.Caption['HandScan:ScanTime'] + ': %d ���', [HeadSData.OperatingTime]); //NO_LANGUAGE
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        S:= Format('��� ������� %s', [HeaderBigStrToString_(HeadSData.CodeDefect)]); //NO_LANGUAGE
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        S:= Format('�������: %s', [HeaderBigStrToString_(HeadSData.Sizes)]); //NO_LANGUAGE
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;

        S:= Format('�����������: %s', [HeaderBigStrToString_(HeadSData.Comments)]); //NO_LANGUAGE
        TextOut(X, Y, S);
        Y:= Y + TextHeight(S) + 2;
      end;
end;


procedure THeadScanForm.PaintBox1Paint(Sender: TObject);
begin
  CreateImage2;
  PaintBox1.Canvas.Draw(0, 0, Buffer2);
end;

procedure THeadScanForm.PaintBox2Paint(Sender: TObject);
begin
  CreateImage1;
  HeadInfoPaintBox.Canvas.Draw(0, 0, HeadInfoBuffer);
end;

procedure THeadScanForm.FormCreate(Sender: TObject);
begin
  DataExists:= False;
  HeadSData:= nil;
  FDataOK:= False;
  HeadInfoBuffer:= nil;
  Buffer2:= nil;
  NeedClear:= False;
  Repaint;
  OnChangeLanguage(nil);
end;

procedure THeadScanForm.OnChangeLanguage(Sender: TObject);
begin
  Self.Caption:= LangTable.Caption['HandScan:HandScan'] + ': ' + FN;
  if Idx <> - 1 then Self.Caption:= Self.Caption + ' Idx:' + IntToStr(Idx);
end;

procedure THeadScanForm.ScrollBox1Resize(Sender: TObject);
begin
  PaintBox1.Top:= 0;
  PaintBox1.Left:= 0;
  PaintBox1.Width:= Max(800, ScrollBox1.ClientWidth);
  PaintBox1.Height:= Max(600, ScrollBox1.ClientHeight);
end;

procedure THeadScanForm.SetFocus_;
begin
  if Button3.Tag = 0 then
  begin
    Button3.SetFocus;
    Button3.Tag:= 1;
  end
  else
  begin
    Button4.SetFocus;
    Button3.Tag:= 0;
  end;
end;

procedure THeadScanForm.FormResize(Sender: TObject);
begin

//  self.Caption:= IntToStr(self.Width) + ' ' + IntToStr(self.Height);

  OnChangeLanguage(nil);
  if not FDataOK then Exit;

  if Assigned(Buffer2) then Buffer2.FreeImage
                       else Buffer2:= TBitMap.Create;

  Buffer2.Width:= PaintBox1.Width;
  Buffer2.Height:= PaintBox1.Height;
  CreateImage2;
  if Assigned(Buffer2) then PaintBox1.Canvas.Draw(0, 0, Buffer2);


  if Assigned(HeadInfoBuffer) then HeadInfoBuffer.FreeImage
                              else HeadInfoBuffer:= TBitMap.Create;

  HeadInfoBuffer.Width:= HeadInfoPaintBox.Width;
  HeadInfoBuffer.Height:= HeadInfoPaintBox.Height;
  CreateImage1;
  if Assigned(HeadInfoBuffer) then HeadInfoPaintBox.Canvas.Draw(0, 0, HeadInfoBuffer);

end;

end.
