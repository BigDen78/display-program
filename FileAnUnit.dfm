object FileAnForm: TFileAnForm
  Left = 204
  Top = 111
  Caption = 'File Analyzer'
  ClientHeight = 446
  ClientWidth = 588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 238
    Width = 588
    Height = 7
    Cursor = crVSplit
    Align = alTop
    Color = clGreen
    ParentColor = False
  end
  object Chart3: TChart
    Left = 0
    Top = 0
    Width = 588
    Height = 238
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Series6: THorizBarSeries
      Marks.Arrow.Width = 2
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Width = 2
      Marks.Callout.Arrow.Visible = True
      Marks.Frame.Width = 4
      Marks.Style = smsLabelValue
      Marks.Visible = True
      BarStyle = bsCilinder
      Gradient.Direction = gdLeftRight
      XValues.Name = 'Bar'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object Chart1: TChart
    Left = 0
    Top = 245
    Width = 588
    Height = 179
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Legend.Visible = False
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.LabelsFont.Height = -16
    LeftAxis.LabelsFont.Height = -16
    LeftAxis.Title.Font.Height = -16
    View3D = False
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Series1: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 424
    Width = 588
    Height = 22
    Align = alBottom
    TabOrder = 2
    object CheckBox1: TCheckBox
      Left = 4
      Top = 4
      Width = 97
      Height = 16
      Caption = #1059#1079#1083#1099
      TabOrder = 0
      OnClick = CheckBox1Click
    end
  end
end
