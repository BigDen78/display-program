{$I DEF.INC}
unit SaveAs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DisplayUnit, Math, ConfigUnit, MyTypes,
  Avk11ViewUnit, GotoPos, AviconDataSource, AviconTypes;

type
  TSaveAsForm = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Button1: TButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    Edit2: TEdit;
    Edit3: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Button2: TButton;
    Button3: TButton;
    SaveDialog1: TSaveDialog;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    CheckBox1: TCheckBox;
    Edit1: TComboBox;
    procedure Button1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure DisToText;
    procedure OnChangeLanguage(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
//    function SaveCrd(StartDisCoord: Integer): Boolean;
  private
    SelectMode: Integer;
    FViewForm: TAvk11ViewForm;
    FDatSrc: TAvk11DatSrc;
    FDisplay: TAvk11Display;
    StDisCoord: Integer;
    EdDisCoord: Integer;
  public
    procedure SetData(AvkViewForm: TAvk11ViewForm);
  end;

var
  SaveAsForm: TSaveAsForm;

implementation

uses
  LanguageUnit;

{$R *.DFM}

procedure TSaveAsForm.SetData(AvkViewForm: TAvk11ViewForm);
begin
  FViewForm:= AvkViewForm;
  FDatSrc:= AvkViewForm.DatSrc;
  FDisplay:= AvkViewForm.Display;
  StDisCoord:= FDisplay.StartDisCoord;
  EdDisCoord:= FDisplay.EndDisCoord;
  SelectMode:= 1;
  if Edit1.Text = '' then Edit1.Text:= Config.SavePath;

  if ExtractFileName(Config.SavePath) = '' then Edit1.Text:= Edit1.Text + 'noname.a11';
  DisToText;
  RadioButton2Click(RadioButton3);
end;

procedure TSaveAsForm.Button1Click(Sender: TObject);
begin
  SaveDialog1.FileName:= Edit1.Text;
  if SaveDialog1.Execute then Edit1.Text:= SaveDialog1.FileName;
end;

procedure TSaveAsForm.RadioButton2Click(Sender: TObject);
begin
  SelectMode:= (Sender as TRadioButton).Tag;
  SpeedButton1.Enabled:= SelectMode = 2;
  SpeedButton2.Enabled:= SelectMode = 2;
  Label3.Enabled:= SelectMode = 2;
  Label4.Enabled:= SelectMode = 2;
  Edit2.Enabled:= SelectMode = 2;
  Edit3.Enabled:= SelectMode = 2;

  if SelectMode = 1 then
  begin
    StDisCoord:= FDisplay.StartDisCoord;
    EdDisCoord:= FDisplay.EndDisCoord;
    DisToText;
  end;
end;
{
type
  TOutEcho = packed record
    Delay: Byte;
    Ampl: Byte;
  end;

  ToneCrd = array [1..7, 0..7] of TOutEcho;

var
  BScanFile: array of ToneCrd;

function TSaveAsForm.SaveCrd(StartDisCoord: Integer): Boolean;
var
  R: RRail;
  Ch: Integer;
  I: Integer;

begin
  R:= r_Left;
  for Ch:= 1 to 7 do
    for I:= 1 to FDatSrc.CurEcho[R, Ch].Count do
    begin
      BScanFile[FDatSrc.CurDisCoord - StartDisCoord, Ch, I - 1].Delay:= FDatSrc.CurEcho[R, Ch].Delay[I];
      BScanFile[FDatSrc.CurDisCoord - StartDisCoord, Ch, I - 1].Ampl:= FDatSrc.CurEcho[R, Ch].Ampl[I];
    end;
  Result:= True;
end;
}
procedure TSaveAsForm.Button2Click(Sender: TObject);
{
var
  Crd, Ch, I, Len: Integer;
  F: file;
}
begin
{
  Len:= EdDisCoord - StDisCoord + 1;
  SetLength(BScanFile, Len);
  for Crd:= 0 to Len - 1 do
    for Ch:= 1 to 7 do
      for I:= 1 to 8 do BScanFile[Crd, Ch, I - 1].Ampl:= $FF;

  FDatSrc.LoadData(StDisCoord, EdDisCoord, 0, SaveCrd);

  AssignFile(F, 'x:\file.raw');
  ReWrite(F, 1);
  BlockWrite(F, BScanFile[0], Length(BScanFile) * SizeOf(ToneCrd));
  CloseFile(F);
   }

  if ExtractFileExt(Edit1.Text) = '' then Edit1.Text:= Edit1.Text + '.a11';
  Config.SavePath:= Edit1.Text;

  if (not FileExists(Edit1.Text)) or
     (FileExists(Edit1.Text) and (MessageBox(Handle, PChar(LangTable.Caption['USB:Filealreadyexist!'] + #10 + SaveDialog1.FileName + #10 + LangTable.Caption['USB:Rewrite?']), PChar(LangTable.Caption['Common:Attention']), 36) = IDYES)) then
  begin
//    FDatSrc.SavePiece(StDisCoord, EdDisCoord, CheckBox1.Checked, Edit1.Text);
    Config.AddToSaveAsHistory(Edit1.Text);
    Self.ModalResult:= mrOk;
  end;
end;

procedure TSaveAsForm.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if Msg.CharCode in [13, 27] then Self.Close;
end;

procedure TSaveAsForm.SpeedButton1Click(Sender: TObject);
begin
  with TGotoForm.Create(nil) do
  begin
    SetData(FViewForm);
    if ShowModal = mrOk then
    begin
      StDisCoord:= ResDisCoord;
      DisToText;
    end;
    Free;
  end;
end;

procedure TSaveAsForm.SpeedButton2Click(Sender: TObject);
begin
  with TGotoForm.Create(nil) do
  begin
    SetData(FViewForm);
    if ShowModal = mrOk then
    begin
      EdDisCoord:= ResDisCoord;
      DisToText;
    end;
    Free;
  end;
end;

procedure TSaveAsForm.DisToText;
//var
//  R1, R2: TRealCoord;

begin
//  R1:= FDatSrc.DisToRealCoord(StDisCoord);
//  R2:= FDatSrc.DisToRealCoord(EdDisCoord);
end;

procedure TSaveAsForm.OnChangeLanguage(Sender: TObject);
begin
  Self.Caption                := LanguageUnit.LangTable.Caption['SaveFile:Saveas'];
  Button1.Caption             := LanguageUnit.LangTable.Caption['SaveFile:Browse'];
  GroupBox2.Caption           := LanguageUnit.LangTable.Caption['SaveFile:FileName'];   
  GroupBox1.Caption           := LanguageUnit.LangTable.Caption['SaveFile:SavePart'];   
  RadioButton3.Caption        := LanguageUnit.LangTable.Caption['SaveFile:OnScreen'];   
  RadioButton2.Caption        := LanguageUnit.LangTable.Caption['SaveFile:Selectpart'];   
  Label3.Caption              := LanguageUnit.LangTable.Caption['SaveFile:from'];   
  Label4.Caption              := LanguageUnit.LangTable.Caption['SaveFile:to'];   
  CheckBox1.Caption           := LanguageUnit.LangTable.Caption['SaveFile:Savenotebookrecords'];   
  Button2.Caption             := LanguageUnit.LangTable.Caption['SaveFile:Save'];   
  Button3.Caption             := LanguageUnit.LangTable.Caption['SaveFile:Cancel'];

{

=��������� ���...=Save as...
0170002=�����...=Browse...
0170003=��� �����=File Name
0170004= ����������� ������� = Save Part
0170005=�� ��� �� ������=On Screen
0170006=��������� �����=Select part
0170007=�=from
0170008=��=to
0170009=��������� ������� ��������=Save notebook records
0170010=C��������=Save
0170011=������=Cancel

 }

end;

procedure TSaveAsForm.FormCreate(Sender: TObject);
begin
  OnChangeLanguage(Sender);
  Edit1.Items.Assign(Config.SaveAsHistory);
end;

procedure TSaveAsForm.FormShow(Sender: TObject);
begin
  Button2.SetFocus;
end;

end.
