object UCSForm: TUCSForm
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = #1053#1077#1087#1088#1086#1082#1086#1085#1090#1088#1086#1083#1080#1088#1086#1074#1072#1085#1085#1099#1077' '#1091#1095#1072#1089#1090#1082#1080
  ClientHeight = 400
  ClientWidth = 1297
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox1: TListBox
    Left = 0
    Top = 35
    Width = 97
    Height = 334
    Align = alLeft
    ItemHeight = 13
    TabOrder = 0
    Visible = False
    OnClick = ListBox1Click
  end
  object Panel1: TPanel
    Left = 0
    Top = 369
    Width = 1297
    Height = 31
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 1214
      Top = 1
      Width = 82
      Height = 29
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 6
        Top = 2
        Width = 73
        Height = 25
        Caption = #1047#1072#1082#1088#1099#1090#1100
        TabOrder = 0
        OnClick = Button1Click
      end
    end
  end
  object Panel3: TPanel
    Left = 571
    Top = 35
    Width = 726
    Height = 334
    Align = alRight
    TabOrder = 2
    object Chart1: TChart
      Left = 1
      Top = 1
      Width = 287
      Height = 332
      Legend.Visible = False
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      View3D = False
      Align = alClient
      TabOrder = 0
      object Series1: TLineSeries
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.Visible = False
        LinePen.Width = 3
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
    end
    object Memo1: TMemo
      Left = 288
      Top = 1
      Width = 437
      Height = 332
      Align = alRight
      ScrollBars = ssVertical
      TabOrder = 1
    end
  end
  object Panel4__: TPanel
    Left = 0
    Top = 0
    Width = 1297
    Height = 35
    Align = alTop
    Alignment = taLeftJustify
    TabOrder = 3
    ExplicitTop = -5
    object Panel4: TPanel
      Left = 6
      Top = 1
      Width = 1290
      Height = 33
      Align = alClient
      Alignment = taLeftJustify
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 12
      ExplicitTop = -3
    end
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 5
      Height = 33
      Align = alLeft
      Alignment = taLeftJustify
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  object ListView1: TListView
    Left = 97
    Top = 35
    Width = 474
    Height = 334
    Align = alClient
    Columns = <
      item
        Caption = #8470
      end
      item
        Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
        Width = 145
      end
      item
        Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1072' '#1082#1086#1085#1094#1072
        Width = 145
      end
      item
        Caption = #1055#1088#1086#1090#1103#1078#1077#1085#1086#1089#1090#1100
        Width = 95
      end>
    ColumnClick = False
    RowSelect = True
    TabOrder = 4
    ViewStyle = vsReport
    OnSelectItem = ListView1SelectItem
  end
end
