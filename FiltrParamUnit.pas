unit FiltrParamUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, MyTypes, TeEngine, Series, TeeProcs,
  Chart, Buttons;

type
  TFiltrParamForm = class(TForm)
    ParamGrid: TStringGrid;
    Button1: TButton;
    eChLen: TLabeledEdit;
    GroupBox1: TGroupBox;
    eWindowsWidth: TLabeledEdit;
    eCrdWinCount: TLabeledEdit;
    GroupBox2: TGroupBox;
    eAmplStep: TLabeledEdit;
    eMinAmpl: TLabeledEdit;
    eMaxAmpl: TLabeledEdit;
    eAmplWinCount: TLabeledEdit;
    GroupBox3: TGroupBox;
    DelayGrid: TStringGrid;
    eDelayWinCount: TLabeledEdit;
    GroupBox5: TGroupBox;
    ParamList: TComboBox;
    Button2: TButton;
    ReCalc: TButton;
    Chart1: TChart;
    Panel1: TPanel;
    eRail: TLabeledEdit;
    eChannel: TLabeledEdit;
    eCoord: TLabeledEdit;
    bCalcGraph: TButton;
    eAmpl: TLabeledEdit;
    bExpand: TSpeedButton;
    lError: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ParamGridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure ParamListClick(Sender: TObject);
    procedure ParamChange(Sender: TObject);
    procedure ParamGridSetEditText(Sender: TObject; ACol, ARow: Integer; const Value: String);
    procedure Button2Click(Sender: TObject);
    procedure ReCalcClick(Sender: TObject);
    procedure bCalcGraphClick(Sender: TObject);
    procedure bExpandClick(Sender: TObject);
  private
    procedure CtrlToData;
    procedure DataToCtrl;
    procedure RefreshDisplay;
    function GetParamList(ParamList: TComboBox): Integer;
  public
    FSkipFlag: Boolean;
    FPCalcPar_: PCalcFiltrParams;
    FCalcPar_Ed: TCalcFiltrParams;
    FPThPar: PThFiltrParams;
    FDisp: Pointer;
    FFiltr: Pointer;
    FSelectedParam: Integer;
    FSeries: array of TLineSeries;
  end;

procedure ShowFiltrParamForm(Disp, Filtr: Pointer);
procedure CloseFiltrParamForm;

procedure LoadParamFromFile(FileName: string; PThPar: PThFiltrParams; PCalcPar: PCalcFiltrParams);
procedure SaveParamToFile(FileName: string; PThPar: PThFiltrParams; PCalcPar: PCalcFiltrParams);


implementation

uses
  DisplayUnit, FiltrUnit;

var
  FiltrParamForm: TFiltrParamForm = nil;

{$R *.dfm}

////////////////////////////////////////////////////////////////////////////////

procedure ShowFiltrParamForm(Disp, Filtr: Pointer);
begin
  if not Assigned(FiltrParamForm) then FiltrParamForm:= TFiltrParamForm.Create(nil);
  FiltrParamForm.FPCalcPar_:= TFiltr(Filtr).PCalcParam;
  FiltrParamForm.FCalcPar_Ed:= TFiltr(Filtr).PCalcParam^;
  FiltrParamForm.FPThPar:= TFiltr(Filtr).PThParam;
  FiltrParamForm.FDisp:= Disp;
  FiltrParamForm.FFiltr:= Filtr;
  FiltrParamForm.Visible:= True;
  FiltrParamForm.Show;
end;

procedure CloseFiltrParamForm;
begin
  if Assigned(FiltrParamForm) then FiltrParamForm.Free;
end;

////////////////////////////////////////////////////////////////////////////////

procedure LoadParamFromFile(FileName: string; PThPar: PThFiltrParams; PCalcPar: PCalcFiltrParams);
var
  Row: Integer;
  MS: TStringList;
  StartLine: Integer;

begin
  MS:= TStringList.Create;
  MS.LoadFromFile(FileName);

  PThPar^.Name:= MS.Strings[0];                              // 0
  TryStrToInt(MS.Strings[1], PCalcPar^.CrdWinWidth);         // 1
  TryStrToInt(MS.Strings[2], PThPar^.CrdWinCount);           // 2
  TryStrToInt(MS.Strings[3], PCalcPar^.AmplMin);             // 3
  TryStrToInt(MS.Strings[4], PCalcPar^.AmplMax);             // 4
  TryStrToInt(MS.Strings[5], PCalcPar^.AmplStep);            // 5
  TryStrToInt(MS.Strings[6], PThPar^.AmplWinCount);          // 6
  TryStrToInt(MS.Strings[7], PThPar^.DlyWinCount);           // 7
  TryStrToInt(MS.Strings[8], PCalcPar^.Ch1Len);              // 8

  StartLine:= 16;
  for Row:= 1 to 13 do
  begin
    TryStrToInt(Copy(MS.Strings[StartLine], 1, 10), PCalcPar^.MinDelay[Row]);
    Inc(StartLine);
    TryStrToInt(Copy(MS.Strings[StartLine], 1, 10), PCalcPar^.MaxDelay[Row]);
    Inc(StartLine);
    TryStrToInt(Copy(MS.Strings[StartLine], 1, 10), PCalcPar^.DelayStep[Row]);
    Inc(StartLine);
    TryStrToFloat(Copy(MS.Strings[StartLine], 1, 10), PThPar^.ChParam[Row].CountTh[1]);
    Inc(StartLine);
    TryStrToFloat(Copy(MS.Strings[StartLine], 1, 10), PThPar^.ChParam[Row].CountTh[2]);
    Inc(StartLine);
    TryStrToInt(Copy(MS.Strings[StartLine], 1, 10), PThPar^.ChParam[Row].BHMinDelay);
    Inc(StartLine);
    TryStrToInt(Copy(MS.Strings[StartLine], 1, 10), PThPar^.ChParam[Row].BHMaxDelay);
    Inc(StartLine);
    TryStrToInt(Copy(MS.Strings[StartLine], 1, 10), PThPar^.ChParam[Row].LenTh[1]);
    Inc(StartLine);
    TryStrToInt(Copy(MS.Strings[StartLine], 1, 10), PThPar^.ChParam[Row].LenTh[2]);
    Inc(StartLine);
  end;
end;

procedure SaveParamToFile(FileName: string; PThPar: PThFiltrParams; PCalcPar: PCalcFiltrParams);
var
  Row: Integer;
  MS: TStringList;

begin
  MS:= TStringList.Create;

  MS.Add(PThPar^.Name);                           // 0
  MS.Add(IntToStr(PCalcPar^.CrdWinWidth));        // 1
  MS.Add(IntToStr(PThPar^.CrdWinCount));          // 2
  MS.Add(IntToStr(PCalcPar^.AmplMin));            // 3
  MS.Add(IntToStr(PCalcPar^.AmplMax));            // 4
  MS.Add(IntToStr(PCalcPar^.AmplStep));           // 5
  MS.Add(IntToStr(PThPar^.AmplWinCount));         // 6
  MS.Add(IntToStr(PThPar^.DlyWinCount));          // 7
  MS.Add(IntToStr(PCalcPar^.Ch1Len));             // 8
  MS.Add('---------------------');                // 9
  MS.Add('---------------------');                // 10
  MS.Add('---------------------');                // 11
  MS.Add('---------------------');                // 12
  MS.Add('---------------------');                // 13
  MS.Add('---------------------');                // 14
  MS.Add('---------------------');                // 15

  for Row:= 1 to 7 do
  begin
    MS.Add(IntToStr(PCalcPar^.MinDelay[Row]));
    MS.Add(IntToStr(PCalcPar^.MaxDelay[Row]));
    MS.Add(IntToStr(PCalcPar^.DelayStep[Row]));
    MS.Add(FloatToStr(PThPar^.ChParam[Row].CountTh[1]));
    MS.Add(FloatToStr(PThPar^.ChParam[Row].CountTh[2]));
    MS.Add(IntToStr(PThPar^.ChParam[Row].BHMinDelay));
    MS.Add(IntToStr(PThPar^.ChParam[Row].BHMaxDelay));
    MS.Add(IntToStr(PThPar^.ChParam[Row].LenTh[1]));
    MS.Add(IntToStr(PThPar^.ChParam[Row].LenTh[2]));
  end;
  MS.SaveToFile(FileName);
end;

////////////////////////////////////////////////////////////////////////////////

function TFiltrParamForm.GetParamList(ParamList: TComboBox): Integer;
var
  I: Integer;
  MS: TStringList;
  FThPar: TThFiltrParams;
  FCalcPar: TCalcFiltrParams;

begin
  if FileExists(ExtractFilePath(Application.ExeName) + 'selparam.txt') then
  begin
    MS:= TStringList.Create;
    MS.LoadFromFile(ExtractFilePath(Application.ExeName) + 'selparam.txt');
    TryStrToInt(MS.Strings[0], FSelectedParam);
    MS.Free;
  end;

  ParamList.Clear;
  I:= 1;
  while FileExists(Format(ExtractFilePath(Application.ExeName) + 'params%d.txt', [I])) do
  begin
    LoadParamFromFile(Format(ExtractFilePath(Application.ExeName) + 'params%d.txt', [I]), @FThPar, @FCalcPar);
    ParamList.Items.AddObject(FThPar.Name, Pointer(I));
    Inc(I);
  end;
end;

procedure TFiltrParamForm.CtrlToData;
var
  Row: Integer;
  SaveCalcPar: TCalcFiltrParams;
  Flg: Boolean;

begin
  if FSkipFlag then Exit;

  SaveCalcPar:= FPCalcPar_^;

  FPThPar^.Name:= ParamList.Text;
  Flg:= TryStrToInt(eWindowsWidth.Text, FCalcPar_Ed.CrdWinWidth);
  Flg:= Flg and TryStrToInt(eCrdWinCount.Text, FPThPar^.CrdWinCount);
  Flg:= Flg and TryStrToInt(eMinAmpl.Text, FCalcPar_Ed.AmplMin);
  Flg:= Flg and TryStrToInt(eMaxAmpl.Text, FCalcPar_Ed.AmplMax);
  Flg:= Flg and TryStrToInt(eAmplStep.Text, FCalcPar_Ed.AmplStep);
  Flg:= Flg and TryStrToInt(eAmplWinCount.Text, FPThPar^.AmplWinCount);
  Flg:= Flg and TryStrToInt(eDelayWinCount.Text, FPThPar^.DlyWinCount);
  Flg:= Flg and TryStrToInt(eChLen.Text, FCalcPar_Ed.Ch1Len);

  for Row:= 1 to 7 do
  begin
    Flg:= Flg and TryStrToInt(DelayGrid.Cells[Row, 1], FCalcPar_Ed.MinDelay[Row]);
    Flg:= Flg and TryStrToInt(DelayGrid.Cells[Row, 2], FCalcPar_Ed.MaxDelay[Row]);
    Flg:= Flg and TryStrToInt(DelayGrid.Cells[Row, 3], FCalcPar_Ed.DelayStep[Row]);
  end;

  for Row:= 1 to 7 do
  begin
    Flg:= Flg and TryStrToFloat(ParamGrid.Cells[Row, 1], FPThPar^.ChParam[Row].CountTh[1]);
    Flg:= Flg and TryStrToFloat(ParamGrid.Cells[Row, 2], FPThPar^.ChParam[Row].CountTh[2]);
    Flg:= Flg and TryStrToInt(ParamGrid.Cells[Row, 3], FPThPar^.ChParam[Row].BHMinDelay);
    Flg:= Flg and TryStrToInt(ParamGrid.Cells[Row, 4], FPThPar^.ChParam[Row].BHMaxDelay);
    Flg:= Flg and TryStrToInt(ParamGrid.Cells[Row, 5], FPThPar^.ChParam[Row].LenTh[1]);
    Flg:= Flg and TryStrToInt(ParamGrid.Cells[Row, 6], FPThPar^.ChParam[Row].LenTh[2]);
  end;

  if not CompareMem(@SaveCalcPar, @FCalcPar_Ed, SizeOf(TCalcFiltrParams)) then ReCalc.Enabled:= True; // ��������� ������� ���������
  lError.Visible:= not Flg;    // ������ ��� �����
end;

procedure TFiltrParamForm.DataToCtrl;
var
  Row: Integer;

begin
  FSkipFlag:= True;

  eWindowsWidth.Text  := IntToStr(FCalcPar_Ed.CrdWinWidth);
  eCrdWinCount.Text   := IntToStr(FPThPar^.CrdWinCount);
  eMinAmpl.Text       := IntToStr(FCalcPar_Ed.AmplMin     );
  eMaxAmpl.Text       := IntToStr(FCalcPar_Ed.AmplMax     );
  eAmplStep.Text      := IntToStr(FCalcPar_Ed.AmplStep    );
  eAmplWinCount.Text  := IntToStr(FPThPar^.AmplWinCount);
  eDelayWinCount.Text := IntToStr(FPThPar^.DlyWinCount);
  eChLen.Text         := IntToStr(FCalcPar_Ed.Ch1Len      );

  for Row:= 1 to 7 do
  begin
    DelayGrid.Cells[Row, 1]:= IntToStr(FCalcPar_Ed.MinDelay[Row]          );
    DelayGrid.Cells[Row, 2]:= IntToStr(FCalcPar_Ed.MaxDelay[Row]          );
    DelayGrid.Cells[Row, 3]:= IntToStr(FCalcPar_Ed.DelayStep[Row]         );
  end;

  for Row:= 1 to 7 do
  begin
    ParamGrid.Cells[Row, 1]:= FloatToStr(FPThPar^.ChParam[Row].CountTh[1]);
    ParamGrid.Cells[Row, 2]:= FloatToStr(FPThPar^.ChParam[Row].CountTh[2]);
    ParamGrid.Cells[Row, 3]:= IntToStr(FPThPar^.ChParam[Row].BHMinDelay  );
    ParamGrid.Cells[Row, 4]:= IntToStr(FPThPar^.ChParam[Row].BHMaxDelay  );
    ParamGrid.Cells[Row, 5]:= IntToStr(FPThPar^.ChParam[Row].LenTh[1]  );
    ParamGrid.Cells[Row, 6]:= IntToStr(FPThPar^.ChParam[Row].LenTh[2]  );
  end;
  FSkipFlag:= False;
end;

procedure TFiltrParamForm.FormCreate(Sender: TObject);
var
  Ch: Integer;

begin
  New(FPThPar);
//  New(FPCalcPar);

  FDisp:= nil;
  GetParamList(ParamList);
  ParamList.ItemIndex:= FSelectedParam - 1;
  ParamListClick(nil);

  for Ch:= 1 to 7 do ParamGrid.Cells[Ch, 0]:= IntToStr(Ch);
  ParamGrid.ColWidths[0]:= 165;
  ParamGrid.Cells[0, 0]:= '�����';
  ParamGrid.Cells[0, 1]:= '����� �1 ��� ���� (��./�)';
  ParamGrid.Cells[0, 2]:= '����� �1 � ���� (��./�)';
  ParamGrid.Cells[0, 3]:= '���� �� ������� (�.�.)';
  ParamGrid.Cells[0, 4]:= '���� �� �������� (�.�.)';
  ParamGrid.Cells[0, 5]:= '����� �2 ��� ���� (��./�)';
  ParamGrid.Cells[0, 6]:= '����� �2 � ���� (��./�)';

  for Ch:= 1 to 7 do DelayGrid.Cells[Ch, 0]:= IntToStr(Ch);
  DelayGrid.ColWidths[0]:= 165;
  DelayGrid.Cells[0, 0]:= '�����';
  DelayGrid.Cells[0, 1]:= '�������� ����������� (�.�.)';
  DelayGrid.Cells[0, 2]:= '�������� ������������ (�.�.)';
  DelayGrid.Cells[0, 3]:= '��� �� ��������';

  Self.Width:= bExpand.Left + bExpand.Width + 10;
  Self.Left:= Screen.Width - Self.Width;
  Self.Top:= Screen.Height - Self.Height;
  lError.Visible:= False;
end;

procedure TFiltrParamForm.Button1Click(Sender: TObject);
begin
  Self.Close;
//  ModalResult:= mrOk;
end;

procedure TFiltrParamForm.ParamGridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  if // ((ARow in [2, 3]) and (ACol in [1..7])) or
     ((ARow in [2, 3, 4]) and (ACol in [2..5])) or
     ((ARow in [5, 6]) and (ACol in [2..7])) then
  begin
    ParamGrid.Canvas.Brush.Color:= clSilver;
    ParamGrid.Canvas.FillRect(Rect);
  end;
end;

procedure TFiltrParamForm.ParamListClick(Sender: TObject);
var
//  Idx: Integer;
  MS: TStringList;

begin
  if ParamList.ItemIndex = - 1 then Exit;

  FSkipFlag:= True;
  FSelectedParam:= Integer(ParamList.Items.Objects[ParamList.ItemIndex]);

  if FileExists(Format(ExtractFilePath(Application.ExeName) + 'params%d.txt', [FSelectedParam])) then
  begin
    LoadParamFromFile(Format(ExtractFilePath(Application.ExeName) + 'params%d.txt', [FSelectedParam]), FPThPar, @FCalcPar_Ed);
    SaveParamToFile(ExtractFilePath(Application.ExeName) + 'default.txt', FPThPar, @FCalcPar_Ed);
    DataToCtrl;
    RefreshDisplay;
  end;

  MS:= TStringList.Create;
  MS.Add(IntToStr(FSelectedParam));
  MS.SaveToFile(ExtractFilePath(Application.ExeName) + 'selparam.txt');
  MS.Free;
  FSkipFlag:= False;
end;

procedure TFiltrParamForm.RefreshDisplay;
begin
  if Assigned(FDisp) then TAvk11Display(FDisp).Refresh;
end;

procedure TFiltrParamForm.ParamChange(Sender: TObject);
var
  I: Integer;
//  SaveCalcPar: PCalcFiltrParams;
//  SaveThPar: PThFiltrParams;

begin
  if FSkipFlag then Exit;

//  SaveCalcPar:= FPCalcPar;
//  SaveThPar  := FPThPar;

  CtrlToData;
//  if not CompareMem(SaveCalcPar, FPCalcPar, SizeOf(TCalcFiltrParams)) then ReCalc.Enabled:= True;
//  SaveThPar  := FPThPar;


  I:= ParamList.ItemIndex;
//  ParamList.Items.Strings[FSelectedParam - 1]:= eParamName.Text;
  ParamList.Items.Strings[FSelectedParam - 1]:= ParamList.Text;
  ParamList.ItemIndex:= I;
  SaveParamToFile(Format(ExtractFilePath(Application.ExeName) + 'params%d.txt', [FSelectedParam]), FPThPar, @FCalcPar_Ed);
  SaveParamToFile(ExtractFilePath(Application.ExeName) + 'default.txt', FPThPar, @FCalcPar_Ed);
  RefreshDisplay;
  bCalcGraphClick(nil);
end;

procedure TFiltrParamForm.ParamGridSetEditText(Sender: TObject; ACol, ARow: Integer; const Value: string);
{
var
  SaveCalcPar: TCalcFiltrParams;
  SaveThPar: TThFiltrParams;
}
begin
 {
  SaveCalcPar:= FPCalcPar^;
  SaveThPar  := FPThPar^;
  }
  CtrlToData;

//  if not CompareMem(@SaveCalcPar, FPCalcPar, SizeOf(TCalcFiltrParams)) then ReCalc.Enabled:= True;

  SaveParamToFile(Format(ExtractFilePath(Application.ExeName) + 'params%d.txt', [FSelectedParam]), FPThPar, @FCalcPar_Ed);
  SaveParamToFile(ExtractFilePath(Application.ExeName) + 'default.txt', FPThPar, @FCalcPar_Ed);
  RefreshDisplay;
end;

procedure TFiltrParamForm.Button2Click(Sender: TObject);
var
  NewIdx: Integer;
  NewThParam: TThFiltrParams;
//  NewCalcParam: TCalcFiltrParams;

begin
  NewThParam:= FPThPar^;
//  NewCalcParam:= FCalcPar_Ed;

  NewThParam.Name:= '����� ���������';
  if InputQuery('��������', '�������, �������� ��������:', NewThParam.Name) then
  begin
    FSelectedParam:= ParamList.Items.Count + 1;
    NewIdx:= FSelectedParam;
    SaveParamToFile(Format(ExtractFilePath(Application.ExeName) + 'params%d.txt', [FSelectedParam]), @NewThParam, @FCalcPar_Ed);
    SaveParamToFile(ExtractFilePath(Application.ExeName) + 'default.txt', @NewThParam, @FCalcPar_Ed);
    GetParamList(ParamList);
    ParamList.ItemIndex:= NewIdx - 1;
  end;
end;

procedure TFiltrParamForm.ReCalcClick(Sender: TObject);
begin
  ReCalc.Enabled:= False;
  FPCalcPar_^:= FCalcPar_Ed;
  TFiltr(FFiltr).ReCalc;
  RefreshDisplay;
  bCalcGraphClick(nil);
end;

procedure TFiltrParamForm.bCalcGraphClick(Sender: TObject);
var
  R: Integer;
  Ch: Integer;
  Crd: Integer;
  Ampl: Integer;
  Idx: Integer;
  A, D: Integer;
  Col: TColor;
  Tmp: Integer;

begin
(*
  if (not TryStrToInt(eCoord.Text, Crd)) or
     (not TryStrToInt(eRail.Text, R)) or
     (not TryStrToInt(eChannel.Text, Ch)) or
     (not TryStrToInt(eAmpl.Text, Ampl)) then Exit;

  if TFiltr(FFiltr).FSummWinLen = 0 then Exit;

  Idx:= TFiltr(FFiltr).StartFilter(Crd);
{
  Series1.Clear;
  A:= Ampl;
    for D:= 0 to High(TFiltr(FFiltr).FFiltr[Idx].Ch27Gis[RRail_(R), Ch, A]) do
      Series1.AddXY(D, TFiltr(FFiltr).GetFiltr27(Idx, RRail_(R), Ch, A, D) / TFiltr(FFiltr).FSummWinLen);
}

  for A:= 0 to High(FSeries) do FSeries[A].Free;


  if Ch <> 1 then
  begin
    SetLength(FSeries, Length(TFiltr(FFiltr).FFiltr[Idx].Ch27Gis[RRail_(R), Ch]));
    for A:= 0 to High(TFiltr(FFiltr).FFiltr[Idx].Ch27Gis[RRail_(R), Ch]) do
    begin
      FSeries[A]:= TLineSeries.Create(Chart1);
      FSeries[A].ParentChart:= Chart1;
      FSeries[A].LinePen.Width:= 2;
      if A = Ampl then Col:= clRed else Col:= clGreen;
      for D:= 0 to High(TFiltr(FFiltr).FFiltr[Idx].Ch27Gis[RRail_(R), Ch, A]) do
        FSeries[A].AddXY(D, TFiltr(FFiltr).GetFiltr27(Idx, RRail_(R), Ch, A, D) / TFiltr(FFiltr).FSummWinLen, '', Col);
    end;
  end
  else
  begin
    SetLength(FSeries, Length(TFiltr(FFiltr).FFiltr[Idx].Ch1Gis[RRail_(R)]));
    for A:= 0 to High(TFiltr(FFiltr).FFiltr[Idx].Ch1Gis[RRail_(R)]) do
    begin
      FSeries[A]:= TLineSeries.Create(Chart1);
      FSeries[A].ParentChart:= Chart1;
      FSeries[A].LinePen.Width:= 2;
      if A = Ampl then Col:= clRed else Col:= clGreen;
      for D:= 0 to High(TFiltr(FFiltr).FFiltr[Idx].Ch1Gis[RRail_(R), A]) do
        FSeries[A].AddXY(D, TFiltr(FFiltr).GetFiltr1ECount(Idx, RRail_(R), A, D) / TFiltr(FFiltr).FSummWinLen, '', Col);
    end;
  end;
  *)
end;

procedure TFiltrParamForm.bExpandClick(Sender: TObject);
begin
  case bExpand.Tag of
    0: begin
         Self.Width:= 793;
         bExpand.Caption:= '<<';
         bExpand.Tag:= 1;
       end;
    1: begin
         Self.Width:= bExpand.Left + bExpand.Width + 10;
         bExpand.Caption:= '>>';
         bExpand.Tag:= 0;
       end;
  end;
  Self.Left:= Screen.Width - Self.Width;
  Self.Top:= Screen.Height - Self.Height;
end;

end.
