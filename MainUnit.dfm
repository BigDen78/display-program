object MainForm: TMainForm
  Left = 50
  Top = 158
  ClientHeight = 557
  ClientWidth = 1173
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIForm
  OldCreateOrder = False
  Position = poDesigned
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnKeyUp = FormKeyUp
  OnResize = FormResize
  OnShortCut = FormShortCut
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object TBDock1: TTBDock
    Left = 0
    Top = 0
    Width = 1173
    Height = 65
    Background = TBBackground1
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      ActivateParent = False
      BorderStyle = bsNone
      Caption = #1043#1083#1072#1074#1085#1086#1077' '#1084#1077#1085#1102
      CloseButton = False
      DockMode = dmCannotFloat
      DockPos = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FullSize = True
      Images = ImageList1
      MenuBar = True
      ParentFont = False
      ProcessShortCuts = True
      ShrinkMode = tbsmWrap
      TabOrder = 0
      object miFileMan: TTBSubmenuItem
        Caption = #1060#1072#1081#1083
        OnClick = miFileManClick
        object miOpenFile: TTBItem
          Caption = #1054#1090#1082#1088#1099#1090#1100'...'
          ImageIndex = 0
          Images = ImageList1
          OnClick = tbOpenClick
        end
        object miRecent: TTBSubmenuItem
          Caption = #1056#1072#1085#1077#1077' '#1086#1090#1082#1088#1099#1074#1072#1074#1096#1080#1077#1089#1103' '#1092#1072#1081#1083#1099
        end
        object miClose: TTBItem
          Caption = #1047#1072#1082#1088#1099#1090#1100
          OnClick = miCloseClick
        end
        object Sep3: TTBSeparatorItem
        end
        object miFileBase: TTBItem
          Caption = #1041#1072#1079#1072' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1086#1074' '#1082#1086#1085#1090#1088#1086#1083#1103
          ImageIndex = 43
          Images = ImageList1
          OnClick = tbBaseClick
        end
        object miSameCoordFiles: TTBSubmenuItem
          Caption = 'Intersect files'
          OnPopup = miSameCoordFilesPopup
        end
        object TBSeparatorItem27: TTBSeparatorItem
        end
        object TBItem30: TTBItem
          Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1086#1088#1084#1072#1090#1077' '#1040#1074#1080#1082#1086#1085'-11...'
        end
        object TBItem31: TTBItem
          Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1086#1088#1084#1072#1090#1077' '#1041#1059#1052'...'
          OnClick = TBItem31Click
        end
        object Sep1: TTBSeparatorItem
        end
        object miFilePar: TTBItem
          Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1092#1072#1081#1083#1077'...'
          ImageIndex = 57
          Images = ImageList1
          ShortCut = 32841
          OnClick = miFileParClick
        end
        object miViewTestRecord: TTBItem
          Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1079#1072#1087#1080#1089#1080' '#1090#1091#1087#1080#1082#1072
          Visible = False
          OnClick = miViewTestRecordClick
        end
        object miOperatorLog: TTBItem
          Caption = #1061#1088#1086#1085#1086#1083#1086#1075#1080#1103' '#1088#1072#1073#1086#1090#1099'...'
          Visible = False
          OnClick = miOperatorLogClick
        end
        object miViewMap: TTBItem
          Caption = #1058#1088#1077#1082' '#1043#1053#1057#1057'...'
          OnClick = miViewMapClick
        end
        object miUncontrolZones: TTBItem
          Caption = #1053#1077#1087#1088#1086#1082#1086#1085#1090#1088#1086#1083#1080#1088#1086#1074#1072#1085#1085#1099#1077' '#1091#1095#1072#1089#1090#1082#1080
          OnClick = miUncontrolZonesClick
        end
        object miSensGraph: TTBItem
          Caption = #1043#1088#1072#1092#1080#1082#1080' '#1095#1091#1074#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086#1089#1090#1080
          Visible = False
          OnClick = miSensGraphClick
        end
        object miViewTestRecordSep: TTBSeparatorItem
          Visible = False
        end
        object TBSeparatorItem20: TTBSeparatorItem
        end
        object TBItem3: TTBItem
          Caption = #1055#1086' '#1082#1080#1083#1086#1084#1077#1090#1088#1072#1084'...'
          Visible = False
          OnClick = TBItem3Click
        end
        object TBSeparatorItem19: TTBSeparatorItem
          Visible = False
        end
        object miIntegrityChecking: TTBItem
          Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1094#1077#1083#1086#1089#1090#1085#1086#1089#1090#1080' '#1092#1072#1081#1083#1072
          ImageIndex = 3
          Images = ImageList1
          Visible = False
          OnClick = miIntegrityCheckingClick
        end
        object miSavePiece: TTBItem
          Caption = #1057#1086#1093#1088#1072#1085#1077#1085#1080#1077' '#1095#1072#1089#1090#1080' '#1076#1077#1092#1077#1082#1090#1086#1075#1088#1072#1084#1084#1099'...'
          Visible = False
          OnClick = miSavePieceClick
        end
        object TBSeparatorItem16: TTBSeparatorItem
        end
        object miNotebook: TTBItem
          Caption = #1041#1083#1086#1082#1085#1086#1090
          ImageIndex = 26
          Images = ImageList1
          OnClick = tbNotebook_Click
        end
        object miNotNewRec: TTBItem
          Caption = #1053#1086#1074#1072#1103' '#1079#1072#1087#1080#1089#1100
          ImageIndex = 16
          Images = ImageList1
          OnClick = tbNewFileRecClick
        end
        object miExportToNORDCO: TTBItem
          Caption = 'Export'
          ImageIndex = 56
          Images = ImageList1
          OnClick = miExportToNORDCOClick
        end
        object Sep4: TTBSeparatorItem
        end
        object miAutoDecodingItem: TTBSubmenuItem
          Caption = #1040#1074#1090#1086#1088#1072#1089#1096#1080#1092#1088#1086#1074#1082#1072
        end
        object TBSeparatorItem28: TTBSeparatorItem
        end
        object miFileDownload: TTBItem
          Caption = #1063#1090#1077#1085#1080#1077' '#1092#1072#1081#1083#1086#1074' '#1089' '#1040#1050#1055' '#1080#1083#1080' '#1041#1059#1048'...'
          ImageIndex = 51
          Images = ImageList1
          Visible = False
          OnClick = miFileDownloadClick
        end
        object miFileDownload2: TTBItem
          Caption = #1063#1090#1077#1085#1080#1077' '#1092#1072#1081#1083#1086#1074' '#1089' Flash '#1076#1080#1089#1082#1072'...'
          ImageIndex = 61
          Images = ImageList1
          OnClick = miFileDownload2Click
        end
        object miFileDownloadList: TTBSubmenuItem
          Caption = #1055#1086#1089#1083#1077#1076#1085#1080#1077' '#1089#1095#1080#1090#1072#1085#1085#1099#1077' '#1092#1072#1081#1083#1099
          Visible = False
          object TBSeparatorItem8: TTBSeparatorItem
          end
          object miClearLastDwdFiles: TTBItem
            Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
            OnClick = miClearLastDwdFilesClick
          end
          object TBSeparatorItem15: TTBSeparatorItem
          end
        end
        object miBUIWork: TTBItem
          Caption = #1056#1072#1073#1086#1090#1072' '#1089' '#1041#1059#1048'...'
          ImageIndex = 50
          Images = ImageList1
          Visible = False
          OnClick = miBUIWorkClick
        end
        object Sep5: TTBSeparatorItem
        end
        object miSettings: TTBItem
          Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1087#1088#1086#1075#1088#1072#1084#1084#1099'...'
          Images = ImageList1
          OnClick = miSettingsClick
        end
        object Sep6: TTBSeparatorItem
        end
        object miExit: TTBItem
          Caption = #1042#1099#1093#1086#1076
          ImageIndex = 42
          Images = ImageList1
          OnClick = miExitClick
        end
      end
      object miViewMan: TTBSubmenuItem
        Caption = #1042#1080#1076
        OnPopup = miViewManPopup
        object miTape: TTBItem
          Tag = 1
          AutoCheck = True
          Caption = #1056#1072#1079#1074#1077#1088#1090#1082#1072' '#1090#1080#1087#1072' "'#1042'" '#1073#1077#1079' '#1089#1074#1077#1076#1077#1085#1080#1103
          Checked = True
          GroupIndex = 3
          Images = ImageList1
          OnClick = MenuItemClick
        end
        object miTapeReduce1: TTBItem
          Tag = 2
          AutoCheck = True
          Caption = #1056#1072#1079#1074#1077#1088#1090#1082#1072' '#1090#1080#1087#1072' "'#1042'" '#1089' '#1089#1074#1077#1076#1077#1085#1080#1077#1084' '#1082' '#1077#1076#1080#1085#1086#1084#1091' '#1089#1077#1095#1077#1085#1080#1102' 1'
          GroupIndex = 3
          Images = ImageList1
          OnClick = MenuItemClick
        end
        object miTapeReduce2: TTBItem
          Tag = 2
          AutoCheck = True
          Caption = #1056#1072#1079#1074#1077#1088#1090#1082#1072' '#1090#1080#1087#1072' "'#1042'" '#1089' '#1089#1074#1077#1076#1077#1085#1080#1077#1084' '#1082' '#1077#1076#1080#1085#1086#1084#1091' '#1089#1077#1095#1077#1085#1080#1102' 2'
          GroupIndex = 3
          Images = ImageList1
          OnClick = MenuItemClick
        end
        object miRail: TTBItem
          Tag = 2
          AutoCheck = True
          Caption = #1042' '#1074#1080#1076#1077' '#1088#1077#1083#1100#1089#1072
          Enabled = False
          GroupIndex = 3
          Images = ImageList1
          OnClick = MenuItemClick
        end
        object N6: TTBSeparatorItem
        end
        object miBothRail: TTBItem
          Tag = 1
          AutoCheck = True
          Caption = #1054#1073#1077' '#1085#1080#1090#1080
          Checked = True
          GroupIndex = 2
          Images = ImageList1
          OnClick = MenuItemClick
        end
        object miLeftRail: TTBItem
          Tag = 3
          AutoCheck = True
          Caption = #1051#1077#1074#1072#1103' '#1085#1080#1090#1100
          GroupIndex = 2
          Images = ImageList1
          OnClick = MenuItemClick
        end
        object miRightRail: TTBItem
          Tag = 2
          AutoCheck = True
          Caption = #1055#1088#1072#1074#1072#1103' '#1085#1080#1090#1100
          GroupIndex = 2
          Images = ImageList1
          OnClick = MenuItemClick
        end
        object TBSeparatorItem9: TTBSeparatorItem
        end
        object miAllChannels: TTBItem
          Tag = 1
          AutoCheck = True
          Caption = #1042#1089#1077' '#1082#1072#1085#1072#1083#1099
          Checked = True
          GroupIndex = 4
          HelpContext = 1
          Images = ImageList1
          OnClick = MenuItemClick
        end
        object miOnlyNaezdChannels: TTBItem
          Tag = 2
          AutoCheck = True
          Caption = #1058#1086#1083#1100#1082#1086' '#1085#1072#1077#1079#1078#1072#1102#1097#1080#1077
          GroupIndex = 4
          HelpContext = 1
          Images = ImageList1
          OnClick = MenuItemClick
        end
        object miOnlyOtezdChannels: TTBItem
          Tag = 3
          AutoCheck = True
          Caption = #1058#1086#1083#1100#1082#1086' '#1086#1090#1098#1077#1079#1078#1072#1102#1097#1080#1077
          GroupIndex = 4
          HelpContext = 1
          Images = ImageList1
          OnClick = MenuItemClick
        end
        object TBSeparatorItem10: TTBSeparatorItem
        end
        object miZoomList: TTBSubmenuItem
          Caption = #1052#1072#1089#1096#1090#1072#1073
          ImageIndex = 3
          Images = ImageList1
        end
        object TBSeparatorItem32: TTBSeparatorItem
        end
        object tbAmplThList: TTBSubmenuItem
          Caption = #1055#1086#1088#1086#1075' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103' '
          object AmplTh0: TTBItem
            AutoCheck = True
            Caption = '- 12 '#1076#1041
            Checked = True
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh1: TTBItem
            Tag = 1
            AutoCheck = True
            Caption = '- 10 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh2: TTBItem
            Tag = 2
            AutoCheck = True
            Caption = '- 8 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh3: TTBItem
            Tag = 3
            AutoCheck = True
            Caption = '- 6 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh4: TTBItem
            Tag = 4
            AutoCheck = True
            Caption = '- 4 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh5: TTBItem
            Tag = 5
            AutoCheck = True
            Caption = '- 2 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh6: TTBItem
            Tag = 6
            AutoCheck = True
            Caption = '   0 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh7: TTBItem
            Tag = 7
            AutoCheck = True
            Caption = '+ 2 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh8: TTBItem
            Tag = 8
            AutoCheck = True
            Caption = '+ 4 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh9: TTBItem
            Tag = 9
            AutoCheck = True
            Caption = '+ 6 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh10: TTBItem
            Tag = 10
            AutoCheck = True
            Caption = '+ 8 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh11: TTBItem
            Tag = 11
            AutoCheck = True
            Caption = '+ 10 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh12: TTBItem
            Tag = 12
            AutoCheck = True
            Caption = '+ 12 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh13: TTBItem
            Tag = 13
            AutoCheck = True
            Caption = '+ 14 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh14: TTBItem
            Tag = 14
            AutoCheck = True
            Caption = '+ 16 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
          object AmplTh15: TTBItem
            Tag = 15
            AutoCheck = True
            Caption = '+ 18 '#1076#1041
            GroupIndex = 11
            OnClick = MenuItemClick
          end
        end
        object TBSeparatorItem30: TTBSeparatorItem
        end
        object miBackMotion: TTBItem
          AutoCheck = True
          Caption = #1047#1086#1085#1099' '#1076#1074#1080#1078#1077#1085#1080#1103' '#1085#1072#1079#1072#1076
          Checked = True
          OnClick = miBackMotionClick
        end
        object TBSeparatorItem5: TTBSeparatorItem
        end
        object miPointSize: TTBSubmenuItem
          Caption = 'Point size'
          object miSmallPoint: TTBItem
            AutoCheck = True
            Caption = 'Small'
            GroupIndex = 1
            OnClick = miSmallPointClick
          end
          object miMediumPoint: TTBItem
            AutoCheck = True
            Caption = 'Medium'
            Checked = True
            GroupIndex = 1
            OnClick = miMediumPointClick
          end
          object miLargePoint: TTBItem
            AutoCheck = True
            Caption = 'Large'
            GroupIndex = 1
            OnClick = miLargePointClick
          end
        end
        object miEchoSizeByAmpl: TTBItem
          AutoCheck = True
          Caption = #1056#1072#1079#1084#1077#1088' '#1090#1086#1095#1082#1080' '#1079#1072#1074#1080#1089#1080#1090' '#1086#1090' '#1072#1084#1087#1083#1080#1090#1091#1076#1099
          OnClick = miEchoSizeByAmplClick
        end
        object miEchoColorByAmpl: TTBItem
          AutoCheck = True
          Caption = 'Point color by Amplitude'
          OnClick = miEchoColorByAmplClick
        end
        object TBSeparatorItem26: TTBSeparatorItem
        end
        object miMarkOnBScan: TTBItem
          AutoCheck = True
          Caption = #1054#1090#1084#1077#1090#1082#1080' '#1085#1072' '#1042'-'#1088#1072#1079#1074#1077#1088#1090#1082#1077
          Checked = True
          OnClick = miMarkOnBScanClick
        end
        object TBSeparatorItem29: TTBSeparatorItem
        end
        object miViewAC: TTBItem
          AutoCheck = True
          Caption = #1040#1082#1091#1089#1090#1080#1095#1077#1089#1082#1080#1081' '#1082#1086#1085#1090#1072#1082#1090'/'#1053#1077' '#1087#1088#1086#1082#1086#1085#1090#1088#1086#1083#1080#1088#1086#1074#1072#1085#1085#1099#1077' '#1091#1095#1072#1089#1090#1082#1080
          Checked = True
          OnClick = miViewACClick
        end
        object miViewAutomaticSearchRes: TTBItem
          Caption = #1040#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1080' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1041#1059#1048' '#171#1079#1085#1072#1095#1080#1084#1099#1077#187' '#1091#1095#1072#1089#1090#1082#1080
          OnClick = miViewAutomaticSearchResClick
        end
        object TBSeparatorItem23: TTBSeparatorItem
        end
        object miDonAmpl: TTBItem
          AutoCheck = True
          Caption = #1054#1075#1080#1073#1072#1102#1097#1072#1103' '#1076#1086#1085#1085#1086#1075#1086' '#1089#1080#1075#1085#1072#1083#1072
          GroupIndex = 6
          OnClick = MenuItemClick
        end
        object TBSeparatorItem24: TTBSeparatorItem
        end
        object miFiltrMenu: TTBSubmenuItem
          Caption = 'Filter'
          object Filtr_Calc: TTBItem
            Caption = 'Calculation'
            Enabled = False
            OnClick = Filtr_CalcClick
          end
          object TBSeparatorItem22: TTBSeparatorItem
          end
          object Filtr_Off: TTBItem
            AutoCheck = True
            Caption = 'Without filter'
            Enabled = False
            GroupIndex = 1
            OnClick = Filtr_OffClick
          end
          object Filtr_Mark: TTBItem
            AutoCheck = True
            Caption = 'Mark noises'
            Enabled = False
            GroupIndex = 1
            OnClick = Filtr_MarkClick
          end
          object Filtr_Hide: TTBItem
            AutoCheck = True
            Caption = 'Hide noises'
            Enabled = False
            GroupIndex = 1
            OnClick = Filtr_HideClick
          end
        end
        object TBSeparatorItem13: TTBSeparatorItem
        end
        object miChGate: TTBItem
          AutoCheck = True
          Caption = 'ChGate'
          Checked = True
          OnClick = miChGateClick
        end
        object miChInfo_: TTBItem
          AutoCheck = True
          Caption = 'ChInfo'
          Checked = True
          OnClick = miChInfoClick
        end
        object miChSettings: TTBItem
          AutoCheck = True
          Caption = 'ChSettings'
          Checked = True
          OnClick = miChSettingsClick
        end
        object TBSeparatorItem21: TTBSeparatorItem
        end
        object miViewPaintSystemState: TTBItem
          Caption = 'Paint system results'
          OnClick = miViewPaintSystemStateClick
        end
        object miMetallSensor: TTBSubmenuItem
          Caption = 'MetallSensor'
          object miMetallSensorValue0: TTBItem
            Caption = 'miMetallSensorValue0'
            OnClick = miMetallSensorValue0Click
          end
          object miMetallSensorValue1: TTBItem
            Caption = 'miMetallSensorValue1'
            OnClick = miMetallSensorValue1Click
          end
          object miMetallSensorZone: TTBItem
            Caption = 'Zone'
            OnClick = miMetallSensorZoneClick
          end
        end
        object miUnstableBottomSignal: TTBItem
          Caption = 'Unstable bottom signel'
          OnClick = miUnstableBottomSignalClick
        end
        object miZerroProbeMode: TTBItem
          Caption = 'Zerro probe mode'
          Visible = False
          OnClick = miZerroProbeModeClick
        end
        object miPaintSystemState: TTBItem
          Caption = 'Paint system state'
          Visible = False
          OnClick = miPaintSystemStateClick
        end
        object TBItem29: TTBItem
          Caption = 'Test'
          Visible = False
          OnClick = TBItem29Click
        end
        object TBItem34: TTBItem
          AutoCheck = True
          Caption = #1047#1086#1085#1099' '#1076#1074#1080#1078#1077#1085#1080#1103' '#1085#1072#1079#1072#1076
          Checked = True
          Visible = False
          OnClick = miBackMotionClick
        end
      end
      object miGotoMan: TTBSubmenuItem
        Caption = #1055#1077#1088#1077#1093#1086#1076
        object miGoto: TTBItem
          Caption = #1053#1072' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1091'...'
          Hint = #1055#1077#1088#1077#1081#1090#1080' '#1085#1072' '#1087#1091#1090#1077#1081#1089#1082#1091#1102' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1091
          ImageIndex = 53
          Images = ImageList1
          OnClick = miGotoClick
        end
        object miGotoLabel: TTBSubmenuItem
          Caption = #1053#1072' '#1086#1090#1084#1077#1090#1082#1091' '#1086#1087#1077#1088#1072#1090#1086#1088#1072
          ImageIndex = 58
          Images = ImageList1
        end
        object miGotoRecord: TTBSubmenuItem
          Caption = #1053#1072' '#1079#1072#1087#1080#1089#1100' '#1073#1083#1086#1082#1085#1086#1090#1072
          ImageIndex = 15
          Images = ImageList1
        end
        object miHandScan: TTBSubmenuItem
          Caption = #1056#1091#1095#1085#1086#1077' '#1089#1082#1072#1085#1080#1088#1086#1074#1072#1085#1080#1077
          ImageIndex = 14
          Images = ImageList1
        end
      end
      object miWinMan: TTBSubmenuItem
        Caption = #1054#1082#1085#1072
        OnPopup = miWinManPopup
        object miWinCascade: TTBItem
          Action = WindowCascade1
        end
        object miWinTileHorizontal: TTBItem
          Action = WindowTileHorizontal1
        end
        object miWinTileVertical: TTBItem
          Action = WindowTileVertical1
        end
        object miWinMinimizeAll: TTBItem
          Action = WindowMinimizeAll1
        end
        object miNextWin: TTBItem
          Caption = #1057#1083#1077#1076#1091#1102#1097#1077#1077
          Enabled = False
          ShortCut = 117
          OnClick = miNextWinClick
        end
        object TBSeparatorItem14: TTBSeparatorItem
        end
        object miLincWin: TTBItem
          Caption = #1057#1074#1072#1079#1072#1090#1100' '#1086#1082#1085#1072
          OnClick = miLincWinClick
        end
        object TBSeparatorItem6: TTBSeparatorItem
        end
        object TBMDIWindowItem1: TTBMDIWindowItem
        end
      end
      object TBSubmenuItem1: TTBSubmenuItem
        Caption = '?'
        object tbHelp: TTBItem
          Caption = #1042#1099#1079#1086#1074' '#1089#1087#1088#1072#1074#1082#1080'...'
          ImageIndex = 54
          Images = ImageList1
          OnClick = tbHelpClick
        end
        object tbAbout: TTBItem
          Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'...'
          OnClick = tbAboutClick
        end
        object TBSeparatorItem1: TTBSeparatorItem
        end
        object tbIE: TTBItem
          Caption = #1054#1090#1082#1088#1099#1090#1100' '#1089#1072#1081#1090' '#1054#1040#1054' '#1056#1072#1076#1080#1086#1072#1074#1080#1086#1085#1080#1082#1072
          ImageIndex = 55
          Images = ImageList1
          OnClick = tbIEClick
        end
        object tbFTP: TTBItem
          Caption = 'Ftp'
          ImageIndex = 37
          Images = ImageList1
          OnClick = tbFTPClick
        end
        object tbMailTo: TTBItem
          Caption = #1053#1072#1087#1080#1089#1072#1090#1100' '#1087#1080#1089#1100#1084#1086' '#1088#1072#1079#1088#1072#1073#1086#1090#1095#1080#1082#1091
          ImageIndex = 56
          Images = ImageList1
          OnClick = tbMailToClick
        end
      end
      object miDebug: TTBSubmenuItem
        Caption = 'Debug'
        OnClick = miDebugClick
        object TBItem20: TTBItem
          Caption = 'Load Log'
          Visible = False
          OnClick = TBItem20Click
        end
        object TBItem17: TTBItem
          Caption = 'Error list...'
          ShortCut = 16465
          Visible = False
          OnClick = TBItem17Click
        end
        object TBItem16: TTBItem
          Caption = 'Event list...'
          OnClick = TBItem16Click
        end
        object TBItem18: TTBItem
          Caption = 'Coord Graph'
          OnClick = TBItem18Click
        end
        object TBItem1: TTBItem
          Caption = 'ReAnalyze file'
          OnClick = TBItem1Click
        end
        object TBItem5: TTBItem
          Caption = 'Cut ExHeader'
          Visible = False
          OnClick = TBItem5Click
        end
        object TBItem2: TTBItem
          Caption = 'File content + Coord Graph'
          Visible = False
          OnClick = TBItem2Click
        end
        object TBItem6: TTBItem
          AutoCheck = True
          Caption = 'Show 0 Echo Count Events'
          Visible = False
          OnClick = TBItem6Click
        end
        object TBItem4: TTBItem
          AutoCheck = True
          Caption = 'Show Events at 0 coord'
          OnClick = TBItem4Click
        end
        object TBItem7: TTBItem
          Caption = 'Scan Step Edit'
          Visible = False
          OnClick = TBItem7Click
        end
        object TBItem9: TTBItem
          AutoCheck = True
          Caption = 'Unpack bottom signal'
          Checked = True
          Visible = False
          OnClick = TBItem9Click
        end
        object biForceUGOUSW: TTBItem
          AutoCheck = True
          Caption = 'force EGO USW'
          OnClick = biForceUGOUSWClick
        end
        object TBSeparatorItem7: TTBSeparatorItem
          Visible = False
        end
        object TBItem32: TTBItem
          AutoCheck = True
          Caption = 'Event Number and ID'
          OnClick = TBItem32Click
        end
        object TBItem33: TTBItem
          AutoCheck = True
          Caption = 'USB log'
          OnClick = TBItem33Click
        end
        object TBItem8: TTBItem
          AutoCheck = True
          Caption = 'Show Mouse Coord & File Offset'
          OnClick = TBItem8Click
        end
        object TBItem26: TTBItem
          AutoCheck = True
          Caption = 'Show Sensor1 Data'
          OnClick = TBItem26Click
        end
        object TBSeparatorItem18: TTBSeparatorItem
          Visible = False
        end
        object ScanStepItem: TTBItem
        end
        object tbHeaderFlags: TTBItem
          Caption = #1060#1083#1072#1075#1080' '#1079#1072#1075#1086#1083#1086#1074#1082#1072' '#1092#1072#1081#1083#1072
          OnClick = tbHeaderFlagsClick
        end
        object TBItemTemperatureChart: TTBItem
          Caption = 'Temperature Chart'
          OnClick = TBItemTemperatureChartClick
        end
        object tbAllEventsOnLine: TTBItem
          AutoCheck = True
          Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1074#1089#1077' '#1089#1086#1073#1099#1090#1080#1103' '#1085#1072' '#1083#1080#1085#1077#1081#1082#1077
          OnClick = tbAllEventsOnLineClick
        end
        object tbGPSTrack: TTBItem
          Caption = 'GPS '#1090#1088#1077#1082' '#1085#1072' '#1082#1072#1088#1090#1077
          OnClick = tbGPSTrackClick
        end
        object TBItem35: TTBItem
          Caption = #1060#1080#1083#1100#1090#1088#1086#1074#1072#1090#1100' '#1082#1086#1088#1086#1090#1082#1080#1077' '#1087#1088#1086#1087#1072#1076#1072#1085#1080#1103' '#1040#1050
          OnClick = TBItem35Click
        end
        object TBItem36: TTBItem
          AutoCheck = True
          Caption = #1054#1090#1086#1073#1088#1072#1078#1077#1085#1080#1077' '#1086#1076#1080#1082#1086#1074#1099#1093' '#1082#1086#1086#1088#1076#1080#1085#1072#1090
          OnClick = TBItem36Click
        end
        object SHOW_REDUCEPOS: TTBItem
          AutoCheck = True
          Caption = 'SHOW_REDUCEPOS'
        end
        object TBItem38: TTBItem
        end
      end
      object miAddNewRecMenu: TTBSubmenuItem
        Caption = 'AddNewRecMenu'
        Visible = False
        OnPopup = miAddNewRecMenuPopup
        object miInFile: TTBItem
          AutoCheck = True
          Caption = 'In File'
          Checked = True
          GroupIndex = 1
          OnClick = miInFileClick
        end
        object miInNordico: TTBItem
          AutoCheck = True
          Caption = 'In Nordico'
          GroupIndex = 1
          OnClick = miInNordicoClick
        end
      end
    end
    object TBToolbar2: TTBToolbar
      Left = 754
      Top = 23
      Align = alLeft
      BorderStyle = bsNone
      Caption = 'TBToolbar2'
      CloseButton = False
      DockMode = dmCannotFloat
      DockPos = 657
      DockRow = 1
      TabOrder = 1
      object TBControlItem3: TTBControlItem
        Control = Panel6
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 409
        Height = 37
        BevelOuter = bvNone
        TabOrder = 0
      end
    end
    object MainToolbar: TTBToolbar
      Left = 0
      Top = 23
      AutoResize = False
      BorderStyle = bsNone
      Caption = 'MainToolbar'
      DockMode = dmCannotFloat
      DockPos = -6
      DockRow = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [tboImageAboveCaption]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
      TabOrder = 2
      object tbOpen: TTBSubmenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100
        DropdownCombo = True
        ImageIndex = 0
        Images = ImageList1
        LinkSubitems = miRecent
        Options = [tboDropdownArrow]
        OnClick = tbOpenClick
        OnPopup = tbOpenPopup
      end
      object tbBase: TTBSubmenuItem
        Caption = #1041#1072#1079#1072
        DropdownCombo = True
        ImageIndex = 43
        Images = ImageList1
        LinkSubitems = miSameCoordFiles
        Options = [tboDropdownArrow]
        OnClick = tbBaseClick
      end
      object TBSeparatorItem2: TTBSeparatorItem
      end
      object tbBScan: TTBSubmenuItem
        Caption = #1051#1077#1085#1090#1072
        DropdownCombo = True
        ImageIndex = 2
        Images = ImageList1
        Options = [tboDropdownArrow]
        OnClick = tbBScan_Click
        object TBItem13: TTBItem
          GroupIndex = 1
          OnClick = tbBScan_Click
        end
        object TBItem12: TTBItem
          Tag = 1
          GroupIndex = 1
          OnClick = tbBScan_Click
        end
        object TBItem11: TTBItem
          Tag = 2
          GroupIndex = 1
          OnClick = tbBScan_Click
        end
        object TBItem10: TTBItem
          Tag = 3
          GroupIndex = 1
          OnClick = tbBScan_Click
        end
      end
      object tbBScan_: TTBItem
        Caption = #1056#1072#1079#1074#1077#1088#1090#1082#1072' "'#1042'"'
        GroupIndex = 1
        ImageIndex = 0
        Images = ImageList1
        MaskOptions = [tboToolbarStyle, tboToolbarSize]
        Visible = False
        OnClick = tbBScan_Click
      end
      object tbViewCh: TTBSubmenuItem
        Caption = #1050#1072#1085#1072#1083#1099
        DropdownCombo = True
        ImageIndex = 4
        Images = ImageList1
        Options = [tboDropdownArrow]
        OnClick = tbViewChClick
        object TBItem14: TTBItem
          Caption = #1042#1089#1077
          OnClick = tbViewChClick
        end
        object TBItem15: TTBItem
          Tag = 1
          Caption = #1053#1072#1077#1079#1078#1072#1102#1097#1080#1077
          OnClick = tbViewChClick
        end
        object TBItem19: TTBItem
          Tag = 2
          Caption = #1054#1090#1098#1077#1079#1078#1072#1102#1097#1080#1077
          OnClick = tbViewChClick
        end
        object TBItem21: TTBItem
          Caption = #1056#1072#1073#1086#1095#1072#1103' '#1075#1088#1072#1085#1100' '#1075#1086#1083#1086#1074#1082#1080
          Visible = False
        end
        object TBItem22: TTBItem
          Caption = #1053#1077#1088#1072#1073#1086#1095#1072#1103' '#1075#1088#1072#1085#1100' '#1075#1086#1083#1086#1074#1082#1080
          Visible = False
        end
      end
      object tbViewCh_: TTBItem
        Caption = #1042#1089#1077' '#1082#1072#1085#1072#1083#1099
        GroupIndex = 2
        ImageIndex = 0
        Visible = False
      end
      object tbDrawRail: TTBSubmenuItem
        Caption = #1053#1080#1090#1080
        DropdownCombo = True
        ImageIndex = 19
        Images = ImageList1
        Options = [tboDropdownArrow]
        OnClick = tbDrawRailClick
        object TBItem25: TTBItem
          OnClick = tbDrawRailClick
        end
        object TBItem24: TTBItem
          Tag = 1
          OnClick = tbDrawRailClick
        end
        object TBItem23: TTBItem
          Tag = 2
          OnClick = tbDrawRailClick
        end
      end
      object tbDrawRail_: TTBItem
        ImageIndex = 0
        MaskOptions = [tboSameWidth]
        Visible = False
      end
      object TBSeparatorItem3: TTBSeparatorItem
      end
      object tbZoomList: TTBSubmenuItem
        Caption = #1052#1072#1089#1096#1090#1072#1073
        ImageIndex = 3
        Images = ImageList1
        LinkSubitems = miZoomList
        Options = [tboDropdownArrow]
      end
      object TBSeparatorItem4: TTBSeparatorItem
      end
      object tbGoto: TTBSubmenuItem
        Caption = #1055#1077#1088#1077#1093#1086#1076
        DropdownCombo = True
        ImageIndex = 13
        Images = ImageList1
        LinkSubitems = miGotoLabel
        Options = [tboDropdownArrow]
        OnClick = miGotoClick
      end
      object TBSeparatorItem11: TTBSeparatorItem
      end
      object TBControlItem1: TTBControlItem
        Control = Panel1
      end
      object tbViewTh: TTBSubmenuItem
        Caption = #1055#1086#1088#1086#1075' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103
        ImageIndex = 59
        Images = ImageList1
        LinkSubitems = tbAmplThList
        Options = [tboDropdownArrow]
        Visible = False
      end
      object tbVideoSep: TTBSeparatorItem
        Visible = False
      end
      object tbVideo: TTBItem
        Caption = #1042#1080#1076#1077#1086
        ImageIndex = 62
        Images = ImageList1
        Visible = False
        OnClick = tbVideoClick
      end
      object TBSeparatorItem31: TTBSeparatorItem
      end
      object tbNewCVSRec: TTBSubmenuItem
        Caption = #1053#1086#1074'.'#1047#1072#1087#1080#1089#1100
        DropdownCombo = True
        ImageIndex = 16
        Images = ImageList1
        LinkSubitems = miAddNewRecMenu
        Options = [tboDropdownArrow]
        Visible = False
        OnClick = tbNewCVSRecClick
      end
      object tbNewFileRec: TTBItem
        Caption = #1053#1086#1074'.'#1047#1072#1087#1080#1089#1100
        ImageIndex = 16
        Images = ImageList1
        OnClick = tbNewFileRecClick
      end
      object tbNotebook: TTBSubmenuItem
        Caption = #1041#1083#1086#1082#1085#1086#1090
        DropdownCombo = True
        ImageIndex = 26
        Images = ImageList1
        LinkSubitems = miGotoRecord
        Options = [tboDropdownArrow]
        OnClick = tbNotebook_Click
      end
      object TBSeparatorItem17: TTBSeparatorItem
      end
      object TopographTBItem: TTBSubmenuItem
        Caption = #1040'-'#1088#1072#1089#1096'.'
        ImageIndex = 37
        Images = ImageList1
        Options = [tboDropdownArrow]
        OnSelect = TopographTBItemSelect
      end
      object TBSeparatorItem25: TTBSeparatorItem
        Visible = False
      end
      object TBItem27: TTBItem
        AutoCheck = True
        Caption = 'Test 1'
        Visible = False
        OnClick = TBItem27Click
      end
      object TBItem28: TTBItem
        AutoCheck = True
        Caption = 'Test 2'
        Visible = False
        OnClick = TBItem28Click
      end
      object Panel1: TPanel
        Left = 435
        Top = 0
        Width = 106
        Height = 37
        BevelOuter = bvNone
        TabOrder = 0
        object AmplThTrackBar: TTrackBar
          Left = 0
          Top = 13
          Width = 106
          Height = 24
          Align = alClient
          Max = 15
          SelEnd = 6
          SelStart = 6
          TabOrder = 1
          TabStop = False
          ThumbLength = 18
          OnChange = AmplThTrackBarChange
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 106
          Height = 13
          Align = alTop
          BevelOuter = bvNone
          Caption = #1055#1086#1088#1086#1075': -12 '#1076#1041
          TabOrder = 0
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 6
          Height = 41
          BevelOuter = bvNone
          TabOrder = 2
        end
        object Panel4: TPanel
          Left = 118
          Top = 0
          Width = 6
          Height = 41
          BevelOuter = bvNone
          TabOrder = 3
        end
      end
    end
    object Info2Toolbar: TTBToolbar
      Left = 584
      Top = 23
      Align = alLeft
      AutoResize = False
      BorderStyle = bsNone
      Caption = 'TBToolbar2'
      CloseButton = False
      DockMode = dmCannotFloat
      DockPos = 657
      DockRow = 1
      Options = [tboSameWidth]
      TabOrder = 3
      object TBControlItem2: TTBControlItem
        Control = InfoPanel2
      end
      object InfoPanel2: TPanel
        Left = 0
        Top = 0
        Width = 160
        Height = 38
        BevelOuter = bvNone
        TabOrder = 0
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 488
    Width = 1173
    Height = 69
    Align = alBottom
    TabOrder = 1
    Visible = False
    object SpeedButton1: TSpeedButton
      Left = 8
      Top = 4
      Width = 97
      Height = 25
      Caption = 'Test'
      Visible = False
      OnClick = TestBtnClick
    end
    object SpeedButton2: TSpeedButton
      Tag = 1
      Left = 111
      Top = 4
      Width = 97
      Height = 25
      Caption = 'Schema 1'
      Visible = False
      OnClick = TestBtnClick
    end
    object SpeedButton3: TSpeedButton
      Tag = 2
      Left = 214
      Top = 4
      Width = 97
      Height = 25
      Caption = 'Schema 2'
      Visible = False
      OnClick = TestBtnClick
    end
    object SpeedButton4: TSpeedButton
      Tag = 3
      Left = 317
      Top = 4
      Width = 97
      Height = 25
      Caption = 'Weels'
      Visible = False
      OnClick = TestBtnClick
    end
    object SpeedButton5: TSpeedButton
      Tag = 4
      Left = 420
      Top = 4
      Width = 63
      Height = 25
      Caption = 'Russion'
      OnClick = TestBtnClick
    end
    object SpeedButton6: TSpeedButton
      Tag = 5
      Left = 489
      Top = 4
      Width = 63
      Height = 25
      Caption = 'English'
      OnClick = TestBtnClick
    end
    object SpeedButton7: TSpeedButton
      Tag = 6
      Left = 558
      Top = 4
      Width = 63
      Height = 25
      Caption = 'Turkish'
      OnClick = TestBtnClick
    end
    object SpeedButton8: TSpeedButton
      Left = 840
      Top = 4
      Width = 23
      Height = 22
      AllowAllUp = True
      GroupIndex = 12
      Caption = 'A'
      Visible = False
      OnClick = SpeedButton8Click
    end
    object SpeedButton9: TSpeedButton
      Tag = 7
      Left = 627
      Top = 4
      Width = 63
      Height = 25
      Caption = 'Franch'
      OnClick = TestBtnClick
    end
    object SpeedButton10: TSpeedButton
      Left = 847
      Top = 37
      Width = 97
      Height = 25
      Caption = 'Make DB'
      OnClick = SpeedButton10Click
    end
    object ScanStepBtm: TButton
      Left = 696
      Top = 4
      Width = 75
      Height = 25
      Caption = 'ScanStepBtm'
      TabOrder = 0
      Visible = False
      OnClick = ScanStepBtmClick
    end
    object Edit1: TEdit
      Left = 775
      Top = 5
      Width = 60
      Height = 21
      TabOrder = 1
      Text = 'Edit1'
      Visible = False
    end
    object Button134: TButton
      Left = 869
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button134'
      TabOrder = 2
      Visible = False
    end
    object TestButton: TButton
      Left = 950
      Top = 6
      Width = 64
      Height = 25
      Caption = 'TestButton'
      TabOrder = 3
      Visible = False
      OnClick = TestButtonClick
    end
    object Button14444: TButton
      Left = 1023
      Top = 5
      Width = 75
      Height = 25
      Caption = 'Button145'
      TabOrder = 4
      Visible = False
      OnClick = Button14444Click
    end
    object Button1563: TButton
      Left = 16
      Top = 40
      Width = 75
      Height = 25
      Caption = 'Button1563'
      TabOrder = 5
      Visible = False
    end
  end
  object TBDock3: TTBDock
    Left = 1005
    Top = 89
    Width = 168
    Height = 399
    FixAlign = True
    Position = dpRight
    object TBToolWindow4: TTBToolWindow
      Left = 0
      Top = 0
      BorderStyle = bsNone
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
      ClientAreaHeight = 395
      ClientAreaWidth = 164
      DefaultDock = TBDock3
      DockableTo = []
      DockMode = dmCannotFloat
      DragHandleStyle = dhNone
      FullSize = True
      TabOrder = 0
      Visible = False
    end
  end
  object DebugPanel: TPanel
    Left = 0
    Top = 65
    Width = 1173
    Height = 24
    Align = alTop
    Alignment = taLeftJustify
    TabOrder = 3
    Visible = False
  end
  object OpenDialog: TOpenDialog
    FileName = '*.a11'
    Filter = #1060#1072#1081#1083#1099' '#1087#1088#1086#1077#1079#1076#1086#1074'|*.a11|'#1042#1089#1077' '#1092#1072#1081#1083#1099'|*.*'
    Left = 40
    Top = 80
  end
  object ImageList1: TImageList
    Left = 200
    Top = 80
    Bitmap = {
      494C01013F004100040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000000001000001002000000000000000
      0100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000021BA
      EF005AAEDE0039BEEF007BFFFF006BF7FF004AE3FF0031CFF70018BEEF0008B6
      EF0021BAEF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000021BA
      EF005AAEDE0039BEEF007BFFFF006BF7FF004AE3FF0031CFF70018BEEF0008B6
      EF0021BAEF00000000000000000000000000C6C7C6007B797B00737973007379
      7300737973007B717B007379730073797300737973007B717B00737973007B79
      7B007B797B007B797B007B797B007B717B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600FFFF0000C6C3C600FFFF0000C6C3C600FFFF0000C6C3C600FFFF0000C6C3
      C6000000000000000000000000000000000000000000000000000000000021BA
      EF005AAEDE0039BEEF006BF7FF006BF7FF004AE3FF0031CFF70018BEEF0008B6
      EF0021BAEF00000000000000000000000000B5B6B500C6CFC600CECFCE00C6CF
      C600CECFCE00CECFCE00C6CFC600C6CFC600D6D7D600D6D7D600A5A6A500BDC7
      BD00A59EA500CECFCE00C6CFC600B5B6B5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000C6C3C600FFFF0000C6C3C600FFFF0000C6C3C600FFFF0000C6C3C600FFFF
      00000000000000000000000000000000000000000000000000000000000021BA
      EF005AAEDE0039BEEF006BF7FF006BF7FF004AE3FF0031CFF70018BEEF0008B6
      EF0021BAEF00000000000000000000000000ADAEAD00D6D7D600F79E2100F79E
      2100F79E2100F79E2100F79E2100F79E2100F79E2100F79E2100F79E2100F79E
      2100F79E2100F7A62100D6D7D600ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008482840000000000C6C3
      C600FFFF00000000000000000000C6C3C600FFFF0000C6C3C600FFFF0000C6C3
      C6000000000000000000000000000000000000000000000000000000000021BA
      EF005AAEDE0039BEEF006BF7FF006BF7FF004AE3FF0031CFF70021BAEF0008B6
      EF0021BAEF00000000000000000000000000A5AEA500DED7DE00F79E1000F7DF
      A500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DF
      A500F7DFA500F7AE3100DED7DE00A5AEA5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000C6C3C6000000000000000000FFFF0000C6C3C600FFFF0000C6C3C600FFFF
      00000000000000000000000000000000000000000000000000000000000021BA
      EF005AAEDE0039BEEF007BFFFF006BF7FF004AE3FF0031CFF70018BEEF0008B6
      EF0021BAEF00000000000000000000000000ADA6AD00DEDFDE00F7962100F7D7
      9C00F7D7A500F7DF9C00F7D79C00F7D7A500F7DF9C00F7DF9C00F7D7A500F7D7
      A500F7DF9C00F7AE3100DEDFDE00ADA6AD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600FFFF00000000000000000000C6C3C600FFFF0000C6C3C600FFFF0000C6C3
      C6000000000000000000000000000000000000000000000000000000000021BA
      EF005AAEDE0039BEEF006BF7FF006BF7FF004AE3FF0031CFF70021BAEF0008B6
      EF0021BAEF000000000000000000000000009CA69C00DEDFDE00F79E2100F7DF
      A500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DF
      A500F7DFA500F7A62100DEDFDE009CA69C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000C6C3C60000000000000000000000000000000000FFFF0000C6C3C600FFFF
      0000000000008482840084828400000000000000000000000000000000000000
      000063594A0063594A0063594A0063594A0063594A0063594A0063594A006359
      4A00000000000000000000000000000000009CA69C00DEDFDE00F79E1000F7D7
      9C00F7D7A500F7DF9C00F7D79C00F7D7A500F7DF9C00F7DF9C00F7D7A500F7D7
      A500F7DF9C00F7AE3100DEDFDE009CA69C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600FFFF000000000000000000000000000000000000C6C3C600FFFF0000C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      000063594A0063594A0063594A0063594A0063594A0063594A0063594A006359
      4A00000000000000000000000000000000009C9E9C00DEDFDE00F7962100F7D7
      9C00F7D7A500F7DF9C00F7D79C00F7D7A500F7DF9C00F7DF9C00F7D7A500F7D7
      A500F7DF9C00F7AE3100DEDFDE009C9E9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000C6C3C6000000000000000000FFFF0000C6C3C600FFFF0000C6C3C600FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      000063594A0063594A0063594A0063594A0063594A0063594A0063594A006359
      4A0000000000000000000000000000000000949E9400DEDFDE00F79E2100F7DF
      A500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DF
      A500F7DFA500F7A62100DEDFDE00949E94000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600FFFF00000000000000000000C6C3C6000000000000000000FFFF0000C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000000000000000000000000000000094969400DEDFDE00F79E1000F7D7
      9C00F7D7A500F7DF9C00F7D79C00F7D7A500F7DF9C00F7DF9C00F7D7A500F7D7
      A500F7DF9C00F7AE3100DEDFDE00949694000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008482840000000000FFFF
      0000C6C3C6000000000000000000000000000000000000000000C6C3C600FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000000000000000000000000000000094969400E7E7E700F7962100F7D7
      9C00F7D7A500F7DF9C00F7D79C00F7D7A500F7DF9C00F7DF9C00F7D7A500F7D7
      A500F7DF9C00F7AE3100E7E7E700949694000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600FFFF0000C6C3C600FFFF0000C6C3C600FFFF0000C6C3C600FFFF0000C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      000000000000C6C3C60063594A00C6C3C600C6C3C60063594A00C6C3C6000000
      000000000000000000000000000000000000948E9400E7E7E700F7961000F79E
      2900F79E1000F7962100F79E2900F79E1000F7962100F7962100F79E1000F79E
      1000F7962100F79E2900E7E7E700948E94000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000C6C3C600FFFF0000C6C3C600FFFF0000C6C3C600FFFF0000C6C3C600FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      0000000000000000000000000000000000008C968C00DED7DE00E7DFE700DED7
      DE00E7E7E700DEE7DE00DEDFDE00DEDFDE00E7E7E700E7E7E700E7DFE700E7E7
      E700E7E7E700DEDFDE00DED7DE008C968C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      000000000000000000000000000000000000C6C7C600A5AEA500A5A6A500949E
      94009C969C00949E94009C969C00A5AEA500A5A6A500949E9400A5AEA500A5A6
      A500949E94009C969C00949E94009C969C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000086
      0000008600000086000000860000008600000086000000860000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008486840084868400848684008486
      8400848684008486840084868400848684008486840000000000000000000000
      0000000000000000000000000000000000000000000000860000008600000086
      0000008600000000000000000000000000000086000000860000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008486840000000000000000000000
      0000000000000000000000000000000000008486840000000000000000000000
      0000000000000000000000000000000000000086000000860000008600000086
      0000000000000000FF000000FF000000FF000000000000860000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000FF000000FF000000000000FFFF0000FFFF00000000
      000000FFFF0000FFFF000000000000000000C6C7C600C6C7C600000000008486
      8400848684008486840084868400848684008486840084868400848684008486
      8400848684008486840084868400848684008486840000000000C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600000000008486840000000000000000000000
      0000000000000000000000000000000000000086000000000000000000000000
      0000008600000000000000000000000000000086000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000FF000000FF000000000000FFFF0000FFFF00000000
      000000FFFF0000FFFF000000000000000000000000000000000000000000C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600848684008486840000000000C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600000000008486840000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000000086000000860000008600000000000084868400000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000FF000000FF000000000000FFFF0000FFFF00000000
      000000FFFF0000FFFF000000000000000000C6C7C600C6C7C60000000000C6C7
      C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00C6C7C600848684008486840000000000C6C7C600C6C7
      C600C6C7C600C6C7C600000000000000000000000000C6C7C600000000000000
      0000000000000000000000000000000000000086000000000000000000000000
      00000086000000000000000000000000000084868400C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF000000FF000000000000FFFF0000FFFF00000000
      000000FFFF0000FFFF000000000000000000000000000000000000000000C6C7
      C60000FFFF0000FFFF0084868400848684008486840084868400848684008486
      840000FFFF0000FFFF00C6C7C600848684008486840000000000C6C7C600C6C7
      C6000000000000000000FFFFCE00FFFFCE00FFFFCE0000000000000000000000
      0000000000000000000000000000000000000086000000860000008600000086
      0000000000000000000084868400C6C7C60000000000C6C7C600C6C7C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000FF000000FF000000000000FFFF0000FFFF00000000
      000000FFFF0000FFFF000000000000000000C6C7C600C6C7C60000000000C6C7
      C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00C6C7C600848684008486840000000000C6C7C6000000
      0000C6C7C600FFFFCE00FFFFCE00FFFFCE00FFFFCE0000000000FFFFCE000000
      0000000000000000000000000000000000000000000084868400C6C7C6000000
      0000000000008486840000000000C6C7C600C6C7C60000000000C6C7C600C6C7
      C600000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000FF000000FF000000000000FFFF0000FFFF00000000
      000000FFFF0000FFFF000000000000000000000000000000000000000000C6C7
      C60000FFFF0000FFFF0084868400848684008486840084868400848684008486
      840000FFFF0000FFFF00C6C7C600848684008486840000000000C6C7C6000000
      0000FFFF0000848684008486840084868400FFFFCE0000000000FFFFCE000000
      0000C6C7C6000000000000000000000000000000000000000000C6C7C600C6C7
      C60000000000C6C7C600C6C7C60000000000C6C7C600C6C7C60000000000C6C7
      C600C6C7C6000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF0000FFFF00000000
      000000FFFF0000FFFF000000000000000000C6C7C600C6C7C60000000000C6C7
      C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF008486840084868400C6C7C60084868400848684000000000000000000FFFF
      0000FFFFCE00FFFF0000FFFFCE00FFFF0000FFFFCE0000000000FFFFCE00FFFF
      CE0000000000000000000000000000000000000000000000000000000000C6C7
      C600C6C7C60000000000C6C7C600C6C7C60000000000C6C7C600C6C7C6000000
      0000C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      000000000000000000000000000000000000FFFF0000FFFF0000FFFF00000000
      000000FFFF0000FFFF000000000000000000000000000000000000000000C6C7
      C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF008486840084868400C6C7C60084868400848684000000000000000000FFFF
      CE0084868400848684008486840084868400FFFFCE0000000000FFFFCE00FFFF
      CE00000000000000000000000000000000000000000000000000000000000000
      0000C6C7C600C6C7C60000000000C6C7C600C6C7C60000000000C6C7C600C6C7
      C60000000000C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      000000000000000000000000000000000000FFFF0000FFFF0000FFFF00000000
      000000FFFF0000FFFF000000000000000000C6C7C600C6C7C60000000000C6C7
      C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00C6C7C60084868400848684008486840000000000FFFF
      0000FFFFCE00FFFF0000FFFFCE00FFFFCE00FFFFCE0000000000FFFFCE00FFFF
      CE00000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C600C6C7C60000000000C6C7C600C6C7C60000000000C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000000000000000000000000000000000000000C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600848684000000000000000000C6C7C6000000
      0000000000000000000000000000000000000000000000000000FFFFCE000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C7C600C6C7C60000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFCE00FFFFCE00FFFFCE00FFFFCE00FFFFCE00FFFFCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFCE00FFFFCE00FFFFCE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C600000000000000000000000000C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008486
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C7C6008486840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848684008486
      8400000000000000000000000000000000000000000000000000848684008486
      8400848684008486840084868400848684008486840084868400848684008486
      8400848684008486840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C600C6C7C600C6C7C6008486840084868400848684000000
      000000000000000000000000000000000000000000000000000000000000C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6008486840000FFFF008486
      8400C6C7C6000000000000000000000000000000000000000000848684000000
      000000000000000000008486840000FFFF0000FFFF0084868400000000000000
      0000000000008486840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C6008486840084868400848684008486
      8400848684000000000000000000000000000000000000000000848684008486
      8400848684008486840084868400848684008486840000000000000000008486
      840084868400C6C7C60000000000000000000000000000000000848684000000
      000000000000000000008486840000FFFF0000FFFF0084868400000000000000
      00000000000084868400000000000000000000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C60000000000000000000000000000000000C6C7
      C600FFFFFF0000000000C6C7C600C6C7C6008486840084868400848684008486
      8400848684000000000000000000000000000000000084868400000000000000
      00000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      00000000000084868400C6C7C600000000000000000000000000848684000000
      000000000000000000008486840000FFFF0000FFFF0084868400000000000000
      00000000000084868400000000000000000000000000C6C7C600000000000000
      0000000000000000000000000000C6C7C600C6C7C60000000000000000000000
      00000000000000000000C6C7C60000000000000000000000000000000000C6C7
      C600FFFFFF0000000000C6C7C600C6C7C6008486840084868400848684008486
      84008486840000000000000000000000000000000000848684000000000000FF
      FF000000000000000000000000008486840084868400000000000000000000FF
      FF000000000084868400C6C7C600000000000000000000000000848684000000
      000000000000000000008486840000FFFF0000FFFF0084868400000000000000
      00000000000084868400000000000000000000000000C6C7C6000000000000FF
      000000FF000000FF000000000000C6C7C600C6C7C6000000000000FF000000FF
      000000FF000000000000C6C7C60000000000000000000000000000000000C6C7
      C600FFFFFF0000000000C6C7C600C6C7C6008486840084868400848684008486
      8400848684000000000000000000000000000000000084868400000000000000
      00000000000000FFFF0000000000000000000000000000FFFF00000000000000
      00000000000084868400C6C7C600000000000000000000000000848684000000
      0000000000000000000084868400848684008486840084868400000000000000
      00000000000084868400000000000000000000000000C6C7C6000000000000FF
      000000FF000000FF000000000000C6C7C600C6C7C6000000000000FF000000FF
      000000FF000000000000C6C7C60000000000000000000000000000000000C6C7
      C6000000000000000000C6C7C600C6C7C6008486840084868400848684008486
      84008486840000000000000000000000000000000000848684000000000000FF
      FF0000000000000000000000000084868400C6C7C600000000000000000000FF
      FF000000000084868400C6C7C600000000000000000000000000848684000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084868400000000000000000000000000C6C7C600000000000000
      0000000000000000000000000000C6C7C600C6C7C60000000000000000000000
      00000000000000000000C6C7C60000000000000000000000000000000000C6C7
      C600FFFFFF0000000000C6C7C600C6C7C6008486840084868400848684008486
      8400848684000000000000000000000000000000000084868400000000000000
      00000000000000FFFF0000000000C6C7C60084868400C6C7C600000000000000
      00000000000084868400C6C7C60000000000000000008486840000FFFF008486
      8400000000000000000000000000000000000000000000000000000000000000
      00008486840000FFFF00848684000000000000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C60000000000000000000000000000000000C6C7
      C600C6C7C600C6C7C600C6C7C600000000000000000084868400848684008486
      84008486840000000000000000000000000000000000848684000000000000FF
      FF0000000000000000000000000000FFFF00C6C7C60084868400C6C7C60000FF
      FF000000000084868400C6C7C6000000000000000000000000008486840000FF
      FF00848684000000000000000000000000000000000000000000000000008486
      840000FFFF0084868400000000000000000000000000C6C7C600000000000000
      0000000000000000000000000000C6C7C600C6C7C60000000000000000000000
      00000000000000000000C6C7C60000000000000000000000000000000000C6C7
      C600C6C7C6000000000000000000C6C7C6008486840000000000000000008486
      8400848684000000000000000000000000000000000084868400000000000000
      00000000000084868400C6C7C600000000000000000084868400848684000000
      00000000000084868400C6C7C600000000000000000000000000000000008486
      840000FFFF0084868400000000000000000000000000000000008486840000FF
      FF008486840000000000000000000000000000000000C6C7C6000000000000FF
      000000FF000000FF000000000000C6C7C600C6C7C6000000000000FF000000FF
      000000FF000000000000C6C7C600000000000000000000000000000000000000
      000000000000C6C7C600C6C7C600C6C7C6008486840084868400848684000000
      00000000000000000000000000000000000000000000848684000000000000FF
      FF0000000000848684008486840000FFFF00C6C7C600848684008486840000FF
      FF000000000084868400C6C7C600000000000000000000000000000000000000
      00008486840000FFFF008486840000000000000000008486840000FFFF008486
      84008486840000000000000000000000000000000000C6C7C6000000000000FF
      000000FF000000FF000000000000C6C7C600C6C7C6000000000000FF000000FF
      000000FF000000000000C6C7C600000000000000000000000000000000000000
      0000C6C7C600C6C7C600C6C7C600C6C7C6008486840084868400848684008486
      8400000000000000000000000000000000000000000084868400000000000000
      000000000000C6C7C60084868400848684008486840084868400C6C7C6000000
      00000000000084868400C6C7C600000000000000000000000000000000000000
      0000000000008486840000FFFF00848684008486840000FFFF00848684008486
      84008486840000000000000000000000000000000000C6C7C600000000000000
      0000000000000000000000000000C6C7C600C6C7C60000000000000000000000
      00000000000000000000C6C7C600000000000000000000000000000000000000
      00000000000000000000C6C7C600C6C7C6008486840084868400000000000000
      00000000000000000000000000000000000000000000848684000000000000FF
      FF0000000000000000000000000000FFFF0000000000000000000000000000FF
      FF000000000084868400C6C7C600000000000000000000000000000000000000
      000000000000000000008486840000FFFF0000FFFF0084868400000000008486
      84008486840000000000000000000000000000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600000000000000000000000000000000000000
      0000000000000000000000000000C6C7C6008486840000000000000000000000
      0000000000000000000000000000000000000000000084868400000000000000
      00000000000000FFFF0000000000000000000000000000FFFF00000000000000
      0000000000008486840000000000000000000000000000000000000000000000
      0000000000000000000000000000848684008486840000000000000000008486
      8400848684000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848684008486
      8400848684008486840084868400848684008486840084868400848684008486
      8400848684000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C7C60000000000FF00
      00000000FF00FF0000000000FF00FF0000000000FF00FF0000000000FF00FF00
      0000FF00000000000000C6C7C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF00E7DFCE00CEAE7300BD9E5200C69E4200C6A65200D6BE7300EFDFBD00FFFF
      F700FFFFFF00FFFFFF000000000000000000C6C7C600C6C7C600C6C7C6000000
      0000FF0000000000FF00FF0000000000FF00FF0000000000FF00FF0000000000
      FF0000000000C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C7C6007B797B00737973007379
      7300737973007B717B007379730073797300737973007B717B00737973007B79
      7B007B797B007B797B007B797B000000000000000000FFFFFF00F7EFE700CEAE
      7300B58E3900BD964200BD964200C69E4200D6B64A00E7BE5200DEBE4A00EFCF
      7300F7EFE700000000000000000000000000C6C7C600C6C7C600C6C7C6000000
      00000000FF00FF0000000000FF00FF0000000000FF00FF0000000000FF00FF00
      000000000000C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      000000000000000000000000000000000000B5B6B500C6CFC600CECFCE00C6CF
      C600CECFCE00CECFCE00C6CFC600C6CFC600D6D7D600A5A6A500BDC7BD00A59E
      A500CECFCE009CA69C00CECFCE007B717B0000000000F7EFE700BD9E5200B58E
      4200B58E4200B58E4200B58E3900C69E4200D6AE4A00EFC75200F7DF5A00F7DF
      6B00EFC76300F7EFD6000000000000000000C6C7C600C6C7C600C6C7C600C6C7
      C600000000000000FF00FF0000000000FF00FF0000000000FF00FF0000000000
      0000C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      000000000000000000000000000000000000ADAEAD00D6D7D600ADAEAD00D6D7
      D600A5AEA500C6C7C600ADAEAD00CECFCE00D6D7D600C6C7C600C6C7C600BDC7
      BD00CECFCE00BDBEBD00CECFCE007B717B00FFFFF700CEB67B00B58E3900B58E
      4200B58E4200BD964200C6A66300CEB67B00DEBE7300EFC76300F7DF6B00FFEF
      A500F7DF6B00EFCF7300FFFFF70000000000C6C7C600C6C7C600C6C7C600C6C7
      C60000000000FF0000000000FF00FF0000000000FF00FF0000000000FF000000
      0000C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A5AEA500D6D7D600BDC7BD00D6D7
      D600C6BEC600BDBEBD00BDBEBD00CECFCE00D6D7D6006BBE730073C77B00A59E
      A500CECFCE006351D600CECFCE0073717300E7DFCE00B58E3900B58E4200B58E
      4200BD9E5200DED7AD00F7F7F700FFFFFF00EFF7FF00F7EFBD00FFEFA500F7DF
      6B00F7CF5200DEBE4A00EFDFBD00FFF7FF00C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C60000000000FF0000000000FF00FF0000000000FF0000000000C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      000000000000000000000000000000000000ADA6AD00D6D7D600A5A6A500D6D7
      D600A59EA500BDBEBD009CA69C00CECFCE00D6D7D600A5A6A500BDC7BD00A59E
      A500CECFCE009CA69C00CECFCE006B696B00CEB68400BD964200B58E4200BD96
      4200E7D7B500F7FFFF00E7E7E700CED7D600E7E7E700EFF7FF00F7EFBD00EFC7
      6300EFC75200E7BE5200DEBE6B00EFE7EF00C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600000000000000FF00FF0000000000FF00FF00000000000000C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      00000000000000000000000000000000FF000000FF00C6C7C600000000000000
      0000000000000000000000000000000000009CA69C00D6D7D600C6C7C600D6D7
      D600F79E2100F79E2100F79E2100F79E2100F79E2100F79E2100F79E2100F7A6
      2100CECFCE00BDBEBD00CECFCE0063696300C6A66300C69E4200C69E4200CEAE
      6300F7F7F700E7E7E700C6C7BD00BDBEBD00BDBEBD00E7E7E700F7F7F700DEBE
      7300D6AE4A00D6B64A00CEA64200EFE7E700C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600000000000000FF00FF00000000000000C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      0000000000000000000000000000C6C7C6000000FF000000FF00C6C7C6000000
      0000000000000000000000000000000000009CA69C00DED7DE006BBE7300DED7
      DE00F79E1000F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7AE
      3100CECFCE006351D600CECFCE0063616300C6A66300D6B64A00D6AE4A00DEBE
      7300FFFFFF00D6D7CE00BDBEBD00000000000000000000000000000000000000
      00000000000000000000C69E4200E7DFCE00C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C60000000000FF0000000000FF0000000000C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C6000000000000000000000000000000
      000000000000000000000000000000000000C6C7C6000000FF000000FF00C6C7
      C600000000000000000000000000000000009C9E9C00DEDFDE00ADAEAD00DEDF
      DE00F7962100F7D79C00F7D7A500F7DF9C00F7D79C00F7D7A500F7DF9C00F7AE
      3100CECFCE00ADAEAD00CECFCE0063616300CEAE6300E7BE5200EFC75200EFCF
      7300F7F7F700E7E7E700BDBEBD00000000000000DE000000DE000000DE000000
      DE0000000000BD964200BD964200E7DFCE008486840084868400848684008486
      8400848684008486840084868400000000000000000084868400848684008486
      8400848684008486840084868400848684000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C7C6000000FF00C6C7
      C60000000000000000000000000000000000949E9400DEDFDE00BDC7BD00DEDF
      DE00F79E2100F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7DFA500F7A6
      2100D6CFBD00BDC7BD00D6CFBD005A595A00DECF8400E7BE5200F7DF5A00F7DF
      6B00F7EFBD00EFF7FF00DEDFDE00000000000000DE000000FF000000FF000000
      0000B58E4200B58E4200CEAE7300EFE7EF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF00C6C7C6000000000000000000000000000000FF000000
      FF000000000000000000000000000000000094969400DEDFDE00A5A6A500DEDF
      DE00F79E1000F7D79C00F7D7A500F7DF9C00F7D79C00F7D7A500F7DF9C00F7AE
      3100D6CFBD00A5A6A500D6CFBD005A595A00F7E7CE00DEBE4A00F7DF5A00FFE7
      7300FFEFA500F7EFBD00F7F7F700000000000000DE000000FF002120FF002120
      FF0000000000B58E3900E7D7B500EFE7F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C7C6000000FF00C6C7C600C6C7C60000000000C6C7C6000000FF000000
      FF000000000000000000000000000000000094969400E7E7E700C6C7C600E7E7
      E700F7962100F7D79C00F7D7A500F7DF9C00F7D79C00F7D7A500F7DF9C00F7AE
      3100DECFBD00C6C7C600DECFBD0052515200FFFFFF00E7CF8400F7DF6B00FFF7
      A500FFE77300F7DF6B00EFCF7B00000000000000DE00000000002120FF004A49
      FF004A49FF0000000000FFF7EF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C7C6000000FF000000FF000000FF000000FF000000FF000000FF00C6C7
      C60000000000000000000000000000000000948E9400E7E7E7006BBE7300E7E7
      E700F7961000F79E2900F79E1000F7962100F79E2900F79E1000F7962100F79E
      2900D6D7C6006BBE7300D6D7C6005251520000000000FFF7EF00EFCF7B00F7DF
      6B00F7DF5A00F7DF5A00EFC752000000000000000000BD964200000000004A49
      FF006B69FF006B69FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C600C6C7C6000000FF000000FF00C6C7C600C6C7C6000000
      0000000000000000000000000000000000008C968C00DED7DE00E7DFE700DED7
      DE00E7E7E700DEE7DE00DEDFDE00DEDFDE00E7E7E700E7DFE700E7E7E700E7E7
      E700DEDFDE00DEDFDE00DEDFDE008C868C000000000000000000F7EFE700E7CF
      8400DEBE4A00E7BE5200E7BE520000000000C69E4200BD964200B58E39000000
      00006B69FF008C8EFF008C8EFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C7C600A5AEA500A5A6A500949E
      94009C969C00949E94009C969C00A5AEA500A5A6A500949E9400A5AEA500A5A6
      A500949E94009C969C00949E94009C969C0000000000FFFFFF0000000000FFFF
      F700F7E7CE00DECF8400CEAE6300C6A65200C6A65200CEAE7300E7D7B500FFF7
      EF00000000008C8EFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFF700FFFFF700FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF00000000000000C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600000000000000FF000000000000000000FF000000FF000000FF00
      000000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600000000000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF00000000000000C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600000000000000FF000000FF00FF000000FF000000FF000000FF00
      000000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600000000000000
      FF000000FF000000FF000000FF000000FF000000000000000000C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF0000000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600000000000000FF000000FF000000FF00FF000000FF000000FF000000FF00
      0000FF00000000000000C6C7C600C6C7C600C6C7C600000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF0000000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600000000000000FF000000FF000000FF00FF000000FF000000FF000000FF00
      0000FF00000000000000C6C7C600C6C7C600C6C7C600000000000000FF000000
      FF000000FF000000FF000000FF000000FF0000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C60000FFFF0000FFFF0000FFFF00C6C7C600C6C7
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF0000000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600000000000000FF000000FF000000FF00FF000000FF000000FF000000FF00
      0000FF00000000000000C6C7C600C6C7C600C6C7C600000000000000FF000000
      FF000000FF000000FF000000FF000000FF0000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600848684008486840084868400C6C7C600C6C7
      C60000000000C6C7C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      000000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6000000
      00000000FF000000FF000000FF000000FF00FF000000FF000000FF000000FF00
      0000FF000000FF00000000000000C6C7C600000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C600C6C7C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      000000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6000000
      00000000FF000000FF000000FF000000FF0000000000FF000000FF000000FF00
      0000FF000000FF00000000000000C6C7C600000000000000FF000000FF000000
      FF000000FF000000FF00000000000000000000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6000000
      0000C6C7C60000000000C6C7C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF000000FF000000FF00
      000000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6000000
      00000000FF000000FF000000FF0000000000C6C7C6000000000000000000FF00
      0000FF000000FF00000000000000C6C7C600000000000000FF000000FF000000
      FF000000000000000000C6C7C600C6C7C6000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C7
      C60000000000C6C7C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C7C6000000000000000000FF00
      0000FF00000000000000C6C7C600C6C7C600C6C7C600C6C7C600000000000000
      FF000000FF000000000000000000C6C7C600C6C7C600C6C7C600C6C7C6000000
      000000000000FF000000FF000000000000000000FF000000FF00000000000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000C6C7C60000000000C6C7C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008486840084868400848684000000
      0000000000000000000084868400848684008486840084868400000000000000
      0000000000008486840084868400848684008486840084868400848684008486
      8400848684000000000000000000000000000000000000000000848684008486
      8400848684008486840084868400848684000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008486840084868400848684008486
      840084868400848684008486840084868400848684008486840000000000C6C7
      C60084868400C6C7C60000000000C6C7C600000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000C6C7C60000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000FF000000FF000000000000FFFF0000FFFF0000FFFF
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008486840084868400848684008486
      8400848684008486840084868400848684008486840084868400848684000000
      000084868400000000008486840084868400000000000000000000FFFF000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      0000C6C7C60000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000FF000000FF000000000000FFFF0000FFFF0000FFFF
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000000000000000000084868400848684008486840000000000000000000000
      000084000000840000008400000084000000000000000000000000FFFF000000
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF000000
      0000C6C7C60000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000FF000000FF000000000000FFFF0000FFFF0000FFFF
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000086840084000000840000008400000084000000008684000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000FF0000008400000000000000000000008486840000000000000000000000
      000084000000000000000000000000000000000000000000000000FFFF000000
      FF0000FFFF0000000000000000000000000000FFFF000000000000FFFF000000
      0000C6C7C60000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000FF000000FF000000000000FFFF0000FFFF0000FFFF
      0000000000000000FF0000000000000000000000000000000000000000000086
      840000FFFF008400000084000000C6C7C600C6C7C600840000008400000000FF
      FF00008684000000000000000000000000000000000000000000000000008400
      000084000000FF00000084000000000000000000000000000000000000000000
      000084000000000000000000000000000000000000000000000000FFFF000000
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF000000
      0000C6C7C60000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000FF000000FF000000000000FFFF0000FFFF0000FFFF
      0000000000000000FF00000000000000000000000000000000008486840000FF
      FF008486840084000000C6C7C600FF000000FF000000C6C7C600840000008486
      840000FFFF000086840000000000000000000000000000000000000000008400
      0000FF00000084000000FF000000000000000000000000000000000000000000
      000084000000000000000000000000000000000000000000000000FFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000000000FFFF000000
      0000C6C7C60000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000FF000000FF000000000000FFFF0000FFFF0000FFFF
      0000000000000000FF000000000000000000000000000000000000FFFF0000FF
      FF00848684008486840084868400FF000000FF00000084868400848684008486
      840000FFFF0000FFFF0000000000000000000000000000000000000000008400
      000084000000FF000000840000000000000000000000FFFF000000000000FFFF
      000084000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF000000FF000000000000FFFF0000FFFF0000FFFF
      00000000000000000000000000000000000000000000000000008486840000FF
      FF008486840084000000C6C7C600FF000000FF000000C6C7C600840000008486
      840000FFFF000086840000000000000000000000000000000000000000008400
      0000FF00000084000000FF00000000000000FFFF000000000000FFFF00000000
      000084000000000000000000000000000000000000000000000000FFFF000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      00000000000000FF000000FF000000FF000000000000FFFF0000FFFF0000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000008684008400000084000000C6C7C600C6C7C60084000000840000000086
      8400008684000000000000000000000000000000000000000000000000008400
      000084000000FF000000840000000000000000000000FFFF000000000000FFFF
      000084000000000000000000000000000000000000000000000000FFFF000000
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      00000000000000FF000000FF000000FF000000000000FFFF0000FFFF0000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000FF00000084000000FF00000000000000FFFF000000000000FFFF00000000
      000084000000000000000000000000000000000000000000000000FFFF000000
      FF0000FFFF0000000000000000000000000000FFFF000000000000FFFF000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      00000000000000FF000000FF000000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      000084000000000000000000000000000000000000000000000000FFFF000000
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      00000000000000FF000000FF000000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000000000FFFF000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000860000008600000086000000860000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000030000000410000005100000051000000410000003000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000030000000410000005100000051000000410000003000000000
      000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000041
      00000041000000790800009E0800009E0800009E0800009E0800007908000049
      0000004900000000000000000000000000000000000000000000000000000041
      00000041000000790800009E0800009E0800009E0800009E0800007908000049
      000000490000000000000000000000000000FFFF000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000000000000000000000000000000000000000000000004908000061
      080000A6080000A6080000A60800009E0800009E080000A6080000A6080000A6
      0800006900000030000000000000000000000000000000000000004908000061
      080000A6080000A6080000A60800009E0800009E080000A6080000A6080000A6
      080000690000003000000000000000000000FFFF00000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000000000000000C6C7C6008486840000000000000000000000
      00000000000000000000000000000000000000000000004908000069100008AE
      180000A61000009E0800009E0800009E0800009E0800009E0800009E080000A6
      080000A6080000690000004900000000000000000000004908000069100008AE
      180000A61000009E0800009E0800009E0800009E0800009E0800009E080000A6
      080000A60800006900000049000000000000FFFF000000000000FFFF00000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000000000000000C6C7C60000FFFF0000000000000000000000
      000000000000000000000000000000000000000000000049080010AE310008AE
      290008A61800009E08004AC75200EFFFEF00D6F7DE0010AE2100009E0800009E
      0800009E080000A608000049000000000000000000000049080010AE310008AE
      290008A61800009E080010AE2100D6F7DE00EFFFEF004AC75200009E0800009E
      0800009E080000A608000049000000000000FFFF0000FFFF0000FFFF0000FFFF
      000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000000084868400C6C7C60000FFFF0000868400000000000000
      000000000000000000000000000000000000005100000886210010B6420008AE
      290000A6100042BE4A00F7FFF700FFFFFF0084D78C0000A61000009E0800009E
      0800009E080000A608000079080000410000005100000886210010B6420008AE
      290000A61000009E080000A6100084D78C00FFFFFF00F7FFF70042BE4A00009E
      0800009E080000A608000079080000410000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      00000000000000000000C6C7C600C6C7C60000FFFF0000868400000000000000
      0000000000000000000000000000000000000051000010A6420018B64A0010AE
      310042BE4A00F7FFF700FFFFFF0073D77B00009E0800009E0800009E0800009E
      0800009E080000A6080000960800004100000051000010A6420018B64A0010AE
      310000A61000009E0800009E0800009E080073D77B00FFFFFF00F7FFF70039BE
      4A00009E080000A608000096080000410000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      00000000000000000000FFFFFF00C6C7C6000086840000FFFF00000000000000
      0000000000000000000000000000000000000069080021B6520018B6520073D7
      9400F7FFF700FFFFFF00F7FFFF00B5EFC600B5E7BD00ADE7B500ADE7B500ADE7
      B500B5E7B50000A60800009E0800004900000069080021B6520018B65200BDEF
      CE00BDEFC600B5EFC600BDEFC600B5EFC600B5EFC600F7FFF700FFFFFF00EFFF
      EF0052C75A0000A60800009E0800004900000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C600C6C7C60000FFFF0000FFFF0000FFFF00008684000000
      0000000000000000000000000000000000000871100039BE6B0029BE5A00BDEF
      CE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000A60800009E0800005100000871100039BE6B0029BE5A00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00ADE7B50000A60800009E0800005100000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000079080052C77B0042C7730029BE
      5A00A5E7BD00FFFFFF00FFFFFF0084DFA50039BE5A0042C75A0042BE5A0042BE
      5A0042BE520000A61800009E0800004100000079080052C77B0042C7730052C7
      7B0052CF840052CF840052CF84004AC773007BD79400FFFFFF00FFFFFF0094DF
      A50018AE310000A61800009E0800004100000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      000000000000000000000000000000000000007908004ABE6B0084DFA50021B6
      520018B64A0094DFB500FFFFFF00E7F7EF004AC7730010AE310008AE310008AE
      290008A6210000A61000008E080000410000007908004ABE6B0084DFA50021B6
      520018B64A0021B6520021B6520052CF7B00E7F7EF00FFFFFF008CDF9C0010AE
      290008A6210000A61000008E0800004100000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C600FFFFFF0000FFFF0000FFFF0000FFFF00008684000000
      0000000000000000000000000000000000000000000021A63100ADE7C6006BD7
      8C0010AE4A0018B64A008CDFAD00FFFFFF00F7FFF70029BE520010AE310008A6
      290008A6210008AE180000690800000000000000000021A63100ADE7C6006BD7
      8C0010AE4A0018B64A0039BE6B00F7FFF700FFFFFF0084DF9C0010AE310008A6
      290008A6210008AE180000690800000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C7C600C6C7C60000FFFF0000868400000000000000
      0000000000000000000000000000000000000000000021A6310052C77300C6F7
      DE0063CF8C0021B6520018B64A0094DFB500ADE7C60018B64A0010AE390010AE
      310008B62900089E180000690800000000000000000021A6310052C77300C6F7
      DE0063CF8C0021B6520021B65200ADE7C60094DFB50018B64A0010AE390010AE
      310008B62900089E180000690800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000109E21006BCF
      8C00D6F7E7009CE7B50052C77B0039BE630029BE5A0031BE630029BE5A0021BE
      4A0010A631000061080000000000000000000000000000000000109E21006BCF
      8C00D6F7E7009CE7B50052C77B0039BE630029BE5A0031BE630029BE5A0021BE
      4A0010A631000061080000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004ABE
      63004ABE63009CE7B500BDEFD600A5E7C60094E7B5007BDF9C004AC77B00189E
      3900189E39000000000000000000000000000000000000000000000000004ABE
      63004ABE63009CE7B500BDEFD600A5E7C60094E7B5007BDF9C004AC77B00189E
      3900189E39000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000018A6310039B6520042BE630042BE630029A64A00108E29000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000018A6310039B6520042BE630042BE630029A64A00108E29000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      00000000000000000000C6C7C6000000000000000000C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000FF000000FF00000000000000
      000000000000C6C7C60000000000000000000000000000000000C6C7C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      000000000000000000000000000000000000FFFF000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000000000000000000000000000FFFF0000000000000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF00848684000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000FFFF000000000000000000000000FF000000
      000000000000000000000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF008486840000FFFF000000000000000000000000000000
      000000000000000000000000000000000000FFFF000000000000FFFF00000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      000000000000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      000000000000FFFF000000000000FFFF000000000000000000000000FF000000
      FF000000000000000000000000000000FF000000FF0000000000000000000000
      000000000000C6C7C60000000000000000000000000000000000000000000000
      000000FFFF008486840000FFFF00848684000000000000000000000000000000
      000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF
      000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      0000FFFF0000FFFF0000FFFF0000FFFF000000000000C6C7C600000000000000
      FF000000FF000000000000000000000000000000FF000000FF00000000000000
      00000000000000000000C6C7C6000000000000000000000000008486840000FF
      FF008486840000FFFF008486840000FFFF000000000000000000000000000000
      000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      00000000FF000000FF000000000000000000000000000000FF000000FF000000
      000000000000000000000000000000000000000000000000000000FFFF008486
      840000FFFF008486840000FFFF00848684000000000000000000000000000000
      000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000FFFF0000FFFF000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000FF000000FF000000000000000000000000000000FF000000
      FF000000000000000000000000000000000000000000000000008486840000FF
      FF008486840000FFFF008486840000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C7C600000000000000
      000000000000000000000000FF000000FF000000000000000000000000000000
      FF000000000000000000C6C7C60000000000000000000000000000FFFF008486
      840000FFFF008486840000FFFF00848684000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000C6C7C6000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008486840000FFFF008486840000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF00000000000000
      000000000000C6C7C60000000000000000000000000000000000000000000000
      0000000000008486840000FFFF00848684000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000C6C7
      C600000000000000000000000000000000000000000000000000000000000000
      0000C6C7C6000000000000000000000000000000000000000000000000000000
      000000000000000000008486840000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C6000000000000000000000000000000000000000000C6C7
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848684000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C7C6000000000000000000C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF0000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      000000000000FFFF000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C7C60000FF
      000000000000000000000000000000000000FFFF000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000FFFF00000000000000000000000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C7C60000FF0000C6C7
      C6000000000000000000000000000000000000000000FFFF0000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      000000000000FFFF00000000000000000000000000000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF0000C6C7C60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C7C60000FF0000C6C7C60000FF
      000000000000000000000000000000000000FFFF000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000FFFF00000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C60000FF0000C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C7C60000FF0000C6C7C60000FF0000C6C7
      C6000000000000000000000000000000000000000000FFFF0000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000000000000000000000000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000000000008486840000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7
      C600000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000000000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000000000008486840000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7
      C6000000000000000000000000000000000000000000FFFF0000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF0000C6C7C60000FF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FF0000C6C7C60000FF0000C6C7C60000FF
      000000000000000000000000000000000000FFFF000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000FFFF00000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C60000FF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF0000C6C7C60000FF0000C6C7
      C6000000000000000000000000000000000000000000FFFF0000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      000000000000FFFF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF0000C6C7C60000FF
      000000000000000000000000000000000000FFFF000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C60000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF0000C6C7
      C6000000000000000000000000000000000000000000FFFF0000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000000000000000000000000000000000FFFF000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF0000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      000000000000FFFF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848684000000000084868400000000008486840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008486
      8400848684008486840084868400000000000000000000000000848684008486
      8400848684008486840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBE
      BD00BDBEBD00BDBEBD00BDBEBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000840000008400000084868400000000000000000084000000840000008400
      0000840000008486840000000000000000000000000000000000848684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00BDBEBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008486
      8400840000008400000084868400000000000000000084000000848684008400
      0000840000008486840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDBEBD00BDBE
      BD00FFFF0000FFFF0000BDBEBD0000000000000000007B797B00BDBEBD00BDBE
      BD00BDBEBD00BDBEBD00BDBEBD00000000000000000000000000BDBEBD00BDBE
      BD00BDBEBD00BDBEBD00BDBEBD00000000000000000000000000848684008400
      0000848684008400000084868400000000000000000084868400840000008486
      8400840000008486840000000000000000000000000000000000848684000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7900007B7900007B79
      00007B7900007B7900007B7900007B7900007B7900007B79000000000000BDBE
      BD00FFFFFF00FFFFFF00BDBEBD0000000000000000007B797B00FFFFFF000000
      0000000000000000000000000000BDBEBD0000000000BDBEBD00000000000000
      0000000000000000000000000000000000000000000000000000840000008486
      8400840000008400000084868400000000000000000084000000848684008400
      0000840000008486840000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000007B790000FFFF0000BDBEBD00FFFF
      0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF000000000000BDBE
      BD00BDBEBD00FFFF0000BDBEBD0000000000000000007B797B00FFFFFF007B79
      7B00BDBEBD00BDBEBD00BDBEBD007B00000000000000FFFFFF00BDBEBD00BDBE
      BD00BDBEBD0000000000FFFF0000000000000000000000000000848684008400
      0000848684008400000084868400000000000000000084868400840000008486
      8400840000008486840000000000000000000000000000000000848684000000
      00000000000000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600000000007B790000FFFF0000FFFF0000BDBE
      BD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD007B7900000000
      0000BDBEBD00FFFFFF00BDBEBD0000000000000000007B797B00FFFFFF007B79
      7B00FFFFFF007B0000007B000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00BDBEBD00000000007B790000000000000000000000000000840000008486
      8400840000008400000084868400000000000000000084000000848684008400
      0000840000008486840000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000007B790000FFFF0000FFFF
      0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD000000
      0000BDBEBD00FFFF0000BDBEBD0000000000000000007B797B00FFFFFF007B79
      7B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000007B000000FFFFFF00FFFF
      FF00BDBEBD000000000000FFFF00000000000000000000000000848684008400
      0000848684008400000084868400000000000000000084868400840000008486
      84008400000084868400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000007B790000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B797B00FFFFFF007B79
      7B00FFFFFF00FFFFFF00FFFFFF007B00000000000000FFFFFF007B0000007B00
      0000BDBEBD000000000000797B00000000000000000000000000840000008486
      8400840000008400000084868400000000000000000084000000848684008400
      00008400000084868400000000000000000000000000C6C7C600C6C7C600C6C7
      C600C6C7C60000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF000000000000000000000000007B797B00FFFFFF007B79
      7B00FFFFFF007B0000007B000000FFFFFF00000000007B000000FFFFFF00FFFF
      FF00BDBEBD0000000000FFFFFF00000000000000000000000000848684008400
      0000848684008400000084868400000000000000000084868400840000008486
      84008400000084868400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000007B797B00FFFFFF007B79
      7B00FFFFFF00FFFFFF00FFFFFF007B00000000000000FFFFFF007B0000007B00
      0000BDBEBD0000000000BDBEBD00000000000000000000000000840000008486
      8400840000008400000084868400000000000000000084000000848684008400
      00008400000084868400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000007B797B00FFFFFF007B79
      7B00FFFFFF007B0000007B000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00BDBEBD00000000000000FF00000000000000000000000000848684008400
      0000848684008400000084868400000000000000000084868400840000008486
      84008400000084868400000000000000000000000000C6C7C600C6C7C600C6C7
      C600C6C7C60000000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000007B797B007B797B007B79
      7B00FFFFFF00FFFFFF00FFFFFF007B797B00000000007B797B00FFFFFF00FFFF
      FF00BDBEBD000000000000007B00000000000000000000000000840000008486
      8400840000008400000084868400000000000000000084000000848684008400
      00008400000084868400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000007B79
      7B007B797B007B797B007B797B000000000000000000000000007B797B007B79
      7B007B797B000000000000000000000000000000000000000000848684008400
      0000848684008400000084868400000000000000000084868400840000008486
      84008400000084868400000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008486
      8400840000008400000000000000000000000000000084000000848684008400
      00008400000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      000000000000000000000000000084868400C6C7C60000000000000000000000
      000000000000C6C7C600C6C7C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000086840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000086840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000086840000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000086840000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000008684000086
      8400008684000086840000868400008684000086840000868400008684000086
      8400008684000086840000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000000000000000
      0000008684000086840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000FFFFFF000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000008684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000086840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000008684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000086840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000008684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000086840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000008684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000086840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000008684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000008684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      00000000000000000000007900000079000000790000007900007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      000000000000000000000079000000FF000000FF0000007900007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848684000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848684000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000079000000FF000000FF0000007900007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008486
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008486
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B79
      7B007B797B007B797B000079000000FF000000FF0000007900007B797B007B79
      7B007B797B007B797B007B797B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848684000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000007900000079
      000000790000007900000079000000FF000000FF000000790000007900000079
      000000790000007900007B797B00000000000000000000000000848684000000
      0000C6C7C600FFFFFF00FFFFFF00C6C7C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848684000000
      0000C6C7C600FFFFFF00FFFFFF00C6C7C6000000000000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      00000000000084868400848684000000000000000000000000000079000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF0000007900007B797B0000000000000000000000000000000000C6C7
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C7C60000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C7
      C600FFFFFF000000000000000000FFFFFF00C6C7C60000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      00000000000084868400848684000000000000000000000000000079000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF0000007900007B797B00000000000000000000000000C6C7C600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000C6C7C600FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00C6C7C600000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000007900000079
      000000790000007900000079000000FF000000FF000000790000007900000079
      0000007900000079000000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      000000000000000000000079000000FF000000FF0000007900007B797B000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      000000000000000000000079000000FF000000FF0000007900007B797B000000
      0000000000000000000000000000000000000000000000000000C6C7C600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000C6C7C600FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000079000000FF000000FF0000007900007B797B000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C7C60000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF000000000000000000FFFFFF00C6C7C60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000790000007900000079000000790000000000000000
      0000000000000000000000000000000000000000000000000000848684000000
      0000C6C7C600FFFFFF00FFFFFF00C6C7C6000000000084868400000000000000
      0000000000000000000000000000000000000000000000000000848684000000
      0000C6C7C600FFFFFF00FFFFFF00C6C7C6000000000084868400000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000848684008486840000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000007B797B00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBE
      BD00BDBEBD00BDBEBD00BDBEBD000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00BDBEBD0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B0000007B0000007B00
      00007B0000007B0000007B0000007B0000007B0000007B0000007B0000007B00
      00007B0000007B0000007B00000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDBEBD00BDBE
      BD00FFFF0000FFFF0000BDBEBD00000000007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B00000000007B000000000000007B00
      0000000000007B000000000000007B000000000000007B000000000000007B00
      0000000000007B0000007B000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000007B7900007B7900007B79
      00007B7900007B7900007B7900007B7900007B7900007B79000000000000BDBE
      BD00FFFFFF00FFFFFF00BDBEBD00000000007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B00000000007B0000007B0000007B00
      00007B0000007B0000007B0000007B0000007B0000007B0000007B0000007B00
      00007B0000007B0000007B0000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000007B790000FFFF0000BDBEBD00FFFF
      0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF000000000000BDBE
      BD00BDBEBD00FFFF0000BDBEBD0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000007B790000FFFF0000FFFF0000BDBE
      BD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD007B7900000000
      0000BDBEBD00FFFFFF00BDBEBD0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000007B000000000000000000
      00000000000000000000000000000000000000000000000000007B0000007B00
      00007B0000007B0000007B0000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000007B790000FFFF0000FFFF
      0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD000000
      0000BDBEBD00FFFF0000BDBEBD0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000007B000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B00
      00007B0000007B0000007B0000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000007B790000FFFF0000BDBE
      BD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF00007B79
      000000000000FFFFFF00BDBEBD0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000007B0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00007B0000007B0000007B0000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000000000000000000000007B790000FFFF
      0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD00BDBE
      BD0000000000BDBEBD00BDBEBD00000000007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B0000000000000000007B0000000000
      0000000000000000000000000000000000000000000000000000000000007B00
      0000000000007B0000007B0000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000000000000000000000007B790000FFFF
      0000FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBE
      BD007B79000000000000BDBEBD00000000007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B000000000000000000000000007B00
      00007B00000000000000000000000000000000000000000000007B0000000000
      000000000000000000007B000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000007B79
      0000FFFF00000000000000000000000000000000000000000000BDBEBD00FFFF
      0000BDBEBD0000000000BDBEBD0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B00000000000000000000000000000000000000
      0000000000007B0000007B0000007B0000007B0000007B000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000007B79
      0000FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBE
      BD00BDBEBD007B790000000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00007B790000FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF0000BDBEBD00FFFF
      0000FFFF0000BDBEBD007B790000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00007B790000FFFF00007B797B00BDBEBD007B797B00BDBEBD007B797B00BDBE
      BD007B797B00BDBEBD007B790000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B7900007B7900007B7900007B7900007B7900007B7900007B79
      00007B7900007B79000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B0000000000000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B00000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B00000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B00000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B000000000000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B00000000007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF007B797B00000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B00000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B00000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B00000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B0000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B00000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B00000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B00000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000FFFFFF0000000000000000000000
      00000000000000000000FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000FFFFFF0000000000000000000000
      00000000000000000000FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000FFFFFF0000000000000000000000
      00000000000000000000FFFFFF007B797B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF0000000000FFFFFF00FFFFFF007B797B00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      000000000000FFFFFF00FFFFFF007B797B007B797B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF007B797B00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B0000000000000000007B797B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B797B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B00000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B00000000000000000000000000000000007B797B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B797B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000007B79
      7B007B797B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B79
      7B007B797B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000797B000079
      7B0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000797B0000797B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000797B000079
      7B0000797B0000797B0000797B0000797B0000797B0000797B0000797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000797B0000FFFF0000797B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B0000000000000000000000000000FFFF00000000000079
      7B0000797B0000797B0000797B0000797B0000797B0000797B0000797B000079
      7B0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000797B0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00007B797B0000000000000000000000000000000000FFFFFF0000FFFF000000
      000000797B0000797B0000797B0000797B0000797B0000797B0000797B000079
      7B0000797B00000000000000000000000000000000000000000000000000FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000797B0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B79
      7B00000000000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF000000000000797B0000797B0000797B0000797B0000797B0000797B000079
      7B0000797B0000797B000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000797B0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B797B000000
      00000000000000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000797B0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00007B797B007B797B007B797B007B797B000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000797B0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B79
      7B00BDBEBD00BDBEBD00BDBEBD00BDBEBD007B797B0000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000797B0000FFFF0000000000000000000000
      00000000000000000000000000000000000000000000000000007B797B00BDBE
      BD00BDBEBD00FFFFFF00BDBEBD00FFFFFF00BDBEBD007B797B00000000000000
      0000000000000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000797B0000FFFF0000000000000000000000
      00000000000000000000000000000000000000000000000000007B797B00BDBE
      BD00FFFFFF00BDBEBD00FFFFFF00BDBEBD00BDBEBD007B797B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF0000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000797B0000FFFF0000000000000000000000
      00000000000000000000000000000000000000000000000000007B797B00BDBE
      BD00BDBEBD00FFFFFF00BDBEBD00FFFFFF00BDBEBD007B797B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000797B0000797B0000FFFF0000797B00000000000000
      00000000000000000000000000000000000000000000000000007B797B00BDBE
      BD00FFFFFF00BDBEBD00FFFFFF00BDBEBD00BDBEBD007B797B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000797B0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000007B79
      7B00BDBEBD00BDBEBD00BDBEBD007B797B007B797B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000797B0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      00007B797B007B797B007B797B007B797B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000797B0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000000100000100010000000000000800000000000000000000
      000000000000000000000000FFFFFF00FFFFE007FFFF0000C007E00700000000
      C007E00700000000C007E007000000000007E007000000000007E00700000000
      C007E00700000000C000F00F00000000C000F00F00000000C007F00F00000000
      0007F81F000000000007F81F00000000C007F81F00000000C007F81F00000000
      C007F81F00000000FFFFFFFFFFFF0000FFFFFFFFE03FFFFFFFFF007F803F0000
      FFFF7F7C003F800120004178007F8001E000417100BF800120004023071F8001
      E00040070C0FF0012000400F8807F001E00040078000F00120004007C000FE01
      E0004007E000FE0120000007F000FE01E000C007F800FFC1FFFFE00FFC00FFC1
      FFFFF01FFE00FFC1FFFFF83FFF00FFFFF000FE7FFFEFFFFFEFFFF81FFFCFC003
      EFFFE007E007DC3B8001C003C063DC3B0000C003BAB9DC3B0000C003AE69DC3B
      0000C003BBB9DC3B0000C003AE69DFFB0000C003BA398FF10000C003AE09C7E3
      0000C003B999E3C70000C003A809F1870000E007B819F8070000F00FAEE9FC27
      0000FC3FBBBBFE678001FE7FC007FFFF8001FFFFFFFFA0030000FFFF00018007
      0000FE7F000080030000FE7F000000010000FFFF000000000000FE7F00000000
      0000FE3F000000000000FE1F000000000000FF0F000000000000FF8F00000000
      FFFFF1CF00000000FC3FF08F00000000FDBFF00F00008001FDBFF81F0000C000
      FC3FFFFF0000A001FFFFFFFFFFFFF81BFFFFFFFF80018001C007FFFF00000000
      8003C803000000000001C803000000000001FFFF000000000001FFFF00000000
      0000FFFF000000000000C803000000008000C80300000000C000FFFF00000000
      E001FFFFFFFFFFFFE007FFFFE187F83FF007C803EDB7FBBFF003C803EDB7FBBF
      F803FFFFE187F83FFFFFFFFFFFFFFFFFFFFFFFFFFFFF80070000FFFF00228003
      8001FFFF001480038001F00F007080038001C003E077800380018001E0F78003
      80010001E0F7800380010000E0A7800380010000E0578003F0078001E0A78003
      F0078003E0578003F0079003E0078003F07FD24BFFFF8003F07FF24FF81F8003
      FFFFFFFFF81F8003FFFFFFFFF81FE003FFFFFFFFF81FF81F0000FC3FE007E007
      0000FC3FC003C0030000CC3B800180010000E433800180010000F81700000000
      0000F81F000000000000F00F00000000F81FF00F00000000F81F900900000000
      F81FF00F00000000F81FF00F80018001F81FF00F80018001F81FE817C003C003
      FDBFDFFBE007E007FFFFFFFFF81FF81FFFFFFFFFFFFFFFFF7DBFFE7D00000000
      3BDFFC7300000000BFF7F84F00000000DCFFF07F00000000CE7B807100000000
      A73D804F00000000F39F807F00000000F9CF807FF81FF81FBCED804FF81FF81F
      DE7F8071F81FF81FFF3BF07FF81FF81FEFF7F84FF81FF81FFBEFFC73F81FF81F
      1DBFFE7DFDBFFDBFFFFFFFFFFFFFFFFFFFFFFFFFDFD7FFFFF9FFFFCFAFAB7C3F
      F8FFFF8F5FD733CFF87FFF0FAFABAFF7F83FFE0F5FD7DCFBF81FFC0FAFA7CE7B
      F80FF80F1FD9A73DF807F00F0000B39DF80FF80F1FD9B9CDF81FFC0FAFA7BCED
      F83FFE0F5FD7DE7BF87FFF0FAFABDF3BF8FFFF8F5FD7EFF7F9FFFFCFAFAFB3CF
      FBFFFFEF5FD71C3FFFFFFFFFAFABBFFFE03FF801FFFFE1C3DFDFF000FFFFC183
      DFDFF0008000C183DFDF80008000C183D80000008000C183D80000008000C183
      D80000008000C183000080008000C183000080008000C1830000F0018000C183
      0000F0018000C1830000F0018000C1830000F0018080C183001FF003E1C3C183
      001FF007FFFFC387001FF00FFFFFFFFFFFFFFFFF800FFFFF9E79FFFF800FC001
      9E79FFFF800380310000FFFF800380310000FFFF800080319E79FFFF80008001
      9E79FFFF800080019E79FFFF80008001FFFF9E7980008FF1FFFF9E7980008FF1
      FFFF9E7980008FF1FFFF000080008FF1FFFF0000E0008FF1FFFF9E79E0018FF5
      FFFF9E79F8038001FFFFFFFFF807FFFFFFFFFFFFFFFFFFFFFE1FFFFBFFFB9E79
      FC1FFFF1FFF19E79FC1FFFE3FFE30000FC1FFFC7FFC70000E001F08FF08F9E79
      C001C01FC01F9E79C001C03FC03F9E79C001801F801F9E79C003801F801F9E79
      FC1F801F801F9E79FC1F801F801F0000FC1FC03FC03F0000FC3FC03FC03F9E79
      FFFFF0FFF0FF9E79FFFFFFFFFFFFFFFFF81FFFFFF807F801E007FFFFF003F000
      C003FFFFE001F00080018001C00180000000AAA9800100000000800100010000
      0000FFFF000000000000BFC1000080000000BFE1000080000000DFF10000C000
      0000DFE90000C0000000E7DD8000E0008001F83FE000E000C003FFFFE001F000
      E007FFFFE00FF001F81FFFFFF03FF803F81FF81FF81FF81FE007E007E007E007
      C003C003C003C003800180018001800100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000008001800180018001C003C003C003C003
      E007E007E007E007F81FF81FF81FF81FFFFFF81FF81FF81FFFBFE007E007E007
      FF03C003C003C003FE03800180018001FC03000000000000FE03000000000000
      FF03000000000000FFBF000000000000FDFF000000000000C0FF000000000000
      C07F000000000000C03F000000000000C07F800180018001C0FFC003C003C003
      FDFFE007E007E007FFFFF81FF81FF81FFFFFFFFF8001FFFF001FFFFF8001FFFB
      000FC007E007FFF10007C007FE3FFFE30003C007FE3FFFC70001C007FE3FF08F
      0000C007FE3FE01F001FC007FE3FC03F001FC007FE3F801F001FC007FE3F801F
      8FF1C007FE3F801FFFF9C007F80F801FFF75C00FF007C03FFF8FC01FF007E07F
      FFFFC03FF007F0FFFFFFFFFFF80FFFFF00000000000000000000000000000000
      000000000000}
  end
  object TBMDIHandler1: TTBMDIHandler
    Toolbar = TBToolbar1
    Left = 120
    Top = 80
  end
  object ActionList1: TActionList
    Left = 40
    Top = 208
    object WindowCascade1: TWindowCascade
      Category = 'Window'
      Caption = #1050#1072#1089#1082#1072#1076#1086#1084
      Enabled = False
      Hint = 'Cascade'
    end
    object WindowTileHorizontal1: TWindowTileHorizontal
      Category = 'Window'
      Caption = #1057#1074#1077#1088#1093#1091' '#1074#1085#1080#1079
      Enabled = False
      Hint = 'Tile Horizontal'
    end
    object WindowTileVertical1: TWindowTileVertical
      Category = 'Window'
      Caption = #1057#1083#1077#1074#1072' '#1085#1072#1087#1088#1072#1074#1086
      Enabled = False
      Hint = 'Tile Vertical'
    end
    object WindowMinimizeAll1: TWindowMinimizeAll
      Category = 'Window'
      Caption = #1057#1074#1077#1088#1085#1091#1090#1100' '#1074#1089#1077
      Enabled = False
      Hint = 'Minimize All'
    end
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 32837
      OnExecute = Action1Execute
    end
    object Action2: TAction
      Caption = 'Action2'
      ShortCut = 16467
      OnExecute = SummFiles
    end
  end
  object TBBackground1: TTBBackground
    Bitmap.Data = {
      1E260100424D1E260100000000003600000028000000E80800000B0000000100
      180000000000E825010000000000000000000000000000000000E7E5E5E7E5E5
      E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5
      E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E6E6E9E7E7E9E7E7E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7EA
      E6E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE7E7EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE6E8EDE7E8EDE7E8EDE7E8EEE7E8
      EEE8E9EEE8E9EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE8E9EFE9E9EE
      E8E9EDE7E9ECE6E7ECE6E7EDE7E8EEE8E9F0EAEAEEEAEAEBE9EAECE9EBECE9EB
      ECE9EBECE9EBECE9EBEDEAECEDEAECEDE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      EDEAEBEDEBEBEDEBEBEDEBEBEDEBEBEEECECEEECECEEECECEEECECEEECECEEEC
      ECEEECECEEECECEEECECEEECECEEECECEEECECEEECECEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      ECECEEEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDF0EEEEF0EEEEEEEDEDEDEDED
      EDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDED
      EDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEDEEEEEDEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEFEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEE
      F0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0F0EEF0F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F2F0F2F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F1F0F3F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1EFF1F1F0F2F1
      F0F2F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1EFEFF1EFEFF0EFEDEFEEEDEFEEEDEFEEED
      EFEEEEF0EFEEEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEBEDECEBEDECEBEDECEBEDECEB
      EDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEDEBEDEDEAECED
      EAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAEC
      EDEAECEDEAECEDEAECEDEAECEDEAECECEAECECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE8EAEB
      E8EAEBE7E9EDE8E9EEE8E9EEE8E9EEE8E9EFE8EAEEE8E9EEE8E9EDE7E9EDE7E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE6E7E9E7E7E9E7E7E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E8E9E6E8E9E6E8E9E6E8E9E6E8E8E5E7
      E8E5E7E8E5E7E8E5E7E8E5E8E8E6E8E8E5E7E8E5E7E8E5E7E8E5E7E7E4E7E7E4
      E6E7E4E6E7E4E6E8E6E6E8E6E6E7E5E5E7E5E5E7E5E5E7E5E5E6E4E4E6E4E4E7
      E4E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E7
      E1E2E7E1E2E7E1E2E7E1E2E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1
      E6E0E1E6E0E1E7E1E2E7E1E2E7E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6DFE1E5DF
      E0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5
      DFE0E5DFE0E5DFE0E5DFE0E5DEE0E4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDF
      E4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE3DEDEE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E4DEDEE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE2DDDDE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DD
      DCE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE1DCDBE1DCDBE1DCDB
      E1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE1DCDBE1DCDBE2DDDCE2DD
      DCE1DCDBE1DCDBE0DCDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDADFDBDADFDAD9DFDAD9DFDAD9DFDAD9DFDAD9
      DFDAD9DFDAD9DFDAD9DFD9D9DED9D8DED9D8DED9D8DED9D8DED9D8DED9D8DED9
      D8DED9D8DDD9D8DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD7D7DCD7D6
      DCD7D6DCD7D6DCD7D6DCD7D6DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6
      D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DAD6D3D9D6D2DAD5D2DAD5D2DAD5D2DC
      D5D2DCD5D2DED5D2DED5D2DBD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DBD6D3DBD6D3DBD6D3DBD6D3DBD6D3DBD6D3DBD6D3DBD6D3DBD6
      D3DAD5D4DAD5D4DBD6D5DBD6D5DBD6D5DBD6D5DCD7D5DCD7D6DCD7D6DCD7D6DC
      D7D6DBD6D5DBD6D5DBD6D5DBD6D5DBD5D5DAD5D4DBD5D4DED4D4DED4D4DED4D4
      DED4D4DED4D4DED4D4DED4D4DED4D4DED5D4DDD6D3DDD6D3DDD5D3DCD5D2DCD5
      D2DCD5D2DCD5D2DCD5D2DBD5D2D9D6D2D9D6D2D9D6D2D9D6D2D9D6D2D9D6D2D9
      D6D2D9D6D2D9D5D2D9D4D1D9D4D1D9D4D1D9D4D1D9D4D1D9D4D1D9D4D1D9D4D1
      DCD4D1DDD4D1DDD4D1DDD3D0DCD3D0DCD3D0DBD2CFDBD2CFDBD2CFD9D3D0D8D3
      D0D8D3D0D8D3D0D8D3D0D8D3D0D4CFCCD5D0CCD5D0CDD5D0CDD5D0CDD6D1CDD6
      D1CED6D2CED6D3CFD6D3CFD6D3CFD6D3CFD6D3CFD6D3CFD6D3CFD6D3CFD7D3CF
      DBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2D0DAD3D0DAD3
      D0D8D3D0D8D3D0D8D4D0D7D4D0D7D4D0D5D4D0D8D4D2DAD5D4DAD5D4DAD5D4DA
      D5D4DAD5D4DAD5D4DAD5D4DAD5D4D9D5D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3
      D8D4D3D8D4D3D8D4D3DAD3D3DBD3D3DBD3D3DBD3D3DBD4D4DCD4D4DCD4D4DCD4
      D4DCD4D4DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5DA
      D5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D3DAD5D4DAD5D4DAD5D4DAD5
      D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DBD6D5DBD6D5DBD6D5DB
      D6D5DBD6D6DCD7D6DCD7D6DCD6D6DBD6D5DBD6D5DBD6D5DBD6D5DBD6D6DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DE
      D8D8DED9D8DED9D8DED9D8DFD9D8DFDAD9DFDAD9E0DBD9E0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2
      DDDCE2DDDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1
      DCDBE1DCDBE1DCDBE2DDDCE2DDDCE2DDDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDB
      E1DCDBE1DCDBE1DCDBE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDCE2DDDCE2DDDC
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE3DDDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDFE4DEDFE4
      DEDFE4DEDFE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DEDDE2DEDDE2DE
      DDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDEE4DEDFE4DEDFE4DEDFE4DEDFE4DEDF
      E4DEDFE4DEDFE4DEDFE5DFDFE5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DF
      E0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E6E0E0E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1
      E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E7E1E2E7E1E2E7E1E2E8E2
      E2E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E7E4
      E4E6E4E4E6E4E4E7E5E5E7E5E5E7E5E5E7E5E5E8E6E6E8E6E6E9E4E5EAE4E5EA
      E4E5EAE4E5EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE7E8EAE7E9E9E7E9E9E6E8
      E9E6E8E9E6E8E9E6E9EAE7E9EAE7E9E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6
      E8E9E6E8E9E6E8E9E6E8EBE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8EDE8E8EEE8E9EEE8E8ECE7E7EDE7
      E8EDE7E8EDE7E8EDE7E8EEE7E9EEE8E9EEE8E9EDE9EAECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEDEAEBEDEAECEDEAECEDEAEC
      EDEAECEDEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEB
      EDEEEBEDEDEAEBECE9EBEDE9EBEDEAECEDEAECEDEAECEDEAEDEEEBEDEEEBEDEF
      ECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEC
      EEEDECEEEDECEFEEEDEFEEEDEFEEEDEFEEEDEFEFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EFEFF0F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F1F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F1F1F3F1F0F2F1F0F2F1F0F2F1F0F2F0F0F1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1EFF1F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EEF1EFEEF0EFEEF0EFEEF0EFEEF0EEEDF0EEEDEFEEEDEFEEEDF0EFEEF0EFEE
      F0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EEEDEFEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEEEDEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDEDEEEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEE
      EEF0EEEEEFEEEEEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFECECEEECECEEECECEE
      ECECEEECECEEECECEEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEEECECEEECECEEECECEEECECEE
      ECECEDECECEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBECEAEBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEBE8EA
      EBE8EAEBE8EAEBE8EAEDE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6E9E6E6E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E7E6E6E7E5E5E7E5E5E8E6E6E8E6E6E8E6
      E6E8E6E6E7E5E5E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EA
      E4E5EAE4E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5
      E7E5E5E7E5E5FFFFFFFFFFFFFFFFFFFFFFFFE7E5E5E7E5E5E7E5E5E7E5E5E7E5
      E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E8E4E5E8E4E5E8E4E5E8E4E5E8
      E4E5E8E4E5E8E4E5E8E4E5E8E5E5E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6
      E8E6E6E8E6E6E8E6E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E6E6E9
      E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6EAE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7EDE6E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EEE8E8EDE7E8EDE7E8ECE6E7
      ECE6E7EDE7E8EEE8E9EFE9E9EDE9E9EBE8EAEBE8EAEBE8EAEBE8EAECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEDEAEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBEDEBEBEDEBEBEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEEECECEEECECEEECECEEECECEEECECEEECECEDECECEDEBEBEDEBEBEE
      ECECEEECECEEECECEEECECEEECECEEECECEEECECEEECECEFECECEEEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEEDEDEDEDEDEDEDEDEDEDEDEDED
      EDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDED
      EDEDEDEDEDEDEDEDEDEDEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEEDEFEEEDEFEFEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0
      EFEEF0EFEEF0EFEEF0F0EEF0F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F2F0F2F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F0F3F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1EFF1F1F0F2F1F0F2F1F0F2F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1EFEFF1EFEFF0EFEDEFEEEDEFEEEDEFEEEDEFEEEEF0EFEEEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEDECEFEDECEEEEECEEEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDEBEDEDEBEDEDEBEDEDEBEDEDEBEDEEEBEDEEEBEDEE
      EBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEDEBEDEDEAECEDEAECEDEAECEDEAEC
      EDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEA
      ECEDEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBEDE9EAEDE9EAEDE9EAEDE9EAECE8EAECE8E9ECE8E9ECE7E9EDE8E9
      EEE8E9EDE8E8EDE7E8EEE7E9EDE7E8EDE7E8ECE6E8ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7EBE6E7E9E6E7E9E6E7E9E6E7E9E6E7E9E6E7E9E6E7
      E9E6E7E9E6E7E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E8E5E7E8E5E7E8E5E7E8E5
      E7E8E5E8E9E5E7E9E5E6E9E5E6E9E5E6E9E5E6E8E4E6E8E4E5E8E4E5E7E4E5E8
      E6E6E8E6E6E7E5E5E7E5E5E7E5E5E7E5E5E6E4E4E6E4E4E7E4E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E8E2E3E8
      E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E2E7E1E2E7E1E2
      E7E1E2E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6DFE1E5DFE0E5DFE0E5DFE0E5
      DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E4DFE0E4DEDFE4DEDFE4DEDFE4DEDF
      E4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DE
      DFE4DEDFE4DEDFE3DEDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDD
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDDE3DDDDE3DDDDE3DDDDE3DDDDE3DDDDE3DDDDE3DDDD
      E3DDDDE3DDDDE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE2DDDCE2DD
      DCE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DEDDE2DEDDE2
      DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DC
      DBE1DCDBE1DCDBE1DCDBE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDADFDBDADFDAD9DFDAD9DFDAD9DFDAD9DFDAD9DFDAD9DFDAD9DFDA
      D9DFD9D9DED9D8DED9D8DED9D8DED9D8DED9D8DED9D8DED8D8DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD7D7DCD7D6DCD7D6DBD6D6DBD6
      D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DB
      D6D5DAD5D4DAD5D4DAD5D3D9D5D2DAD5D2DAD5D2DAD5D2DBD5D2DBD5D2DCD5D2
      DCD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D4DAD5D4DA
      D6D5DAD6D5DAD6D5DAD6D5DBD6D5DBD6D5DBD6D5DCD6D6DCD6D6DBD5D5DBD5D5
      DBD5D5DBD5D5DBD5D5DAD4D4DBD4D4DED4D4DED4D4DED4D4DED4D4DED4D4DED4
      D4DED4D4DED4D4DDD5D4DBD5D3DBD5D3DBD5D3DBD5D2DBD5D2DBD5D2DBD5D2DB
      D5D2D9D4D2D7D5D1D8D5D1D8D5D1D8D5D1D8D4D1D8D4D1D9D4D1D9D4D1D9D4D1
      D8D4D1D8D4D1D8D4D1D8D4D1D8D4D1D8D4D1D8D4D1D8D4D1DBD3D0DCD3D0DCD3
      D0DCD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFD9D3D0D8D3D0D8D3D0D7D2CFD7
      D2CFD7D2CFD4CFCCD5D0CCD5D0CDD5D0CDD5D0CDD6D1CDD6D1CED6D2CED6D3CF
      D6D3CFD6D3CFD6D3CFD6D3CFD6D3CFD6D3CFD6D3CFD7D3CFDBD2CFDBD2CFDBD2
      CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2D0DAD3D0DAD3D0D8D3D0D8D3D0D8
      D4D0D7D4D0D7D4D0D5D4D0D8D4D2DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4
      DAD5D4DAD5D4D9D5D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4
      D3DAD3D3DBD3D3DBD3D3DBD3D3DBD4D4DCD4D4DCD4D4DCD4D4DCD4D4DFD5D5DF
      D5D5DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D3DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DA
      D5D4DAD5D4DAD5D4DAD5D4DAD5D4DBD6D5DBD6D5DBD6D5DBD6D5DBD6D6DCD7D6
      DCD7D6DCD6D6DBD6D5DBD6D5DBD6D5DBD6D5DBD6D6DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DED8D8DED9D8DED9D8
      DED9D8DFD9D8DFDAD9DFDAD9E0DBD9E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE2DDDBE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDB
      E2DDDCE2DDDCE2DDDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DC
      DBE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDCE2DDDCE2DDDCE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE2DDDCE3DDDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDFE4DEDFE4DEDFE4DEDFE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2
      DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDEE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DE
      DFE5DFDFE5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5
      DFE0E5DFE0E5DFE0E6E0E0E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1
      E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E7E1E2E7E1E2E7E1E2E8E2E2E8E2E3E8E2E3E8
      E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E7E4E4E6E4E4E6E4E4E7
      E5E5E7E5E5E7E5E5E7E5E5E8E6E6E8E6E6E9E4E5EAE4E5EAE4E5EAE4E5EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EAE7E8EAE7E9E9E7E9E9E6E8E9E6E8E9E6E8E9E6
      E9EAE7E9EAE7E9E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9
      E6E8EBE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7
      E8EDE7E8EDE7E8EDE7E8EDE8E8EEE8E9EEE8E8ECE7E7EDE7E8EDE7E8EDE7E8ED
      E7E8EEE7E9EEE8E9EEE8E9EDE9EAECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBEDEAEBEDEAECEDEAECEDEAECEDEAECEDEBEDEEEB
      EDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEDEAEBEC
      E9EBEDE9EBEDEAECEDEAECEDEAECEDEAEDEEEBEDEEEBEDEFECEEEFECEEEFECEE
      EFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEECEEEEED
      EFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEFEE
      EDEFEEEDEFEEEDEFEEEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEFF0
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F1F0F1F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F1F1F3F1F0F2F1F0F2F1F0F2F1F0F2F0F0F1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1EFF1F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EEF1EFEEF0EFEE
      F0EFEEF0EFEEF0EEEDF0EEEDEFEEEDEFEEEDF0EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EFEEF0EFEEF0EEEDEFEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEEEDEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDEDEEEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEEEEF0EEEEEFEEEEEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFECECEEECECEEECECEEECECEEECECEEECEC
      EEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEEECECEEECECEEECECEEECECEEECECEEECECEEECECEEECECEDECECEDEBEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBECEAEBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEBE8EAEBE8EAEBE8EAEBE8
      EAEDE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6E9E6E6E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E7E7E7E6E6E7E5E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7
      E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5E5
      E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5FFFF
      FFFFFFFFFFFFFFFFFFFFE7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7
      E5E5E7E5E5E7E5E5E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5
      EAE4E5EAE4E5E7E5E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7E5E5E7E6
      E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E6E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE8E9EEE8E9EEE8E9EEE8
      E9EEE8E9EDE8E9EBE8EAEBE8EAEBE8EAEBE8EAECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECEAEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEDEBEBEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEE
      ECECEDEBEBEDEBEBEDEBEBEDEBEBECEBEBECEAEAECEAEAEDEBEBEDEBEBEEEBEB
      EEECECEEECECEEECECEEECECEEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EDEDEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEF
      EDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0
      EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F1EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F2F0F2F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F0F2F0EFF1F0F0F1F1F0F2F1F0F2F1F0
      F2F1F0F2F1F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F0F3F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F0F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1EFEFF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EEEDEF
      EEEDEFEEEDEFEEEDEFEDECEFEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEFECEEEFECEEEFECEEEF
      ECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEEEBEEEEEBED
      EEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEB
      EDEEEBEDEEEBEDEEEBEDEDEBEDEDEAECEDEAECEDEAECEDEAECEDEAEBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEDE9EBEFE9EA
      EFE9EAEFE9EAEFE9E9EEE8E9EEE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E8EDE7
      E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7EBE6E7E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6
      E8EAE7E9EAE7E9E9E6E9E9E6E8E9E6E8E9E6E8E9E7E9EAE7E9EAE7E8EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EAE4E5EAE4E5EAE4E5E9E4E5E8E6E6E8E6E6E7E5E5
      E7E5E5E7E5E5E7E5E5E6E4E4E6E4E4E7E4E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E8E2E3E8E2E3E8E2E3E8E2E3
      E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E2E7E1E2E7E1E2E7E1E2E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E0
      E5DFE0E5DFE0E5DFE0E4DFE0E4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DE
      DFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE3
      DEDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DEDDE2DEDD
      E2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE4DEDFE4DEDFE4DEDFE3DDDFE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DDDDE2DD
      DCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE2DDDCE3DEDCE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2
      DDDBE2DDDCE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE2DDDBE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDB
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBD9DFDAD9DF
      DAD9DFD9D8DED9D8DED9D8DED9D8DED8D8DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DBD6D6DBD6D5DBD6D5DBD6D5DB
      D6D5DCD6D6DCD7D6DCD7D6DBD6D6DBD6D5DBD6D5DBD6D5DBD6D5DAD5D4DAD5D4
      DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D3DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DA
      D5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5
      DAD6D5DAD6D5DAD6D5DBD6D5DED6D6DDD5D5DDD5D5DDD5D5DDD5D5DCD4D4DCD4
      D4DCD4D4DCD4D4DED4D4DED4D4DED4D4DED4D4DED4D4DED4D4DED4D4DED4D4DD
      D5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4D8D4D2D5D4D0
      D7D4D0D7D4D0D8D4D0D8D3D0D8D3D0DAD3D0DAD3D0D9D4D1D8D5D1D8D5D1D8D5
      D1D8D5D1D8D5D1D8D5D1D8D5D1D8D5D1DAD3CFDBD2CFDBD2CFDBD2CFDBD2CFDB
      D2CFDBD2CFDBD2CFDBD2CFD8D2CFD7D2CFD7D2CFD7D1CFD6D1CED6D1CED5D0CD
      D5D0CDD5D0CDD5D0CDD5D1CDD6D1CED6D1CED6D2CED6D2CFD6D2CFD6D2CFD6D2
      CFD6D2CFD6D2CFD6D2CFD6D2CFD7D2CFDAD2CFDAD2CFDAD2CFDAD2CFDAD2CFDA
      D2CFDAD2CFDAD2CFDAD2D0DAD3D0DAD3D0D8D3D0D8D3D0D8D4D0D7D4D0D7D4D0
      D6D4D0D8D4D2DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4D9D5
      D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3DAD3D3DBD3D3DB
      D3D3DBD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DDD5D5DDD5D5DDD5D5DDD5D5
      DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DA
      D5D2DAD5D3DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4
      DAD5D4DAD5D4DBD6D5DBD6D5DBD6D5DBD6D5DBD6D6DCD7D6DCD7D6DCD6D6DBD6
      D5DBD6D5DBD6D5DBD6D5DBD6D6DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DED8D8DED9D8DED9D8DED9D8DFD9D8DFDA
      D9DFDAD9E0DBD9E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE2DDDBE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE2DDDCE2DD
      DBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDCE2DDDCE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2
      DDDCE3DDDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDFE4DEDFE4DEDFE4DEDFE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDD
      E2DEDDE2DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DEE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE5DFDFE5DFE0E5
      DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0
      E6E0E0E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E7E1E2E7E1E2E7E1E2E8E2E2E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3
      E8E2E3E8E2E3E8E2E3E8E2E3E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E7E4E4E6E4E4E6E4E4E7E5E5E7E5E5E7E5E5
      E7E5E5E8E6E6E8E6E6E9E4E5EAE4E5EAE4E5EAE4E5EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EAE7E8EAE7E9E9E7E9E9E6E8E9E6E8E9E6E8E9E6E9EAE7E9EAE7E9E9
      E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8EBE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8ED
      E7E8EDE8E8EEE8E9EEE8E8EDE7E8EDE7E8EDE7E8EEE8E9EEE8E9EEE8E9EEE8E9
      EEE8E9EDE9EAECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBEDEAEBEDEAECEDEAECEDEAECEDEAECEDEBEDEEEBEDEEEBEDEEEBEDEE
      EBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEDEAEBEDEAECEDEAECEDEAEC
      EDEAECEEEAECEEEBEDEEEBEDEEEBEDEFECEEEFECEEEFECEEEFECEEEFECEEEFEC
      EEEFECEEEFECEEEFECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEECEEEEEDEFEEEDEFEEEDEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEFEEEDEFEEEDEFEEEDEF
      EEEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEFF0F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0F0F1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F1F2
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F1F3F1F0F2F1
      F0F2F1F0F2F1F0F2F0F0F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F1EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EEF1EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEDEFEEEDEFEEEDF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0
      EFEEF0EEEDEFEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEED
      EEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEDEE
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEEEEF0EEEEEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFECECEEECECEDECECEEECECEEECECEEECECEEECECEEECECEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECEC
      EEECECEEECECEEECECEEECECEEECECEEECECEDECECEDEBEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBECEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBEBE8EAEBE8EAEBE8EAEBE8EAEDE8E9EEE8E9EE
      E8E9EEE8E9EEE8E9EDE8E9EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6E9E6E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E7
      E6E6E7E5E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7E5E5EAE4E5EAE4E5
      EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5E5E7E5E5E7E5E5E7E5
      E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5FFFFFFFFFFFFFFFFFFFF
      FFFFE7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5
      E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5
      E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7E5E5E7E6E6E9E7E7E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E6E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EB
      E8EAEBE8EAEBE8EAEBE8EAECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECEAEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEDEBEB
      EDEBEBEDEBEBEDEBEBECEAEAECEAEAEDEBEBEDEBEBEEECECEEECECEEECECEEEC
      ECEEECECEEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEDEDEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEECEEEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEFEDEFEFEEF0EFEEF0EFEEF0EFEE
      F0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEFF0F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1EFF1F0EFF1F0F0F1F1F0F2F1F0F2F1F0F2F1F0F2F1F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0F0F2F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      EFEFF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EEEDEFEEEDEFEEEDEFEEED
      EFEDECEFEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEE
      EDEFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEE
      EFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEEEBEEEEEBEDEEEBEDEEEBEDEDEA
      EDEEEAECEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEE
      EBEDEDEBEDEDEAECEDEAECEDEAECEDEAECEDEAEBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEEEAEBEFEAEAEFE9EAEFE9EAEFE9
      EAEFE9EAEEE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E8EDE7E8EDE7E8EDE7E8ED
      E7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE6E7E9E6
      E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8EAE7E9EAE7E9E9
      E6E9E9E6E8E9E6E8E9E6E8E9E7E9EAE7E9EAE7E8EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EAE4E5EAE4E5EAE4E5E9E4E5E8E6E6E8E6E6E7E5E5E7E5E5E7E5E5E7E5
      E5E6E4E4E6E4E4E7E4E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2
      E3E8E2E3E8E2E3E8E2E2E7E1E2E7E1E2E7E1E2E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1
      E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E0E5DFE0E5DFE0E5DF
      E0E4DFE0E4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4
      DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE3DEDEE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DE
      DDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE4DEDFE4DEDFE4DEDFE3DDDFE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DDDDE2DDDCE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE2DDDCE2DDDCE3DEDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE1
      DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDBE2DDDCE2DDDC
      E1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE2DDDBE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBD9DFDAD9DFDAD9DFD9D8DED9D8
      DED9D8DED9D8DED8D8DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DBD6D6DBD6D5DBD6D5DBD6D5DBD6D5DCD6D6DCD7D6
      DCD7D6DBD6D6DBD6D5DBD6D5DBD6D5DBD6D5DAD5D4DAD5D4DAD5D4DAD5D4DAD5
      D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D3DAD5D2DAD5D2DAD5D2DA
      D5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DAD5D2DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6
      D5DBD6D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DCD4D4DCD4D4DCD4D4DCD4D4DD
      D4D4DDD4D4DDD4D4DDD4D4DDD4D4DDD4D4DDD4D4DDD4D4DCD5D4DAD5D4DAD5D4
      DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4D8D4D2D5D4D0D7D4D0D7D4D0D8D4
      D0D8D3D0D8D3D0DAD3D0DAD3D0D9D3D0D8D4D0D8D4D0D8D4D0D8D4D0D8D4D0D8
      D4D0D8D4D0D8D4D0D9D2CFDAD2CFDAD2CFDAD2CFDAD2CFDAD2CFDAD2CFDAD2CF
      DAD2CFD7D2CFD7D2CFD7D2CFD7D1CFD6D1CED6D1CED5D0CDD5D0CDD5D0CDD5D0
      CDD5D1CDD6D1CED6D1CED6D2CED7D2CFD7D2CFD7D2CFD7D2CFD7D2CFD7D2CFD7
      D2CFD7D2CFD8D2CFDAD3D0DAD3D0DAD3D0DAD3D0DAD3D0DAD3D0DAD3D0DAD3D0
      DAD3D0DBD4D1DBD4D1D9D4D1D9D4D1D8D5D1D8D5D1D8D5D1D7D5D1D8D5D3DAD5
      D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D3D9D4D3D9D4D3D9
      D4D3D9D4D3D9D4D3D9D4D3D9D4D3D9D4D3DBD3D3DBD3D3DBD3D3DBD4D4DCD4D4
      DCD4D4DCD4D4DCD4D4DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5
      D5DDD5D5DDD5D5DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DA
      D5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D3DAD5D4
      DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DBD6
      D5DBD6D5DBD6D5DBD6D5DBD6D6DCD7D6DCD7D6DCD6D6DBD6D5DBD6D5DBD6D5DB
      D6D5DBD6D6DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DED8D8DED9D8DED9D8DED9D8DFD9D8DFDAD9DFDAD9E0DBD9E0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DC
      DBE1DCDBE1DCDBE2DDDCE2DDDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE2DDDCE2DDDBE1DCDBE1DCDBE1
      DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDCE2DDDCE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE3DDDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDFE4DEDFE4DEDFE4DEDFE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDEE4DEDFE4DEDFE4
      DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE5DFDFE5DFE0E5DFE0E5DFE0E5DFE0
      E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E6E0E0E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E7E1E2
      E7E1E2E7E1E2E8E2E2E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2
      E3E8E2E3E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E7E4E4E6E4E4E6E4E4E7E5E5E7E5E5E7E5E5E7E5E5E8E6E6E8E6
      E6E9E4E5EAE4E5EAE4E5EAE4E5EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE7E8EA
      E7E9E9E7E9E9E6E8E9E6E8E9E6E8E9E6E9EAE7E9EAE7E9E9E6E8E9E6E8E9E6E8
      E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8EBE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8EDE8E8EEE8E9
      EEE8E9EEE8E9EEE8E9EEE8E9EEE8E9EFE9E9EFE9EAEFE9EAEFE9EAEDE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEDEAEBED
      EAECEDEAECEDEAECEDEAECEDEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBED
      EEEBEDEEEBEDEEEBEDEEEBEDEEEAECEDEAECEDEAECEDEAECEDEAECEEEAECEEEB
      EDEEEBEDEEEBEDEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEF
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEECEEEDECEEEDECEFEEEDEFEEEDEFEEEDEFEEEDEFEFEEF0EFEE
      F0EFEEF0EFEEF0EFEEF0EFEEF0EFEFF0F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F1F3F1F0F2F1F0F2F1F0F2F1F0F2
      F0F0F1F0EFF1F1EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1EFEEF1EFEEF0EFEEF0EFEEF0EFEEF0EFEEEF
      EEEDF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEDEFEEED
      EFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEDEEEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEEEC
      ECEDECECEDEBEBEDEBEBEDEBEBEEECECEEECECEEECECEEECECEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEEECECEEEC
      ECEEECECEEECECEEECECEDECECEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      ECEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBEBE8EAEBE8EAEBE8EAEBE8EAEDE8E9EEE8E9EEE8E9EEE8E9EEE8E9
      EDE8E9EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6E9E6E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E7E6E6E7E5E5E7E5E5
      E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4
      E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7
      E5E5E7E5E5E7E5E5E7E5E5E7E5E5FFFFFFFFFFFFFFFFFFFFFFFFE7E5E5E7E5E5
      E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5EAE4E5EAE4
      E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5E5E7E5E5E8E6E6E8
      E6E6E8E6E6E8E6E6E7E5E5E7E5E5E7E6E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E6E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EBE8EAEBE8EAEBE8EA
      EBE8EAECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECEAEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEDEBEBEDEBEBEDEBEBEDEB
      EBECEAEAECEAEAEDEBEBEDEBEBEEECECEEECECEEECECEEECECEEECECEEECECEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEDEDEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEECEEEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEEDEFEEEDEFEEEDEFEFEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EFEEF0EFEEF0EFEFF0F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      EFF1F0EFF1F0F0F1F1F0F2F1F0F2F1F0F2F1F0F2F1F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0F0F2F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1EFEFF0EFEEF0EFEE
      F0EFEEF0EFEEF0EFEEF0EFEEF0EEEDEFEEEDEFEEEDEFEEEDEFEDECEFEDECEEEE
      ECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFEC
      EEEFECEEEFECEEEFECEEEEEBEEEEEBEDEEEBEDEEEBEDEDEAEDEEEAECEEEBEDEE
      EBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEDEBEDEDEAEC
      EDEAECEDEAECEDEAECEDEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBEEEAEBEFEAEAEFE9EAEFE9EAEFE9EAEFE9EAEEE8E9EE
      E8E9EEE8E9EEE8E9EEE8E9EDE8E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE6E7E9E6E8E9E6E8E9E6E8E9
      E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8EAE7E9EAE7E9E9E6E9E9E6E8E9E6E8
      E9E6E8E9E7E9EAE7E9EAE7E8EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE4E5EAE4
      E5EAE4E5E9E4E5E8E6E6E8E6E6E7E5E5E7E5E5E7E5E5E7E5E5E6E4E4E6E4E4E7
      E4E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8
      E2E2E7E1E2E7E1E2E7E1E2E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1
      E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E0E5DFE0E5DFE0E5DFE0E4DFE0E4DEDFE4
      DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDF
      E4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE3DEDEE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2
      DEDDE2DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE4DEDFE4DE
      DFE4DEDFE3DDDFE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DDDDE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E2DDDCE2DDDCE3DEDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE1DCDBE1DCDBE1DCDB
      E1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDBE2DDDCE2DDDCE1DCDBE1DCDBE1DC
      DBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE2DDDBE2DD
      DCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBD9DFDAD9DFDAD9DFD9D8DED9D8DED9D8DED9D8DED8
      D8DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DBD6D6DBD6D5DBD6D5DBD6D5DBD6D5DCD6D6DCD7D6DCD7D6DBD6D6DBD6
      D5DBD6D5DBD6D5DBD6D5DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DA
      D5D4DAD5D4DAD5D4DAD5D4DAD5D3DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5
      D2DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DBD6D5DDD5D5DD
      D5D5DDD5D5DDD5D5DDD5D5DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4
      DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DBD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5
      D4DAD5D4DAD5D4DAD5D4D8D4D2D6D4D0D7D4D0D7D4D0D8D4D0D8D3D0D8D3D0DA
      D3D0DAD3D0D9D3D0D8D3D0D8D3D0D8D3D0D8D3D0D8D3D0D8D3D0D8D3D0D8D3D0
      D8D2CFD9D2CFD9D2CFD9D2CFD9D2CFD9D2CFD9D2CFD9D2CFD9D2CFD7D2CFD7D2
      CFD7D2CFD7D1CFD6D1CED6D1CED5D0CDD5D0CDD5D0CDD6D1CED6D1CED6D1CED6
      D1CED6D2CED7D2CFD7D2CFD7D2CFD7D2CFD7D2CFD7D2CFD7D2CFD7D2CFD8D2CF
      D9D3D0D9D3D0D9D3D0D9D3D0D9D3D0D9D3D0D9D3D0D9D3D0D9D3D0DBD4D1DBD4
      D1D9D4D1D9D4D1D8D5D1D8D5D1D8D5D1D7D5D1D8D5D3DAD5D4DAD5D4DAD5D4DA
      D5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D3DAD4D3DAD4D3DAD4D3DAD4D3DAD4D3
      DAD4D3DAD4D3DAD4D3DBD3D3DBD3D3DCD4D4DCD4D4DCD4D4DCD4D4DDD5D5DDD5
      D5DDD5D5DCD5D5DCD5D5DCD5D5DCD5D5DCD5D5DCD5D5DCD5D5DCD5D5DCD5D5DA
      D5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D3DAD5D4DAD5D4DAD5D4DAD5
      D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DBD6D5DBD6D5DBD6D5DB
      D6D5DBD6D6DCD7D6DCD7D6DCD6D6DBD6D5DBD6D5DBD6D5DBD6D5DBD6D6DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DE
      D8D8DED9D8DED9D8DED9D8DFD9D8DFDAD9DFDAD9E0DBD9E0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2
      DDDCE2DDDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1
      DCDBE1DCDBE1DCDBE2DDDCE2DDDCE2DDDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDB
      E1DCDBE1DCDBE1DCDBE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDCE2DDDCE2DDDC
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE3DDDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDFE4DEDFE4
      DEDFE4DEDFE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DEDDE2DEDDE2DE
      DDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDEE4DEDFE4DEDFE4DEDFE4DEDFE4DEDF
      E4DEDFE4DEDFE4DEDFE5DFDFE5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DF
      E0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E6E0E0E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1
      E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E7E1E2E7E1E2E7E1E2E8E2
      E2E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E7E4
      E4E6E4E4E6E4E4E7E5E5E7E5E5E7E5E5E7E5E5E8E6E6E8E6E6E9E4E5EAE4E5EA
      E4E5EAE4E5EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE7E8EAE7E9E9E7E9E9E6E8
      E9E6E8E9E6E8E9E6E9EAE7E9EAE7E9E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6
      E8E9E6E8E9E6E8E9E6E8EBE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8EDE8E8EEE8E9EEE8E9EEE8E9EEE8
      E9EFE9EAEFE9EAEFE9EAEFE9EAEFE9EBF0EAEBEEEAEBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEDEAEBEDEAECEDEAECEDEAEC
      EDEAECEDEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEB
      EDEEEBEDEEEAECEDEAECEDEAECEDEAECEEEBECEEEBEDEEEBEDEEEBEDEEEBEDEF
      ECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEC
      EEEDECEEEDECEFEEEDEFEEEDEFEEEDEFEEEDEFEFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EFEFF0F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F1F1F3F1F0F2F1F0F2F1F0F2F1F0F2F0F0F1F0EFF1F1EF
      F1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEE
      F0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEDEFEEEDEFEEEDEFEEEDEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDEDEEEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEEECECEDECECEDEBEBED
      EBEBEDEBEBEDECECEEECECEEECECEEECECEEECECEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEEECECEEECECEEECECEEECECEE
      ECECEDECECEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBECEAEBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEBE8EA
      EBE8EAEBE8EAEBE8EAEDE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6E9E6E6E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E7E6E6E7E5E5E7E5E5E8E6E6E8E6E6E8E6
      E6E8E6E6E7E5E5E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EA
      E4E5EAE4E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5
      E7E5E5E7E5E5FFFFFFFFFFFFFFFFFFFFFFFFE7E5E5E7E5E5E7E5E5E7E5E5E7E5
      E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EA
      E4E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6
      E7E5E5E7E5E5E7E6E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E6E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE8E9
      EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EBE8EAEBE8EAEBE8EAEBE8EAECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECEAEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBEDEBEBEDEBEBEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEEECECEEECECEEECECEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEEECECEEECECEEECECEEECECEEECECEFECECEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEDEDEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEE
      EDEFEFEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0
      EFEEF0EFEEF0EFEEF0F0EEF0F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1EFF1F0EFF1F0F0F1
      F1F0F2F1F0F2F1F0F2F1F0F2F1F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F0F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1EFEFF0EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EEEDEFEEEDEFEEEDEFEEEDEFEDECEFEDECEEEEECEEEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEFEC
      EEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEEEBEDEE
      EBEDEEEBEDEEEBEDEEEAECEDEAECEDEAECEEEAECEEEBEDEEEBEDEEEBEDEEEBED
      EEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEDEBEDEDEAECEDEAECEDEAECEDEA
      ECEDEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBEEEAEBF0EAEBF0EAEBEFEAEBEFE9EAEFE9EAEFE9EAEFE9EAEEE8E9EEE8E9
      EEE8E9EDE8E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7EBE6E7E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8
      E9E6E8E9E6E8E9E6E8EAE7E9EAE7E9E9E6E9E9E6E8E9E6E8E9E6E8E9E7E9EAE7
      E9EAE7E8EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE4E5EAE4E5EAE4E5E9E4E5E8
      E6E6E8E6E6E7E5E5E7E5E5E7E5E5E7E5E5E6E4E4E6E4E4E7E4E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E8E2E3E8
      E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E2E7E1E2E7E1E2
      E7E1E2E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E0E5DFE0E5DFE0E5DFE0E4DFE0E4DEDFE4DEDFE4DEDFE4DEDF
      E4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DE
      DFE4DEDFE4DEDFE3DEDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDD
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE4DEDFE4DEDFE4DEDFE3DDDFE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DDDDE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE2DDDCE3DE
      DCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DC
      DBE1DCDBE1DCDBE2DDDBE2DDDCE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE2DDDBE2DDDCE1DCDBE1DCDBE1
      DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBD9DFDAD9DFDAD9DFD9D8DED9D8DED9D8DED9D8DED8D8DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DBD6D6DBD6
      D5DBD6D5DBD6D5DBD6D5DCD6D6DCD7D6DCD7D6DBD6D6DBD6D5DBD6D5DBD6D5DB
      D6D5DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4
      DAD5D4DAD5D3DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DBD6D5DBD6D5DB
      D6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DCD6D5DDD5D5DDD5D5DDD5D5DDD5D5
      DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4
      D4DCD4D4DCD4D4DBD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DA
      D5D4D8D5D3D7D5D1D8D5D1D8D5D1D8D5D1D9D4D1D9D4D1DBD4D1DBD4D1D9D3D0
      D8D3D0D8D3D0D8D3D0D8D3D0D8D3D0D8D3D0D8D3D0D8D3D0D8D2CFD9D2CFD9D2
      CFD9D2CFD9D2CFD9D2CFD9D2CFD9D2CFD9D2CFD7D2CFD6D1CED6D1CED6D1CED6
      D1CED6D1CDD5D0CDD5D0CDD6D1CDD6D1CED6D1CED6D1CED6D1CED7D2CFD9D2CF
      D9D2CFD9D2CFD9D2CFD9D2CFD9D2CFD9D2CFD9D2CFD8D2CFD8D3D0D8D3D0D8D3
      D0D8D3D0D8D3D0D8D3D0D8D3D0D8D3D0D9D3D0DBD4D1DBD4D1D9D4D1D9D4D1D8
      D5D1D8D5D1D8D5D1D7D5D1D8D5D3DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4
      DAD5D4DAD5D4DBD5D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4
      D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DDD5D5DDD5D5DDD5D5DDD5D5DCD6D5DB
      D6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D3DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DA
      D5D4DAD5D4DAD5D4DAD5D4DAD5D4DBD6D5DBD6D5DBD6D5DBD6D5DBD6D6DCD7D6
      DCD7D6DCD6D6DBD6D5DBD6D5DBD6D5DBD6D5DBD6D6DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DED8D8DED9D8DED9D8
      DED9D8DFD9D8DFDAD9DFDAD9E0DBD9E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE2DDDBE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDB
      E2DDDCE2DDDCE2DDDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DC
      DBE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDCE2DDDCE2DDDCE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE2DDDCE3DDDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDFE4DEDFE4DEDFE4DEDFE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2
      DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDEE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DE
      DFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DFE0E5
      DFE0E5DFE0E5DFE0E6E0E0E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1
      E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E7E1E2E7E1E2E7E1E2E8E2E2E8E2E3E8E2E3E8
      E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E7E4E4E6E4E4E6E4E4E7
      E5E5E7E5E5E7E5E5E7E5E5E8E6E6E8E6E6E9E4E5EAE4E5EAE4E5EAE4E5EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EAE7E8EAE7E9E9E7E9E9E6E8E9E6E8E9E6E8E9E6
      E9EAE7E9EAE7E9E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9
      E6E8EBE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7
      E8EDE7E8EDE7E8EDE7E8EDE8E8EEE8E9EEE8E9EEE8E9EFE9EAEFE9EAEFE9EAEF
      E9EAEFEAEBF0EAEBF0EAEBEEEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBEDEAEBEDEAECEDEAECEDEAECEDEAECEDEBEDEEEB
      EDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEAECED
      EAECEDEAECEEEAECEEEBEDEEEBEDEEEBEDEEEBEDEFECEEEFECEEEFECEEEFECEE
      EFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEECEEEEED
      EFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEFEE
      EDEFEEEDEFEEEDEFEEEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEFF0
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F1F1F3F1F0F2F1F0F2F1F0F2F1F0F2F0F0F1F0EFF1F1EFF1F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EFEEF0EFEEF0EFEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDEDEEEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFECECEE
      ECECEEECECEEECECEEECECEEECECEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEEECECEEECECEEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBECEAEBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEBE8EAEBE8EAEBE8EAEBE8
      EAEDE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6E9E6E6E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E7E7E7E6E6E7E5E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7
      E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5E5
      E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5FFFF
      FFFFFFFFFFFFFFFFFFFFE7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7
      E5E5E7E5E5E7E5E5E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5
      EAE4E5EAE4E5E7E5E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7E5E5E7E6
      E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E6E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE8E9EEE8E9EEE8E9EEE8
      E9EEE8E9EDE8E9EBE8EAEBE8EAEBE8EAEBE8EAECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECEAEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBEDEBEBEDECECEEECECEEECECEEECECEEECECEEECECEEECEC
      EEECECEEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEE
      ECECEEECECEEECECEDECECEDEBEBEDEBEBEDEBEBEDECECEEECECEEECECEEECEC
      EEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EDEDEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEFEDEFEFEEF0
      EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEE
      F0F0EEF0F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1EFF1F0EFF1F0F0F1F1F0F2F1F0F2F1F0
      F2F1F0F2F1F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F0F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1EFEFF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EEEDEF
      EEEDEFEEEDEFEEEDEFEDECEFEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEFECEEEFECEEEFECEEEF
      ECEEEFECEEEFECEEEFECEEEFECEEEFECEEEEEBEDEEEBEDEEEBEDEEEBEDEEEBEC
      EDEAECEDEAECEDEAECEEEAECEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEB
      EDEEEBEDEEEBEDEEEBEDEDEBEDEDEAECEDEAECEDEAECEDEAECEDEAEBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEEEAEBF0EAEB
      EFE9EBEFE9EAEFE9EAEFE9EAEFE9EAEEE8E9EEE8E9EEE8E9EEE8E9EDE8E8EDE7
      E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7EBE6E7E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6
      E8EAE7E9EAE7E9E9E6E9E9E6E8E9E6E8E9E6E8E9E7E9EAE7E9EAE7E8EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EAE4E5EAE4E5EAE4E5E9E4E5E8E6E6E8E6E6E7E5E5
      E7E5E5E7E5E5E7E5E5E6E4E4E6E4E4E7E4E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E8E2E3E8E2E3E8E2E3E8E2E3
      E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E2E7E1E2E7E1E2E7E1E2E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E0
      E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DF
      E0E5DFE0E5DFDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE3
      DEDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DEDDE2DEDD
      E2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE4DEDFE4DEDFE4DEDFE3DDDFE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DDDDE2DD
      DCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE2DDDCE3DEDCE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2
      DDDBE2DDDCE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE2DDDBE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDB
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBD9DFDAD9DF
      DAD9DFD9D8DED9D8DED9D8DED9D8DED8D8DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DBD6D6DBD6D5DBD6D5DBD6D5DB
      D6D5DCD6D6DCD7D6DCD7D6DBD6D6DBD6D5DBD6D5DBD6D5DBD6D5DAD5D4DAD5D4
      DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D3DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DA
      D5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DCD5D5DCD5D5DCD5D5DCD5D5DCD5D5
      DCD5D5DCD5D5DCD5D5DCD5D5DDD5D5DDD5D5DDD5D5DCD4D4DCD4D4DCD4D4DCD4
      D4DBD3D3DBD3D3DAD4D3DAD4D3DAD4D3DAD4D3DAD4D3DAD4D3DAD4D3DAD4D3DA
      D5D3DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4D8D5D3D7D5D1
      D8D5D1D8D5D1D8D5D1D9D4D1D9D4D1DBD4D1DBD4D1D9D3D0D9D3D0D9D3D0D9D3
      D0D9D3D0D9D3D0D9D3D0D9D3D0D9D3D0D8D2CFD7D2CFD7D2CFD7D2CFD7D2CFD7
      D2CFD7D2CFD7D2CFD7D2CFD6D2CED6D1CED6D1CED6D1CED6D1CED5D0CDD6D1CE
      D6D1CED6D1CED6D1CED7D1CFD7D2CFD7D2CFD7D2CFD9D2CFD9D2CFD9D2CFD9D2
      CFD9D2CFD9D2CFD9D2CFD9D2CFD8D2CFD8D3D0D8D3D0D8D3D0D8D3D0D8D3D0D8
      D3D0D8D3D0D8D3D0D9D3D0DAD3D0DAD3D0D8D3D0D8D3D0D8D4D0D7D4D0D7D4D0
      D6D4D0D8D4D2DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DBD5
      D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DCD4D4DC
      D4D4DCD4D4DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DBD6D5DAD6D5DAD6D5DAD6D5
      DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DA
      D5D2DAD5D3DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4
      DAD5D4DAD5D4DBD6D5DBD6D5DBD6D5DBD6D5DBD6D6DCD7D6DCD7D6DCD6D6DBD6
      D5DBD6D5DBD6D5DBD6D5DBD6D6DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DED8D8DED9D8DED9D8DED9D8DFD9D8DFDA
      D9DFDAD9E0DBD9E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE2DDDBE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE2DDDCE2DD
      DBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDCE2DDDCE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2
      DDDCE3DDDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDFE4DEDFE4DEDFE4DEDFE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDD
      E2DEDDE2DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DEE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4
      DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DFE0E5DFE0E5DFE0E5DFE0
      E6E0E0E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E7E1E2E7E1E2E7E1E2E8E2E2E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3
      E8E2E3E8E2E3E8E2E3E8E2E3E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E7E4E4E6E4E4E6E4E4E7E5E5E7E5E5E7E5E5
      E7E5E5E8E6E6E8E6E6E9E4E5EAE4E5EAE4E5EAE4E5EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EAE7E8EAE7E9E9E7E9E9E6E8E9E6E8E9E6E8E9E6E9EAE7E9EAE7E9E9
      E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8EBE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8ED
      E7E8EDE8E8EEE8E9EEE8E9EEE8E9EEE8E9EEE8E9EFE9EAEFE9EAEFE9EAEFE9EA
      EFEAEAEEEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBEDEAEBEDEAECEDEAECEDEAECEDEAECEDEBEDEEEBEDEEEBEDEEEBEDEE
      EBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEAECEDEAEDEEEBEDEEEBED
      EEEBEDEEEBEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFEC
      EEEFECEEEFECEEEFECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEECEEEEEDEFEEEDEFEEEDEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEFEEEDEFEEEDEFEEEDEF
      EEEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEFF0F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F1F3F1F0F2F1
      F0F2F1F0F2F1F0F2F0F0F1F0EFF1F1EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1EFEFF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0
      EFEEF0EFEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEDEE
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEEECECEEECEC
      EEECECEDEBEBEDEBEBECEAEAECEAEAEDEBEBEDEBEBEDEBEBEDEBEBEEECECEEEC
      ECEEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEDEBEBEDEBEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBECEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBEBE8EAEBE8EAEBE8EAEBE8EAEDE8E9EEE8E9EE
      E8E9EEE8E9EEE8E9EDE8E9EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6E9E6E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E7
      E6E6E7E5E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7E5E5EAE4E5EAE4E5
      EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5E5E7E5E5E7E5E5E7E5
      E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5FFFFFFFFFFFFFFFFFFFF
      FFFFE7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5
      E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5
      E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7E5E5E7E6E6E9E7E7E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E6E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EB
      E8EAEBE8EAEBE8EAEBE8EAECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECEAEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEDECECEEECECEEECECEEECECEEECECEEECECEEECECEEECECEEECECEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECEC
      EEECECEDEBEBEDEBEBEDEBEBEDECECEEECECEEECECEEECECEEECECEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEDEDEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEECEEEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEFEDEFEFEEF0EFEEF0EFEEF0EFEE
      F0EFEEF0EFEEF0EFEEF0EFEEF0EEEDF0EFEEEFEFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1EFF1F0EFF1F0F0F1F1F0F2F1F0F2F1F0F2F1F0F2F1F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0F0F2F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      EFEFF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EEEDEFEEEDEFEEEDEFEEED
      EFEDECEFEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEE
      EDEFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEE
      EFECEEEFECEEEFECEEEEEBEDEEEBEDEEEBEDEEEAECEDEAECEDEAECEDEAECEDEA
      ECEEEAECEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEE
      EBEDEDEBEDEDEAECEDEAECEDEAECEDEAECEDEAEBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEDE9EBEFE9EAEFE9EAEFE9EAEFE9
      E9EEE8E9EEE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E8EDE7E8EDE7E8EDE7E8ED
      E7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE6E7E9E6
      E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8EAE7E9EAE7E9E9
      E6E9E9E6E8E9E6E8E9E6E8E9E7E9EAE7E9EAE7E8EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EAE4E5EAE4E5EAE4E5E9E4E5E8E6E6E8E6E6E7E5E5E7E5E5E7E5E5E7E5
      E5E6E4E4E6E4E4E7E4E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2
      E3E8E2E3E8E2E3E8E2E2E7E1E2E7E1E2E7E1E2E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1
      E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E0E5DFE0E5DFE0E5DF
      E0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFDFE4
      DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE3DEDEE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DE
      DDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE4DEDFE4DEDFE4DEDFE3DDDFE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DDDDE2DDDCE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE2DDDCE2DDDCE3DEDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE1
      DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDBE2DDDCE2DDDC
      E1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE2DDDBE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBD9DFDAD9DFDAD9DFD9D8DED9D8
      DED9D8DED9D8DED8D8DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DBD6D6DBD6D5DBD6D5DBD6D5DBD6D5DCD6D6DCD7D6
      DCD7D6DBD6D6DBD6D5DBD6D5DBD6D5DBD6D5DAD5D4DAD5D4DAD5D4DAD5D4DAD5
      D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D3DAD5D2DAD5D2DAD5D2DA
      D5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DAD5D2DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5
      D5DDD5D5DDD5D5DCD4D4DCD4D4DCD4D4DCD4D4DBD4D4DBD3D3DBD3D3DBD3D3D9
      D4D3D9D4D3D9D4D3D9D4D3D9D4D3D9D4D3D9D4D3D9D4D3DAD5D3DAD5D4DAD5D4
      DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4D8D5D3D7D5D1D8D5D1D8D5D1D8D5
      D1D9D4D1D9D4D1DBD4D1DBD4D1DAD3D0DAD3D0DAD3D0DAD3D0DAD3D0DAD3D0DA
      D3D0DAD3D0DAD3D0D8D2CFD7D2CFD7D2CFD7D2CFD7D2CFD7D2CFD7D2CFD7D2CF
      D7D2CFD6D2CED6D1CED6D1CED5D1CDD5D0CDD5D0CDD6D1CED6D1CED6D1CED6D1
      CED7D1CFD7D2CFD7D2CFD7D2CFDAD2CFDAD2CFDAD2CFDAD2CFDAD2CFDAD2CFDA
      D2CFDAD2CFD9D2CFD8D4D0D8D4D0D8D4D0D8D4D0D8D4D0D8D4D0D8D4D0D8D4D0
      D9D3D0DAD3D0DAD3D0D8D3D0D8D3D0D8D4D0D7D4D0D7D4D0D5D4D0D8D4D2DAD5
      D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DCD5D4DDD4D4DDD4D4DD
      D4D4DDD4D4DDD4D4DDD4D4DDD4D4DDD4D4DCD4D4DCD4D4DCD4D4DCD4D4DDD5D5
      DDD5D5DDD5D5DDD5D5DDD5D5DBD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6
      D5DAD6D5DAD6D5DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DA
      D5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D3DAD5D4
      DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DBD6
      D5DBD6D5DBD6D5DBD6D5DBD6D6DCD7D6DCD7D6DCD6D6DBD6D5DBD6D5DBD6D5DB
      D6D5DBD6D6DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DED8D8DED9D8DED9D8DED9D8DFD9D8DFDAD9DFDAD9E0DBD9E0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DC
      DBE1DCDBE1DCDBE2DDDCE2DDDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE2DDDCE2DDDBE1DCDBE1DCDBE1
      DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDCE2DDDCE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE3DDDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDFE4DEDFE4DEDFE4DEDFE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDEE4DEDFE4DEDFE4
      DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDF
      E4DEDFE4DEDFE4DEDFE4DEDFE4DFE0E5DFE0E5DFE0E5DFE0E6E0E0E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E7E1E2
      E7E1E2E7E1E2E8E2E2E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2
      E3E8E2E3E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E7E4E4E6E4E4E6E4E4E7E5E5E7E5E5E7E5E5E7E5E5E8E6E6E8E6
      E6E9E4E5EAE4E5EAE4E5EAE4E5EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE7E8EA
      E7E9E9E7E9E9E6E8E9E6E8E9E6E8E9E6E9EAE7E9EAE7E9E9E6E8E9E6E8E9E6E8
      E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8EBE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8EDE8E8EEE8E9
      EEE8E9EEE8E9EEE8E9EEE8E9EFE9EAEFE9EAEFE9EAEFE9EAEFEAEAEEEAEBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEDEAEBED
      EAECEDEAECEDEAECEDEAECEDEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBED
      EEEBEDEEEBEDEEEBEDEEEBEDEEEAECEDEAEDEEEBEDEEEBEDEEEBEDEEEBEEEFEC
      EEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEF
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEECEEEDECEEEDECEFEEEDEFEEEDEFEEEDEFEEEDEFEFEEF0EFEE
      F0EFEEF0EFEEF0EFEEF0EFEEF0EFEFF0F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F1F3F1F0F2F1F0F2F1F0F2F1F0F2
      F0F0F1F0EFF1F1EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1EFEFF0EFEEF0
      EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEDEFEEED
      EFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEDEEEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEEECECEEECECEEECECEDEBEBEDEB
      EBECEAEAECEAEAEDEBEBEDEBEBEDEBEBEDEBEBEEECECEEECECEEECECEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEDEBEBEDEBEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      ECEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBEBE8EAEBE8EAEBE8EAEBE8EAEDE8E9EEE8E9EEE8E9EEE8E9EEE8E9
      EDE8E9EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6E9E6E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E7E6E6E7E5E5E7E5E5
      E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4
      E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7
      E5E5E7E5E5E7E5E5E7E5E5E7E5E5FFFFFFFFFFFFFFFFFFFFFFFFE7E5E5E7E5E5
      E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5EAE4E5EAE4
      E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5E5E7E5E5E8E6E6E8
      E6E6E8E6E6E8E6E6E7E5E5E7E5E5E7E6E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E6E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EBE8EAEBE8EAEBE8EA
      EBE8EAECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECEAEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDECECEEEC
      ECEEECECEEECECEEECECEEECECEEECECEEECECEEECECEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEEEC
      ECEEECECEDECECEEECECEFECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDF0EEEEEF
      EEEEEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEDEDEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEECEEEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEEEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EEEDF0EEEDEFEFEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0F0EEF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0F0F1F1F0F2F1F0F2F1F0F2F1F0F2F1F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F1F1F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F0F0F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F1EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0F0F2F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1EFEFF0EFEEF0EFEE
      F0EFEEF0EFEEF0EFEEF0EFEEF0EEEDEFEEEDEFEEEDEFEEEDEFEDECEFEDECEEEE
      ECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFEC
      EEEEEBEDEEEBEDEEEBEDEEEAECEDEAECEDEAECEDEAECEDEAECEDEAEBEEEBEDEE
      EBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEDEBEDEDEAEC
      EDEAECEDEAECEDEAECEDEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBEDE9EAEEE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE7E8ED
      E7E8EDE7E8EEE8E8EEE8E9EDE8E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE6E7E9E6E8E9E6E8E9E6E8E9
      E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8EAE7E9EAE7E9E9E6E9E9E6E8E9E6E8
      E9E6E8E9E7E9EAE7E9EAE7E8EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE4E5EAE4
      E5EAE4E5E9E4E5E8E6E6E8E6E6E7E5E5E7E5E5E7E5E5E7E5E5E6E4E4E6E4E4E7
      E4E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8
      E2E2E7E1E2E7E1E2E7E1E2E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1
      E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5
      DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFDFE4DEDFE4DEDFE4DEDF
      E4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE3DEDEE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2
      DEDDE2DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE4DEDFE4DE
      DFE4DEDFE3DDDFE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DDDDE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E2DDDCE2DDDCE3DEDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE1DCDBE1DCDBE1DCDB
      E1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDBE2DDDCE2DDDCE1DCDBE1DCDBE1DC
      DBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE2DDDBE2DD
      DCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBD9DFDAD9DFDAD9DFD9D8DED9D8DED9D8DED9D8DED8
      D8DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DBD6D6DBD6D5DBD6D5DBD6D5DBD6D5DCD6D6DCD7D6DCD7D6DBD6D6DBD6
      D5DBD6D5DBD6D5DBD6D5DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DA
      D5D4DAD5D4DAD5D4DAD5D4DAD5D3DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5
      D2DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DDD5D5DCD4D4DC
      D4D4DCD4D4DCD4D4DCD4D4DBD4D4DBD3D3DBD3D3DAD3D3D8D4D3D8D4D3D8D4D3
      D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D9D5D3DAD5D4DAD5D4DAD5D4DAD5D4DAD5
      D4DAD5D4DAD5D4DAD5D4D8D4D2D6D4D0D7D4D0D7D4D0D8D4D0D8D3D0D8D3D0DA
      D3D0DAD3D0DAD2D0DAD2CFDAD2CFDAD2CFDAD2CFDAD2CFDAD2CFDAD2CFDAD2CF
      D7D2CFD6D2CFD6D2CFD6D2CFD6D2CFD6D2CFD6D2CFD6D2CFD6D2CFD6D2CED6D1
      CED6D1CED5D1CDD5D0CDD5D0CDD6D1CED6D1CED6D1CED6D1CED7D1CFD7D2CFD7
      D2CFD8D2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDAD3CF
      D8D5D1D8D5D1D8D5D1D8D5D1D8D5D1D8D5D1D8D5D1D8D5D1D9D4D1DAD3D0DAD3
      D0D8D3D0D8D3D0D8D4D0D7D4D0D7D4D0D5D4D0D8D4D2DAD5D4DAD5D4DAD5D4DA
      D5D4DAD5D4DAD5D4DAD5D4DAD5D4DDD5D4DED4D4DED4D4DED4D4DED4D4DED4D4
      DED4D4DED4D4DED4D4DCD4D4DCD4D4DCD4D4DCD4D4DDD5D5DDD5D5DDD5D5DDD5
      D5DED6D6DBD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD6D5DA
      D5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D3DAD5D4DAD5D4DAD5D4DAD5
      D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DBD6D5DBD6D5DBD6D5DB
      D6D5DBD6D6DCD7D6DCD7D6DCD6D6DBD6D5DBD6D5DBD6D5DBD6D5DBD6D6DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DE
      D8D8DED9D8DED9D8DED9D8DFD9D8DFDAD9DFDAD9E0DBD9E0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2
      DDDCE2DDDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1
      DCDBE1DCDBE1DCDBE2DDDCE2DDDCE2DDDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDB
      E1DCDBE1DCDBE1DCDBE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDCE2DDDCE2DDDC
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE3DDDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDFE4DEDFE4
      DEDFE4DEDFE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DEDDE2DEDDE2DE
      DDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDEE4DEDFE4DEDFE4DEDFE4DEDFE4DEDF
      E4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DE
      DFE4DEDFE4DFE0E5DFE0E5DFE0E5DFE0E6E0E0E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1
      E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E7E1E2E7E1E2E7E1E2E8E2
      E2E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E7E4
      E4E6E4E4E6E4E4E7E5E5E7E5E5E7E5E5E7E5E5E8E6E6E8E6E6E9E4E5EAE4E5EA
      E4E5EAE4E5EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE7E8EAE7E9E9E7E9E9E6E8
      E9E6E8E9E6E8E9E6E9EAE7E9EAE7E9E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6
      E8E9E6E8E9E6E8E9E6E8EBE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8EDE8E8EEE8E9EEE8E9EEE8E9EEE8
      E9EEE8E9EEE8E9EFE9E9EFE9EAEFE9EAEFE9EAEDE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEDEAEBEDEAECEDEAECEDEAEC
      EDEAECEDEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEB
      EDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEEEFECEEEFECEEEFECEEEF
      ECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEC
      EEEDECEEEDECEFEEEDEFEEEDEFEEEDEFEEEDEFEFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EFEFF0F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F2F0F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F1F1F3F1F0F2F1F0F2F1F0F2F1F0F2F0F0F1F0EFF1F1F0
      F2F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1EFF1F0EFF1F0EFF1F0EFF1F0EFF1EFEFF1EFEEF0EFEEF0EFEE
      F0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDEDEEEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEEECECEEECECEEECECEEECECEEECECEEEBEBEDEBEBEDEBEBECEAEAECEAEAEC
      EBEBEDEBEBEDEBEBEDEBEBEDEBEBEEECECEEECECEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBECEAEBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEBE8EA
      EBE8EAEBE8EAEBE8EAEDE8E9EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6E9E6E6E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E7E6E6E7E5E5E7E5E5E8E6E6E8E6E6E8E6
      E6E8E6E6E7E5E5E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EA
      E4E5EAE4E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5
      E7E5E5E7E5E5FFFFFFFFFFFFFFFFFFFFFFFFE7E5E5E7E5E5E7E5E5E7E5E5E7E5
      E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EA
      E4E5EAE4E5EAE4E5EAE4E5EAE4E5E7E5E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6
      E7E5E5E7E5E5E7E6E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E6E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE8E9
      EEE8E9EEE8E9EEE8E9EEE8E9EDE8E9EBE8EAEBE8EAEBE8EAEBE8EAECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECEAEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDECECEEECECEEECECEEECECEE
      ECECEEECECEEECECEEECECEEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECECEEECECEEECECEEECECEEECECEF
      ECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEEEEF0EEEEEFEEEEEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEDEDEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEEDEFEEEDEFEEEDEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEEEDEFEFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EEEDF0
      EEEDEFEEEDEFEEEDF0EFEEF0EFEEF0EFEEF0EFEEF0F0EEF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0F0F1
      F1F0F2F1F0F2F1F0F2F1F0F2F1F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F1F0F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1EFF1F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F0F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1EFEFF0EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EEEDEFEEEDEFEEEDEFEEEDEFEDECEFEDECEEEEECEEEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEFEC
      EEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEFECEEEEEBEDEEEBEDED
      EAEDEDEAECEDEAECEDEAECEDE9EBECE9EBEDEAEBEEEBEDEEEBEDEEEBEDEEEBED
      EEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEDEBEDEDEAECEDEAECEDEAECEDEA
      ECEDEAEBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBEDE9EAEEE8E9EEE8E9EEE7E9EDE7E8EDE7E8EDE7E8EDE7E8ECE7E7EEE8E8
      EEE8E9EDE8E8EDE7E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7EBE6E7E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8
      E9E6E8E9E6E8E9E6E8EAE7E9EAE7E9E9E6E9E9E6E8E9E6E8E9E6E8E9E7E9EAE7
      E9EAE7E8EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE4E5EAE4E5EAE4E5E9E4E5E8
      E6E6E8E6E6E7E5E5E7E5E5E7E5E5E7E5E5E6E4E4E6E4E4E7E4E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E8E2E3E8
      E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E2E7E1E2E7E1E2
      E7E1E2E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0
      E5DFE0E5DFE0E5DFE0E5DFE0E5DFDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DE
      DFE4DEDFE4DEDFE3DEDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDD
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE4DEDFE4DEDFE4DEDFE3DDDFE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DDDDE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE2DDDCE3DE
      DCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DC
      DBE1DCDBE1DCDBE2DDDBE2DDDCE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE2DDDBE2DDDCE1DCDBE1DCDBE1
      DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBD9DFDAD9DFDAD9DFD9D8DED9D8DED9D8DED9D8DED8D8DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DBD6D6DBD6
      D5DBD6D5DBD6D5DBD6D5DCD6D6DCD7D6DCD7D6DBD6D6DBD6D5DBD6D5DBD6D5DB
      D6D5DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4
      DAD5D4DAD5D3DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DFD5D5DFD5D5DF
      D5D5DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5DCD4D4DCD4D4DCD4D4DCD4D4
      DBD4D4DBD3D3DBD3D3DBD3D3DAD3D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4
      D3D8D4D3D8D4D3D9D5D3DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DA
      D5D4D8D4D2D5D4D0D7D4D0D7D4D0D8D4D0D8D3D0D8D3D0DAD3D0DAD3D0DBD2D0
      DBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFD7D3CFD6D3CFD6D3
      CFD6D3CFD6D3CFD6D3CFD6D3CFD6D3CFD6D3CFD6D2CED6D1CED6D1CDD5D0CDD5
      D0CDD5D0CDD6D1CED7D2CFD7D2CFD7D2CFD7D2CFD8D3D0D8D3D0D9D3D0DBD2CF
      DBD2CFDBD2CFDBD2CFDBD2CFDCD2CFDCD3D0DCD3D0DBD3D0D8D4D1D8D4D1D8D4
      D1D8D4D1D8D4D1D8D4D1D8D4D1D8D4D1D9D4D1D9D4D1D9D4D1D8D4D1D8D4D1D8
      D5D1D8D5D1D8D5D1D7D5D1D9D4D2DBD5D2DBD5D2DBD5D2DBD5D2DBD5D2DBD5D3
      DBD5D3DBD5D3DDD5D4DED4D4DED4D4DED4D4DED4D4DED4D4DED4D4DED4D4DED4
      D4DBD4D4DAD4D4DBD5D5DBD5D5DBD5D5DBD5D5DBD5D5DCD6D6DCD6D6DBD6D5DB
      D6D5DBD6D5DAD6D5DAD6D5DAD6D5DAD6D5DAD5D4DAD5D4DAD5D2DAD5D2DAD5D2
      DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D2DCD5D2DCD5D2DBD5D2DBD5D2DAD5D2DAD5D2DA
      D5D2D9D5D2DAD5D3DAD5D4DAD5D4DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5
      DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D6DCD7D6DCD7D6DDD7D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DED8D8DED9D8DED9D8
      DED9D8DED9D8DED9D8DED9D8DFD9D9DFDAD9DFDAD9DFDAD9DFDAD9DFDAD9DFDA
      D9DFDAD9DFDAD9DFDBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE1DCDBE1DCDBE1DCDBE1DCDB
      E1DCDBE0DBDAE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DC
      DBE2DDDCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DEDDE2
      DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DDDCE2DDDCE2DDDC
      E2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE3DEDDE3DEDDE3DE
      DDE3DEDDE3DEDDE2DDDCE3DDDDE3DDDDE3DDDDE3DDDDE3DDDDE3DDDDE3DDDDE3
      DDDDE3DDDDE3DDDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2
      DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDEE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DE
      DFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DFE0E5
      DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E6DFE1E6E0E1
      E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E7E1E2E7E1E2E7E1E2E8E2E2E8E2E3E8E2E3E8
      E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E9E3E4E9E3E4E9E3E4E9E3E4
      E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E7E4E4E6E4E4E6E4E4E7
      E5E5E7E5E5E7E5E5E7E5E5E8E6E6E8E6E6E7E4E5E8E4E5E8E4E5E8E4E6E9E5E6
      E9E5E6E9E5E6E9E5E6E9E5E7E8E5E8E8E5E7E8E5E7E8E5E7E8E5E7E9E6E8E9E6
      E8E9E6E8E9E6E8E9E6E8E9E6E7E9E6E7E9E6E7E9E6E7E9E6E7E9E6E7E9E6E7E9
      E6E7EBE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E8EDE7
      E8EDE7E8EEE7E9EDE7E8EDE8E8EEE8E9EDE8E9ECE7E9ECE8E9ECE8E9ECE8EAED
      E9EAEDE9EAEDE9EAEDE9EAECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBEDEAEBEDEAECEDEAECEDEAECEDEAECEDEAECEDEA
      ECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEBEDEE
      EBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEDEBEDEDEBEDEDEBED
      EDEBEDEDEBEDEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEECEEEEED
      EFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEFEEEFEEEEF0EEEDEFEEEDEFEEEDEF
      EFEDEFEFEFF0EFEFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1F0F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F1F1F3F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EF
      EEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0
      EFEEF0EFEEF0EFEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDEDEEEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDED
      EDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEEEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEEDEDEFECECEEECECEEECECEE
      ECECEEECECEEECECEEECECEEECECEEECECEDEBEBEDEBEBEDECECEEECECEEECEC
      EEECECEEECECEEECECEEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEB
      EDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEAEBECE9EBECE9EBECE9
      EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEBE8EAEBE8EAEBE8EAEBE8
      EAEDE9E9EFE9E9EEE8E9EDE7E8ECE6E7ECE6E7EDE7E8EDE7E8EEE8E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE6E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE5E6E9E6E6E9E6E6E9
      E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E7E7E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6E8
      E6E6E8E5E5E8E4E5E8E4E5E8E4E5E8E4E5E8E4E5E8E4E5E8E4E5E8E4E5E7E5E5
      E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5FFFF
      FFFFFFFFFFFFFFFFFFFFE7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7
      E5E5E7E5E5E7E5E5E7E5E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5EAE4E5
      EAE4E5EAE4E5E7E5E5E7E5E5E8E6E6E8E6E6E8E6E6E8E6E6E7E5E5E7E5E5E7E6
      E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E6E6EBE5E6EBE5E6EBE5E6
      EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8ED
      E7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE8E9EEE8E9EEE8E9EEE8
      E9EEE8E9EDE8E9EBE8EAEBE8EAEBE8EAEBE8EAECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECEAEBEDEBEBEDEBEBEDEB
      EBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBEDEBEBED
      EBEBEDEBEBEDEBEBEDEBEBEDECECEEECECEEECECEEECECEEECECEEECECEEECEC
      EEECECEEECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEEECECEEECECEEECECEEECECEEECECEEECECEFECECEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEEEEF0EEEEEFEEEEEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EDEDEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEE
      EDEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEEDEFEFEEF0
      EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EEEDF0EEEDEFEEEDEFEEED
      F0EFEEF0EFEEF0EFEEF0EFEEF0F0EEF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0F0F1F1F0F2F1F0F2F1F0
      F2F1F0F2F1F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F1F0F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F1EFF1F1F0F2F1F0F2F1F0F2F1
      F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2
      F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F0F0F2F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1EFEFF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EEEDEF
      EEEDEFEEEDEFEEEDEFEDECEFEDECEEEEECEEEEEDEFEEEDEFEEEDEFEEEDEFEEED
      EFEEEDEFEEEDEFEEEDEFEEECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEFECEEEFECEEEFECEEEF
      ECEEEFECEEEFECEEEFECEEEFECEEEFECEEEEEBEDEEEBEDEDEAEDEDEAECEDEAEC
      EDEAECEDE9EBECE9EBEDEAEBEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEBEDEEEB
      EDEEEBEDEEEBEDEEEBEDEDEBEDEDEAECEDEAECEDEAECEDEAECEDEAEBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEDE9EAEEE8E9
      EEE8E9EEE7E9EDE7E8EDE7E8EDE7E8EDE7E8ECE7E7EEE8E8EEE8E9EDE8E8EDE7
      E8EDE7E8EDE7E8EDE7E8ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      ECE6E7EBE6E7E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6E8E9E6
      E8EAE7E9EAE7E9E9E6E9E9E6E8E9E6E8E9E6E8E9E7E9EAE7E9EAE7E8EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EAE4E5EAE4E5EAE4E5E9E4E5E8E6E6E8E6E6E7E5E5
      E7E5E5E7E5E5E7E5E5E6E4E4E6E4E4E7E4E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E8E2E3E8E2E3E8E2E3E8E2E3
      E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3E8E2E2E7E1E2E7E1E2E7E1E2E6E0E1E6E0
      E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E0
      E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DF
      E0E5DFE0E5DFDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE3
      DEDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DEDDE2DEDD
      E2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE3DEDDE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE4DEDFE4DEDFE4DEDFE3DDDFE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DDDDE2DD
      DCE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DDDCE2DDDCE3DEDCE3DEDDE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DDE3DEDDE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2
      DDDBE2DDDCE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE2DDDBE2DDDCE1DCDBE1DCDBE1DCDBE1DCDBE1DCDB
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBD9DFDAD9DF
      DAD9DFD9D8DED9D8DED9D8DED9D8DED8D8DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8
      D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DBD6D6DBD6D5DBD6D5DBD6D5DB
      D6D5DCD6D6DCD7D6DCD7D6DBD6D6DBD6D5DBD6D5DBD6D5DBD6D5DAD5D4DAD5D4
      DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D3DAD5
      D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DA
      D5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DFD5D5DFD5D5DFD5D5DFD5D5DFD5D5
      DFD5D5DFD5D5DFD5D5DFD5D5DCD4D4DCD4D4DCD4D4DCD4D4DBD4D4DBD3D3DBD3
      D3DBD3D3DAD3D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D8D4D3D9
      D5D3DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4DAD5D4D8D4D2D5D4D0
      D7D4D0D7D4D0D8D4D0D8D3D0D8D3D0DAD3D0DAD3D0DBD2D0DBD2CFDBD2CFDBD2
      CFDBD2CFDBD2CFDBD2CFDBD2CFDBD2CFD7D3CFD6D3CFD6D3CFD6D3CFD6D3CFD6
      D3CFD6D3CFD6D3CFD6D3CFD6D2CED6D1CED6D1CDD5D0CDD5D0CDD5D0CDD7D2CF
      D7D2CFD8D3D0D8D3D0D8D3D0D8D3D0D8D3D0D9D3D0DBD2CFDBD2CFDBD2CFDCD3
      D0DCD3D0DDD3D0DDD4D1DDD4D1DCD4D1D9D4D1D9D4D1D9D4D1D9D4D1D9D4D1D9
      D4D1D9D4D1D9D4D1D9D5D2D9D6D2D9D6D2D9D6D2D9D6D2D9D6D2D9D6D2D9D6D2
      D9D6D2DBD5D2DCD5D2DCD5D2DCD5D2DCD5D2DCD5D2DDD5D3DDD6D3DDD6D3DED5
      D4DED4D4DED4D4DED4D4DED4D4DED4D4DED4D4DED4D4DED4D4DBD5D4DAD5D4DB
      D5D5DBD6D5DBD6D5DBD6D5DBD6D5DCD7D6DCD7D6DCD7D6DCD7D6DCD7D5DBD6D5
      DBD6D5DBD6D5DBD6D5DAD5D4DAD5D4DBD6D3DBD6D3DBD6D3DBD6D3DBD6D3DBD6
      D3DBD6D3DBD6D3DBD6D3DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DAD5D2DA
      D5D2DBD5D2DED5D2DED5D2DCD5D2DCD5D2DAD5D2DAD5D2DAD5D2D9D6D2DAD6D3
      DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6D5DBD6
      D5DBD6D5DCD7D6DCD7D6DCD7D6DCD7D6DCD7D6DDD7D7DDD8D7DDD8D7DDD8D7DD
      D8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7DDD8D7
      DDD8D7DDD8D7DDD8D7DDD9D8DED9D8DED9D8DED9D8DED9D8DED9D8DED9D8DED9
      D8DED9D8DFD9D9DFDAD9DFDAD9DFDAD9DFDAD9DFDAD9DFDAD9DFDAD9DFDAD9DF
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DB
      DAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0
      DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDAE0DBDA
      E0DBDAE0DBDAE0DCDAE1DCDBE1DCDBE2DDDCE2DDDCE1DCDBE1DCDBE0DBDAE1DC
      DBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE1DCDBE2DDDCE3DEDDE3
      DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE2DEDDE2DEDDE2DEDDE2DEDD
      E2DEDDE2DEDDE2DEDDE2DEDDE2DEDDE2DDDCE2DDDCE2DDDCE2DDDCE2DDDCE2DD
      DCE2DDDCE2DDDCE2DDDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3
      DEDDE3DEDDE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DD
      DEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDE
      E3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE4DEDFE4DEDFE4DEDFE4DE
      DFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDEE3DDDEE3DDDEE3DDDEE3DDDEE3DDDEE3
      DDDEE3DDDEE3DDDEE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDD
      E3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DEDDE3DE
      DEE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4DEDFE4
      DEDFE5DEE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0
      E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E5DFE0E6DFE1E6E0E1E6E0E1E6E0E1E6E0
      E1E7E0E1E7E1E2E7E1E2E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6E0E1E6
      E0E1E6E0E1E7E1E2E7E1E2E7E1E2E7E1E2E8E2E3E8E2E3E8E2E3E8E2E3E8E2E3
      E8E2E3E8E2E3E8E2E3E8E2E3E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3
      E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9E3E4E9
      E3E4E9E3E4E9E3E4E9E3E4E9E3E4E7E4E4E6E4E4E6E4E4E7E5E5E7E5E5E7E5E5
      E7E5E5E8E6E6E8E6E6E7E4E6E7E4E6E7E4E6E7E4E7E8E5E7E8E5E7E8E5E7E8E5
      E7E8E6E8E8E5E8E8E5E7E8E5E7E8E5E7E8E5E7E9E6E8E9E6E8E9E6E8E9E6E8E9
      E7E8E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7EBE6E7ECE6E7
      ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6
      E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EDE7E7EDE7E9EEE8E9EEE8E9EFE8EAEE
      E8E9EEE8E9EEE8E9EDE8E9EBE7E9EBE8EAECE8EAECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECEA
      ECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECED
      EAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEAECEDEBEDEEEBEDEEEBEDEEEBED
      EEEBEDEEEBEDEEEBEDEEEBEDEEEBEDECEBEDECEBEDECEBEDECEBEDEDEBEDEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEEECEEEEEDEFEEED
      EFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEF
      EEEDEFEEEDEFEEEDEFEFEEEFEEEEF0EEEDEFEEEDEFEEEDEFEFEDEFEFEFF0EFEF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0
      EFF1F0EFF1F0EFF1F0EFF1F1F0F2F1F0F2F1F0F2F1EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F1F0F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1
      F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2
      F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3F2F1F3
      F2F1F3F2F1F3F2F1F3F2F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0
      F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F1F0F2F0
      EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1
      F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EF
      F1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EFF1F0EEF0EF
      EEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0
      EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFEEF0EFED
      EFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEE
      EDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEEEDEFEDEDEEEDECEEEDECEEEDECEE
      EDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEC
      EEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEED
      ECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDECEEEDEDEE
      EDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDED
      EDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEEEDEDF0EEEEF0EEEEEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEEEDEDEFECECEFEDEDEFEDEDEFEDEDEFEDEDEFEDED
      EFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFED
      EDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEF
      EDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEFEDEDEEECECEEECEC
      EEECECEEECECEEECECEEECECEEECECEEECECEEECECEEECECEEECECEEECECEEEC
      ECEDEBEBEDEBEBEDEBEBEDEBEBEDEAEBECE9EBECE9EBECE9EBECE9EBECE9EBEC
      E9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EB
      ECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBECE9EBEDE9
      EBEDEAECEDEAECECE9EBECE9EBECE9EBECE9EBECE9EBEBE9EAEEEAEAF0EAEAEE
      E8E9EDE7E8ECE6E7ECE6E7EDE7E9EEE8E9EFE9E9EDE8E9EDE7E8EDE7E8EDE7E8
      EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7E8EDE7
      E8EDE7E8EDE7E8EDE7E8EEE8E9EEE8E9EEE7E8EDE7E8EDE7E8EDE7E8EDE6E8EC
      E6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7
      EDE7E8EDE7E8ECE7E7ECE6E7ECE6E7ECE6E7ECE6E7ECE6E7EBE5E6EBE5E6EBE5
      E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EBE5E6EB
      E5E6EBE5E6EBE5E6EBE5E6EBE5E6EAE6E6E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7
      E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7
      E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9
      E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E9E7E7E7E6E6E7E5E5
      E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5
      E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5FFFFFFFFFFFFFFFFFFFF
      FFFF}
    Left = 120
    Top = 144
  end
  object XPManifest1: TXPManifest
    Left = 40
    Top = 144
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 300
    OnTimer = miExitClick
    Left = 200
    Top = 144
  end
  object PopupMenu1: TPopupMenu
    Left = 200
    Top = 272
  end
  object Timer2: TTimer
    OnTimer = Timer2Timer
    Left = 120
    Top = 272
  end
  object SummOpenDialog: TOpenDialog
    DefaultExt = '*.a11'
    FileName = '*.a11'
    Filter = '*.a11'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Title = 
      #1057#1086#1077#1076#1080#1085#1077#1085#1080#1077' '#1092#1072#1081#1083#1086#1074' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1086#1074' '#1082#1086#1085#1090#1088#1086#1083#1103'. '#1042#1099#1073#1077#1088#1080#1090#1077' '#1089#1086#1077#1076#1080#1085#1103#1077#1084#1099#1077' '#1092#1072#1081 +
      #1083#1099
    Left = 40
    Top = 272
  end
  object SaveDialog1: TSaveDialog
    InitialDir = 'c:\'
    Title = #1056#1077#1079#1091#1083#1100#1090#1080#1088#1091#1102#1097#1080#1081' '#1092#1072#1081#1083'. '#1042#1099#1073#1077#1088#1080#1090#1077' '#1080#1084#1103' '#1092#1072#1081#1083#1072' '#1080' '#1082#1072#1090#1072#1083#1086#1075
    Left = 120
    Top = 208
  end
  object Timer3: TTimer
    Interval = 500
    OnTimer = Timer3Timer
    Left = 280
    Top = 80
  end
  object SelRailImageList: TImageList
    Width = 69
    Left = 280
    Top = 144
    Bitmap = {
      494C010103000500040045001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000140100001000000001002000000000000045
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000008482840084828400000000000000000000000000000000008482
      8400848284000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084828400848284000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000008482840084828400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008482
      8400848284000000000000000000000000000000000084828400848284000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008482840084828400000000000000000000000000000000008482
      8400848284000000000000000000000000000000000084828400848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000008482840084828400000000000000000000000000000000008482
      8400848284000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084828400848284000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000008482840084828400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008482
      8400848284000000000000000000000000000000000084828400848284000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008482840084828400000000000000000000000000000000008482
      8400848284000000000000000000000000000000000084828400848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000008482840084828400000000000000000000000000000000008482
      8400848284000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084828400848284000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000008482840084828400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008482
      8400848284000000000000000000000000000000000084828400848284000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008482840084828400000000000000
      0000000000000000000084828400848284000000000000000000000000000000
      0000848284008482840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000008482840084828400000000000000000000000000000000008482
      8400848284000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848284008482840000000000000000000000000000000000848284008482
      8400000000000000000000000000000000008482840084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008482
      8400848284000000000000000000000000000000000084828400848284000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008482840084828400000000000000
      0000000000000000000084828400848284000000000000000000000000000000
      0000848284008482840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000008482840084828400000000000000000000000000000000008482
      8400848284000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848284008482840000000000000000000000000000000000848284008482
      8400000000000000000000000000000000008482840084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008482
      8400848284000000000000000000000000000000000084828400848284000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008482840084828400000000000000
      0000000000000000000084828400848284000000000000000000000000000000
      0000848284008482840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000014010000100000000100010000000000400200000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFE00000000000000000000FFFFFFE79E7FFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFF9E79FFFFFFE00000000000000000000FFFFFFE79E7FFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFF9E79FFFFFFE00000000000000000000FFFFFFC0
      003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFE00000000000000000000
      FFFFFFC0003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFE000000000000
      00000000FFFFFFE79E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E79FFFFFFE0000
      0000000000000000FFFFFFE79E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E79FFF
      FFFE00000000000000000000FFFFFFE79E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      F9E79FFFFFFE00000000000000000000FFFFFFE79E7FFFFFFFFFFFFF3CF3FFFF
      FFFFFFFFFFFFFFFFFFFE00000000000000000000FFFFFFE79E7FFFFFFFFFFFFF
      3CF3FFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000FFFFFFE79E7FFFFF
      FFFFFFFF3CF3FFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000FFFFFFC0
      003FFFFFFFFFFFFE0001FFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000
      FFFFFFC0003FFFFFFFFFFFFE0001FFFFFFFFFFFFFFFFFFFFFFFE000000000000
      00000000FFFFFFE79E7FFFFFFFFFFFFF3CF3FFFFFFFFFFFFFFFFFFFFFFFE0000
      0000000000000000FFFFFFE79E7FFFFFFFFFFFFF3CF3FFFFFFFFFFFFFFFFFFFF
      FFFE00000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFE00000000000000000000}
  end
  object ChDirImageList: TImageList
    Width = 82
    Left = 280
    Top = 208
    Bitmap = {
      494C010103000500040052001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000480100001000000001002000000000000052
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF0000000000000000
      000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000000000000000000000000000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000000000000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000000000000000000000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF0000000000000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF00000000000000000000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF00000000000000000000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF000000000000FFFF
      000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF000000000000FFFF000000000000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF000000000000FFFF000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF000000000000FFFF000000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF0000FFFF0000FFFF
      0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      000000000000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF00000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000048010000100000000100010000000000C00200000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000FFFFFFFF
      80007FFFFFFFFFFFFFFFE0001FFFFFFFFFFFFFFFF80007FFFFFFFC0000000000
      0000000000000000FFFFFFFF80007FFFFFFFFFFFFFFFE0001FFFFFFFFFFFFFFF
      F80007FFFFFFFC00000000000000000000000000FFFFFFFF80007FFFFFFFFFFF
      FFFFE0001FFFFFFFFFFFFFFFF80007FFFFFFFC00000000000000000000000000
      FFFFFFFF80007FFFFFFFFFFFFFFFE0001FFFFFFFFFFFFFFFF80007FFFFFFFC00
      000000000000000000000000FFFFFFFF80007FFFFFFFFFFFFFFFE0001FFFFFFF
      FFFFFFFFF80007FFFFFFFC00000000000000000000000000FFFFFFFF80007FFF
      FFFFFFFFFFFFE0001FFFFFFFFFFFFFFFF80007FFFFFFFC000000000000000000
      00000000FFFFFFFF80007FFFFFFFFFFFFFFFE0001FFFFFFFFFFFFFFFF80007FF
      FFFFFC00000000000000000000000000FFFFFFFFFC0FFFFFFFFFFFFFFFFFFF03
      FFFFFFFFFFFFFFFFFFC0FFFFFFFFFC00000000000000000000000000FFFFFFFF
      FC0FFFFFFFFFFFFFFFFFFF03FFFFFFFFFFFFFFFFFFC0FFFFFFFFFC0000000000
      0000000000000000FFFFFFFFFC0FFFFFFFFFFFFFFFFFFF03FFFFFFFFFFFFFFFF
      FFC0FFFFFFFFFC00000000000000000000000000FFFFFFFFFC0FFFFFFFFFFFFF
      FFFFFF03FFFFFFFFFFFFFFFFFFC0FFFFFFFFFC00000000000000000000000000
      FFFFFFFFFC0FFFFFFFFFFFFFFFFFFF03FFFFFFFFFFFFFFFFFFC0FFFFFFFFFC00
      000000000000000000000000FFFFFFFFFC0FFFFFFFFFFFFFFFFFFF03FFFFFFFF
      FFFFFFFFFFC0FFFFFFFFFC00000000000000000000000000FFFFFFFFFEDFFFFF
      FFFFFFFFFFFFFFB7FFFFFFFFFFFFFFFFFFEDFFFFFFFFFC000000000000000000
      00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFC00000000000000000000000000}
  end
  object ViewModeImageList: TImageList
    Width = 82
    Left = 280
    Top = 272
    Bitmap = {
      494C010104000600040052001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000004801000020000000010020000000000000A4
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF00000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600000000000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF00000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C60000000000FF0000000000FF00FF0000000000FF00FF00
      00000000FF00FF0000000000FF00FF000000FF00000000000000C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF00000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600000000000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF00000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C60000000000FF0000000000FF00FF0000000000
      FF00FF0000000000FF00FF0000000000FF0000000000C6C3C600C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000008284000082840000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00008284000082
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF0000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF000000FF00000000000000C6C3C600C6C3C600C6C3C6000000
      00000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600000000000000FF00FF0000000000FF00FF00
      00000000FF00FF0000000000FF00FF00000000000000C6C3C600C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000082840000FFFF00008284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF0000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF000000FF00000000000000C6C3C600C6C3C600C6C3C6000000
      00000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600000000000000FF00FF0000000000
      FF00FF0000000000FF00FF00000000000000C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000082840000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF0000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF000000FF00000000000000C6C3C600C6C3C600C6C3C6000000
      00000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C60000000000FF0000000000FF00FF00
      00000000FF00FF0000000000FF0000000000C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000082840000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF000000FF000000FF00000000000000C6C3C600000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C60000000000FF0000000000
      FF00FF0000000000FF0000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000082840000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF000000FF000000FF000000FF00000000000000C6C3C600000000000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600000000000000FF00FF00
      00000000FF00FF00000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000082840000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF000000FF000000FF00
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C6000000
      00000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C3C6000000
      000000000000FF000000FF000000FF00000000000000C6C3C600000000000000
      FF000000FF000000FF000000000000000000C6C3C600C6C3C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600000000000000
      FF00FF00000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000082840000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C3C6000000000000000000FF00
      0000FF00000000000000C6C3C600C6C3C600C6C3C600C6C3C600000000000000
      FF000000FF000000000000000000C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C3C600C6C3
      C600C6C3C6000000000000000000FF000000FF000000000000000000FF000000
      FF000000000000000000C6C3C600C6C3C600C6C3C600C6C3C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C60000000000FF00
      00000000FF0000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000082840000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008482840084828400848284000000
      0000000000000000000084828400848284008482840084828400000000000000
      0000000000008482840084828400848284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848284008482
      8400848284008482840084828400000000000000000000000000000000000000
      0000848284008482840084828400848284008482840084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848284008482840084828400848284008482840084828400848284000000
      0000000000008482840084828400848284008482840084828400848284008482
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000082840000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000082840000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008284000082840000FFFF00008284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000082840000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000082840000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000082840000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000048010000200000000100010000000000800500000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFF0000FFFFFFFFFFFFFFFFC000
      3FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFE0007FFFFFFFF000000FFFFFFFF
      0000FFFFFFFFFFFFFFFFC0003FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFE00
      07FFFFFFFF000000FFFFFFFF0000FFFFFFFFFFFFFFFFC0003FFFFFFFFFFFFFFF
      F0000FFFFFFFFFFFFFFFFF801FFFFFFFFF000000FFFFFFFF0000FFFFFFFFFFFF
      FFFFC0003FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF8FFFFFFFFFF000000
      FFFFFFFF0000FFFFFFFFFFFFFFFFC0003FFFFFFFFFFFFFFFF0000FFFFFFFFFFF
      FFFFFFF8FFFFFFFFFF000000FFFFFFFF0000FFFFFFFFFFFFFFFFC0003FFFFFFF
      FFFFFFFFF0000FFFFFFFFFFFFFFFFFF8FFFFFFFFFF000000FFFFFFFF0000FFFF
      FFFFFFFFFFFFC0003FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF8FFFFFFFF
      FF000000FFFFFFFF0000FFFFFFFFFFFFFFFFC0003FFFFFFFFFFFFFFFF0000FFF
      FFFFFFFFFFFFFFF8FFFFFFFFFF000000FFFFFFFF0000FFFFFFFFFFFFFFFFC000
      3FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF8FFFFFFFFFF000000FFFFFFFF
      0000FFFFFFFFFFFFFFFFC0003FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF8
      FFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFF8FFFFFFFFFF000000FFFFFFFFE187FFFFFFFFFFFF
      FFFFFE0FFFFFFFFFFFFFFFFFFFC3FFFFFFFFFFFFFFFFFFE03FFFFFFFFF000000
      FFFFFFFFEDB7FFFFFFFFFFFFFFFFFEEFFFFFFFFFFFFFFFFFFFDBFFFFFFFFFFFF
      FFFFFFC01FFFFFFFFF000000FFFFFFFFEDB7FFFFFFFFFFFFFFFFFEEFFFFFFFFF
      FFFFFFFFFFDBFFFFFFFFFFFFFFFFFFC01FFFFFFFFF000000FFFFFFFFE187FFFF
      FFFFFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFC3FFFFFFFFFFFFFFFFFFC01FFFFFFF
      FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFE03FFFFFFFFF000000}
  end
  object ImageList2: TImageList
    Left = 200
    Top = 216
  end
  object WindowTileHorizontalTimer: TTimer
    Enabled = False
    Interval = 200
    OnTimer = WindowTileHorizontalTimerTimer
    Left = 280
    Top = 344
  end
  object AddRecPopupMenu: TPopupMenu
    Left = 40
    Top = 344
  end
  object ApplicationEvents1: TApplicationEvents
    OnMessage = ApplicationEvents1Message
    OnShortCut = ApplicationEvents1ShortCut
    Left = 616
    Top = 280
  end
  object GPS_SaveDialog: TSaveDialog
    Filter = '.gpx'
    FilterIndex = 0
    InitialDir = 'c:\'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = #1056#1077#1079#1091#1083#1100#1090#1080#1088#1091#1102#1097#1080#1081' '#1092#1072#1081#1083'. '#1042#1099#1073#1077#1088#1080#1090#1077' '#1080#1084#1103' '#1092#1072#1081#1083#1072' '#1080' '#1082#1072#1090#1072#1083#1086#1075
    Left = 136
    Top = 344
  end
end
