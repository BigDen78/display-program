object OperatorLogForm: TOperatorLogForm
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = #1061#1088#1086#1085#1086#1083#1086#1075#1080#1103' '#1088#1072#1073#1086#1090#1099
  ClientHeight = 400
  ClientWidth = 902
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 369
    Width = 902
    Height = 31
    Align = alBottom
    TabOrder = 0
    object Panel2: TPanel
      Left = 819
      Top = 1
      Width = 82
      Height = 29
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 6
        Top = 2
        Width = 73
        Height = 25
        Caption = #1047#1072#1082#1088#1099#1090#1100
        TabOrder = 0
        OnClick = Button1Click
      end
    end
  end
  object ListView1: TListView
    Left = 0
    Top = 0
    Width = 902
    Height = 369
    Align = alClient
    Columns = <
      item
        Caption = #1042#1088#1077#1084#1103
        Width = 70
      end
      item
        Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1072
        Width = 140
      end
      item
        Caption = #1048#1089#1090#1086#1095#1085#1080#1082
        Width = 100
      end
      item
        Caption = #1057#1086#1073#1099#1090#1080#1077
        Width = 200
      end
      item
        Caption = #1050#1072#1085#1072#1083' '#1082#1086#1085#1090#1088#1086#1083#1103
        Width = 225
      end
      item
        Caption = #1042#1088#1077#1084#1103' ('#1089#1077#1082'.)'
        Width = 80
      end>
    ColumnClick = False
    Groups = <
      item
        Header = #1054#1087#1077#1088#1072#1090#1086#1088
        GroupID = 0
        State = [lgsNormal]
        HeaderAlign = taLeftJustify
        FooterAlign = taLeftJustify
        TitleImage = -1
        ExtendedImage = -1
      end
      item
        Header = #1041#1059#1048
        GroupID = 1
        State = [lgsNormal]
        HeaderAlign = taLeftJustify
        FooterAlign = taLeftJustify
        Subtitle = #1099#1074#1099#1074
        TitleImage = -1
        ExtendedImage = -1
        SubsetTitle = #1077#1091#1099#1077
      end>
    ReadOnly = True
    RowSelect = True
    TabOrder = 1
    ViewStyle = vsReport
    OnClick = ListView1Click
  end
end
