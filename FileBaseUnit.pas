unit FileBaseUnit; {Language 5}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TB2Item, TB2ExtItems, TB2Dock, TB2Toolbar, ImgList, //DataBase,
  StdCtrls, ComCtrls, BaseCfg, BaseCfg2Unit, ExtCtrls, CommCtrl, Math, LanguageUnit;

type
  TFileBaseForm = class(TForm)
    TBDock1: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem1: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem2: TTBItem;
    ListBox1: TListBox;
    ListView1: TListView;
    TBItem4: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TreeView1: TTreeView;
    Splitter1: TSplitter;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    TBControlItem1: TTBControlItem;
    TBEditItem1: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TBItem1Click(Sender: TObject);
    procedure TBItem2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TBItem4Click(Sender: TObject);
    procedure TreeView1Click(Sender: TObject);
//    procedure ListView1AdvancedCustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState; Stage: TCustomDrawStage; var DefaultDraw: Boolean);
//    procedure ListView1AdvancedCustomDrawSubItem(Sender: TCustomListView; Item: TListItem; SubItem: Integer; State: TCustomDrawState; Stage: TCustomDrawStage; var DefaultDraw: Boolean);
    procedure ListView1DblClick(Sender: TObject);
    procedure ListView1Click(Sender: TObject);
    procedure OnChangeLanguage(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ListView1Changing(Sender: TObject; Item: TListItem; Change: TItemChange; var AllowChange: Boolean);
    procedure ListView1ColumnClick(Sender: TObject; Column: TListColumn);
    procedure ListView1Data(Sender: TObject; Item: TListItem);
    procedure ListView1CustomDrawSubItem(Sender: TCustomListView; Item: TListItem; SubItem: Integer; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure ListView1CustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure ListView1DataHint(Sender: TObject; StartIndex, EndIndex: Integer);
    function MySelectDirectory(const Caption: string; const Root: WideString; var Directory: string): Boolean;
  private
    FCurMinCoord: Integer;
    FCurMaxCoord: Integer;
    FSortIdx: array of array of Integer;
    FSortIdx_Item: Integer;
  public
    function DataToStr(ItemIndex, DataID: Integer; InsertName: Boolean): string;
    function CompareDate(ItemIndex1, ItemIndex2, DataID: Integer): Integer;
    procedure CalcMinMax;
    procedure CreateList;
    procedure SortList;          // ����������
    procedure SetUpColums;
  end;

  TTreeItem = record
    Name: string;
    ListItems: TListItems;
  end;

  TTreeItemList = array of TTreeItem;

implementation

uses
  MainUnit, Types, Avk11ViewUnit, ConfigUnit, ShlObj, ActiveX, AviconTypes, DataBase;

{$R *.dfm}

procedure TFileBaseForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  I, J: Integer;

begin
  for I:= 0 to ListView1.Columns.Count - 1 do
    for J:= 0 to High(Config.Fields) do
      if ListView1.Columns.Items[I].Caption = Config.Fields[J].Name then
        Config.Fields[J].Width:= ListView1.Columns.Items[I].Width;

  for I:= 0 to High(FSortIdx) do SetLength(FSortIdx, 0);

  Self.Release;
  MainUnit.MainForm.FileBaseForm:= nil;
end;

function SelectDirCB(Wnd: HWND; uMsg: UINT; lParam, lpData: LPARAM): Integer stdcall;
begin
  if (uMsg = BFFM_INITIALIZED) and (lpData <> 0) then
    SendMessage(Wnd, BFFM_SETSELECTION, Integer(True), lpdata);
  result := 0;
end;

function TFileBaseForm.MySelectDirectory(const Caption: string; const Root: WideString; var Directory: string): Boolean;
var
  WindowList: Pointer;
  BrowseInfo: TBrowseInfo;
  Buffer: PChar;
  OldErrorMode: Cardinal;
  RootItemIDList, ItemIDList: PItemIDList;
  ShellMalloc: IMalloc;
  IDesktopFolder: IShellFolder;
  Eaten, Flags: LongWord;

begin
  Result := False;
  if not DirectoryExists(Directory) then
    Directory := '';
  FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
  if (ShGetMalloc(ShellMalloc) = S_OK) and (ShellMalloc <> nil) then
  begin
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      RootItemIDList := nil;
      if Root <> '' then
      begin
        SHGetDesktopFolder(IDesktopFolder);
        IDesktopFolder.ParseDisplayName(Application.Handle, nil,
          POleStr(Root), Eaten, RootItemIDList, Flags);
      end;
      with BrowseInfo do
      begin
        hwndOwner := Application.Handle;
        pidlRoot := RootItemIDList;
        pszDisplayName := Buffer;
        lpszTitle := PChar(Caption);
        ulFlags := BIF_RETURNONLYFSDIRS or BIF_NEWDIALOGSTYLE;
        if Directory <> '' then
        begin
          lpfn := SelectDirCB;
          lParam := Integer(PChar(Directory));
        end;
      end;
      WindowList := DisableTaskWindows(0);
      OldErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS);
      try
        ItemIDList := ShBrowseForFolder(BrowseInfo);
      finally
        SetErrorMode(OldErrorMode);
        EnableTaskWindows(WindowList);
      end;
      Result :=  ItemIDList <> nil;
      if Result then
      begin
        ShGetPathFromIDList(ItemIDList, Buffer);
        ShellMalloc.Free(ItemIDList);
        Directory := Buffer;
      end;
    finally
      ShellMalloc.Free(Buffer);
    end;
  end;
end;

procedure TFileBaseForm.TBItem1Click(Sender: TObject);
var
  Dir: string;

begin
  Dir:= Config.BasePath;
  if MySelectDirectory(LangTable.Caption['FileBase:Selectdirectory'], '', Dir) then
  begin
    TBEditItem1.Text:= Dir;
    Config.BasePath:= TBEditItem1.Text;
  end;
end;

procedure TFileBaseForm.TBItem2Click(Sender: TObject);
begin
  ListView1.Items.Clear;
  DataBase_.Update(Config.BasePath);
  CreateList;
end;

procedure TFileBaseForm.FormShow(Sender: TObject);
begin
  TBEditItem1.Text:= Config.BasePath;
//  SetUpColums;
end;

procedure TFileBaseForm.SetUpColums;
var
  I, J: Integer;
  New: TListColumn;

begin
  Screen.Cursor:= crHourGlass;

  for I:= 0 to ListView1.Columns.Count - 1 do
    for J:= 0 to High(Config.Fields) do
      if ListView1.Columns.Items[I].Caption = Config.Fields[J].Name then Config.Fields[J].Width:= ListView1.Columns.Items[I].Width;

  ListView1.Items.BeginUpdate;
  ListView1.Columns.BeginUpdate;

  ListView1.Items.Clear;

  ListView1.Columns.Clear;

  for I:= 0 to High(Config.Fields) do
    if Config.Fields[I].Enabled and (Config.Fields[I].Mode = fmVisible) then
    begin
      New:= ListView1.Columns.Add;
      New.Caption:= Config.Fields[I].Name;
      New.Width:= Config.Fields[I].Width;
      New.Tag:= I;
    end;

  New:= ListView1.Columns.Add;
  New.Caption:= '';
  New.Width:= 10;


  CreateList;

  ListView1.Columns.EndUpdate;
  ListView1.Items.EndUpdate;

  Screen.Cursor:= crDefault;
end;

function TFileBaseForm.DataToStr(ItemIndex, DataID: Integer; InsertName: Boolean): string;
var
  S: string;
  I: Integer;
  J: Integer;
  SystemTime: TSystemTime;

function GetStartRealCrd(ItemIndex: Integer): TRealCoord;
begin
  with DataBase_.Item[ItemIndex] do
  begin
    Result.Sys:= TCoordSys(PathCoordSystem);
    if Result.Sys = csMetricRF then
    begin
      Result.MRFCrd.Km:= StartKM;
      Result.MRFCrd.Pk:= StartPk;
      Result.MRFCrd.mm:= StartMetre * 1000;
    end
    else Result.CaCrd:= StartChainage;
  end;
end;

function GetEndRealCrd(ItemIndex: Integer): TRealCoord;
begin
  with DataBase_.Item[ItemIndex] do
  begin
    Result.Sys:= TCoordSys(PathCoordSystem);
    if Result.Sys = csMetricRF then Result.MRFCrd:= EndMRFCrd
                               else Result.CaCrd:= EndCaCrd;
  end;
end;

begin
  {$Q+}
  if ItemIndex < DataBase_.Count then
    with DataBase_.Item[ItemIndex] do
      case DataID of

       - 1: begin
              if FCurMaxCoord = FCurMinCoord then
              begin
                Result:= '';
                Exit;
              end;

              if TCoordSys(PathCoordSystem) = csMetricRF then
              begin
                try
                  I:= StartKM * 1000 + StartPk * 100 + StartMetre;
                  J:= EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000;
                except
                end;
              end
              else
              if TCoordSys(PathCoordSystem) = csImperial then
              begin
                try
                  I:= Round(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0304.8);
                  J:= Round(CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0304.8);
                except
                end;
              end
              else
              if TCoordSys(PathCoordSystem) in [csMetric1km, csMetric1kmTurkish] then
              begin
                try
                  I:= Round(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 1000);
                  J:= Round(CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 1000);
                except
                end;
              end
              else
              begin
                try
                  I:= Round(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 100);
                  J:= Round(CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 100);
                except
                end;
              end;

              S:= Format('%1.8f', [(I - FCurMinCoord) / (FCurMaxCoord - FCurMinCoord)]) +
                  Format('%1.8f', [(J - FCurMinCoord) / (FCurMaxCoord - FCurMinCoord)]) +
                  Format('%10d', [MoveDir]);
              Result:= S;
            end;

        1: if Config.OnlyName then Result:= ExtractFileName(FileName)           // ��� �����;
                              else Result:= FileName;
        2: Result:= IntToStr(FileSize);                                         // ������ �����;
        3: begin
             S:= DateTimeToStr(FileDateTime);                                   // ���� � ����� �������� �����
             I:= System.Length(S);
             Delete(S, I - 4, 3);
             Result:= S;
           end;
        4: begin // ����� �������;

             Result:= '';

             if UnitsInfo[9].UnitType = dutFlawDetectorNumber then // ����� ������������ (����)
             begin
               Result:= HeaderStrToString(UnitsInfo[9].WorksNumber);
             end else
             for I := 0 to UnitsCount - 1 do
               if UnitsInfo[I].UnitType = dutUpUnit then
               begin
                 Result:= HeaderStrToString(UnitsInfo[I].WorksNumber);
                 Break;
               end;
           end;
//        5: Result:= LangTable.Caption[GetDeviceNameTextId(DeviceID)];                                                 // �����������;

        6: begin                              // ���� � ����� �������;
              SystemTime.wYear:=   Year;
              SystemTime.wMonth:=  Month;
              SystemTime.wDay:=    Day;
              SystemTime.wHour:=   Hour;
              SystemTime.wMinute:= Minute;
              try
                Result:= DateTimeToStr(SystemTimeToDateTime(SystemTime));
              except
                Result:= '';
              end;
           end;
        7: Result:= HeaderStrToString(RailRoadName); // �������� �.�.;
        8: begin                                          // ����� ��;
             Result:= HeaderStrToString(Organization);
             if InsertName then Result:= LangTable.Caption['FileBase:PChNo'] + ': ' + Result;
           end;
        9: begin                                          // ��� ���������;
             Result:= HeaderStrToString(OperatorName);
             if InsertName then Result:= LangTable.Caption['FileBase:Operator'] + ': ' + Result;
           end;
       10: begin                                          // ��� �����������;
             Result:= IntToStr(DirectionCode);
             if InsertName then Result:= LangTable.Caption['FileBase:DirectionCode'] + ': ' + Result;
           end;
       11: begin                                          // �������� �������;
             Result:= HeaderStrToString(PathSectionName);
             if InsertName then Result:= LangTable.Caption['FileBase:Tracksection'] + ': ' + Result;
           end;
       12: Result:= RealCrdToStr(GetStartRealCrd(ItemIndex));
//            Result:= Format('%d ' + LangTable.Caption['common:km'] +
//                           ' %d ' + LangTable.Caption['common:pk'] +
//                           ' %d '  + LangTable.Caption['common:m'], [StartKM, StartPk, StartMetre]);   // ��������� ����������

       13:
//           Result:= Format('%d ' + LangTable.Caption['common:km'] +
//                           ' %d ' + LangTable.Caption['common:pk'] +
  //                         ' %d '  + LangTable.Caption['common:m'], [EndMRFCrd.KM, EndMRFCrd.Pk, EndMRFCrd.MM div 1000]);         // �������� ����������
           Result:= RealCrdToStr(GetEndRealCrd(ItemIndex));
       14: begin
             Result:= HeaderStrToString(RailPathNumber);
             if InsertName then Result:= LangTable.Caption['common:Tracknumber'] + ': ' + Result;
           end;
       15: if MoveDir > 0 then
                  Result:= LangTable.Caption['InfoBar:MoveDirInc']     // ����������� ��������;
                          else
                  Result:= LangTable.Caption['InfoBar:MoveDirDec'];
       16: Result:= IntToStr(RailSection);                                           // �����;
//       17: Result:= IntToStr(FileContent);                                              // ���������� �����;
//       18: Result:= IntToStr(EnCodeProc);                                               // �����������
//       19: Result:= EnCodeFIO;                                                          // �������������
//       20: Result:= IntToStr(Ord(LabelExts));                                           // ������� �����
//       21: Result:= IntToStr(Ord(LabelExts));                                           //
//       22: case FileContent of
//             0: Result:= '';
//             1: Result:= LangTable.Caption['common:Left'];
//             2: Result:= LangTable.Caption['common:Right'];
//           end;
       23: begin
             S:= Format('%d', [Length]);
             if System.Length(S) > 3 then Insert(' ', S, System.Length(S) - 2);
             Result:= S + ' ' + LangTable.Caption['common:m'];
           end;
       24: Result:=  LangTable.Caption['InfoBar:' + GetDeviceNameTextId(DeviceID) + Config.GetProgramModeId];
      end;
  {$Q-}
end;

function TFileBaseForm.CompareDate(ItemIndex1, ItemIndex2, DataID: Integer): Integer;
const
  List: array [1..17] of Integer = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 23);

var
  S: string;
  I, J: Integer;

function Comp_ID(ID1, ID2: TAviconID): Integer;
var
  I: Integer;

begin
  Result:= 0;
  for I := 0 to 7 do
    if ID1[I] < ID1[I] then
    begin
      Result:= - 1;
      Break;
    end
    else
    if ID1[I] > ID1[I] then
    begin
      Result:= 1;
      Break;
    end;
end;

function Comp_Float(Float1, Float2: Extended): Integer;
begin
  if Float1 < Float2 then Result:= - 1 else
    if Float1 > Float2 then Result:= 1 else
      if Float1 = Float2 then Result:= 0;
end;

function Comp_Int(Int1, Int2: Integer): Integer;
begin
  if Int1 < Int2 then Result:= - 1 else
    if Int1 > Int2 then Result:= 1 else
      if Int1 = Int2 then Result:= 0;
end;

function Comp_Str(Str1, Str2: string): Integer;
begin
  if Str1 < Str2 then Result:= - 1 else
    if Str1 > Str2 then Result:= 1 else
      if Str1 = Str2 then Result:= 0;
end;

function Comp_DT(DT1, DT2: TDateTime): Integer;
begin
  if DT1 < DT2 then Result:= - 1 else
    if DT1 > DT2 then Result:= 1 else
      if DT1 = DT2 then Result:= 0;
end;

function Comp(ItemIndex1, ItemIndex2, DataID: Integer): Integer;
var
  I: Integer;
  S1, S2: string;
  DT1, DT2: TDateTime;
  SystemTime: TSystemTime;

begin
  with DataBase_.Item[ItemIndex1] do
    case DataID of
                                                                                      // ��� �����;
      1: if Config.OnlyName then Result:= Comp_Str(ExtractFileName(DataBase_.Item[ItemIndex2].FileName), ExtractFileName(FileName))
                            else Result:= Comp_Str(DataBase_.Item[ItemIndex2].FileName, FileName);
      2: Result:= Comp_Int(DataBase_.Item[ItemIndex2].FileSize, FileSize);             // ������ �����;
      3: Result:= Comp_DT(DataBase_.Item[ItemIndex2].FileDateTime, FileDateTime);      // ���� � ����� �������� �����;
        4: begin // ����� �������;
             S1:= '';
             if DataBase_.Item[ItemIndex2].UnitsInfo[9].UnitType = dutFlawDetectorNumber then // ����� ������������ (����)
             begin
               S1:= HeaderStrToString(DataBase_.Item[ItemIndex2].UnitsInfo[9].WorksNumber);
             end else
             for I := 0 to DataBase_.Item[ItemIndex2].UnitsCount - 1 do
               if DataBase_.Item[ItemIndex2].UnitsInfo[I].UnitType = dutUpUnit then
               begin
                 S1:= HeaderStrToString(DataBase_.Item[ItemIndex2].UnitsInfo[I].WorksNumber);
                 Break;
               end;

             S2:= '';
             if UnitsInfo[9].UnitType = dutFlawDetectorNumber then // ����� ������������ (����)
             begin
               S2:= HeaderStrToString(UnitsInfo[9].WorksNumber);
             end else
             for I := 0 to UnitsCount - 1 do
               if UnitsInfo[I].UnitType = dutUpUnit then
               begin
                 S2:= HeaderStrToString(UnitsInfo[I].WorksNumber);
                 Break;
               end;
             Result:= Comp_Str(S1, S2);
           end;
{        5: begin
             S1:= LangTable.Caption[GetDeviceNameTextId(DataBase_.Item[ItemIndex2].DeviceID)];                                                 // �����������;
             S2:= LangTable.Caption[GetDeviceNameTextId(DeviceID)];                                                 // �����������;
             Result:= Comp_Str(S1, S2);
           end; }
        6: begin                              // ���� � ����� �������;
              SystemTime.wYear:=   DataBase_.Item[ItemIndex2].Year;
              SystemTime.wMonth:=  DataBase_.Item[ItemIndex2].Month;
              SystemTime.wDay:=    DataBase_.Item[ItemIndex2].Day;
              SystemTime.wHour:=   DataBase_.Item[ItemIndex2].Hour;
              SystemTime.wMinute:= DataBase_.Item[ItemIndex2].Minute;
              DT1:= SystemTimeToDateTime(SystemTime);

              SystemTime.wYear:=   Year;
              SystemTime.wMonth:=  Month;
              SystemTime.wDay:=    Day;
              SystemTime.wHour:=   Hour;
              SystemTime.wMinute:= Minute;
              DT2:= SystemTimeToDateTime(SystemTime);

              Result:= Comp_DT(DT1, DT2);              // ���� � ����� �������;
           end;


      7: Result:= Comp_Str(HeaderStrToString(DataBase_.Item[ItemIndex2].RailRoadName),
                          HeaderStrToString(RailRoadName));             // �������� �.�.;
      8: Result:= Comp_Str(HeaderStrToString(DataBase_.Item[ItemIndex2].Organization),
                           HeaderStrToString(Organization));                       // ��
      9: Result:= Comp_Str(HeaderStrToString(DataBase_.Item[ItemIndex2].OperatorName),
                           HeaderStrToString(OperatorName));             // ��� ���������;
     10: Result:= Comp_Int(DataBase_.Item[ItemIndex2].DirectionCode, DirectionCode);   // ��� �����������;
     11: Result:= Comp_Str(HeaderStrToString(DataBase_.Item[ItemIndex2].PathSectionName),
                           HeaderStrToString(PathSectionName));               // �������� �������;
     12: begin
           if TCoordSys(PathCoordSystem) <> csMetricRF then
           begin
             Result:= Comp_Float(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage), CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd));
           end
           else
           begin
             Result:= Comp_Int(DataBase_.Item[ItemIndex2]. StartKM, StartKM);
             if Result = 0 then Result:= Comp_Int(DataBase_.Item[ItemIndex2].StartPK, StartPK);
             if Result = 0 then Result:= Comp_Int(DataBase_.Item[ItemIndex2].StartMetre, StartMetre);
           end;
         end;
     13: begin
           if TCoordSys(PathCoordSystem) <> csMetricRF then
           begin
             Result:= Comp_Float(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage), CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd));
           end
           else
           begin
             Result:= Comp_Int(DataBase_.Item[ItemIndex2].EndMRFCrd.KM, EndMRFCrd.KM);
             if Result = 0 then Result:= Comp_Int(DataBase_.Item[ItemIndex2].EndMRFCrd.PK, EndMRFCrd.PK);
             if Result = 0 then Result:= Comp_Int(DataBase_.Item[ItemIndex2].EndMRFCrd.MM, EndMRFCrd.MM);
           end;
         end;
     14: Result:= Comp_Str(HeaderStrToString(DataBase_.Item[ItemIndex2].RailPathNumber), HeaderStrToString(RailPathNumber)); // ����;
     15: Result:= Comp_Int(DataBase_.Item[ItemIndex2].MoveDir, MoveDir);
     16: Result:= Comp_Int(DataBase_.Item[ItemIndex2].RailSection, RailSection); // �����;
     23: Result:= Comp_Int(MainUnit.DataBase_.Item[ItemIndex2].Length, Length);
     24: begin

        // Result:= Comp_ID(MainUnit.DataBase_.Item[ItemIndex2].DeviceID, DeviceID);
           S1:= LangTable.Caption[GetDeviceNameTextId(DataBase_.Item[ItemIndex2].DeviceID)];                                                 // �����������;
           S2:= LangTable.Caption[GetDeviceNameTextId(DeviceID)];                                                 // �����������;
           Result:= Comp_Str(S1, S2);
         end;

    end;
end;

begin
  Result:= Comp(ItemIndex1, ItemIndex2, DataID);
  if Result = 0 then
    for J:= 1 to 17 do
      if List[J] <> DataID then
      begin
        Result:= Comp(ItemIndex1, ItemIndex2, List[J]);
        if Result <> 0 then Break;
      end;
end;

procedure TFileBaseForm.CreateList;
var
  I, J, K, L, Col: Integer;
  S: string;
  Cur: array [0..100] of TTreeNode;
  F: Boolean;
  Idx: Integer;

begin
  TreeView1.Items.Clear;
  TreeView1.Items.BeginUpdate;

  if Config.Fields[0].Mode = fmTree then
  begin
    for I:= 0 to High(FSortIdx) do SetLength(FSortIdx, 0);

    for I:= 0 to DataBase_.Count - 1 do
    begin
      for L:= 0 to 100 do Cur[L]:= nil;
      F:= True;
      for J:= 0 to High(Config.Fields) do  // ���������� �������
      begin
        if Config.Fields[J].Mode = fmTree then // ���� ������� ������� ����� ������
        begin
          if J = 0 then // ��� ������ ����� ������
          begin
            Cur[J]:= nil;
            for K:= 0 to TreeView1.Items.Count - 1 do
              if TreeView1.Items.Item[K].Text = DataToStr(I, Config.Fields[J].DataID, True) then
              begin
                Cur[J]:= TreeView1.Items.Item[K];
                Break;
              end;
            if Cur[J] = nil then   // ����� �����
            begin
              SetLength(FSortIdx, Length(FSortIdx) + 1);
              Cur[J]:= TreeView1.Items.AddObject(nil, DataToStr(I, Config.Fields[J].DataID, True), Pointer(High(FSortIdx)));
            end;
          end
          else
          begin
            Cur[J]:= nil;
            for K:= 0 to Cur[J - 1].Count - 1 do      // ��������� ����� ����� ��� ���� ?
              if Cur[J - 1].Item[K].Text = DataToStr(I, Config.Fields[J].DataID, True) then
              begin
                Cur[J]:= Cur[J - 1].Item[K];
                Break;
              end;
            if Cur[J] = nil then   // ����� �����
            begin
              SetLength(FSortIdx, Length(FSortIdx) + 1);
              Cur[J]:= TreeView1.Items.AddChildObject(Cur[J - 1], DataToStr(I, Config.Fields[J].DataID, True), Pointer(High(FSortIdx)));
            end;
          end;
          L:= J;
        end
        else
        if F and (Config.Fields[J].Mode = fmVisible) then // ���� ������� ������� �������
        begin
          Idx:= Integer(Cur[L].Data);
          SetLength(FSortIdx[Idx], Length(FSortIdx[Idx]) + 1);
          FSortIdx[Idx, High(FSortIdx[Idx])]:= I;
          F:= False;
        end;
      end;
    end;
    FSortIdx_Item:= - 1;
  end
  else
  begin
    FSortIdx_Item:= 0;
    SetLength(FSortIdx, 1);
    SetLength(FSortIdx[0], DataBase_.Count);
    for I:= 0 to DataBase_.Count - 1 do FSortIdx[0, I]:= I;
    ListView1.Items.Count:= DataBase_.Count;
  end;

  CalcMinMax;
  TreeView1.Items.EndUpdate;
  TreeView1.Visible:= TreeView1.Items.Count <> 0;
  if TreeView1.Items.Count <> 0 then TreeView1.Selected:= TreeView1.Items[0];
end;

procedure TFileBaseForm.SortList;          // ����������
var
  I, J, K: Integer;

begin
  if Length(FSortIdx) = 0 then Exit;
  if FSortIdx_Item = - 1 then Exit;
  for I:= 0 to Length(FSortIdx[FSortIdx_Item]) - 1 do
  begin
//    ProgressBar1.Position:= I * 100 div Length(FSortIdx[FSortIdx_Item]);
    for J:= I + 1 to Length(FSortIdx[FSortIdx_Item]) - 1 do
      if FSortIdx[FSortIdx_Item, I] <> FSortIdx[FSortIdx_Item, J] then
        if (CompareDate(FSortIdx[FSortIdx_Item, I], FSortIdx[FSortIdx_Item, J], Config.SortDataID) < 0) xor Config.SortDir then
        begin
          K:= FSortIdx[FSortIdx_Item, I];
          FSortIdx[FSortIdx_Item, I]:= FSortIdx[FSortIdx_Item, J];
          FSortIdx[FSortIdx_Item, J]:= K;
        end;
  end;
  ListView1.Repaint;
end;

procedure TFileBaseForm.FormCreate(Sender: TObject);
begin
  FSortIdx_Item:= - 1;
  ListView1.DoubleBuffered:= True;
  OnChangeLanguage(nil);
end;

procedure TFileBaseForm.TBItem4Click(Sender: TObject);
var
  BaseCfg2: TBaseCfg2Form;

begin
  BaseCfg2:= TBaseCfg2Form.Create(nil);
  BaseCfg2.OwnerForm:= Self;
  BaseCfg2.ShowModal;
  BaseCfg2.Release;
end;

procedure TFileBaseForm.TreeView1Click(Sender: TObject);
begin
  FSortIdx_Item:= Integer(TreeView1.Selected.Data);
  ListView1.Items.Count:= Length(FSortIdx[FSortIdx_Item]);

//  if FSortIdx_Item > High(FSortIdx) then ShowMessage('!!!!');

  CalcMinMax;

  SortList;
  ListView1.Refresh;
end;

var
  Test: Integer;

procedure TFileBaseForm.CalcMinMax;
var
  I, J: Integer;
  T1, T2: Extended;

begin
{
  if FSortIdx_Item = - 1 then Exit;

  FCurMinCoord:= MaxInt;
  FCurMaxCoord:= - MaxInt;

  for I:= 0 to Length(FSortIdx[FSortIdx_Item]) - 1 do
    with DataBase_.Item[Integer(FSortIdx[FSortIdx_Item, I])] do
    begin
      FCurMinCoord:= Min(FCurMinCoord, Min(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000));
      FCurMaxCoord:= Max(FCurMaxCoord, Max(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000));
    end;
}


  if FSortIdx_Item = - 1 then Exit;

  FCurMinCoord:= MaxInt;
  FCurMaxCoord:= - MaxInt;

  {$Q+}

  for I:= 0 to Length(FSortIdx[FSortIdx_Item]) - 1 do
    with DataBase_.Item[Integer(FSortIdx[FSortIdx_Item, I])] do
    begin
      if (StartKM < - 30000) or (StartKM > 30000) or (StartPk < - 100) or (StartPk > 100) then Continue;
      if (EndMRFCrd.KM < - 30000) or (EndMRFCrd.KM > 30000) or (EndMRFCrd.Pk < - 100) or (EndMRFCrd.Pk > 100) then Continue;

      Test:= I;
      if TCoordSys(PathCoordSystem) = csMetricRF then
      begin
        try
          FCurMinCoord:= Min(FCurMinCoord, Min(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000));
          FCurMaxCoord:= Max(FCurMaxCoord, Max(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000));
        except
        end;
      end
      else
      if TCoordSys(PathCoordSystem) = csImperial then
      begin
        try
          T1:= CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048;
          T2:= CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048;
        //FCurMinCoord:= Min(FCurMinCoord, Round(Min(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
        //FCurMaxCoord:= Max(FCurMaxCoord, Round(Max(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
          FCurMinCoord:= Round(Min(FCurMinCoord, Min(T1, T2)));
          FCurMaxCoord:= Round(Max(FCurMaxCoord, Max(T1, T2)));
        except
        end;
      end
      else
      if TCoordSys(PathCoordSystem) in [csMetric1km, csMetric1kmTurkish] then
      begin
        try
          T1:= CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 1000;
          T2:= CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 1000;
        //FCurMinCoord:= Min(FCurMinCoord, Round(Min(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
        //FCurMaxCoord:= Max(FCurMaxCoord, Round(Max(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
          FCurMinCoord:= Round(Min(FCurMinCoord, Min(T1, T2)));
          FCurMaxCoord:= Round(Max(FCurMaxCoord, Max(T1, T2)));
        except
        end;
      end
      else
      begin
        try
          T1:= CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 100;
          T2:= CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 100;
        //FCurMinCoord:= Min(FCurMinCoord, Round(Min(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
        //FCurMaxCoord:= Max(FCurMaxCoord, Round(Max(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
          FCurMinCoord:= Round(Min(FCurMinCoord, Min(T1, T2)));
          FCurMaxCoord:= Round(Max(FCurMaxCoord, Max(T1, T2)));
        except
        end;
      end
    end;

  {$Q-}

end;

function StepIn(Src: TRect): TRect;
begin
  Result:= Classes.Rect(Src.Left + 1, Src.Top + 1, Src.Right - 1, Src.Bottom - 1);
end;
(*
procedure TFileBaseForm.ListView1AdvancedCustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState;
                                                        Stage: TCustomDrawStage; var DefaultDraw: Boolean);
 var
  I: Integer;
  T1, T2: Extended;
  Rect1: TRect;
  Rect2: TRect;
  Rect: TRect;
  Save1: TColor;
  MinCoord: Integer;
  MaxCoord: Integer;

begin
  {$Q+}
//  Sender.Canvas.Font.Style:= [fsItalic];
  Sender.Canvas.Font.Style:= [];

  if Config.Fields[Sender.Column[0].Tag].DataID <> - 1 then
  begin
    DefaultDraw:= True;
    Exit;
  end;

  ListView_GetItemRect(Sender.Handle, Item.Index, Rect1, LVIR_BOUNDS);
  ListView_GetSubItemRect(Sender.Handle, Item.Index, 1, LVIR_BOUNDS, @Rect2);
  Rect:= Rect1;
  Rect.Right:= Rect2.Left;

  with Sender.Canvas do
  begin
    with DataBase_.Item[Integer(Item.Data)] do
    begin
{      try // - ������ ������� ��� ����� ���� ��������� ����������
      except
        MinCoord:= Min(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000);
        MaxCoord:= Max(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000);
      end;
}
      if TCoordSys(PathCoordSystem) = csMetricRF then
      begin
        try
          MinCoord:= Min(FCurMinCoord, Min(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000));
          MaxCoord:= Max(FCurMaxCoord, Max(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000));
        except
        end;
      end
      else
      if TCoordSys(PathCoordSystem) = csImperial then
      begin
        try
          T1:= CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048;
          T2:= CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048;
        //FCurMinCoord:= Min(FCurMinCoord, Round(Min(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
        //FCurMaxCoord:= Max(FCurMaxCoord, Round(Max(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
          MinCoord:= Round(Min(FCurMinCoord, Min(T1, T2)));
          MaxCoord:= Round(Max(FCurMaxCoord, Max(T1, T2)));
        except
        end;
      end
      else
      if TCoordSys(PathCoordSystem) in [csMetric1km, csMetric1kmTurkish] then
      begin
        try
          T1:= CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 1000;
          T2:= CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 1000;
        //FCurMinCoord:= Min(FCurMinCoord, Round(Min(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
        //FCurMaxCoord:= Max(FCurMaxCoord, Round(Max(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
          MinCoord:= Round(Min(FCurMinCoord, Min(T1, T2)));
          MaxCoord:= Round(Max(FCurMaxCoord, Max(T1, T2)));
        except
        end;
      end
      else
      begin
        try
          T1:= CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 100;
          T2:= CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 100;
        //FCurMinCoord:= Min(FCurMinCoord, Round(Min(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
        //FCurMaxCoord:= Max(FCurMaxCoord, Round(Max(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
          MinCoord:= Round(Min(FCurMinCoord, Min(T1, T2)));
          MaxCoord:= Round(Max(FCurMaxCoord, Max(T1, T2)));
        except
        end;
      end

    end;

    Save1:= Brush.Color;
    if cdsSelected in State then Brush.Color:= Self.Color;
    FillRect(Rect);
//    if DataBase_.Item[Integer(Item.Data)].MoveDir > 0 then Brush.Color:= clRed
//                                                     else Brush.Color:= clGreen;

    Brush.Color:= ConfigUnit.Config.GetMoveDirColor(DataBase_.Item[Integer(Item.Data)].MoveDir);

    FillRect(StepIn(Classes.Rect(Rect.Left + Round((MinCoord - FCurMinCoord) / (FCurMaxCoord - FCurMinCoord) * (Rect.Right - Rect.Left)) - 2, Rect.Top,
                                 Rect.Left + Round((MaxCoord - FCurMinCoord) / (FCurMaxCoord - FCurMinCoord) * (Rect.Right - Rect.Left)) + 2, Rect.Bottom)));
    Brush.Color:= Save1;
  end;
  {$Q-}
end;

procedure TFileBaseForm.ListView1AdvancedCustomDrawSubItem(Sender: TCustomListView; Item: TListItem; SubItem: Integer;
                                                           State: TCustomDrawState; Stage: TCustomDrawStage; var DefaultDraw: Boolean);

var
  T1, T2: Extended;
  I, J: Integer;
  Rect: TRect;
  Save1: TColor;
  MinCoord: Integer;
  MaxCoord: Integer;

begin
  {$Q+}
  Sender.Canvas.Font.Style:= [fsItalic];
  Sender.Canvas.Font.Style:= [];

  if Config.Fields[Sender.Column[SubItem].Tag].DataID <> - 1 then
  begin
    DefaultDraw:= True;
    Exit;
  end;

  if Integer(Item.Data) >= MainUnit.DataBase_.Count then Exit;
  ListView_GetSubItemRect(Sender.Handle, Item.Index, SubItem, LVIR_BOUNDS, @Rect);
  DefaultDraw:= False;
  with Sender.Canvas do
  begin

    with DataBase_.Item[Integer(Item.Data)] do
    begin
{      try // - ������ ������� ��� ����� ���� ��������� ����������
      except
        MinCoord:= Min(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000);
        MaxCoord:= Max(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000);
      end;
}
      if TCoordSys(PathCoordSystem) = csMetricRF then
      begin
        try
          MinCoord:= Min(FCurMinCoord, Min(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000));
          MaxCoord:= Max(FCurMaxCoord, Max(StartKM * 1000 + StartPk * 100 + StartMetre, EndMRFCrd.KM * 1000 + EndMRFCrd.Pk * 100 + EndMRFCrd.MM div 1000));
        except
        end;
      end
      else
      if TCoordSys(PathCoordSystem) = csImperial then
      begin
        try
          T1:= CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048;
          T2:= CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048;
        //FCurMinCoord:= Min(FCurMinCoord, Round(Min(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
        //FCurMaxCoord:= Max(FCurMaxCoord, Round(Max(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
          MinCoord:= Round(Min(FCurMinCoord, Min(T1, T2)));
          MaxCoord:= Round(Max(FCurMaxCoord, Max(T1, T2)));
        except
        end;
      end
      else
      if TCoordSys(PathCoordSystem) in [csMetric1km, csMetric1kmTurkish] then
      begin
        try
          T1:= CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 1000;
          T2:= CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 1000;
        //FCurMinCoord:= Min(FCurMinCoord, Round(Min(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
        //FCurMaxCoord:= Max(FCurMaxCoord, Round(Max(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
          MinCoord:= Round(Min(FCurMinCoord, Min(T1, T2)));
          MaxCoord:= Round(Max(FCurMaxCoord, Max(T1, T2)));
        except
        end;
      end
      else
      begin
        try
          T1:= CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 100;
          T2:= CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 100;
        //FCurMinCoord:= Min(FCurMinCoord, Round(Min(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
        //FCurMaxCoord:= Max(FCurMaxCoord, Round(Max(CaCrdToVal(TCoordSys(PathCoordSystem), StartChainage) * 0.3048, CaCrdToVal(TCoordSys(PathCoordSystem), EndCaCrd) * 0.3048)));
          MinCoord:= Round(Min(FCurMinCoord, Min(T1, T2)));
          MaxCoord:= Round(Max(FCurMaxCoord, Max(T1, T2)));
        except
        end;
      end

    end;

    Save1:= Brush.Color;
    if cdsSelected in State then Brush.Color:= Self.Color;
    FillRect(Rect);
//    if DataBase_.Item[Integer(Item.Data)].MoveDir > 0 then Brush.Color:= clRed
//                                                     else Brush.Color:= clGreen;
    Brush.Color:= ConfigUnit.Config.GetMoveDirColor(DataBase_.Item[Integer(Item.Data)].MoveDir);

    Brush.Style:= bsSolid;
    if FCurMaxCoord - FCurMinCoord <> 0 then
      FillRect(StepIn(Classes.Rect(Rect.Left + Round((MinCoord - FCurMinCoord) / (FCurMaxCoord - FCurMinCoord) * (Rect.Right - Rect.Left)) - 1, Rect.Top,
                                   Rect.Left + Round((MaxCoord - FCurMinCoord) / (FCurMaxCoord - FCurMinCoord) * (Rect.Right - Rect.Left)) + 1, Rect.Bottom)));
    Brush.Color:= Save1;
  end;
  {$Q-}
end;
*)
procedure TFileBaseForm.ListView1DblClick(Sender: TObject);
var
  New: TAvk11ViewForm;

begin
  if Assigned(ListView1.Selected) then
    MainForm.OpenFile(DataBase_.Item[Integer(ListView1.Selected.Data)].FileName);
end;

procedure TFileBaseForm.ListView1Click(Sender: TObject);
var
  I, S: Integer;

begin
  S:= 0;
  for I:= 0 to ListView1.Items.Count - 1 do
    if ListView1.Items.Item[I].Selected then
        S:= S + DataBase_.Item[Integer(ListView1.Items.Item[I].Data)].Length;
  StatusBar1.Panels.Items[0].Text:= LangTable.Caption['FileBase:Selectfileslength'] + ' ' + IntToStr(S) + ' ' + LangTable.Caption['common:m'] + '.';
end;

procedure TFileBaseForm.OnChangeLanguage(Sender: TObject);
var
  I: Integer;
  
begin
  TBToolbar2.Font.Name        := LangTable.Caption['General:FontName'];
  TreeView1.Font.Name         := LangTable.Caption['General:FontName'];
  ListView1.Font.Name         := LangTable.Caption['General:FontName'];

  Self.Caption                := LangTable.Caption['FileBase:FileDatabase'];
  TBItem1.Caption             := LangTable.Caption['FileBase:Directory'];
  TBItem2.Caption             := LangTable.Caption['FileBase:Update'];
  TBItem4.Caption             := LangTable.Caption['FileBase:Settings'];

  with Config do
    for I:= 0 to High(Fields) do
    begin
      case Fields[I].DataID of
        -1: Fields[I].Name:= LangTable.Caption['FileBase:Graphics']; // ����������� �������������
         1: Fields[I].Name:= LangTable.Caption['FileBase:FileName']; // ��� �����
         4: Fields[I].Name:= LangTable.Caption['FileBase:Defectoscopenumber']; // ����� �������
         6: Fields[I].Name:= LangTable.Caption['FileBase:DateandTime']; // ���� � ����� �������
         7: Fields[I].Name:= LangTable.Caption['FileBase:RailRoadName']; // �������� �.�.
         8: Fields[I].Name:= LangTable.Caption['FileBase:RailRoadName']; // ����� ��
         9: Fields[I].Name:= LangTable.Caption['FileBase:Operator']; // ��� ���������
        10: Fields[I].Name:= LangTable.Caption['FileBase:DirectionCode']; // ��� �����������
        11: Fields[I].Name:= LangTable.Caption['FileBase:Tracksection']; // �������� �������
        12: Fields[I].Name:= LangTable.Caption['FileBase:StartCoordinate']; // ��������� ����������
        13: Fields[I].Name:= LangTable.Caption['FileBase:EndCoordinate']; // �������� ����������
        14: Fields[I].Name:= LangTable.Caption['FileBase:Tracknumber']; // ����
        15: Fields[I].Name:= LangTable.Caption['FileBase:MovingDirection']; // ����������� ��������
        16: Fields[I].Name:= LangTable.Caption['FileBase:Section']; // �����
        23: Fields[I].Name:= LangTable.Caption['FileBase:Length']; // �����
        24: Fields[I].Name:= LangTable.Caption['InfoBar:Device']; // ������
      end;
    end;

  SetUpColums;
end;

procedure TFileBaseForm.FormActivate(Sender: TObject);
begin
  MainForm.DisplayModeToControl({nil} Self);
end;

procedure TFileBaseForm.ListView1Changing(Sender: TObject; Item: TListItem; Change: TItemChange; var AllowChange: Boolean);
begin
  ListView1Click(nil);
end;

procedure TFileBaseForm.ListView1ColumnClick(Sender: TObject; Column: TListColumn);
begin
  if Config.Fields[Column.Tag].DataID = - 1 then Exit;

  Screen.Cursor:= crHourGlass;

  if Config.Fields[Column.Tag].DataID = Config.SortDataID
    then Config.SortDir := not Config.SortDir
    else Config.SortDataID:= Config.Fields[Column.Tag].DataID;

  SortList;
  Screen.Cursor:= crDefault
end;

procedure TFileBaseForm.ListView1Data(Sender: TObject; Item: TListItem);
var
  F: Boolean;
  I, J: Integer;

begin
  if (Length(FSortIdx) = 0) or (FSortIdx_Item = - 1) then Exit;
  I:= FSortIdx[FSortIdx_Item, Item.Index];
  F:= True;
  for J:= 0 to High(Config.Fields) do
    if Config.Fields[J].Mode = fmVisible then
    begin
      if F then
      begin
        Item.Caption:= DataToStr(I, Config.Fields[J].DataID, False);
        Item.Data:= Pointer(I);
        F:= False;
      end else Item.SubItems.Add(DataToStr(I, Config.Fields[J].DataID, False));
    end;
end;

procedure MyGraphDraw(Canvas: TCanvas; Rt: TRect; Str: string);
var
  SaveColor: TColor;
  St, Ed: Single;
  Dir: Integer;
  Code: Integer;

begin
  with Canvas do
  begin

    Val(Copy(Str,  1, 10),  St, Code); if Code <> 0 then Exit;
    Val(Copy(Str, 11, 10),  Ed, Code); if Code <> 0 then Exit;
    Val(Copy(Str, 21, 10),  Dir, Code); if Code <> 0 then Exit;
{
    St:= StrToFloat(Copy(Str, 1, 10));
    Ed:= StrToFloat(Copy(Str, 11, 10));
    Dir:= StrToInt(Copy(Str, 21, 1));
}
    Brush.Color:= clWhite;
    FillRect(Rt);
    Pen.Color:= clBlack;
    Rectangle(Rect(Rt.Left + 1, Rt.Top + 1, Rt.Right - 1, Rt.Bottom - 1));


//    if Dir > 0 then Brush.Color:= clGreen
//               else Brush.Color:= clRed;

    Brush.Color:= ConfigUnit.Config.GetMoveDirColor(Dir);

    FillRect(Rect(Round(Rt.Left + 2 + (Rt.Right - Rt.Left - 4) * St), Rt.Top + 2,
                  Round(Rt.Left + 2 + (Rt.Right - Rt.Left - 4) * Ed), Rt.Bottom - 2));
  end;
end;

procedure MyTextDraw(Canvas: TCanvas; Rt: TRect; Text: string);
begin
  with Canvas do
  begin
    Brush.Style:= bsClear;
//    TextOut(Rt.Left, Rt.Top, Text);
    TextRect(Rect(Rt.Left, Rt.Top, Rt.Right - 5, Rt.Bottom), Rt.Left + 3, Rt.Top, Text);
  end;
end;

procedure TFileBaseForm.ListView1CustomDrawSubItem(Sender: TCustomListView; Item: TListItem; SubItem: Integer; State: TCustomDrawState; var DefaultDraw: Boolean);
var
  Rt: TRect;

begin
  ListView_GetSubItemRect(Sender.Handle, Item.Index, SubItem, LVIR_BOUNDS, @Rt);

  with TCustomListView(Sender).Canvas do
  begin
    if Item.Selected then
    begin
      Brush.Color:= clHighlight;
      Font.Color:= clHighlightText;
    end
    else
    begin
      Brush.Color:= clWindow;
      Font.Color:= clWindowText;
      Brush.Style:= bsClear;
    end;

    FillRect(Rt);
  end;

  if Config.Fields[Sender.Column[SubItem].Tag].DataID = - 1 then MyGraphDraw(TCustomListView(Sender).Canvas, Rt, Item.SubItems.Strings[SubItem - 1])
                                                            else MyTextDraw(TCustomListView(Sender).Canvas, Rt, Item.SubItems.Strings[SubItem - 1]);
  DefaultDraw:= false
end;

procedure TFileBaseForm.ListView1CustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
var
  I: Integer;
  Rt: TRect;
  Rt1: TRect;
  Rt2: TRect;
  MyDefaultDraw: Boolean;

begin

  ListView_GetItemRect(Sender.Handle, Item.Index, Rt1, LVIR_BOUNDS);
  ListView_GetSubItemRect(Sender.Handle, Item.Index, 1, LVIR_BOUNDS, @Rt2);
  Rt:= Rt1;
  Rt.Right:= Rt2.Left - 1;

  with TCustomListView(Sender).Canvas do
  begin
    if Item.Selected then
    begin
      Brush.Color:= clHighlight;
      Font.Color:= clHighlightText;
    end
    else
    begin
      Brush.Color:= clWindow;
      Font.Color:= clWindowText;
    end;

    Inc(Rt.Right);
    FillRect(Rt);
    Brush.Style:= bsClear;
  end;

  if Config.Fields[Sender.Column[0].Tag].DataID = - 1 then MyGraphDraw(TCustomListView(Sender).Canvas, Rt, Item.Caption)
                                                      else MyTextDraw(TCustomListView(Sender).Canvas, Rt, Item.Caption);

  for I:= 0 to Item.SubItems.Count - 1 do
    ListView1CustomDrawSubItem(Sender, Item, I + 1, State, MyDefaultDraw);

  DefaultDraw:= False;
end;

procedure TFileBaseForm.ListView1DataHint(Sender: TObject; StartIndex, EndIndex: Integer);
begin
  TListView(Sender).Hint:= '';
end;

end.
