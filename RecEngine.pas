{$I DEF.INC}
unit RecEngine;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TB2Item, ImgList, TB2Dock, TB2Toolbar, StdCtrls, Math, // Avk11Engine,
  RecTypes, Avk11ViewUnit, TeEngine, Series, ExtCtrls, TeeProcs, Chart,
  ComCtrls, MyTypes, RecProgressUnit, AviconDataSource, AviconTypes;

type

  TRecEngine = class ;

  TREDebugForm = class(TForm)
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Log: TRichEdit;
  private
    RE: TRecEngine;
  public
  end;

// ------- RecEngine -----------------------------------------------------------

  TBMDataItem = record
    DisRange: TRange;
    SysRange: TRange;
    DisItems: array of TRange;
  end;

  TExecuteMode = (emAll, emDebug, emToPointDebug);

  TRecEngine = class(TThread)
  private
    FDatSrc: TAviconDataSource;
    FDebugForm: TREDebugForm;
    FRecData: Pointer;
    FLoadRange: TRange;
    FScanStep: Single;
    FEndWord: Boolean;

  protected
    procedure SortRes;
    function MoveRange(Range: TRange; Idx: Integer = 2): TRange;
    function MoveCrd(Crd: Integer): Integer;
    procedure Execute; override;

  public
    RecProgForm: TRecProgressForm;
    Res: TRecRes;
//    OnEnd: TNotifyEvent;

    FLastResIdx: Integer;

    DispWin: Integer;
    MinLen: Integer;
    MaxLen: Integer;
    DispIdx: array of TMinMax;
    Step: Integer;
    MaxStep: Integer;
    ViewObj: array [TResType] of Boolean;
    CancelFlg: Boolean;

    procedure ClearItems;
    procedure AddResItem(Typ_: TResType; Rail: RRail_; Range: TRange; Params: string);
    procedure AddZone(Rail: RRail_; Range: TRange; StTape, EdTape: Integer; CrdMode: TCrdMode; Color: Integer);
    procedure AddLine(Rail: RRail_; Crd_: Integer; StTape, EdTape, Color: Integer);

    procedure AddPBMark(R: RRail_; Ch: Integer; Crd_, Del: TRange; CrdMode: TCrdMode; Color1, Color2: Integer);
    procedure AddMark(R: RRail_; Crd_: TRange; NoRed: Boolean; MarkList: TMarkList; StTape, EdTape, Color1, Color2: Integer);

    procedure ResItemRangeByPBList;
    procedure SaveRes(RecData: Pointer);
    procedure Cancel(Sender: TObject);

    constructor Create(CreateSuspended: Boolean);
    destructor Destroy;
    procedure SetData(DatSrc: TAvk11DatSrc);
    procedure Calc(Mode: TExecuteMode; DebugCoord: Integer = - 1);
    procedure ShowDebug;
    procedure ProgMess(Msg: string; Pos: Integer);
    property DatSrc: TAvk11DatSrc read FDatSrc;
    property EndWord: Boolean read FEndWord;
  end;

// ------- RecEngine -----------------------------------------------------------
{
  TThreadCalc = class(TThread)
    RecData: Pointer;
    procedure Execute; override;
  end;
}
implementation


uses
  MainUnit, Menus, RecDataUnit, Types, ConfigUnit, RecData;

{$R *.dfm}

// ---- TRecEngine -------------------------------------------------------------

procedure TRecEngine.ProgMess(Msg: string; Pos: Integer);
begin
  Inc(Step);
  with RecProgForm do
  begin
    PB.Position:= Step;
    Refresh;
  end;
end;

constructor TRecEngine.Create(CreateSuspended: Boolean);
begin
  FEndWord:= False;
  CancelFlg:= False;
  FLastResIdx:= - 1;
  FDebugForm:= TREDebugForm.Create(nil);
  FDebugForm.RE:= Self;

  ViewObj[rt_OneHole       ]:= True;  // ��������� ���������
  ViewObj[rt_Plug          ]:= True;  // ��������
  ViewObj[rt_WeldedJoint   ]:= True;  // ������� ����
  ViewObj[rt_BoltJoint     ]:= True;  // �������� ����
  ViewObj[rt_FlangeRail    ]:= True;  // ����� �����
  ViewObj[rt_Blade         ]:= True;  // ������ ����������� ��������
  ViewObj[rt_Switch        ]:= True;  // �������
  ViewObj[rt_UCSite        ]:= True;  // ��������������������� �������
  ViewObj[rt_Defect        ]:= True;  // ������
  ViewObj[rt_TEDoublePass  ]:= True;  // ��� �������� ������� �� ��
  ViewObj[rt_TESkipBJButton]:= True;  // �� ������ ������ ��
  ViewObj[rt_TESens        ]:= True;  // ��������� ����������������
  ViewObj[rt_TESpeed       ]:= True;  // ��������� �������� ��������
  ViewObj[rt_Frog          ]:= True;  // ����������
  ViewObj[rt_SupportingRail]:= True;  // ������ �����
  ViewObj[rt_TEBSLost      ]:= True;

  inherited Create(CreateSuspended);
end;

destructor TRecEngine.Destroy;
begin
  SetLength(DispIdx, 0);
  SetLength(Res, 0);
  FDebugForm.Free;
end;

procedure TRecEngine.SetData(DatSrc: TAvk11DatSrc);
begin
  FDatSrc:= TAvk11DatSrc.Create;
  FDatSrc.LoadFromFile(DatSrc.FileName);
  FScanStep:= FDatSrc.Header.ScanStep / 100;
//  FDatSrc:= DatSrc;
end;

procedure TRecEngine.SaveRes(RecData: Pointer);       // ----- ���������� ����������� ------------------------------------------------
var
  R: RRail_;
  LoadRange: TRange;
  I, J: Integer;
  S: string;
  Typ_: TResType;
  ChEx: array [0..7] of Boolean;
  ChCount: Integer;
  Flg: Boolean;
  MarkList: TMarkList;
  Crd_: TRange;
  Del_: TRange;

begin
  ClearItems;
  with TRecData(RecData) do
  begin

      R:= rLeft;
//    for R:= rLeft to rRight do
      for I:= 0 to High(Construct[R]) do         // �������������� ��������
        with Construct[R, I] do
        begin
          Typ_:= rt_BoltJoint;   // �������� ����
          AddResItem(Typ_, rNotSet, {MoveRange(}Head{, 1)}, '');

          AddLine(rNotSet, Head.StCrd, 0, 3, 0);
          AddLine(rNotSet, Head.EdCrd, 0, 3, 0);

          {
           AddLine(R, Head.StCrd, 0, 3, $007070E0);
           AddLine(R, Head.EdCrd, 0, 3, $007070E0);
           AddLine(R, Body.StCrd, 0, 3, $007070E0);
           AddLine(R, Body.EdCrd, 0, 3, $007070E0);
          }

        //  AddZone(R, {MoveRange(}Head{, 2)}, 0, 1, cm_Normal, $009FFF9F);
        //  AddZone(R, {MoveRange(}Body{, 2)}, 2, 3, cm_Normal, $009FFF9F);
        end;

    for R:= rLeft to rRight do
      for I:= 0 to High(Defect[R]) do            // �������
        with Defect[R, I] do
        begin
          case Typ of
            dtHead: S:= '� �������';
            dtBody: S:= '� �����';
          dtBottom: S:= '� �������';
         dtUnknown: S:= '������������ �� ����������';
         dtLostBtm: S:= '���������� ������� �������';
      pbtiUnChkCon: S:= '�� ���������� ��';
//             dtDel: S:= '�������';
          end;

          if Typ <> dtLostBtm then // ������ �������
          begin
            for J:= 0 to 7 do ChEx[J]:= False;
            for J:= 0 to High(PBList) do ChEx[PBList[J].Ch]:= True;
            ChCount:= 0;
            for J:= 0 to 7 do if ChEx[J] then Inc(ChCount);
            if ChCount > 1 then S:= S + ' (������: '
                           else S:= S + ' (�����: ';
            Flg:= False;
            for J:= 0 to 7 do if ChEx[J] then
            begin
              S:= S + IntToStr(J) + ', ';
              Flg:= True;
            end;
            if Flg then Delete(S, Length(S) - 1, 2);
            S:= S + ')';
          end;

          {$IFDEF RECDEBUG}
          S:= S + Text + ' ';
          {$ENDIF}

          if Typ <> dtLostBtm then AddResItem(rt_Defect, R, Rgn, S)
                              else AddResItem(rt_TEBSLost, R, Rgn, '');
//          AddZone(R, MoveRange(Rgn, 2), 0, 3, $009FFF9F);


          for J:= 0 to High(PBList) do
//            AddPBMark(R, PBList[J].Ch, PBList[J].Crd, PBList[J].Dly, cm_Normal,{ cm_Reduce,} $00FFFFA8, $00F2F200);
            AddPBMark(R, PBList[J].Ch, PBList[J].Crd, PBList[J].Dly, cm_Normal,{ cm_Reduce,} $00CDCDCD, $00888888);

          ResItemRangeByPBList;
          AddLine(R, GetCentr(Res[FLastResIdx].Bounds), 0, 3, $007070E0 {clBlack} );
        end;

    for R:= rLeft to rRight do
      for I:= 0 to High(UnCtrlZ[R]) do
        with UnCtrlZ[R, I] do
          begin
            Typ_:= rt_UCSite;
            AddResItem(Typ_,   R, {MoveRange(}UnCtrlZ[R, I].Rng{)}, '');
            AddZone(R, {MoveRange(}UnCtrlZ[R, I].Rng{, 2)}, 3, 3, cm_Normal, $00B9B9F0);
            for J:= 2 to High(IdxList) do
            begin

              Crd_:= TRecData(RecData).FPBList[R, 0, TRecData(RecData).UnCtrlZ[R, I].IdxList[J]].Crd;
              Del_:= TRecData(RecData).FPBList[R, 0, TRecData(RecData).UnCtrlZ[R, I].IdxList[J]].Del;

              AddPBMark(R, 0, Crd_, Range(Round(Del_.StCrd / 3), Round(Del_.EdCrd / 3)), cm_Normal,{ cm_Reduce,} $00CDCDCD, $00888888);
//              AddPBMark(R, 0, FPBList[R, 0, IdxList[J]].Crd, FPBList[R, 0, IdxList[J]].Del, cm_Normal,{ cm_Reduce,} $00CDCDCD, $00888888);
            end;
//            UnCtrlZ[R, I].
          end;
  end;


             {
    for I:= 0 to High(FBJButton) do            // �����
    begin
      AddResItem(rt_BoltJoint, r_Both, FBJButton[I], '');
//      AddPBMark(rLeft, 6, Range(109970, 110005), Range(88, 61), cm_Normal, 0, 0);
//      AddZone(rLeft, FBJButton[I], 2, 3, cm_Normal, $009FFF9F);

      MarkList[1].Used:= True;
      MarkList[1].AllDelay:= True;
      MarkList[2].Used:= True;
      MarkList[2].AllDelay:= True;
      MarkList[3].Used:= True;
      MarkList[3].AllDelay:= True;
      MarkList[4].Used:= True;
      MarkList[4].AllDelay:= True;
      MarkList[5].Used:= True;
      MarkList[5].AllDelay:= True;
      MarkList[6].Used:= True;
      MarkList[6].AllDelay:= True;
      MarkList[7].Used:= True;
      MarkList[7].AllDelay:= True;

      AddMark(r_Both, FBJButton[I], True, MarkList, 0, 3, clLime, 0);
    end;
              }


{         // �������� ������ �� �-���������
    AddResItem(rt_Defect, rLeft, Range(109970, 110005), '50000');
    AddPBMark(rLeft, 6, Range(109970, 110005), Range(88, 61), cm_Normal, 0, 0);
    AddPBMark(rLeft, 7, Range(109561, 109580), Range(57, 84), cm_Reduce, 0, 0);

    MarkList[1].Used:= False;
    MarkList[2].Used:= True;
    MarkList[2].AllDelay:= True;
    MarkList[3].Used:= True;
    MarkList[3].AllDelay:= True;
    MarkList[4].Used:= True;
    MarkList[4].AllDelay:= True;
    MarkList[5].Used:= True;
    MarkList[5].AllDelay:= True;
    MarkList[6].Used:= False;
    MarkList[7].Used:= False;
    AddMark(rLeft, Range(109724, 109762), MarkList, 0, 1, 0, 0);
}


    (*
    for R:= rLeft to rRight do
      for I:= 0 to High(FEnd2[R]) do            // �����
      begin
        AddResItem(rt_Defect, R, Range(FEnd2[R, I].Crd, FEnd2[R, I].Crd), Format('����� Max: %d; Crd: %d', [0 {FEnd2[R, I].MaxVal}, FEnd2[R, I].Crd]));
        MarkList[1].Used:= False;
        MarkList[2].Used:= True;
        MarkList[2].AllDelay:= True;
        MarkList[3].Used:= True;
        MarkList[3].AllDelay:= True;
        MarkList[4].Used:= True;
        MarkList[4].AllDelay:= True;
        MarkList[5].Used:= True;
        MarkList[5].AllDelay:= True;
        MarkList[6].Used:= True;
        MarkList[6].AllDelay:= False;
        MarkList[6].Delay:= Range(120, 170);
        MarkList[7].Used:= True;
        MarkList[7].AllDelay:= False;
        MarkList[7].Delay:= Range(120, 170);

        AddMark(R, FEnd2[R, I].Range, MarkList, 0, 1, clLime, 0);
      end;
      *)
   (*
    for R:= rLeft to rRight do
      for I:= 0 to High(FSingleObj[R]) do
        if FSingleObj[R, I].Typ in [so_BHOk, so_BHUnCtrl, so_BHDef] then
        begin
          AddResItem(rt_Defect, R, FSingleObj[R, I].DisRange, Format('���������', []));
          MarkList[1].Used:= True;
          MarkList[1].AllDelay:= True;

          MarkList[2].Used:= False;
          MarkList[3].Used:= False;
          MarkList[4].Used:= False;
          MarkList[5].Used:= False;

          MarkList[6].Used:= True;
          MarkList[6].AllDelay:= False;
          MarkList[6].Delay:= Range(30, 110);
          MarkList[7].Used:= True;
          MarkList[7].AllDelay:= False;
          MarkList[7].Delay:= Range(30, 110);

          AddMark(R, FSingleObj[R, I].DisRange, MarkList, 2, 3, clLime, 0);
        end;

    for I:= 0 to High(TechnoError) do            // ��������� ����������
      with TechnoError[I] do
        case Typ of
        teDoublePass: begin
                        AddResItem(rt_TEDoublePass,   R, MoveRange(Range, 1), '');
                        AddLine(R, GetCentr(MoveRange(Range, 1)), 0, 3, clBlack);
                      end;
          teBJButton: begin
                        AddResItem(rt_TESkipBJButton, R, MoveRange(Range, 1), '');
                        AddLine(R, GetCentr(MoveRange(Range, 1)), 0, 3, clBlack);
                      end;
              teSens: begin
                        if Value > 0 then AddResItem(rt_TESens, R, MoveRange(Range, 1), Format(' ������ %d ��������', [TechnoError[I].Ch]))
                                     else AddResItem(rt_TESens, R, MoveRange(Range, 1), Format(' ������ %d ��������', [TechnoError[I].Ch]));
                        AddLine(R, GetCentr(MoveRange(Range, 1)), 0, 3, clBlack);
                      end;
             teSpeed: begin
                        AddResItem(rt_TESpeed, R, MoveRange(Range, 1), Format(' %3.1f ��/�', [TechnoError[I].Value / 10]));
                        AddLine(R, GetCentr(MoveRange(Range, 1)), 0, 3, clBlack);
                      end;
        end;
    *)
     (*
    for R:= rLeft to rRight do
      for I:= 0 to High(Construct[R]) do         // �������������� ��������
       if not Construct[R, I].Del then
        with Construct[R, I] do
        begin
          case Typ of
            ct_OneHole    : begin
                              Typ_:= rt_OneHole    ; // ��������� ���������
                              AddResItem(Typ_,   R, MoveRange(Body, 1), '');
                              AddZone(R, MoveRange(Body, 2), 2, 3, $009FFF9F);
                            end;
            ct_Plug       : begin
                              Typ_:= rt_Plug       ; // ��������
                              AddResItem(Typ_,   R, MoveRange(Body), '');
                              AddZone(R, MoveRange(Body, 2), 2, 3, $009FFF9F);
                            end;
            ct_WeldedJoint: begin

                              Typ_:= rt_WeldedJoint; // ������� ����
                              AddResItem(Typ_,   R, MoveRange(Body), '');
                              AddZone(R, MoveRange(Body, 2), 2, 3, $009FFF9F);

                            end;
            ct_BoltJoint  : begin
                              Typ_:= rt_BoltJoint;   // �������� ����
                              AddResItem(Typ_,   R, MoveRange(Head, 1), '');
                             {
                              AddLine(R, Head.StCrd, 0, 3, $007070E0);
                              AddLine(R, Head.EdCrd, 0, 3, $007070E0);
                              AddLine(R, Body.StCrd, 0, 3, $007070E0);
                              AddLine(R, Body.EdCrd, 0, 3, $007070E0);
                             }

                              AddZone(R, MoveRange(Head, 2), 0, 1, $009FFF9F);
                              AddZone(R, MoveRange(Body, 2), 2, 3, $009FFF9F);
                            end;
            ct_FlangeRail : begin
                              Typ_:= rt_FlangeRail ; // ����� �����
                              AddResItem(Typ_,   R, MoveRange(Body), '');
                              AddZone(R, MoveRange(Body, 2), 2, 3, $009FFF9F);
                            end;
            ct_Blade      : begin
                              Typ_:= rt_Blade      ; // ������ ����������� ��������
                              AddResItem(Typ_,   R, MoveRange(Body), '');
                              AddZone(R, MoveRange(Body, 2), 2, 3, $009FFF9F);
                            end;
            ct_Frog       : begin
                              Typ_:= rt_Frog      ; // ����������
                              AddResItem(Typ_,   R, MoveRange(Body), '');
                              AddZone(R, MoveRange(Body, 2), 2, 3, $009FFF9F);
                            end;
        ct_SupportingRail : begin
                              Typ_:= rt_SupportingRail; // ������ �����
                              AddResItem(Typ_,   R, MoveRange(Body), '');
                              AddZone(R, MoveRange(Body, 2), 2, 3, $009FFF9F);
                            end;
          end;
        end;  *)
end;

procedure TRecEngine.Execute;
begin
  TRecData(FRecData).Work(Self, FDatSrc, FLoadRange);
  SaveRes(TRecData(FRecData));
  TRecData(FRecData).Free;
  SortRes;
  FEndWord:= True;
 // if Assigned(OnEnd) then OnEnd(self);
end;

procedure TRecEngine.Calc(Mode: TExecuteMode; DebugCoord: Integer = - 1);
var
  R: RRail_;
  I, J: Integer;
  S: string;
  Typ_: TResType;
  ChEx: array [0..7] of Boolean;

begin
  if Mode = emAll then // ���������� ���������
  begin
  {
    Step:= 0;
    MaxStep:= 30;
    RecProgForm:= TRecProgressForm.Create(nil);
    RecProgForm.Visible:= True;
    RecProgForm.BringToFront;
    RecProgForm.Refresh;
    RecProgForm.PB.Max:= MaxStep;
    RecProgForm.PB.Position:= 0;
    RecProgForm.Refresh;
    RecProgForm.BorderStyle:= bsNone;
    RecProgForm.Align:= alBottom;
    RecProgForm.Parent:= MainForm;
   }
    FRecData:= TRecData.Create;
    TRecData(FRecData).ProgressMess:= ProgMess;
    FLoadRange.StCrd:= 0;
    FLoadRange.EdCrd:= FDatSrc.MaxDisCoord;

    if True then
    begin
//      TRecData(FRecData).ProgressMess:= nil;
      Resume;
    end
    else
    begin
      TRecData(FRecData).Work(Self, FDatSrc, FLoadRange, Mode = emDebug);
      RecProgForm.PB.Position:= MaxStep;
      RecProgForm.Refresh;
      RecProgForm.Visible:= False;
//      RecProgForm.ShowModal;
      RecProgForm.Free;

      SaveRes(TRecData(FRecData));
      TRecData(FRecData).Free;
      SortRes;
    end;
  end
  else
  if Mode = emDebug then // ������� �������� �����
  begin
    TRecData(FRecData):= TRecData.Create;
    FLoadRange.StCrd:= MainForm.ActiveViewForm.Display.CenterDisCoord - Round(100000 / (FDatSrc.Header.ScanStep / 100));
    FLoadRange.EdCrd:= MainForm.ActiveViewForm.Display.CenterDisCoord + Round(100000 / (FDatSrc.Header.ScanStep / 100));
    TRecData(FRecData).Work(Self, FDatSrc, FLoadRange, Mode = emDebug, MainForm.ActiveViewForm.Display.CenterDisCoord);
    SaveRes(TRecData(FRecData));
    TRecData(FRecData).Free;
    SortRes;
  end;

  MinLen:= MaxInt;
  MaxLen:= - MaxInt;
  for I:= 0 to High(Res) do
  begin
    MinLen:= Min(MinLen, Res[I].Len);
    MaxLen:= Max(MaxLen, Res[I].Len);
  end;
end;

procedure TRecEngine.Cancel(Sender: TObject);
begin
  Self.Suspend;
  FEndWord:= True;
  CancelFlg:= True;
end;

procedure TRecEngine.ShowDebug;
begin
  FDebugForm.ShowModal;
end;

procedure TRecEngine.SortRes;
var
  I, J, K: Integer;
  Tmp: TResItem;
  Idx1: Integer;
  Idx2: Integer;
  Flg: Boolean;

function Compare(Idx1, Idx2: Integer): Integer;
begin
  if GetCentr(Res[Idx1].Bounds) > GetCentr(Res[Idx2].Bounds) then Result:= + 1 else
  if GetCentr(Res[Idx1].Bounds) < GetCentr(Res[Idx2].Bounds) then Result:= - 1 else
  if Ord(Res[Idx1].R) > Ord(Res[Idx2].R) then Result:= + 1 else Result:= - 1;
end;

begin
  for I:= 0 to High(Res) - 1 do           // ���������� �� ������ ���� �������
  begin
    K:= I;
    for J:= I + 1 to High(Res) do
      if Compare(K, J) > 0 then K:= J;

    if I <> K then
    begin
      Tmp:= Res[I];
      Res[I]:= Res[K];
      Res[K]:= Tmp;
    end;
  end;

  DispWin:= 5000;
  SetLength(DispIdx, 1 + FDatSrc.MaxDisCoord div DispWin);

  for I:= 0 to High(DispIdx) do
  begin
    DispIdx[I].Min:= + MaxInt;
    DispIdx[I].Max:= - MaxInt;
  end;

  for I:= 0 to High(Res) do
  begin
    Idx1:= Max(            0, Res[I].Bounds.StCrd div DispWin - 1);
    Idx2:= Min(High(DispIdx), Res[I].Bounds.EdCrd div DispWin + 1);
    for J:= Idx1 to Idx2 do
    begin
      if I < DispIdx[J].Min then DispIdx[J].Min:= I;
      if I > DispIdx[J].Max then DispIdx[J].Max:= I;
    end;
  end;

  DispIdx[High(DispIdx)].Max:= High(Res);
  repeat
    Flg:= True;
    for I:= 0 to High(DispIdx) - 1 do
      if (DispIdx[I    ].Max =  - MaxInt) and
         (DispIdx[I + 1].Max <> - MaxInt) then
      begin
        Flg:= False;
        DispIdx[I].Max:= DispIdx[I + 1].Max;
      end;
  until Flg;

  DispIdx[0].Min:= 0;
  repeat
    Flg:= True;
    for I:= High(DispIdx) downto 1 do
      if (DispIdx[I    ].Min =  + MaxInt) and
         (DispIdx[I - 1].Min <> + MaxInt) then
      begin
        Flg:= False;
        DispIdx[I].Min:= DispIdx[I - 1].Min;
      end;
  until Flg;
end;

function TRecEngine.MoveRange(Range: TRange; Idx: Integer = 2): TRange;
begin
  case Idx of
    1: begin
         Result.StCrd:= Range.StCrd + Round(75 / (FDatSrc.Header.ScanStep / 100));
         Result.EdCrd:= Range.EdCrd + Round(75 / (FDatSrc.Header.ScanStep / 100));
       end;
    2: begin
         Result.StCrd:= Range.StCrd + Round(75 / (FDatSrc.Header.ScanStep / 100 ) / 2);
         Result.EdCrd:= Range.EdCrd + Round(75 / (FDatSrc.Header.ScanStep / 100 ) / 2);
       end;
  end;
end;

function TRecEngine.MoveCrd(Crd: Integer): Integer;
begin
  Result:= Crd + Round(75 / (FDatSrc.Header.ScanStep / 100));
end;

procedure TRecEngine.ClearItems;
var
  I: Integer;

begin
  for I:= 0 to High(Res) do
  begin
    SetLength(Res[I].Zones, 0);
    SetLength(Res[I].Lines, 0);
    SetLength(Res[I].PBMark, 0);
  end;

  SetLength(Res, 0);
end;

procedure TRecEngine.AddResItem(Typ_: TResType; Rail: RRail_; Range: TRange; Params: string);
begin
  FLastResIdx:= Length(Res);
  SetLength(Res, FLastResIdx + 1);
  Res[FLastResIdx].R:= Rail;
  Res[FLastResIdx].Typ:= Typ_;

  Res[FLastResIdx].Bounds:= Range;
  Res[FLastResIdx].Params:= Params;

//  AddLine(Rail, Range.StCrd, 0, 3, clBlack);
//  AddLine(Rail, Range.EdCrd, 0, 3, clBlack);
end;

procedure TRecEngine.AddZone(Rail: RRail_; Range: TRange; StTape, EdTape: Integer; CrdMode: TCrdMode; Color: Integer);
var
  I: Integer;

begin

  if FLastResIdx <> - 1 then
  begin
    I:= Length(Res[FLastResIdx].Zones);
    SetLength(Res[FLastResIdx].Zones, I + 1);
    Res[FLastResIdx].Zones[I].R:= Rail;

    case CrdMode of
     cm_Normal: Res[FLastResIdx].Zones[I].Crd[0]:= Range;
{     cm_Reduce: begin
                  Res[FLastResIdx].Zones[I].Crd[0].StCrd:= Round(Range.StCrd - (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * Del_.StCrd) / FScanStep);
                  Res[FLastResIdx].Zones[I].Crd[0].EdCrd:= Round(Range.EdCrd - (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * Del_.EdCrd) / FScanStep);
                end;
}    end;

//    Res[FLastResIdx].PBMark[I].Crd[1].StCrd:= Res[FLastResIdx].PBMark[I].Crd[0].StCrd + Round((Config.DisplayConfig.ReducePos[1, 1, Ch] + Config.DisplayConfig.ReduceDel[1, 1, Ch] * Del_.StCrd) / FScanStep);
//    Res[FLastResIdx].PBMark[I].Crd[1].EdCrd:= Res[FLastResIdx].PBMark[I].Crd[0].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 1, Ch] + Config.DisplayConfig.ReduceDel[1, 1, Ch] * Del_.EdCrd) / FScanStep);

//    Res[FLastResIdx].PBMark[I].Crd[2].StCrd:= Res[FLastResIdx].PBMark[I].Crd[0].StCrd + Round((Config.DisplayConfig.ReducePos[1, 2, Ch] + Config.DisplayConfig.ReduceDel[1, 2, Ch] * Del_.StCrd) / FScanStep);
//    Res[FLastResIdx].PBMark[I].Crd[2].EdCrd:= Res[FLastResIdx].PBMark[I].Crd[0].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 2, Ch] + Config.DisplayConfig.ReduceDel[1, 2, Ch] * Del_.EdCrd) / FScanStep);

    Res[FLastResIdx].Zones[I].Crd[0]:= {MoveRange(}Range{)};
    Res[FLastResIdx].Zones[I].Crd[1]:= {MoveRange(}Range{)};
    Res[FLastResIdx].Zones[I].Crd[2]:= {MoveRange(}Range{)};
    Res[FLastResIdx].Zones[I].StTape:= StTape;
    Res[FLastResIdx].Zones[I].EdTape:= EdTape;
    Res[FLastResIdx].Zones[I].Color:= Color;
  end;

end;

procedure TRecEngine.AddLine(Rail: RRail_; Crd_: Integer; StTape, EdTape, Color: Integer);
var
  I: Integer;

begin

  if FLastResIdx <> - 1 then
  begin
    I:= Length(Res[FLastResIdx].Lines);
    SetLength(Res[FLastResIdx].Lines, I + 1);
    Res[FLastResIdx].Lines[I].R:= Rail;
    Res[FLastResIdx].Lines[I].Crd:= {MoveCrd(}Crd_{)};
    Res[FLastResIdx].Lines[I].StTape:= StTape;
    Res[FLastResIdx].Lines[I].EdTape:= EdTape;
    Res[FLastResIdx].Lines[I].Color:= Color;
  end;

end;

procedure TRecEngine.AddPBMark(R: RRail_; Ch: Integer; Crd_, Del: TRange; CrdMode: TCrdMode; Color1, Color2: Integer);
var
  I: Integer;
  Del_: TRange;

begin
  if FLastResIdx <> - 1 then
  begin
    I:= Length(Res[FLastResIdx].PBMark);
    SetLength(Res[FLastResIdx].PBMark, I + 1);
    Res[FLastResIdx].PBMark[I].R:= R;
    Res[FLastResIdx].PBMark[I].Ch:= Ch;
    Res[FLastResIdx].PBMark[I].Del:= Del;
//    Res[FLastResIdx].PBMark[I].Crd:= Crd_;
                {
    if not Odd(Ch) then Del_:= Del else
    begin
      Del_.StCrd:= Del.EdCrd;
      Del_.EdCrd:= Del.StCrd;
    end;
               }
    Del_:= Del;

    case CrdMode of
     cm_Normal: Res[FLastResIdx].PBMark[I].Crd[0]:=Crd_;
     cm_Reduce: begin
                  Res[FLastResIdx].PBMark[I].Crd[0].StCrd:= Round(Crd_.StCrd - (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * Del_.StCrd) / FScanStep);
                  Res[FLastResIdx].PBMark[I].Crd[0].EdCrd:= Round(Crd_.EdCrd - (ChList[Ch].Shift[2] + ChList[Ch].RedPar[2] * Del_.EdCrd) / FScanStep);
                end;
    end;

//DF    Res[FLastResIdx].PBMark[I].Crd[1].StCrd:= Res[FLastResIdx].PBMark[I].Crd[0].StCrd + Round((Config.DisplayConfig.ReducePos[1, 1, Ch] + Config.DisplayConfig.ReduceDel[1, 1, Ch] * Del_.StCrd) / FScanStep);
//DF    Res[FLastResIdx].PBMark[I].Crd[1].EdCrd:= Res[FLastResIdx].PBMark[I].Crd[0].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 1, Ch] + Config.DisplayConfig.ReduceDel[1, 1, Ch] * Del_.EdCrd) / FScanStep);

//DF    Res[FLastResIdx].PBMark[I].Crd[2].StCrd:= Res[FLastResIdx].PBMark[I].Crd[0].StCrd + Round((Config.DisplayConfig.ReducePos[1, 2, Ch] + Config.DisplayConfig.ReduceDel[1, 2, Ch] * Del_.StCrd) / FScanStep);
//DF    Res[FLastResIdx].PBMark[I].Crd[2].EdCrd:= Res[FLastResIdx].PBMark[I].Crd[0].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 2, Ch] + Config.DisplayConfig.ReduceDel[1, 2, Ch] * Del_.EdCrd) / FScanStep);

    Res[FLastResIdx].PBMark[I].Color1:= Color1;
    Res[FLastResIdx].PBMark[I].Color2:= Color2;
  end;
end;

procedure TRecEngine.AddMark(R: RRail_; Crd_: TRange; NoRed: Boolean; MarkList: TMarkList; StTape, EdTape, Color1, Color2: Integer);
var
  I, J: Integer;
  CrdArr: array [0..2, 1..7] of TRange;
  Zone: array [0..2] of TRange;

begin
  if FLastResIdx = - 1 then Exit;

  for I:= 1 to 7 do                                     // �� �������� ����������� ��������� � ��� ��������
    if MarkList[I].Used and MarkList[I].AllDelay then
    begin
      if Odd(I) then
      begin
        if NoRed then
        begin
          CrdArr[0, I].StCrd:= Crd_.StCrd;
          CrdArr[0, I].EdCrd:= Crd_.EdCrd;
          CrdArr[1, I].StCrd:= Crd_.StCrd;
          CrdArr[1, I].EdCrd:= Crd_.EdCrd;
          CrdArr[2, I].StCrd:= Crd_.StCrd;
          CrdArr[2, I].EdCrd:= Crd_.EdCrd;
        end
        else
        begin
//DF          CrdArr[0, I].StCrd:= Round(Crd_.StCrd - (ChList[I].Shift[2] + ChList[I].RedPar[2] * Config.DisplayConfig.BScanViewZone[I].BegDelay) / FScanStep);
//DF          CrdArr[0, I].EdCrd:= Round(Crd_.EdCrd - (ChList[I].Shift[2] + ChList[I].RedPar[2] * Config.DisplayConfig.BScanViewZone[I].EndDelay) / FScanStep);
//DF          CrdArr[1, I].StCrd:= CrdArr[0, I].StCrd + Round((Config.DisplayConfig.ReducePos[1, 1, I] + Config.DisplayConfig.ReduceDel[1, 1, I] * Config.DisplayConfig.BScanViewZone[I].BegDelay) / FScanStep);
//DF          CrdArr[1, I].EdCrd:= CrdArr[0, I].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 1, I] + Config.DisplayConfig.ReduceDel[1, 1, I] * Config.DisplayConfig.BScanViewZone[I].EndDelay) / FScanStep);
//DF          CrdArr[2, I].StCrd:= CrdArr[0, I].StCrd + Round((Config.DisplayConfig.ReducePos[1, 2, I] + Config.DisplayConfig.ReduceDel[1, 2, I] * Config.DisplayConfig.BScanViewZone[I].BegDelay) / FScanStep);
//DF          CrdArr[2, I].EdCrd:= CrdArr[0, I].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 2, I] + Config.DisplayConfig.ReduceDel[1, 2, I] * Config.DisplayConfig.BScanViewZone[I].EndDelay) / FScanStep);
        end;
      end
      else
      begin

        if NoRed then
        begin
          CrdArr[0, I].StCrd:= Crd_.StCrd;
          CrdArr[0, I].EdCrd:= Crd_.EdCrd;
          CrdArr[1, I].StCrd:= Crd_.StCrd;
          CrdArr[1, I].EdCrd:= Crd_.EdCrd;
          CrdArr[2, I].StCrd:= Crd_.StCrd;
          CrdArr[2, I].EdCrd:= Crd_.EdCrd;
        end
        else
        begin
//DF          CrdArr[0, I].StCrd:= Round(Crd_.StCrd - (ChList[I].Shift[2] + ChList[I].RedPar[2] * Config.DisplayConfig.BScanViewZone[I].EndDelay) / FScanStep);
//DF          CrdArr[0, I].EdCrd:= Round(Crd_.EdCrd - (ChList[I].Shift[2] + ChList[I].RedPar[2] * Config.DisplayConfig.BScanViewZone[I].BegDelay) / FScanStep);
//DF          CrdArr[1, I].StCrd:= CrdArr[0, I].StCrd + Round((Config.DisplayConfig.ReducePos[1, 1, I] + Config.DisplayConfig.ReduceDel[1, 1, I] * Config.DisplayConfig.BScanViewZone[I].EndDelay) / FScanStep);
//DF          CrdArr[1, I].EdCrd:= CrdArr[0, I].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 1, I] + Config.DisplayConfig.ReduceDel[1, 1, I] * Config.DisplayConfig.BScanViewZone[I].BegDelay) / FScanStep);
//DF          CrdArr[2, I].StCrd:= CrdArr[0, I].StCrd + Round((Config.DisplayConfig.ReducePos[1, 2, I] + Config.DisplayConfig.ReduceDel[1, 2, I] * Config.DisplayConfig.BScanViewZone[I].EndDelay) / FScanStep);
//DF          CrdArr[2, I].EdCrd:= CrdArr[0, I].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 2, I] + Config.DisplayConfig.ReduceDel[1, 2, I] * Config.DisplayConfig.BScanViewZone[I].BegDelay) / FScanStep);
        end;
       end;
    end else
    if MarkList[I].Used then
    begin
      if Odd(I) then
      begin
        CrdArr[0, I].StCrd:= Round(Crd_.StCrd - (ChList[I].Shift[2] + ChList[I].RedPar[2] * MarkList[I].Delay.StCrd) / FScanStep);
        CrdArr[0, I].EdCrd:= Round(Crd_.EdCrd - (ChList[I].Shift[2] + ChList[I].RedPar[2] * MarkList[I].Delay.EdCrd) / FScanStep);
//DF        CrdArr[1, I].StCrd:= CrdArr[0, I].StCrd + Round((Config.DisplayConfig.ReducePos[1, 1, I] + Config.DisplayConfig.ReduceDel[1, 1, I] * MarkList[I].Delay.StCrd) / FScanStep);
//DF        CrdArr[1, I].EdCrd:= CrdArr[0, I].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 1, I] + Config.DisplayConfig.ReduceDel[1, 1, I] * MarkList[I].Delay.EdCrd) / FScanStep);
//DF        CrdArr[2, I].StCrd:= CrdArr[0, I].StCrd + Round((Config.DisplayConfig.ReducePos[1, 2, I] + Config.DisplayConfig.ReduceDel[1, 2, I] * MarkList[I].Delay.StCrd) / FScanStep);
//DF        CrdArr[2, I].EdCrd:= CrdArr[0, I].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 2, I] + Config.DisplayConfig.ReduceDel[1, 2, I] * MarkList[I].Delay.EdCrd) / FScanStep);
      end
      else
      begin
        CrdArr[0, I].StCrd:= Round(Crd_.StCrd - (ChList[I].Shift[2] + ChList[I].RedPar[2] * MarkList[I].Delay.EdCrd) / FScanStep);
        CrdArr[0, I].EdCrd:= Round(Crd_.EdCrd - (ChList[I].Shift[2] + ChList[I].RedPar[2] * MarkList[I].Delay.StCrd) / FScanStep);
//DF        CrdArr[1, I].StCrd:= CrdArr[0, I].StCrd + Round((Config.DisplayConfig.ReducePos[1, 1, I] + Config.DisplayConfig.ReduceDel[1, 1, I] * MarkList[I].Delay.EdCrd) / FScanStep);
//DF        CrdArr[1, I].EdCrd:= CrdArr[0, I].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 1, I] + Config.DisplayConfig.ReduceDel[1, 1, I] * MarkList[I].Delay.StCrd) / FScanStep);
//DF        CrdArr[2, I].StCrd:= CrdArr[0, I].StCrd + Round((Config.DisplayConfig.ReducePos[1, 2, I] + Config.DisplayConfig.ReduceDel[1, 2, I] * MarkList[I].Delay.EdCrd) / FScanStep);
//DF        CrdArr[2, I].EdCrd:= CrdArr[0, I].EdCrd + Round((Config.DisplayConfig.ReducePos[1, 2, I] + Config.DisplayConfig.ReduceDel[1, 2, I] * MarkList[I].Delay.StCrd) / FScanStep);
      end;
    end;

  for J:= 0 to 2 do
  begin
    Zone[J].StCrd:= MaxInt;
    Zone[J].EdCrd:= - MaxInt;
    for I:= 1 to 7 do
      if MarkList[I].Used then
      begin
        Zone[J].StCrd:= Min(Zone[J].StCrd, CrdArr[J, I].StCrd);
        Zone[J].StCrd:= Min(Zone[J].StCrd, CrdArr[J, I].EdCrd);

        Zone[J].EdCrd:= Max(Zone[J].EdCrd, CrdArr[J, I].EdCrd);
        Zone[J].EdCrd:= Max(Zone[J].EdCrd, CrdArr[J, I].StCrd);
      end;
  end;

  I:= Length(Res[FLastResIdx].Zones);
  SetLength(Res[FLastResIdx].Zones, I + 1);
  Res[FLastResIdx].Zones[I].R:= R;

  Res[FLastResIdx].Zones[I].Crd[0]:= Zone[0];
  Res[FLastResIdx].Zones[I].Crd[1]:= Zone[1];
  Res[FLastResIdx].Zones[I].Crd[2]:= Zone[2];
  Res[FLastResIdx].Zones[I].StTape:= StTape;
  Res[FLastResIdx].Zones[I].EdTape:= EdTape;
  Res[FLastResIdx].Zones[I].Color:= Color1;
end;

procedure TRecEngine.ResItemRangeByPBList;
var
  I: Integer;

begin
{  Res[FLastResIdx].Bounds.StCrd:= + MaxInt;
  Res[FLastResIdx].Bounds.EdCrd:= - MaxInt;
  for I:= 0 to High(Res[FLastResIdx].PBMark) do
  begin
    Res[FLastResIdx].Bounds.StCrd:= Min(Res[FLastResIdx].Bounds.StCrd, Res[FLastResIdx].PBMark[I].Crd.StCrd);
    Res[FLastResIdx].Bounds.EdCrd:= Max(Res[FLastResIdx].Bounds.EdCrd, Res[FLastResIdx].PBMark[I].Crd.EdCrd);
  end;}
end;

end.
