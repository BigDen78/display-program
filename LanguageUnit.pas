unit LanguageUnit;

interface

uses
  Classes, Forms, SysUtils, Dialogs, CfgTablesUnit_2010;

type

 TLangItem = record
   ID: Integer;
   Rus: string;
   Eng: string;
 end;

 TLangDat = array of TLangItem;
(*
 TProgLanguage = class
   FDat: TStringList;
   FLangDat: TLangDat;
   FWorkLang: Integer;
   constructor Create;
   destructor Destroy; override;
   function GetCaption(ID: Integer): string;
   property WorkLang: Integer read FWorkLang write FWorkLang;
 end;
 *)
var
//  Language: TProgLanguage;
  LangTable: TLanguageTable;

implementation

uses ConfigUnit;
(*
constructor TProgLanguage.Create;
var
  I, J, ID: Integer;
  S1, S2, S3: string;
  Code: Integer;

begin
{  FDat:= TStringList.Create;
  FDat.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Language.Dat');

  for I:= 0 to FDat.Count - 1 do
  begin
    J:= Pos('=', FDat.Strings[I]);
    if J = 0 then
    begin
      S1:= FDat.Strings[I];
      S2:= '';
      S3:= '';
    end
    else
    begin
      S1:= Copy(FDat.Strings[I], 1, J - 1);
      S2:= Copy(FDat.Strings[I], J + 1, Length(FDat.Strings[I]) - J);
      S3:= '';
    end;

    J:= Pos('=', S2);
    if J = 0 then
    begin
    end
    else
    begin
      S3:= Copy(S2, J + 1, Length(S2) - J);
      S2:= Copy(S2, 1, J - 1);
    end;

    Val(S1, ID, Code);

    if Code = 0 then
    begin
      SetLength(FLangDat, Length(FLangDat) + 1);
      FLangDat[High(FLangDat)].ID:= ID;
      FLangDat[High(FLangDat)].Rus:= S2;
      FLangDat[High(FLangDat)].Eng:= S3;
    end;
  end;
        {
  for I:= 0 to High(FLangDat) do
    for J:= 0 to High(FLangDat) do
     if (I <> J) and (FLangDat[I].ID = FLangDat[J].ID) then ShowMessage(Format('Double ID: %d', [FLangDat[J].ID]));
            }
end;

destructor TProgLanguage.Destroy;
begin
  FDat.Free;
  FDat:= nil;
  inherited Destroy;
end;

function TProgLanguage.GetCaption(ID: Integer): string;
var
  I: Integer;

begin
  for I:= 0 to High(FLangDat) do
    if FLangDat[I].ID = ID then
    begin
      case FWorkLang of
        0: Result:= FLangDat[I].Rus;
        1: Result:= FLangDat[I].Eng;
      end;
      Exit;
    end;
end;
  *)
end.


