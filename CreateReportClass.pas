unit CreateReportClass;

interface

uses {FR_Class,} Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ConfigUnit, LanguageUnit;

type
   TReportDataPage = record
    RZDOrgName: string;
    RepNo: Integer;
    RepDate: string;
    DefName: string;
    AVikonNum: string;
    Path: Integer;
    Rail: string;
    Km: Integer;
    Piket: Integer;
    Metre: Integer;
    ViewAs: string;
    Assign: string;
    FileName: string;
    Section: string;
    ScrMetre: Single;
    ViewTh: string;
    ViewMode: string;
    Oper: string;
    Prim: string;
    ManName: string; // ������������
    FindAt: string; // ��������� ��� ...
    ViewType: string; // ������ ...
    Pic1: TPicture;
    Pic2: TPicture;
  end;

  TReportData = array of TReportDataPage;

  TCreateReportClass = class
{     SrcRep1: TfrReport;
     SrcRep2: TfrReport;
     DestRep: TfrReport; }

     Data: TReportData;
     VedomostNomer: string;
     Peregon: string;
     Nachalo: string;
     Konec: string;
     KontrolDate: string;
     RasshifrDate: string;

     constructor Create;
     destructor Destroy;
     procedure Generate(Vedomost :Boolean);
     procedure ClearData;
     procedure ShowReport;
     procedure PrintReport;
  end;

implementation

// { $ R RepDATA.RES}

procedure TCreateReportClass.Generate(Vedomost :Boolean);

procedure CreateMemo(X, Y, DX, DY, PageNum: Integer; Text: string; Adjust: Integer; FrameTyp: Word);
{
var
  m: TfrMemoView;
}
Begin
{  m:= TfrMemoView.Create;
  m.x:= X;
  m.y:= Y;
  m.dx:= DX;
  m.dy:= DY;
  m.Typ:= gtMemo;
  m.FrameTyp:= FrameTyp;
  m.Font.Name:= 'Arial cyr';
  m.Font.Size:= 10;
  m.Memo.Text:= Text;
  m.Adjust:= Adjust;
  DestRep.Pages.Pages[PageNum].Objects.Add(m);
  }
end;

const
    Coord: array[0..9] of Integer = (36, 72, 116, 176, 220, 296, 368, 488, 620, 732);

var
  I, Obj, PagesCount, j, k: Integer;
{  v: TfrView;
  m: TfrMemoView;
  p: TfrPictureView;
  VarName: string;
  VarPar: string;

  Rep1: TResourceStream;
  Rep2Part1: TResourceStream;
  Rep2Part2: TResourceStream;

  Rep1En: TResourceStream;
  Rep2Part1En: TResourceStream;
  Rep2Part2En: TResourceStream;

 }
begin
 { Rep1       := TResourceStream.Create(Hinstance, 'Rep1',      RT_RCDATA);
  Rep2Part1  := TResourceStream.Create(Hinstance, 'Rep2Part1', RT_RCDATA);
  Rep2Part2  := TResourceStream.Create(Hinstance, 'Rep2Part2', RT_RCDATA);

  Rep1En     := TResourceStream.Create(Hinstance, 'Rep1En',      RT_RCDATA);
  Rep2Part1En:= TResourceStream.Create(Hinstance, 'Rep2Part1En', RT_RCDATA);
  Rep2Part2En:= TResourceStream.Create(Hinstance, 'Rep2Part2En', RT_RCDATA);

  if Config.WorkLang = 0 then
  begin
    SrcRep1.LoadFromStream(Rep1); // LoadFromFile(ExtractFilePath(Application.ExeName) + 'Rep1.frf');
    if Vedomost then SrcRep2.LoadFromStream(Rep2Part1); // LoadFromFile(ExtractFilePath(Application.ExeName) + 'Rep2Part1.frf');
  end
  else
  begin
    SrcRep1.LoadFromStream(Rep1En); // LoadFromFile(ExtractFilePath(Application.ExeName) + 'Rep1En.frf');
    if Vedomost then SrcRep2.LoadFromStream(Rep2Part1En) //LoadFromFile(ExtractFilePath(Application.ExeName) + 'Rep2Part1En.frf');
  end;

  for I:= 0 to High(Data) do
  begin
    DestRep.Pages.Add;
    for Obj:= 0 to SrcRep1.Pages[0].Objects.Count - 1 do
    begin
      v:= SrcRep1.Pages[0].Objects.Items[Obj];
      if v is TfrMemoView then
      begin
        m:= TfrMemoView.Create;
        m.Assign(v);
        if TfrMemoView(v).Memo.Count <> 0 then VarName:= TfrMemoView(v).Memo.Strings[0];
        VarPar:= 'NoDataFlag';
        if VarName = '<RZDOrgName>'  then VarPar:= Data[I].RZDOrgName;
        if VarName = '<RepNo>'       then VarPar:= IntToStr(Data[I].RepNo);
        if VarName = '<RepDate>'     then VarPar:= Data[I].RepDate;
        if VarName = '<DefName>'     then VarPar:= Data[I].DefName;
        if VarName = '<AvikonNum>'   then VarPar:= Data[I].AVikonNum;
        if VarName = '<Path>'        then VarPar:= IntToStr(Data[I].Path);
        if VarName = '<Rail>'        then VarPar:= Data[I].Rail;
        if VarName = '<Km>'          then VarPar:= IntToStr(Data[I].Km);
        if VarName = '<Piket>'       then VarPar:= IntToStr(Data[I].Piket);
        if VarName = '<Metre>'       then VarPar:= IntToStr(Data[I].Metre);
        if VarName = '<ViewAs>'      then VarPar:= Data[I].ViewAs;
        if VarName = '<Assign>'      then VarPar:= Data[I].Assign;
        if VarName = '<FileName>'    then VarPar:= Data[I].FileName;
        if VarName = '<Section>'     then VarPar:= Data[I].Section;
        if VarName = '<ScrMetre>'    then VarPar:= Format('%f', [Data[I].ScrMetre]);
        if VarName = '<ViewTh>'      then VarPar:= Data[I].ViewTh;
        if VarName = '<ViewMode>'    then VarPar:= Data[I].ViewMode;
        if VarName = '<Oper>'        then VarPar:= Data[I].Oper;
        if VarName = '<Primechanie>' then VarPar:= Data[I].Prim;
        if VarName = '<Oper2>'       then VarPar:= Data[I].ManName;
        if VarName = '<FindAt>'      then
          VarPar:= Data[I].FindAt;
        if VarName = '<ViewType>'    then
          VarPar:= Data[I].ViewType;
        if VarPar <> 'NoDataFlag'    then m.Memo.Text:= VarPar
                                     else m.Memo.Text:= TfrMemoView(v).Memo.Text;
        DestRep.Pages.Pages[I].Objects.Add(m);
      end;

      if v is TfrPictureView then
      begin
        p:= TfrPictureView.Create;
        p.Assign(v);
        if TfrPictureView(v).Memo.Count <> 0 then VarName:= TfrMemoView(v).Memo.Strings[0];
        VarPar:= 'NoDataFlag';
        if (VarName = '<Picture1>') and Assigned(Data[I].Pic1) then p.Picture.Assign(Data[I].Pic1);
        if (VarName = '<Picture2>') and Assigned(Data[I].Pic2) then p.Picture.Assign(Data[I].Pic2);
        DestRep.Pages.Pages[I].Objects.Add(p);
      end;
    end;
  end;
   PagesCount:=High(Data)+1;


  If Vedomost Then
    Begin
      DestRep.Pages.Add;
      for Obj:= 0 to SrcRep2.Pages[0].Objects.Count - 1 do
         Begin
           v:= SrcRep2.Pages[0].Objects.Items[Obj];

           if v is TfrMemoView then
             begin
               m:= TfrMemoView.Create;
               m.Assign(v);
               if TfrMemoView(v).Memo.Count <> 0 then VarName:= TfrMemoView(v).Memo.Strings[0];
               VarPar:= 'NoDataFlag';
               if VarName = '<VedomostNomer>' then VarPar:= VedomostNomer;
               if VarName = '<Peregon>' then VarPar:= Peregon;
               if VarName = '<Nachalo>' then VarPar:= Nachalo;
               if VarName = '<Konec>' then VarPar:= Konec;
               if VarName = '<KontrolDate>' then VarPar:= KontrolDate;
               if VarName = '<RasshifrDate>' then VarPar:= RasshifrDate;
               if VarName = '<FFNN>' then VarPar:= Data[0].FileName;
               if VarPar <> 'NoDataFlag' then m.Memo.Text:= VarPar
                 else m.Memo.Text:= TfrMemoView(v).Memo.Text;
               DestRep.Pages.Pages[PagesCount].Objects.Add(m);
             end;
         end;
      k:=13;
     for j:=0 To High(Data) Do
        begin
          for i:=0 to 8 Do
             begin
               VarPar:='';
               case i of
                  0: VarPar:=IntToStr(j+1);
                  1: VarPar:=IntToStr(Data[J].Km);
                  2: VarPar:=IntToStr(Data[J].Piket);
                  3: VarPar:=IntToStr(Data[J].Metre);
                  4: VarPar:=IntToStr(Data[J].Path);
                  5: VarPar:=Data[J].Rail;
                  6: VarPar:=Data[J].ViewAs;
               end;
               if i=0 then CreateMemo(Coord[i],(k*20)-2,Coord[i+1]-Coord[i],20, PagesCount, VarPar, 2, 15)
                 else CreateMemo(Coord[i],(k*20)-2,Coord[i+1]-Coord[i],20, PagesCount, VarPar, 2, 15);
             end;
          if (J=36) or (k mod 49 = 0) Then
            begin
              if PagesCount-High(Data) <> 1 then CreateMemo(690,(k*20)+50,50,20, PagesCount, LangTable.Caption['Notebook:Page'] + ' ' +IntToStr(PagesCount-High(Data)), 0, 0);
              DestRep.Pages.Add;
              PagesCount:=PagesCount+1;
              k:=0;
            end;
          k:=k+1;
        end;

      if Config.WorkLang = 0 then SrcRep2.LoadFromStream(Rep2Part2) // LoadFromFile(ExtractFilePath(Application.ExeName) + 'Rep2Part2.frf')
                             else SrcRep2.LoadFromStream(Rep2Part2En); // LoadFromFile(ExtractFilePath(Application.ExeName) + 'Rep2Part2En.frf');

      for Obj:= 0 to SrcRep2.Pages[0].Objects.Count - 1 do
         begin
           v:= SrcRep2.Pages[0].Objects.Items[Obj];
           if v is TfrMemoView then
             begin
               m:= TfrMemoView.Create;
               m.Assign(v);
               m.y:=(k*20)+40+Obj*30;
               if TfrMemoView(v).Memo.Count <> 0 then VarName:= TfrMemoView(v).Memo.Strings[0];
               VarPar:= 'NoDataFlag';
               m.Memo.Text:= TfrMemoView(v).Memo.Text;
               DestRep.Pages.Pages[PagesCount].Objects.Add(m);
             end;
         end;
    End;
  if PagesCount-High(Data) <> 1 then CreateMemo(690,1030,50,20, PagesCount, LangTable.Caption['Notebook:Page'] + ' ' + IntToStr(PagesCount-High(Data)), 0, 0);

  Rep1.Free;
  Rep2Part1.Free;
  Rep2Part2.Free;

  Rep1En.Free;
  Rep2Part1En.Free;
  Rep2Part2En.Free;
  }
end;

constructor TCreateReportClass.Create;
begin
//  SrcRep1:= TfrReport.Create(nil);
//  SrcRep2:= TfrReport.Create(nil);
//  DestRep:= TfrReport.Create(nil);
end;

destructor TCreateReportClass.Destroy;
begin
//  SrcRep1.Free;
//  SrcRep2.Free;
//  DestRep.Free;
end;

procedure TCreateReportClass.ClearData;
begin
  SetLength(Data, 0);
end;

procedure TCreateReportClass.ShowReport;
begin
//  DestRep.ShowReport(Config.WorkLang, False);
end;

procedure TCreateReportClass.PrintReport;
begin
//  DestRep.ShowReport(Config.WorkLang, True);
end;

end.
