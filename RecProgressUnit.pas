unit RecProgressUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Gauges, ExtCtrls, ComCtrls, StdCtrls;

type
  TRecProgressForm = class(TForm)
    PB: TProgressBar;
    Panel1: TPanel;
    Panel2: TPanel;
    CancelButton: TButton;
    Panel3: TPanel;
    procedure OnProgress(Sender: TObject);
  private
    FMin: Integer;
    FMax: Integer;
  public
    procedure SetDiap(Min, Max: Integer);
    procedure SetMode(Mode: Integer);
  end;

var
  RecProgressForm: TRecProgressForm;

implementation

{$R *.dfm}

procedure TRecProgressForm.OnProgress(Sender: TObject);
var
  NewPos: Integer;

begin
  NewPos:= FMin + Round((FMax - FMin) * Integer(Sender) / 100);
  if NewPos <> PB.Position then PB.Position:= NewPos;
end;

procedure TRecProgressForm.SetDiap(Min, Max: Integer);
begin
  FMin:= Min;
  FMax:= Max;
  PB.Position:= Min;
end;

procedure TRecProgressForm.SetMode(Mode: Integer);
begin
  case Mode of
    1: begin
         Self.Height:= 86;
         Panel1.Visible:= False;
         Self.BorderStyle:= bsDialog;
         Self.BorderWidth:= 12;
       end;
    2: begin
         Self.Height:= 26;
         Panel1.Visible:= True;
         Self.BorderWidth:= 0;
         Panel1.BorderStyle:= bsNone;
       end;
  end;
end;  

end.
