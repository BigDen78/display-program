unit EventListUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, LanguageUnit;

type
  TEventListForm = class(TForm)
    EventListBox: TListBox;
  private
    { Private declarations }
  public
    procedure SetData(Text: string; CrdTxt: string);
  end;

var
  EventListForm: TEventListForm;

implementation

{$R *.dfm}

procedure TEventListForm.SetData(Text: string; CrdTxt: string);
begin
  self.Caption:= '���������� - ' + CrdTxt; // LangTable.Caption['MenuView:']; //NO_LANGUAGE
  repeat
    if Pos(Chr(10), Text) <> 0 then EventListBox.Items.Add(Copy(Text, 1, Pos(#10, Text) - 1))
                               else
                               begin
                                 EventListBox.Items.Add(Text);
                                 Break;
                               end;
    Text:= Copy(Text, Pos(Chr(10), Text) + 1, Length(Text));
  until False;
end;

end.
