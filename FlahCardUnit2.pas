{$I DEF.INC}
unit FlahCardUnit2;  {Language 10}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Mask, TB2Item, TB2Dock, TB2Toolbar, Registry,
  ImgList, ComCtrls, FileCtrl, Buttons, Math, LanguageUnit, TB2ExtItems, ShellApi,
  MyTypes, AviconTypes, ShlObj, ActiveX;

type

  TCreateDirListProc = class (TThread)
  protected

    Drives: TStringList;
    procedure Execute; override;
    procedure SearchDrives(DiskList: TStrings);

  public

    List: TStringList;
    Paths: TStringList;
    LastPaths: TStringList;
    ListOK: TNotifyEvent;
    Stop: Boolean;

    constructor Create;
    destructor Destroy;
  end;


  TFileListItem = packed record
    Data: TFileHeader;
    Name: string;
  end;

//  TDriveState = record
//    Select: string;
//    Count: Integer;
//  end;

  TFlashCardForm2 = class(TForm)
    ImageList1: TImageList;
    SaveDialog1: TSaveDialog;
    PageControl2: TPageControl;
    WaitSheet1: TTabSheet;
    Panel10: TPanel;
    lWait: TLabel;
    ProgressBar1: TProgressBar;
    Panel12: TPanel;
    SaveDialog2: TSaveDialog;
    Timer2: TTimer;
    OpenDialog1: TOpenDialog;
    ImageList2: TImageList;
    ReadFiles: TTabSheet;
    Panel1: TPanel;
    TBDock1: TTBDock;
    MainToolbar: TTBToolbar;
    sbUpdate: TTBItem;
    sbLoad: TTBItem;
    TBItem1: TTBItem;
    TBControlItem1: TTBControlItem;
    tbiDelFiles: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    tbiSelectAll: TTBItem;
    cbAskFileName: TCheckBox;
    TBSeparatorItem1: TTBSeparatorItem;
    TBSeparatorItem3: TTBSeparatorItem;
    Panel32: TPanel;
    Panel34: TPanel;
    bClose: TButton;
    OpenDialog2: TOpenDialog;
    SaveDialog3: TSaveDialog;
    ListView1: TListView;
    WaitSheet3: TTabSheet;
    lConnect: TLabel;
    Panel26: TPanel;
    lDisk: TLabel;
    cbDiskList: TComboBox;
    procedure Button2Click(Sender: TObject);
    procedure CreateFileList;
    procedure sbUpdateClick(Sender: TObject);
    procedure cbAskFileNameClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure OnChangeLanguage(Sender: TObject);
    procedure tbiSelectAllClick(Sender: TObject);
    procedure tbiDelFilesClick(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DirsListOk(Sender: TObject);

  private
    BusyFlag: Boolean;
    ModeIdx: Integer;
    CurrCharSet: Integer;
    CurrCharSet2: Integer;
    FileList: array of TFileListItem;
    DirList: TCreateDirListProc;
//    LastState: TDriveState;
//    LastSearchPaths: TStringList;

    SearchPaths: TStringList;

//    procedure CreateDirList;
    procedure DeleteEmptyDir(Path: string);
  public
    { Public declarations }
  end;

var
  FlashCardForm2: TFlashCardForm2;

implementation

uses
  ConfigUnit, CheckFileAccessUnit;

var
  ProcRes: Integer;

{$R *.DFM}

function TestFileAttr(FN: string): Boolean;
var
  Attr: Integer;

begin
  Attr := FileGetAttr(FN);
  Result:= ((Attr and faHidden) = 0) and   // 'Hidden';
           ((Attr and faReadOnly) = 0) and // 'Read-Only';
           ((Attr and faSysFile) = 0) { and  // 'System';
           ((Attr and faArchive) = 0)};     // 'Archive';
end;

function CodeToDate(Day, Month, Year: Byte; Razdelitel: Char) : string;
var
  S: string;
  I: Integer;

begin
  Result:= Format('%d' + Razdelitel + '%d' + Razdelitel + '%d', [(Day   shr 4 and $0F) * 10 + (Day   and $0F),
                                                 (Month shr 4 and $0F) * 10 + (Month and $0F),
                                                 (Year  shr 4 and $0F) * 10 + (Year  and $0F + 2000)]);
end;

function CodeToTime(Hour, Minute: Byte) : string;
var
  S: string;
  I: Integer;

begin
  Result:= Format('%d:%d', [(Hour   shr 4 and $0F) * 10 + (Hour   and $0F),
                            (Minute shr 4 and $0F) * 10 + (Minute and $0F)]);
end;

// ---------------------------------------------------------------------------------------------------------

procedure ScanDir(StartDir: string; Mask: string; List: TStrings);
var
  SearchRec: TSearchRec;

begin
  if Mask = '' then Mask := '*.*';
  if StartDir[Length(StartDir)] <> '\' then
  StartDir := StartDir + '\';
  try
    if FindFirst(StartDir + Mask, faAnyFile, SearchRec) = 0 then
    begin
      repeat
      //  Application.ProcessMessages;
        if (SearchRec.Attr and faDirectory) <> faDirectory
          then List.AddObject(StartDir + SearchRec.Name, Pointer(SearchRec.Size))
          else if (SearchRec.Name <> '..') and (SearchRec.Name <> '.') then
          begin
            if List.IndexOf(StartDir + SearchRec.Name + '\') = - 1 then
              List.AddObject(StartDir + SearchRec.Name + '\', Pointer(0));
            ScanDir(StartDir + SearchRec.Name + '\', Mask, List);
          end;
      until FindNext(SearchRec) <> 0;
      FindClose(SearchRec);
    end;
  except
    FindClose(SearchRec);
  end;
end;

// ---------------------------------------------------------------------------------------------------------

procedure ScanDir_(StartDir: string; Mask: string; List: TStrings);
var
  SearchRec: TSearchRec;

begin
  if Mask = '' then Mask := '*.*';
  if StartDir[Length(StartDir)] <> '\' then
  StartDir := StartDir + '\';
  try
    if FindFirst(StartDir + Mask, faDirectory, SearchRec) = 0 then
    begin
      repeat
      //  Application.ProcessMessages;
        if (SearchRec.Attr and faDirectory) = faDirectory then
          if (SearchRec.Name <> '..') and (SearchRec.Name <> '.') then
          begin
          //  List.Add(StartDir + SearchRec.Name);
            if List.IndexOf(StartDir + SearchRec.Name) = - 1 then
              List.AddObject(StartDir + SearchRec.Name, Pointer(0));
            ScanDir_(StartDir + SearchRec.Name + '\', Mask, List);
          end;
      until FindNext(SearchRec) <> 0;
      FindClose(SearchRec);
    end;
  except
    FindClose(SearchRec);
  end;
end;

// ---------------------------------------------------------------------------------------------------------

procedure TFlashCardForm2.StartButtonClick(Sender: TObject);
begin
  Timer2.Enabled:= true;
end;

procedure TFlashCardForm2.FormCreate(Sender: TObject);

begin
  DirList:= TCreateDirListProc.Create;
  DirList.ListOK:= DirsListOk;
  DirList.Resume;

//  LastSearchPaths:= TStringList.Create;
  SearchPaths:= TStringList.Create;

//  LastState.Paths:= TStringList.Create;
//  SearchDrives(cbDiskList.Items);
//  if cbDiskList.Items.Count <> 0 then cbDiskList.ItemIndex := 0;

  WaitSheet1.TabVisible:= False;
  cbAskFileName.Checked:= Config.AskFlashName;
  Self.Refresh;

  ReadFiles.TabVisible:= False;
  WaitSheet3.TabVisible:= False;

  OnChangeLanguage(nil);
  Self.Width := 748;

//  LastState.Select:= '';
//  LastState.Count:= - 1;
//  Timer2Timer(nil);

//  CreateFileList;
//  ReadFiles.Show;
end;

procedure TFlashCardForm2.DirsListOk(Sender: TObject);
begin
  SearchPaths.Clear;
  SearchPaths.Assign(TStringList(Sender));
  Timer2.Enabled:= true;
//  SearchPaths.SaveToFile('D:\!!\flash_log' + IntToStr(GetTickCount()) + '.txt');
end;

procedure TFlashCardForm2.FormDestroy(Sender: TObject);
begin
  DirList.Terminate;
  while not DirList.Stop do Application.ProcessMessages;
  DirList.Free;

//  LastState.Paths.Free;
//  LastSearchPaths.Free;
  SearchPaths.Free;
end;

procedure TFlashCardForm2.CreateFileList;
var
  S1: string;
  S2: string;
  J: Integer;
  Res: Integer;
  Chip: Integer;
  Page: Integer;
  Item: TListItem;
  DecCount: Integer;
  TestCRCFlag: Boolean;
  ss: Integer;
  LastCalcCS: Byte;
  Flg: Boolean;
  FData: file;
  B1: Byte;
  B2: Byte;
  List: TStringList;
  F: file;
  NewItem: TFileListItem;
  FileID: TAviconID;
  Crd: TRealCoord;

begin
  SetLength(FileList, 0);
  tbiSelectAll.Tag:= 0;
//  if cbDiskList.Items.Count = 0 then Exit;

  ProgressBar1.Position:= 0;
  WaitSheet1.Show;
  Self.Refresh;

  ListView1.Items.Clear;

  List:= TStringList.Create;

  for J:= 0 to SearchPaths.Count - 1 do
    ScanDir(SearchPaths[J], '*.*', List);

  if List.Count <> 0 then
  begin


    ProgressBar1.Min:= 0;
    ProgressBar1.Max:= {Min(512, }List.Count - 1{)};

    for J:= 0 to {Min(512, }List.Count - 1{)} do
    begin
      ProgressBar1.Position:= J;
      ProgressBar1.Refresh;
      Application.ProcessMessages;
      if FileExists(List[J]) and
         TestFileAttr(List[J]) and
         (Integer(List.Objects[J]) >= SizeOf(TFileHeader)) { and
         (Integer(List.Objects[J]) < 64 * 1024 * 1024)} then
      begin
        AssignFile(F, List[J]);
        try
          Reset(F, 1);
          BlockRead(F, NewItem.Data, SizeOf(TFileHeader));
        except
          CloseFile(F);
          Continue;
        end;
        CloseFile(F);

        NewItem.Name:= List[J];
        if CompareMem(@NewItem.Data.FileID, @AviconFileID, 8) then
        begin
          SetLength(FileList, Length(FileList) + 1);
          FileList[High(FileList)]:= NewItem;

          Item:= ListView1.Items.Add;
          Item.Caption:= '';
          Item.Data:= Pointer(J);

          with NewItem.Data do
          begin
            Item.SubItems.Add(Format('%s:%s:%d %s:%s', [UpgradeInt(Day, '0', 2), UpgradeInt(Month, '0', 2), Year, UpgradeInt(Hour, '0', 2), UpgradeInt(Minute, '0', 2)]));
            Item.SubItems.Add(HeaderStrToString(PathSectionName));

            if NewItem.Data.UsedItems[uiRailPathNumber] = 1 then Item.SubItems.Add(IntToStr(RailPathNumber));
            if NewItem.Data.UsedItems[uiRailPathTextNumber] = 1 then Item.SubItems.Add(HeaderStrToString(RailPathTextNumber));

            Crd.Sys:= TCoordSys(PathCoordSystem);
            Crd.MRFCrd.Km:= StartKM;
            Crd.MRFCrd.Pk:= StartPk;
            Crd.MRFCrd.mm:= StartMetre;
            Crd.CaCrd:= StartChainage;

            Item.SubItems.Add(RealCrdToStr(Crd));
            Item.SubItems.Add(HeaderStrToString(OperatorName));
            Item.SubItems.Add(NewItem.Name);
          end;
        end;
      end;
    end;

//    Log.SaveToFile('D:\flash_log.txt');
//    Log.Free;

  end;
  List.Free;
  ReadFiles.Show;
  Self.Refresh;
end;

function SelectDirCB(Wnd: HWND; uMsg: UINT; lParam, lpData: LPARAM): Integer stdcall;
begin
  if (uMsg = BFFM_INITIALIZED) and (lpData <> 0) then
    SendMessage(Wnd, BFFM_SETSELECTION, Integer(True), lpdata);
  result := 0;
end;

function MySelectDirectory(const Caption: string; const Root: WideString; var Directory: string): Boolean;
var
  WindowList: Pointer;
  BrowseInfo: TBrowseInfo;
  Buffer: PChar;
  OldErrorMode: Cardinal;
  RootItemIDList, ItemIDList: PItemIDList;
  ShellMalloc: IMalloc;
  IDesktopFolder: IShellFolder;
  Eaten, Flags: LongWord;

begin
  Result := False;
  if not DirectoryExists(Directory) then
    Directory := '';
  FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
  if (ShGetMalloc(ShellMalloc) = S_OK) and (ShellMalloc <> nil) then
  begin
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      RootItemIDList := nil;
      if Root <> '' then
      begin
        SHGetDesktopFolder(IDesktopFolder);
        IDesktopFolder.ParseDisplayName(Application.Handle, nil,
          POleStr(Root), Eaten, RootItemIDList, Flags);
      end;
      with BrowseInfo do
      begin
        hwndOwner := Application.Handle;
        pidlRoot := RootItemIDList;
        pszDisplayName := Buffer;
        lpszTitle := PChar(Caption);
        ulFlags := BIF_RETURNONLYFSDIRS or BIF_NEWDIALOGSTYLE;
        if Directory <> '' then
        begin
          lpfn := SelectDirCB;
          lParam := Integer(PChar(Directory));
        end;
      end;
      WindowList := DisableTaskWindows(0);
      OldErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS);
      try
        ItemIDList := ShBrowseForFolder(BrowseInfo);
      finally
        SetErrorMode(OldErrorMode);
        EnableTaskWindows(WindowList);
      end;
      Result :=  ItemIDList <> nil;
      if Result then
      begin
        ShGetPathFromIDList(ItemIDList, Buffer);
        ShellMalloc.Free(ItemIDList);
        Directory := Buffer;
      end;
    finally
      ShellMalloc.Free(Buffer);
    end;
  end;
end;


procedure TFlashCardForm2.Button2Click(Sender: TObject);
label
  L1;

var
  I, J, K, QQ: Integer;
  Res: Integer;
  ResFile: TMemoryStream;
  Flag: Boolean;
  T1: Integer;
  S, SS: Integer;
  S1, S2: string;
  F: string;
  FName: string;
  FName2: string;
  Dir: string;
  ResFileName: string;
//  ResFileName2: string;

function GetName(I: Integer): string;
{
var
  J, K, QQ: Integer;
  Res: Integer;
  ResFile: TMemoryStream;
  Flag: Boolean;
  T1: Integer;
  S, SS: Integer;
  S1, S2: string;
  F: string;
  FName: string;
  FName2: string;
  Dir: string;
  ResFileName: string;
  ResFileName2: string;
}
begin

  Result:= ExtractFileName(FileList[I].Name);

(*  with TAvk11Header2((@FileList[I].Data)^) do   // ������������ ����� �����
  begin
    S1:= IntToStr(Day and $0F + (Day shr 4) * 10);
    while Length(S1) < 2 do S1:= '0' + S1;
    S2:= S1;
    S1:= IntToStr(Month and $0F + (Month shr 4) * 10);
    while Length(S1) < 2 do S1:= '0' + S1;
    S2:= S2 + S1;
    S1:= IntToStr(Year and $0F + (Year shr 4) * 10);
    while Length(S1) < 2 do S1:= '0' + S1;
    S2:= S2 + S1;
    FName:= '';
    for J:= 0 to 15 do
      if Config.FNStruc[J].On_ then
      begin
        case Config.FNStruc[J].Idx of
          9900001: F:= S2; //CodeToDate(Day, Month, Year, '_');                       // ���� �������
          9900002: F:= Format('%d' + LangTable.Caption['Misc:km'] + '%d' + LangTable.Caption['Misc:pk'], [StartKM, StartPk]);                        // ��������� ����������
          9900003: F:= Format('%d', [I]);                                       // ���������� ����� �����
          9900004: F:= Format('No%d', [UpDeviceID]);                                  // ��������� ����� ���
          9900005: F:= Format('No%d', [DwnDeviceID]);                                 // ��������� ����� ���
          9900006: begin                                                              // ����� ������ �������
                     S1:= IntToStr((Hour   shr 4 and $0F) * 10 + (Hour   and $0F));
                     S2:= IntToStr((Minute shr 4 and $0F) * 10 + (Minute and $0F));
                     while Length(S1) < 2 do S1:= '0' + S1;
                     while Length(S2) < 2 do S2:= '0' + S2;
                     F:= S1 + '-'+ S2;
                   end;
          9900007: F:= CodeToText(Operator, CharSet);                                 // ��������
          9900008: F:= CodeToText(Name, CharSet);                                     // �������� ������������� ��� ���
          9900009: F:= Format('%d', [StartKM]);                                       // ��������� ��������
          9900010: F:= Format('%d', [StartPk]);                                       // ��������� �����
          9900011: F:= Format('%d', [StartMetre]);                                    // ��������� ����
          9900012: F:= Format('%d', [Zveno]);                                         // ��������� �����
          9900013: F:= Format('%d', [DirCode]);                                       // ��� �����������
          9900014: F:= CodeToText(SecName, CharSet);                                  // �������
          9900015: F:= Format('%d', [Path]);                                          // ����
          9900016: if MoveDir = 0 then F:= '[-]' else F:= '[+]';                      // ����������� ��������
        end;
        FName:= FName + F + Config.FNSpace;
      end;
  end;
  Delete(FName, Length(FName), 1);
  FName:= FName + ExtractFileExt(FileList[I].Name);
  Result:= FName; *)
end;

begin
  J:= 0;                  // ���������� ���������� ������
  for I:= 0 to ListView1.Items.Count - 1 do if ListView1.Items.Item[I].Checked then Inc(J);
  if J = 0 then Exit;

  ProgressBar1.Position:= 0;
  WaitSheet1.Show;
  ProgressBar1.Min:= 0;
  ProgressBar1.Max:= J;
  Self.Refresh;

  if not cbAskFileName.Checked then // �� ���������� ��� �����
  begin
    Dir:= Config.LastFlashDir;
    if MySelectDirectory(LangTable.Caption['FileBase:Selectdirectory'], '', Dir) then Config.LastFlashDir:= Dir
                                                                else begin
                                                                       ReadFiles.Show;
                                                                       Self.Refresh;
                                                                       Exit;
                                                                     end;

    for I:= 0 to ListView1.Items.Count - 1 do
      if ListView1.Items.Item[I].Checked then
      begin
        ProgressBar1.Position:= ProgressBar1.Position + 1;
        Self.Refresh;
        Sleep(10);
        FName:= Config.LastFlashDir + '\' + GetName(I);
        if FileExists(FName) then
          case MessageBox(Handle, PChar(LangTable.Caption['USBFlash:Filealreadyexist'] + #10 + ExtractFileName(FName) + #10 + LangTable.Caption['USBFlash:Rewrite']), PChar(LangTable.Caption['Common:Attention']), 35) of
            IDYES: DeleteFile(PChar(FName));
            IDNO: Continue;
            IDCANCEL: Break;
          end;
        Config.AddToDownLoadFilesHistory(FName);
        CopyFile(PChar(FileList[I].Name), PChar(FName), True);
      end;

  end
  else
  begin                          // ���������� ��� �����
    for I:= 0 to ListView1.Items.Count - 1 do
      if ListView1.Items.Item[I].Checked then
      begin
        ProgressBar1.Position:= ProgressBar1.Position + 1;
        SaveDialog1.InitialDir:= Config.LastFlashDir;
        SaveDialog1.FileName:= GetName(I);
        if not SaveDialog1.Execute then Break; // ��� �� ����� - ��������� � ���������� �����
        Config.LastFlashDir:= ExtractFilePath(SaveDialog1.FileName);
        Self.Refresh;

        if FileExists(SaveDialog1.FileName) then
          case MessageBox(Handle, PChar(LangTable.Caption['USBFlash:Filealreadyexist'] + #10 + ExtractFileName(SaveDialog1.FileName) + #10 + LangTable.Caption['USBFlash:Rewrite']), PChar(LangTable.Caption['Common:Attention']), 35) of
            IDYES: DeleteFile(PChar(SaveDialog1.FileName));
            IDNO: Continue;
            IDCANCEL: Break;
          end;
        Config.AddToDownLoadFilesHistory(SaveDialog1.FileName);
        CopyFile(PChar(FileList[I].Name), PChar(SaveDialog1.FileName), True);
        Config.LastFlashDir:= ExtractFilePath(SaveDialog1.FileName);
      end;
  end;

  ProgressBar1.Position:= ProgressBar1.Max;
  ReadFiles.Show;
  Self.Refresh;

  /////////////////////////////////////////////////////////////////////////////

end;

procedure TFlashCardForm2.sbUpdateClick(Sender: TObject);
begin
  CreateFileList;
end;

procedure TFlashCardForm2.cbAskFileNameClick(Sender: TObject);
begin
  Config.AskFlashName:= cbAskFileName.Checked;
end;

procedure TFlashCardForm2.FormResize(Sender: TObject);
begin
  Panel10.Left:= (WaitSheet1.Width - Panel10.Width) div 2;
  Panel10.Top:= (WaitSheet1.Height - Panel10.Height) div 2;
end;

procedure TFlashCardForm2.FormShow(Sender: TObject);
begin
//  Timer2.Enabled:= true;
end;

procedure TFlashCardForm2.OnChangeLanguage(Sender: TObject);
var
  Item: TTreeNode;

begin
  Self.Caption                := LangTable.Caption['USBFlash:FormTitle'];
  lConnect.Caption            := LangTable.Caption['USBFlash:ConnectMessage'];
  cbAskFileName.Caption       := LangTable.Caption['USBFlash:RequestFileName'];
  tbiSelectAll.Caption        := LangTable.Caption['USBFlash:SelectAll'];
  sbLoad.Caption              := LangTable.Caption['USBFlash:Load'];
  lDisk.Caption               := LangTable.Caption['USBFlash:Disk'];

  tbiDelFiles.Caption         := LangTable.Caption['Notebook:Delete'];
  lWait.Caption               := LangTable.Caption['Common:Wait'];
  sbUpdate.Caption            := LangTable.Caption['FileBase:Update'];
  bClose.Caption              := LangTable.Caption['Common:CloseButton'];
{
  ListView1.Column[0].Caption := '';
  ListView1.Column[1].Caption := LangTable.Caption['InfoBar:Date'];
  ListView1.Column[2].Caption := LangTable.Caption['Common:Coordinate'];
  ListView1.Column[3].Caption := LangTable.Caption['Common:Tracknumber'];
  ListView1.Column[4].Caption := LangTable.Caption['InfoBar:OperatorName'];
  ListView1.Column[5].Caption := LangTable.Caption['FileBase:Tracksection'];
  ListView1.Column[6].Caption := LangTable.Caption['FileBase:FileName'];
}

  ListView1.Column[0].Caption := '';
  ListView1.Column[1].Caption := LangTable.Caption['InfoBar:Date'];
  ListView1.Column[2].Caption := LangTable.Caption['FileBase:Tracksection'];
  ListView1.Column[3].Caption := LangTable.Caption['Common:Tracknumber'];
  ListView1.Column[4].Caption := LangTable.Caption['Common:Coordinate'];
  ListView1.Column[5].Caption := LangTable.Caption['InfoBar:OperatorName'];
  ListView1.Column[6].Caption := LangTable.Caption['FileBase:FileName'];


end;

procedure TFlashCardForm2.tbiSelectAllClick(Sender: TObject);
var
  I: Integer;

begin

  for I:= 0 to ListView1.Items.Count - 1 do
    ListView1.Items.Item[I].Checked:= tbiSelectAll.Tag = 0;

  if tbiSelectAll.Tag = 0 then tbiSelectAll.Tag:= 1
                          else tbiSelectAll.Tag:= 0;
end;

procedure TFlashCardForm2.tbiDelFilesClick(Sender: TObject);
var
  I, J, K, QQ: Integer;
  Res: Integer;
  ResFile: TMemoryStream;
  Flag: Boolean;
  T1: Integer;
  S, SS: Integer;
  S1, S2: string;
  F: string;
  FName: string;
  FName2: string;
  Dir: string;
  ResFileName: string;
  ResFileName2: string;

begin
  if MessageBox(Handle,
                PChar(LangTable.Caption['Config:Delete'] + '?'),
                PChar(LangTable.Caption['Common:Attention']),
                36) <> IDYES then Exit;

  J:= 0;
  for I:= 0 to ListView1.Items.Count - 1 do if ListView1.Items.Item[I].Checked then Inc(J);
  if J = 0 then Exit;

  Self.Refresh;

  ProgressBar1.Position:= 0;
  WaitSheet1.Show;
  ProgressBar1.Min:= 0;
  ProgressBar1.Max:= J;
  Self.Refresh;

  for I:= 0 to ListView1.Items.Count - 1 do
    if ListView1.Items.Item[I].Checked then
    begin
      ProgressBar1.Position:= ProgressBar1.Position + 1;
      ProgressBar1.Refresh;
      sleep(10);
      DeleteFile(PChar(FileList[I].Name));
    end;

  ProgressBar1.Position:= ProgressBar1.Max;
  if Length(FileList) <> 0 then
    DeleteEmptyDir(ExtractFileDrive(FileList[0].Name) + '\DefResults\');
  ReadFiles.Show;
  Self.Refresh;
  CreateFileList;
end;

(*
function TFlashCardForm2.TestState: TDriveState;
var
  F: file;
  EndFlg: Boolean;
  I, J: Integer;
  List: TStringList;
  Drives: TStringList;
  FileID: TAviconID;

begin
  Drives:= TStringList.Create;

  EndFlg:= False;
  Result.Select:= '';
  Result.Count:= 0;

  SearchDrives(Drives);
  if Drives.Count = 0 then Exit;

  for I:= 0 to Drives.Count - 1 do
  begin
    List:= TStringList.Create;

    ScanDir(Drives[I] + 'DefResults\', '*.*', List);
    ScanDir(Drives[I] + '\', '*.*', List);

    if List.Count <> 0 then
      for J:= 0 to {Min(512, }List.Count - 1{)} do
        if FileExists(List[J]) and
           TestFileAttr(List[J]) and
           (Integer(List.Objects[J]) >= SizeOf(TFileHeader)) { and
           (Integer(List.Objects[J]) < 64 * 1024 * 1024)} then
        begin
          AssignFile(F, List[J]);
          try
            Reset(F, 1);
            BlockRead(F, FileID, 8);
          except
            CloseFile(F);
            Continue;
          end;
          CloseFile(F);

          if CompareMem(@FileID, @AviconFileID, 8) then
          begin
            Result.Select:= Drives[I];
            Inc(Result.Count);
            Break;
          end;
        end;
    List.Free;
  end;
  Drives.Free;
end;
*)
(*
procedure TFlashCardForm2.CreateDirList;
var
  F: file;
  EndFlg: Boolean;
  I, J, K: Integer;
  List: TStringList;
  Drives: TStringList;
  FileID: TAviconID;
  Value: Integer;
  DirName: string;

begin
  Drives:= TStringList.Create;

  EndFlg:= False;
  //Result.Select:= '';
  //Result.Count:= 0;

  SearchDrives(Drives);
  if Drives.Count = 0 then Exit;

  for I:= 0 to Drives.Count - 1 do
  begin
    List:= TStringList.Create;

    ScanDir_(Drives[I] {+ '\'}, '*.*', List);

    if List.Count <> 0 then
      for J:= 0 to List.Count - 1 do
      begin

        for K:= Length(List[J]) downto 1 do
          if List[J][K] = '\' then
          begin
            DirName:= Copy(List[J], K + 1, 255);
            Break;
          end;

        if (Pos('DefResults', List[J]) <> 0) or
          ((Length(DirName) = 5) and TryStrToInt(DirName, Value) and (Value > 14001) and (Value < 30999)) then SearchPaths.Add(List[J] + '\');

      end;

          (*

      for J:= 0 to {Min(512, }List.Count - 1{)} do
        if FileExists(List[J]) and
           TestFileAttr(List[J]) and
           (Integer(List.Objects[J]) >= SizeOf(TFileHeader)) { and
           (Integer(List.Objects[J]) < 64 * 1024 * 1024)} then
        begin
          AssignFile(F, List[J]);
          try
            Reset(F, 1);
            BlockRead(F, FileID, 8);
          except
            CloseFile(F);
            Continue;
          end;
          CloseFile(F);

          if CompareMem(@FileID, @AviconFileID, 8) then
          begin
            Result.Select:= Drives[I];
            Inc(Result.Count);
            Break;
          end;
        end;
    List.Free;  * )
  end;
  Drives.Free;

//  SearchPaths.SaveToFile('D:\flash_log.txt');
end;
*)

procedure TFlashCardForm2.Timer2Timer(Sender: TObject);
var
  I: Integer;
  SaveIdx: Integer;
  SaveName: string;

begin
  Timer2.Enabled:= false;
//  CreateDirList;

//  if (LastSearchPaths.Count <> SearchPaths.Count) then
//  begin
{    SaveIdx:= cbDiskList.ItemIndex;
    if SaveIdx <> - 1 then SaveName:= cbDiskList.Items[SaveIdx];
    SearchDrives(cbDiskList.Items);
    if SaveIdx = - 1
      then cbDiskList.ItemIndex:= 0
      else
      begin
        I:= cbDiskList.Items.IndexOf(SaveName);
        if I <> - 1 then cbDiskList.ItemIndex:= I
                    else cbDiskList.ItemIndex:= 0;
      end;

 }
    if SearchPaths.Count <> 0 then
    begin
//      cbDiskList.ItemIndex:= cbDiskList.Items.IndexOf(State.Select);
      CreateFileList;
      ReadFiles.Show;
    end
    else WaitSheet3.Show;
//    LastState:= State;
//  end;
end;

procedure TFlashCardForm2.DeleteEmptyDir(Path: string);
var
  I: Integer;
  List: TStringList;
  DirList: TStringList;

begin
  List:= TStringList.Create;
  ScanDir(Path, '*.*', List);
  for I:= 0 to List.Count - 1 do
    if GetFileAttributes(PChar(List[I])) and FILE_ATTRIBUTE_DIRECTORY <> 0 then
    begin
      DirList:= TStringList.Create;
      ScanDir(List[I], '*.*', DirList);
      if DirList.Count = 0 then RemoveDir(List[I]);
      DirList.Free;
    end;
  List.Free;
end;

// ----------------------------------------------------------------------------------------

constructor TCreateDirListProc.Create();
begin
  Stop:= False;
  ListOK:= nil;
  Drives:= TStringList.Create;
  List:= TStringList.Create;
  Paths:= TStringList.Create;
  LastPaths:= TStringList.Create;
  inherited Create(True);
end;

destructor TCreateDirListProc.Destroy;
begin
  Drives.Free;
  List.Free;
  Paths.Free;
  LastPaths.Free;
end;

procedure TCreateDirListProc.SearchDrives(DiskList: TStrings);
var
  w: dword;
  Root: string;
  i: integer;
  f:textfile; // Dalee textovie peremennie

function DiskInDrive(ADriveLetter : Char) : Boolean;
var
  SectorsPerCluster,
  BytesPerSector,
  NumberOfFreeClusters,
  TotalNumberOfClusters   : Cardinal;
  OldMode:Word;

begin
  OldMode:=SetErrorMode(SEM_FAILCRITICALERRORS);
  Result := GetDiskFreeSpace(PChar(ADriveLetter+':\'),
                              SectorsPerCluster,
                              BytesPerSector,
                              NumberOfFreeClusters,
                              TotalNumberOfClusters);

  SetErrorMode(OldMode);
end;

begin

  DiskList.Clear;
  w := GetLogicalDrives;
  Root := '#:\';

  for i := Ord('C') to Ord('X') do
  begin
    Root[1] := Char(i);
    if (W and (1 shl (i - Ord('A')))) > 0 then
    if DiskInDrive(Char(i)) and (GetDriveType(Pchar(Root)) = DRIVE_removable) then
      DiskList.Add(Root);
  end;
end;

procedure TCreateDirListProc.Execute;
var
  I, J, K: Integer;
  Value: Integer;
  DirName: string;
  Time: LongInt;

begin

  Time:= 0;
  while not Terminated do
  begin
    while GetTickCount() - Time < 3000 do
    begin
      if Terminated then Break;
      Sleep(300);
    end;
    SearchDrives(Drives);
    Paths.Clear;
    if Drives.Count <> 0 then
    begin
      for I:= 0 to Drives.Count - 1 do
      begin
        List.Clear;
        ScanDir_(Drives[I], '*.*', List);

        if List.Count <> 0 then
          for J:= 0 to List.Count - 1 do
          begin

            for K:= Length(List[J]) downto 1 do
              if List[J][K] = '\' then
              begin
                DirName:= Copy(List[J], K + 1, 255);
                Break;
              end;

            if (Pos('DefResults', List[J]) <> 0) or
              ((Length(DirName) = 5) and TryStrToInt(DirName, Value) and (Value > 14001) and (Value < 30999)) then Paths.Add(List[J] + '\');

            if Terminated then Break;
          end;
          if Terminated then Break;
      end;
    end;
    if Assigned(ListOK) then
    begin
      if LastPaths.Text <> Paths.Text then
      begin
        ListOK(Paths);
        LastPaths.Assign(Paths);
      end;
    end;
    Time:= GetTickCount();
  end;
  Stop:= True;
end;


end.


